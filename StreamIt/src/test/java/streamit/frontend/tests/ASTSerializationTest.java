package streamit.frontend.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.TestName;

import streamit.frontend.ToAST;

/*
 * Created by farleylai on 8/27/14.
 */
public class ASTSerializationTest {
    private String str() {
        return String.format("StreamIt/src/test/streamit/%s/%s.str", INFO.getMethodName(), INFO.getMethodName());
    }

    private String xml() {
        return String.format("StreamIt/src/test/streamit/%s/%s.xml", INFO.getMethodName(), INFO.getMethodName());
    }

    private String[] args() {
        return new String[] { str(), "--output", xml() };
    }

    @Rule
    public TestName INFO = new TestName();

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void insert() throws Exception {
        System.out.println(args());
        new ToAST().run(args());
    }

    @Test
    public void FIR() throws Exception {
        System.out.println(args());
        new ToAST().run(args());
    }

    @Test
    public void FIRcoarse() throws Exception {
        System.out.println(args());
        new ToAST().run(args());
    }

    @Test
    public void test3() throws Exception {
        System.out.println(args());
        new ToAST().run(args());
    }

    @Test
    public void MergeSort() throws Exception {
        System.out.println(args());
        new ToAST().run(args());
    }

    @Test
    public void SplitJoin() throws Exception {
        System.out.println(args());
        new ToAST().run(args());
    }
}
