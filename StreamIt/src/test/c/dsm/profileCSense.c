#include "profile.h"

#define SIZE0   1024 * MULT
#define SIZE1   2048 * MULT
static int BUFFER[SIZE1];

// work0 push window range0
#define offset00    0
#define size00      512 * MULT
#define limit00     512 * MULT
// work0 push window range1
#define offset01    1536 * MULT
#define size01       512 * MULT
#define limit01     2048 * MULT
#ifdef POINTER
static int *_push00, *_push01;
#else
static int _push00, _push01;
#endif
static inline void reset0() {
#ifdef POINTER
    //_push00 = &BUFFER[offset00];
    //_push01 = &BUFFER[offset01];
    _push00 = BUFFER + offset00;
    _push01 = BUFFER + offset01;
#else
    _push00 = offset00;
    _push01 = offset01;
#endif
}
static inline void push00(int val) {
#ifdef POINTER
    *_push00++ = val;
#else
    BUFFER[_push00++] = val;
#endif
}
static inline void push01(int val) {
#ifdef POINTER
    *_push01++ = val;
#else
    BUFFER[_push01++] = val;
#endif
}

// work1 pop/push window range0
#define offset10    0
#define size10      512 * MULT
#define limit10     512 * MULT
// work1 pop/push window range1
#define offset11     512 * MULT
#define size11      1024 * MULT
#define limit11     1536 * MULT
// work1 pop/push window range2
#define offset12    1536 * MULT
#define size12       512 * MULT
#define limit12     2048 * MULT
#ifdef POINTER
static int *_pop10, *_pop12;
static int *_push10, *_push11, *_push12;
#else
static int _pop10, _pop12;
static int _push10, _push11, _push12;
#endif
static inline void reset1() {
#ifdef POINTER
    //_pop10 = &BUFFER[offset10];
    //_pop12 = &BUFFER[offset12];
    //_push10 = &BUFFER[offset10];
    //_push11 = &BUFFER[offset11];
    //_push12 = &BUFFER[offset12];
    _pop10 = BUFFER + offset10;
    _pop12 = BUFFER + offset12;
    _push10 = BUFFER + offset10;
    _push11 = BUFFER + offset11;
    _push12 = BUFFER + offset12;
#else
    _pop10 = offset10;
    _pop12 = offset12;
    _push10 = offset10;
    _push11 = offset11;
    _push12 = offset12;
#endif
}
static inline int pop10() {
#ifdef POINTER
    return *_pop10++;
#else
    return BUFFER[_pop10++];
#endif
}
static inline int pop12() {
#ifdef POINTER
    return *_pop12++;
#else
    return BUFFER[_pop12++];
#endif
}
static inline void push10(int val) {
}
static inline void push11(int val) {
#ifdef POINTER
    *_push11++ = val;
#else
    BUFFER[_push11++] = val;
#endif
}
static inline void push12(int val) {
}

// work2 input window pop ranges
#define offset20    0
#define size20      2048 * MULT
#define limit20     2048 * MULT
#ifdef POINTER
static int *_pop2;
#else
static int _pop2;
#endif
static inline void reset2() {
#ifdef POINTER
    //_pop2 = &BUFFER[offset20];
    _pop2 = BUFFER + offset20;
#else
    _pop2 = offset20;
#endif
}
static inline int pop2() {
#ifdef POINTER
    return *_pop2++;
#else
    return BUFFER[_pop2++];
#endif
}

static inline int compute(int i) {
    return i + i * 4 / -2;
}

void work0() {
    for(int i = 0; i < 512 * MULT; i++)
        push00(i);
    for(int i = 512 * MULT; i < 1024 * MULT; i++)
        push01(i);
}

void work1() {
    for(int i = 0; i < 512 * MULT; i++) 
        push10(pop10());
    for(int i = 0; i < 1024 * MULT; i++) 
        push11(compute(i));
    for(int i = 0; i < 512 * MULT; i++)
        push12(pop12());
}

void work2() {
    int sum = 0;
    for(int i = 0; i < size20; i++) {
        //printf("BUFFER[%d]: %d\n", i, BUFFER[i]);
        sum += pop2();
    }
    if(sum != 0) {
        printf("checksum error: %d != 0\n", sum);
        exit(-1);
    }
}

int main() {
    start();
    for(int i = 0; i < ITERATIONS; i++) {
        reset0();
        work0();
        reset1();
        work1();
        reset2();
        work2();
    }
    timeout("CSense");
    return 0;
}
