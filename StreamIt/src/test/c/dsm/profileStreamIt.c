#include "profile.h"

#define SIZE0 1024 * MULT
#define SIZE1 2048 * MULT

static int BUF0[SIZE0];
static int BUF1[SIZE1];
static int head0, tail0;
static int head1, tail1;
static inline void reset0() {
    head0 = 0;
}
static inline void reset1() {
    tail0 = 0;
    head1 = 0;
}
static inline void reset2() {
    tail1 = 0;
}

static inline void push0(int val) {
    BUF0[head0++] = val;
}

static inline int pop0() {
    return BUF0[tail0++];
}

static inline void push1(int val) {
    BUF1[head1++] = val;
}

static inline int pop1() {
    return BUF1[tail1++];
}

static inline int compute(int i) {
    return i + i * 4 / -2;
}

void work0() {
    for(int i = 0; i < SIZE0; i++)
        push0(i);
}

void work1() {
    for(int i = 0; i < SIZE0/2; i++)
        push1(pop0());
    for(int i = 0; i < SIZE0; i++) 
        push1(compute(i));
    for(int i = 0; i < SIZE0/2; i++)
        push1(pop0());
}

void work2() {
    int sum = 0;
    for(int i = 0; i < SIZE1; i++) {
        //printf("OUTPUT[%d]: %d\n", i, OUTPUT[i]);
        sum += pop1();
    }
    if(sum != 0) {
        printf("checksum error: %d != 0\n", sum);
        exit(-1);
    }
}

int main() {
    start();
    for(int i = 0; i < ITERATIONS; i++) {
        reset0();
        work0();
        reset1();
        work1();
        reset2();
        work2();
    }
    timeout("StreamIt");
    return 0;
}
