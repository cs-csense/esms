#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

// timer
#define ITERATIONS 100
#define MULT 1024 * 10

static struct timeval beginning, now;

time_t us(struct timeval val) {
    return val.tv_sec * 1000000 + val.tv_usec;
}

struct timeval timeval(time_t us) {
    struct timeval val;
    val.tv_sec = us / 1000000;
    val.tv_usec = us - val.tv_sec * 1000000;
    return val;
}

time_t elapse(struct timeval from, struct timeval to) {
    return us(to) - us(from);
}

void start() {
    gettimeofday(&beginning, NULL);
}

void timeout(const char* test) {
    gettimeofday(&now, NULL);
    time_t duration = elapse(beginning, now);
    printf("%s time elapse: %ldus\n", test, duration);
}
