#include<stdio.h>
#include<math.h>

void testFloat() {
    int taps = 64; 
    int cutoff = 108000000;
    int rate = 250000000;
    float pi = 3.1415927f;//(float)Math.PI;
    float coeff[taps];

    float m = 64 - 1;
    float w = 2 * pi * cutoff / rate;
    for (int i = 0; i < taps; i++) {
        if (i - m/2 == 0)
            coeff[i] = w/pi;
        else
            coeff[i] = (float)(sinf(w*(i-m/2)) / pi / (i-m/2) *
                    (0.54 - 0.46 * cosf(2*pi*i/m)));
    }

    float sum = 0;
    for(int i = 0; i < taps; i++) {
        sum += (float)i * coeff[i];
        printf("peek(%d): %f, coeff[%d]: %f, sum: %f\n", i, (float)i, i, coeff[i], sum);
    }
}

void testDouble() {
    int taps = 64; 
    int cutoff = 108000000;
    int rate = 250000000;
    double pi = 3.1415927;//Math.PI;
    double coeff[taps];

    double m = 64 - 1;
    double w = 2 * pi * cutoff / rate;
    for (int i = 0; i < taps; i++) {
        if (i - m/2 == 0)
            coeff[i] = w/pi;
        else
            coeff[i] = (sin(w*(i-m/2)) / pi / (i-m/2) *
                    (0.54 - 0.46 * cos(2*pi*i/m)));
    }

    double sum = 0;
    for(int i = 0; i < taps; i++) {
        sum += i * coeff[i];
        printf("peek(%d): %f, coeff[%d]: %f, sum: %f\n", i, (float)i, i, coeff[i], sum);
    }
}

void testCSense() {
    float LowPassFilter0_coeff[64];
    float m=(64)-(1);
    float w=(((2)*(3.1415927f))*(108000000))/(250000000);
    for(int i = 0; (i)<(64); (i)++) {
        if(((i)-((m)/(2)))==(0)) LowPassFilter0_coeff[i] = (w)/(3.1415927f);
        else LowPassFilter0_coeff[i] = (((sinf((w)*((i)-((m)/(2)))))/(3.1415927f))/((i)-((m)/(2))))*((0.54f)-((0.46f)*(cosf((((2)*(3.1415927f))*(i))/(m)))));
    }   

    float sum=0;
    for(int i=0; (i)<(64); (i)++) {
        sum += (i)*(LowPassFilter0_coeff[i]);
        printf("%s%d%s%f%s%d%s%f%s%f\n", "peek(", i, "): ", (float)i, ", coeff[", i, "]: ", LowPassFilter0_coeff[i], ", sum: ", sum);
    }  
}

#include<float.h>

void testAtan() {
    double temp1 = 62981764.0000000000000000f;
    double temp2 = (2.14859168E8f  *  atanf(temp1));
    printf("temp2: %.16f\n", temp2);
}

int main() {
    //testFloat();
    //testDouble();
    //testCSense();
    printf("FLT_EVAL_METHOD=%d\n", FLT_EVAL_METHOD);
    testAtan();
    return 0;
}
