#include <stdio.h>
#include <string.h>
#include "profile.h"

#define LOOP COUNT/FACTOR

float BUFFER[PEEK-1]; 
int push_index = 0; 
int pop_index = 0;

void prework() { 
    for (int i = 0; i < PEEK - 1; i++) 
        BUFFER[push_index++] = i ;
}

#ifdef RET
float work() { 
#else
void work() {
#endif
    float TEMP_BUFFER[SIZE]; 
    push_index = PEEK - 1; 
    pop_index = 0;

    // copy from BUFFER to TEMP_BUFFER 
#ifdef MEMCPY
    memcpy(TEMP_BUFFER, BUFFER, (PEEK-1) * sizeof(float));
#else
    for (int i = 0; i < (PEEK - 1); i++) 
        TEMP_BUFFER[i] = BUFFER[i];
#endif

    // run Source FACTOR times
    for (int k = 0; k < FACTOR; k++) 
        TEMP_BUFFER[push_index++] = k ;
    
    // run FIR FACTOR times
    float result = 0; 
    for (int k = 0; k < FACTOR; k++) { 
        for (int i = 1; i < PEEK; i++) 
            result += i * TEMP_BUFFER[pop_index + i];
        pop_index++; 
#ifdef IO        
        printf("%.02f", result);
#endif
    }

    // copy from TEMP_BUFFER to BUFFER
#ifdef MEMCPY
    memcpy(BUFFER, TEMP_BUFFER+PEEK, (PEEK-1) * sizeof(float));
#else
    for (int i = 0; i < PEEK-1; i++)  
        BUFFER[i] = TEMP_BUFFER[i + PEEK];
#endif

#ifdef RET    
    return result;
#endif
} 

int main() {
#ifdef RET
    float ret;
#endif
    prework();
    for(int i = 0; i < LOOP; i++)
#ifdef RET
        ret = work();
#else
        work();
#endif
    return 0;
}
