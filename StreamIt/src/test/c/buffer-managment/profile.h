#define COUNT (10080000*2)
#ifdef LARGE
#define PEEK 64
#define SIZE 512
#define FACTOR 256
#else
#define PEEK 4
#define SIZE 32
#define FACTOR 16
#endif
