#include <stdio.h>
#include "profile.h"

#define LOOP COUNT/FACTOR

float BUFFER[SIZE]; 
int push_index = 0; 
int pop_index = 0;

void prework() { 
    for (int i = 0; i < PEEK - 1; i++) 
        BUFFER[push_index++] = i ;
}

#ifdef RET
float work() { 
#else
void work() {
#endif
    // run Source 
    for (int k = 0; k < FACTOR; k++) {
        BUFFER[push_index] = k; 
        push_index = (push_index + 1) & (SIZE-1);
    }

    // run FIR 48 times
    float result = 0; 
    for (int k = 0; k < FACTOR; k++) { 
        for (int i = 1; i < PEEK; i++)
            result += i * BUFFER[(pop_index + i) & (SIZE-1)];
        pop_index = (pop_index+1) & (SIZE-1); 
#ifdef IO    
        printf("%.02f", result);
#endif
    }
#ifdef RET
    return result;
#endif
} 

int main() {
#ifdef RET
    float ret;
#endif
    prework();
    for(int i = 0; i < LOOP; i++)
#ifdef RET
        ret = work();
#else
        work();
#endif
    return 0;
}
