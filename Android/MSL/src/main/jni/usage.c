#include <jni.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>

/* For getrusage().  Note that on Linux _GNU_SOURCE needs to be defined
 * explicitly before including sys/resource.h otherwise RUSAGE_THREAD won't
 * be available.  The configure script should place _GNU_SOURCE in config.h
 * which is included above.  */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE 1
#endif

#include <sys/resource.h>
#include <unistd.h>

#include "log.h"

/* Android headers don't define RUSAGE_THREAD */
#ifndef RUSAGE_THREAD
#define RUSAGE_THREAD 1
#endif

static long JIFFY_HZ;

static inline long jiffy2ms(int jiffies) {
    if(!JIFFY_HZ) JIFFY_HZ = sysconf(_SC_CLK_TCK);
    //LOGD("jiffy2ms()", "JIFFY_HZ=%d, jiffies=%d, ms=%d\n", JIFFY_HZ, jiffies, jiffies * 1000 / JIFFY_HZ);
    return jiffies * (1000 / JIFFY_HZ);
}

static inline long long real_time() {
    struct timespec spec;
    if(clock_gettime(CLOCK_MONOTONIC, &spec)) {
        LOGE("clock_gettime(CLOCK_MONOTONIC)", "failed: %s\n", strerror(errno));
        return -1;
    }
    return spec.tv_sec * 1000000000ll + spec.tv_nsec;
}

static inline long long cpu_time() {
    struct timespec spec;
    if(clock_gettime(CLOCK_THREAD_CPUTIME_ID, &spec)) {
        LOGE("clock_gettime(CLOCK_THREAD_CPUTIME_ID)", "failed: %s\n", strerror(errno));
        return -1;
    }
    return spec.tv_sec * 1000000000ll + spec.tv_nsec;
}

static inline int stat(struct rusage* ru, long long* real, long long* cpu) {
#if defined(RUSAGE_THREAD)
    int who = RUSAGE_THREAD;
#elif defined(RUSAGE_LWP)
    int who = RUSAGE_LWP;
#else
    #error Neither RUSAGE_THREAD nor RUSAGE_LWP available. Only Linux (>2.6.26) and Solaris are known to support getrusage() on a per-thread basis.
#endif

    if(getrusage(who, ru)) {
        LOGW("getrusage()", "failed with errno %d\n", errno);
        return -1;
    }
//    long long user = ru->ru_utime.tv_sec * 1000000000ll + ru->ru_utime.tv_usec * 1000;
//    long long sys = ru->ru_stime.tv_sec * 1000000000ll + ru->ru_stime.tv_usec * 1000;
//    LOGI("getrusage()", "thread: %lldns, user: %lldns, sys: %lldns\n", user + sys, user, sys);

//    if(!clock_getres(CLOCK_REALTIME, spec))
//        LOGI("clock_gettime()", "CLOCK_REALTIME res: %lds, %ldns\n", spec->tv_sec, spec->tv_nsec);
//    if(!clock_getres(CLOCK_THREAD_CPUTIME_ID, spec))
//        LOGI("clock_gettime()", "CLOCK_THREAD_CPUTIME_ID res: %lds, %ldns\n", spec->tv_sec, spec->tv_nsec);
    *real = real_time();
    *cpu = cpu_time();
    return 0;
}

JNIEXPORT jint JNICALL Java_esms_usage_CPUStat_jiffies2ms(JNIEnv *env, jobject stat, jint jiffies) {
    return jiffy2ms(jiffies);
}

JNIEXPORT jobject JNICALL Java_esms_usage_RUsage_get(JNIEnv *env, jobject usage) {
    jclass cl = (*env)->GetObjectClass(env, usage);
    jfieldID timestamp = (*env)->GetFieldID(env, cl, "_timestamp", "J");
    jfieldID fieldsID = (*env)->GetFieldID(env, cl, "_fields", "[J");
    jlongArray fieldArray = (*env)->GetObjectField(env, usage, fieldsID);
    jlong *fields = (*env)->GetLongArrayElements(env, fieldArray, 0);

    struct rusage ru;
    long long clock, cpu;
    stat(&ru, &clock, &cpu);
    (*env)->SetLongField(env, usage, timestamp, clock / 1000000); // ns -> ms
    fields[0] = cpu / 1000; // replace (ru.ru_utime + ru.ru_stime)
    //fields[0] = ru.ru_utime.tv_sec * 1000000LL + ru.ru_utime.tv_usec;
    //fields[1] = ru.ru_stime.tv_sec * 1000000LL + ru.ru_stime.tv_usec;
    fields[2] = ru.ru_maxrss;
    fields[3] = ru.ru_ixrss;
    fields[4] = ru.ru_idrss;
    fields[5] = ru.ru_isrss;
    fields[6] = ru.ru_minflt;
    fields[7] = ru.ru_majflt;
    fields[8] = ru.ru_nswap;
    fields[9] = ru.ru_inblock;
    fields[10] = ru.ru_oublock;
    fields[11] = ru.ru_msgsnd;
    fields[12] = ru.ru_msgrcv;
    fields[13] = ru.ru_nsignals;
    fields[14] = ru.ru_nvcsw;
    fields[15] = ru.ru_nivcsw;
    (*env)->ReleaseLongArrayElements(env, fieldArray, fields, 0);
    return usage;
}

