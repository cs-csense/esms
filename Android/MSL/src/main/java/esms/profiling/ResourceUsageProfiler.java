package esms.profiling;

import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import esms.data.Performance;
import esms.usage.RUsage;
import esms.util.Formatter;

/**
 * Created by farleylai on 3/13/15.
 */
public class ResourceUsageProfiler extends InstantProfiler {
    private RUsage mRUsage = new RUsage();

    @Override
    public ResourceUsageProfiler save(File file) throws IOException {
        Performance perf = new Performance();
        perf.put(mRUsage);

        // report to logcat
        String title = Formatter.title("Performance");
        StringBuilder builder = new StringBuilder();
        builder.append(title).append("\n");
        builder.append(perf).append("\n");
        Log.d(tag(), builder.toString());

        // save to sdcard
        RandomAccessFile log = new RandomAccessFile(file.isFile() ? file : new File(file, "performance.log"), "rw");
        log.write(builder.toString().getBytes());
        log.close();
        return this;
    }
}
