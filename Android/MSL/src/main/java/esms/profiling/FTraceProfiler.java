package esms.profiling;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import esms.profiling.ftrace.FTraceEvent;
import esms.profiling.ftrace.FTracer;
import esms.runtime.Environment;
import esms.runtime.Program;
import esms.util.Log;
import esms.util.SuperSU;

/**
 * Created by farleylai on 12/10/15.
 */
public class FTraceProfiler extends ExternalProfiler {
    private final FTracer Instance;
    private final int BufferSizeInKBs;
    public FTraceProfiler() {
        this(16 * 1024);
    }

    public FTraceProfiler(int bufferSizeInKBs) {
        Instance = new FTracer();
        BufferSizeInKBs = Math.min(64 * 1024, bufferSizeInKBs); // 4096 on Nexus 6?
    }

    public boolean mark(String tag) {
        return Instance.mark(tag);
    }

    public FTraceProfiler register(FTraceEvent e) {
        Instance.register(e);
        return this;
    }

    public FTraceProfiler register(String e) {
        Instance.register(e);
        return this;
    }

    @Override
    public FTraceProfiler startService() {
       return this;
    }

    @Override
    public FTraceProfiler doStartProfiling() {
        if(SuperSU.available()) {
            Log.d(tag(), "SU is available for ftrace profiling");
            Instance.overwrite(false);
            Instance.bufferSizeInKBs(BufferSizeInKBs);
            Instance.clock(FTracer.Clock.global);
            if(Instance.enable()) {
                Log.i(tag(), "Enabled ftrace events");
                Instance.clear();
                Instance.mark("startProfiling-" + _timestamp);
            } else {
                Log.w(tag(), "Failed to enable trace events");
                Instance.reset();
                return this;
            }
        } else
            Log.e(tag(), "SU is not available or granted for ftrace profiling, quit");
        return this;
    }

    @Override
    public FTraceProfiler doStopProfiling() {
        Instance.mark("stopProfiling-" + timestamp());
        if(Instance.disable()) {
            Log.i(tag(), "Disabled trace events");
        } else {
            Log.w(tag(), "Failed to clear all trace events");
        }
        return this;
    }

    @Override
    public FTraceProfiler stopService() {
        Instance.reset();
        return this;
    }

    @Override
    public FTraceProfiler save(File file) throws IOException {
        if(Instance.save(file.getAbsolutePath()))
            Log.i(tag(), "Saved trace to %s\n", file.getAbsolutePath());
        else
            Log.e(tag(), "Failed to save trace to %s\n", file.getAbsolutePath());

        return this;
    }
}
