package esms.profiling.ftrace;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import esms.util.Log;
import esms.util.SuperSU;

/** Must have root access to debugfs
 *
 */
public class FTracer {
    public enum Clock {
        local, global, counter, uptime, perf
    }

    public static final String TAG = FTracer.class.getSimpleName();
    public static final File Path = new File("/d/tracing");
    public static final File OPTIONS = new File("/d/tracing/options");
    public static final File Events = new File(Path, "events");
    public static final File Trace = new File(Path, "trace");
    public static final File PIPE = new File(Path, "trace_pipe");
    private final List<FTraceEvent> Registry;
    public FTracer() {
        Registry = new ArrayList<>();
    }

    private String option(String opt) {
        return String.format("%s/%s", OPTIONS, opt);
    }

    private String field(String name) {
        return String.format("%s/%s", Path, name);
    }

    public int getInt(String target) { return getInt(target, true); }
    public int getInt(String target, boolean root) {
        List<String> res = SuperSU.run(root, "cat", target);
        if(res != null && res.size() > 0)
            return Integer.parseInt(res.get(0));
        return -1;
    }

    public boolean setInt(String target, int value) { return setInt(target, value, true); }
    public boolean setInt(String target, int value, boolean root) {
        return root
                ? SuperSU.su("echo", String.valueOf(value), ">", target)
                : SuperSU.sh("echo", String.valueOf(value), ">", target);
    }

    public String getString(String target) { return getString(target, true); }
    public String getString(String target, boolean root) {
        List<String> res = SuperSU.run(root, "cat", target);
        if(res != null && res.size() > 0)
            return res.get(0);
        return "";
    }

    public boolean setString(String target, String value) { return setString(target, value, true); }
    public boolean setString(String target, String value, boolean root) {
        return root
                ? SuperSU.su("echo", value, ">", target)
                : SuperSU.sh("echo", value, ">", target);
    }

    private boolean enable(FTraceEvent e) {
        return setInt(e.enable(), 1);
    }

    private boolean disable(FTraceEvent e) {
        return setInt(e.enable(), 0);
    }

    public int bufferSizeInKBs() {
        return getInt(field("buffer_size_kb"));
    }

    public boolean bufferSizeInKBs(int kbs) {
        return setInt(field("buffer_size_kb"), Math.min(32 * 1024, kbs));
    }

    public boolean overwrite() {
        return getInt(option("overwrite")) == 1;
    }

    public boolean overwrite(boolean value) {
        return setInt(option("overwrite"), value ? 1 : 0);
    }

    public boolean mark(String tag) {
        return setString(field("trace_marker"), tag);
    }

    public Clock clock() {
        String clockStr = getString(field("trace_clock"));
        String[] clocks = getString(field("trace_clock")).split(" ");
        for(String c: clocks) {
            if(c.startsWith("[")) {
                return Clock.valueOf(c.substring(1, c.length() - 1));
            }
        }
        throw new RuntimeException("Unknown trace clock " + clockStr);
    }

    public boolean clock(Clock clock) {
        // clock change causes to reset trace buffer
        return setString(field("trace_clock"), clock.name());
    }

    public boolean exists() {
        return Path.exists();
    }

//    public boolean create() {
//        if(Path.exists() || SuperSU.su("mkdir", Path.getAbsolutePath())) {
//            maxBufferSize = Math.min(8 * 1024, getInt(field("buffer_total_size_kb")));
//            return true;
//        }
//        return false;
//    }

//    public boolean remove() {
//        if (Path.exists()) SuperSU.su("rmdir", Path.getAbsolutePath());
//        return !Path.exists();
//    }

    public FTracer register(String event) {
        Registry.add(new FTraceEvent(event));
        return this;
    }

    public FTracer register(FTraceEvent event) {
        Registry.add(event);
        return this;
    }

    public boolean isEnabled(FTraceEvent e) {
        return getInt(e.enable()) == 1;
    }

    public boolean enable() {
        boolean ret = false;
        for(FTraceEvent e: Registry) {
            if(enable(e)) ret = true;
            else Log.w(TAG, "Failed to enable event %s\n", e);
        }
        if(ret) setInt(field("tracing_on"), 1);
        return ret;
    }

    public boolean disable() {
        return setString(field("set_event"), "\"\"") && setInt(field("tracing_on"), 0);
    }

    public boolean clear() {
        return SuperSU.su("echo", "\"\"", ">", Trace);
    }

    public boolean save(String filename) throws IOException {
        return SuperSU.su("cat", Trace, ">", filename);
    }

    public FTracer reset() {
        disable();
        clear();
        overwrite(true);
        clock(Clock.local);
        bufferSizeInKBs(1);
        return this;
    }

    @Override
    public String toString() {
        return Path.getAbsolutePath();
    }
}
