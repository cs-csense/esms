package esms.profiling.ftrace;

import java.io.File;

/**
 * Created by farleylai on 12/11/15.
 */
public class FTraceEvent {
    public static FTraceEvent SCHED_SWITCH       = new FTraceEvent("sched/sched_switch");
    public static FTraceEvent SCHED_MIGRATE_TASK = new FTraceEvent("sched/sched_migrate_task");       // root
    public static FTraceEvent SCHED_PROCESS_WAIT = new FTraceEvent("sched/sched_process_wait");       // root
    public static FTraceEvent SCHED_STAT_BLOCKED = new FTraceEvent("sched/sched_stat_blocked");       // root
    public static FTraceEvent SCHED_STAT_IOWAIT  = new FTraceEvent("sched/sched_stat_iowait");        // root
    public static FTraceEvent SCHED              = new FTraceEvent("sched", true);                    // root

    public static FTraceEvent CPU_IDLE          = new FTraceEvent("power/cpu_idle");
    public static FTraceEvent CPU_FREQ          = new FTraceEvent("power/cpu_frequency");
    public static FTraceEvent POWER             = new FTraceEvent("power", true);                     // root

    public static FTraceEvent NET               = new FTraceEvent("net", true);                       // root
    public static FTraceEvent NETIF_RX          = new FTraceEvent("net/netif_rx", true);              // root

    public static FTraceEvent BLK_RQ_ISSUE      = new FTraceEvent("block/block_rq_issue", true);      // root
    public static FTraceEvent BLK_RQ_COMPLETE   = new FTraceEvent("block/block_rq_complete", true);   // root

    private final File Path;
    private String Enable;
    private boolean isRoot;
    public FTraceEvent(String name, boolean root) {
        Path = new File(FTracer.Events, name);
        isRoot = root;
    }
    public FTraceEvent(String name) {
        this(name, false);
    }

    public String name() {
        return Path.getName();
    }

    public boolean root() { return isRoot; }

    private String make(String field) {
        return new File(Path, field).getAbsolutePath();
    }

    public String enable() {
        return Enable == null ? Enable = make("enable") : Enable;
    }

    public String filter() {
        return make("filter");
    }

    // Only available for terminal events
    public String id() {
        return make("id");
    }

    public String format() {
        return make("format");
    }

    public boolean isTerminal() {
        return new File(id()).exists();
    }

    @Override
    public boolean equals(Object e) {
        return e instanceof FTraceEvent && ((FTraceEvent) e).Path.equals(Path);
    }

    @Override
    public String toString() {
        return Path.getAbsolutePath();
    }
}
