package esms.profiling;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import esms.util.CommandLine;
import esms.util.Shell;

/**
 * Created by farleylai on 3/11/15.
 */
public class TrepnProfiler extends ExternalProfiler {
    private enum State {
        READY, PROFILING, PROFILED, EXPORTING
    }

    private static String TAG = TrepnProfiler.class.getName();
    private Context mCtx;
    private File mDB;
    private int mState;
    private Map<Integer, String> mStates;

    public TrepnProfiler(Context ctx) {
        mCtx = ctx;
        mStates = new HashMap<>();
    }

    @Override
    public int state() {
        int retries = 3;
        File state = new File(Environment.getExternalStorageDirectory(), "trepn/trepn_state");
        Scanner sc = null;
        while(retries-- > 0) {
            try {
                sc = new Scanner(state);
            } catch (FileNotFoundException e) {
                if(retries == 0)
                    throw new RuntimeException(e);
                else
                    wait(500);
            }
        }

        sc.nextLine();
        String key = sc.next();
        String value = sc.next();
        sc.close();
//        Log.d(TAG, String.format("trepn_state %s %s\n", key, value));
        if(key.startsWith("State")) {
            if(value.equals("Not_Profiling"))
                return State.READY.ordinal();
            else if(value.equals("Profiling"))
                return State.PROFILING.ordinal();
            else if(value.equals("Exporting"))
                return State.EXPORTING.ordinal();
            else
                throw new RuntimeException("Unexpected Trepn state: " + value);
        } else
            throw new RuntimeException("Incorrect Trepn state file: " + state);
    }

    public String description() {
        return mStates.get(mState);
    }

    public String description(int state) {
        return mStates.get(state);
    }

    public String info() {
        return String.format("Current AppState[%d] %s", mState, mStates.get(mState));
    }

    private void wait(int ms, State state) {
        double t = ms / 1000.0;
        int retries = 10;
        while(retries-- > 0 && state() != state.ordinal()) {
            Log.d(TAG, String.format("wait for %.2fs until %s\n", t, state));
            wait(ms);
        }
    }

    @Override
    public TrepnProfiler startService() {
        Log.d(TAG, "Starting service");
        Intent intent = new Intent();
        intent.setClassName("com.quicinc.trepn", "com.quicinc.trepn.TrepnService");
        mCtx.startService(intent);
        wait(1000);
        return this;
    }

    @Override
    public TrepnProfiler stopService() {
        Log.d(TAG, "Stopping service");
        Intent intent = new Intent();
        intent.setClassName("com.quicinc.trepn", "com.quicinc.trepn.TrepnService");
        mCtx.stopService(intent);
        wait(2000);
        return this;
    }

    @Override
    public TrepnProfiler doStartProfiling() {
        Intent intent = new Intent("com.quicinc.trepn.start_profiling");
        intent.putExtra("com.quicinc.trepn.database_file", (mDB = new File("energy")).getName());
        mCtx.sendBroadcast(intent);
        wait(500);
        update(State.PROFILING.ordinal(), State.PROFILING.name());
        return this;
    }

    @Override
    public TrepnProfiler doStopProfiling() {
        update(State.PROFILED.ordinal(), State.PROFILED.name());
        Intent intent = new Intent("com.quicinc.trepn.stop_profiling");
        mCtx.sendBroadcast(intent);
        //wait(250, State.READY);
        wait(1000);
        return this;
    }

    @Override
    public TrepnProfiler update() {
        return update(-1, null);
    }

    @Override
    public TrepnProfiler update(int state) {
        return update(state, null);
    }

    @Override
    public TrepnProfiler update(String desc) {
        return update(-1, desc);
    }

    @Override
    public TrepnProfiler update(int state, String desc) {
        Intent intent = new Intent("com.quicinc.Trepn.UpdateAppState");
        if(state >= 0) intent.putExtra("com.quicinc.Trepn.UpdateAppState.Value", mState = state);
        else mState++;
        if(desc != null)
            intent.putExtra("com.quicinc.Trepn.UpdateAppState.Value.Desc", desc);

        if(mStates.containsKey(mState))
            mStates.put(mState, desc == null ? mStates.get(mState) : desc);
        else
            mStates.put(mState, desc == null ? "" : desc);
        mCtx.sendBroadcast(intent);
        wait(500);
        Log.d(TAG, String.format("updating state[%d] %s\n", mState, desc));
        return this;
    }

    private TrepnProfiler exportCSV() {
        wait(250, State.READY);
        Intent intent = new Intent("com.quicinc.trepn.export_to_csv");
        intent.putExtra("com.quicinc.trepn.export_db_input_file", mDB.getName());
//        intent.putExtra("com.quicinc.trepn.export_csv_output_file", mDB.getName());
        mCtx.sendBroadcast(intent);
        wait(100, State.EXPORTING);
        wait(1000, State.READY);
        Log.d(TAG, "Done exporting CSV\n");
        return this;
    }

//    @Override
//    public TrepnProfiler save(String tk, String benchmark) {
//        // FIXME Exporting CSV after stopping profiling may fail the saving of the database.
//        exportCSV();
//        File from = new File("trepn/" + mDB + ".csv");
//        String to = path().getPath();
//        if(Shell.run(Environment.getExternalStorageDirectory(), new CommandLine("mv", from.getPath(), to)) != 0)
//            throw new RuntimeException("Failed to save to " + to);
//
//        // rename database
//        from = new File("trepn/" + mDB + ".db");
//        if(path().getName().equals(benchmark))
//            to = String.format("trepn/%s-%s.db", benchmark, tk);
//        else
//            to = String.format("trepn/%s-%s-%s.db", benchmark, tk, path().getName());
//        if(Shell.run(Environment.getExternalStorageDirectory(), new CommandLine("mv", from.getPath(), to)) != 0)
//            throw new RuntimeException("Failed to mv " + from + " to " + to);
//
//        return this;
//    }

    @Override
    public TrepnProfiler save(File file) {
        // FIXME Exporting CSV after stopping profiling may fail the saving of the database.
        exportCSV();
        File from = new File("trepn/" + mDB + ".csv");
        if(Shell.run(Environment.getExternalStorageDirectory(), new CommandLine("mv", from.getPath(), file.getAbsolutePath())) != 0)
            throw new RuntimeException("Failed to save to " + file.getAbsolutePath());
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n================ TrepnProfiler ================\n");
        builder.append(info()).append("\n");
        Object[] states = mStates.keySet().toArray();
        Arrays.sort(states);
        for(Object state: states) {
            builder.append("AppState[").append(state).append("] ");
            builder.append(mStates.get(state)).append("\n");
        }
        builder.append("DB: ").append(mDB).append(".db");
        return builder.toString();
    }
}
