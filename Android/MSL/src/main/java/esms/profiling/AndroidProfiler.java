package esms.profiling;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;

//import esms.usage.NetStat;
import esms.usage.BlockStat;
import esms.usage.RUsage;

public class AndroidProfiler extends InstantProfiler {
    private esms.usage.RUsage mRU;
    private BlockStat mIO;
//    private esms.usage.NetStat mNet;
    private ByteBuffer mRecords;

    public AndroidProfiler() {
        mRU = new RUsage();
        mIO = new BlockStat();
//        mNet = new NetStat();
        mRecords = ByteBuffer.allocateDirect(1024 * 1024);
    }

    @Override
    public AndroidProfiler profile(int cid, int eid) {
        if(!mRecords.hasRemaining()) {
            ByteBuffer prev = mRecords;
            mRecords = ByteBuffer.allocateDirect(2 * prev.capacity());
            prev.flip();
            mRecords.put(prev);
        }
        mRU.get();
        mRecords.putInt(cid);
        mRecords.putInt(eid);
        mRecords.putDouble(mRU.real());
        mRecords.putDouble(mRU.utime());
        mRecords.putDouble(mRU.stime());
        return this;
    }

    @Override
    public AndroidProfiler save(File file) throws IOException {
        PrintWriter out = new PrintWriter(file);
        out.println("Component, Event, real, user, sys");
        while(mRecords.hasRemaining()) {
            out.printf("%d, %d, %f, %f, %f\n", mRecords.getInt(), mRecords.getInt(), mRecords.getDouble(), mRecords.getDouble(), mRecords.getDouble());
        }
        out.close();
        return this;
    }
}
