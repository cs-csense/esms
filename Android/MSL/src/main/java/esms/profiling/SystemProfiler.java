package esms.profiling;

import android.content.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import esms.runtime.Environment;
import esms.runtime.Program;
import esms.runtime.Resource;
import esms.usage.BlockStat;
import esms.usage.CPUStat;
import esms.usage.CellUsage;
import esms.usage.DisplayUsage;
import esms.usage.EnergyUsage;
import esms.usage.Usage;
import esms.usage.WifiUsage;

/** Periodic profiler for various system stats.
 *
 */
public class SystemProfiler extends Profiler {
    private Context _ctx;
    private Deque<Usage> _cpuStats;
    private Deque<Usage> _blockStats;
    private Deque<Usage> _displayStats;
    private Deque<Usage> _wifiStats;
    private Deque<Usage> _cellStats;
    private Deque<Usage> _energyStats;
    private List<Deque<Usage>> _stats;
    private int _history;
    private File _tmp;
    private PrintStream _out;

    public SystemProfiler(Context ctx) {
        this(ctx, 1);
    }

    public SystemProfiler(Context ctx, int history) {
        _ctx = ctx;
        _history = history;
        _cpuStats = new ArrayDeque<>(history);
        _blockStats = new ArrayDeque<>(history);
        _displayStats = new ArrayDeque<>(history);
        _wifiStats = new ArrayDeque<>(history);
        _cellStats = new ArrayDeque<>(history);
        _energyStats = new ArrayDeque<>(history);
        _stats = new ArrayList<>();
        _stats.add(_cpuStats);
        _stats.add(_displayStats);
        _stats.add(_blockStats);
        _stats.add(_wifiStats);
        _stats.add(_cellStats);
        _stats.add(_energyStats);

        _tmp = Environment.file("system.log");
        try {
            _out = new PrintStream(_tmp);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public int history() {
        return _history;
    }

    public int history(int history) {
        int old = _history;
        if(history >= 1) {
            int diff = history - _history;
            if(diff < 0) {
                for (Deque<Usage> stats : _stats) {
                    if(stats.size() > history) {
                        for (int i = 0; i < -diff; i++)
                            stats.poll();
                    }
                }
            }
            _history = history;
        }
        return old;
    }

    public void profileCPU() {
        if(_cpuStats.size() < _history) _cpuStats.add(new CPUStat().get());
        else _cpuStats.add(_cpuStats.poll().get());
        _out.println(_cpuStats.peekLast());
    }

    public void profileBlock() {
        if(_blockStats.size() < _history) _blockStats.add(new BlockStat().get());
        else _blockStats.add(_blockStats.poll().get());
        _out.println(_blockStats.peekLast());
    }

    public void profileDisplay() {
        if(_displayStats.size() < _history) _displayStats.add(new DisplayUsage(_ctx).get());
        else _displayStats.add(_displayStats.poll().get());
        _out.println(_displayStats.peekLast());
    }

    public void profileWifi() {
        if(_wifiStats.size() < _history) _wifiStats.add(new WifiUsage(_ctx).get());
        else _wifiStats.add(_wifiStats.poll().get());
        _out.println(_wifiStats.peekLast());
    }

    public void profileCell() {
        if(_cellStats.size() < _history) _cellStats.add(new CellUsage(_ctx).get());
        else _cellStats.add(_cellStats.poll().get());
        _out.println(_cellStats.peekLast());
    }

    public void profileEnergy() {
        if(_energyStats.size() < _history) _energyStats.add(new EnergyUsage(_ctx).get());
        else _energyStats.add(_energyStats.poll().get());
        _out.println(_energyStats.peekLast());
    }

    public void profile() {
        profileCPU();
        profileBlock();
        profileDisplay();
        profileWifi();
        profileCell();
        profileEnergy();
    }

    @Override
    public SystemProfiler save(File file) {
        _out.close();
        if(!file.equals(_tmp))
            _tmp.renameTo(file);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(Deque<Usage> stats: _stats) {
            if(stats.isEmpty()) continue;
            Usage u = stats.peekLast();
            builder.append(u).append("\n");
        }
        builder.setLength(builder.length() - 1);
        return builder.toString();
    }
}
