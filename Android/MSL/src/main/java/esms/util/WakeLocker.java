package esms.util;

import android.content.Context;
import android.os.PowerManager;

/**
 * Created by farleylai on 6/6/16.
 */
public class WakeLocker {
    private final String TAG = getClass().getSimpleName();
    private Context _ctx;
    private PowerManager _pm;
    private PowerManager.WakeLock _partial;

    public WakeLocker(Context ctx) {
        _ctx = ctx;
        _pm = (PowerManager)_ctx.getSystemService(Context.POWER_SERVICE);
        _partial = _pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, TAG);
    }

    public WakeLocker release() {
        _partial.release();
        return this;
    }

    public WakeLocker acquire() {
        _partial.acquire();
        return this;
    }
}
