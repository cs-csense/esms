package esms.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import eu.chainfire.libsuperuser.Shell;

public class SuperSU {
    private static final String TAG = SuperSU.class.getSimpleName();
    private Shell.Interactive mRootSession;

    public static boolean available() {
        return Shell.SU.available();
    }

//    public SuperSU open() {
//        String suVersion = null;
//        String suVersionInternal = null;
//        boolean suAvailable = Shell.SU.available();
//        if(suAvailable) {
//            suVersion = Shell.SU.version(false);
//            suVersionInternal = Shell.SU.version(true);
//        }
//        mRootSession = new Shell.Builder()
//                .useSU()
//                .setWantSTDERR(true)
//                .setWatchdogTimeout(5)
//                .setMinimalLogging(true)
//                .open(new Shell.OnCommandResultListener() {
//                    @Override
//                    public void onCommandResult(int commandCode, int exitCode, List<String> output) {
//                        // note: this will FC if you rotate the phone while the dialog is up
//                        if (exitCode != Shell.OnCommandResultListener.SHELL_RUNNING) {
//                            Log.e(getClass().getSimpleName(), "Error opening root shell: exitCode %d\n", exitCode);
//                        } else {
//                            sendRootCommand();
//                        }
//                    }
//                });
//        StringBuilder sb = (new StringBuilder()).
//                append("Root? ").append(suAvailable ? "Yes" : "No").append((char)10).
//                append("Version: ").append(suVersion == null ? "N/A" : suVersion).append((char)10).
//                append("Version (internal): ").append(suVersionInternal == null ? "N/A" : suVersionInternal).append((char)10).
//                append((char)10);
//        Log.i(getClass().getSimpleName(), sb);
//        return this;
//    }

    private static StringBuilder mCommandBuilder = new StringBuilder();
    private static String command(String cmd, Object... args) {
        mCommandBuilder.setLength(0);
        mCommandBuilder.append(cmd);
        for(Object arg: args)
            mCommandBuilder.append(" ").append(arg);
//        mRootSession.addCommand(mCommandBuilder.toString());
        return mCommandBuilder.toString();
    }

    public static List<String> run(boolean su, String cmd, Object... args) {
        return su ? Shell.SU.run(command(cmd, args)) : Shell.SH.run(command(cmd, args));
    }

    public static boolean su(String cmd, Object... args) {
        Log.d(TAG, "su -C %s\n", command(cmd, args));
        List<String> res = run(true, cmd, args);
        if(res == null) {
            Log.w(TAG, "Failed to run su -C %s", command(cmd, args));
        } else {
            for (String line: res) Log.i(TAG, line);
        }
        return res != null;
    }

    public static boolean sh(String cmd, Object...args) {
        Log.d(TAG, "sh -C %s\n", command(cmd, args));
        List<String> res = run(false, cmd, args);
        if(res == null) {
            Log.w(TAG, "Failed to run su -C %s", command(cmd, args));
        } else {
            for (String line: res) Log.i(TAG, line);
        }
        return res != null;
    }

//    private void sendRootCommand() {
//        mRootSession.addCommand(new String[]{"id", "date", "ls -l /"}, 0, new Shell.OnCommandResultListener() {
//            @Override
//            public void onCommandResult(int commandCode, int exitCode, List<String> output) {
//                if (exitCode < 0) {
//                    Log.e(TAG, "Error executing commands: exitCode " + exitCode);
//                } else {
//                    for (String line : output) Log.i(TAG, line);
//                    Log.i(TAG, "----------");
//                    Log.i(TAG, "ls -l /");
//                }
//            }
//        });
//
//        mRootSession.addCommand(new String[]{"ls -l /"}, 1, new Shell.OnCommandLineListener() {
//            @Override
//            public void onCommandResult(int commandCode, int exitCode) {
//                if (exitCode < 0) {
//                    Log.e(TAG, "Error executing commands: exitCode %d\n", exitCode);
//                } else {
//                    Log.i(TAG, "----------");
//                    Log.i(TAG, "ls -l /sdcard");
//                }
//            }
//
//            @Override
//            public void onLine(String line) {
//                Log.i(TAG, line);
//            }
//        });
//
//        mRootSession.addCommand(new String[]{"ls -l /sdcard"}, 2, new Shell.OnCommandLineListener() {
//            @Override
//            public void onCommandResult(int commandCode, int exitCode) {
//                if (exitCode < 0)
//                    Log.e(TAG, "Error executing commands: exitCode " + exitCode);
//            }
//
//            @Override
//            public void onLine(String line) {
//                Log.i(TAG, line);
//            }
//        });
//    }
}

