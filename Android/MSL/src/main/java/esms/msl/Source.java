package esms.msl;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by farleylai on 7/27/15.
 */
public abstract class Source {
    public boolean open() { return true; }
    public boolean close() { return true; }
    public boolean start() { return true; }
    public boolean stop() { return true; }
    //public int read(ByteBuffer buffer, int size) { return 0; }
    public ByteBuffer read() { return null; }
    public List<ByteBuffer> acquire() { return null; }
    public boolean release() { return true; }
}
