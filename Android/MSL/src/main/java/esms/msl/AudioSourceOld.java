package esms.msl;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import java.nio.ByteBuffer;
/**
 * Created by farleylai on 7/27/15.
 */
public class AudioSourceOld extends Source {
    public static final String TAG = AudioSourceOld.class.getName();

    public enum SRC {
        MIC,
        VOICE_RECOGNITION;
        public int getValue() {
            switch(this) {
                case MIC: return MediaRecorder.AudioSource.MIC;
                default:
                    return MediaRecorder.AudioSource.VOICE_RECOGNITION;
            }
        }
    }

    public enum Rate {
        SAMPLE_RATE_8K,
        SAMPLE_RATE_16K,
        SAMPLE_RATE_44K;
        public int getValue() {
            switch(this) {
                case SAMPLE_RATE_8K: return 8000;
                case SAMPLE_RATE_44K: return 44100;
                default:
                    return 16000;
            }
        }
    }

    public enum ChannelConfig {
        MONO, STEREO;
        public int getValue() {
            switch(this) {
                case STEREO: return AudioFormat.CHANNEL_IN_STEREO;
                default: return AudioFormat.CHANNEL_IN_MONO;
            }
        }
    }

    public enum Encoding {
        PCM_16BIT,
        PCM_FLOAT;
        public int getValue() {
            switch(this) {
                case PCM_16BIT: return AudioFormat.ENCODING_PCM_16BIT;
                default: return AudioFormat.ENCODING_PCM_FLOAT;
            }
        }
    }

    private AudioRecord mRecorder;
    private SRC mSource;
    private Rate mRate;
    private ChannelConfig mChannelConfig;
    private Encoding mEncoding;
    private ByteBuffer mBuffer;

    public AudioSourceOld(SRC src, Rate rate, ChannelConfig cfg, Encoding encoding, int bufferSizeInBytes) {
        mSource = src;
        mRate = rate;
        mChannelConfig = cfg;
        mEncoding = encoding;
        mBuffer = ByteBuffer.allocateDirect(Math.max(bufferSizeInBytes, AudioRecord.getMinBufferSize(rate.getValue(), cfg.getValue(), encoding.getValue())));
    }

    @Override
    public boolean open() {
        mRecorder = new AudioRecord(mSource.getValue(), mRate.getValue(), mChannelConfig.getValue(), mEncoding.getValue(), mBuffer.capacity());
        mBuffer.clear();
        return mRecorder != null;
    }

    @Override
    public boolean close() {
        mRecorder.release();
        mRecorder = null;
        return true;
    }

    @Override
    public boolean start() {
        mRecorder.startRecording();
        return true;
    }

    @Override
    public boolean stop() {
        mRecorder.stop();
        return true;
    }

//    private int read(ByteBuffer buffer, int size) {
//        int bytes = mRecorder.read(buffer, size);
//        if(bytes < size)
//            throw new RuntimeException(String.format("Failed to read sufficient audio samples: %d < %d", bytes, size));
//        return bytes;
////        int size = limit;
////        while(size > 0) {
////            int bytes = recorder.read(buffer, size);
////            if(bytes < 0)
////                throw new RuntimeException("Failed to read audio: " + bytes);
////            size -= bytes;
////            Log.d(TAG, "read "+ bytes + " bytes of samples");
////        }
//    }

    @Override
    public ByteBuffer read() {
        int bytes = mRecorder.read(mBuffer, mBuffer.capacity());
        if(bytes < mBuffer.remaining())
            throw new RuntimeException(String.format("Failed to read sufficient audio samples: %d < %d", bytes, mBuffer.capacity()));
        return mBuffer;
    }

    @Override
    public boolean release() {
        mBuffer.clear();
        return true;
    }
}
