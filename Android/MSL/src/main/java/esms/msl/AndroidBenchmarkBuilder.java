package esms.msl;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.util.Log;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.List;

import esms.runtime.Environment;
import esms.benchmark.BenchmarkCFG;
import esms.profiling.ExternalProfiler;
import esms.types.Benchmark;
import esms.util.Formatter;
import esms.util.Shell;

/**
 * Created by farleylai on 3/13/15.
 */
public class AndroidBenchmarkBuilder {
    private static final String TAG = AndroidBenchmarkBuilder.class.getName();
    static {
        try {
            System.loadLibrary("ESMS");
            System.loadLibrary("MSL");
            Log.d(TAG, "libESMS and libMSL loaded");
        } catch (UnsatisfiedLinkError e) {
            throw new RuntimeException(e);
        }
    }

    private String mToolkit;
    private Benchmark mBenchmark;
    private boolean mCacheopt;
    private List<ExternalProfiler> mProfilers;

    private native void benchmark(int iterations);
    private native void initialization(ByteBuffer samples);
    private native void iterations(int periods, ByteBuffer samples);

    public AndroidBenchmarkBuilder(String toolkit, String benchmark, List<ExternalProfiler> profilers) {
        mToolkit = toolkit;
        if(mToolkit.equals("esms")) {
            if(benchmark.endsWith("AA")) {
                benchmark = benchmark.substring(0, benchmark.length() - "AA".length());
            } else if(benchmark.endsWith("AoC")) {
                benchmark = benchmark.substring(0, benchmark.length() - "AoC".length());
            } else if(benchmark.endsWith("IP")) {
                benchmark = benchmark.substring(0, benchmark.length() - "IP".length());
            }
        } else if(mToolkit.equals("streamit")) {
            mCacheopt =  benchmark.endsWith("CacheOpt");
            benchmark = benchmark.substring(0, benchmark.length() - "CacheOpt".length());
        }
        mBenchmark = Benchmark.valueOf(benchmark);
        mProfilers = profilers;
    }

    private String benchmark() {
        return mBenchmark.name() + (mCacheopt ? "CacheOpt" : "");
    }

    private int iterations() {
        return BenchmarkCFG.iterations(mBenchmark, Shell.OS.Android);
    }

    private void startProfilerServices() {
        for(ExternalProfiler profiler: mProfilers)
            profiler.startService();
    }

    private void startProfiling() {
        File path = Environment.file(String.format("Download/%s/%s", mToolkit, benchmark()));
        if(!path.exists() && !path.mkdirs())
            throw new RuntimeException("Failed to mkdirs " + path);
        for(ExternalProfiler profiler: mProfilers)
            profiler.startProfiling();
    }

    private void startProfiling(int scaling) {
        File path = Environment.file(String.format("Download/%s/%s/%d", mToolkit, benchmark(), scaling));
        if(!path.exists() && !path.mkdirs())
            throw new RuntimeException("Failed to mkdirs " + path);
        for(ExternalProfiler profiler: mProfilers)
            profiler.startProfiling();
    }

    private void stopProfiling() {
        for(ExternalProfiler profiler: mProfilers)
            profiler.stopProfiling();
    }

    private void stopProfilerServices() {
        for(ExternalProfiler profiler: mProfilers)
            profiler.stopService();
    }

    private void save() throws Exception {
        String benchmark = benchmark();
        for(ExternalProfiler profiler: mProfilers)
            profiler.save(Environment.file(String.format("%s-%s.log", mToolkit, benchmark)));
    }

    public AndroidBenchmarkBuilder run() throws Exception {
        Log.d(TAG, Formatter.title("%s %s", mToolkit, benchmark()));
        startProfilerServices();
        startProfiling();
        benchmark(iterations());
        stopProfiling();
        save();
        stopProfilerServices();
        return this;
    }

    /**
     * Run real audio apps.
     *
     * @param secs
     * @param rate
     * @return
     * @throws Exception
     */
    public AndroidBenchmarkBuilder run(int secs, int rate, int scaling) throws Exception {
        Log.d(TAG, Formatter.title("%s %s", mToolkit, benchmark()));
        startProfilerServices();
        startProfiling(scaling);
        if(mBenchmark.name().startsWith("BeepBeepA")) {
            if (mToolkit.equals("esms"))
                runAudioApp(secs, 271, 16, rate, scaling);
            else if (mToolkit.equals("streamit"))
                runAudioApp(secs, 255, 16 * (mCacheopt ? BenchmarkCFG.scaling(mBenchmark) : 1), rate, scaling);
        } else if(mBenchmark.name().startsWith("CrowdA")) {
            if (mToolkit.equals("esms"))
                runAudioApp(secs, 256, 128, rate, scaling);
            else if (mToolkit.equals("streamit"))
                runAudioApp(secs, 128, 128, rate, scaling);
        } else if(mBenchmark.name().startsWith("SpeakerIdentifier")) {
            if (mToolkit.equals("esms"))
                runAudioApp(secs, 256, 128, rate, scaling);
            else if (mToolkit.equals("streamit"))
                runAudioApp(secs, 128, 128, rate, scaling);
        }

        stopProfiling();
        save();
        stopProfilerServices();
        return this;
    }

    /**
     * initialization(char[init] data)
     * iteration(char[steady] data)
     *
     * @param secs benchmark duration in seconds
     * @param init number of samples required in initialization
     * @param steady number of samples required in a steady state
     * @param rate sampling rate
     * @param scaling in units of steady states from 1, 10, 100, 150, 200, 250 to 300
     * @return
     * @throws Exception
     */
    public AndroidBenchmarkBuilder runAudioApp(int secs, int init, int steady, int rate, int scaling) {
        // buffer >= max(min, init, steady)
        // buffer <= init + steady * x = initSamples
        // buffer <= scaling * steady = steadySamples
        int sampleSizeInBytes = 2;
        int min = AudioRecord.getMinBufferSize(rate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT) / sampleSizeInBytes;
        int samples = Math.max(Math.max(min, init), steady);
        int initMult = (samples - init + (steady - 1)) / steady;
        int initSamples = init + steady * initMult;
        int steadyMult = (samples + (steady - 1))/ steady * scaling;
        int steadySamples = steadyMult * steady;
        int rounds = (secs * rate - initSamples + (steadySamples - 1)) / steadySamples;
        Log.d(TAG, String.format("min: %d, init: %d, steady: %d at %d samples/s for %d secs\n", samples, init, steady, rate, secs));
        Log.d(TAG, String.format("initMult: %d, initSamples: %d, steadyMult: %d, steadySamples[%d]: %d\n", initMult, initSamples, steadyMult, scaling, steadySamples));
        Log.d(TAG, String.format("initialization steady periods: %d, steady rounds: %d, steady round periods: %d\n", initMult, rounds, scaling * steadyMult));

        Source src = new AudioSourceOld(
                AudioSourceOld.SRC.VOICE_RECOGNITION,
                AudioSourceOld.Rate.SAMPLE_RATE_16K,
                AudioSourceOld.ChannelConfig.MONO,
                AudioSourceOld.Encoding.PCM_16BIT,
                Math.max(initSamples, steadySamples) * sampleSizeInBytes);
        runMSA(src, initSamples, initMult, steadySamples, steadyMult, rounds, sampleSizeInBytes);
        return this;
    }

    public AndroidBenchmarkBuilder runMSA(Source src, int initSamples, int initMult, int steadySamples, int steadyMult, int rounds, int sampleSizeInBytes) {
        src.open();
        src.start();

        // initialization() + iterations(x)
        ByteBuffer samples = src.read();
        initialization(samples);
        iterations(initMult, samples);
        src.release();

        // rounds * iterations(scaling)
        for(int round = 0; round < rounds; round++) {
            samples = src.read();
            iterations(steadyMult, samples);
            src.release();
        }

        src.stop();
        src.close();
        return this;
    }
}
