package esms.msl;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by farleylai on 7/27/15.
 */
public class VideoSource extends Source {
    public enum Lens {
        LENS_FACING_FRONT,
        LENS_FACING_BACK;
        public int getValue() {
            switch(this) {
                case LENS_FACING_FRONT: return CameraCharacteristics.LENS_FACING_FRONT;
                default: return CameraCharacteristics.LENS_FACING_BACK;
            }
        }
    }

    public enum Format {
        JPEG,
        NV16,
        NV21,
        RAW_SENSOR,
        RGB_565,
        YUV_420_888;
        public int getValue() {
            switch(this) {
                case JPEG: return ImageFormat.JPEG;
                case NV16: return ImageFormat.NV16;
                case NV21: return ImageFormat.NV21;
                case RAW_SENSOR: return ImageFormat.RAW_SENSOR;
                case RGB_565: return ImageFormat.RGB_565;
                default: return ImageFormat.YUV_420_888;
            }
        }
    }

    private static final SizeComparator SizeComparator = new SizeComparator();
    private static final String TAG = VideoSource.class.getName();
    private static String toString(Format fmt) {
        switch(fmt) {
            case JPEG: return "JPEG";
            case NV16: return "NV16";
            case NV21: return "NV21";
            case RAW_SENSOR: return "RAW_SENSOR";
            case RGB_565: return "RGB_565";
            default: return "YUV_420_888";
        }
    }

    private Context mCtx;
    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;

    private ImageReader mImageReader;
    private CameraDevice mCameraDevice;
    private Lens mLens;
    private Format mFmt;
    private int mWidth;
    private int mHeight;
    private int mFPS;
    private long mDuration;
    private long mStall;

    private Image mImage;
    private Size mImageSize;
    private BlockingQueue<Image> mImages;
    private List<ByteBuffer> mBuffers;
    private CameraCaptureSession mCaptureSession;

    private static class SizeComparator implements Comparator<Size> {
        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() - (long) rhs.getWidth() * rhs.getHeight());
        }
    }

    private Format selectImageFormat(StreamConfigurationMap cfg) {
        List<Format> supported = new ArrayList<>();
        for(Format fmt: Format.values()) {
            if(cfg.isOutputSupportedFor(fmt.getValue())) {
                supported.add(fmt);
//                Log.d(TAG, toString(fmt) + " is supported");
            }
        }

        Format fmt = supported.contains(mFmt)
                ? mFmt
                : supported.contains(Format.YUV_420_888)
                    ? Format.YUV_420_888
                    : supported.get(0);
        Log.d(TAG, "Selected " + toString(fmt));
        return fmt;
    }

    private static Size selectImageSize(Size[] choices, int width, int height) {
        List<Size> bigEnough = new ArrayList<Size>();
        for (Size option : choices) {
//            Log.d(TAG, "Available output size: " + option);
            if (option.getHeight() == option.getWidth() * height / width &&
                    option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
            }
        }

        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            Size s = Collections.min(bigEnough, SizeComparator);
            Log.d(TAG, "Selected image size: " + s);
            return s;
        } else {
            Log.w(TAG, "Couldn't find any matched image size");
            return choices[0];
        }
    }

    public VideoSource(Context ctx, Lens lens, int width, int height, Format fmt, int fps) {
        mCtx = ctx;
        mLens = lens;
        mWidth = width;
        mHeight = height;
        mFmt = fmt;
        mFPS = fps;
        mImages = new ArrayBlockingQueue<>(2, true);
        mBuffers = new ArrayList<>();
    }

    @Override
    public boolean open() {
        mBackgroundHandler = new Handler(mCtx.getMainLooper());
        CameraManager manager = (CameraManager)mCtx.getSystemService(Context.CAMERA_SERVICE);
        String cameraId = null;
        try {
            for (String id : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(id);
                if (characteristics.get(CameraCharacteristics.LENS_FACING) != mLens.getValue())
                    continue;
                StreamConfigurationMap cfg = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                mFmt = selectImageFormat(cfg);
                mImageSize = selectImageSize(cfg.getOutputSizes(mFmt.getValue()), mWidth, mHeight);
                mDuration = cfg.getOutputMinFrameDuration(mFmt.getValue(), mImageSize);
                mStall = cfg.getOutputStallDuration(mFmt.getValue(), mImageSize);
                mImageReader = ImageReader.newInstance(mImageSize.getWidth(), mImageSize.getHeight(), mFmt.getValue(), 2);
                mImageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
                    @Override
                    public void onImageAvailable(ImageReader reader) {
                        mImages.add(reader.acquireLatestImage());
                    }
                }, mBackgroundHandler);
                cameraId = id;
                break;
            }
        } catch (CameraAccessException e) {
            throw new RuntimeException(e);
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runshows.
            throw new RuntimeException("Camera2API is not supported: " + e.getMessage());
        }

        if(cameraId == null)
            throw new RuntimeException("Found no " + mLens);

        try {
            manager.openCamera(cameraId, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(CameraDevice cameraDevice) {
                    mCameraDevice = cameraDevice;
                    Log.d(TAG, "onOpened()");
                }

                @Override
                public void onClosed(CameraDevice cameraDevice) {
                    Log.d(TAG, "onClosed()");
                }

                @Override
                public void onDisconnected(CameraDevice cameraDevice) {
                    cameraDevice.close();
                    mCameraDevice = null;
                    Log.d(TAG, "onDisconnected()");
                }

                @Override
                public void onError(CameraDevice cameraDevice, int error) {
                    cameraDevice.close();
                    mCameraDevice = null;
                    Log.d(TAG, "onError()");
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
            return false;
        }

        int i = 0;
        while(mCameraDevice == null && i++ < 5) {
            Log.d(TAG, "waiting for 100ms until camera device is opened");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(mCameraDevice == null)
            throw new RuntimeException("Unable to open camera device");
        return true;
    }

    @Override
    public boolean close() {
        try {
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mImageReader) {
                mImageReader.close();
                mImageReader = null;
            }
            if(mImage != null) {
                mImage.close();
                mImage = null;
            }
        }
        finally {
            mBackgroundHandler = null;
            for(Image img: mImages)
                img.close();
            mImages.clear();
            mBuffers.clear();
        }
        mBackgroundHandler = null;
        return true;
    }

    @Override
    public boolean start() {
        if(mCameraDevice == null)
            return false;

        try {
            mCameraDevice.createCaptureSession(Arrays.asList(mImageReader.getSurface()), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                    if (null == mCameraDevice)
                        return;

                    mCaptureSession = cameraCaptureSession;
                    try {
                        long duration = Math.max(Math.max(1000000000L / mFPS, mDuration), mStall);
                        CaptureRequest.Builder builder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                        builder.addTarget(mImageReader.getSurface());
                        builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_VIDEO);
                        builder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
                        builder.set(CaptureRequest.SENSOR_FRAME_DURATION, duration);
                        Log.d(TAG, String.format("min: %.3fms, stall: %.3fms, duration: %.3fms\n", mDuration / 1000000.0, mStall / 1000000.0, duration / 1000000.0));
                        mCaptureSession.setRepeatingRequest(builder.build(), new CameraCaptureSession.CaptureCallback() {
                            @Override
                            public void onCaptureProgressed(CameraCaptureSession session,
                                                            CaptureRequest request,
                                                            CaptureResult partialResult) {
//                                Log.d(TAG, "Partially captured frame: " + partialResult.getFrameNumber());
                            }

                            @Override
                            public void onCaptureCompleted(CameraCaptureSession session,
                                                           CaptureRequest request,
                                                           TotalCaptureResult result) {
//                                Log.d(TAG, "Completely captured frame: " + result.getFrameNumber());
                            }

                        }, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        mCameraDevice.close();
                        mCameraDevice = null;
                        throw new RuntimeException("Failed to set a repeating capture request: " + e.getMessage());
                    }
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                    throw new RuntimeException("Failed to configure a video capture session");
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            Log.e(TAG, "Failed to create a video capture session:" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean stop() {
        if(mCaptureSession != null) {
            mCaptureSession.close();
            mCaptureSession = null;
        }
        return true;
    }

    @Override
    public ByteBuffer read() {
        return acquire().get(0);
    }

    @Override
    public List<ByteBuffer> acquire() {
        if(mImage != null || !mBuffers.isEmpty())
            throw new RuntimeException("Acquired image is not released");

        while(true) {
            try {
                mImage = mImages.take();
                for(Image.Plane plane : mImage.getPlanes())
                    mBuffers.add(plane.getBuffer());
//                Log.d(TAG, "acquired image timestamped at " + mImage.getTimestamp());
                return mBuffers;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean release() {
        if(mImage != null) {
            mBuffers.clear();
            mImage.close();
            mImage = null;
        }
        return true;
    }
}
