package esms.usage;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.PowerManager;

import esms.util.Log;

/** Power and battery states and readings.
 */
public class EnergyUsage extends Usage {
    private static PowerManager PM;
    private static BatteryManager BM;
    private static Intent BatteryStatus;;
    public EnergyUsage(Context ctx) {
        super("energy");
        header("interactive", "power_save", "discharging",
                "voltage", "current_now", "current_avg",
                "capacity", "energy_counter", "charge_counter");
        if(BatteryStatus == null) {
            PM = (PowerManager)ctx.getSystemService(Context.POWER_SERVICE);
            BM = (BatteryManager)ctx.getSystemService(Context.BATTERY_SERVICE);
            BatteryStatus = ctx.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        }
    }

    @Override
    public EnergyUsage get() {
        int voltage = BatteryStatus.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
        int status = BatteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        //int level = BatteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        //int scale = BatteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        //int temp = BatteryStatus.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
        //boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;
        //boolean isDischarging = status == BatteryManager.BATTERY_STATUS_DISCHARGING;
        //Log.i(tag(), "level=%d, scale=%d, temp=%d, voltage=%d, status=%d\n", level, scale, temp, voltage, status);

        timestamp(System.currentTimeMillis());
        field(0, PM.isInteractive() ? 1 : 0);
        field(1, PM.isPowerSaveMode() ? 1 : 0);
        //field(2, BM.getIntProperty(BatteryManager.BATTERY_STATUS_DISCHARGING));
        field(2, status == BatteryManager.BATTERY_STATUS_DISCHARGING ? 1 : 0);
        field(3, voltage);                                                             // mV
        field(4, BM.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW));      // uA
        field(5, BM.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE));  // uA
        field(6, BM.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY));         // remaining percentage
        field(7, BM.getLongProperty(BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER));  // remaining nWh
        field(8, BM.getIntProperty(BatteryManager.BATTERY_PROPERTY_CHARGE_COUNTER));   // total uAh

        //int discharging = BM.getIntProperty(BatteryManager.BATTERY_STATUS_DISCHARGING); // == BATTERY_PROPERTY_CURRENT_NOW
        //int charging = BM.getIntProperty(BatteryManager.BATTERY_STATUS_CHARGING);      // == BATTERY_PROPERTY_CURRENT_AVERAGE
        //int ncharging = BM.getIntProperty(BatteryManager.BATTERY_STATUS_NOT_CHARGING); // == BATTERY_PROPERTY_CAPACITY
        //int full = BM.getIntProperty(BatteryManager.BATTERY_STATUS_FULL);              // binary boolean?
        //Log.i(tag(), "discharging=%d, charging=%d, not charging=%d, full=%d\n", discharging, charging, ncharging, full);
        return this;
    }
}
