package esms.usage;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.StringBuilderPrinter;

import esms.runtime.Resource;
import esms.util.Log;

/**
 * Created by farleylai on 6/26/16.
 */
public class WifiUsage extends Usage {
    private NetStat _stat;
    private WifiManager _wm;
    public WifiUsage(Context ctx) {
        super(NetStat.WIFI);
        _stat = new NetStat(NetStat.WIFI);
        _wm = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        header("state", "rssi", "speed");
    }

    @Override
    public WifiUsage get() {
        _stat.get();
        timestamp(_stat.timestamp());
        WifiInfo info = _wm.getConnectionInfo();
        field(0, _wm.getWifiState());     // WIFI_STATE_DISABLED, WIFI_STATE_DISABLING, WIFI_STATE_ENABLED, WIFI_STATE_ENABLING, WIFI_STATE_UNKNOWN
        field(1, info.getRssi());         // dbm
        field(2, info.getLinkSpeed());    // Mbps
        return this;
    }

    @Override
    public int count() {
        return _stat.count() + super.count();
    }

    @Override
    public String header(int i) {
        int count = _stat.count();
        return i < count ? _stat.header(i) : super.header(i - count);
    }

    @Override
    public long field(int i) {
        int count = _stat.count();
        return i < count ? _stat.field(i) : super.field(i - count);
    }
}
