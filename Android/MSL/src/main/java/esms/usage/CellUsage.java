package esms.usage;

import android.content.Context;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrength;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import static android.telephony.PhoneStateListener.LISTEN_DATA_ACTIVITY;
import static android.telephony.PhoneStateListener.LISTEN_DATA_CONNECTION_STATE;
import static android.telephony.PhoneStateListener.LISTEN_SERVICE_STATE;
import static android.telephony.PhoneStateListener.LISTEN_SIGNAL_STRENGTHS;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import esms.util.Log;

/** Usage of the current network type
 */
public class CellUsage extends Usage {
    private static Method getLteSignalStrength;
    private static Method getLteRsrp;
    private static Method getLteRsrq;
    private static Method getLteRssnr;
    private static Method getLteCqi;
    static {
        for (Method m: SignalStrength.class.getMethods()) {
            switch(m.getName()) {
                case "getLteSignalStrength": getLteSignalStrength = m; break;
                case "getLteRsrp": getLteRsrp = m; break;
                case "getLteRsrq": getLteRsrq = m; break;
                case "getLteRssnr": getLteRssnr = m; break;
                case "getLteCqi": getLteCqi = m; break;
            }
        }
    }

    private static TelephonyManager TM;
    private static boolean onDataConnectionStateChanged;
    private static boolean onDataActivityChanged;
    private static boolean onCellInfoChanged;
    private static boolean onSignalStrengthChanged;
    private static int Type;
    private static int State;
    private static int Direction;
    private static int Asu;
    private static int Dbm;
    private static int Level;
    private static int LteSignalStrength;
    private static int LteRsrp;
    private static int LteRsrq;
    private static int LteRssnr;
    private static int LteCqi;
    private static PhoneStateListener Listener = new PhoneStateListener() {
        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            // IN_SERVICE, OUT_OF_SERVICE, EMERGENCY_ONLY, POWER_OFF
        }

        @Override
        public void onDataConnectionStateChanged(int state, int type) {
            // DATA_DISCONNECTED, DATA_CONNECTING, DATA_CONNECTED, DATA_SUSPENDED
            Type = type;
            State = state;
            onDataConnectionStateChanged = true;
            Log.d(getClass().getSimpleName(), "DataConnectionStateChanged");
        }

        @Override
        public void onDataActivity(int direction) {
            // DATA_ACTIVITY_NONE, DATA_ACTIVITY_IN, DATA_ACTIVITY_OUT, DATA_ACTIVITY_INOUT, DATA_ACTIVITY_DORMANT
            Direction = direction;
            onDataActivityChanged = true;
            Log.d(getClass().getSimpleName(), "DataActivityChanged");
        }

        @Override
        public void onCellInfoChanged(List<CellInfo> cellInfo) {
            // FIXME assume only one
            for (CellInfo info: TM.getAllCellInfo()) {
                if(info.isRegistered()) {
                    CellSignalStrength ss = null;
                    if (info instanceof CellInfoLte) {
                        ss = ((CellInfoLte) info).getCellSignalStrength();
                        String[] entries = ss.toString().split("\\s+");
                        for(String entry: entries) {
                            String[] field = entry.split("=");
                            if(field.length == 2) {
                                switch (field[0]) {
                                    case "ss": LteSignalStrength = Integer.parseInt(field[1]); break;
                                    case "rsrp": LteRsrp = Integer.parseInt(field[1]); break;
                                    case "rsrq": LteRsrq = Integer.parseInt(field[1]); break;
                                    case "rssnr": LteRssnr = Integer.parseInt(field[1]); break;
                                    case "cqi": LteCqi = Integer.parseInt(field[1]); break;
                                }
                            }
                        }
                    } else if (info instanceof CellInfoGsm) {
                        ss = ((CellInfoGsm) info).getCellSignalStrength();
                    } else if (info instanceof CellInfoCdma) {
                        ss = ((CellInfoCdma) info).getCellSignalStrength();
                    } else if (info instanceof CellInfoWcdma) {
                        ss = ((CellInfoWcdma) info).getCellSignalStrength();
                    } else continue;
                    Asu = ss.getAsuLevel();
                    Dbm = ss.getDbm();
                    Level = ss.getLevel();
                    onCellInfoChanged = true;
                    Log.d(getClass().getSimpleName(), "onCellInfoChanged");
                    break;
                }
            }
        }

        @Override
        public void onSignalStrengthsChanged(SignalStrength ss) {
            try {
                if(getLteSignalStrength != null) LteSignalStrength = (int)getLteSignalStrength.invoke(ss);
                if(getLteRsrp != null) LteRsrp = (int)getLteRsrp.invoke(ss);
                if(getLteRsrq != null) LteRsrq = (int)getLteRsrq.invoke(ss);
                if(getLteRssnr != null) LteRssnr = (int)getLteRssnr.invoke(ss);
                if(getLteCqi != null) LteCqi = (int)getLteCqi.invoke(ss);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            onSignalStrengthChanged = true;
            Log.i(getClass().getSimpleName(), "onSignalStrengthChanged");
        }
    };

    private NetStat _stat;
    public CellUsage(Context ctx) {
        super(NetStat.CELL);
        _stat = new NetStat(NetStat.CELL);
        if(TM == null) {
            TM = TM == null ? (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE) : TM;
            TM.listen(Listener, LISTEN_SERVICE_STATE | LISTEN_DATA_ACTIVITY | LISTEN_DATA_CONNECTION_STATE | LISTEN_SIGNAL_STRENGTHS);
        }
        header("type", "state", "asu", "dbm", "level", "signal", "rsrp", "rsrq", "rssnr", "cqi");
    }

    @Override
    public CellUsage get() {
        if(TM.getNetworkType() == 0 || TM.getAllCellInfo() == null) return this;
        _stat.get();
        timestamp(_stat.timestamp());
        if(onDataConnectionStateChanged) {
            field(0, Type);
            field(1, State);
        } else {
            field(0, TM.getNetworkType());
            field(1, TM.getDataState());
        }
        if(onCellInfoChanged) {
            field(2, Asu);   // asu value from dbm between 0..31, 99 is unknown
            field(3, Dbm);   // rsrp
            field(4, Level); // 0..4 by rsrp
            field(5, LteSignalStrength);
            field(6, LteRsrp);
            field(7, LteRsrq);
            field(8, LteRssnr);
            field(9, LteCqi);
        } else {
            // FIXME assume only one
            for (CellInfo info: TM.getAllCellInfo()) {
                if(info.isRegistered()) {
                    CellSignalStrength ss = null;
                    if (info instanceof CellInfoLte) {
                        ss = ((CellInfoLte) info).getCellSignalStrength();
                        if(onSignalStrengthChanged) {
                            field(5, LteSignalStrength);
                            field(6, LteRsrp);
                            field(7, LteRsrq);
                            field(8, LteRssnr);
                            field(9, LteCqi);
                        } else {
                            String[] entries = ss.toString().split("\\s+");
                            for(String entry: entries) {
                                Log.d(tag(), entry);
                                String[] field = entry.split("=");
                                if(field.length == 2) {
                                    switch (field[0]) {
                                        case "ss": field(5, Long.parseLong(field[1])); break;
                                        case "rsrp": field(6, Long.parseLong(field[1])); break;
                                        case "rsrq": field(7, Long.parseLong(field[1])); break;
                                        case "rssnr": field(8, Long.parseLong(field[1])); break;
                                        case "cqi": field(9, Long.parseLong(field[1])); break;
                                    }
                                }
                            }
                        }
                    } else if (info instanceof CellInfoGsm) {
                        ss = ((CellInfoGsm) info).getCellSignalStrength();
                    } else if (info instanceof CellInfoCdma) {
                        ss = ((CellInfoCdma) info).getCellSignalStrength();
                    } else if (info instanceof CellInfoWcdma) {
                        ss = ((CellInfoWcdma) info).getCellSignalStrength();
                    } else continue;
                    field(2, ss.getAsuLevel()); // asu value between 0..31, 99 is unknown
                    field(3, ss.getDbm());
                    field(4, ss.getLevel());    // 0..4
                    break;
                }
            }
        }
        return this;
    }

    @Override
    public int count() {
        return _stat.count() + super.count();
    }

    @Override
    public String header(int i) {
        int count = _stat.count();
        return i < count ? _stat.header(i) : super.header(i - count);
    }

    @Override
    public long field(int i) {
        int count = _stat.count();
        return i < count ? _stat.field(i) : super.field(i - count);
    }
}
