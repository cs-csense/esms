package esms.usage;

import android.app.usage.NetworkStatsManager;

import java.nio.ByteBuffer;

/**
 * Created by farleylai on 12/13/15.
 */
public class NetUsageOld extends Usage {
    public enum NetStat {
//        MobileRxBytes, MobileTxBytes, MobileRxPackets, MobileTxPackets,
//        WiFiRxBytees, WiFiTxBytes, WiFiRxPackets, WiFiTxPackets,
        MobileUserRxBytes, MobileUserTxBytes, MobileUserRxPackets, MobileUserTxPackets,
        WiFiUserRxBytes, WiFiUserTxBytes, WiFiUserRxPackets, WiFiUserTxPackets,
        MobileDeviceRxBytes, MobileDeviceTxBytes, MobileDeviceRxPackets, MobileDeviceTxPackets,
        WiFiDeviceRxBytes, WiFiDeviceTxBytes, WiFiDeviceRxPackets, WiFiDeviceTxPackets,
    }

    ByteBuffer stats;

//    // Thread-specific socket traffic
//    int tag;
//    long mobileRxBytes;
//    long mobileTxBytes;
//    long mobileRxPackets;
//    long mobileTxPackets;
//
//    long wifiRxBytes;
//    long wifiTxBytes;
//    long wifiRxPackets;
//    long wifiTxPackets;

    // System-wide network stats from NetworkStatsManager.querySummaryForUser()
    private NetworkStatsManager net;
    private long last = Long.MIN_VALUE;
    private boolean mobileUsedSinceLastTime;
    private boolean wiFiUsedSinceLastTime;

    private NetUsageOld() {
        super("net");
        stats = ByteBuffer.allocate(NetStat.values().length * Long.SIZE / Byte.SIZE);
    }

    public NetUsageOld(NetworkStatsManager net) {
        this();
        this.net = net;
//        long tid = Thread.currentThread().getId();
//        TrafficStats.setThreadStatsTag(tag = (int)tid);
    }

//    public NetUsageOld(NetworkStatsManager net, int tag) {
//        this();
//        TrafficStats.setThreadStatsTag(this.tag = tag);
//    }
//
//    public void register(Socket socket) throws SocketException {
//        TrafficStats.tagSocket(socket);
//    }
//
//    public void unregister(Socket socket) throws SocketException {
//        long tid = Thread.currentThread().getId();
//        TrafficStats.setThreadStatsTag((int) tid);
//        TrafficStats.tagSocket(socket);
//    }

    public long get(NetStat stat) {
        return stats.getLong(stat.ordinal() * Long.SIZE / Byte.SIZE);
    }

    public void set(NetStat stat, long value) {
        stats.putLong(stat.ordinal() * Long.SIZE / Byte.SIZE, value);
    }

    public boolean isMobileUsedSinceLastTime() {
        return mobileUsedSinceLastTime;
    }

    public boolean isWiFiUsedSinceLastTime() {
        return wiFiUsedSinceLastTime;
    }

    private static String TAG = NetUsageOld.class.getSimpleName();
    @Override
    public NetUsageOld get() {
//        long now = System.currentTimeMillis();
//        try {
//            stats.clear();
//            NetworkStats.Bucket mobile = net.querySummaryForUser(ConnectivityManager.TYPE_MOBILE, null, last, now);
//            NetworkStats.Bucket wifi = net.querySummaryForUser(ConnectivityManager.TYPE_WIFI, null, last, now);
//
//            Log.i(TAG, "WiFi user bucket start: %dms, end: %dms:\n", last, now);
//            Log.i(TAG, "WiFi user RX: %d bytes\n", wifi.getRxBytes());
//            Log.i(TAG, "WiFi user TX: %d bytes\n", wifi.getTxBytes());
//
//            set(NetStat.MobileUserRxBytes, get(NetStat.MobileUserRxBytes) + mobile.getRxBytes());
//            set(NetStat.MobileUserTxBytes, get(NetStat.MobileUserTxBytes) + mobile.getTxBytes());
//            set(NetStat.MobileUserRxPackets, get(NetStat.MobileUserRxPackets) + mobile.getRxPackets());
//            set(NetStat.MobileUserTxPackets, get(NetStat.MobileUserTxPackets) + mobile.getTxPackets());
//
//            set(NetStat.WiFiUserRxBytes, get(NetStat.WiFiUserRxBytes) + wifi.getRxBytes());
//            set(NetStat.WiFiUserTxBytes, get(NetStat.WiFiUserTxBytes) + wifi.getTxBytes());
//            set(NetStat.WiFiUserRxPackets, get(NetStat.WiFiUserRxPackets) + wifi.getRxPackets());
//            set(NetStat.WiFiUserTxPackets, get(NetStat.WiFiUserTxPackets) + wifi.getTxPackets());
//
//            long bytes;
//            mobileUsedSinceLastTime = wiFiUsedSinceLastTime = false;
//            mobile = net.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE, null, last, now);
//            wifi = net.querySummaryForDevice(ConnectivityManager.TYPE_WIFI, null, last, now);
//
//            Log.i(TAG, "WiFi device bucket start: %dms, end: %dms:\n", last, now);
//            Log.i(TAG, "WiFi device RX: %d bytes\n", wifi.getRxBytes());
//            Log.i(TAG, "WiFi device TX: %d bytes\n", wifi.getTxBytes());
//
//            bytes = mobile.getRxBytes(); mobileUsedSinceLastTime |= bytes > 0; set(NetStat.MobileDeviceRxBytes, get(NetStat.MobileDeviceRxBytes) + bytes);
//            bytes = mobile.getTxBytes(); mobileUsedSinceLastTime |= bytes > 0; set(NetStat.MobileDeviceTxBytes, get(NetStat.MobileDeviceTxBytes) + bytes);
//            set(NetStat.MobileDeviceRxPackets, get(NetStat.MobileDeviceRxPackets) + mobile.getRxPackets());
//            set(NetStat.MobileDeviceTxPackets, get(NetStat.MobileDeviceTxPackets) + mobile.getTxPackets());
//
//            bytes = wifi.getRxBytes(); wiFiUsedSinceLastTime |= bytes > 0; set(NetStat.WiFiDeviceRxBytes, get(NetStat.WiFiDeviceRxBytes) + bytes);
//            bytes = wifi.getTxBytes(); wiFiUsedSinceLastTime |= bytes > 0; set(NetStat.WiFiDeviceTxBytes, get(NetStat.WiFiDeviceTxBytes) + bytes);
//            set(NetStat.WiFiUserRxPackets, get(NetStat.WiFiUserRxPackets) + wifi.getRxPackets());
//            set(NetStat.WiFiUserTxPackets, get(NetStat.WiFiUserTxPackets) + wifi.getTxPackets());
//            last = now;
//        } catch (RemoteException e) {
//            throw new RuntimeException(e);
//        }
        return null;
    }

    @Override
    public Usage diff(Usage usage) {
        if(!(usage instanceof NetUsageOld))
            throw new IllegalArgumentException("usage is not instance of NetStats");
        NetUsageOld nu = (NetUsageOld)usage;
        NetUsageOld stats = new NetUsageOld();
        stats.set(NetStat.MobileUserRxBytes, get(NetStat.MobileUserRxBytes) - nu.get(NetStat.MobileUserRxBytes));
        stats.set(NetStat.MobileUserTxBytes, get(NetStat.MobileUserTxBytes) - nu.get(NetStat.MobileUserTxBytes));
        stats.set(NetStat.WiFiUserRxPackets, get(NetStat.WiFiUserRxPackets) - nu.get(NetStat.WiFiUserRxPackets));
        stats.set(NetStat.WiFiUserTxPackets, get(NetStat.WiFiUserTxPackets) - nu.get(NetStat.WiFiUserTxPackets));

        stats.set(NetStat.MobileDeviceRxBytes, get(NetStat.MobileDeviceRxBytes) - nu.get(NetStat.MobileDeviceRxBytes));
        stats.set(NetStat.MobileDeviceTxBytes, get(NetStat.MobileDeviceTxBytes) - nu.get(NetStat.MobileDeviceTxBytes));
        stats.set(NetStat.WiFiDeviceRxPackets, get(NetStat.WiFiDeviceRxPackets) - nu.get(NetStat.WiFiDeviceRxPackets));
        stats.set(NetStat.WiFiDeviceTxPackets, get(NetStat.WiFiDeviceTxPackets) - nu.get(NetStat.WiFiDeviceTxPackets));
        return stats;
    }

    @Override
    public String header() {
        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (NetStat stat : NetStat.values())
            builder.append(stat).append(": ").append(get(stat)).append("\n");
        return builder.toString();
    }
}
