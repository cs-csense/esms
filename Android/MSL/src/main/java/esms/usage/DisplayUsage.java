package esms.usage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.view.Display;
import android.view.WindowManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import esms.util.Log;

/**
 * Created by farleylai on 6/26/16.
 */
public class DisplayUsage extends Usage {
    private Display _display;
    public DisplayUsage(Context ctx) {
        super("display");
        header("brightness", "state", "rotation");
        DisplayManager dm = (DisplayManager) ctx.getSystemService(Context.DISPLAY_SERVICE);
        _display = dm.getDisplay(Display.DEFAULT_DISPLAY);
    }

    @Override
    public DisplayUsage get() {
        timestamp(System.currentTimeMillis());
        try {
            RandomAccessFile stat = new RandomAccessFile("/sys/class/leds/lcd-backlight/brightness", "r");
            field(0, Long.parseLong(stat.readLine()));
            stat.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        field(1, _display.getState());      // STATE_UNKNOWN, STATE_OFF, STATE_ON, STATE_DOZE, STATE_DOZE_SUSPEND
        field(2, _display.getRotation());   // ROTATION_0, ROTATION_90, ROTATION_180, ROTATION_270
        return this;
    }
}
