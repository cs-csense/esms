package esms.usage;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.BatteryManager;
import android.os.PowerManager;

import esms.profiling.Profiler;
import esms.profiling.SystemProfiler;
import esms.runtime.Environment;

/** System events receiver to notify application listeners.
 */
public class SystemMonitor extends Profiler {
    public enum Event {
        CPU, Block, Display, Wifi, Cell, Energy, ALL
    }

    private static SystemMonitor Instance;
    public static synchronized SystemMonitor instance(Context ctx) {
        if(Instance == null)
            Instance = new SystemMonitor(ctx);
        return Instance;
    }

    private BroadcastReceiver _receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case Intent.ACTION_SCREEN_ON:
                case Intent.ACTION_SCREEN_OFF:
                    if(_events.containsKey(Event.Display)) _profiler.profileDisplay();
                    break;
                case ConnectivityManager.CONNECTIVITY_ACTION:
                    if(_events.containsKey(Event.Wifi)) _profiler.profileWifi();
                    if(_events.containsKey(Event.Cell)) _profiler.profileCell();
                    break;
                case PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED:
                case PowerManager.ACTION_POWER_SAVE_MODE_CHANGED:
                case BatteryManager.ACTION_CHARGING:
                case BatteryManager.ACTION_DISCHARGING:
                case Intent.ACTION_POWER_CONNECTED:
                case Intent.ACTION_POWER_DISCONNECTED:
                case Intent.ACTION_BATTERY_LOW:
                case Intent.ACTION_BATTERY_OKAY:
                    if(_events.containsKey(Event.Energy)) _profiler.profileEnergy();
                    break;
                default:
                    throw new IllegalArgumentException(intent.getAction());
            }

        }
    };

    private boolean _monitoring;
    private Context _ctx;
    private IntentFilter _filter;
    private SystemProfiler _profiler;
    private ScheduledExecutorService _scheduler;
    private Map<Event, ScheduledFuture<?>> _events;
    private SystemMonitor(Context ctx) {
        this(ctx, 2);
    }
    private SystemMonitor(Context ctx, int history) {
        _filter = new IntentFilter();
        _filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        //_filter.addAction(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED);
        _filter.addAction(PowerManager.ACTION_POWER_SAVE_MODE_CHANGED);
        _filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        _filter.addAction(Intent.ACTION_POWER_CONNECTED);
        _filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        _filter.addAction(Intent.ACTION_POWER_USAGE_SUMMARY);
        _filter.addAction(Intent.ACTION_BATTERY_LOW);
        _filter.addAction(Intent.ACTION_BATTERY_OKAY);
        _filter.addAction(Intent.ACTION_SCREEN_ON);
        _filter.addAction(Intent.ACTION_SCREEN_OFF);
        _scheduler = Executors.newSingleThreadScheduledExecutor();
        _profiler = new SystemProfiler(_ctx = ctx, history);
        _events = new HashMap<>();
    }

    public int history() {
        return _profiler.history();
    }

    public synchronized int history(int hist) {
        return _profiler.history(hist);
    }

    public void monitor(final Event e, long ms) {
        if(e == Event.ALL) {
            for(Event event: Event.values()) {
                if(event != Event.ALL)
                    monitor(event, ms);
            }
        }
        if(_events.containsKey(e)) {
            ScheduledFuture<?> future = _events.get(e);
            future.cancel(false);
        }
        ScheduledFuture<?> future = _scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if(!_monitoring) return;
                switch(e) {
                    case CPU: _profiler.profileCPU(); break;
                    case Block: _profiler.profileBlock(); break;
                    case Display: _profiler.profileDisplay(); break;
                    case Wifi: _profiler.profileWifi(); break;
                    case Cell: _profiler.profileCell(); break;
                    case Energy: _profiler.profileEnergy(); break;
                }
            }
        }, 500, ms, TimeUnit.MILLISECONDS);
        _events.put(e, future);
    }

    public synchronized void start() {
        if(!_monitoring) {
            _ctx.registerReceiver(_receiver, _filter);
            _monitoring = true;
        }
    }

    public synchronized void stop() {
        if(_monitoring) {
            _ctx.unregisterReceiver(_receiver);
            for (ScheduledFuture future : _events.values())
                future.cancel(false);
            _events.clear();
            _monitoring = false;
        }
    }

    @Override
    public SystemMonitor save(File file) {
        _profiler.save(file);
        return this;
    }

    @Override
    public String toString() {
        return _profiler.toString();
    }
}
