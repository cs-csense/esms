#ifndef __FUSION_H
#define __FUSION_H

#define max(A,B) (((A)>(B))?(A):(B))
#define pow2ceil(A) ((A<=2)?(2): (((A<=4)?(4): (((A<=8)?(8): (((A<=16)?(16): (((A<=32)?(32): (((A<=64)?(64): (((A<=128)?(128): (((A<=256)?(256):(((A<=1024)?(1024):(((A<=4096)?(4096):(((A<=16384)?(16384):(((A<=65536)?(65536):(((A<=131072)?(131072):(((A<=262144)?(262144):(((A<=524288)?(524288):(((A<=1048576)?(1048576):(((A<=2097152)?(2097152):(((A<=4194304)?(4194304):(((A<=8388608)?(8388608):(((A<=16777216)?(16777216):(((A<=33554432)?(33554432):(((A<=67108864)?(67108864):(((A<=134217728)?(134217728):(((A<=268435456)?(268435456):(((A<=536870912)?(536870912):(1073741824))))))))))))))))))))))))))))))))))))))))))))))))))
#define __BUF_SIZE_MASK_0_1 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_1_2 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_2_3 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_3_4 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_4_5 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_4_275 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_5_6 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_6_7 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_7_8 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_8_9 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_8_272 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_9_10 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_10_11 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_11_12 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_12_13 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_13_14 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_14_15 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_14_258 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_15_16 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_15_249 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_15_252 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_15_255 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_16_17 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_17_18 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_18_19 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_19_20 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_20_21 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_21_22 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_21_223 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_22_23 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_22_202 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_22_205 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_22_208 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_22_211 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_22_214 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_22_217 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_22_220 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_23_24 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_24_25 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_25_26 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_26_27 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_27_28 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_28_29 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_28_152 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_29_30 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_107 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_110 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_113 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_116 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_119 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_122 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_125 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_128 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_131 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_134 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_137 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_140 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_143 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_146 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_149 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_30_31 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_31_32 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_32_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_33_34 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_34_35 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_35_36 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_35_73 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_36_37 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_42 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_43 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_44 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_45 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_47 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_48 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_49 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_50 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_51 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_52 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_53 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_54 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_55 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_57 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_58 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_59 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_60 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_61 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_62 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_63 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_64 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_65 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_66 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_67 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_68 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_69 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_70 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_71 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_36_72 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_37_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_38_39 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_39_40 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_40_41 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_42_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_43_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_45_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_46_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_47_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_48_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_49_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_50_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_51_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_52_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_53_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_55_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_56_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_57_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_58_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_59_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_60_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_61_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_62_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_63_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_64_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_65_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_66_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_67_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_68_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_69_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_70_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_71_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_72_38 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_74 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_76 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_77 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_78 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_79 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_80 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_81 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_82 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_83 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_84 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_85 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_86 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_87 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_88 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_89 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_90 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_91 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_92 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_93 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_94 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_95 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_96 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_97 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_98 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_99 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_100 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_101 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_102 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_103 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_104 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_105 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_106 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_74_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_75_39 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_76_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_77_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_78_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_79_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_80_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_81_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_82_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_83_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_84_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_85_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_86_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_87_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_88_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_89_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_90_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_91_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_92_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_93_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_94_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_95_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_96_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_97_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_98_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_99_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_100_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_101_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_102_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_103_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_104_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_105_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_106_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_107_108 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_108_109 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_109_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_110_111 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_111_112 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_112_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_113_114 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_114_115 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_115_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_116_117 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_117_118 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_118_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_119_120 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_120_121 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_121_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_122_123 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_123_124 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_124_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_125_126 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_126_127 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_127_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_128_129 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_129_130 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_130_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_131_132 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_132_133 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_133_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_134_135 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_135_136 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_136_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_137_138 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_138_139 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_139_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_140_141 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_141_142 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_142_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_143_144 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_144_145 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_145_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_146_147 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_147_148 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_148_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_149_150 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_150_151 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_151_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_153 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_157 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_160 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_163 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_166 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_169 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_172 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_175 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_178 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_181 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_184 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_187 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_190 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_193 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_196 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_152_199 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_153_154 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_154_155 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_155_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_156_34 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_157_158 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_158_159 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_159_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_160_161 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_161_162 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_162_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_163_164 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_164_165 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_165_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_166_167 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_167_168 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_168_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_169_170 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_170_171 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_171_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_172_173 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_173_174 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_174_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_175_176 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_176_177 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_177_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_178_179 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_179_180 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_180_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_181_182 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_182_183 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_183_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_184_185 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_185_186 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_186_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_187_188 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_188_189 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_189_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_190_191 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_191_192 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_192_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_193_194 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_194_195 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_195_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_196_197 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_197_198 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_198_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_199_200 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_200_201 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_201_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_202_203 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_203_204 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_204_26 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_205_206 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_206_207 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_207_26 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_208_209 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_209_210 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_210_26 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_211_212 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_212_213 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_213_26 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_214_215 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_215_216 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_216_26 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_217_218 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_218_219 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_219_26 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_220_221 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_221_222 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_222_26 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_223_224 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_223_228 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_223_231 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_223_234 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_223_237 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_223_240 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_223_243 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_223_246 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_224_225 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_225_226 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_226_227 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_227_27 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_228_229 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_229_230 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_230_227 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_231_232 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_232_233 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_233_227 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_234_235 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_235_236 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_236_227 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_237_238 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_238_239 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_239_227 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_240_241 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_241_242 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_242_227 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_243_244 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_244_245 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_245_227 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_246_247 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_247_248 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_248_227 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_249_250 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_250_251 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_251_19 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_252_253 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_253_254 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_254_19 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_255_256 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_256_257 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_257_19 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_258_259 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_258_263 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_258_266 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_258_269 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_259_260 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_260_261 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_261_262 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_262_20 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_263_264 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_264_265 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_265_262 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_266_267 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_267_268 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_268_262 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_269_270 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_270_271 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_271_262 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_272_273 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_273_274 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_274_12 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_275_276 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_276_277 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_277_278 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_278_279 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_278_283 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_279_280 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_280_281 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_281_282 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_282_13 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_283_284 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_284_285 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_285_282 (pow2ceil(64+0)-1)

#endif
