#include <jni.h>
#include <stdio.h>
#include "log.h"

extern int main(int argc, const char **argv);
extern "C" JNIEXPORT void JNICALL Java_esms_msl_AndroidBenchmarkBuilder_benchmark(JNIEnv* env, jobject thiz, jint iterations) {
    int argc = 3;
    char ITERATIONS[16];
    snprintf(ITERATIONS, sizeof(ITERATIONS), "%d", iterations);
    const char* argv[3] = {"Benchmark", "-i", ITERATIONS};
    LOGD(TAG, "iterations: %d\n", iterations);
    main(3, argv);
}