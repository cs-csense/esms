#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <netsocket.h>
#include <node_server.h>
#include <init_instance.h>
#include <master_server.h>
#include <save_state.h>
#include <save_manager.h>
#include <delete_chkpts.h>
#include <object_write_buffer.h>
#include <read_setup.h>
#include <ccp.h>
#include <streamit_random.h>
#include "structs.h"
#include "fusion.h"

int __max_iteration;
int __timer_enabled = 0;
int __frequency_of_chkpts;
volatile int __vol;


int BUFFER_0_1[__BUF_SIZE_MASK_0_1 + 1];
int HEAD_0_1 = 0;
int TAIL_0_1 = 0;
int BUFFER_1_2[__BUF_SIZE_MASK_1_2 + 1];
int HEAD_1_2 = 0;
int TAIL_1_2 = 0;
int BUFFER_1_5[__BUF_SIZE_MASK_1_5 + 1];
int HEAD_1_5 = 0;
int TAIL_1_5 = 0;
int BUFFER_2_3[__BUF_SIZE_MASK_2_3 + 1];
int HEAD_2_3 = 0;
int TAIL_2_3 = 0;
int BUFFER_3_4[__BUF_SIZE_MASK_3_4 + 1];
int HEAD_3_4 = 0;
int TAIL_3_4 = 0;
int BUFFER_5_3[__BUF_SIZE_MASK_5_3 + 1];
int HEAD_5_3 = 0;
int TAIL_5_3 = 0;
void init_source__3_10__0();
void work_source__3_10__0(int);
void __splitter_1_work(int);
void init_repeater__9_12__2();
void work_repeater__9_12__2(int);
void __joiner_3_work(int);
void init_printer__19_14__4();
void work_printer__19_14__4(int);
void init_Identity__13_13__5();
void work_Identity__13_13__5(int);

int main(int argc, char **argv) {
  read_setup::read_setup_file();
  __max_iteration = read_setup::max_iteration;
  for (int a = 1; a < argc; a++) {
    if (argc > a + 1 && strcmp(argv[a], "-i") == 0) {
      int tmp;
      sscanf(argv[a + 1], "%d", &tmp);
#ifdef VERBOSE
#endif
      __max_iteration = tmp;
    }
    if (strcmp(argv[a], "-t") == 0) {
#ifdef VERBOSE
#endif
      __timer_enabled = 1;
    }
  }
// number of phases: 5


  // ============= Initialization =============

init_source__3_10__0();
init_repeater__9_12__2();
init_Identity__13_13__5();
init_printer__19_14__4();

  // ============= Steady State =============

  if (__timer_enabled) {
  }
  for (int n = 0; n < (__max_iteration  ); n++) {
HEAD_0_1 = 0;
TAIL_0_1 = 0;
    work_source__3_10__0(1 );
HEAD_1_2 = 0;
TAIL_1_2 = 0;
HEAD_1_5 = 0;
TAIL_1_5 = 0;
    __splitter_1_work(128 );
HEAD_2_3 = 0;
TAIL_2_3 = 0;
    work_repeater__9_12__2(128 );
HEAD_5_3 = 0;
TAIL_5_3 = 0;
    work_Identity__13_13__5(128 );
HEAD_3_4 = 0;
TAIL_3_4 = 0;
    __joiner_3_work(128 );
    work_printer__19_14__4(128 );
  }
if (__timer_enabled) {
  }


  return 0;
}

// moved or inserted by concat_cluster_threads.pl
#include <message.h>
message *__msg_stack_2;
message *__msg_stack_0;
message *__msg_stack_1;
message *__msg_stack_3;
message *__msg_stack_5;
message *__msg_stack_4;

// end of moved or inserted by concat_cluster_threads.pl

// peek: 0 pop: 0 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false


#include <mysocket.h>
#include <sdep.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "global.h"

int __number_of_iterations_0;
int __counter_0 = 0;
int __steady_0 = 0;
int __tmp_0 = 0;
int __tmp2_0 = 0;
int *__state_flag_0 = NULL;
thread_info *__thread_0 = NULL;



void save_file_pointer__0(object_write_buffer *buf);
void load_file_pointer__0(object_write_buffer *buf);

inline void check_status__0() {
  check_thread_status(__state_flag_0, __thread_0);
}

void check_status_during_io__0() {
  check_thread_status_during_io(__state_flag_0, __thread_0);
}

void __init_thread_info_0(thread_info *info) {
  __state_flag_0 = info->get_state_flag();
}

thread_info *__get_thread_info_0() {
  if (__thread_0 != NULL) return __thread_0;
  __thread_0 = new thread_info(0, check_status_during_io__0);
  __init_thread_info_0(__thread_0);
  return __thread_0;
}

void __declare_sockets_0() {
  init_instance::add_outgoing(0,1, DATA_SOCKET);
}

void __init_sockets_0(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_0() {
}

void __peek_sockets_0() {
}

 
void init_source__3_10__0();
inline void check_status__0();

void work_source__3_10__0(int);



inline void __push_0_1(int data) {
BUFFER_0_1[HEAD_0_1]=data;
HEAD_0_1++;
}



 
void init_source__3_10__0(){
}
void save_file_pointer__0(object_write_buffer *buf) {}
void load_file_pointer__0(object_write_buffer *buf) {}
 
void work_source__3_10__0(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__2 = 0;/* int */

      // mark begin: SIRFilter source

      for ((i__2 = 0)/*int*/; (i__2 < 256); (i__2++)) {__push_0_1(i__2);
      }
      // mark end: SIRFilter source

    }
  }
}

// init counts: 0 steady counts: 128

// ClusterFusion isEliminated: false



int __number_of_iterations_1;
int __counter_1 = 0;
int __steady_1 = 0;
int __tmp_1 = 0;
int __tmp2_1 = 0;
int *__state_flag_1 = NULL;
thread_info *__thread_1 = NULL;



inline void check_status__1() {
  check_thread_status(__state_flag_1, __thread_1);
}

void check_status_during_io__1() {
  check_thread_status_during_io(__state_flag_1, __thread_1);
}

void __init_thread_info_1(thread_info *info) {
  __state_flag_1 = info->get_state_flag();
}

thread_info *__get_thread_info_1() {
  if (__thread_1 != NULL) return __thread_1;
  __thread_1 = new thread_info(1, check_status_during_io__1);
  __init_thread_info_1(__thread_1);
  return __thread_1;
}

void __declare_sockets_1() {
  init_instance::add_incoming(0,1, DATA_SOCKET);
  init_instance::add_outgoing(1,2, DATA_SOCKET);
  init_instance::add_outgoing(1,5, DATA_SOCKET);
}

void __init_sockets_1(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_1() {
}

void __peek_sockets_1() {
}

inline int __pop_0_1() {
int res=BUFFER_0_1[TAIL_0_1];
TAIL_0_1++;
return res;
}


inline void __push_1_2(int data) {
BUFFER_1_2[HEAD_1_2]=data;
HEAD_1_2++;
}



inline void __push_1_5(int data) {
BUFFER_1_5[HEAD_1_5]=data;
HEAD_1_5++;
}



void __splitter_1_work(int ____n) {
  for (;____n > 0; ____n--) {
__push_1_2(__pop_0_1());
__push_1_5(__pop_0_1());
  }
}


// peek: 1 pop: 1 push 16
// init counts: 0 steady counts: 128

// ClusterFusion isEliminated: false



int __number_of_iterations_2;
int __counter_2 = 0;
int __steady_2 = 0;
int __tmp_2 = 0;
int __tmp2_2 = 0;
int *__state_flag_2 = NULL;
thread_info *__thread_2 = NULL;



void save_peek_buffer__2(object_write_buffer *buf);
void load_peek_buffer__2(object_write_buffer *buf);
void save_file_pointer__2(object_write_buffer *buf);
void load_file_pointer__2(object_write_buffer *buf);

inline void check_status__2() {
  check_thread_status(__state_flag_2, __thread_2);
}

void check_status_during_io__2() {
  check_thread_status_during_io(__state_flag_2, __thread_2);
}

void __init_thread_info_2(thread_info *info) {
  __state_flag_2 = info->get_state_flag();
}

thread_info *__get_thread_info_2() {
  if (__thread_2 != NULL) return __thread_2;
  __thread_2 = new thread_info(2, check_status_during_io__2);
  __init_thread_info_2(__thread_2);
  return __thread_2;
}

void __declare_sockets_2() {
  init_instance::add_incoming(1,2, DATA_SOCKET);
  init_instance::add_outgoing(2,3, DATA_SOCKET);
}

void __init_sockets_2(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_2() {
}

void __peek_sockets_2() {
}

 
void init_repeater__9_12__2();
inline void check_status__2();

void work_repeater__9_12__2(int);


inline int __pop_1_2() {
int res=BUFFER_1_2[TAIL_1_2];
TAIL_1_2++;
return res;
}

inline void __pop_1_2(int n) {
TAIL_1_2+=n;
}

inline int __peek_1_2(int offs) {
return BUFFER_1_2[TAIL_1_2+offs];
}



inline void __push_2_3(int data) {
BUFFER_2_3[HEAD_2_3]=data;
HEAD_2_3++;
}



 
void init_repeater__9_12__2(){
}
void save_file_pointer__2(object_write_buffer *buf) {}
void load_file_pointer__2(object_write_buffer *buf) {}
 
void work_repeater__9_12__2(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int __tmp4__6 = 0;/* int */
      int i__7 = 0;/* int */
      int __tmp5__8 = 0;/* int */

      // mark begin: SIRFilter repeater

      for ((i__7 = 0)/*int*/; (i__7 < 15); (i__7++)) {{
          __tmp4__6 = BUFFER_1_2[TAIL_1_2+0];
;
          __push_2_3(__tmp4__6);
        }
      }
      __tmp5__8 = BUFFER_1_2[TAIL_1_2]; TAIL_1_2++;
;
      __push_2_3(__tmp5__8);
      // mark end: SIRFilter repeater

    }
  }
}

// init counts: 0 steady counts: 128

// ClusterFusion isEliminated: false



int __number_of_iterations_3;
int __counter_3 = 0;
int __steady_3 = 0;
int __tmp_3 = 0;
int __tmp2_3 = 0;
int *__state_flag_3 = NULL;
thread_info *__thread_3 = NULL;



inline void check_status__3() {
  check_thread_status(__state_flag_3, __thread_3);
}

void check_status_during_io__3() {
  check_thread_status_during_io(__state_flag_3, __thread_3);
}

void __init_thread_info_3(thread_info *info) {
  __state_flag_3 = info->get_state_flag();
}

thread_info *__get_thread_info_3() {
  if (__thread_3 != NULL) return __thread_3;
  __thread_3 = new thread_info(3, check_status_during_io__3);
  __init_thread_info_3(__thread_3);
  return __thread_3;
}

void __declare_sockets_3() {
  init_instance::add_incoming(2,3, DATA_SOCKET);
  init_instance::add_incoming(5,3, DATA_SOCKET);
  init_instance::add_outgoing(3,4, DATA_SOCKET);
}

void __init_sockets_3(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_3() {
}

void __peek_sockets_3() {
}


inline void __push_3_4(int data) {
BUFFER_3_4[HEAD_3_4]=data;
HEAD_3_4++;
}


inline int __pop_2_3() {
int res=BUFFER_2_3[TAIL_2_3];
TAIL_2_3++;
return res;
}

inline int __pop_5_3() {
int res=BUFFER_5_3[TAIL_5_3];
TAIL_5_3++;
return res;
}


void __joiner_3_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_5_3());
  }
}


// peek: 17 pop: 17 push 0
// init counts: 0 steady counts: 128

// ClusterFusion isEliminated: false



int __number_of_iterations_4;
int __counter_4 = 0;
int __steady_4 = 0;
int __tmp_4 = 0;
int __tmp2_4 = 0;
int *__state_flag_4 = NULL;
thread_info *__thread_4 = NULL;



int sum__14__4 = 0;
void save_peek_buffer__4(object_write_buffer *buf);
void load_peek_buffer__4(object_write_buffer *buf);
void save_file_pointer__4(object_write_buffer *buf);
void load_file_pointer__4(object_write_buffer *buf);

inline void check_status__4() {
  check_thread_status(__state_flag_4, __thread_4);
}

void check_status_during_io__4() {
  check_thread_status_during_io(__state_flag_4, __thread_4);
}

void __init_thread_info_4(thread_info *info) {
  __state_flag_4 = info->get_state_flag();
}

thread_info *__get_thread_info_4() {
  if (__thread_4 != NULL) return __thread_4;
  __thread_4 = new thread_info(4, check_status_during_io__4);
  __init_thread_info_4(__thread_4);
  return __thread_4;
}

void __declare_sockets_4() {
  init_instance::add_incoming(3,4, DATA_SOCKET);
}

void __init_sockets_4(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_4() {
}

void __peek_sockets_4() {
}

 
void init_printer__19_14__4();
inline void check_status__4();

void work_printer__19_14__4(int);


inline int __pop_3_4() {
int res=BUFFER_3_4[TAIL_3_4];
TAIL_3_4++;
return res;
}

inline void __pop_3_4(int n) {
TAIL_3_4+=n;
}

inline int __peek_3_4(int offs) {
return BUFFER_3_4[TAIL_3_4+offs];
}


 
void init_printer__19_14__4(){
  ((sum__14__4) = 0)/*int*/;
}
void save_file_pointer__4(object_write_buffer *buf) {}
void load_file_pointer__4(object_write_buffer *buf) {}
 
void work_printer__19_14__4(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int v__17 = 0;/* int */
      int i__18 = 0;/* int */

      // mark begin: SIRFilter printer

      for ((i__18 = 0)/*int*/; (i__18 < 17); (i__18++)) {{
          v__17 = BUFFER_3_4[TAIL_3_4]; TAIL_3_4++;
;
          ((sum__14__4) = ((sum__14__4) + v__17))/*int*/;

          // TIMER_PRINT_CODE: __print_sink__ += (int)(v__17); 

        }
      }
      // mark end: SIRFilter printer

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 128

// ClusterFusion isEliminated: false



int __number_of_iterations_5;
int __counter_5 = 0;
int __steady_5 = 0;
int __tmp_5 = 0;
int __tmp2_5 = 0;
int *__state_flag_5 = NULL;
thread_info *__thread_5 = NULL;



void save_peek_buffer__5(object_write_buffer *buf);
void load_peek_buffer__5(object_write_buffer *buf);
void save_file_pointer__5(object_write_buffer *buf);
void load_file_pointer__5(object_write_buffer *buf);

inline void check_status__5() {
  check_thread_status(__state_flag_5, __thread_5);
}

void check_status_during_io__5() {
  check_thread_status_during_io(__state_flag_5, __thread_5);
}

void __init_thread_info_5(thread_info *info) {
  __state_flag_5 = info->get_state_flag();
}

thread_info *__get_thread_info_5() {
  if (__thread_5 != NULL) return __thread_5;
  __thread_5 = new thread_info(5, check_status_during_io__5);
  __init_thread_info_5(__thread_5);
  return __thread_5;
}

void __declare_sockets_5() {
  init_instance::add_incoming(1,5, DATA_SOCKET);
  init_instance::add_outgoing(5,3, DATA_SOCKET);
}

void __init_sockets_5(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_5() {
}

void __peek_sockets_5() {
}

 
void init_Identity__13_13__5();
inline void check_status__5();

void work_Identity__13_13__5(int);


inline int __pop_1_5() {
int res=BUFFER_1_5[TAIL_1_5];
TAIL_1_5++;
return res;
}



inline void __push_5_3(int data) {
BUFFER_5_3[HEAD_5_3]=data;
HEAD_5_3++;
}



 
void init_Identity__13_13__5(){
}
void save_file_pointer__5(object_write_buffer *buf) {}
void load_file_pointer__5(object_write_buffer *buf) {}
 
void work_Identity__13_13__5(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int __tmp3__12 = 0;/* int */

      // mark begin: SIRIdentity Identity

      __tmp3__12 = BUFFER_1_5[TAIL_1_5]; TAIL_1_5++;
;
      __push_5_3(__tmp3__12);
      // mark end: SIRIdentity Identity

    }
  }
}

