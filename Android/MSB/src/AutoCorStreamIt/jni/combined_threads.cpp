#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <netsocket.h>
#include <node_server.h>
#include <init_instance.h>
#include <master_server.h>
#include <save_state.h>
#include <save_manager.h>
#include <delete_chkpts.h>
#include <object_write_buffer.h>
#include <read_setup.h>
#include <ccp.h>
#include <streamit_random.h>
#include "structs.h"
#include "fusion.h"

int __max_iteration;
int __timer_enabled = 0;
int __frequency_of_chkpts;
volatile int __vol;


float BUFFER_0_1[__BUF_SIZE_MASK_0_1 + 1];
int HEAD_0_1 = 0;
int TAIL_0_1 = 0;
float BUFFER_1_2[__BUF_SIZE_MASK_1_2 + 1];
int HEAD_1_2 = 0;
int TAIL_1_2 = 0;
float BUFFER_1_5[__BUF_SIZE_MASK_1_5 + 1];
int HEAD_1_5 = 0;
int TAIL_1_5 = 0;
float BUFFER_1_6[__BUF_SIZE_MASK_1_6 + 1];
int HEAD_1_6 = 0;
int TAIL_1_6 = 0;
float BUFFER_1_7[__BUF_SIZE_MASK_1_7 + 1];
int HEAD_1_7 = 0;
int TAIL_1_7 = 0;
float BUFFER_1_8[__BUF_SIZE_MASK_1_8 + 1];
int HEAD_1_8 = 0;
int TAIL_1_8 = 0;
float BUFFER_1_9[__BUF_SIZE_MASK_1_9 + 1];
int HEAD_1_9 = 0;
int TAIL_1_9 = 0;
float BUFFER_1_10[__BUF_SIZE_MASK_1_10 + 1];
int HEAD_1_10 = 0;
int TAIL_1_10 = 0;
float BUFFER_1_11[__BUF_SIZE_MASK_1_11 + 1];
int HEAD_1_11 = 0;
int TAIL_1_11 = 0;
float BUFFER_1_12[__BUF_SIZE_MASK_1_12 + 1];
int HEAD_1_12 = 0;
int TAIL_1_12 = 0;
float BUFFER_1_13[__BUF_SIZE_MASK_1_13 + 1];
int HEAD_1_13 = 0;
int TAIL_1_13 = 0;
float BUFFER_1_14[__BUF_SIZE_MASK_1_14 + 1];
int HEAD_1_14 = 0;
int TAIL_1_14 = 0;
float BUFFER_1_15[__BUF_SIZE_MASK_1_15 + 1];
int HEAD_1_15 = 0;
int TAIL_1_15 = 0;
float BUFFER_1_16[__BUF_SIZE_MASK_1_16 + 1];
int HEAD_1_16 = 0;
int TAIL_1_16 = 0;
float BUFFER_1_17[__BUF_SIZE_MASK_1_17 + 1];
int HEAD_1_17 = 0;
int TAIL_1_17 = 0;
float BUFFER_1_18[__BUF_SIZE_MASK_1_18 + 1];
int HEAD_1_18 = 0;
int TAIL_1_18 = 0;
float BUFFER_1_19[__BUF_SIZE_MASK_1_19 + 1];
int HEAD_1_19 = 0;
int TAIL_1_19 = 0;
float BUFFER_2_3[__BUF_SIZE_MASK_2_3 + 1];
int HEAD_2_3 = 0;
int TAIL_2_3 = 0;
float BUFFER_3_4[__BUF_SIZE_MASK_3_4 + 1];
int HEAD_3_4 = 0;
int TAIL_3_4 = 0;
float BUFFER_5_3[__BUF_SIZE_MASK_5_3 + 1];
int HEAD_5_3 = 0;
int TAIL_5_3 = 0;
float BUFFER_6_3[__BUF_SIZE_MASK_6_3 + 1];
int HEAD_6_3 = 0;
int TAIL_6_3 = 0;
float BUFFER_7_3[__BUF_SIZE_MASK_7_3 + 1];
int HEAD_7_3 = 0;
int TAIL_7_3 = 0;
float BUFFER_8_3[__BUF_SIZE_MASK_8_3 + 1];
int HEAD_8_3 = 0;
int TAIL_8_3 = 0;
float BUFFER_9_3[__BUF_SIZE_MASK_9_3 + 1];
int HEAD_9_3 = 0;
int TAIL_9_3 = 0;
float BUFFER_10_3[__BUF_SIZE_MASK_10_3 + 1];
int HEAD_10_3 = 0;
int TAIL_10_3 = 0;
float BUFFER_11_3[__BUF_SIZE_MASK_11_3 + 1];
int HEAD_11_3 = 0;
int TAIL_11_3 = 0;
float BUFFER_12_3[__BUF_SIZE_MASK_12_3 + 1];
int HEAD_12_3 = 0;
int TAIL_12_3 = 0;
float BUFFER_13_3[__BUF_SIZE_MASK_13_3 + 1];
int HEAD_13_3 = 0;
int TAIL_13_3 = 0;
float BUFFER_14_3[__BUF_SIZE_MASK_14_3 + 1];
int HEAD_14_3 = 0;
int TAIL_14_3 = 0;
float BUFFER_15_3[__BUF_SIZE_MASK_15_3 + 1];
int HEAD_15_3 = 0;
int TAIL_15_3 = 0;
float BUFFER_16_3[__BUF_SIZE_MASK_16_3 + 1];
int HEAD_16_3 = 0;
int TAIL_16_3 = 0;
float BUFFER_17_3[__BUF_SIZE_MASK_17_3 + 1];
int HEAD_17_3 = 0;
int TAIL_17_3 = 0;
float BUFFER_18_3[__BUF_SIZE_MASK_18_3 + 1];
int HEAD_18_3 = 0;
int TAIL_18_3 = 0;
float BUFFER_19_3[__BUF_SIZE_MASK_19_3 + 1];
int HEAD_19_3 = 0;
int TAIL_19_3 = 0;
void init_OneSource__3_10__0();
void work_OneSource__3_10__0(int);
void __splitter_1_work(int);
void init_AnonFilter_a0__13_11__2();
void work_AnonFilter_a0__13_11__2(int);
void __joiner_3_work(int);
void init_FloatPrinter__167_27__4();
void work_FloatPrinter__167_27__4(int);
void init_AnonFilter_a0__23_12__5();
void work_AnonFilter_a0__23_12__5(int);
void init_AnonFilter_a0__33_13__6();
void work_AnonFilter_a0__33_13__6(int);
void init_AnonFilter_a0__43_14__7();
void work_AnonFilter_a0__43_14__7(int);
void init_AnonFilter_a0__53_15__8();
void work_AnonFilter_a0__53_15__8(int);
void init_AnonFilter_a0__63_16__9();
void work_AnonFilter_a0__63_16__9(int);
void init_AnonFilter_a0__73_17__10();
void work_AnonFilter_a0__73_17__10(int);
void init_AnonFilter_a0__83_18__11();
void work_AnonFilter_a0__83_18__11(int);
void init_AnonFilter_a0__93_19__12();
void work_AnonFilter_a0__93_19__12(int);
void init_AnonFilter_a0__103_20__13();
void work_AnonFilter_a0__103_20__13(int);
void init_AnonFilter_a0__113_21__14();
void work_AnonFilter_a0__113_21__14(int);
void init_AnonFilter_a0__123_22__15();
void work_AnonFilter_a0__123_22__15(int);
void init_AnonFilter_a0__133_23__16();
void work_AnonFilter_a0__133_23__16(int);
void init_AnonFilter_a0__143_24__17();
void work_AnonFilter_a0__143_24__17(int);
void init_AnonFilter_a0__153_25__18();
void work_AnonFilter_a0__153_25__18(int);
void init_AnonFilter_a0__163_26__19();
void work_AnonFilter_a0__163_26__19(int);

int main(int argc, char **argv) {
  read_setup::read_setup_file();
  __max_iteration = read_setup::max_iteration;
  for (int a = 1; a < argc; a++) {
    if (argc > a + 1 && strcmp(argv[a], "-i") == 0) {
      int tmp;
      sscanf(argv[a + 1], "%d", &tmp);
#ifdef VERBOSE
#endif
      __max_iteration = tmp;
    }
    if (strcmp(argv[a], "-t") == 0) {
#ifdef VERBOSE
#endif
      __timer_enabled = 1;
    }
  }
// number of phases: 5


  // ============= Initialization =============

init_OneSource__3_10__0();
init_AnonFilter_a0__13_11__2();
init_AnonFilter_a0__23_12__5();
init_AnonFilter_a0__33_13__6();
init_AnonFilter_a0__43_14__7();
init_AnonFilter_a0__53_15__8();
init_AnonFilter_a0__63_16__9();
init_AnonFilter_a0__73_17__10();
init_AnonFilter_a0__83_18__11();
init_AnonFilter_a0__93_19__12();
init_AnonFilter_a0__103_20__13();
init_AnonFilter_a0__113_21__14();
init_AnonFilter_a0__123_22__15();
init_AnonFilter_a0__133_23__16();
init_AnonFilter_a0__143_24__17();
init_AnonFilter_a0__153_25__18();
init_AnonFilter_a0__163_26__19();
init_FloatPrinter__167_27__4();

  // ============= Steady State =============

  if (__timer_enabled) {
  }
  for (int n = 0; n < (__max_iteration  ); n++) {
HEAD_0_1 = 0;
TAIL_0_1 = 0;
    work_OneSource__3_10__0(128 );
HEAD_1_2 = 0;
TAIL_1_2 = 0;
HEAD_1_5 = 0;
TAIL_1_5 = 0;
HEAD_1_6 = 0;
TAIL_1_6 = 0;
HEAD_1_7 = 0;
TAIL_1_7 = 0;
HEAD_1_8 = 0;
TAIL_1_8 = 0;
HEAD_1_9 = 0;
TAIL_1_9 = 0;
HEAD_1_10 = 0;
TAIL_1_10 = 0;
HEAD_1_11 = 0;
TAIL_1_11 = 0;
HEAD_1_12 = 0;
TAIL_1_12 = 0;
HEAD_1_13 = 0;
TAIL_1_13 = 0;
HEAD_1_14 = 0;
TAIL_1_14 = 0;
HEAD_1_15 = 0;
TAIL_1_15 = 0;
HEAD_1_16 = 0;
TAIL_1_16 = 0;
HEAD_1_17 = 0;
TAIL_1_17 = 0;
HEAD_1_18 = 0;
TAIL_1_18 = 0;
HEAD_1_19 = 0;
TAIL_1_19 = 0;
    __splitter_1_work(128 );
HEAD_2_3 = 0;
TAIL_2_3 = 0;
    work_AnonFilter_a0__13_11__2(1 );
HEAD_5_3 = 0;
TAIL_5_3 = 0;
    work_AnonFilter_a0__23_12__5(1 );
HEAD_6_3 = 0;
TAIL_6_3 = 0;
    work_AnonFilter_a0__33_13__6(1 );
HEAD_7_3 = 0;
TAIL_7_3 = 0;
    work_AnonFilter_a0__43_14__7(1 );
HEAD_8_3 = 0;
TAIL_8_3 = 0;
    work_AnonFilter_a0__53_15__8(1 );
HEAD_9_3 = 0;
TAIL_9_3 = 0;
    work_AnonFilter_a0__63_16__9(1 );
HEAD_10_3 = 0;
TAIL_10_3 = 0;
    work_AnonFilter_a0__73_17__10(1 );
HEAD_11_3 = 0;
TAIL_11_3 = 0;
    work_AnonFilter_a0__83_18__11(1 );
HEAD_12_3 = 0;
TAIL_12_3 = 0;
    work_AnonFilter_a0__93_19__12(1 );
HEAD_13_3 = 0;
TAIL_13_3 = 0;
    work_AnonFilter_a0__103_20__13(1 );
HEAD_14_3 = 0;
TAIL_14_3 = 0;
    work_AnonFilter_a0__113_21__14(1 );
HEAD_15_3 = 0;
TAIL_15_3 = 0;
    work_AnonFilter_a0__123_22__15(1 );
HEAD_16_3 = 0;
TAIL_16_3 = 0;
    work_AnonFilter_a0__133_23__16(1 );
HEAD_17_3 = 0;
TAIL_17_3 = 0;
    work_AnonFilter_a0__143_24__17(1 );
HEAD_18_3 = 0;
TAIL_18_3 = 0;
    work_AnonFilter_a0__153_25__18(1 );
HEAD_19_3 = 0;
TAIL_19_3 = 0;
    work_AnonFilter_a0__163_26__19(1 );
HEAD_3_4 = 0;
TAIL_3_4 = 0;
    __joiner_3_work(1 );
    work_FloatPrinter__167_27__4(16 );
  }
if (__timer_enabled) {
  }


  return 0;
}

// moved or inserted by concat_cluster_threads.pl
#include <message.h>
message *__msg_stack_2;
message *__msg_stack_15;
message *__msg_stack_4;
message *__msg_stack_16;
message *__msg_stack_17;
message *__msg_stack_1;
message *__msg_stack_10;
message *__msg_stack_11;
message *__msg_stack_14;
message *__msg_stack_19;
message *__msg_stack_7;
message *__msg_stack_9;
message *__msg_stack_12;
message *__msg_stack_3;
message *__msg_stack_0;
message *__msg_stack_13;
message *__msg_stack_6;
message *__msg_stack_5;
message *__msg_stack_18;
message *__msg_stack_8;

// end of moved or inserted by concat_cluster_threads.pl

// peek: 0 pop: 0 push 1
// init counts: 0 steady counts: 128

// ClusterFusion isEliminated: false


#include <mysocket.h>
#include <sdep.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "global.h"

int __number_of_iterations_0;
int __counter_0 = 0;
int __steady_0 = 0;
int __tmp_0 = 0;
int __tmp2_0 = 0;
int *__state_flag_0 = NULL;
thread_info *__thread_0 = NULL;



float n__0__0 = 0.0f;
void save_file_pointer__0(object_write_buffer *buf);
void load_file_pointer__0(object_write_buffer *buf);

inline void check_status__0() {
  check_thread_status(__state_flag_0, __thread_0);
}

void check_status_during_io__0() {
  check_thread_status_during_io(__state_flag_0, __thread_0);
}

void __init_thread_info_0(thread_info *info) {
  __state_flag_0 = info->get_state_flag();
}

thread_info *__get_thread_info_0() {
  if (__thread_0 != NULL) return __thread_0;
  __thread_0 = new thread_info(0, check_status_during_io__0);
  __init_thread_info_0(__thread_0);
  return __thread_0;
}

void __declare_sockets_0() {
  init_instance::add_outgoing(0,1, DATA_SOCKET);
}

void __init_sockets_0(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_0() {
}

void __peek_sockets_0() {
}

 
void init_OneSource__3_10__0();
inline void check_status__0();

void work_OneSource__3_10__0(int);



inline void __push_0_1(float data) {
BUFFER_0_1[HEAD_0_1]=data;
HEAD_0_1++;
}



 
void init_OneSource__3_10__0(){
  ((n__0__0) = ((float)0.0))/*float*/;
}
void save_file_pointer__0(object_write_buffer *buf) {}
void load_file_pointer__0(object_write_buffer *buf) {}
 
void work_OneSource__3_10__0(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter OneSource

      __push_0_1((n__0__0));
      ((n__0__0) = ((n__0__0) + ((float)0.01)))/*float*/;
      // mark end: SIRFilter OneSource

    }
  }
}

// init counts: 0 steady counts: 128

// ClusterFusion isEliminated: false



int __number_of_iterations_1;
int __counter_1 = 0;
int __steady_1 = 0;
int __tmp_1 = 0;
int __tmp2_1 = 0;
int *__state_flag_1 = NULL;
thread_info *__thread_1 = NULL;



inline void check_status__1() {
  check_thread_status(__state_flag_1, __thread_1);
}

void check_status_during_io__1() {
  check_thread_status_during_io(__state_flag_1, __thread_1);
}

void __init_thread_info_1(thread_info *info) {
  __state_flag_1 = info->get_state_flag();
}

thread_info *__get_thread_info_1() {
  if (__thread_1 != NULL) return __thread_1;
  __thread_1 = new thread_info(1, check_status_during_io__1);
  __init_thread_info_1(__thread_1);
  return __thread_1;
}

void __declare_sockets_1() {
  init_instance::add_incoming(0,1, DATA_SOCKET);
  init_instance::add_outgoing(1,2, DATA_SOCKET);
  init_instance::add_outgoing(1,5, DATA_SOCKET);
  init_instance::add_outgoing(1,6, DATA_SOCKET);
  init_instance::add_outgoing(1,7, DATA_SOCKET);
  init_instance::add_outgoing(1,8, DATA_SOCKET);
  init_instance::add_outgoing(1,9, DATA_SOCKET);
  init_instance::add_outgoing(1,10, DATA_SOCKET);
  init_instance::add_outgoing(1,11, DATA_SOCKET);
  init_instance::add_outgoing(1,12, DATA_SOCKET);
  init_instance::add_outgoing(1,13, DATA_SOCKET);
  init_instance::add_outgoing(1,14, DATA_SOCKET);
  init_instance::add_outgoing(1,15, DATA_SOCKET);
  init_instance::add_outgoing(1,16, DATA_SOCKET);
  init_instance::add_outgoing(1,17, DATA_SOCKET);
  init_instance::add_outgoing(1,18, DATA_SOCKET);
  init_instance::add_outgoing(1,19, DATA_SOCKET);
}

void __init_sockets_1(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_1() {
}

void __peek_sockets_1() {
}

inline float __pop_0_1() {
float res=BUFFER_0_1[TAIL_0_1];
TAIL_0_1++;
return res;
}


inline void __push_1_2(float data) {
BUFFER_1_2[HEAD_1_2]=data;
HEAD_1_2++;
}



inline void __push_1_5(float data) {
BUFFER_1_5[HEAD_1_5]=data;
HEAD_1_5++;
}



inline void __push_1_6(float data) {
BUFFER_1_6[HEAD_1_6]=data;
HEAD_1_6++;
}



inline void __push_1_7(float data) {
BUFFER_1_7[HEAD_1_7]=data;
HEAD_1_7++;
}



inline void __push_1_8(float data) {
BUFFER_1_8[HEAD_1_8]=data;
HEAD_1_8++;
}



inline void __push_1_9(float data) {
BUFFER_1_9[HEAD_1_9]=data;
HEAD_1_9++;
}



inline void __push_1_10(float data) {
BUFFER_1_10[HEAD_1_10]=data;
HEAD_1_10++;
}



inline void __push_1_11(float data) {
BUFFER_1_11[HEAD_1_11]=data;
HEAD_1_11++;
}



inline void __push_1_12(float data) {
BUFFER_1_12[HEAD_1_12]=data;
HEAD_1_12++;
}



inline void __push_1_13(float data) {
BUFFER_1_13[HEAD_1_13]=data;
HEAD_1_13++;
}



inline void __push_1_14(float data) {
BUFFER_1_14[HEAD_1_14]=data;
HEAD_1_14++;
}



inline void __push_1_15(float data) {
BUFFER_1_15[HEAD_1_15]=data;
HEAD_1_15++;
}



inline void __push_1_16(float data) {
BUFFER_1_16[HEAD_1_16]=data;
HEAD_1_16++;
}



inline void __push_1_17(float data) {
BUFFER_1_17[HEAD_1_17]=data;
HEAD_1_17++;
}



inline void __push_1_18(float data) {
BUFFER_1_18[HEAD_1_18]=data;
HEAD_1_18++;
}



inline void __push_1_19(float data) {
BUFFER_1_19[HEAD_1_19]=data;
HEAD_1_19++;
}



void __splitter_1_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_0_1[TAIL_0_1]; TAIL_0_1++;
;
  __push_1_2(tmp);
  __push_1_5(tmp);
  __push_1_6(tmp);
  __push_1_7(tmp);
  __push_1_8(tmp);
  __push_1_9(tmp);
  __push_1_10(tmp);
  __push_1_11(tmp);
  __push_1_12(tmp);
  __push_1_13(tmp);
  __push_1_14(tmp);
  __push_1_15(tmp);
  __push_1_16(tmp);
  __push_1_17(tmp);
  __push_1_18(tmp);
  __push_1_19(tmp);
  }
}


// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_10;
int __counter_10 = 0;
int __steady_10 = 0;
int __tmp_10 = 0;
int __tmp2_10 = 0;
int *__state_flag_10 = NULL;
thread_info *__thread_10 = NULL;



void save_peek_buffer__10(object_write_buffer *buf);
void load_peek_buffer__10(object_write_buffer *buf);
void save_file_pointer__10(object_write_buffer *buf);
void load_file_pointer__10(object_write_buffer *buf);

inline void check_status__10() {
  check_thread_status(__state_flag_10, __thread_10);
}

void check_status_during_io__10() {
  check_thread_status_during_io(__state_flag_10, __thread_10);
}

void __init_thread_info_10(thread_info *info) {
  __state_flag_10 = info->get_state_flag();
}

thread_info *__get_thread_info_10() {
  if (__thread_10 != NULL) return __thread_10;
  __thread_10 = new thread_info(10, check_status_during_io__10);
  __init_thread_info_10(__thread_10);
  return __thread_10;
}

void __declare_sockets_10() {
  init_instance::add_incoming(1,10, DATA_SOCKET);
  init_instance::add_outgoing(10,3, DATA_SOCKET);
}

void __init_sockets_10(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_10() {
}

void __peek_sockets_10() {
}

 
void init_AnonFilter_a0__73_17__10();
inline void check_status__10();

void work_AnonFilter_a0__73_17__10(int);


inline float __pop_1_10() {
float res=BUFFER_1_10[TAIL_1_10];
TAIL_1_10++;
return res;
}

inline void __pop_1_10(int n) {
TAIL_1_10+=n;
}

inline float __peek_1_10(int offs) {
return BUFFER_1_10[TAIL_1_10+offs];
}



inline void __push_10_3(float data) {
BUFFER_10_3[HEAD_10_3]=data;
HEAD_10_3++;
}



 
void init_AnonFilter_a0__73_17__10(){
}
void save_file_pointer__10(object_write_buffer *buf) {}
void load_file_pointer__10(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__73_17__10(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__66 = 0.0f;/* float */
      float __tmp2__67 = 0.0f;/* float */
      float __tmp3__68 = 0.0f;/* float */
      float __tmp4__69 = 0.0f;/* float */
      int __tmp5__70 = 0;/* int */
      int i__conflict__0__71 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__66 = ((float)0.0))/*float*/;
      for ((i__conflict__0__71 = 0)/*int*/; (i__conflict__0__71 < 122); (i__conflict__0__71++)) {{
          __tmp3__68 = BUFFER_1_10[TAIL_1_10+i__conflict__0__71];
;
          (__tmp5__70 = (i__conflict__0__71 + 6))/*int*/;
          __tmp4__69 = BUFFER_1_10[TAIL_1_10+__tmp5__70];
;
          (__tmp2__67 = (__tmp3__68 * __tmp4__69))/*float*/;
          (sum__66 = (sum__66 + __tmp2__67))/*float*/;
        }
      }
      __pop_1_10(128);
      __push_10_3(sum__66);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_11;
int __counter_11 = 0;
int __steady_11 = 0;
int __tmp_11 = 0;
int __tmp2_11 = 0;
int *__state_flag_11 = NULL;
thread_info *__thread_11 = NULL;



void save_peek_buffer__11(object_write_buffer *buf);
void load_peek_buffer__11(object_write_buffer *buf);
void save_file_pointer__11(object_write_buffer *buf);
void load_file_pointer__11(object_write_buffer *buf);

inline void check_status__11() {
  check_thread_status(__state_flag_11, __thread_11);
}

void check_status_during_io__11() {
  check_thread_status_during_io(__state_flag_11, __thread_11);
}

void __init_thread_info_11(thread_info *info) {
  __state_flag_11 = info->get_state_flag();
}

thread_info *__get_thread_info_11() {
  if (__thread_11 != NULL) return __thread_11;
  __thread_11 = new thread_info(11, check_status_during_io__11);
  __init_thread_info_11(__thread_11);
  return __thread_11;
}

void __declare_sockets_11() {
  init_instance::add_incoming(1,11, DATA_SOCKET);
  init_instance::add_outgoing(11,3, DATA_SOCKET);
}

void __init_sockets_11(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_11() {
}

void __peek_sockets_11() {
}

 
void init_AnonFilter_a0__83_18__11();
inline void check_status__11();

void work_AnonFilter_a0__83_18__11(int);


inline float __pop_1_11() {
float res=BUFFER_1_11[TAIL_1_11];
TAIL_1_11++;
return res;
}

inline void __pop_1_11(int n) {
TAIL_1_11+=n;
}

inline float __peek_1_11(int offs) {
return BUFFER_1_11[TAIL_1_11+offs];
}



inline void __push_11_3(float data) {
BUFFER_11_3[HEAD_11_3]=data;
HEAD_11_3++;
}



 
void init_AnonFilter_a0__83_18__11(){
}
void save_file_pointer__11(object_write_buffer *buf) {}
void load_file_pointer__11(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__83_18__11(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__76 = 0.0f;/* float */
      float __tmp2__77 = 0.0f;/* float */
      float __tmp3__78 = 0.0f;/* float */
      float __tmp4__79 = 0.0f;/* float */
      int __tmp5__80 = 0;/* int */
      int i__conflict__0__81 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__76 = ((float)0.0))/*float*/;
      for ((i__conflict__0__81 = 0)/*int*/; (i__conflict__0__81 < 121); (i__conflict__0__81++)) {{
          __tmp3__78 = BUFFER_1_11[TAIL_1_11+i__conflict__0__81];
;
          (__tmp5__80 = (i__conflict__0__81 + 7))/*int*/;
          __tmp4__79 = BUFFER_1_11[TAIL_1_11+__tmp5__80];
;
          (__tmp2__77 = (__tmp3__78 * __tmp4__79))/*float*/;
          (sum__76 = (sum__76 + __tmp2__77))/*float*/;
        }
      }
      __pop_1_11(128);
      __push_11_3(sum__76);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_12;
int __counter_12 = 0;
int __steady_12 = 0;
int __tmp_12 = 0;
int __tmp2_12 = 0;
int *__state_flag_12 = NULL;
thread_info *__thread_12 = NULL;



void save_peek_buffer__12(object_write_buffer *buf);
void load_peek_buffer__12(object_write_buffer *buf);
void save_file_pointer__12(object_write_buffer *buf);
void load_file_pointer__12(object_write_buffer *buf);

inline void check_status__12() {
  check_thread_status(__state_flag_12, __thread_12);
}

void check_status_during_io__12() {
  check_thread_status_during_io(__state_flag_12, __thread_12);
}

void __init_thread_info_12(thread_info *info) {
  __state_flag_12 = info->get_state_flag();
}

thread_info *__get_thread_info_12() {
  if (__thread_12 != NULL) return __thread_12;
  __thread_12 = new thread_info(12, check_status_during_io__12);
  __init_thread_info_12(__thread_12);
  return __thread_12;
}

void __declare_sockets_12() {
  init_instance::add_incoming(1,12, DATA_SOCKET);
  init_instance::add_outgoing(12,3, DATA_SOCKET);
}

void __init_sockets_12(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_12() {
}

void __peek_sockets_12() {
}

 
void init_AnonFilter_a0__93_19__12();
inline void check_status__12();

void work_AnonFilter_a0__93_19__12(int);


inline float __pop_1_12() {
float res=BUFFER_1_12[TAIL_1_12];
TAIL_1_12++;
return res;
}

inline void __pop_1_12(int n) {
TAIL_1_12+=n;
}

inline float __peek_1_12(int offs) {
return BUFFER_1_12[TAIL_1_12+offs];
}



inline void __push_12_3(float data) {
BUFFER_12_3[HEAD_12_3]=data;
HEAD_12_3++;
}



 
void init_AnonFilter_a0__93_19__12(){
}
void save_file_pointer__12(object_write_buffer *buf) {}
void load_file_pointer__12(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__93_19__12(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__86 = 0.0f;/* float */
      float __tmp2__87 = 0.0f;/* float */
      float __tmp3__88 = 0.0f;/* float */
      float __tmp4__89 = 0.0f;/* float */
      int __tmp5__90 = 0;/* int */
      int i__conflict__0__91 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__86 = ((float)0.0))/*float*/;
      for ((i__conflict__0__91 = 0)/*int*/; (i__conflict__0__91 < 120); (i__conflict__0__91++)) {{
          __tmp3__88 = BUFFER_1_12[TAIL_1_12+i__conflict__0__91];
;
          (__tmp5__90 = (i__conflict__0__91 + 8))/*int*/;
          __tmp4__89 = BUFFER_1_12[TAIL_1_12+__tmp5__90];
;
          (__tmp2__87 = (__tmp3__88 * __tmp4__89))/*float*/;
          (sum__86 = (sum__86 + __tmp2__87))/*float*/;
        }
      }
      __pop_1_12(128);
      __push_12_3(sum__86);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_13;
int __counter_13 = 0;
int __steady_13 = 0;
int __tmp_13 = 0;
int __tmp2_13 = 0;
int *__state_flag_13 = NULL;
thread_info *__thread_13 = NULL;



void save_peek_buffer__13(object_write_buffer *buf);
void load_peek_buffer__13(object_write_buffer *buf);
void save_file_pointer__13(object_write_buffer *buf);
void load_file_pointer__13(object_write_buffer *buf);

inline void check_status__13() {
  check_thread_status(__state_flag_13, __thread_13);
}

void check_status_during_io__13() {
  check_thread_status_during_io(__state_flag_13, __thread_13);
}

void __init_thread_info_13(thread_info *info) {
  __state_flag_13 = info->get_state_flag();
}

thread_info *__get_thread_info_13() {
  if (__thread_13 != NULL) return __thread_13;
  __thread_13 = new thread_info(13, check_status_during_io__13);
  __init_thread_info_13(__thread_13);
  return __thread_13;
}

void __declare_sockets_13() {
  init_instance::add_incoming(1,13, DATA_SOCKET);
  init_instance::add_outgoing(13,3, DATA_SOCKET);
}

void __init_sockets_13(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_13() {
}

void __peek_sockets_13() {
}

 
void init_AnonFilter_a0__103_20__13();
inline void check_status__13();

void work_AnonFilter_a0__103_20__13(int);


inline float __pop_1_13() {
float res=BUFFER_1_13[TAIL_1_13];
TAIL_1_13++;
return res;
}

inline void __pop_1_13(int n) {
TAIL_1_13+=n;
}

inline float __peek_1_13(int offs) {
return BUFFER_1_13[TAIL_1_13+offs];
}



inline void __push_13_3(float data) {
BUFFER_13_3[HEAD_13_3]=data;
HEAD_13_3++;
}



 
void init_AnonFilter_a0__103_20__13(){
}
void save_file_pointer__13(object_write_buffer *buf) {}
void load_file_pointer__13(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__103_20__13(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__96 = 0.0f;/* float */
      float __tmp2__97 = 0.0f;/* float */
      float __tmp3__98 = 0.0f;/* float */
      float __tmp4__99 = 0.0f;/* float */
      int __tmp5__100 = 0;/* int */
      int i__conflict__0__101 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__96 = ((float)0.0))/*float*/;
      for ((i__conflict__0__101 = 0)/*int*/; (i__conflict__0__101 < 119); (i__conflict__0__101++)) {{
          __tmp3__98 = BUFFER_1_13[TAIL_1_13+i__conflict__0__101];
;
          (__tmp5__100 = (i__conflict__0__101 + 9))/*int*/;
          __tmp4__99 = BUFFER_1_13[TAIL_1_13+__tmp5__100];
;
          (__tmp2__97 = (__tmp3__98 * __tmp4__99))/*float*/;
          (sum__96 = (sum__96 + __tmp2__97))/*float*/;
        }
      }
      __pop_1_13(128);
      __push_13_3(sum__96);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_14;
int __counter_14 = 0;
int __steady_14 = 0;
int __tmp_14 = 0;
int __tmp2_14 = 0;
int *__state_flag_14 = NULL;
thread_info *__thread_14 = NULL;



void save_peek_buffer__14(object_write_buffer *buf);
void load_peek_buffer__14(object_write_buffer *buf);
void save_file_pointer__14(object_write_buffer *buf);
void load_file_pointer__14(object_write_buffer *buf);

inline void check_status__14() {
  check_thread_status(__state_flag_14, __thread_14);
}

void check_status_during_io__14() {
  check_thread_status_during_io(__state_flag_14, __thread_14);
}

void __init_thread_info_14(thread_info *info) {
  __state_flag_14 = info->get_state_flag();
}

thread_info *__get_thread_info_14() {
  if (__thread_14 != NULL) return __thread_14;
  __thread_14 = new thread_info(14, check_status_during_io__14);
  __init_thread_info_14(__thread_14);
  return __thread_14;
}

void __declare_sockets_14() {
  init_instance::add_incoming(1,14, DATA_SOCKET);
  init_instance::add_outgoing(14,3, DATA_SOCKET);
}

void __init_sockets_14(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_14() {
}

void __peek_sockets_14() {
}

 
void init_AnonFilter_a0__113_21__14();
inline void check_status__14();

void work_AnonFilter_a0__113_21__14(int);


inline float __pop_1_14() {
float res=BUFFER_1_14[TAIL_1_14];
TAIL_1_14++;
return res;
}

inline void __pop_1_14(int n) {
TAIL_1_14+=n;
}

inline float __peek_1_14(int offs) {
return BUFFER_1_14[TAIL_1_14+offs];
}



inline void __push_14_3(float data) {
BUFFER_14_3[HEAD_14_3]=data;
HEAD_14_3++;
}



 
void init_AnonFilter_a0__113_21__14(){
}
void save_file_pointer__14(object_write_buffer *buf) {}
void load_file_pointer__14(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__113_21__14(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__106 = 0.0f;/* float */
      float __tmp2__107 = 0.0f;/* float */
      float __tmp3__108 = 0.0f;/* float */
      float __tmp4__109 = 0.0f;/* float */
      int __tmp5__110 = 0;/* int */
      int i__conflict__0__111 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__106 = ((float)0.0))/*float*/;
      for ((i__conflict__0__111 = 0)/*int*/; (i__conflict__0__111 < 118); (i__conflict__0__111++)) {{
          __tmp3__108 = BUFFER_1_14[TAIL_1_14+i__conflict__0__111];
;
          (__tmp5__110 = (i__conflict__0__111 + 10))/*int*/;
          __tmp4__109 = BUFFER_1_14[TAIL_1_14+__tmp5__110];
;
          (__tmp2__107 = (__tmp3__108 * __tmp4__109))/*float*/;
          (sum__106 = (sum__106 + __tmp2__107))/*float*/;
        }
      }
      __pop_1_14(128);
      __push_14_3(sum__106);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_15;
int __counter_15 = 0;
int __steady_15 = 0;
int __tmp_15 = 0;
int __tmp2_15 = 0;
int *__state_flag_15 = NULL;
thread_info *__thread_15 = NULL;



void save_peek_buffer__15(object_write_buffer *buf);
void load_peek_buffer__15(object_write_buffer *buf);
void save_file_pointer__15(object_write_buffer *buf);
void load_file_pointer__15(object_write_buffer *buf);

inline void check_status__15() {
  check_thread_status(__state_flag_15, __thread_15);
}

void check_status_during_io__15() {
  check_thread_status_during_io(__state_flag_15, __thread_15);
}

void __init_thread_info_15(thread_info *info) {
  __state_flag_15 = info->get_state_flag();
}

thread_info *__get_thread_info_15() {
  if (__thread_15 != NULL) return __thread_15;
  __thread_15 = new thread_info(15, check_status_during_io__15);
  __init_thread_info_15(__thread_15);
  return __thread_15;
}

void __declare_sockets_15() {
  init_instance::add_incoming(1,15, DATA_SOCKET);
  init_instance::add_outgoing(15,3, DATA_SOCKET);
}

void __init_sockets_15(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_15() {
}

void __peek_sockets_15() {
}

 
void init_AnonFilter_a0__123_22__15();
inline void check_status__15();

void work_AnonFilter_a0__123_22__15(int);


inline float __pop_1_15() {
float res=BUFFER_1_15[TAIL_1_15];
TAIL_1_15++;
return res;
}

inline void __pop_1_15(int n) {
TAIL_1_15+=n;
}

inline float __peek_1_15(int offs) {
return BUFFER_1_15[TAIL_1_15+offs];
}



inline void __push_15_3(float data) {
BUFFER_15_3[HEAD_15_3]=data;
HEAD_15_3++;
}



 
void init_AnonFilter_a0__123_22__15(){
}
void save_file_pointer__15(object_write_buffer *buf) {}
void load_file_pointer__15(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__123_22__15(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__116 = 0.0f;/* float */
      float __tmp2__117 = 0.0f;/* float */
      float __tmp3__118 = 0.0f;/* float */
      float __tmp4__119 = 0.0f;/* float */
      int __tmp5__120 = 0;/* int */
      int i__conflict__0__121 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__116 = ((float)0.0))/*float*/;
      for ((i__conflict__0__121 = 0)/*int*/; (i__conflict__0__121 < 117); (i__conflict__0__121++)) {{
          __tmp3__118 = BUFFER_1_15[TAIL_1_15+i__conflict__0__121];
;
          (__tmp5__120 = (i__conflict__0__121 + 11))/*int*/;
          __tmp4__119 = BUFFER_1_15[TAIL_1_15+__tmp5__120];
;
          (__tmp2__117 = (__tmp3__118 * __tmp4__119))/*float*/;
          (sum__116 = (sum__116 + __tmp2__117))/*float*/;
        }
      }
      __pop_1_15(128);
      __push_15_3(sum__116);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_16;
int __counter_16 = 0;
int __steady_16 = 0;
int __tmp_16 = 0;
int __tmp2_16 = 0;
int *__state_flag_16 = NULL;
thread_info *__thread_16 = NULL;



void save_peek_buffer__16(object_write_buffer *buf);
void load_peek_buffer__16(object_write_buffer *buf);
void save_file_pointer__16(object_write_buffer *buf);
void load_file_pointer__16(object_write_buffer *buf);

inline void check_status__16() {
  check_thread_status(__state_flag_16, __thread_16);
}

void check_status_during_io__16() {
  check_thread_status_during_io(__state_flag_16, __thread_16);
}

void __init_thread_info_16(thread_info *info) {
  __state_flag_16 = info->get_state_flag();
}

thread_info *__get_thread_info_16() {
  if (__thread_16 != NULL) return __thread_16;
  __thread_16 = new thread_info(16, check_status_during_io__16);
  __init_thread_info_16(__thread_16);
  return __thread_16;
}

void __declare_sockets_16() {
  init_instance::add_incoming(1,16, DATA_SOCKET);
  init_instance::add_outgoing(16,3, DATA_SOCKET);
}

void __init_sockets_16(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_16() {
}

void __peek_sockets_16() {
}

 
void init_AnonFilter_a0__133_23__16();
inline void check_status__16();

void work_AnonFilter_a0__133_23__16(int);


inline float __pop_1_16() {
float res=BUFFER_1_16[TAIL_1_16];
TAIL_1_16++;
return res;
}

inline void __pop_1_16(int n) {
TAIL_1_16+=n;
}

inline float __peek_1_16(int offs) {
return BUFFER_1_16[TAIL_1_16+offs];
}



inline void __push_16_3(float data) {
BUFFER_16_3[HEAD_16_3]=data;
HEAD_16_3++;
}



 
void init_AnonFilter_a0__133_23__16(){
}
void save_file_pointer__16(object_write_buffer *buf) {}
void load_file_pointer__16(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__133_23__16(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__126 = 0.0f;/* float */
      float __tmp2__127 = 0.0f;/* float */
      float __tmp3__128 = 0.0f;/* float */
      float __tmp4__129 = 0.0f;/* float */
      int __tmp5__130 = 0;/* int */
      int i__conflict__0__131 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__126 = ((float)0.0))/*float*/;
      for ((i__conflict__0__131 = 0)/*int*/; (i__conflict__0__131 < 116); (i__conflict__0__131++)) {{
          __tmp3__128 = BUFFER_1_16[TAIL_1_16+i__conflict__0__131];
;
          (__tmp5__130 = (i__conflict__0__131 + 12))/*int*/;
          __tmp4__129 = BUFFER_1_16[TAIL_1_16+__tmp5__130];
;
          (__tmp2__127 = (__tmp3__128 * __tmp4__129))/*float*/;
          (sum__126 = (sum__126 + __tmp2__127))/*float*/;
        }
      }
      __pop_1_16(128);
      __push_16_3(sum__126);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_17;
int __counter_17 = 0;
int __steady_17 = 0;
int __tmp_17 = 0;
int __tmp2_17 = 0;
int *__state_flag_17 = NULL;
thread_info *__thread_17 = NULL;



void save_peek_buffer__17(object_write_buffer *buf);
void load_peek_buffer__17(object_write_buffer *buf);
void save_file_pointer__17(object_write_buffer *buf);
void load_file_pointer__17(object_write_buffer *buf);

inline void check_status__17() {
  check_thread_status(__state_flag_17, __thread_17);
}

void check_status_during_io__17() {
  check_thread_status_during_io(__state_flag_17, __thread_17);
}

void __init_thread_info_17(thread_info *info) {
  __state_flag_17 = info->get_state_flag();
}

thread_info *__get_thread_info_17() {
  if (__thread_17 != NULL) return __thread_17;
  __thread_17 = new thread_info(17, check_status_during_io__17);
  __init_thread_info_17(__thread_17);
  return __thread_17;
}

void __declare_sockets_17() {
  init_instance::add_incoming(1,17, DATA_SOCKET);
  init_instance::add_outgoing(17,3, DATA_SOCKET);
}

void __init_sockets_17(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_17() {
}

void __peek_sockets_17() {
}

 
void init_AnonFilter_a0__143_24__17();
inline void check_status__17();

void work_AnonFilter_a0__143_24__17(int);


inline float __pop_1_17() {
float res=BUFFER_1_17[TAIL_1_17];
TAIL_1_17++;
return res;
}

inline void __pop_1_17(int n) {
TAIL_1_17+=n;
}

inline float __peek_1_17(int offs) {
return BUFFER_1_17[TAIL_1_17+offs];
}



inline void __push_17_3(float data) {
BUFFER_17_3[HEAD_17_3]=data;
HEAD_17_3++;
}



 
void init_AnonFilter_a0__143_24__17(){
}
void save_file_pointer__17(object_write_buffer *buf) {}
void load_file_pointer__17(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__143_24__17(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__136 = 0.0f;/* float */
      float __tmp2__137 = 0.0f;/* float */
      float __tmp3__138 = 0.0f;/* float */
      float __tmp4__139 = 0.0f;/* float */
      int __tmp5__140 = 0;/* int */
      int i__conflict__0__141 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__136 = ((float)0.0))/*float*/;
      for ((i__conflict__0__141 = 0)/*int*/; (i__conflict__0__141 < 115); (i__conflict__0__141++)) {{
          __tmp3__138 = BUFFER_1_17[TAIL_1_17+i__conflict__0__141];
;
          (__tmp5__140 = (i__conflict__0__141 + 13))/*int*/;
          __tmp4__139 = BUFFER_1_17[TAIL_1_17+__tmp5__140];
;
          (__tmp2__137 = (__tmp3__138 * __tmp4__139))/*float*/;
          (sum__136 = (sum__136 + __tmp2__137))/*float*/;
        }
      }
      __pop_1_17(128);
      __push_17_3(sum__136);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_18;
int __counter_18 = 0;
int __steady_18 = 0;
int __tmp_18 = 0;
int __tmp2_18 = 0;
int *__state_flag_18 = NULL;
thread_info *__thread_18 = NULL;



void save_peek_buffer__18(object_write_buffer *buf);
void load_peek_buffer__18(object_write_buffer *buf);
void save_file_pointer__18(object_write_buffer *buf);
void load_file_pointer__18(object_write_buffer *buf);

inline void check_status__18() {
  check_thread_status(__state_flag_18, __thread_18);
}

void check_status_during_io__18() {
  check_thread_status_during_io(__state_flag_18, __thread_18);
}

void __init_thread_info_18(thread_info *info) {
  __state_flag_18 = info->get_state_flag();
}

thread_info *__get_thread_info_18() {
  if (__thread_18 != NULL) return __thread_18;
  __thread_18 = new thread_info(18, check_status_during_io__18);
  __init_thread_info_18(__thread_18);
  return __thread_18;
}

void __declare_sockets_18() {
  init_instance::add_incoming(1,18, DATA_SOCKET);
  init_instance::add_outgoing(18,3, DATA_SOCKET);
}

void __init_sockets_18(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_18() {
}

void __peek_sockets_18() {
}

 
void init_AnonFilter_a0__153_25__18();
inline void check_status__18();

void work_AnonFilter_a0__153_25__18(int);


inline float __pop_1_18() {
float res=BUFFER_1_18[TAIL_1_18];
TAIL_1_18++;
return res;
}

inline void __pop_1_18(int n) {
TAIL_1_18+=n;
}

inline float __peek_1_18(int offs) {
return BUFFER_1_18[TAIL_1_18+offs];
}



inline void __push_18_3(float data) {
BUFFER_18_3[HEAD_18_3]=data;
HEAD_18_3++;
}



 
void init_AnonFilter_a0__153_25__18(){
}
void save_file_pointer__18(object_write_buffer *buf) {}
void load_file_pointer__18(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__153_25__18(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__146 = 0.0f;/* float */
      float __tmp2__147 = 0.0f;/* float */
      float __tmp3__148 = 0.0f;/* float */
      float __tmp4__149 = 0.0f;/* float */
      int __tmp5__150 = 0;/* int */
      int i__conflict__0__151 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__146 = ((float)0.0))/*float*/;
      for ((i__conflict__0__151 = 0)/*int*/; (i__conflict__0__151 < 114); (i__conflict__0__151++)) {{
          __tmp3__148 = BUFFER_1_18[TAIL_1_18+i__conflict__0__151];
;
          (__tmp5__150 = (i__conflict__0__151 + 14))/*int*/;
          __tmp4__149 = BUFFER_1_18[TAIL_1_18+__tmp5__150];
;
          (__tmp2__147 = (__tmp3__148 * __tmp4__149))/*float*/;
          (sum__146 = (sum__146 + __tmp2__147))/*float*/;
        }
      }
      __pop_1_18(128);
      __push_18_3(sum__146);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_19;
int __counter_19 = 0;
int __steady_19 = 0;
int __tmp_19 = 0;
int __tmp2_19 = 0;
int *__state_flag_19 = NULL;
thread_info *__thread_19 = NULL;



void save_peek_buffer__19(object_write_buffer *buf);
void load_peek_buffer__19(object_write_buffer *buf);
void save_file_pointer__19(object_write_buffer *buf);
void load_file_pointer__19(object_write_buffer *buf);

inline void check_status__19() {
  check_thread_status(__state_flag_19, __thread_19);
}

void check_status_during_io__19() {
  check_thread_status_during_io(__state_flag_19, __thread_19);
}

void __init_thread_info_19(thread_info *info) {
  __state_flag_19 = info->get_state_flag();
}

thread_info *__get_thread_info_19() {
  if (__thread_19 != NULL) return __thread_19;
  __thread_19 = new thread_info(19, check_status_during_io__19);
  __init_thread_info_19(__thread_19);
  return __thread_19;
}

void __declare_sockets_19() {
  init_instance::add_incoming(1,19, DATA_SOCKET);
  init_instance::add_outgoing(19,3, DATA_SOCKET);
}

void __init_sockets_19(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_19() {
}

void __peek_sockets_19() {
}

 
void init_AnonFilter_a0__163_26__19();
inline void check_status__19();

void work_AnonFilter_a0__163_26__19(int);


inline float __pop_1_19() {
float res=BUFFER_1_19[TAIL_1_19];
TAIL_1_19++;
return res;
}

inline void __pop_1_19(int n) {
TAIL_1_19+=n;
}

inline float __peek_1_19(int offs) {
return BUFFER_1_19[TAIL_1_19+offs];
}



inline void __push_19_3(float data) {
BUFFER_19_3[HEAD_19_3]=data;
HEAD_19_3++;
}



 
void init_AnonFilter_a0__163_26__19(){
}
void save_file_pointer__19(object_write_buffer *buf) {}
void load_file_pointer__19(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__163_26__19(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__156 = 0.0f;/* float */
      float __tmp2__157 = 0.0f;/* float */
      float __tmp3__158 = 0.0f;/* float */
      float __tmp4__159 = 0.0f;/* float */
      int __tmp5__160 = 0;/* int */
      int i__conflict__0__161 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__156 = ((float)0.0))/*float*/;
      for ((i__conflict__0__161 = 0)/*int*/; (i__conflict__0__161 < 113); (i__conflict__0__161++)) {{
          __tmp3__158 = BUFFER_1_19[TAIL_1_19+i__conflict__0__161];
;
          (__tmp5__160 = (i__conflict__0__161 + 15))/*int*/;
          __tmp4__159 = BUFFER_1_19[TAIL_1_19+__tmp5__160];
;
          (__tmp2__157 = (__tmp3__158 * __tmp4__159))/*float*/;
          (sum__156 = (sum__156 + __tmp2__157))/*float*/;
        }
      }
      __pop_1_19(128);
      __push_19_3(sum__156);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_2;
int __counter_2 = 0;
int __steady_2 = 0;
int __tmp_2 = 0;
int __tmp2_2 = 0;
int *__state_flag_2 = NULL;
thread_info *__thread_2 = NULL;



void save_peek_buffer__2(object_write_buffer *buf);
void load_peek_buffer__2(object_write_buffer *buf);
void save_file_pointer__2(object_write_buffer *buf);
void load_file_pointer__2(object_write_buffer *buf);

inline void check_status__2() {
  check_thread_status(__state_flag_2, __thread_2);
}

void check_status_during_io__2() {
  check_thread_status_during_io(__state_flag_2, __thread_2);
}

void __init_thread_info_2(thread_info *info) {
  __state_flag_2 = info->get_state_flag();
}

thread_info *__get_thread_info_2() {
  if (__thread_2 != NULL) return __thread_2;
  __thread_2 = new thread_info(2, check_status_during_io__2);
  __init_thread_info_2(__thread_2);
  return __thread_2;
}

void __declare_sockets_2() {
  init_instance::add_incoming(1,2, DATA_SOCKET);
  init_instance::add_outgoing(2,3, DATA_SOCKET);
}

void __init_sockets_2(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_2() {
}

void __peek_sockets_2() {
}

 
void init_AnonFilter_a0__13_11__2();
inline void check_status__2();

void work_AnonFilter_a0__13_11__2(int);


inline float __pop_1_2() {
float res=BUFFER_1_2[TAIL_1_2];
TAIL_1_2++;
return res;
}

inline void __pop_1_2(int n) {
TAIL_1_2+=n;
}

inline float __peek_1_2(int offs) {
return BUFFER_1_2[TAIL_1_2+offs];
}



inline void __push_2_3(float data) {
BUFFER_2_3[HEAD_2_3]=data;
HEAD_2_3++;
}



 
void init_AnonFilter_a0__13_11__2(){
}
void save_file_pointer__2(object_write_buffer *buf) {}
void load_file_pointer__2(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__13_11__2(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__6 = 0.0f;/* float */
      float __tmp2__7 = 0.0f;/* float */
      float __tmp3__8 = 0.0f;/* float */
      float __tmp4__9 = 0.0f;/* float */
      int __tmp5__10 = 0;/* int */
      int i__conflict__0__11 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__6 = ((float)0.0))/*float*/;
      for ((i__conflict__0__11 = 0)/*int*/; (i__conflict__0__11 < 128); (i__conflict__0__11++)) {{
          __tmp3__8 = BUFFER_1_2[TAIL_1_2+i__conflict__0__11];
;
          (__tmp5__10 = (i__conflict__0__11 + 0))/*int*/;
          __tmp4__9 = BUFFER_1_2[TAIL_1_2+__tmp5__10];
;
          (__tmp2__7 = (__tmp3__8 * __tmp4__9))/*float*/;
          (sum__6 = (sum__6 + __tmp2__7))/*float*/;
        }
      }
      __pop_1_2(128);
      __push_2_3(sum__6);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_3;
int __counter_3 = 0;
int __steady_3 = 0;
int __tmp_3 = 0;
int __tmp2_3 = 0;
int *__state_flag_3 = NULL;
thread_info *__thread_3 = NULL;



inline void check_status__3() {
  check_thread_status(__state_flag_3, __thread_3);
}

void check_status_during_io__3() {
  check_thread_status_during_io(__state_flag_3, __thread_3);
}

void __init_thread_info_3(thread_info *info) {
  __state_flag_3 = info->get_state_flag();
}

thread_info *__get_thread_info_3() {
  if (__thread_3 != NULL) return __thread_3;
  __thread_3 = new thread_info(3, check_status_during_io__3);
  __init_thread_info_3(__thread_3);
  return __thread_3;
}

void __declare_sockets_3() {
  init_instance::add_incoming(2,3, DATA_SOCKET);
  init_instance::add_incoming(5,3, DATA_SOCKET);
  init_instance::add_incoming(6,3, DATA_SOCKET);
  init_instance::add_incoming(7,3, DATA_SOCKET);
  init_instance::add_incoming(8,3, DATA_SOCKET);
  init_instance::add_incoming(9,3, DATA_SOCKET);
  init_instance::add_incoming(10,3, DATA_SOCKET);
  init_instance::add_incoming(11,3, DATA_SOCKET);
  init_instance::add_incoming(12,3, DATA_SOCKET);
  init_instance::add_incoming(13,3, DATA_SOCKET);
  init_instance::add_incoming(14,3, DATA_SOCKET);
  init_instance::add_incoming(15,3, DATA_SOCKET);
  init_instance::add_incoming(16,3, DATA_SOCKET);
  init_instance::add_incoming(17,3, DATA_SOCKET);
  init_instance::add_incoming(18,3, DATA_SOCKET);
  init_instance::add_incoming(19,3, DATA_SOCKET);
  init_instance::add_outgoing(3,4, DATA_SOCKET);
}

void __init_sockets_3(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_3() {
}

void __peek_sockets_3() {
}


inline void __push_3_4(float data) {
BUFFER_3_4[HEAD_3_4]=data;
HEAD_3_4++;
}


inline float __pop_2_3() {
float res=BUFFER_2_3[TAIL_2_3];
TAIL_2_3++;
return res;
}

inline float __pop_5_3() {
float res=BUFFER_5_3[TAIL_5_3];
TAIL_5_3++;
return res;
}

inline float __pop_6_3() {
float res=BUFFER_6_3[TAIL_6_3];
TAIL_6_3++;
return res;
}

inline float __pop_7_3() {
float res=BUFFER_7_3[TAIL_7_3];
TAIL_7_3++;
return res;
}

inline float __pop_8_3() {
float res=BUFFER_8_3[TAIL_8_3];
TAIL_8_3++;
return res;
}

inline float __pop_9_3() {
float res=BUFFER_9_3[TAIL_9_3];
TAIL_9_3++;
return res;
}

inline float __pop_10_3() {
float res=BUFFER_10_3[TAIL_10_3];
TAIL_10_3++;
return res;
}

inline float __pop_11_3() {
float res=BUFFER_11_3[TAIL_11_3];
TAIL_11_3++;
return res;
}

inline float __pop_12_3() {
float res=BUFFER_12_3[TAIL_12_3];
TAIL_12_3++;
return res;
}

inline float __pop_13_3() {
float res=BUFFER_13_3[TAIL_13_3];
TAIL_13_3++;
return res;
}

inline float __pop_14_3() {
float res=BUFFER_14_3[TAIL_14_3];
TAIL_14_3++;
return res;
}

inline float __pop_15_3() {
float res=BUFFER_15_3[TAIL_15_3];
TAIL_15_3++;
return res;
}

inline float __pop_16_3() {
float res=BUFFER_16_3[TAIL_16_3];
TAIL_16_3++;
return res;
}

inline float __pop_17_3() {
float res=BUFFER_17_3[TAIL_17_3];
TAIL_17_3++;
return res;
}

inline float __pop_18_3() {
float res=BUFFER_18_3[TAIL_18_3];
TAIL_18_3++;
return res;
}

inline float __pop_19_3() {
float res=BUFFER_19_3[TAIL_19_3];
TAIL_19_3++;
return res;
}


void __joiner_3_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_3_4(__pop_2_3());
    __push_3_4(__pop_5_3());
    __push_3_4(__pop_6_3());
    __push_3_4(__pop_7_3());
    __push_3_4(__pop_8_3());
    __push_3_4(__pop_9_3());
    __push_3_4(__pop_10_3());
    __push_3_4(__pop_11_3());
    __push_3_4(__pop_12_3());
    __push_3_4(__pop_13_3());
    __push_3_4(__pop_14_3());
    __push_3_4(__pop_15_3());
    __push_3_4(__pop_16_3());
    __push_3_4(__pop_17_3());
    __push_3_4(__pop_18_3());
    __push_3_4(__pop_19_3());
  }
}


// peek: 1 pop: 1 push 0
// init counts: 0 steady counts: 16

// ClusterFusion isEliminated: false



int __number_of_iterations_4;
int __counter_4 = 0;
int __steady_4 = 0;
int __tmp_4 = 0;
int __tmp2_4 = 0;
int *__state_flag_4 = NULL;
thread_info *__thread_4 = NULL;



void save_peek_buffer__4(object_write_buffer *buf);
void load_peek_buffer__4(object_write_buffer *buf);
void save_file_pointer__4(object_write_buffer *buf);
void load_file_pointer__4(object_write_buffer *buf);

inline void check_status__4() {
  check_thread_status(__state_flag_4, __thread_4);
}

void check_status_during_io__4() {
  check_thread_status_during_io(__state_flag_4, __thread_4);
}

void __init_thread_info_4(thread_info *info) {
  __state_flag_4 = info->get_state_flag();
}

thread_info *__get_thread_info_4() {
  if (__thread_4 != NULL) return __thread_4;
  __thread_4 = new thread_info(4, check_status_during_io__4);
  __init_thread_info_4(__thread_4);
  return __thread_4;
}

void __declare_sockets_4() {
  init_instance::add_incoming(3,4, DATA_SOCKET);
}

void __init_sockets_4(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_4() {
}

void __peek_sockets_4() {
}

 
void init_FloatPrinter__167_27__4();
inline void check_status__4();

void work_FloatPrinter__167_27__4(int);


inline float __pop_3_4() {
float res=BUFFER_3_4[TAIL_3_4];
TAIL_3_4++;
return res;
}

inline void __pop_3_4(int n) {
TAIL_3_4+=n;
}

inline float __peek_3_4(int offs) {
return BUFFER_3_4[TAIL_3_4+offs];
}


 
void init_FloatPrinter__167_27__4(){
}
void save_file_pointer__4(object_write_buffer *buf) {}
void load_file_pointer__4(object_write_buffer *buf) {}
 
void work_FloatPrinter__167_27__4(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__166 = 0.0f;/* float */

      // mark begin: SIRFilter FloatPrinter

      v__166 = BUFFER_3_4[TAIL_3_4]; TAIL_3_4++;
;

      // TIMER_PRINT_CODE: __print_sink__ += (int)(v__166); 

      // mark end: SIRFilter FloatPrinter

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_5;
int __counter_5 = 0;
int __steady_5 = 0;
int __tmp_5 = 0;
int __tmp2_5 = 0;
int *__state_flag_5 = NULL;
thread_info *__thread_5 = NULL;



void save_peek_buffer__5(object_write_buffer *buf);
void load_peek_buffer__5(object_write_buffer *buf);
void save_file_pointer__5(object_write_buffer *buf);
void load_file_pointer__5(object_write_buffer *buf);

inline void check_status__5() {
  check_thread_status(__state_flag_5, __thread_5);
}

void check_status_during_io__5() {
  check_thread_status_during_io(__state_flag_5, __thread_5);
}

void __init_thread_info_5(thread_info *info) {
  __state_flag_5 = info->get_state_flag();
}

thread_info *__get_thread_info_5() {
  if (__thread_5 != NULL) return __thread_5;
  __thread_5 = new thread_info(5, check_status_during_io__5);
  __init_thread_info_5(__thread_5);
  return __thread_5;
}

void __declare_sockets_5() {
  init_instance::add_incoming(1,5, DATA_SOCKET);
  init_instance::add_outgoing(5,3, DATA_SOCKET);
}

void __init_sockets_5(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_5() {
}

void __peek_sockets_5() {
}

 
void init_AnonFilter_a0__23_12__5();
inline void check_status__5();

void work_AnonFilter_a0__23_12__5(int);


inline float __pop_1_5() {
float res=BUFFER_1_5[TAIL_1_5];
TAIL_1_5++;
return res;
}

inline void __pop_1_5(int n) {
TAIL_1_5+=n;
}

inline float __peek_1_5(int offs) {
return BUFFER_1_5[TAIL_1_5+offs];
}



inline void __push_5_3(float data) {
BUFFER_5_3[HEAD_5_3]=data;
HEAD_5_3++;
}



 
void init_AnonFilter_a0__23_12__5(){
}
void save_file_pointer__5(object_write_buffer *buf) {}
void load_file_pointer__5(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__23_12__5(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__16 = 0.0f;/* float */
      float __tmp2__17 = 0.0f;/* float */
      float __tmp3__18 = 0.0f;/* float */
      float __tmp4__19 = 0.0f;/* float */
      int __tmp5__20 = 0;/* int */
      int i__conflict__0__21 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__16 = ((float)0.0))/*float*/;
      for ((i__conflict__0__21 = 0)/*int*/; (i__conflict__0__21 < 127); (i__conflict__0__21++)) {{
          __tmp3__18 = BUFFER_1_5[TAIL_1_5+i__conflict__0__21];
;
          (__tmp5__20 = (i__conflict__0__21 + 1))/*int*/;
          __tmp4__19 = BUFFER_1_5[TAIL_1_5+__tmp5__20];
;
          (__tmp2__17 = (__tmp3__18 * __tmp4__19))/*float*/;
          (sum__16 = (sum__16 + __tmp2__17))/*float*/;
        }
      }
      __pop_1_5(128);
      __push_5_3(sum__16);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_6;
int __counter_6 = 0;
int __steady_6 = 0;
int __tmp_6 = 0;
int __tmp2_6 = 0;
int *__state_flag_6 = NULL;
thread_info *__thread_6 = NULL;



void save_peek_buffer__6(object_write_buffer *buf);
void load_peek_buffer__6(object_write_buffer *buf);
void save_file_pointer__6(object_write_buffer *buf);
void load_file_pointer__6(object_write_buffer *buf);

inline void check_status__6() {
  check_thread_status(__state_flag_6, __thread_6);
}

void check_status_during_io__6() {
  check_thread_status_during_io(__state_flag_6, __thread_6);
}

void __init_thread_info_6(thread_info *info) {
  __state_flag_6 = info->get_state_flag();
}

thread_info *__get_thread_info_6() {
  if (__thread_6 != NULL) return __thread_6;
  __thread_6 = new thread_info(6, check_status_during_io__6);
  __init_thread_info_6(__thread_6);
  return __thread_6;
}

void __declare_sockets_6() {
  init_instance::add_incoming(1,6, DATA_SOCKET);
  init_instance::add_outgoing(6,3, DATA_SOCKET);
}

void __init_sockets_6(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_6() {
}

void __peek_sockets_6() {
}

 
void init_AnonFilter_a0__33_13__6();
inline void check_status__6();

void work_AnonFilter_a0__33_13__6(int);


inline float __pop_1_6() {
float res=BUFFER_1_6[TAIL_1_6];
TAIL_1_6++;
return res;
}

inline void __pop_1_6(int n) {
TAIL_1_6+=n;
}

inline float __peek_1_6(int offs) {
return BUFFER_1_6[TAIL_1_6+offs];
}



inline void __push_6_3(float data) {
BUFFER_6_3[HEAD_6_3]=data;
HEAD_6_3++;
}



 
void init_AnonFilter_a0__33_13__6(){
}
void save_file_pointer__6(object_write_buffer *buf) {}
void load_file_pointer__6(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__33_13__6(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__26 = 0.0f;/* float */
      float __tmp2__27 = 0.0f;/* float */
      float __tmp3__28 = 0.0f;/* float */
      float __tmp4__29 = 0.0f;/* float */
      int __tmp5__30 = 0;/* int */
      int i__conflict__0__31 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__26 = ((float)0.0))/*float*/;
      for ((i__conflict__0__31 = 0)/*int*/; (i__conflict__0__31 < 126); (i__conflict__0__31++)) {{
          __tmp3__28 = BUFFER_1_6[TAIL_1_6+i__conflict__0__31];
;
          (__tmp5__30 = (i__conflict__0__31 + 2))/*int*/;
          __tmp4__29 = BUFFER_1_6[TAIL_1_6+__tmp5__30];
;
          (__tmp2__27 = (__tmp3__28 * __tmp4__29))/*float*/;
          (sum__26 = (sum__26 + __tmp2__27))/*float*/;
        }
      }
      __pop_1_6(128);
      __push_6_3(sum__26);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_7;
int __counter_7 = 0;
int __steady_7 = 0;
int __tmp_7 = 0;
int __tmp2_7 = 0;
int *__state_flag_7 = NULL;
thread_info *__thread_7 = NULL;



void save_peek_buffer__7(object_write_buffer *buf);
void load_peek_buffer__7(object_write_buffer *buf);
void save_file_pointer__7(object_write_buffer *buf);
void load_file_pointer__7(object_write_buffer *buf);

inline void check_status__7() {
  check_thread_status(__state_flag_7, __thread_7);
}

void check_status_during_io__7() {
  check_thread_status_during_io(__state_flag_7, __thread_7);
}

void __init_thread_info_7(thread_info *info) {
  __state_flag_7 = info->get_state_flag();
}

thread_info *__get_thread_info_7() {
  if (__thread_7 != NULL) return __thread_7;
  __thread_7 = new thread_info(7, check_status_during_io__7);
  __init_thread_info_7(__thread_7);
  return __thread_7;
}

void __declare_sockets_7() {
  init_instance::add_incoming(1,7, DATA_SOCKET);
  init_instance::add_outgoing(7,3, DATA_SOCKET);
}

void __init_sockets_7(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_7() {
}

void __peek_sockets_7() {
}

 
void init_AnonFilter_a0__43_14__7();
inline void check_status__7();

void work_AnonFilter_a0__43_14__7(int);


inline float __pop_1_7() {
float res=BUFFER_1_7[TAIL_1_7];
TAIL_1_7++;
return res;
}

inline void __pop_1_7(int n) {
TAIL_1_7+=n;
}

inline float __peek_1_7(int offs) {
return BUFFER_1_7[TAIL_1_7+offs];
}



inline void __push_7_3(float data) {
BUFFER_7_3[HEAD_7_3]=data;
HEAD_7_3++;
}



 
void init_AnonFilter_a0__43_14__7(){
}
void save_file_pointer__7(object_write_buffer *buf) {}
void load_file_pointer__7(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__43_14__7(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__36 = 0.0f;/* float */
      float __tmp2__37 = 0.0f;/* float */
      float __tmp3__38 = 0.0f;/* float */
      float __tmp4__39 = 0.0f;/* float */
      int __tmp5__40 = 0;/* int */
      int i__conflict__0__41 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__36 = ((float)0.0))/*float*/;
      for ((i__conflict__0__41 = 0)/*int*/; (i__conflict__0__41 < 125); (i__conflict__0__41++)) {{
          __tmp3__38 = BUFFER_1_7[TAIL_1_7+i__conflict__0__41];
;
          (__tmp5__40 = (i__conflict__0__41 + 3))/*int*/;
          __tmp4__39 = BUFFER_1_7[TAIL_1_7+__tmp5__40];
;
          (__tmp2__37 = (__tmp3__38 * __tmp4__39))/*float*/;
          (sum__36 = (sum__36 + __tmp2__37))/*float*/;
        }
      }
      __pop_1_7(128);
      __push_7_3(sum__36);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_8;
int __counter_8 = 0;
int __steady_8 = 0;
int __tmp_8 = 0;
int __tmp2_8 = 0;
int *__state_flag_8 = NULL;
thread_info *__thread_8 = NULL;



void save_peek_buffer__8(object_write_buffer *buf);
void load_peek_buffer__8(object_write_buffer *buf);
void save_file_pointer__8(object_write_buffer *buf);
void load_file_pointer__8(object_write_buffer *buf);

inline void check_status__8() {
  check_thread_status(__state_flag_8, __thread_8);
}

void check_status_during_io__8() {
  check_thread_status_during_io(__state_flag_8, __thread_8);
}

void __init_thread_info_8(thread_info *info) {
  __state_flag_8 = info->get_state_flag();
}

thread_info *__get_thread_info_8() {
  if (__thread_8 != NULL) return __thread_8;
  __thread_8 = new thread_info(8, check_status_during_io__8);
  __init_thread_info_8(__thread_8);
  return __thread_8;
}

void __declare_sockets_8() {
  init_instance::add_incoming(1,8, DATA_SOCKET);
  init_instance::add_outgoing(8,3, DATA_SOCKET);
}

void __init_sockets_8(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_8() {
}

void __peek_sockets_8() {
}

 
void init_AnonFilter_a0__53_15__8();
inline void check_status__8();

void work_AnonFilter_a0__53_15__8(int);


inline float __pop_1_8() {
float res=BUFFER_1_8[TAIL_1_8];
TAIL_1_8++;
return res;
}

inline void __pop_1_8(int n) {
TAIL_1_8+=n;
}

inline float __peek_1_8(int offs) {
return BUFFER_1_8[TAIL_1_8+offs];
}



inline void __push_8_3(float data) {
BUFFER_8_3[HEAD_8_3]=data;
HEAD_8_3++;
}



 
void init_AnonFilter_a0__53_15__8(){
}
void save_file_pointer__8(object_write_buffer *buf) {}
void load_file_pointer__8(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__53_15__8(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__46 = 0.0f;/* float */
      float __tmp2__47 = 0.0f;/* float */
      float __tmp3__48 = 0.0f;/* float */
      float __tmp4__49 = 0.0f;/* float */
      int __tmp5__50 = 0;/* int */
      int i__conflict__0__51 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__46 = ((float)0.0))/*float*/;
      for ((i__conflict__0__51 = 0)/*int*/; (i__conflict__0__51 < 124); (i__conflict__0__51++)) {{
          __tmp3__48 = BUFFER_1_8[TAIL_1_8+i__conflict__0__51];
;
          (__tmp5__50 = (i__conflict__0__51 + 4))/*int*/;
          __tmp4__49 = BUFFER_1_8[TAIL_1_8+__tmp5__50];
;
          (__tmp2__47 = (__tmp3__48 * __tmp4__49))/*float*/;
          (sum__46 = (sum__46 + __tmp2__47))/*float*/;
        }
      }
      __pop_1_8(128);
      __push_8_3(sum__46);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 128 pop: 128 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_9;
int __counter_9 = 0;
int __steady_9 = 0;
int __tmp_9 = 0;
int __tmp2_9 = 0;
int *__state_flag_9 = NULL;
thread_info *__thread_9 = NULL;



void save_peek_buffer__9(object_write_buffer *buf);
void load_peek_buffer__9(object_write_buffer *buf);
void save_file_pointer__9(object_write_buffer *buf);
void load_file_pointer__9(object_write_buffer *buf);

inline void check_status__9() {
  check_thread_status(__state_flag_9, __thread_9);
}

void check_status_during_io__9() {
  check_thread_status_during_io(__state_flag_9, __thread_9);
}

void __init_thread_info_9(thread_info *info) {
  __state_flag_9 = info->get_state_flag();
}

thread_info *__get_thread_info_9() {
  if (__thread_9 != NULL) return __thread_9;
  __thread_9 = new thread_info(9, check_status_during_io__9);
  __init_thread_info_9(__thread_9);
  return __thread_9;
}

void __declare_sockets_9() {
  init_instance::add_incoming(1,9, DATA_SOCKET);
  init_instance::add_outgoing(9,3, DATA_SOCKET);
}

void __init_sockets_9(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_9() {
}

void __peek_sockets_9() {
}

 
void init_AnonFilter_a0__63_16__9();
inline void check_status__9();

void work_AnonFilter_a0__63_16__9(int);


inline float __pop_1_9() {
float res=BUFFER_1_9[TAIL_1_9];
TAIL_1_9++;
return res;
}

inline void __pop_1_9(int n) {
TAIL_1_9+=n;
}

inline float __peek_1_9(int offs) {
return BUFFER_1_9[TAIL_1_9+offs];
}



inline void __push_9_3(float data) {
BUFFER_9_3[HEAD_9_3]=data;
HEAD_9_3++;
}



 
void init_AnonFilter_a0__63_16__9(){
}
void save_file_pointer__9(object_write_buffer *buf) {}
void load_file_pointer__9(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__63_16__9(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__56 = 0.0f;/* float */
      float __tmp2__57 = 0.0f;/* float */
      float __tmp3__58 = 0.0f;/* float */
      float __tmp4__59 = 0.0f;/* float */
      int __tmp5__60 = 0;/* int */
      int i__conflict__0__61 = 0;/* int */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__56 = ((float)0.0))/*float*/;
      for ((i__conflict__0__61 = 0)/*int*/; (i__conflict__0__61 < 123); (i__conflict__0__61++)) {{
          __tmp3__58 = BUFFER_1_9[TAIL_1_9+i__conflict__0__61];
;
          (__tmp5__60 = (i__conflict__0__61 + 5))/*int*/;
          __tmp4__59 = BUFFER_1_9[TAIL_1_9+__tmp5__60];
;
          (__tmp2__57 = (__tmp3__58 * __tmp4__59))/*float*/;
          (sum__56 = (sum__56 + __tmp2__57))/*float*/;
        }
      }
      __pop_1_9(128);
      __push_9_3(sum__56);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

