#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <netsocket.h>
#include <node_server.h>
#include <init_instance.h>
#include <master_server.h>
#include <save_state.h>
#include <save_manager.h>
#include <delete_chkpts.h>
#include <object_write_buffer.h>
#include <read_setup.h>
#include <ccp.h>
#include <streamit_random.h>
#include "structs.h"
#include "fusion.h"

int __max_iteration;
int __timer_enabled = 0;
int __frequency_of_chkpts;
volatile int __vol;


float BUFFER_0_1[__BUF_SIZE_MASK_0_1 + 1];
int HEAD_0_1 = 0;
int TAIL_0_1 = 0;
float BUFFER_1_2[__BUF_SIZE_MASK_1_2 + 1];
int HEAD_1_2 = 0;
int TAIL_1_2 = 0;
float BUFFER_1_17[__BUF_SIZE_MASK_1_17 + 1];
int HEAD_1_17 = 0;
int TAIL_1_17 = 0;
float BUFFER_2_3[__BUF_SIZE_MASK_2_3 + 1];
int HEAD_2_3 = 0;
int TAIL_2_3 = 0;
float BUFFER_3_4[__BUF_SIZE_MASK_3_4 + 1];
int HEAD_3_4 = 0;
int TAIL_3_4 = 0;
float BUFFER_4_5[__BUF_SIZE_MASK_4_5 + 1];
int HEAD_4_5 = 0;
int TAIL_4_5 = 0;
float BUFFER_5_6[__BUF_SIZE_MASK_5_6 + 1];
int HEAD_5_6 = 0;
int TAIL_5_6 = 0;
float BUFFER_6_7[__BUF_SIZE_MASK_6_7 + 1];
int HEAD_6_7 = 0;
int TAIL_6_7 = 0;
float BUFFER_7_8[__BUF_SIZE_MASK_7_8 + 1];
int HEAD_7_8 = 0;
int TAIL_7_8 = 0;
float BUFFER_8_9[__BUF_SIZE_MASK_8_9 + 1];
int HEAD_8_9 = 0;
int TAIL_8_9 = 0;
float BUFFER_9_10[__BUF_SIZE_MASK_9_10 + 1];
int HEAD_9_10 = 0;
int TAIL_9_10 = 0;
float BUFFER_10_11[__BUF_SIZE_MASK_10_11 + 1];
int HEAD_10_11 = 0;
int TAIL_10_11 = 0;
float BUFFER_11_12[__BUF_SIZE_MASK_11_12 + 1];
int HEAD_11_12 = 0;
int TAIL_11_12 = 0;
float BUFFER_12_13[__BUF_SIZE_MASK_12_13 + 1];
int HEAD_12_13 = 0;
int TAIL_12_13 = 0;
float BUFFER_13_14[__BUF_SIZE_MASK_13_14 + 1];
int HEAD_13_14 = 0;
int TAIL_13_14 = 0;
float BUFFER_14_15[__BUF_SIZE_MASK_14_15 + 1];
int HEAD_14_15 = 0;
int TAIL_14_15 = 0;
float BUFFER_15_16[__BUF_SIZE_MASK_15_16 + 1];
int HEAD_15_16 = 0;
int TAIL_15_16 = 0;
float BUFFER_17_18[__BUF_SIZE_MASK_17_18 + 1];
int HEAD_17_18 = 0;
int TAIL_17_18 = 0;
float BUFFER_18_19[__BUF_SIZE_MASK_18_19 + 1];
int HEAD_18_19 = 0;
int TAIL_18_19 = 0;
float BUFFER_19_20[__BUF_SIZE_MASK_19_20 + 1];
int HEAD_19_20 = 0;
int TAIL_19_20 = 0;
float BUFFER_20_21[__BUF_SIZE_MASK_20_21 + 1];
int HEAD_20_21 = 0;
int TAIL_20_21 = 0;
float BUFFER_21_22[__BUF_SIZE_MASK_21_22 + 1];
int HEAD_21_22 = 0;
int TAIL_21_22 = 0;
float BUFFER_22_23[__BUF_SIZE_MASK_22_23 + 1];
int HEAD_22_23 = 0;
int TAIL_22_23 = 0;
float BUFFER_23_24[__BUF_SIZE_MASK_23_24 + 1];
int HEAD_23_24 = 0;
int TAIL_23_24 = 0;
float BUFFER_24_25[__BUF_SIZE_MASK_24_25 + 1];
int HEAD_24_25 = 0;
int TAIL_24_25 = 0;
float BUFFER_25_26[__BUF_SIZE_MASK_25_26 + 1];
int HEAD_25_26 = 0;
int TAIL_25_26 = 0;
float BUFFER_26_27[__BUF_SIZE_MASK_26_27 + 1];
int HEAD_26_27 = 0;
int TAIL_26_27 = 0;
float BUFFER_27_28[__BUF_SIZE_MASK_27_28 + 1];
int HEAD_27_28 = 0;
int TAIL_27_28 = 0;
float BUFFER_28_29[__BUF_SIZE_MASK_28_29 + 1];
int HEAD_28_29 = 0;
int TAIL_28_29 = 0;
float BUFFER_29_15[__BUF_SIZE_MASK_29_15 + 1];
int HEAD_29_15 = 0;
int TAIL_29_15 = 0;
void init_FFTTestSource__3_32__0();
void work_FFTTestSource__3_32__0(int);
void __splitter_1_work(int);
void init_FFTReorderSimple__13_33__2();
void work_FFTReorderSimple__13_33__2(int);
void init_FFTReorderSimple__23_34__3();
void work_FFTReorderSimple__23_34__3(int);
void init_FFTReorderSimple__33_35__4();
void work_FFTReorderSimple__33_35__4(int);
void init_FFTReorderSimple__43_36__5();
void work_FFTReorderSimple__43_36__5(int);
void init_FFTReorderSimple__53_37__6();
void work_FFTReorderSimple__53_37__6(int);
void init_FFTReorderSimple__63_38__7();
void work_FFTReorderSimple__63_38__7(int);
void init_CombineDFT__88_39__8();
void work_CombineDFT__88_39__8(int);
void init_CombineDFT__113_40__9();
void work_CombineDFT__113_40__9(int);
void init_CombineDFT__138_41__10();
void work_CombineDFT__138_41__10(int);
void init_CombineDFT__163_42__11();
void work_CombineDFT__163_42__11(int);
void init_CombineDFT__188_43__12();
void work_CombineDFT__188_43__12(int);
void init_CombineDFT__213_44__13();
void work_CombineDFT__213_44__13(int);
void init_CombineDFT__238_45__14();
void work_CombineDFT__238_45__14(int);
void __joiner_15_work(int);
void init_FloatPrinter__478_59__16();
void work_FloatPrinter__478_59__16(int);
void init_FFTReorderSimple__248_46__17();
void work_FFTReorderSimple__248_46__17(int);
void init_FFTReorderSimple__258_47__18();
void work_FFTReorderSimple__258_47__18(int);
void init_FFTReorderSimple__268_48__19();
void work_FFTReorderSimple__268_48__19(int);
void init_FFTReorderSimple__278_49__20();
void work_FFTReorderSimple__278_49__20(int);
void init_FFTReorderSimple__288_50__21();
void work_FFTReorderSimple__288_50__21(int);
void init_FFTReorderSimple__298_51__22();
void work_FFTReorderSimple__298_51__22(int);
void init_CombineDFT__323_52__23();
void work_CombineDFT__323_52__23(int);
void init_CombineDFT__348_53__24();
void work_CombineDFT__348_53__24(int);
void init_CombineDFT__373_54__25();
void work_CombineDFT__373_54__25(int);
void init_CombineDFT__398_55__26();
void work_CombineDFT__398_55__26(int);
void init_CombineDFT__423_56__27();
void work_CombineDFT__423_56__27(int);
void init_CombineDFT__448_57__28();
void work_CombineDFT__448_57__28(int);
void init_CombineDFT__473_58__29();
void work_CombineDFT__473_58__29(int);

int main(int argc, char **argv) {
  read_setup::read_setup_file();
  __max_iteration = read_setup::max_iteration;
  for (int a = 1; a < argc; a++) {
    if (argc > a + 1 && strcmp(argv[a], "-i") == 0) {
      int tmp;
      sscanf(argv[a + 1], "%d", &tmp);
#ifdef VERBOSE
#endif
      __max_iteration = tmp;
    }
    if (strcmp(argv[a], "-t") == 0) {
#ifdef VERBOSE
#endif
      __timer_enabled = 1;
    }
  }
// number of phases: 17


  // ============= Initialization =============

init_FFTTestSource__3_32__0();
init_FFTReorderSimple__13_33__2();
init_FFTReorderSimple__248_46__17();
init_FFTReorderSimple__23_34__3();
init_FFTReorderSimple__258_47__18();
init_FFTReorderSimple__268_48__19();
init_FFTReorderSimple__33_35__4();
init_FFTReorderSimple__278_49__20();
init_FFTReorderSimple__43_36__5();
init_FFTReorderSimple__288_50__21();
init_FFTReorderSimple__53_37__6();
init_FFTReorderSimple__298_51__22();
init_FFTReorderSimple__63_38__7();
init_CombineDFT__323_52__23();
init_CombineDFT__88_39__8();
init_CombineDFT__348_53__24();
init_CombineDFT__113_40__9();
init_CombineDFT__373_54__25();
init_CombineDFT__138_41__10();
init_CombineDFT__398_55__26();
init_CombineDFT__163_42__11();
init_CombineDFT__423_56__27();
init_CombineDFT__188_43__12();
init_CombineDFT__448_57__28();
init_CombineDFT__213_44__13();
init_CombineDFT__473_58__29();
init_CombineDFT__238_45__14();
init_FloatPrinter__478_59__16();

  // ============= Steady State =============

  if (__timer_enabled) {
  }
  for (int n = 0; n < (__max_iteration  ); n++) {
HEAD_0_1 = 0;
TAIL_0_1 = 0;
    work_FFTTestSource__3_32__0(2 );
HEAD_1_2 = 0;
TAIL_1_2 = 0;
HEAD_1_17 = 0;
TAIL_1_17 = 0;
    __splitter_1_work(1 );
HEAD_2_3 = 0;
TAIL_2_3 = 0;
    work_FFTReorderSimple__13_33__2(1 );
HEAD_17_18 = 0;
TAIL_17_18 = 0;
    work_FFTReorderSimple__248_46__17(1 );
HEAD_3_4 = 0;
TAIL_3_4 = 0;
    work_FFTReorderSimple__23_34__3(2 );
HEAD_18_19 = 0;
TAIL_18_19 = 0;
    work_FFTReorderSimple__258_47__18(2 );
HEAD_19_20 = 0;
TAIL_19_20 = 0;
    work_FFTReorderSimple__268_48__19(4 );
HEAD_4_5 = 0;
TAIL_4_5 = 0;
    work_FFTReorderSimple__33_35__4(4 );
HEAD_20_21 = 0;
TAIL_20_21 = 0;
    work_FFTReorderSimple__278_49__20(8 );
HEAD_5_6 = 0;
TAIL_5_6 = 0;
    work_FFTReorderSimple__43_36__5(8 );
HEAD_21_22 = 0;
TAIL_21_22 = 0;
    work_FFTReorderSimple__288_50__21(16 );
HEAD_6_7 = 0;
TAIL_6_7 = 0;
    work_FFTReorderSimple__53_37__6(16 );
HEAD_22_23 = 0;
TAIL_22_23 = 0;
    work_FFTReorderSimple__298_51__22(32 );
HEAD_7_8 = 0;
TAIL_7_8 = 0;
    work_FFTReorderSimple__63_38__7(32 );
HEAD_23_24 = 0;
TAIL_23_24 = 0;
    work_CombineDFT__323_52__23(64 );
HEAD_8_9 = 0;
TAIL_8_9 = 0;
    work_CombineDFT__88_39__8(64 );
HEAD_24_25 = 0;
TAIL_24_25 = 0;
    work_CombineDFT__348_53__24(32 );
HEAD_9_10 = 0;
TAIL_9_10 = 0;
    work_CombineDFT__113_40__9(32 );
HEAD_25_26 = 0;
TAIL_25_26 = 0;
    work_CombineDFT__373_54__25(16 );
HEAD_10_11 = 0;
TAIL_10_11 = 0;
    work_CombineDFT__138_41__10(16 );
HEAD_26_27 = 0;
TAIL_26_27 = 0;
    work_CombineDFT__398_55__26(8 );
HEAD_11_12 = 0;
TAIL_11_12 = 0;
    work_CombineDFT__163_42__11(8 );
HEAD_27_28 = 0;
TAIL_27_28 = 0;
    work_CombineDFT__423_56__27(4 );
HEAD_12_13 = 0;
TAIL_12_13 = 0;
    work_CombineDFT__188_43__12(4 );
HEAD_28_29 = 0;
TAIL_28_29 = 0;
    work_CombineDFT__448_57__28(2 );
HEAD_13_14 = 0;
TAIL_13_14 = 0;
    work_CombineDFT__213_44__13(2 );
HEAD_29_15 = 0;
TAIL_29_15 = 0;
    work_CombineDFT__473_58__29(1 );
HEAD_14_15 = 0;
TAIL_14_15 = 0;
    work_CombineDFT__238_45__14(1 );
HEAD_15_16 = 0;
TAIL_15_16 = 0;
    __joiner_15_work(1 );
    work_FloatPrinter__478_59__16(512 );
  }
if (__timer_enabled) {
  }


  return 0;
}

// moved or inserted by concat_cluster_threads.pl
#include <message.h>
message *__msg_stack_13;
message *__msg_stack_29;
message *__msg_stack_22;
message *__msg_stack_0;
message *__msg_stack_26;
message *__msg_stack_5;
message *__msg_stack_19;
message *__msg_stack_28;
message *__msg_stack_27;
message *__msg_stack_15;
message *__msg_stack_21;
message *__msg_stack_14;
message *__msg_stack_1;
message *__msg_stack_17;
message *__msg_stack_12;
message *__msg_stack_10;
message *__msg_stack_7;
message *__msg_stack_6;
message *__msg_stack_23;
message *__msg_stack_20;
message *__msg_stack_9;
message *__msg_stack_2;
message *__msg_stack_4;
message *__msg_stack_8;
message *__msg_stack_3;
message *__msg_stack_11;
message *__msg_stack_16;
message *__msg_stack_25;
message *__msg_stack_18;
message *__msg_stack_24;

// end of moved or inserted by concat_cluster_threads.pl

// peek: 0 pop: 0 push 256
// init counts: 0 steady counts: 2

// ClusterFusion isEliminated: false


#include <mysocket.h>
#include <sdep.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "global.h"

int __number_of_iterations_0;
int __counter_0 = 0;
int __steady_0 = 0;
int __tmp_0 = 0;
int __tmp2_0 = 0;
int *__state_flag_0 = NULL;
thread_info *__thread_0 = NULL;



void save_file_pointer__0(object_write_buffer *buf);
void load_file_pointer__0(object_write_buffer *buf);

inline void check_status__0() {
  check_thread_status(__state_flag_0, __thread_0);
}

void check_status_during_io__0() {
  check_thread_status_during_io(__state_flag_0, __thread_0);
}

void __init_thread_info_0(thread_info *info) {
  __state_flag_0 = info->get_state_flag();
}

thread_info *__get_thread_info_0() {
  if (__thread_0 != NULL) return __thread_0;
  __thread_0 = new thread_info(0, check_status_during_io__0);
  __init_thread_info_0(__thread_0);
  return __thread_0;
}

void __declare_sockets_0() {
  init_instance::add_outgoing(0,1, DATA_SOCKET);
}

void __init_sockets_0(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_0() {
}

void __peek_sockets_0() {
}

 
void init_FFTTestSource__3_32__0();
inline void check_status__0();

void work_FFTTestSource__3_32__0(int);



inline void __push_0_1(float data) {
BUFFER_0_1[HEAD_0_1]=data;
HEAD_0_1++;
}



 
void init_FFTTestSource__3_32__0(){
}
void save_file_pointer__0(object_write_buffer *buf) {}
void load_file_pointer__0(object_write_buffer *buf) {}
 
void work_FFTTestSource__3_32__0(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__2 = 0;/* int */

      // mark begin: SIRFilter FFTTestSource

      __push_0_1(((float)0.0));
      __push_0_1(((float)0.0));
      __push_0_1(((float)1.0));
      __push_0_1(((float)0.0));
      for ((i__2 = 0)/*int*/; (i__2 < 252); (i__2++)) {__push_0_1(((float)0.0));
      }
      // mark end: SIRFilter FFTTestSource

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_1;
int __counter_1 = 0;
int __steady_1 = 0;
int __tmp_1 = 0;
int __tmp2_1 = 0;
int *__state_flag_1 = NULL;
thread_info *__thread_1 = NULL;



inline void check_status__1() {
  check_thread_status(__state_flag_1, __thread_1);
}

void check_status_during_io__1() {
  check_thread_status_during_io(__state_flag_1, __thread_1);
}

void __init_thread_info_1(thread_info *info) {
  __state_flag_1 = info->get_state_flag();
}

thread_info *__get_thread_info_1() {
  if (__thread_1 != NULL) return __thread_1;
  __thread_1 = new thread_info(1, check_status_during_io__1);
  __init_thread_info_1(__thread_1);
  return __thread_1;
}

void __declare_sockets_1() {
  init_instance::add_incoming(0,1, DATA_SOCKET);
  init_instance::add_outgoing(1,2, DATA_SOCKET);
  init_instance::add_outgoing(1,17, DATA_SOCKET);
}

void __init_sockets_1(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_1() {
}

void __peek_sockets_1() {
}

inline float __pop_0_1() {
float res=BUFFER_0_1[TAIL_0_1];
TAIL_0_1++;
return res;
}


inline void __push_1_2(float data) {
BUFFER_1_2[HEAD_1_2]=data;
HEAD_1_2++;
}



inline void __push_1_17(float data) {
BUFFER_1_17[HEAD_1_17]=data;
HEAD_1_17++;
}



void __splitter_1_work(int ____n) {
  for (;____n > 0; ____n--) {
  for (int __k = 0; __k < 32; __k++) {
__push_1_2(__pop_0_1());
__push_1_2(__pop_0_1());
__push_1_2(__pop_0_1());
__push_1_2(__pop_0_1());
__push_1_2(__pop_0_1());
__push_1_2(__pop_0_1());
__push_1_2(__pop_0_1());
__push_1_2(__pop_0_1());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_1_17(__pop_0_1());
__push_1_17(__pop_0_1());
__push_1_17(__pop_0_1());
__push_1_17(__pop_0_1());
__push_1_17(__pop_0_1());
__push_1_17(__pop_0_1());
__push_1_17(__pop_0_1());
__push_1_17(__pop_0_1());
  }
  }
}


// peek: 16 pop: 16 push 16
// init counts: 0 steady counts: 16

// ClusterFusion isEliminated: false



int __number_of_iterations_10;
int __counter_10 = 0;
int __steady_10 = 0;
int __tmp_10 = 0;
int __tmp2_10 = 0;
int *__state_flag_10 = NULL;
thread_info *__thread_10 = NULL;



float w__114__10[8] = {0};
void save_peek_buffer__10(object_write_buffer *buf);
void load_peek_buffer__10(object_write_buffer *buf);
void save_file_pointer__10(object_write_buffer *buf);
void load_file_pointer__10(object_write_buffer *buf);

inline void check_status__10() {
  check_thread_status(__state_flag_10, __thread_10);
}

void check_status_during_io__10() {
  check_thread_status_during_io(__state_flag_10, __thread_10);
}

void __init_thread_info_10(thread_info *info) {
  __state_flag_10 = info->get_state_flag();
}

thread_info *__get_thread_info_10() {
  if (__thread_10 != NULL) return __thread_10;
  __thread_10 = new thread_info(10, check_status_during_io__10);
  __init_thread_info_10(__thread_10);
  return __thread_10;
}

void __declare_sockets_10() {
  init_instance::add_incoming(9,10, DATA_SOCKET);
  init_instance::add_outgoing(10,11, DATA_SOCKET);
}

void __init_sockets_10(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_10() {
}

void __peek_sockets_10() {
}

 
void init_CombineDFT__138_41__10();
inline void check_status__10();

void work_CombineDFT__138_41__10(int);


inline float __pop_9_10() {
float res=BUFFER_9_10[TAIL_9_10];
TAIL_9_10++;
return res;
}

inline void __pop_9_10(int n) {
TAIL_9_10+=n;
}

inline float __peek_9_10(int offs) {
return BUFFER_9_10[TAIL_9_10+offs];
}



inline void __push_10_11(float data) {
BUFFER_10_11[HEAD_10_11]=data;
HEAD_10_11++;
}



 
void init_CombineDFT__138_41__10(){
  float real__133 = 0.0f;/* float */
  float imag__134 = 0.0f;/* float */
  float next_real__135 = 0.0f;/* float */float next_imag__136 = 0.0f;/* float */
  int i__137 = 0;/* int */

  (real__133 = ((float)1.0))/*float*/;
  (imag__134 = ((float)0.0))/*float*/;
  for ((i__137 = 0)/*int*/; (i__137 < 8); (i__137 = (i__137 + 2))/*int*/) {{
      (((w__114__10)[(int)i__137]) = real__133)/*float*/;
      (((w__114__10)[(int)(i__137 + 1)]) = imag__134)/*float*/;
      (next_real__135 = ((real__133 * ((float)0.70710677)) - (imag__134 * ((float)-0.70710677))))/*float*/;
      (next_imag__136 = ((real__133 * ((float)-0.70710677)) + (imag__134 * ((float)0.70710677))))/*float*/;
      (real__133 = next_real__135)/*float*/;
      (imag__134 = next_imag__136)/*float*/;
    }
  }
}
void save_file_pointer__10(object_write_buffer *buf) {}
void load_file_pointer__10(object_write_buffer *buf) {}
 
void work_CombineDFT__138_41__10(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__117 = 0;/* int */
      float results__118[16] = {0};/* float[16] */
      int i_plus_1__119 = 0;/* int */
      float y0_r__120 = 0.0f;/* float */
      float y0_i__121 = 0.0f;/* float */
      float y1_r__122 = 0.0f;/* float */
      float __tmp14__123 = 0.0f;/* float */
      int __tmp15__124 = 0;/* int */
      float y1_i__125 = 0.0f;/* float */
      float __tmp16__126 = 0.0f;/* float */
      int __tmp17__127 = 0;/* int */
      float weight_real__128 = 0.0f;/* float */
      float weight_imag__129 = 0.0f;/* float */
      float y1w_r__130 = 0.0f;/* float */
      float y1w_i__131 = 0.0f;/* float */
      float v__132 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__117 = 0)/*int*/; (i__117 < 8); (i__117 = (i__117 + 2))/*int*/) {{
          (i_plus_1__119 = (i__117 + 1))/*int*/;
          y0_r__120 = BUFFER_9_10[TAIL_9_10+i__117];
;
          y0_i__121 = BUFFER_9_10[TAIL_9_10+i_plus_1__119];
;
          (__tmp15__124 = (8 + i__117))/*int*/;
          __tmp14__123 = BUFFER_9_10[TAIL_9_10+__tmp15__124];
;
          (y1_r__122 = __tmp14__123)/*float*/;
          (__tmp17__127 = (8 + i_plus_1__119))/*int*/;
          __tmp16__126 = BUFFER_9_10[TAIL_9_10+__tmp17__127];
;
          (y1_i__125 = __tmp16__126)/*float*/;
          (weight_real__128 = ((w__114__10)[(int)i__117]))/*float*/;
          (weight_imag__129 = ((w__114__10)[(int)i_plus_1__119]))/*float*/;
          (y1w_r__130 = ((y1_r__122 * weight_real__128) - (y1_i__125 * weight_imag__129)))/*float*/;
          (y1w_i__131 = ((y1_r__122 * weight_imag__129) + (y1_i__125 * weight_real__128)))/*float*/;
          ((results__118[(int)i__117]) = (y0_r__120 + y1w_r__130))/*float*/;
          ((results__118[(int)(i__117 + 1)]) = (y0_i__121 + y1w_i__131))/*float*/;
          ((results__118[(int)(8 + i__117)]) = (y0_r__120 - y1w_r__130))/*float*/;
          ((results__118[(int)((8 + i__117) + 1)]) = (y0_i__121 - y1w_i__131))/*float*/;
        }
      }
      for ((i__117 = 0)/*int*/; (i__117 < 16); (i__117++)) {{
          __pop_9_10();
          (v__132 = (results__118[(int)i__117]))/*float*/;
          __push_10_11(v__132);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 32 pop: 32 push 32
// init counts: 0 steady counts: 8

// ClusterFusion isEliminated: false



int __number_of_iterations_11;
int __counter_11 = 0;
int __steady_11 = 0;
int __tmp_11 = 0;
int __tmp2_11 = 0;
int *__state_flag_11 = NULL;
thread_info *__thread_11 = NULL;



float w__139__11[16] = {0};
void save_peek_buffer__11(object_write_buffer *buf);
void load_peek_buffer__11(object_write_buffer *buf);
void save_file_pointer__11(object_write_buffer *buf);
void load_file_pointer__11(object_write_buffer *buf);

inline void check_status__11() {
  check_thread_status(__state_flag_11, __thread_11);
}

void check_status_during_io__11() {
  check_thread_status_during_io(__state_flag_11, __thread_11);
}

void __init_thread_info_11(thread_info *info) {
  __state_flag_11 = info->get_state_flag();
}

thread_info *__get_thread_info_11() {
  if (__thread_11 != NULL) return __thread_11;
  __thread_11 = new thread_info(11, check_status_during_io__11);
  __init_thread_info_11(__thread_11);
  return __thread_11;
}

void __declare_sockets_11() {
  init_instance::add_incoming(10,11, DATA_SOCKET);
  init_instance::add_outgoing(11,12, DATA_SOCKET);
}

void __init_sockets_11(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_11() {
}

void __peek_sockets_11() {
}

 
void init_CombineDFT__163_42__11();
inline void check_status__11();

void work_CombineDFT__163_42__11(int);


inline float __pop_10_11() {
float res=BUFFER_10_11[TAIL_10_11];
TAIL_10_11++;
return res;
}

inline void __pop_10_11(int n) {
TAIL_10_11+=n;
}

inline float __peek_10_11(int offs) {
return BUFFER_10_11[TAIL_10_11+offs];
}



inline void __push_11_12(float data) {
BUFFER_11_12[HEAD_11_12]=data;
HEAD_11_12++;
}



 
void init_CombineDFT__163_42__11(){
  float real__158 = 0.0f;/* float */
  float imag__159 = 0.0f;/* float */
  float next_real__160 = 0.0f;/* float */float next_imag__161 = 0.0f;/* float */
  int i__162 = 0;/* int */

  (real__158 = ((float)1.0))/*float*/;
  (imag__159 = ((float)0.0))/*float*/;
  for ((i__162 = 0)/*int*/; (i__162 < 16); (i__162 = (i__162 + 2))/*int*/) {{
      (((w__139__11)[(int)i__162]) = real__158)/*float*/;
      (((w__139__11)[(int)(i__162 + 1)]) = imag__159)/*float*/;
      (next_real__160 = ((real__158 * ((float)0.9238795)) - (imag__159 * ((float)-0.38268346))))/*float*/;
      (next_imag__161 = ((real__158 * ((float)-0.38268346)) + (imag__159 * ((float)0.9238795))))/*float*/;
      (real__158 = next_real__160)/*float*/;
      (imag__159 = next_imag__161)/*float*/;
    }
  }
}
void save_file_pointer__11(object_write_buffer *buf) {}
void load_file_pointer__11(object_write_buffer *buf) {}
 
void work_CombineDFT__163_42__11(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__142 = 0;/* int */
      float results__143[32] = {0};/* float[32] */
      int i_plus_1__144 = 0;/* int */
      float y0_r__145 = 0.0f;/* float */
      float y0_i__146 = 0.0f;/* float */
      float y1_r__147 = 0.0f;/* float */
      float __tmp14__148 = 0.0f;/* float */
      int __tmp15__149 = 0;/* int */
      float y1_i__150 = 0.0f;/* float */
      float __tmp16__151 = 0.0f;/* float */
      int __tmp17__152 = 0;/* int */
      float weight_real__153 = 0.0f;/* float */
      float weight_imag__154 = 0.0f;/* float */
      float y1w_r__155 = 0.0f;/* float */
      float y1w_i__156 = 0.0f;/* float */
      float v__157 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__142 = 0)/*int*/; (i__142 < 16); (i__142 = (i__142 + 2))/*int*/) {{
          (i_plus_1__144 = (i__142 + 1))/*int*/;
          y0_r__145 = BUFFER_10_11[TAIL_10_11+i__142];
;
          y0_i__146 = BUFFER_10_11[TAIL_10_11+i_plus_1__144];
;
          (__tmp15__149 = (16 + i__142))/*int*/;
          __tmp14__148 = BUFFER_10_11[TAIL_10_11+__tmp15__149];
;
          (y1_r__147 = __tmp14__148)/*float*/;
          (__tmp17__152 = (16 + i_plus_1__144))/*int*/;
          __tmp16__151 = BUFFER_10_11[TAIL_10_11+__tmp17__152];
;
          (y1_i__150 = __tmp16__151)/*float*/;
          (weight_real__153 = ((w__139__11)[(int)i__142]))/*float*/;
          (weight_imag__154 = ((w__139__11)[(int)i_plus_1__144]))/*float*/;
          (y1w_r__155 = ((y1_r__147 * weight_real__153) - (y1_i__150 * weight_imag__154)))/*float*/;
          (y1w_i__156 = ((y1_r__147 * weight_imag__154) + (y1_i__150 * weight_real__153)))/*float*/;
          ((results__143[(int)i__142]) = (y0_r__145 + y1w_r__155))/*float*/;
          ((results__143[(int)(i__142 + 1)]) = (y0_i__146 + y1w_i__156))/*float*/;
          ((results__143[(int)(16 + i__142)]) = (y0_r__145 - y1w_r__155))/*float*/;
          ((results__143[(int)((16 + i__142) + 1)]) = (y0_i__146 - y1w_i__156))/*float*/;
        }
      }
      for ((i__142 = 0)/*int*/; (i__142 < 32); (i__142++)) {{
          __pop_10_11();
          (v__157 = (results__143[(int)i__142]))/*float*/;
          __push_11_12(v__157);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 64 pop: 64 push 64
// init counts: 0 steady counts: 4

// ClusterFusion isEliminated: false



int __number_of_iterations_12;
int __counter_12 = 0;
int __steady_12 = 0;
int __tmp_12 = 0;
int __tmp2_12 = 0;
int *__state_flag_12 = NULL;
thread_info *__thread_12 = NULL;



float w__164__12[32] = {0};
void save_peek_buffer__12(object_write_buffer *buf);
void load_peek_buffer__12(object_write_buffer *buf);
void save_file_pointer__12(object_write_buffer *buf);
void load_file_pointer__12(object_write_buffer *buf);

inline void check_status__12() {
  check_thread_status(__state_flag_12, __thread_12);
}

void check_status_during_io__12() {
  check_thread_status_during_io(__state_flag_12, __thread_12);
}

void __init_thread_info_12(thread_info *info) {
  __state_flag_12 = info->get_state_flag();
}

thread_info *__get_thread_info_12() {
  if (__thread_12 != NULL) return __thread_12;
  __thread_12 = new thread_info(12, check_status_during_io__12);
  __init_thread_info_12(__thread_12);
  return __thread_12;
}

void __declare_sockets_12() {
  init_instance::add_incoming(11,12, DATA_SOCKET);
  init_instance::add_outgoing(12,13, DATA_SOCKET);
}

void __init_sockets_12(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_12() {
}

void __peek_sockets_12() {
}

 
void init_CombineDFT__188_43__12();
inline void check_status__12();

void work_CombineDFT__188_43__12(int);


inline float __pop_11_12() {
float res=BUFFER_11_12[TAIL_11_12];
TAIL_11_12++;
return res;
}

inline void __pop_11_12(int n) {
TAIL_11_12+=n;
}

inline float __peek_11_12(int offs) {
return BUFFER_11_12[TAIL_11_12+offs];
}



inline void __push_12_13(float data) {
BUFFER_12_13[HEAD_12_13]=data;
HEAD_12_13++;
}



 
void init_CombineDFT__188_43__12(){
  float real__183 = 0.0f;/* float */
  float imag__184 = 0.0f;/* float */
  float next_real__185 = 0.0f;/* float */float next_imag__186 = 0.0f;/* float */
  int i__187 = 0;/* int */

  (real__183 = ((float)1.0))/*float*/;
  (imag__184 = ((float)0.0))/*float*/;
  for ((i__187 = 0)/*int*/; (i__187 < 32); (i__187 = (i__187 + 2))/*int*/) {{
      (((w__164__12)[(int)i__187]) = real__183)/*float*/;
      (((w__164__12)[(int)(i__187 + 1)]) = imag__184)/*float*/;
      (next_real__185 = ((real__183 * ((float)0.98078525)) - (imag__184 * ((float)-0.19509032))))/*float*/;
      (next_imag__186 = ((real__183 * ((float)-0.19509032)) + (imag__184 * ((float)0.98078525))))/*float*/;
      (real__183 = next_real__185)/*float*/;
      (imag__184 = next_imag__186)/*float*/;
    }
  }
}
void save_file_pointer__12(object_write_buffer *buf) {}
void load_file_pointer__12(object_write_buffer *buf) {}
 
void work_CombineDFT__188_43__12(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__167 = 0;/* int */
      float results__168[64] = {0};/* float[64] */
      int i_plus_1__169 = 0;/* int */
      float y0_r__170 = 0.0f;/* float */
      float y0_i__171 = 0.0f;/* float */
      float y1_r__172 = 0.0f;/* float */
      float __tmp14__173 = 0.0f;/* float */
      int __tmp15__174 = 0;/* int */
      float y1_i__175 = 0.0f;/* float */
      float __tmp16__176 = 0.0f;/* float */
      int __tmp17__177 = 0;/* int */
      float weight_real__178 = 0.0f;/* float */
      float weight_imag__179 = 0.0f;/* float */
      float y1w_r__180 = 0.0f;/* float */
      float y1w_i__181 = 0.0f;/* float */
      float v__182 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__167 = 0)/*int*/; (i__167 < 32); (i__167 = (i__167 + 2))/*int*/) {{
          (i_plus_1__169 = (i__167 + 1))/*int*/;
          y0_r__170 = BUFFER_11_12[TAIL_11_12+i__167];
;
          y0_i__171 = BUFFER_11_12[TAIL_11_12+i_plus_1__169];
;
          (__tmp15__174 = (32 + i__167))/*int*/;
          __tmp14__173 = BUFFER_11_12[TAIL_11_12+__tmp15__174];
;
          (y1_r__172 = __tmp14__173)/*float*/;
          (__tmp17__177 = (32 + i_plus_1__169))/*int*/;
          __tmp16__176 = BUFFER_11_12[TAIL_11_12+__tmp17__177];
;
          (y1_i__175 = __tmp16__176)/*float*/;
          (weight_real__178 = ((w__164__12)[(int)i__167]))/*float*/;
          (weight_imag__179 = ((w__164__12)[(int)i_plus_1__169]))/*float*/;
          (y1w_r__180 = ((y1_r__172 * weight_real__178) - (y1_i__175 * weight_imag__179)))/*float*/;
          (y1w_i__181 = ((y1_r__172 * weight_imag__179) + (y1_i__175 * weight_real__178)))/*float*/;
          ((results__168[(int)i__167]) = (y0_r__170 + y1w_r__180))/*float*/;
          ((results__168[(int)(i__167 + 1)]) = (y0_i__171 + y1w_i__181))/*float*/;
          ((results__168[(int)(32 + i__167)]) = (y0_r__170 - y1w_r__180))/*float*/;
          ((results__168[(int)((32 + i__167) + 1)]) = (y0_i__171 - y1w_i__181))/*float*/;
        }
      }
      for ((i__167 = 0)/*int*/; (i__167 < 64); (i__167++)) {{
          __pop_11_12();
          (v__182 = (results__168[(int)i__167]))/*float*/;
          __push_12_13(v__182);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 128 pop: 128 push 128
// init counts: 0 steady counts: 2

// ClusterFusion isEliminated: false



int __number_of_iterations_13;
int __counter_13 = 0;
int __steady_13 = 0;
int __tmp_13 = 0;
int __tmp2_13 = 0;
int *__state_flag_13 = NULL;
thread_info *__thread_13 = NULL;



float w__189__13[64] = {0};
void save_peek_buffer__13(object_write_buffer *buf);
void load_peek_buffer__13(object_write_buffer *buf);
void save_file_pointer__13(object_write_buffer *buf);
void load_file_pointer__13(object_write_buffer *buf);

inline void check_status__13() {
  check_thread_status(__state_flag_13, __thread_13);
}

void check_status_during_io__13() {
  check_thread_status_during_io(__state_flag_13, __thread_13);
}

void __init_thread_info_13(thread_info *info) {
  __state_flag_13 = info->get_state_flag();
}

thread_info *__get_thread_info_13() {
  if (__thread_13 != NULL) return __thread_13;
  __thread_13 = new thread_info(13, check_status_during_io__13);
  __init_thread_info_13(__thread_13);
  return __thread_13;
}

void __declare_sockets_13() {
  init_instance::add_incoming(12,13, DATA_SOCKET);
  init_instance::add_outgoing(13,14, DATA_SOCKET);
}

void __init_sockets_13(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_13() {
}

void __peek_sockets_13() {
}

 
void init_CombineDFT__213_44__13();
inline void check_status__13();

void work_CombineDFT__213_44__13(int);


inline float __pop_12_13() {
float res=BUFFER_12_13[TAIL_12_13];
TAIL_12_13++;
return res;
}

inline void __pop_12_13(int n) {
TAIL_12_13+=n;
}

inline float __peek_12_13(int offs) {
return BUFFER_12_13[TAIL_12_13+offs];
}



inline void __push_13_14(float data) {
BUFFER_13_14[HEAD_13_14]=data;
HEAD_13_14++;
}



 
void init_CombineDFT__213_44__13(){
  float real__208 = 0.0f;/* float */
  float imag__209 = 0.0f;/* float */
  float next_real__210 = 0.0f;/* float */float next_imag__211 = 0.0f;/* float */
  int i__212 = 0;/* int */

  (real__208 = ((float)1.0))/*float*/;
  (imag__209 = ((float)0.0))/*float*/;
  for ((i__212 = 0)/*int*/; (i__212 < 64); (i__212 = (i__212 + 2))/*int*/) {{
      (((w__189__13)[(int)i__212]) = real__208)/*float*/;
      (((w__189__13)[(int)(i__212 + 1)]) = imag__209)/*float*/;
      (next_real__210 = ((real__208 * ((float)0.9951847)) - (imag__209 * ((float)-0.09801714))))/*float*/;
      (next_imag__211 = ((real__208 * ((float)-0.09801714)) + (imag__209 * ((float)0.9951847))))/*float*/;
      (real__208 = next_real__210)/*float*/;
      (imag__209 = next_imag__211)/*float*/;
    }
  }
}
void save_file_pointer__13(object_write_buffer *buf) {}
void load_file_pointer__13(object_write_buffer *buf) {}
 
void work_CombineDFT__213_44__13(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__192 = 0;/* int */
      float results__193[128] = {0};/* float[128] */
      int i_plus_1__194 = 0;/* int */
      float y0_r__195 = 0.0f;/* float */
      float y0_i__196 = 0.0f;/* float */
      float y1_r__197 = 0.0f;/* float */
      float __tmp14__198 = 0.0f;/* float */
      int __tmp15__199 = 0;/* int */
      float y1_i__200 = 0.0f;/* float */
      float __tmp16__201 = 0.0f;/* float */
      int __tmp17__202 = 0;/* int */
      float weight_real__203 = 0.0f;/* float */
      float weight_imag__204 = 0.0f;/* float */
      float y1w_r__205 = 0.0f;/* float */
      float y1w_i__206 = 0.0f;/* float */
      float v__207 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__192 = 0)/*int*/; (i__192 < 64); (i__192 = (i__192 + 2))/*int*/) {{
          (i_plus_1__194 = (i__192 + 1))/*int*/;
          y0_r__195 = BUFFER_12_13[TAIL_12_13+i__192];
;
          y0_i__196 = BUFFER_12_13[TAIL_12_13+i_plus_1__194];
;
          (__tmp15__199 = (64 + i__192))/*int*/;
          __tmp14__198 = BUFFER_12_13[TAIL_12_13+__tmp15__199];
;
          (y1_r__197 = __tmp14__198)/*float*/;
          (__tmp17__202 = (64 + i_plus_1__194))/*int*/;
          __tmp16__201 = BUFFER_12_13[TAIL_12_13+__tmp17__202];
;
          (y1_i__200 = __tmp16__201)/*float*/;
          (weight_real__203 = ((w__189__13)[(int)i__192]))/*float*/;
          (weight_imag__204 = ((w__189__13)[(int)i_plus_1__194]))/*float*/;
          (y1w_r__205 = ((y1_r__197 * weight_real__203) - (y1_i__200 * weight_imag__204)))/*float*/;
          (y1w_i__206 = ((y1_r__197 * weight_imag__204) + (y1_i__200 * weight_real__203)))/*float*/;
          ((results__193[(int)i__192]) = (y0_r__195 + y1w_r__205))/*float*/;
          ((results__193[(int)(i__192 + 1)]) = (y0_i__196 + y1w_i__206))/*float*/;
          ((results__193[(int)(64 + i__192)]) = (y0_r__195 - y1w_r__205))/*float*/;
          ((results__193[(int)((64 + i__192) + 1)]) = (y0_i__196 - y1w_i__206))/*float*/;
        }
      }
      for ((i__192 = 0)/*int*/; (i__192 < 128); (i__192++)) {{
          __pop_12_13();
          (v__207 = (results__193[(int)i__192]))/*float*/;
          __push_13_14(v__207);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 256 pop: 256 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_14;
int __counter_14 = 0;
int __steady_14 = 0;
int __tmp_14 = 0;
int __tmp2_14 = 0;
int *__state_flag_14 = NULL;
thread_info *__thread_14 = NULL;



float w__214__14[128] = {0};
void save_peek_buffer__14(object_write_buffer *buf);
void load_peek_buffer__14(object_write_buffer *buf);
void save_file_pointer__14(object_write_buffer *buf);
void load_file_pointer__14(object_write_buffer *buf);

inline void check_status__14() {
  check_thread_status(__state_flag_14, __thread_14);
}

void check_status_during_io__14() {
  check_thread_status_during_io(__state_flag_14, __thread_14);
}

void __init_thread_info_14(thread_info *info) {
  __state_flag_14 = info->get_state_flag();
}

thread_info *__get_thread_info_14() {
  if (__thread_14 != NULL) return __thread_14;
  __thread_14 = new thread_info(14, check_status_during_io__14);
  __init_thread_info_14(__thread_14);
  return __thread_14;
}

void __declare_sockets_14() {
  init_instance::add_incoming(13,14, DATA_SOCKET);
  init_instance::add_outgoing(14,15, DATA_SOCKET);
}

void __init_sockets_14(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_14() {
}

void __peek_sockets_14() {
}

 
void init_CombineDFT__238_45__14();
inline void check_status__14();

void work_CombineDFT__238_45__14(int);


inline float __pop_13_14() {
float res=BUFFER_13_14[TAIL_13_14];
TAIL_13_14++;
return res;
}

inline void __pop_13_14(int n) {
TAIL_13_14+=n;
}

inline float __peek_13_14(int offs) {
return BUFFER_13_14[TAIL_13_14+offs];
}



inline void __push_14_15(float data) {
BUFFER_14_15[HEAD_14_15]=data;
HEAD_14_15++;
}



 
void init_CombineDFT__238_45__14(){
  float real__233 = 0.0f;/* float */
  float imag__234 = 0.0f;/* float */
  float next_real__235 = 0.0f;/* float */float next_imag__236 = 0.0f;/* float */
  int i__237 = 0;/* int */

  (real__233 = ((float)1.0))/*float*/;
  (imag__234 = ((float)0.0))/*float*/;
  for ((i__237 = 0)/*int*/; (i__237 < 128); (i__237 = (i__237 + 2))/*int*/) {{
      (((w__214__14)[(int)i__237]) = real__233)/*float*/;
      (((w__214__14)[(int)(i__237 + 1)]) = imag__234)/*float*/;
      (next_real__235 = ((real__233 * ((float)0.99879545)) - (imag__234 * ((float)-0.049067676))))/*float*/;
      (next_imag__236 = ((real__233 * ((float)-0.049067676)) + (imag__234 * ((float)0.99879545))))/*float*/;
      (real__233 = next_real__235)/*float*/;
      (imag__234 = next_imag__236)/*float*/;
    }
  }
}
void save_file_pointer__14(object_write_buffer *buf) {}
void load_file_pointer__14(object_write_buffer *buf) {}
 
void work_CombineDFT__238_45__14(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__217 = 0;/* int */
      float results__218[256] = {0};/* float[256] */
      int i_plus_1__219 = 0;/* int */
      float y0_r__220 = 0.0f;/* float */
      float y0_i__221 = 0.0f;/* float */
      float y1_r__222 = 0.0f;/* float */
      float __tmp14__223 = 0.0f;/* float */
      int __tmp15__224 = 0;/* int */
      float y1_i__225 = 0.0f;/* float */
      float __tmp16__226 = 0.0f;/* float */
      int __tmp17__227 = 0;/* int */
      float weight_real__228 = 0.0f;/* float */
      float weight_imag__229 = 0.0f;/* float */
      float y1w_r__230 = 0.0f;/* float */
      float y1w_i__231 = 0.0f;/* float */
      float v__232 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__217 = 0)/*int*/; (i__217 < 128); (i__217 = (i__217 + 2))/*int*/) {{
          (i_plus_1__219 = (i__217 + 1))/*int*/;
          y0_r__220 = BUFFER_13_14[TAIL_13_14+i__217];
;
          y0_i__221 = BUFFER_13_14[TAIL_13_14+i_plus_1__219];
;
          (__tmp15__224 = (128 + i__217))/*int*/;
          __tmp14__223 = BUFFER_13_14[TAIL_13_14+__tmp15__224];
;
          (y1_r__222 = __tmp14__223)/*float*/;
          (__tmp17__227 = (128 + i_plus_1__219))/*int*/;
          __tmp16__226 = BUFFER_13_14[TAIL_13_14+__tmp17__227];
;
          (y1_i__225 = __tmp16__226)/*float*/;
          (weight_real__228 = ((w__214__14)[(int)i__217]))/*float*/;
          (weight_imag__229 = ((w__214__14)[(int)i_plus_1__219]))/*float*/;
          (y1w_r__230 = ((y1_r__222 * weight_real__228) - (y1_i__225 * weight_imag__229)))/*float*/;
          (y1w_i__231 = ((y1_r__222 * weight_imag__229) + (y1_i__225 * weight_real__228)))/*float*/;
          ((results__218[(int)i__217]) = (y0_r__220 + y1w_r__230))/*float*/;
          ((results__218[(int)(i__217 + 1)]) = (y0_i__221 + y1w_i__231))/*float*/;
          ((results__218[(int)(128 + i__217)]) = (y0_r__220 - y1w_r__230))/*float*/;
          ((results__218[(int)((128 + i__217) + 1)]) = (y0_i__221 - y1w_i__231))/*float*/;
        }
      }
      for ((i__217 = 0)/*int*/; (i__217 < 256); (i__217++)) {{
          __pop_13_14();
          (v__232 = (results__218[(int)i__217]))/*float*/;
          __push_14_15(v__232);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_15;
int __counter_15 = 0;
int __steady_15 = 0;
int __tmp_15 = 0;
int __tmp2_15 = 0;
int *__state_flag_15 = NULL;
thread_info *__thread_15 = NULL;



inline void check_status__15() {
  check_thread_status(__state_flag_15, __thread_15);
}

void check_status_during_io__15() {
  check_thread_status_during_io(__state_flag_15, __thread_15);
}

void __init_thread_info_15(thread_info *info) {
  __state_flag_15 = info->get_state_flag();
}

thread_info *__get_thread_info_15() {
  if (__thread_15 != NULL) return __thread_15;
  __thread_15 = new thread_info(15, check_status_during_io__15);
  __init_thread_info_15(__thread_15);
  return __thread_15;
}

void __declare_sockets_15() {
  init_instance::add_incoming(14,15, DATA_SOCKET);
  init_instance::add_incoming(29,15, DATA_SOCKET);
  init_instance::add_outgoing(15,16, DATA_SOCKET);
}

void __init_sockets_15(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_15() {
}

void __peek_sockets_15() {
}


inline void __push_15_16(float data) {
BUFFER_15_16[HEAD_15_16]=data;
HEAD_15_16++;
}


inline float __pop_14_15() {
float res=BUFFER_14_15[TAIL_14_15];
TAIL_14_15++;
return res;
}

inline float __pop_29_15() {
float res=BUFFER_29_15[TAIL_29_15];
TAIL_29_15++;
return res;
}


void __joiner_15_work(int ____n) {
  for (;____n > 0; ____n--) {
    for (int __k = 0; __k < 32; __k++) {
      __push_15_16(__pop_14_15());
      __push_15_16(__pop_14_15());
      __push_15_16(__pop_14_15());
      __push_15_16(__pop_14_15());
      __push_15_16(__pop_14_15());
      __push_15_16(__pop_14_15());
      __push_15_16(__pop_14_15());
      __push_15_16(__pop_14_15());
    }
    for (int __k = 0; __k < 32; __k++) {
      __push_15_16(__pop_29_15());
      __push_15_16(__pop_29_15());
      __push_15_16(__pop_29_15());
      __push_15_16(__pop_29_15());
      __push_15_16(__pop_29_15());
      __push_15_16(__pop_29_15());
      __push_15_16(__pop_29_15());
      __push_15_16(__pop_29_15());
    }
  }
}


// peek: 1 pop: 1 push 0
// init counts: 0 steady counts: 512

// ClusterFusion isEliminated: false



int __number_of_iterations_16;
int __counter_16 = 0;
int __steady_16 = 0;
int __tmp_16 = 0;
int __tmp2_16 = 0;
int *__state_flag_16 = NULL;
thread_info *__thread_16 = NULL;



float _TheGlobal__sum__0____16 = 0.0f;
void save_peek_buffer__16(object_write_buffer *buf);
void load_peek_buffer__16(object_write_buffer *buf);
void save_file_pointer__16(object_write_buffer *buf);
void load_file_pointer__16(object_write_buffer *buf);

inline void check_status__16() {
  check_thread_status(__state_flag_16, __thread_16);
}

void check_status_during_io__16() {
  check_thread_status_during_io(__state_flag_16, __thread_16);
}

void __init_thread_info_16(thread_info *info) {
  __state_flag_16 = info->get_state_flag();
}

thread_info *__get_thread_info_16() {
  if (__thread_16 != NULL) return __thread_16;
  __thread_16 = new thread_info(16, check_status_during_io__16);
  __init_thread_info_16(__thread_16);
  return __thread_16;
}

void __declare_sockets_16() {
  init_instance::add_incoming(15,16, DATA_SOCKET);
}

void __init_sockets_16(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_16() {
}

void __peek_sockets_16() {
}

 
void init_FloatPrinter__478_59__16();
inline void check_status__16();

void work_FloatPrinter__478_59__16(int);


inline float __pop_15_16() {
float res=BUFFER_15_16[TAIL_15_16];
TAIL_15_16++;
return res;
}

inline void __pop_15_16(int n) {
TAIL_15_16+=n;
}

inline float __peek_15_16(int offs) {
return BUFFER_15_16[TAIL_15_16+offs];
}


 
void init_FloatPrinter__478_59__16(){
  ((_TheGlobal__sum__0____16) = ((float)0.0))/*float*/;
}
void save_file_pointer__16(object_write_buffer *buf) {}
void load_file_pointer__16(object_write_buffer *buf) {}
 
void work_FloatPrinter__478_59__16(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__477 = 0.0f;/* float */

      // mark begin: SIRFilter FloatPrinter

      v__477 = BUFFER_15_16[TAIL_15_16]; TAIL_15_16++;
;
      ((_TheGlobal__sum__0____16) = ((_TheGlobal__sum__0____16) + v__477))/*float*/;

      // TIMER_PRINT_CODE: __print_sink__ += (int)(v__477); 

      // mark end: SIRFilter FloatPrinter

    }
  }
}

// peek: 256 pop: 256 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_17;
int __counter_17 = 0;
int __steady_17 = 0;
int __tmp_17 = 0;
int __tmp2_17 = 0;
int *__state_flag_17 = NULL;
thread_info *__thread_17 = NULL;



void save_peek_buffer__17(object_write_buffer *buf);
void load_peek_buffer__17(object_write_buffer *buf);
void save_file_pointer__17(object_write_buffer *buf);
void load_file_pointer__17(object_write_buffer *buf);

inline void check_status__17() {
  check_thread_status(__state_flag_17, __thread_17);
}

void check_status_during_io__17() {
  check_thread_status_during_io(__state_flag_17, __thread_17);
}

void __init_thread_info_17(thread_info *info) {
  __state_flag_17 = info->get_state_flag();
}

thread_info *__get_thread_info_17() {
  if (__thread_17 != NULL) return __thread_17;
  __thread_17 = new thread_info(17, check_status_during_io__17);
  __init_thread_info_17(__thread_17);
  return __thread_17;
}

void __declare_sockets_17() {
  init_instance::add_incoming(1,17, DATA_SOCKET);
  init_instance::add_outgoing(17,18, DATA_SOCKET);
}

void __init_sockets_17(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_17() {
}

void __peek_sockets_17() {
}

 
void init_FFTReorderSimple__248_46__17();
inline void check_status__17();

void work_FFTReorderSimple__248_46__17(int);


inline float __pop_1_17() {
float res=BUFFER_1_17[TAIL_1_17];
TAIL_1_17++;
return res;
}

inline void __pop_1_17(int n) {
TAIL_1_17+=n;
}

inline float __peek_1_17(int offs) {
return BUFFER_1_17[TAIL_1_17+offs];
}



inline void __push_17_18(float data) {
BUFFER_17_18[HEAD_17_18]=data;
HEAD_17_18++;
}



 
void init_FFTReorderSimple__248_46__17(){
}
void save_file_pointer__17(object_write_buffer *buf) {}
void load_file_pointer__17(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__248_46__17(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__241 = 0;/* int */
      float __tmp8__242 = 0.0f;/* float */
      float __tmp9__243 = 0.0f;/* float */
      int __tmp10__244 = 0;/* int */
      float __tmp11__245 = 0.0f;/* float */
      float __tmp12__246 = 0.0f;/* float */
      int __tmp13__247 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__241 = 0)/*int*/; (i__241 < 256); (i__241 = (i__241 + 4))/*int*/) {{
          __tmp8__242 = BUFFER_1_17[TAIL_1_17+i__241];
;
          __push_17_18(__tmp8__242);
          (__tmp10__244 = (i__241 + 1))/*int*/;
          __tmp9__243 = BUFFER_1_17[TAIL_1_17+__tmp10__244];
;
          __push_17_18(__tmp9__243);
        }
      }
      for ((i__241 = 2)/*int*/; (i__241 < 256); (i__241 = (i__241 + 4))/*int*/) {{
          __tmp11__245 = BUFFER_1_17[TAIL_1_17+i__241];
;
          __push_17_18(__tmp11__245);
          (__tmp13__247 = (i__241 + 1))/*int*/;
          __tmp12__246 = BUFFER_1_17[TAIL_1_17+__tmp13__247];
;
          __push_17_18(__tmp12__246);
        }
      }
      __pop_1_17(256);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 128 pop: 128 push 128
// init counts: 0 steady counts: 2

// ClusterFusion isEliminated: false



int __number_of_iterations_18;
int __counter_18 = 0;
int __steady_18 = 0;
int __tmp_18 = 0;
int __tmp2_18 = 0;
int *__state_flag_18 = NULL;
thread_info *__thread_18 = NULL;



void save_peek_buffer__18(object_write_buffer *buf);
void load_peek_buffer__18(object_write_buffer *buf);
void save_file_pointer__18(object_write_buffer *buf);
void load_file_pointer__18(object_write_buffer *buf);

inline void check_status__18() {
  check_thread_status(__state_flag_18, __thread_18);
}

void check_status_during_io__18() {
  check_thread_status_during_io(__state_flag_18, __thread_18);
}

void __init_thread_info_18(thread_info *info) {
  __state_flag_18 = info->get_state_flag();
}

thread_info *__get_thread_info_18() {
  if (__thread_18 != NULL) return __thread_18;
  __thread_18 = new thread_info(18, check_status_during_io__18);
  __init_thread_info_18(__thread_18);
  return __thread_18;
}

void __declare_sockets_18() {
  init_instance::add_incoming(17,18, DATA_SOCKET);
  init_instance::add_outgoing(18,19, DATA_SOCKET);
}

void __init_sockets_18(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_18() {
}

void __peek_sockets_18() {
}

 
void init_FFTReorderSimple__258_47__18();
inline void check_status__18();

void work_FFTReorderSimple__258_47__18(int);


inline float __pop_17_18() {
float res=BUFFER_17_18[TAIL_17_18];
TAIL_17_18++;
return res;
}

inline void __pop_17_18(int n) {
TAIL_17_18+=n;
}

inline float __peek_17_18(int offs) {
return BUFFER_17_18[TAIL_17_18+offs];
}



inline void __push_18_19(float data) {
BUFFER_18_19[HEAD_18_19]=data;
HEAD_18_19++;
}



 
void init_FFTReorderSimple__258_47__18(){
}
void save_file_pointer__18(object_write_buffer *buf) {}
void load_file_pointer__18(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__258_47__18(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__251 = 0;/* int */
      float __tmp8__252 = 0.0f;/* float */
      float __tmp9__253 = 0.0f;/* float */
      int __tmp10__254 = 0;/* int */
      float __tmp11__255 = 0.0f;/* float */
      float __tmp12__256 = 0.0f;/* float */
      int __tmp13__257 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__251 = 0)/*int*/; (i__251 < 128); (i__251 = (i__251 + 4))/*int*/) {{
          __tmp8__252 = BUFFER_17_18[TAIL_17_18+i__251];
;
          __push_18_19(__tmp8__252);
          (__tmp10__254 = (i__251 + 1))/*int*/;
          __tmp9__253 = BUFFER_17_18[TAIL_17_18+__tmp10__254];
;
          __push_18_19(__tmp9__253);
        }
      }
      for ((i__251 = 2)/*int*/; (i__251 < 128); (i__251 = (i__251 + 4))/*int*/) {{
          __tmp11__255 = BUFFER_17_18[TAIL_17_18+i__251];
;
          __push_18_19(__tmp11__255);
          (__tmp13__257 = (i__251 + 1))/*int*/;
          __tmp12__256 = BUFFER_17_18[TAIL_17_18+__tmp13__257];
;
          __push_18_19(__tmp12__256);
        }
      }
      __pop_17_18(128);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 64 pop: 64 push 64
// init counts: 0 steady counts: 4

// ClusterFusion isEliminated: false



int __number_of_iterations_19;
int __counter_19 = 0;
int __steady_19 = 0;
int __tmp_19 = 0;
int __tmp2_19 = 0;
int *__state_flag_19 = NULL;
thread_info *__thread_19 = NULL;



void save_peek_buffer__19(object_write_buffer *buf);
void load_peek_buffer__19(object_write_buffer *buf);
void save_file_pointer__19(object_write_buffer *buf);
void load_file_pointer__19(object_write_buffer *buf);

inline void check_status__19() {
  check_thread_status(__state_flag_19, __thread_19);
}

void check_status_during_io__19() {
  check_thread_status_during_io(__state_flag_19, __thread_19);
}

void __init_thread_info_19(thread_info *info) {
  __state_flag_19 = info->get_state_flag();
}

thread_info *__get_thread_info_19() {
  if (__thread_19 != NULL) return __thread_19;
  __thread_19 = new thread_info(19, check_status_during_io__19);
  __init_thread_info_19(__thread_19);
  return __thread_19;
}

void __declare_sockets_19() {
  init_instance::add_incoming(18,19, DATA_SOCKET);
  init_instance::add_outgoing(19,20, DATA_SOCKET);
}

void __init_sockets_19(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_19() {
}

void __peek_sockets_19() {
}

 
void init_FFTReorderSimple__268_48__19();
inline void check_status__19();

void work_FFTReorderSimple__268_48__19(int);


inline float __pop_18_19() {
float res=BUFFER_18_19[TAIL_18_19];
TAIL_18_19++;
return res;
}

inline void __pop_18_19(int n) {
TAIL_18_19+=n;
}

inline float __peek_18_19(int offs) {
return BUFFER_18_19[TAIL_18_19+offs];
}



inline void __push_19_20(float data) {
BUFFER_19_20[HEAD_19_20]=data;
HEAD_19_20++;
}



 
void init_FFTReorderSimple__268_48__19(){
}
void save_file_pointer__19(object_write_buffer *buf) {}
void load_file_pointer__19(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__268_48__19(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__261 = 0;/* int */
      float __tmp8__262 = 0.0f;/* float */
      float __tmp9__263 = 0.0f;/* float */
      int __tmp10__264 = 0;/* int */
      float __tmp11__265 = 0.0f;/* float */
      float __tmp12__266 = 0.0f;/* float */
      int __tmp13__267 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__261 = 0)/*int*/; (i__261 < 64); (i__261 = (i__261 + 4))/*int*/) {{
          __tmp8__262 = BUFFER_18_19[TAIL_18_19+i__261];
;
          __push_19_20(__tmp8__262);
          (__tmp10__264 = (i__261 + 1))/*int*/;
          __tmp9__263 = BUFFER_18_19[TAIL_18_19+__tmp10__264];
;
          __push_19_20(__tmp9__263);
        }
      }
      for ((i__261 = 2)/*int*/; (i__261 < 64); (i__261 = (i__261 + 4))/*int*/) {{
          __tmp11__265 = BUFFER_18_19[TAIL_18_19+i__261];
;
          __push_19_20(__tmp11__265);
          (__tmp13__267 = (i__261 + 1))/*int*/;
          __tmp12__266 = BUFFER_18_19[TAIL_18_19+__tmp13__267];
;
          __push_19_20(__tmp12__266);
        }
      }
      __pop_18_19(64);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 256 pop: 256 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_2;
int __counter_2 = 0;
int __steady_2 = 0;
int __tmp_2 = 0;
int __tmp2_2 = 0;
int *__state_flag_2 = NULL;
thread_info *__thread_2 = NULL;



void save_peek_buffer__2(object_write_buffer *buf);
void load_peek_buffer__2(object_write_buffer *buf);
void save_file_pointer__2(object_write_buffer *buf);
void load_file_pointer__2(object_write_buffer *buf);

inline void check_status__2() {
  check_thread_status(__state_flag_2, __thread_2);
}

void check_status_during_io__2() {
  check_thread_status_during_io(__state_flag_2, __thread_2);
}

void __init_thread_info_2(thread_info *info) {
  __state_flag_2 = info->get_state_flag();
}

thread_info *__get_thread_info_2() {
  if (__thread_2 != NULL) return __thread_2;
  __thread_2 = new thread_info(2, check_status_during_io__2);
  __init_thread_info_2(__thread_2);
  return __thread_2;
}

void __declare_sockets_2() {
  init_instance::add_incoming(1,2, DATA_SOCKET);
  init_instance::add_outgoing(2,3, DATA_SOCKET);
}

void __init_sockets_2(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_2() {
}

void __peek_sockets_2() {
}

 
void init_FFTReorderSimple__13_33__2();
inline void check_status__2();

void work_FFTReorderSimple__13_33__2(int);


inline float __pop_1_2() {
float res=BUFFER_1_2[TAIL_1_2];
TAIL_1_2++;
return res;
}

inline void __pop_1_2(int n) {
TAIL_1_2+=n;
}

inline float __peek_1_2(int offs) {
return BUFFER_1_2[TAIL_1_2+offs];
}



inline void __push_2_3(float data) {
BUFFER_2_3[HEAD_2_3]=data;
HEAD_2_3++;
}



 
void init_FFTReorderSimple__13_33__2(){
}
void save_file_pointer__2(object_write_buffer *buf) {}
void load_file_pointer__2(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__13_33__2(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__6 = 0;/* int */
      float __tmp8__7 = 0.0f;/* float */
      float __tmp9__8 = 0.0f;/* float */
      int __tmp10__9 = 0;/* int */
      float __tmp11__10 = 0.0f;/* float */
      float __tmp12__11 = 0.0f;/* float */
      int __tmp13__12 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__6 = 0)/*int*/; (i__6 < 256); (i__6 = (i__6 + 4))/*int*/) {{
          __tmp8__7 = BUFFER_1_2[TAIL_1_2+i__6];
;
          __push_2_3(__tmp8__7);
          (__tmp10__9 = (i__6 + 1))/*int*/;
          __tmp9__8 = BUFFER_1_2[TAIL_1_2+__tmp10__9];
;
          __push_2_3(__tmp9__8);
        }
      }
      for ((i__6 = 2)/*int*/; (i__6 < 256); (i__6 = (i__6 + 4))/*int*/) {{
          __tmp11__10 = BUFFER_1_2[TAIL_1_2+i__6];
;
          __push_2_3(__tmp11__10);
          (__tmp13__12 = (i__6 + 1))/*int*/;
          __tmp12__11 = BUFFER_1_2[TAIL_1_2+__tmp13__12];
;
          __push_2_3(__tmp12__11);
        }
      }
      __pop_1_2(256);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 32 pop: 32 push 32
// init counts: 0 steady counts: 8

// ClusterFusion isEliminated: false



int __number_of_iterations_20;
int __counter_20 = 0;
int __steady_20 = 0;
int __tmp_20 = 0;
int __tmp2_20 = 0;
int *__state_flag_20 = NULL;
thread_info *__thread_20 = NULL;



void save_peek_buffer__20(object_write_buffer *buf);
void load_peek_buffer__20(object_write_buffer *buf);
void save_file_pointer__20(object_write_buffer *buf);
void load_file_pointer__20(object_write_buffer *buf);

inline void check_status__20() {
  check_thread_status(__state_flag_20, __thread_20);
}

void check_status_during_io__20() {
  check_thread_status_during_io(__state_flag_20, __thread_20);
}

void __init_thread_info_20(thread_info *info) {
  __state_flag_20 = info->get_state_flag();
}

thread_info *__get_thread_info_20() {
  if (__thread_20 != NULL) return __thread_20;
  __thread_20 = new thread_info(20, check_status_during_io__20);
  __init_thread_info_20(__thread_20);
  return __thread_20;
}

void __declare_sockets_20() {
  init_instance::add_incoming(19,20, DATA_SOCKET);
  init_instance::add_outgoing(20,21, DATA_SOCKET);
}

void __init_sockets_20(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_20() {
}

void __peek_sockets_20() {
}

 
void init_FFTReorderSimple__278_49__20();
inline void check_status__20();

void work_FFTReorderSimple__278_49__20(int);


inline float __pop_19_20() {
float res=BUFFER_19_20[TAIL_19_20];
TAIL_19_20++;
return res;
}

inline void __pop_19_20(int n) {
TAIL_19_20+=n;
}

inline float __peek_19_20(int offs) {
return BUFFER_19_20[TAIL_19_20+offs];
}



inline void __push_20_21(float data) {
BUFFER_20_21[HEAD_20_21]=data;
HEAD_20_21++;
}



 
void init_FFTReorderSimple__278_49__20(){
}
void save_file_pointer__20(object_write_buffer *buf) {}
void load_file_pointer__20(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__278_49__20(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__271 = 0;/* int */
      float __tmp8__272 = 0.0f;/* float */
      float __tmp9__273 = 0.0f;/* float */
      int __tmp10__274 = 0;/* int */
      float __tmp11__275 = 0.0f;/* float */
      float __tmp12__276 = 0.0f;/* float */
      int __tmp13__277 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__271 = 0)/*int*/; (i__271 < 32); (i__271 = (i__271 + 4))/*int*/) {{
          __tmp8__272 = BUFFER_19_20[TAIL_19_20+i__271];
;
          __push_20_21(__tmp8__272);
          (__tmp10__274 = (i__271 + 1))/*int*/;
          __tmp9__273 = BUFFER_19_20[TAIL_19_20+__tmp10__274];
;
          __push_20_21(__tmp9__273);
        }
      }
      for ((i__271 = 2)/*int*/; (i__271 < 32); (i__271 = (i__271 + 4))/*int*/) {{
          __tmp11__275 = BUFFER_19_20[TAIL_19_20+i__271];
;
          __push_20_21(__tmp11__275);
          (__tmp13__277 = (i__271 + 1))/*int*/;
          __tmp12__276 = BUFFER_19_20[TAIL_19_20+__tmp13__277];
;
          __push_20_21(__tmp12__276);
        }
      }
      __pop_19_20(32);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 16 pop: 16 push 16
// init counts: 0 steady counts: 16

// ClusterFusion isEliminated: false



int __number_of_iterations_21;
int __counter_21 = 0;
int __steady_21 = 0;
int __tmp_21 = 0;
int __tmp2_21 = 0;
int *__state_flag_21 = NULL;
thread_info *__thread_21 = NULL;



void save_peek_buffer__21(object_write_buffer *buf);
void load_peek_buffer__21(object_write_buffer *buf);
void save_file_pointer__21(object_write_buffer *buf);
void load_file_pointer__21(object_write_buffer *buf);

inline void check_status__21() {
  check_thread_status(__state_flag_21, __thread_21);
}

void check_status_during_io__21() {
  check_thread_status_during_io(__state_flag_21, __thread_21);
}

void __init_thread_info_21(thread_info *info) {
  __state_flag_21 = info->get_state_flag();
}

thread_info *__get_thread_info_21() {
  if (__thread_21 != NULL) return __thread_21;
  __thread_21 = new thread_info(21, check_status_during_io__21);
  __init_thread_info_21(__thread_21);
  return __thread_21;
}

void __declare_sockets_21() {
  init_instance::add_incoming(20,21, DATA_SOCKET);
  init_instance::add_outgoing(21,22, DATA_SOCKET);
}

void __init_sockets_21(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_21() {
}

void __peek_sockets_21() {
}

 
void init_FFTReorderSimple__288_50__21();
inline void check_status__21();

void work_FFTReorderSimple__288_50__21(int);


inline float __pop_20_21() {
float res=BUFFER_20_21[TAIL_20_21];
TAIL_20_21++;
return res;
}

inline void __pop_20_21(int n) {
TAIL_20_21+=n;
}

inline float __peek_20_21(int offs) {
return BUFFER_20_21[TAIL_20_21+offs];
}



inline void __push_21_22(float data) {
BUFFER_21_22[HEAD_21_22]=data;
HEAD_21_22++;
}



 
void init_FFTReorderSimple__288_50__21(){
}
void save_file_pointer__21(object_write_buffer *buf) {}
void load_file_pointer__21(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__288_50__21(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__281 = 0;/* int */
      float __tmp8__282 = 0.0f;/* float */
      float __tmp9__283 = 0.0f;/* float */
      int __tmp10__284 = 0;/* int */
      float __tmp11__285 = 0.0f;/* float */
      float __tmp12__286 = 0.0f;/* float */
      int __tmp13__287 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__281 = 0)/*int*/; (i__281 < 16); (i__281 = (i__281 + 4))/*int*/) {{
          __tmp8__282 = BUFFER_20_21[TAIL_20_21+i__281];
;
          __push_21_22(__tmp8__282);
          (__tmp10__284 = (i__281 + 1))/*int*/;
          __tmp9__283 = BUFFER_20_21[TAIL_20_21+__tmp10__284];
;
          __push_21_22(__tmp9__283);
        }
      }
      for ((i__281 = 2)/*int*/; (i__281 < 16); (i__281 = (i__281 + 4))/*int*/) {{
          __tmp11__285 = BUFFER_20_21[TAIL_20_21+i__281];
;
          __push_21_22(__tmp11__285);
          (__tmp13__287 = (i__281 + 1))/*int*/;
          __tmp12__286 = BUFFER_20_21[TAIL_20_21+__tmp13__287];
;
          __push_21_22(__tmp12__286);
        }
      }
      __pop_20_21(16);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 8 pop: 8 push 8
// init counts: 0 steady counts: 32

// ClusterFusion isEliminated: false



int __number_of_iterations_22;
int __counter_22 = 0;
int __steady_22 = 0;
int __tmp_22 = 0;
int __tmp2_22 = 0;
int *__state_flag_22 = NULL;
thread_info *__thread_22 = NULL;



void save_peek_buffer__22(object_write_buffer *buf);
void load_peek_buffer__22(object_write_buffer *buf);
void save_file_pointer__22(object_write_buffer *buf);
void load_file_pointer__22(object_write_buffer *buf);

inline void check_status__22() {
  check_thread_status(__state_flag_22, __thread_22);
}

void check_status_during_io__22() {
  check_thread_status_during_io(__state_flag_22, __thread_22);
}

void __init_thread_info_22(thread_info *info) {
  __state_flag_22 = info->get_state_flag();
}

thread_info *__get_thread_info_22() {
  if (__thread_22 != NULL) return __thread_22;
  __thread_22 = new thread_info(22, check_status_during_io__22);
  __init_thread_info_22(__thread_22);
  return __thread_22;
}

void __declare_sockets_22() {
  init_instance::add_incoming(21,22, DATA_SOCKET);
  init_instance::add_outgoing(22,23, DATA_SOCKET);
}

void __init_sockets_22(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_22() {
}

void __peek_sockets_22() {
}

 
void init_FFTReorderSimple__298_51__22();
inline void check_status__22();

void work_FFTReorderSimple__298_51__22(int);


inline float __pop_21_22() {
float res=BUFFER_21_22[TAIL_21_22];
TAIL_21_22++;
return res;
}

inline void __pop_21_22(int n) {
TAIL_21_22+=n;
}

inline float __peek_21_22(int offs) {
return BUFFER_21_22[TAIL_21_22+offs];
}



inline void __push_22_23(float data) {
BUFFER_22_23[HEAD_22_23]=data;
HEAD_22_23++;
}



 
void init_FFTReorderSimple__298_51__22(){
}
void save_file_pointer__22(object_write_buffer *buf) {}
void load_file_pointer__22(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__298_51__22(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__291 = 0;/* int */
      float __tmp8__292 = 0.0f;/* float */
      float __tmp9__293 = 0.0f;/* float */
      int __tmp10__294 = 0;/* int */
      float __tmp11__295 = 0.0f;/* float */
      float __tmp12__296 = 0.0f;/* float */
      int __tmp13__297 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__291 = 0)/*int*/; (i__291 < 8); (i__291 = (i__291 + 4))/*int*/) {{
          __tmp8__292 = BUFFER_21_22[TAIL_21_22+i__291];
;
          __push_22_23(__tmp8__292);
          (__tmp10__294 = (i__291 + 1))/*int*/;
          __tmp9__293 = BUFFER_21_22[TAIL_21_22+__tmp10__294];
;
          __push_22_23(__tmp9__293);
        }
      }
      for ((i__291 = 2)/*int*/; (i__291 < 8); (i__291 = (i__291 + 4))/*int*/) {{
          __tmp11__295 = BUFFER_21_22[TAIL_21_22+i__291];
;
          __push_22_23(__tmp11__295);
          (__tmp13__297 = (i__291 + 1))/*int*/;
          __tmp12__296 = BUFFER_21_22[TAIL_21_22+__tmp13__297];
;
          __push_22_23(__tmp12__296);
        }
      }
      __pop_21_22(8);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 4 pop: 4 push 4
// init counts: 0 steady counts: 64

// ClusterFusion isEliminated: false



int __number_of_iterations_23;
int __counter_23 = 0;
int __steady_23 = 0;
int __tmp_23 = 0;
int __tmp2_23 = 0;
int *__state_flag_23 = NULL;
thread_info *__thread_23 = NULL;



float w__299__23[2] = {0};
void save_peek_buffer__23(object_write_buffer *buf);
void load_peek_buffer__23(object_write_buffer *buf);
void save_file_pointer__23(object_write_buffer *buf);
void load_file_pointer__23(object_write_buffer *buf);

inline void check_status__23() {
  check_thread_status(__state_flag_23, __thread_23);
}

void check_status_during_io__23() {
  check_thread_status_during_io(__state_flag_23, __thread_23);
}

void __init_thread_info_23(thread_info *info) {
  __state_flag_23 = info->get_state_flag();
}

thread_info *__get_thread_info_23() {
  if (__thread_23 != NULL) return __thread_23;
  __thread_23 = new thread_info(23, check_status_during_io__23);
  __init_thread_info_23(__thread_23);
  return __thread_23;
}

void __declare_sockets_23() {
  init_instance::add_incoming(22,23, DATA_SOCKET);
  init_instance::add_outgoing(23,24, DATA_SOCKET);
}

void __init_sockets_23(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_23() {
}

void __peek_sockets_23() {
}

 
void init_CombineDFT__323_52__23();
inline void check_status__23();

void work_CombineDFT__323_52__23(int);


inline float __pop_22_23() {
float res=BUFFER_22_23[TAIL_22_23];
TAIL_22_23++;
return res;
}

inline void __pop_22_23(int n) {
TAIL_22_23+=n;
}

inline float __peek_22_23(int offs) {
return BUFFER_22_23[TAIL_22_23+offs];
}



inline void __push_23_24(float data) {
BUFFER_23_24[HEAD_23_24]=data;
HEAD_23_24++;
}



 
void init_CombineDFT__323_52__23(){
  float real__318 = 0.0f;/* float */
  float imag__319 = 0.0f;/* float */
  float next_real__320 = 0.0f;/* float */float next_imag__321 = 0.0f;/* float */
  int i__322 = 0;/* int */

  (real__318 = ((float)1.0))/*float*/;
  (imag__319 = ((float)0.0))/*float*/;
  for ((i__322 = 0)/*int*/; (i__322 < 2); (i__322 = (i__322 + 2))/*int*/) {{
      (((w__299__23)[(int)i__322]) = real__318)/*float*/;
      (((w__299__23)[(int)(i__322 + 1)]) = imag__319)/*float*/;
      (next_real__320 = ((real__318 * ((float)-1.0)) - (imag__319 * ((float)8.742278E-8))))/*float*/;
      (next_imag__321 = ((real__318 * ((float)8.742278E-8)) + (imag__319 * ((float)-1.0))))/*float*/;
      (real__318 = next_real__320)/*float*/;
      (imag__319 = next_imag__321)/*float*/;
    }
  }
}
void save_file_pointer__23(object_write_buffer *buf) {}
void load_file_pointer__23(object_write_buffer *buf) {}
 
void work_CombineDFT__323_52__23(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__302 = 0;/* int */
      float results__303[4] = {0};/* float[4] */
      int i_plus_1__304 = 0;/* int */
      float y0_r__305 = 0.0f;/* float */
      float y0_i__306 = 0.0f;/* float */
      float y1_r__307 = 0.0f;/* float */
      float __tmp14__308 = 0.0f;/* float */
      int __tmp15__309 = 0;/* int */
      float y1_i__310 = 0.0f;/* float */
      float __tmp16__311 = 0.0f;/* float */
      int __tmp17__312 = 0;/* int */
      float weight_real__313 = 0.0f;/* float */
      float weight_imag__314 = 0.0f;/* float */
      float y1w_r__315 = 0.0f;/* float */
      float y1w_i__316 = 0.0f;/* float */
      float v__317 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__302 = 0)/*int*/; (i__302 < 2); (i__302 = (i__302 + 2))/*int*/) {{
          (i_plus_1__304 = (i__302 + 1))/*int*/;
          y0_r__305 = BUFFER_22_23[TAIL_22_23+i__302];
;
          y0_i__306 = BUFFER_22_23[TAIL_22_23+i_plus_1__304];
;
          (__tmp15__309 = (2 + i__302))/*int*/;
          __tmp14__308 = BUFFER_22_23[TAIL_22_23+__tmp15__309];
;
          (y1_r__307 = __tmp14__308)/*float*/;
          (__tmp17__312 = (2 + i_plus_1__304))/*int*/;
          __tmp16__311 = BUFFER_22_23[TAIL_22_23+__tmp17__312];
;
          (y1_i__310 = __tmp16__311)/*float*/;
          (weight_real__313 = ((w__299__23)[(int)i__302]))/*float*/;
          (weight_imag__314 = ((w__299__23)[(int)i_plus_1__304]))/*float*/;
          (y1w_r__315 = ((y1_r__307 * weight_real__313) - (y1_i__310 * weight_imag__314)))/*float*/;
          (y1w_i__316 = ((y1_r__307 * weight_imag__314) + (y1_i__310 * weight_real__313)))/*float*/;
          ((results__303[(int)i__302]) = (y0_r__305 + y1w_r__315))/*float*/;
          ((results__303[(int)(i__302 + 1)]) = (y0_i__306 + y1w_i__316))/*float*/;
          ((results__303[(int)(2 + i__302)]) = (y0_r__305 - y1w_r__315))/*float*/;
          ((results__303[(int)((2 + i__302) + 1)]) = (y0_i__306 - y1w_i__316))/*float*/;
        }
      }
      for ((i__302 = 0)/*int*/; (i__302 < 4); (i__302++)) {{
          __pop_22_23();
          (v__317 = (results__303[(int)i__302]))/*float*/;
          __push_23_24(v__317);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 8 pop: 8 push 8
// init counts: 0 steady counts: 32

// ClusterFusion isEliminated: false



int __number_of_iterations_24;
int __counter_24 = 0;
int __steady_24 = 0;
int __tmp_24 = 0;
int __tmp2_24 = 0;
int *__state_flag_24 = NULL;
thread_info *__thread_24 = NULL;



float w__324__24[4] = {0};
void save_peek_buffer__24(object_write_buffer *buf);
void load_peek_buffer__24(object_write_buffer *buf);
void save_file_pointer__24(object_write_buffer *buf);
void load_file_pointer__24(object_write_buffer *buf);

inline void check_status__24() {
  check_thread_status(__state_flag_24, __thread_24);
}

void check_status_during_io__24() {
  check_thread_status_during_io(__state_flag_24, __thread_24);
}

void __init_thread_info_24(thread_info *info) {
  __state_flag_24 = info->get_state_flag();
}

thread_info *__get_thread_info_24() {
  if (__thread_24 != NULL) return __thread_24;
  __thread_24 = new thread_info(24, check_status_during_io__24);
  __init_thread_info_24(__thread_24);
  return __thread_24;
}

void __declare_sockets_24() {
  init_instance::add_incoming(23,24, DATA_SOCKET);
  init_instance::add_outgoing(24,25, DATA_SOCKET);
}

void __init_sockets_24(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_24() {
}

void __peek_sockets_24() {
}

 
void init_CombineDFT__348_53__24();
inline void check_status__24();

void work_CombineDFT__348_53__24(int);


inline float __pop_23_24() {
float res=BUFFER_23_24[TAIL_23_24];
TAIL_23_24++;
return res;
}

inline void __pop_23_24(int n) {
TAIL_23_24+=n;
}

inline float __peek_23_24(int offs) {
return BUFFER_23_24[TAIL_23_24+offs];
}



inline void __push_24_25(float data) {
BUFFER_24_25[HEAD_24_25]=data;
HEAD_24_25++;
}



 
void init_CombineDFT__348_53__24(){
  float real__343 = 0.0f;/* float */
  float imag__344 = 0.0f;/* float */
  float next_real__345 = 0.0f;/* float */float next_imag__346 = 0.0f;/* float */
  int i__347 = 0;/* int */

  (real__343 = ((float)1.0))/*float*/;
  (imag__344 = ((float)0.0))/*float*/;
  for ((i__347 = 0)/*int*/; (i__347 < 4); (i__347 = (i__347 + 2))/*int*/) {{
      (((w__324__24)[(int)i__347]) = real__343)/*float*/;
      (((w__324__24)[(int)(i__347 + 1)]) = imag__344)/*float*/;
      (next_real__345 = ((real__343 * ((float)-4.371139E-8)) - (imag__344 * ((float)-1.0))))/*float*/;
      (next_imag__346 = ((real__343 * ((float)-1.0)) + (imag__344 * ((float)-4.371139E-8))))/*float*/;
      (real__343 = next_real__345)/*float*/;
      (imag__344 = next_imag__346)/*float*/;
    }
  }
}
void save_file_pointer__24(object_write_buffer *buf) {}
void load_file_pointer__24(object_write_buffer *buf) {}
 
void work_CombineDFT__348_53__24(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__327 = 0;/* int */
      float results__328[8] = {0};/* float[8] */
      int i_plus_1__329 = 0;/* int */
      float y0_r__330 = 0.0f;/* float */
      float y0_i__331 = 0.0f;/* float */
      float y1_r__332 = 0.0f;/* float */
      float __tmp14__333 = 0.0f;/* float */
      int __tmp15__334 = 0;/* int */
      float y1_i__335 = 0.0f;/* float */
      float __tmp16__336 = 0.0f;/* float */
      int __tmp17__337 = 0;/* int */
      float weight_real__338 = 0.0f;/* float */
      float weight_imag__339 = 0.0f;/* float */
      float y1w_r__340 = 0.0f;/* float */
      float y1w_i__341 = 0.0f;/* float */
      float v__342 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__327 = 0)/*int*/; (i__327 < 4); (i__327 = (i__327 + 2))/*int*/) {{
          (i_plus_1__329 = (i__327 + 1))/*int*/;
          y0_r__330 = BUFFER_23_24[TAIL_23_24+i__327];
;
          y0_i__331 = BUFFER_23_24[TAIL_23_24+i_plus_1__329];
;
          (__tmp15__334 = (4 + i__327))/*int*/;
          __tmp14__333 = BUFFER_23_24[TAIL_23_24+__tmp15__334];
;
          (y1_r__332 = __tmp14__333)/*float*/;
          (__tmp17__337 = (4 + i_plus_1__329))/*int*/;
          __tmp16__336 = BUFFER_23_24[TAIL_23_24+__tmp17__337];
;
          (y1_i__335 = __tmp16__336)/*float*/;
          (weight_real__338 = ((w__324__24)[(int)i__327]))/*float*/;
          (weight_imag__339 = ((w__324__24)[(int)i_plus_1__329]))/*float*/;
          (y1w_r__340 = ((y1_r__332 * weight_real__338) - (y1_i__335 * weight_imag__339)))/*float*/;
          (y1w_i__341 = ((y1_r__332 * weight_imag__339) + (y1_i__335 * weight_real__338)))/*float*/;
          ((results__328[(int)i__327]) = (y0_r__330 + y1w_r__340))/*float*/;
          ((results__328[(int)(i__327 + 1)]) = (y0_i__331 + y1w_i__341))/*float*/;
          ((results__328[(int)(4 + i__327)]) = (y0_r__330 - y1w_r__340))/*float*/;
          ((results__328[(int)((4 + i__327) + 1)]) = (y0_i__331 - y1w_i__341))/*float*/;
        }
      }
      for ((i__327 = 0)/*int*/; (i__327 < 8); (i__327++)) {{
          __pop_23_24();
          (v__342 = (results__328[(int)i__327]))/*float*/;
          __push_24_25(v__342);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 16 pop: 16 push 16
// init counts: 0 steady counts: 16

// ClusterFusion isEliminated: false



int __number_of_iterations_25;
int __counter_25 = 0;
int __steady_25 = 0;
int __tmp_25 = 0;
int __tmp2_25 = 0;
int *__state_flag_25 = NULL;
thread_info *__thread_25 = NULL;



float w__349__25[8] = {0};
void save_peek_buffer__25(object_write_buffer *buf);
void load_peek_buffer__25(object_write_buffer *buf);
void save_file_pointer__25(object_write_buffer *buf);
void load_file_pointer__25(object_write_buffer *buf);

inline void check_status__25() {
  check_thread_status(__state_flag_25, __thread_25);
}

void check_status_during_io__25() {
  check_thread_status_during_io(__state_flag_25, __thread_25);
}

void __init_thread_info_25(thread_info *info) {
  __state_flag_25 = info->get_state_flag();
}

thread_info *__get_thread_info_25() {
  if (__thread_25 != NULL) return __thread_25;
  __thread_25 = new thread_info(25, check_status_during_io__25);
  __init_thread_info_25(__thread_25);
  return __thread_25;
}

void __declare_sockets_25() {
  init_instance::add_incoming(24,25, DATA_SOCKET);
  init_instance::add_outgoing(25,26, DATA_SOCKET);
}

void __init_sockets_25(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_25() {
}

void __peek_sockets_25() {
}

 
void init_CombineDFT__373_54__25();
inline void check_status__25();

void work_CombineDFT__373_54__25(int);


inline float __pop_24_25() {
float res=BUFFER_24_25[TAIL_24_25];
TAIL_24_25++;
return res;
}

inline void __pop_24_25(int n) {
TAIL_24_25+=n;
}

inline float __peek_24_25(int offs) {
return BUFFER_24_25[TAIL_24_25+offs];
}



inline void __push_25_26(float data) {
BUFFER_25_26[HEAD_25_26]=data;
HEAD_25_26++;
}



 
void init_CombineDFT__373_54__25(){
  float real__368 = 0.0f;/* float */
  float imag__369 = 0.0f;/* float */
  float next_real__370 = 0.0f;/* float */float next_imag__371 = 0.0f;/* float */
  int i__372 = 0;/* int */

  (real__368 = ((float)1.0))/*float*/;
  (imag__369 = ((float)0.0))/*float*/;
  for ((i__372 = 0)/*int*/; (i__372 < 8); (i__372 = (i__372 + 2))/*int*/) {{
      (((w__349__25)[(int)i__372]) = real__368)/*float*/;
      (((w__349__25)[(int)(i__372 + 1)]) = imag__369)/*float*/;
      (next_real__370 = ((real__368 * ((float)0.70710677)) - (imag__369 * ((float)-0.70710677))))/*float*/;
      (next_imag__371 = ((real__368 * ((float)-0.70710677)) + (imag__369 * ((float)0.70710677))))/*float*/;
      (real__368 = next_real__370)/*float*/;
      (imag__369 = next_imag__371)/*float*/;
    }
  }
}
void save_file_pointer__25(object_write_buffer *buf) {}
void load_file_pointer__25(object_write_buffer *buf) {}
 
void work_CombineDFT__373_54__25(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__352 = 0;/* int */
      float results__353[16] = {0};/* float[16] */
      int i_plus_1__354 = 0;/* int */
      float y0_r__355 = 0.0f;/* float */
      float y0_i__356 = 0.0f;/* float */
      float y1_r__357 = 0.0f;/* float */
      float __tmp14__358 = 0.0f;/* float */
      int __tmp15__359 = 0;/* int */
      float y1_i__360 = 0.0f;/* float */
      float __tmp16__361 = 0.0f;/* float */
      int __tmp17__362 = 0;/* int */
      float weight_real__363 = 0.0f;/* float */
      float weight_imag__364 = 0.0f;/* float */
      float y1w_r__365 = 0.0f;/* float */
      float y1w_i__366 = 0.0f;/* float */
      float v__367 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__352 = 0)/*int*/; (i__352 < 8); (i__352 = (i__352 + 2))/*int*/) {{
          (i_plus_1__354 = (i__352 + 1))/*int*/;
          y0_r__355 = BUFFER_24_25[TAIL_24_25+i__352];
;
          y0_i__356 = BUFFER_24_25[TAIL_24_25+i_plus_1__354];
;
          (__tmp15__359 = (8 + i__352))/*int*/;
          __tmp14__358 = BUFFER_24_25[TAIL_24_25+__tmp15__359];
;
          (y1_r__357 = __tmp14__358)/*float*/;
          (__tmp17__362 = (8 + i_plus_1__354))/*int*/;
          __tmp16__361 = BUFFER_24_25[TAIL_24_25+__tmp17__362];
;
          (y1_i__360 = __tmp16__361)/*float*/;
          (weight_real__363 = ((w__349__25)[(int)i__352]))/*float*/;
          (weight_imag__364 = ((w__349__25)[(int)i_plus_1__354]))/*float*/;
          (y1w_r__365 = ((y1_r__357 * weight_real__363) - (y1_i__360 * weight_imag__364)))/*float*/;
          (y1w_i__366 = ((y1_r__357 * weight_imag__364) + (y1_i__360 * weight_real__363)))/*float*/;
          ((results__353[(int)i__352]) = (y0_r__355 + y1w_r__365))/*float*/;
          ((results__353[(int)(i__352 + 1)]) = (y0_i__356 + y1w_i__366))/*float*/;
          ((results__353[(int)(8 + i__352)]) = (y0_r__355 - y1w_r__365))/*float*/;
          ((results__353[(int)((8 + i__352) + 1)]) = (y0_i__356 - y1w_i__366))/*float*/;
        }
      }
      for ((i__352 = 0)/*int*/; (i__352 < 16); (i__352++)) {{
          __pop_24_25();
          (v__367 = (results__353[(int)i__352]))/*float*/;
          __push_25_26(v__367);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 32 pop: 32 push 32
// init counts: 0 steady counts: 8

// ClusterFusion isEliminated: false



int __number_of_iterations_26;
int __counter_26 = 0;
int __steady_26 = 0;
int __tmp_26 = 0;
int __tmp2_26 = 0;
int *__state_flag_26 = NULL;
thread_info *__thread_26 = NULL;



float w__374__26[16] = {0};
void save_peek_buffer__26(object_write_buffer *buf);
void load_peek_buffer__26(object_write_buffer *buf);
void save_file_pointer__26(object_write_buffer *buf);
void load_file_pointer__26(object_write_buffer *buf);

inline void check_status__26() {
  check_thread_status(__state_flag_26, __thread_26);
}

void check_status_during_io__26() {
  check_thread_status_during_io(__state_flag_26, __thread_26);
}

void __init_thread_info_26(thread_info *info) {
  __state_flag_26 = info->get_state_flag();
}

thread_info *__get_thread_info_26() {
  if (__thread_26 != NULL) return __thread_26;
  __thread_26 = new thread_info(26, check_status_during_io__26);
  __init_thread_info_26(__thread_26);
  return __thread_26;
}

void __declare_sockets_26() {
  init_instance::add_incoming(25,26, DATA_SOCKET);
  init_instance::add_outgoing(26,27, DATA_SOCKET);
}

void __init_sockets_26(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_26() {
}

void __peek_sockets_26() {
}

 
void init_CombineDFT__398_55__26();
inline void check_status__26();

void work_CombineDFT__398_55__26(int);


inline float __pop_25_26() {
float res=BUFFER_25_26[TAIL_25_26];
TAIL_25_26++;
return res;
}

inline void __pop_25_26(int n) {
TAIL_25_26+=n;
}

inline float __peek_25_26(int offs) {
return BUFFER_25_26[TAIL_25_26+offs];
}



inline void __push_26_27(float data) {
BUFFER_26_27[HEAD_26_27]=data;
HEAD_26_27++;
}



 
void init_CombineDFT__398_55__26(){
  float real__393 = 0.0f;/* float */
  float imag__394 = 0.0f;/* float */
  float next_real__395 = 0.0f;/* float */float next_imag__396 = 0.0f;/* float */
  int i__397 = 0;/* int */

  (real__393 = ((float)1.0))/*float*/;
  (imag__394 = ((float)0.0))/*float*/;
  for ((i__397 = 0)/*int*/; (i__397 < 16); (i__397 = (i__397 + 2))/*int*/) {{
      (((w__374__26)[(int)i__397]) = real__393)/*float*/;
      (((w__374__26)[(int)(i__397 + 1)]) = imag__394)/*float*/;
      (next_real__395 = ((real__393 * ((float)0.9238795)) - (imag__394 * ((float)-0.38268346))))/*float*/;
      (next_imag__396 = ((real__393 * ((float)-0.38268346)) + (imag__394 * ((float)0.9238795))))/*float*/;
      (real__393 = next_real__395)/*float*/;
      (imag__394 = next_imag__396)/*float*/;
    }
  }
}
void save_file_pointer__26(object_write_buffer *buf) {}
void load_file_pointer__26(object_write_buffer *buf) {}
 
void work_CombineDFT__398_55__26(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__377 = 0;/* int */
      float results__378[32] = {0};/* float[32] */
      int i_plus_1__379 = 0;/* int */
      float y0_r__380 = 0.0f;/* float */
      float y0_i__381 = 0.0f;/* float */
      float y1_r__382 = 0.0f;/* float */
      float __tmp14__383 = 0.0f;/* float */
      int __tmp15__384 = 0;/* int */
      float y1_i__385 = 0.0f;/* float */
      float __tmp16__386 = 0.0f;/* float */
      int __tmp17__387 = 0;/* int */
      float weight_real__388 = 0.0f;/* float */
      float weight_imag__389 = 0.0f;/* float */
      float y1w_r__390 = 0.0f;/* float */
      float y1w_i__391 = 0.0f;/* float */
      float v__392 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__377 = 0)/*int*/; (i__377 < 16); (i__377 = (i__377 + 2))/*int*/) {{
          (i_plus_1__379 = (i__377 + 1))/*int*/;
          y0_r__380 = BUFFER_25_26[TAIL_25_26+i__377];
;
          y0_i__381 = BUFFER_25_26[TAIL_25_26+i_plus_1__379];
;
          (__tmp15__384 = (16 + i__377))/*int*/;
          __tmp14__383 = BUFFER_25_26[TAIL_25_26+__tmp15__384];
;
          (y1_r__382 = __tmp14__383)/*float*/;
          (__tmp17__387 = (16 + i_plus_1__379))/*int*/;
          __tmp16__386 = BUFFER_25_26[TAIL_25_26+__tmp17__387];
;
          (y1_i__385 = __tmp16__386)/*float*/;
          (weight_real__388 = ((w__374__26)[(int)i__377]))/*float*/;
          (weight_imag__389 = ((w__374__26)[(int)i_plus_1__379]))/*float*/;
          (y1w_r__390 = ((y1_r__382 * weight_real__388) - (y1_i__385 * weight_imag__389)))/*float*/;
          (y1w_i__391 = ((y1_r__382 * weight_imag__389) + (y1_i__385 * weight_real__388)))/*float*/;
          ((results__378[(int)i__377]) = (y0_r__380 + y1w_r__390))/*float*/;
          ((results__378[(int)(i__377 + 1)]) = (y0_i__381 + y1w_i__391))/*float*/;
          ((results__378[(int)(16 + i__377)]) = (y0_r__380 - y1w_r__390))/*float*/;
          ((results__378[(int)((16 + i__377) + 1)]) = (y0_i__381 - y1w_i__391))/*float*/;
        }
      }
      for ((i__377 = 0)/*int*/; (i__377 < 32); (i__377++)) {{
          __pop_25_26();
          (v__392 = (results__378[(int)i__377]))/*float*/;
          __push_26_27(v__392);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 64 pop: 64 push 64
// init counts: 0 steady counts: 4

// ClusterFusion isEliminated: false



int __number_of_iterations_27;
int __counter_27 = 0;
int __steady_27 = 0;
int __tmp_27 = 0;
int __tmp2_27 = 0;
int *__state_flag_27 = NULL;
thread_info *__thread_27 = NULL;



float w__399__27[32] = {0};
void save_peek_buffer__27(object_write_buffer *buf);
void load_peek_buffer__27(object_write_buffer *buf);
void save_file_pointer__27(object_write_buffer *buf);
void load_file_pointer__27(object_write_buffer *buf);

inline void check_status__27() {
  check_thread_status(__state_flag_27, __thread_27);
}

void check_status_during_io__27() {
  check_thread_status_during_io(__state_flag_27, __thread_27);
}

void __init_thread_info_27(thread_info *info) {
  __state_flag_27 = info->get_state_flag();
}

thread_info *__get_thread_info_27() {
  if (__thread_27 != NULL) return __thread_27;
  __thread_27 = new thread_info(27, check_status_during_io__27);
  __init_thread_info_27(__thread_27);
  return __thread_27;
}

void __declare_sockets_27() {
  init_instance::add_incoming(26,27, DATA_SOCKET);
  init_instance::add_outgoing(27,28, DATA_SOCKET);
}

void __init_sockets_27(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_27() {
}

void __peek_sockets_27() {
}

 
void init_CombineDFT__423_56__27();
inline void check_status__27();

void work_CombineDFT__423_56__27(int);


inline float __pop_26_27() {
float res=BUFFER_26_27[TAIL_26_27];
TAIL_26_27++;
return res;
}

inline void __pop_26_27(int n) {
TAIL_26_27+=n;
}

inline float __peek_26_27(int offs) {
return BUFFER_26_27[TAIL_26_27+offs];
}



inline void __push_27_28(float data) {
BUFFER_27_28[HEAD_27_28]=data;
HEAD_27_28++;
}



 
void init_CombineDFT__423_56__27(){
  float real__418 = 0.0f;/* float */
  float imag__419 = 0.0f;/* float */
  float next_real__420 = 0.0f;/* float */float next_imag__421 = 0.0f;/* float */
  int i__422 = 0;/* int */

  (real__418 = ((float)1.0))/*float*/;
  (imag__419 = ((float)0.0))/*float*/;
  for ((i__422 = 0)/*int*/; (i__422 < 32); (i__422 = (i__422 + 2))/*int*/) {{
      (((w__399__27)[(int)i__422]) = real__418)/*float*/;
      (((w__399__27)[(int)(i__422 + 1)]) = imag__419)/*float*/;
      (next_real__420 = ((real__418 * ((float)0.98078525)) - (imag__419 * ((float)-0.19509032))))/*float*/;
      (next_imag__421 = ((real__418 * ((float)-0.19509032)) + (imag__419 * ((float)0.98078525))))/*float*/;
      (real__418 = next_real__420)/*float*/;
      (imag__419 = next_imag__421)/*float*/;
    }
  }
}
void save_file_pointer__27(object_write_buffer *buf) {}
void load_file_pointer__27(object_write_buffer *buf) {}
 
void work_CombineDFT__423_56__27(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__402 = 0;/* int */
      float results__403[64] = {0};/* float[64] */
      int i_plus_1__404 = 0;/* int */
      float y0_r__405 = 0.0f;/* float */
      float y0_i__406 = 0.0f;/* float */
      float y1_r__407 = 0.0f;/* float */
      float __tmp14__408 = 0.0f;/* float */
      int __tmp15__409 = 0;/* int */
      float y1_i__410 = 0.0f;/* float */
      float __tmp16__411 = 0.0f;/* float */
      int __tmp17__412 = 0;/* int */
      float weight_real__413 = 0.0f;/* float */
      float weight_imag__414 = 0.0f;/* float */
      float y1w_r__415 = 0.0f;/* float */
      float y1w_i__416 = 0.0f;/* float */
      float v__417 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__402 = 0)/*int*/; (i__402 < 32); (i__402 = (i__402 + 2))/*int*/) {{
          (i_plus_1__404 = (i__402 + 1))/*int*/;
          y0_r__405 = BUFFER_26_27[TAIL_26_27+i__402];
;
          y0_i__406 = BUFFER_26_27[TAIL_26_27+i_plus_1__404];
;
          (__tmp15__409 = (32 + i__402))/*int*/;
          __tmp14__408 = BUFFER_26_27[TAIL_26_27+__tmp15__409];
;
          (y1_r__407 = __tmp14__408)/*float*/;
          (__tmp17__412 = (32 + i_plus_1__404))/*int*/;
          __tmp16__411 = BUFFER_26_27[TAIL_26_27+__tmp17__412];
;
          (y1_i__410 = __tmp16__411)/*float*/;
          (weight_real__413 = ((w__399__27)[(int)i__402]))/*float*/;
          (weight_imag__414 = ((w__399__27)[(int)i_plus_1__404]))/*float*/;
          (y1w_r__415 = ((y1_r__407 * weight_real__413) - (y1_i__410 * weight_imag__414)))/*float*/;
          (y1w_i__416 = ((y1_r__407 * weight_imag__414) + (y1_i__410 * weight_real__413)))/*float*/;
          ((results__403[(int)i__402]) = (y0_r__405 + y1w_r__415))/*float*/;
          ((results__403[(int)(i__402 + 1)]) = (y0_i__406 + y1w_i__416))/*float*/;
          ((results__403[(int)(32 + i__402)]) = (y0_r__405 - y1w_r__415))/*float*/;
          ((results__403[(int)((32 + i__402) + 1)]) = (y0_i__406 - y1w_i__416))/*float*/;
        }
      }
      for ((i__402 = 0)/*int*/; (i__402 < 64); (i__402++)) {{
          __pop_26_27();
          (v__417 = (results__403[(int)i__402]))/*float*/;
          __push_27_28(v__417);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 128 pop: 128 push 128
// init counts: 0 steady counts: 2

// ClusterFusion isEliminated: false



int __number_of_iterations_28;
int __counter_28 = 0;
int __steady_28 = 0;
int __tmp_28 = 0;
int __tmp2_28 = 0;
int *__state_flag_28 = NULL;
thread_info *__thread_28 = NULL;



float w__424__28[64] = {0};
void save_peek_buffer__28(object_write_buffer *buf);
void load_peek_buffer__28(object_write_buffer *buf);
void save_file_pointer__28(object_write_buffer *buf);
void load_file_pointer__28(object_write_buffer *buf);

inline void check_status__28() {
  check_thread_status(__state_flag_28, __thread_28);
}

void check_status_during_io__28() {
  check_thread_status_during_io(__state_flag_28, __thread_28);
}

void __init_thread_info_28(thread_info *info) {
  __state_flag_28 = info->get_state_flag();
}

thread_info *__get_thread_info_28() {
  if (__thread_28 != NULL) return __thread_28;
  __thread_28 = new thread_info(28, check_status_during_io__28);
  __init_thread_info_28(__thread_28);
  return __thread_28;
}

void __declare_sockets_28() {
  init_instance::add_incoming(27,28, DATA_SOCKET);
  init_instance::add_outgoing(28,29, DATA_SOCKET);
}

void __init_sockets_28(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_28() {
}

void __peek_sockets_28() {
}

 
void init_CombineDFT__448_57__28();
inline void check_status__28();

void work_CombineDFT__448_57__28(int);


inline float __pop_27_28() {
float res=BUFFER_27_28[TAIL_27_28];
TAIL_27_28++;
return res;
}

inline void __pop_27_28(int n) {
TAIL_27_28+=n;
}

inline float __peek_27_28(int offs) {
return BUFFER_27_28[TAIL_27_28+offs];
}



inline void __push_28_29(float data) {
BUFFER_28_29[HEAD_28_29]=data;
HEAD_28_29++;
}



 
void init_CombineDFT__448_57__28(){
  float real__443 = 0.0f;/* float */
  float imag__444 = 0.0f;/* float */
  float next_real__445 = 0.0f;/* float */float next_imag__446 = 0.0f;/* float */
  int i__447 = 0;/* int */

  (real__443 = ((float)1.0))/*float*/;
  (imag__444 = ((float)0.0))/*float*/;
  for ((i__447 = 0)/*int*/; (i__447 < 64); (i__447 = (i__447 + 2))/*int*/) {{
      (((w__424__28)[(int)i__447]) = real__443)/*float*/;
      (((w__424__28)[(int)(i__447 + 1)]) = imag__444)/*float*/;
      (next_real__445 = ((real__443 * ((float)0.9951847)) - (imag__444 * ((float)-0.09801714))))/*float*/;
      (next_imag__446 = ((real__443 * ((float)-0.09801714)) + (imag__444 * ((float)0.9951847))))/*float*/;
      (real__443 = next_real__445)/*float*/;
      (imag__444 = next_imag__446)/*float*/;
    }
  }
}
void save_file_pointer__28(object_write_buffer *buf) {}
void load_file_pointer__28(object_write_buffer *buf) {}
 
void work_CombineDFT__448_57__28(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__427 = 0;/* int */
      float results__428[128] = {0};/* float[128] */
      int i_plus_1__429 = 0;/* int */
      float y0_r__430 = 0.0f;/* float */
      float y0_i__431 = 0.0f;/* float */
      float y1_r__432 = 0.0f;/* float */
      float __tmp14__433 = 0.0f;/* float */
      int __tmp15__434 = 0;/* int */
      float y1_i__435 = 0.0f;/* float */
      float __tmp16__436 = 0.0f;/* float */
      int __tmp17__437 = 0;/* int */
      float weight_real__438 = 0.0f;/* float */
      float weight_imag__439 = 0.0f;/* float */
      float y1w_r__440 = 0.0f;/* float */
      float y1w_i__441 = 0.0f;/* float */
      float v__442 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__427 = 0)/*int*/; (i__427 < 64); (i__427 = (i__427 + 2))/*int*/) {{
          (i_plus_1__429 = (i__427 + 1))/*int*/;
          y0_r__430 = BUFFER_27_28[TAIL_27_28+i__427];
;
          y0_i__431 = BUFFER_27_28[TAIL_27_28+i_plus_1__429];
;
          (__tmp15__434 = (64 + i__427))/*int*/;
          __tmp14__433 = BUFFER_27_28[TAIL_27_28+__tmp15__434];
;
          (y1_r__432 = __tmp14__433)/*float*/;
          (__tmp17__437 = (64 + i_plus_1__429))/*int*/;
          __tmp16__436 = BUFFER_27_28[TAIL_27_28+__tmp17__437];
;
          (y1_i__435 = __tmp16__436)/*float*/;
          (weight_real__438 = ((w__424__28)[(int)i__427]))/*float*/;
          (weight_imag__439 = ((w__424__28)[(int)i_plus_1__429]))/*float*/;
          (y1w_r__440 = ((y1_r__432 * weight_real__438) - (y1_i__435 * weight_imag__439)))/*float*/;
          (y1w_i__441 = ((y1_r__432 * weight_imag__439) + (y1_i__435 * weight_real__438)))/*float*/;
          ((results__428[(int)i__427]) = (y0_r__430 + y1w_r__440))/*float*/;
          ((results__428[(int)(i__427 + 1)]) = (y0_i__431 + y1w_i__441))/*float*/;
          ((results__428[(int)(64 + i__427)]) = (y0_r__430 - y1w_r__440))/*float*/;
          ((results__428[(int)((64 + i__427) + 1)]) = (y0_i__431 - y1w_i__441))/*float*/;
        }
      }
      for ((i__427 = 0)/*int*/; (i__427 < 128); (i__427++)) {{
          __pop_27_28();
          (v__442 = (results__428[(int)i__427]))/*float*/;
          __push_28_29(v__442);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 256 pop: 256 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_29;
int __counter_29 = 0;
int __steady_29 = 0;
int __tmp_29 = 0;
int __tmp2_29 = 0;
int *__state_flag_29 = NULL;
thread_info *__thread_29 = NULL;



float w__449__29[128] = {0};
void save_peek_buffer__29(object_write_buffer *buf);
void load_peek_buffer__29(object_write_buffer *buf);
void save_file_pointer__29(object_write_buffer *buf);
void load_file_pointer__29(object_write_buffer *buf);

inline void check_status__29() {
  check_thread_status(__state_flag_29, __thread_29);
}

void check_status_during_io__29() {
  check_thread_status_during_io(__state_flag_29, __thread_29);
}

void __init_thread_info_29(thread_info *info) {
  __state_flag_29 = info->get_state_flag();
}

thread_info *__get_thread_info_29() {
  if (__thread_29 != NULL) return __thread_29;
  __thread_29 = new thread_info(29, check_status_during_io__29);
  __init_thread_info_29(__thread_29);
  return __thread_29;
}

void __declare_sockets_29() {
  init_instance::add_incoming(28,29, DATA_SOCKET);
  init_instance::add_outgoing(29,15, DATA_SOCKET);
}

void __init_sockets_29(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_29() {
}

void __peek_sockets_29() {
}

 
void init_CombineDFT__473_58__29();
inline void check_status__29();

void work_CombineDFT__473_58__29(int);


inline float __pop_28_29() {
float res=BUFFER_28_29[TAIL_28_29];
TAIL_28_29++;
return res;
}

inline void __pop_28_29(int n) {
TAIL_28_29+=n;
}

inline float __peek_28_29(int offs) {
return BUFFER_28_29[TAIL_28_29+offs];
}



inline void __push_29_15(float data) {
BUFFER_29_15[HEAD_29_15]=data;
HEAD_29_15++;
}



 
void init_CombineDFT__473_58__29(){
  float real__468 = 0.0f;/* float */
  float imag__469 = 0.0f;/* float */
  float next_real__470 = 0.0f;/* float */float next_imag__471 = 0.0f;/* float */
  int i__472 = 0;/* int */

  (real__468 = ((float)1.0))/*float*/;
  (imag__469 = ((float)0.0))/*float*/;
  for ((i__472 = 0)/*int*/; (i__472 < 128); (i__472 = (i__472 + 2))/*int*/) {{
      (((w__449__29)[(int)i__472]) = real__468)/*float*/;
      (((w__449__29)[(int)(i__472 + 1)]) = imag__469)/*float*/;
      (next_real__470 = ((real__468 * ((float)0.99879545)) - (imag__469 * ((float)-0.049067676))))/*float*/;
      (next_imag__471 = ((real__468 * ((float)-0.049067676)) + (imag__469 * ((float)0.99879545))))/*float*/;
      (real__468 = next_real__470)/*float*/;
      (imag__469 = next_imag__471)/*float*/;
    }
  }
}
void save_file_pointer__29(object_write_buffer *buf) {}
void load_file_pointer__29(object_write_buffer *buf) {}
 
void work_CombineDFT__473_58__29(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__452 = 0;/* int */
      float results__453[256] = {0};/* float[256] */
      int i_plus_1__454 = 0;/* int */
      float y0_r__455 = 0.0f;/* float */
      float y0_i__456 = 0.0f;/* float */
      float y1_r__457 = 0.0f;/* float */
      float __tmp14__458 = 0.0f;/* float */
      int __tmp15__459 = 0;/* int */
      float y1_i__460 = 0.0f;/* float */
      float __tmp16__461 = 0.0f;/* float */
      int __tmp17__462 = 0;/* int */
      float weight_real__463 = 0.0f;/* float */
      float weight_imag__464 = 0.0f;/* float */
      float y1w_r__465 = 0.0f;/* float */
      float y1w_i__466 = 0.0f;/* float */
      float v__467 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__452 = 0)/*int*/; (i__452 < 128); (i__452 = (i__452 + 2))/*int*/) {{
          (i_plus_1__454 = (i__452 + 1))/*int*/;
          y0_r__455 = BUFFER_28_29[TAIL_28_29+i__452];
;
          y0_i__456 = BUFFER_28_29[TAIL_28_29+i_plus_1__454];
;
          (__tmp15__459 = (128 + i__452))/*int*/;
          __tmp14__458 = BUFFER_28_29[TAIL_28_29+__tmp15__459];
;
          (y1_r__457 = __tmp14__458)/*float*/;
          (__tmp17__462 = (128 + i_plus_1__454))/*int*/;
          __tmp16__461 = BUFFER_28_29[TAIL_28_29+__tmp17__462];
;
          (y1_i__460 = __tmp16__461)/*float*/;
          (weight_real__463 = ((w__449__29)[(int)i__452]))/*float*/;
          (weight_imag__464 = ((w__449__29)[(int)i_plus_1__454]))/*float*/;
          (y1w_r__465 = ((y1_r__457 * weight_real__463) - (y1_i__460 * weight_imag__464)))/*float*/;
          (y1w_i__466 = ((y1_r__457 * weight_imag__464) + (y1_i__460 * weight_real__463)))/*float*/;
          ((results__453[(int)i__452]) = (y0_r__455 + y1w_r__465))/*float*/;
          ((results__453[(int)(i__452 + 1)]) = (y0_i__456 + y1w_i__466))/*float*/;
          ((results__453[(int)(128 + i__452)]) = (y0_r__455 - y1w_r__465))/*float*/;
          ((results__453[(int)((128 + i__452) + 1)]) = (y0_i__456 - y1w_i__466))/*float*/;
        }
      }
      for ((i__452 = 0)/*int*/; (i__452 < 256); (i__452++)) {{
          __pop_28_29();
          (v__467 = (results__453[(int)i__452]))/*float*/;
          __push_29_15(v__467);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 128 pop: 128 push 128
// init counts: 0 steady counts: 2

// ClusterFusion isEliminated: false



int __number_of_iterations_3;
int __counter_3 = 0;
int __steady_3 = 0;
int __tmp_3 = 0;
int __tmp2_3 = 0;
int *__state_flag_3 = NULL;
thread_info *__thread_3 = NULL;



void save_peek_buffer__3(object_write_buffer *buf);
void load_peek_buffer__3(object_write_buffer *buf);
void save_file_pointer__3(object_write_buffer *buf);
void load_file_pointer__3(object_write_buffer *buf);

inline void check_status__3() {
  check_thread_status(__state_flag_3, __thread_3);
}

void check_status_during_io__3() {
  check_thread_status_during_io(__state_flag_3, __thread_3);
}

void __init_thread_info_3(thread_info *info) {
  __state_flag_3 = info->get_state_flag();
}

thread_info *__get_thread_info_3() {
  if (__thread_3 != NULL) return __thread_3;
  __thread_3 = new thread_info(3, check_status_during_io__3);
  __init_thread_info_3(__thread_3);
  return __thread_3;
}

void __declare_sockets_3() {
  init_instance::add_incoming(2,3, DATA_SOCKET);
  init_instance::add_outgoing(3,4, DATA_SOCKET);
}

void __init_sockets_3(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_3() {
}

void __peek_sockets_3() {
}

 
void init_FFTReorderSimple__23_34__3();
inline void check_status__3();

void work_FFTReorderSimple__23_34__3(int);


inline float __pop_2_3() {
float res=BUFFER_2_3[TAIL_2_3];
TAIL_2_3++;
return res;
}

inline void __pop_2_3(int n) {
TAIL_2_3+=n;
}

inline float __peek_2_3(int offs) {
return BUFFER_2_3[TAIL_2_3+offs];
}



inline void __push_3_4(float data) {
BUFFER_3_4[HEAD_3_4]=data;
HEAD_3_4++;
}



 
void init_FFTReorderSimple__23_34__3(){
}
void save_file_pointer__3(object_write_buffer *buf) {}
void load_file_pointer__3(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__23_34__3(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__16 = 0;/* int */
      float __tmp8__17 = 0.0f;/* float */
      float __tmp9__18 = 0.0f;/* float */
      int __tmp10__19 = 0;/* int */
      float __tmp11__20 = 0.0f;/* float */
      float __tmp12__21 = 0.0f;/* float */
      int __tmp13__22 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__16 = 0)/*int*/; (i__16 < 128); (i__16 = (i__16 + 4))/*int*/) {{
          __tmp8__17 = BUFFER_2_3[TAIL_2_3+i__16];
;
          __push_3_4(__tmp8__17);
          (__tmp10__19 = (i__16 + 1))/*int*/;
          __tmp9__18 = BUFFER_2_3[TAIL_2_3+__tmp10__19];
;
          __push_3_4(__tmp9__18);
        }
      }
      for ((i__16 = 2)/*int*/; (i__16 < 128); (i__16 = (i__16 + 4))/*int*/) {{
          __tmp11__20 = BUFFER_2_3[TAIL_2_3+i__16];
;
          __push_3_4(__tmp11__20);
          (__tmp13__22 = (i__16 + 1))/*int*/;
          __tmp12__21 = BUFFER_2_3[TAIL_2_3+__tmp13__22];
;
          __push_3_4(__tmp12__21);
        }
      }
      __pop_2_3(128);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 64 pop: 64 push 64
// init counts: 0 steady counts: 4

// ClusterFusion isEliminated: false



int __number_of_iterations_4;
int __counter_4 = 0;
int __steady_4 = 0;
int __tmp_4 = 0;
int __tmp2_4 = 0;
int *__state_flag_4 = NULL;
thread_info *__thread_4 = NULL;



void save_peek_buffer__4(object_write_buffer *buf);
void load_peek_buffer__4(object_write_buffer *buf);
void save_file_pointer__4(object_write_buffer *buf);
void load_file_pointer__4(object_write_buffer *buf);

inline void check_status__4() {
  check_thread_status(__state_flag_4, __thread_4);
}

void check_status_during_io__4() {
  check_thread_status_during_io(__state_flag_4, __thread_4);
}

void __init_thread_info_4(thread_info *info) {
  __state_flag_4 = info->get_state_flag();
}

thread_info *__get_thread_info_4() {
  if (__thread_4 != NULL) return __thread_4;
  __thread_4 = new thread_info(4, check_status_during_io__4);
  __init_thread_info_4(__thread_4);
  return __thread_4;
}

void __declare_sockets_4() {
  init_instance::add_incoming(3,4, DATA_SOCKET);
  init_instance::add_outgoing(4,5, DATA_SOCKET);
}

void __init_sockets_4(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_4() {
}

void __peek_sockets_4() {
}

 
void init_FFTReorderSimple__33_35__4();
inline void check_status__4();

void work_FFTReorderSimple__33_35__4(int);


inline float __pop_3_4() {
float res=BUFFER_3_4[TAIL_3_4];
TAIL_3_4++;
return res;
}

inline void __pop_3_4(int n) {
TAIL_3_4+=n;
}

inline float __peek_3_4(int offs) {
return BUFFER_3_4[TAIL_3_4+offs];
}



inline void __push_4_5(float data) {
BUFFER_4_5[HEAD_4_5]=data;
HEAD_4_5++;
}



 
void init_FFTReorderSimple__33_35__4(){
}
void save_file_pointer__4(object_write_buffer *buf) {}
void load_file_pointer__4(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__33_35__4(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__26 = 0;/* int */
      float __tmp8__27 = 0.0f;/* float */
      float __tmp9__28 = 0.0f;/* float */
      int __tmp10__29 = 0;/* int */
      float __tmp11__30 = 0.0f;/* float */
      float __tmp12__31 = 0.0f;/* float */
      int __tmp13__32 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__26 = 0)/*int*/; (i__26 < 64); (i__26 = (i__26 + 4))/*int*/) {{
          __tmp8__27 = BUFFER_3_4[TAIL_3_4+i__26];
;
          __push_4_5(__tmp8__27);
          (__tmp10__29 = (i__26 + 1))/*int*/;
          __tmp9__28 = BUFFER_3_4[TAIL_3_4+__tmp10__29];
;
          __push_4_5(__tmp9__28);
        }
      }
      for ((i__26 = 2)/*int*/; (i__26 < 64); (i__26 = (i__26 + 4))/*int*/) {{
          __tmp11__30 = BUFFER_3_4[TAIL_3_4+i__26];
;
          __push_4_5(__tmp11__30);
          (__tmp13__32 = (i__26 + 1))/*int*/;
          __tmp12__31 = BUFFER_3_4[TAIL_3_4+__tmp13__32];
;
          __push_4_5(__tmp12__31);
        }
      }
      __pop_3_4(64);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 32 pop: 32 push 32
// init counts: 0 steady counts: 8

// ClusterFusion isEliminated: false



int __number_of_iterations_5;
int __counter_5 = 0;
int __steady_5 = 0;
int __tmp_5 = 0;
int __tmp2_5 = 0;
int *__state_flag_5 = NULL;
thread_info *__thread_5 = NULL;



void save_peek_buffer__5(object_write_buffer *buf);
void load_peek_buffer__5(object_write_buffer *buf);
void save_file_pointer__5(object_write_buffer *buf);
void load_file_pointer__5(object_write_buffer *buf);

inline void check_status__5() {
  check_thread_status(__state_flag_5, __thread_5);
}

void check_status_during_io__5() {
  check_thread_status_during_io(__state_flag_5, __thread_5);
}

void __init_thread_info_5(thread_info *info) {
  __state_flag_5 = info->get_state_flag();
}

thread_info *__get_thread_info_5() {
  if (__thread_5 != NULL) return __thread_5;
  __thread_5 = new thread_info(5, check_status_during_io__5);
  __init_thread_info_5(__thread_5);
  return __thread_5;
}

void __declare_sockets_5() {
  init_instance::add_incoming(4,5, DATA_SOCKET);
  init_instance::add_outgoing(5,6, DATA_SOCKET);
}

void __init_sockets_5(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_5() {
}

void __peek_sockets_5() {
}

 
void init_FFTReorderSimple__43_36__5();
inline void check_status__5();

void work_FFTReorderSimple__43_36__5(int);


inline float __pop_4_5() {
float res=BUFFER_4_5[TAIL_4_5];
TAIL_4_5++;
return res;
}

inline void __pop_4_5(int n) {
TAIL_4_5+=n;
}

inline float __peek_4_5(int offs) {
return BUFFER_4_5[TAIL_4_5+offs];
}



inline void __push_5_6(float data) {
BUFFER_5_6[HEAD_5_6]=data;
HEAD_5_6++;
}



 
void init_FFTReorderSimple__43_36__5(){
}
void save_file_pointer__5(object_write_buffer *buf) {}
void load_file_pointer__5(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__43_36__5(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__36 = 0;/* int */
      float __tmp8__37 = 0.0f;/* float */
      float __tmp9__38 = 0.0f;/* float */
      int __tmp10__39 = 0;/* int */
      float __tmp11__40 = 0.0f;/* float */
      float __tmp12__41 = 0.0f;/* float */
      int __tmp13__42 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__36 = 0)/*int*/; (i__36 < 32); (i__36 = (i__36 + 4))/*int*/) {{
          __tmp8__37 = BUFFER_4_5[TAIL_4_5+i__36];
;
          __push_5_6(__tmp8__37);
          (__tmp10__39 = (i__36 + 1))/*int*/;
          __tmp9__38 = BUFFER_4_5[TAIL_4_5+__tmp10__39];
;
          __push_5_6(__tmp9__38);
        }
      }
      for ((i__36 = 2)/*int*/; (i__36 < 32); (i__36 = (i__36 + 4))/*int*/) {{
          __tmp11__40 = BUFFER_4_5[TAIL_4_5+i__36];
;
          __push_5_6(__tmp11__40);
          (__tmp13__42 = (i__36 + 1))/*int*/;
          __tmp12__41 = BUFFER_4_5[TAIL_4_5+__tmp13__42];
;
          __push_5_6(__tmp12__41);
        }
      }
      __pop_4_5(32);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 16 pop: 16 push 16
// init counts: 0 steady counts: 16

// ClusterFusion isEliminated: false



int __number_of_iterations_6;
int __counter_6 = 0;
int __steady_6 = 0;
int __tmp_6 = 0;
int __tmp2_6 = 0;
int *__state_flag_6 = NULL;
thread_info *__thread_6 = NULL;



void save_peek_buffer__6(object_write_buffer *buf);
void load_peek_buffer__6(object_write_buffer *buf);
void save_file_pointer__6(object_write_buffer *buf);
void load_file_pointer__6(object_write_buffer *buf);

inline void check_status__6() {
  check_thread_status(__state_flag_6, __thread_6);
}

void check_status_during_io__6() {
  check_thread_status_during_io(__state_flag_6, __thread_6);
}

void __init_thread_info_6(thread_info *info) {
  __state_flag_6 = info->get_state_flag();
}

thread_info *__get_thread_info_6() {
  if (__thread_6 != NULL) return __thread_6;
  __thread_6 = new thread_info(6, check_status_during_io__6);
  __init_thread_info_6(__thread_6);
  return __thread_6;
}

void __declare_sockets_6() {
  init_instance::add_incoming(5,6, DATA_SOCKET);
  init_instance::add_outgoing(6,7, DATA_SOCKET);
}

void __init_sockets_6(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_6() {
}

void __peek_sockets_6() {
}

 
void init_FFTReorderSimple__53_37__6();
inline void check_status__6();

void work_FFTReorderSimple__53_37__6(int);


inline float __pop_5_6() {
float res=BUFFER_5_6[TAIL_5_6];
TAIL_5_6++;
return res;
}

inline void __pop_5_6(int n) {
TAIL_5_6+=n;
}

inline float __peek_5_6(int offs) {
return BUFFER_5_6[TAIL_5_6+offs];
}



inline void __push_6_7(float data) {
BUFFER_6_7[HEAD_6_7]=data;
HEAD_6_7++;
}



 
void init_FFTReorderSimple__53_37__6(){
}
void save_file_pointer__6(object_write_buffer *buf) {}
void load_file_pointer__6(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__53_37__6(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__46 = 0;/* int */
      float __tmp8__47 = 0.0f;/* float */
      float __tmp9__48 = 0.0f;/* float */
      int __tmp10__49 = 0;/* int */
      float __tmp11__50 = 0.0f;/* float */
      float __tmp12__51 = 0.0f;/* float */
      int __tmp13__52 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__46 = 0)/*int*/; (i__46 < 16); (i__46 = (i__46 + 4))/*int*/) {{
          __tmp8__47 = BUFFER_5_6[TAIL_5_6+i__46];
;
          __push_6_7(__tmp8__47);
          (__tmp10__49 = (i__46 + 1))/*int*/;
          __tmp9__48 = BUFFER_5_6[TAIL_5_6+__tmp10__49];
;
          __push_6_7(__tmp9__48);
        }
      }
      for ((i__46 = 2)/*int*/; (i__46 < 16); (i__46 = (i__46 + 4))/*int*/) {{
          __tmp11__50 = BUFFER_5_6[TAIL_5_6+i__46];
;
          __push_6_7(__tmp11__50);
          (__tmp13__52 = (i__46 + 1))/*int*/;
          __tmp12__51 = BUFFER_5_6[TAIL_5_6+__tmp13__52];
;
          __push_6_7(__tmp12__51);
        }
      }
      __pop_5_6(16);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 8 pop: 8 push 8
// init counts: 0 steady counts: 32

// ClusterFusion isEliminated: false



int __number_of_iterations_7;
int __counter_7 = 0;
int __steady_7 = 0;
int __tmp_7 = 0;
int __tmp2_7 = 0;
int *__state_flag_7 = NULL;
thread_info *__thread_7 = NULL;



void save_peek_buffer__7(object_write_buffer *buf);
void load_peek_buffer__7(object_write_buffer *buf);
void save_file_pointer__7(object_write_buffer *buf);
void load_file_pointer__7(object_write_buffer *buf);

inline void check_status__7() {
  check_thread_status(__state_flag_7, __thread_7);
}

void check_status_during_io__7() {
  check_thread_status_during_io(__state_flag_7, __thread_7);
}

void __init_thread_info_7(thread_info *info) {
  __state_flag_7 = info->get_state_flag();
}

thread_info *__get_thread_info_7() {
  if (__thread_7 != NULL) return __thread_7;
  __thread_7 = new thread_info(7, check_status_during_io__7);
  __init_thread_info_7(__thread_7);
  return __thread_7;
}

void __declare_sockets_7() {
  init_instance::add_incoming(6,7, DATA_SOCKET);
  init_instance::add_outgoing(7,8, DATA_SOCKET);
}

void __init_sockets_7(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_7() {
}

void __peek_sockets_7() {
}

 
void init_FFTReorderSimple__63_38__7();
inline void check_status__7();

void work_FFTReorderSimple__63_38__7(int);


inline float __pop_6_7() {
float res=BUFFER_6_7[TAIL_6_7];
TAIL_6_7++;
return res;
}

inline void __pop_6_7(int n) {
TAIL_6_7+=n;
}

inline float __peek_6_7(int offs) {
return BUFFER_6_7[TAIL_6_7+offs];
}



inline void __push_7_8(float data) {
BUFFER_7_8[HEAD_7_8]=data;
HEAD_7_8++;
}



 
void init_FFTReorderSimple__63_38__7(){
}
void save_file_pointer__7(object_write_buffer *buf) {}
void load_file_pointer__7(object_write_buffer *buf) {}
 
void work_FFTReorderSimple__63_38__7(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__56 = 0;/* int */
      float __tmp8__57 = 0.0f;/* float */
      float __tmp9__58 = 0.0f;/* float */
      int __tmp10__59 = 0;/* int */
      float __tmp11__60 = 0.0f;/* float */
      float __tmp12__61 = 0.0f;/* float */
      int __tmp13__62 = 0;/* int */

      // mark begin: SIRFilter FFTReorderSimple

      for ((i__56 = 0)/*int*/; (i__56 < 8); (i__56 = (i__56 + 4))/*int*/) {{
          __tmp8__57 = BUFFER_6_7[TAIL_6_7+i__56];
;
          __push_7_8(__tmp8__57);
          (__tmp10__59 = (i__56 + 1))/*int*/;
          __tmp9__58 = BUFFER_6_7[TAIL_6_7+__tmp10__59];
;
          __push_7_8(__tmp9__58);
        }
      }
      for ((i__56 = 2)/*int*/; (i__56 < 8); (i__56 = (i__56 + 4))/*int*/) {{
          __tmp11__60 = BUFFER_6_7[TAIL_6_7+i__56];
;
          __push_7_8(__tmp11__60);
          (__tmp13__62 = (i__56 + 1))/*int*/;
          __tmp12__61 = BUFFER_6_7[TAIL_6_7+__tmp13__62];
;
          __push_7_8(__tmp12__61);
        }
      }
      __pop_6_7(8);
      // mark end: SIRFilter FFTReorderSimple

    }
  }
}

// peek: 4 pop: 4 push 4
// init counts: 0 steady counts: 64

// ClusterFusion isEliminated: false



int __number_of_iterations_8;
int __counter_8 = 0;
int __steady_8 = 0;
int __tmp_8 = 0;
int __tmp2_8 = 0;
int *__state_flag_8 = NULL;
thread_info *__thread_8 = NULL;



float w__64__8[2] = {0};
void save_peek_buffer__8(object_write_buffer *buf);
void load_peek_buffer__8(object_write_buffer *buf);
void save_file_pointer__8(object_write_buffer *buf);
void load_file_pointer__8(object_write_buffer *buf);

inline void check_status__8() {
  check_thread_status(__state_flag_8, __thread_8);
}

void check_status_during_io__8() {
  check_thread_status_during_io(__state_flag_8, __thread_8);
}

void __init_thread_info_8(thread_info *info) {
  __state_flag_8 = info->get_state_flag();
}

thread_info *__get_thread_info_8() {
  if (__thread_8 != NULL) return __thread_8;
  __thread_8 = new thread_info(8, check_status_during_io__8);
  __init_thread_info_8(__thread_8);
  return __thread_8;
}

void __declare_sockets_8() {
  init_instance::add_incoming(7,8, DATA_SOCKET);
  init_instance::add_outgoing(8,9, DATA_SOCKET);
}

void __init_sockets_8(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_8() {
}

void __peek_sockets_8() {
}

 
void init_CombineDFT__88_39__8();
inline void check_status__8();

void work_CombineDFT__88_39__8(int);


inline float __pop_7_8() {
float res=BUFFER_7_8[TAIL_7_8];
TAIL_7_8++;
return res;
}

inline void __pop_7_8(int n) {
TAIL_7_8+=n;
}

inline float __peek_7_8(int offs) {
return BUFFER_7_8[TAIL_7_8+offs];
}



inline void __push_8_9(float data) {
BUFFER_8_9[HEAD_8_9]=data;
HEAD_8_9++;
}



 
void init_CombineDFT__88_39__8(){
  float real__83 = 0.0f;/* float */
  float imag__84 = 0.0f;/* float */
  float next_real__85 = 0.0f;/* float */float next_imag__86 = 0.0f;/* float */
  int i__87 = 0;/* int */

  (real__83 = ((float)1.0))/*float*/;
  (imag__84 = ((float)0.0))/*float*/;
  for ((i__87 = 0)/*int*/; (i__87 < 2); (i__87 = (i__87 + 2))/*int*/) {{
      (((w__64__8)[(int)i__87]) = real__83)/*float*/;
      (((w__64__8)[(int)(i__87 + 1)]) = imag__84)/*float*/;
      (next_real__85 = ((real__83 * ((float)-1.0)) - (imag__84 * ((float)8.742278E-8))))/*float*/;
      (next_imag__86 = ((real__83 * ((float)8.742278E-8)) + (imag__84 * ((float)-1.0))))/*float*/;
      (real__83 = next_real__85)/*float*/;
      (imag__84 = next_imag__86)/*float*/;
    }
  }
}
void save_file_pointer__8(object_write_buffer *buf) {}
void load_file_pointer__8(object_write_buffer *buf) {}
 
void work_CombineDFT__88_39__8(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__67 = 0;/* int */
      float results__68[4] = {0};/* float[4] */
      int i_plus_1__69 = 0;/* int */
      float y0_r__70 = 0.0f;/* float */
      float y0_i__71 = 0.0f;/* float */
      float y1_r__72 = 0.0f;/* float */
      float __tmp14__73 = 0.0f;/* float */
      int __tmp15__74 = 0;/* int */
      float y1_i__75 = 0.0f;/* float */
      float __tmp16__76 = 0.0f;/* float */
      int __tmp17__77 = 0;/* int */
      float weight_real__78 = 0.0f;/* float */
      float weight_imag__79 = 0.0f;/* float */
      float y1w_r__80 = 0.0f;/* float */
      float y1w_i__81 = 0.0f;/* float */
      float v__82 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__67 = 0)/*int*/; (i__67 < 2); (i__67 = (i__67 + 2))/*int*/) {{
          (i_plus_1__69 = (i__67 + 1))/*int*/;
          y0_r__70 = BUFFER_7_8[TAIL_7_8+i__67];
;
          y0_i__71 = BUFFER_7_8[TAIL_7_8+i_plus_1__69];
;
          (__tmp15__74 = (2 + i__67))/*int*/;
          __tmp14__73 = BUFFER_7_8[TAIL_7_8+__tmp15__74];
;
          (y1_r__72 = __tmp14__73)/*float*/;
          (__tmp17__77 = (2 + i_plus_1__69))/*int*/;
          __tmp16__76 = BUFFER_7_8[TAIL_7_8+__tmp17__77];
;
          (y1_i__75 = __tmp16__76)/*float*/;
          (weight_real__78 = ((w__64__8)[(int)i__67]))/*float*/;
          (weight_imag__79 = ((w__64__8)[(int)i_plus_1__69]))/*float*/;
          (y1w_r__80 = ((y1_r__72 * weight_real__78) - (y1_i__75 * weight_imag__79)))/*float*/;
          (y1w_i__81 = ((y1_r__72 * weight_imag__79) + (y1_i__75 * weight_real__78)))/*float*/;
          ((results__68[(int)i__67]) = (y0_r__70 + y1w_r__80))/*float*/;
          ((results__68[(int)(i__67 + 1)]) = (y0_i__71 + y1w_i__81))/*float*/;
          ((results__68[(int)(2 + i__67)]) = (y0_r__70 - y1w_r__80))/*float*/;
          ((results__68[(int)((2 + i__67) + 1)]) = (y0_i__71 - y1w_i__81))/*float*/;
        }
      }
      for ((i__67 = 0)/*int*/; (i__67 < 4); (i__67++)) {{
          __pop_7_8();
          (v__82 = (results__68[(int)i__67]))/*float*/;
          __push_8_9(v__82);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

// peek: 8 pop: 8 push 8
// init counts: 0 steady counts: 32

// ClusterFusion isEliminated: false



int __number_of_iterations_9;
int __counter_9 = 0;
int __steady_9 = 0;
int __tmp_9 = 0;
int __tmp2_9 = 0;
int *__state_flag_9 = NULL;
thread_info *__thread_9 = NULL;



float w__89__9[4] = {0};
void save_peek_buffer__9(object_write_buffer *buf);
void load_peek_buffer__9(object_write_buffer *buf);
void save_file_pointer__9(object_write_buffer *buf);
void load_file_pointer__9(object_write_buffer *buf);

inline void check_status__9() {
  check_thread_status(__state_flag_9, __thread_9);
}

void check_status_during_io__9() {
  check_thread_status_during_io(__state_flag_9, __thread_9);
}

void __init_thread_info_9(thread_info *info) {
  __state_flag_9 = info->get_state_flag();
}

thread_info *__get_thread_info_9() {
  if (__thread_9 != NULL) return __thread_9;
  __thread_9 = new thread_info(9, check_status_during_io__9);
  __init_thread_info_9(__thread_9);
  return __thread_9;
}

void __declare_sockets_9() {
  init_instance::add_incoming(8,9, DATA_SOCKET);
  init_instance::add_outgoing(9,10, DATA_SOCKET);
}

void __init_sockets_9(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_9() {
}

void __peek_sockets_9() {
}

 
void init_CombineDFT__113_40__9();
inline void check_status__9();

void work_CombineDFT__113_40__9(int);


inline float __pop_8_9() {
float res=BUFFER_8_9[TAIL_8_9];
TAIL_8_9++;
return res;
}

inline void __pop_8_9(int n) {
TAIL_8_9+=n;
}

inline float __peek_8_9(int offs) {
return BUFFER_8_9[TAIL_8_9+offs];
}



inline void __push_9_10(float data) {
BUFFER_9_10[HEAD_9_10]=data;
HEAD_9_10++;
}



 
void init_CombineDFT__113_40__9(){
  float real__108 = 0.0f;/* float */
  float imag__109 = 0.0f;/* float */
  float next_real__110 = 0.0f;/* float */float next_imag__111 = 0.0f;/* float */
  int i__112 = 0;/* int */

  (real__108 = ((float)1.0))/*float*/;
  (imag__109 = ((float)0.0))/*float*/;
  for ((i__112 = 0)/*int*/; (i__112 < 4); (i__112 = (i__112 + 2))/*int*/) {{
      (((w__89__9)[(int)i__112]) = real__108)/*float*/;
      (((w__89__9)[(int)(i__112 + 1)]) = imag__109)/*float*/;
      (next_real__110 = ((real__108 * ((float)-4.371139E-8)) - (imag__109 * ((float)-1.0))))/*float*/;
      (next_imag__111 = ((real__108 * ((float)-1.0)) + (imag__109 * ((float)-4.371139E-8))))/*float*/;
      (real__108 = next_real__110)/*float*/;
      (imag__109 = next_imag__111)/*float*/;
    }
  }
}
void save_file_pointer__9(object_write_buffer *buf) {}
void load_file_pointer__9(object_write_buffer *buf) {}
 
void work_CombineDFT__113_40__9(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__92 = 0;/* int */
      float results__93[8] = {0};/* float[8] */
      int i_plus_1__94 = 0;/* int */
      float y0_r__95 = 0.0f;/* float */
      float y0_i__96 = 0.0f;/* float */
      float y1_r__97 = 0.0f;/* float */
      float __tmp14__98 = 0.0f;/* float */
      int __tmp15__99 = 0;/* int */
      float y1_i__100 = 0.0f;/* float */
      float __tmp16__101 = 0.0f;/* float */
      int __tmp17__102 = 0;/* int */
      float weight_real__103 = 0.0f;/* float */
      float weight_imag__104 = 0.0f;/* float */
      float y1w_r__105 = 0.0f;/* float */
      float y1w_i__106 = 0.0f;/* float */
      float v__107 = 0.0f;/* float */

      // mark begin: SIRFilter CombineDFT

      for ((i__92 = 0)/*int*/; (i__92 < 4); (i__92 = (i__92 + 2))/*int*/) {{
          (i_plus_1__94 = (i__92 + 1))/*int*/;
          y0_r__95 = BUFFER_8_9[TAIL_8_9+i__92];
;
          y0_i__96 = BUFFER_8_9[TAIL_8_9+i_plus_1__94];
;
          (__tmp15__99 = (4 + i__92))/*int*/;
          __tmp14__98 = BUFFER_8_9[TAIL_8_9+__tmp15__99];
;
          (y1_r__97 = __tmp14__98)/*float*/;
          (__tmp17__102 = (4 + i_plus_1__94))/*int*/;
          __tmp16__101 = BUFFER_8_9[TAIL_8_9+__tmp17__102];
;
          (y1_i__100 = __tmp16__101)/*float*/;
          (weight_real__103 = ((w__89__9)[(int)i__92]))/*float*/;
          (weight_imag__104 = ((w__89__9)[(int)i_plus_1__94]))/*float*/;
          (y1w_r__105 = ((y1_r__97 * weight_real__103) - (y1_i__100 * weight_imag__104)))/*float*/;
          (y1w_i__106 = ((y1_r__97 * weight_imag__104) + (y1_i__100 * weight_real__103)))/*float*/;
          ((results__93[(int)i__92]) = (y0_r__95 + y1w_r__105))/*float*/;
          ((results__93[(int)(i__92 + 1)]) = (y0_i__96 + y1w_i__106))/*float*/;
          ((results__93[(int)(4 + i__92)]) = (y0_r__95 - y1w_r__105))/*float*/;
          ((results__93[(int)((4 + i__92) + 1)]) = (y0_i__96 - y1w_i__106))/*float*/;
        }
      }
      for ((i__92 = 0)/*int*/; (i__92 < 8); (i__92++)) {{
          __pop_8_9();
          (v__107 = (results__93[(int)i__92]))/*float*/;
          __push_9_10(v__107);
        }
      }
      // mark end: SIRFilter CombineDFT

    }
  }
}

