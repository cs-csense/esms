#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <netsocket.h>
#include <node_server.h>
#include <init_instance.h>
#include <master_server.h>
#include <save_state.h>
#include <save_manager.h>
#include <delete_chkpts.h>
#include <object_write_buffer.h>
#include <read_setup.h>
#include <ccp.h>
#include <streamit_random.h>
#include "structs.h"
#include "fusion.h"

int __max_iteration;
int __timer_enabled = 0;
int __frequency_of_chkpts;
volatile int __vol;


float BUFFER_0_1[__BUF_SIZE_MASK_0_1 + 1];
int HEAD_0_1 = 0;
int TAIL_0_1 = 0;
float BUFFER_1_2[__BUF_SIZE_MASK_1_2 + 1];
int HEAD_1_2 = 0;
int TAIL_1_2 = 0;
float BUFFER_2_3[__BUF_SIZE_MASK_2_3 + 1];
int HEAD_2_3 = 0;
int TAIL_2_3 = 0;
float BUFFER_3_4[__BUF_SIZE_MASK_3_4 + 1];
int HEAD_3_4 = 0;
int TAIL_3_4 = 0;
float BUFFER_3_13[__BUF_SIZE_MASK_3_13 + 1];
int HEAD_3_13 = 0;
int TAIL_3_13 = 0;
float BUFFER_3_19[__BUF_SIZE_MASK_3_19 + 1];
int HEAD_3_19 = 0;
int TAIL_3_19 = 0;
float BUFFER_3_25[__BUF_SIZE_MASK_3_25 + 1];
int HEAD_3_25 = 0;
int TAIL_3_25 = 0;
float BUFFER_3_31[__BUF_SIZE_MASK_3_31 + 1];
int HEAD_3_31 = 0;
int TAIL_3_31 = 0;
float BUFFER_3_37[__BUF_SIZE_MASK_3_37 + 1];
int HEAD_3_37 = 0;
int TAIL_3_37 = 0;
float BUFFER_3_43[__BUF_SIZE_MASK_3_43 + 1];
int HEAD_3_43 = 0;
int TAIL_3_43 = 0;
float BUFFER_3_49[__BUF_SIZE_MASK_3_49 + 1];
int HEAD_3_49 = 0;
int TAIL_3_49 = 0;
float BUFFER_3_55[__BUF_SIZE_MASK_3_55 + 1];
int HEAD_3_55 = 0;
int TAIL_3_55 = 0;
float BUFFER_3_61[__BUF_SIZE_MASK_3_61 + 1];
int HEAD_3_61 = 0;
int TAIL_3_61 = 0;
float BUFFER_4_5[__BUF_SIZE_MASK_4_5 + 1];
int HEAD_4_5 = 0;
int TAIL_4_5 = 0;
float BUFFER_4_12[__BUF_SIZE_MASK_4_12 + 1];
int HEAD_4_12 = 0;
int TAIL_4_12 = 0;
float BUFFER_5_6[__BUF_SIZE_MASK_5_6 + 1];
int HEAD_5_6 = 0;
int TAIL_5_6 = 0;
float BUFFER_6_7[__BUF_SIZE_MASK_6_7 + 1];
int HEAD_6_7 = 0;
int TAIL_6_7 = 0;
float BUFFER_7_8[__BUF_SIZE_MASK_7_8 + 1];
int HEAD_7_8 = 0;
int TAIL_7_8 = 0;
float BUFFER_8_9[__BUF_SIZE_MASK_8_9 + 1];
int HEAD_8_9 = 0;
int TAIL_8_9 = 0;
float BUFFER_9_10[__BUF_SIZE_MASK_9_10 + 1];
int HEAD_9_10 = 0;
int TAIL_9_10 = 0;
float BUFFER_10_11[__BUF_SIZE_MASK_10_11 + 1];
int HEAD_10_11 = 0;
int TAIL_10_11 = 0;
float BUFFER_12_6[__BUF_SIZE_MASK_12_6 + 1];
int HEAD_12_6 = 0;
int TAIL_12_6 = 0;
float BUFFER_13_14[__BUF_SIZE_MASK_13_14 + 1];
int HEAD_13_14 = 0;
int TAIL_13_14 = 0;
float BUFFER_13_18[__BUF_SIZE_MASK_13_18 + 1];
int HEAD_13_18 = 0;
int TAIL_13_18 = 0;
float BUFFER_14_15[__BUF_SIZE_MASK_14_15 + 1];
int HEAD_14_15 = 0;
int TAIL_14_15 = 0;
float BUFFER_15_16[__BUF_SIZE_MASK_15_16 + 1];
int HEAD_15_16 = 0;
int TAIL_15_16 = 0;
float BUFFER_16_17[__BUF_SIZE_MASK_16_17 + 1];
int HEAD_16_17 = 0;
int TAIL_16_17 = 0;
float BUFFER_17_9[__BUF_SIZE_MASK_17_9 + 1];
int HEAD_17_9 = 0;
int TAIL_17_9 = 0;
float BUFFER_18_15[__BUF_SIZE_MASK_18_15 + 1];
int HEAD_18_15 = 0;
int TAIL_18_15 = 0;
float BUFFER_19_20[__BUF_SIZE_MASK_19_20 + 1];
int HEAD_19_20 = 0;
int TAIL_19_20 = 0;
float BUFFER_19_24[__BUF_SIZE_MASK_19_24 + 1];
int HEAD_19_24 = 0;
int TAIL_19_24 = 0;
float BUFFER_20_21[__BUF_SIZE_MASK_20_21 + 1];
int HEAD_20_21 = 0;
int TAIL_20_21 = 0;
float BUFFER_21_22[__BUF_SIZE_MASK_21_22 + 1];
int HEAD_21_22 = 0;
int TAIL_21_22 = 0;
float BUFFER_22_23[__BUF_SIZE_MASK_22_23 + 1];
int HEAD_22_23 = 0;
int TAIL_22_23 = 0;
float BUFFER_23_9[__BUF_SIZE_MASK_23_9 + 1];
int HEAD_23_9 = 0;
int TAIL_23_9 = 0;
float BUFFER_24_21[__BUF_SIZE_MASK_24_21 + 1];
int HEAD_24_21 = 0;
int TAIL_24_21 = 0;
float BUFFER_25_26[__BUF_SIZE_MASK_25_26 + 1];
int HEAD_25_26 = 0;
int TAIL_25_26 = 0;
float BUFFER_25_30[__BUF_SIZE_MASK_25_30 + 1];
int HEAD_25_30 = 0;
int TAIL_25_30 = 0;
float BUFFER_26_27[__BUF_SIZE_MASK_26_27 + 1];
int HEAD_26_27 = 0;
int TAIL_26_27 = 0;
float BUFFER_27_28[__BUF_SIZE_MASK_27_28 + 1];
int HEAD_27_28 = 0;
int TAIL_27_28 = 0;
float BUFFER_28_29[__BUF_SIZE_MASK_28_29 + 1];
int HEAD_28_29 = 0;
int TAIL_28_29 = 0;
float BUFFER_29_9[__BUF_SIZE_MASK_29_9 + 1];
int HEAD_29_9 = 0;
int TAIL_29_9 = 0;
float BUFFER_30_27[__BUF_SIZE_MASK_30_27 + 1];
int HEAD_30_27 = 0;
int TAIL_30_27 = 0;
float BUFFER_31_32[__BUF_SIZE_MASK_31_32 + 1];
int HEAD_31_32 = 0;
int TAIL_31_32 = 0;
float BUFFER_31_36[__BUF_SIZE_MASK_31_36 + 1];
int HEAD_31_36 = 0;
int TAIL_31_36 = 0;
float BUFFER_32_33[__BUF_SIZE_MASK_32_33 + 1];
int HEAD_32_33 = 0;
int TAIL_32_33 = 0;
float BUFFER_33_34[__BUF_SIZE_MASK_33_34 + 1];
int HEAD_33_34 = 0;
int TAIL_33_34 = 0;
float BUFFER_34_35[__BUF_SIZE_MASK_34_35 + 1];
int HEAD_34_35 = 0;
int TAIL_34_35 = 0;
float BUFFER_35_9[__BUF_SIZE_MASK_35_9 + 1];
int HEAD_35_9 = 0;
int TAIL_35_9 = 0;
float BUFFER_36_33[__BUF_SIZE_MASK_36_33 + 1];
int HEAD_36_33 = 0;
int TAIL_36_33 = 0;
float BUFFER_37_38[__BUF_SIZE_MASK_37_38 + 1];
int HEAD_37_38 = 0;
int TAIL_37_38 = 0;
float BUFFER_37_42[__BUF_SIZE_MASK_37_42 + 1];
int HEAD_37_42 = 0;
int TAIL_37_42 = 0;
float BUFFER_38_39[__BUF_SIZE_MASK_38_39 + 1];
int HEAD_38_39 = 0;
int TAIL_38_39 = 0;
float BUFFER_39_40[__BUF_SIZE_MASK_39_40 + 1];
int HEAD_39_40 = 0;
int TAIL_39_40 = 0;
float BUFFER_40_41[__BUF_SIZE_MASK_40_41 + 1];
int HEAD_40_41 = 0;
int TAIL_40_41 = 0;
float BUFFER_41_9[__BUF_SIZE_MASK_41_9 + 1];
int HEAD_41_9 = 0;
int TAIL_41_9 = 0;
float BUFFER_42_39[__BUF_SIZE_MASK_42_39 + 1];
int HEAD_42_39 = 0;
int TAIL_42_39 = 0;
float BUFFER_43_44[__BUF_SIZE_MASK_43_44 + 1];
int HEAD_43_44 = 0;
int TAIL_43_44 = 0;
float BUFFER_43_48[__BUF_SIZE_MASK_43_48 + 1];
int HEAD_43_48 = 0;
int TAIL_43_48 = 0;
float BUFFER_44_45[__BUF_SIZE_MASK_44_45 + 1];
int HEAD_44_45 = 0;
int TAIL_44_45 = 0;
float BUFFER_45_46[__BUF_SIZE_MASK_45_46 + 1];
int HEAD_45_46 = 0;
int TAIL_45_46 = 0;
float BUFFER_46_47[__BUF_SIZE_MASK_46_47 + 1];
int HEAD_46_47 = 0;
int TAIL_46_47 = 0;
float BUFFER_47_9[__BUF_SIZE_MASK_47_9 + 1];
int HEAD_47_9 = 0;
int TAIL_47_9 = 0;
float BUFFER_48_45[__BUF_SIZE_MASK_48_45 + 1];
int HEAD_48_45 = 0;
int TAIL_48_45 = 0;
float BUFFER_49_50[__BUF_SIZE_MASK_49_50 + 1];
int HEAD_49_50 = 0;
int TAIL_49_50 = 0;
float BUFFER_49_54[__BUF_SIZE_MASK_49_54 + 1];
int HEAD_49_54 = 0;
int TAIL_49_54 = 0;
float BUFFER_50_51[__BUF_SIZE_MASK_50_51 + 1];
int HEAD_50_51 = 0;
int TAIL_50_51 = 0;
float BUFFER_51_52[__BUF_SIZE_MASK_51_52 + 1];
int HEAD_51_52 = 0;
int TAIL_51_52 = 0;
float BUFFER_52_53[__BUF_SIZE_MASK_52_53 + 1];
int HEAD_52_53 = 0;
int TAIL_52_53 = 0;
float BUFFER_53_9[__BUF_SIZE_MASK_53_9 + 1];
int HEAD_53_9 = 0;
int TAIL_53_9 = 0;
float BUFFER_54_51[__BUF_SIZE_MASK_54_51 + 1];
int HEAD_54_51 = 0;
int TAIL_54_51 = 0;
float BUFFER_55_56[__BUF_SIZE_MASK_55_56 + 1];
int HEAD_55_56 = 0;
int TAIL_55_56 = 0;
float BUFFER_55_60[__BUF_SIZE_MASK_55_60 + 1];
int HEAD_55_60 = 0;
int TAIL_55_60 = 0;
float BUFFER_56_57[__BUF_SIZE_MASK_56_57 + 1];
int HEAD_56_57 = 0;
int TAIL_56_57 = 0;
float BUFFER_57_58[__BUF_SIZE_MASK_57_58 + 1];
int HEAD_57_58 = 0;
int TAIL_57_58 = 0;
float BUFFER_58_59[__BUF_SIZE_MASK_58_59 + 1];
int HEAD_58_59 = 0;
int TAIL_58_59 = 0;
float BUFFER_59_9[__BUF_SIZE_MASK_59_9 + 1];
int HEAD_59_9 = 0;
int TAIL_59_9 = 0;
float BUFFER_60_57[__BUF_SIZE_MASK_60_57 + 1];
int HEAD_60_57 = 0;
int TAIL_60_57 = 0;
float BUFFER_61_62[__BUF_SIZE_MASK_61_62 + 1];
int HEAD_61_62 = 0;
int TAIL_61_62 = 0;
float BUFFER_61_66[__BUF_SIZE_MASK_61_66 + 1];
int HEAD_61_66 = 0;
int TAIL_61_66 = 0;
float BUFFER_62_63[__BUF_SIZE_MASK_62_63 + 1];
int HEAD_62_63 = 0;
int TAIL_62_63 = 0;
float BUFFER_63_64[__BUF_SIZE_MASK_63_64 + 1];
int HEAD_63_64 = 0;
int TAIL_63_64 = 0;
float BUFFER_64_65[__BUF_SIZE_MASK_64_65 + 1];
int HEAD_64_65 = 0;
int TAIL_64_65 = 0;
float BUFFER_65_9[__BUF_SIZE_MASK_65_9 + 1];
int HEAD_65_9 = 0;
int TAIL_65_9 = 0;
float BUFFER_66_63[__BUF_SIZE_MASK_66_63 + 1];
int HEAD_66_63 = 0;
int TAIL_66_63 = 0;
void init_FloatOneSource__4_82__0();
void work_FloatOneSource__4_82__0(int);
void init_LowPassFilter__25_84__1();
void work_LowPassFilter__25_84__1(int);
void init_FMDemodulator__39_85__2();
void work_FMDemodulator__39_85__2(int);
void __splitter_3_work(int);
void __splitter_4_work(int);
void init_LowPassFilter__60_90__5();
void work_LowPassFilter__60_90__5(int);
void __joiner_6_work(int);
void init_Subtracter__87_92__7();
void work_Subtracter__87_92__7(int);
void init_Amplify__92_93__8();
void work_Amplify__92_93__8(int);
void __joiner_9_work(int);
void init_AnonFilter_a0__574_157__10();
void work_AnonFilter_a0__574_157__10(int);
void init_FloatPrinter__578_158__11();
void work_FloatPrinter__578_158__11(int);
void init_LowPassFilter__81_91__12();
void work_LowPassFilter__81_91__12(int);
void __splitter_13_work(int);
void init_LowPassFilter__113_97__14();
void work_LowPassFilter__113_97__14(int);
void __joiner_15_work(int);
void init_Subtracter__140_99__16();
void work_Subtracter__140_99__16(int);
void init_Amplify__145_100__17();
void work_Amplify__145_100__17(int);
void init_LowPassFilter__134_98__18();
void work_LowPassFilter__134_98__18(int);
void __splitter_19_work(int);
void init_LowPassFilter__166_104__20();
void work_LowPassFilter__166_104__20(int);
void __joiner_21_work(int);
void init_Subtracter__193_106__22();
void work_Subtracter__193_106__22(int);
void init_Amplify__198_107__23();
void work_Amplify__198_107__23(int);
void init_LowPassFilter__187_105__24();
void work_LowPassFilter__187_105__24(int);
void __splitter_25_work(int);
void init_LowPassFilter__219_111__26();
void work_LowPassFilter__219_111__26(int);
void __joiner_27_work(int);
void init_Subtracter__246_113__28();
void work_Subtracter__246_113__28(int);
void init_Amplify__251_114__29();
void work_Amplify__251_114__29(int);
void init_LowPassFilter__240_112__30();
void work_LowPassFilter__240_112__30(int);
void __splitter_31_work(int);
void init_LowPassFilter__272_118__32();
void work_LowPassFilter__272_118__32(int);
void __joiner_33_work(int);
void init_Subtracter__299_120__34();
void work_Subtracter__299_120__34(int);
void init_Amplify__304_121__35();
void work_Amplify__304_121__35(int);
void init_LowPassFilter__293_119__36();
void work_LowPassFilter__293_119__36(int);
void __splitter_37_work(int);
void init_LowPassFilter__325_125__38();
void work_LowPassFilter__325_125__38(int);
void __joiner_39_work(int);
void init_Subtracter__352_127__40();
void work_Subtracter__352_127__40(int);
void init_Amplify__357_128__41();
void work_Amplify__357_128__41(int);
void init_LowPassFilter__346_126__42();
void work_LowPassFilter__346_126__42(int);
void __splitter_43_work(int);
void init_LowPassFilter__378_132__44();
void work_LowPassFilter__378_132__44(int);
void __joiner_45_work(int);
void init_Subtracter__405_134__46();
void work_Subtracter__405_134__46(int);
void init_Amplify__410_135__47();
void work_Amplify__410_135__47(int);
void init_LowPassFilter__399_133__48();
void work_LowPassFilter__399_133__48(int);
void __splitter_49_work(int);
void init_LowPassFilter__431_139__50();
void work_LowPassFilter__431_139__50(int);
void __joiner_51_work(int);
void init_Subtracter__458_141__52();
void work_Subtracter__458_141__52(int);
void init_Amplify__463_142__53();
void work_Amplify__463_142__53(int);
void init_LowPassFilter__452_140__54();
void work_LowPassFilter__452_140__54(int);
void __splitter_55_work(int);
void init_LowPassFilter__484_146__56();
void work_LowPassFilter__484_146__56(int);
void __joiner_57_work(int);
void init_Subtracter__511_148__58();
void work_Subtracter__511_148__58(int);
void init_Amplify__516_149__59();
void work_Amplify__516_149__59(int);
void init_LowPassFilter__505_147__60();
void work_LowPassFilter__505_147__60(int);
void __splitter_61_work(int);
void init_LowPassFilter__537_153__62();
void work_LowPassFilter__537_153__62(int);
void __joiner_63_work(int);
void init_Subtracter__564_155__64();
void work_Subtracter__564_155__64(int);
void init_Amplify__569_156__65();
void work_Amplify__569_156__65(int);
void init_LowPassFilter__558_154__66();
void work_LowPassFilter__558_154__66(int);

int main(int argc, char **argv) {
  read_setup::read_setup_file();
  __max_iteration = read_setup::max_iteration;
  for (int a = 1; a < argc; a++) {
    if (argc > a + 1 && strcmp(argv[a], "-i") == 0) {
      int tmp;
      sscanf(argv[a + 1], "%d", &tmp);
#ifdef VERBOSE
#endif
      __max_iteration = tmp;
    }
    if (strcmp(argv[a], "-t") == 0) {
#ifdef VERBOSE
#endif
      __timer_enabled = 1;
    }
  }
// number of phases: 12


  // ============= Initialization =============

  init_FloatOneSource__4_82__0();   work_FloatOneSource__4_82__0(379);
  init_LowPassFilter__25_84__1();   work_LowPassFilter__25_84__1(64);
  init_FMDemodulator__39_85__2();   work_FMDemodulator__39_85__2(63);
    __splitter_3_work(63);
    __splitter_4_work(63);
    __splitter_55_work(63);
    __splitter_13_work(63);
    __splitter_61_work(63);
    __splitter_19_work(63);
    __splitter_25_work(63);
    __splitter_31_work(63);
    __splitter_37_work(63);
    __splitter_43_work(63);
    __splitter_49_work(63);
init_LowPassFilter__113_97__14();
init_LowPassFilter__134_98__18();
init_LowPassFilter__378_132__44();
init_LowPassFilter__399_133__48();
init_LowPassFilter__166_104__20();
init_LowPassFilter__187_105__24();
init_LowPassFilter__431_139__50();
init_LowPassFilter__452_140__54();
init_LowPassFilter__219_111__26();
init_LowPassFilter__240_112__30();
init_LowPassFilter__484_146__56();
init_LowPassFilter__505_147__60();
init_LowPassFilter__272_118__32();
init_LowPassFilter__293_119__36();
init_LowPassFilter__537_153__62();
init_LowPassFilter__558_154__66();
init_LowPassFilter__60_90__5();
init_LowPassFilter__81_91__12();
init_LowPassFilter__325_125__38();
init_LowPassFilter__346_126__42();
init_Subtracter__246_113__28();
init_Subtracter__140_99__16();
init_Subtracter__511_148__58();
init_Subtracter__405_134__46();
init_Subtracter__299_120__34();
init_Subtracter__193_106__22();
init_Subtracter__564_155__64();
init_Subtracter__87_92__7();
init_Subtracter__458_141__52();
init_Subtracter__352_127__40();
init_Amplify__357_128__41();
init_Amplify__251_114__29();
init_Amplify__145_100__17();
init_Amplify__516_149__59();
init_Amplify__410_135__47();
init_Amplify__304_121__35();
init_Amplify__198_107__23();
init_Amplify__569_156__65();
init_Amplify__92_93__8();
init_Amplify__463_142__53();
init_AnonFilter_a0__574_157__10();
init_FloatPrinter__578_158__11();

  // ============= Steady State =============

  if (__timer_enabled) {
  }
  for (int n = 0; n < (__max_iteration  ); n++) {
for (int __y = 0; __y < __PEEK_BUF_SIZE_0_1; __y++) {
  BUFFER_0_1[__y] = BUFFER_0_1[__y + TAIL_0_1];
}
HEAD_0_1 -= TAIL_0_1;
TAIL_0_1 = 0;
    work_FloatOneSource__4_82__0(5 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_1_2; __y++) {
  BUFFER_1_2[__y] = BUFFER_1_2[__y + TAIL_1_2];
}
HEAD_1_2 -= TAIL_1_2;
TAIL_1_2 = 0;
    work_LowPassFilter__25_84__1(1 );
HEAD_2_3 = 0;
TAIL_2_3 = 0;
    work_FMDemodulator__39_85__2(1 );
HEAD_3_4 = 0;
TAIL_3_4 = 0;
HEAD_3_13 = 0;
TAIL_3_13 = 0;
HEAD_3_19 = 0;
TAIL_3_19 = 0;
HEAD_3_25 = 0;
TAIL_3_25 = 0;
HEAD_3_31 = 0;
TAIL_3_31 = 0;
HEAD_3_37 = 0;
TAIL_3_37 = 0;
HEAD_3_43 = 0;
TAIL_3_43 = 0;
HEAD_3_49 = 0;
TAIL_3_49 = 0;
HEAD_3_55 = 0;
TAIL_3_55 = 0;
HEAD_3_61 = 0;
TAIL_3_61 = 0;
    __splitter_3_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_4_5; __y++) {
  BUFFER_4_5[__y] = BUFFER_4_5[__y + TAIL_4_5];
}
HEAD_4_5 -= TAIL_4_5;
TAIL_4_5 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_4_12; __y++) {
  BUFFER_4_12[__y] = BUFFER_4_12[__y + TAIL_4_12];
}
HEAD_4_12 -= TAIL_4_12;
TAIL_4_12 = 0;
    __splitter_4_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_55_56; __y++) {
  BUFFER_55_56[__y] = BUFFER_55_56[__y + TAIL_55_56];
}
HEAD_55_56 -= TAIL_55_56;
TAIL_55_56 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_55_60; __y++) {
  BUFFER_55_60[__y] = BUFFER_55_60[__y + TAIL_55_60];
}
HEAD_55_60 -= TAIL_55_60;
TAIL_55_60 = 0;
    __splitter_55_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_13_14; __y++) {
  BUFFER_13_14[__y] = BUFFER_13_14[__y + TAIL_13_14];
}
HEAD_13_14 -= TAIL_13_14;
TAIL_13_14 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_13_18; __y++) {
  BUFFER_13_18[__y] = BUFFER_13_18[__y + TAIL_13_18];
}
HEAD_13_18 -= TAIL_13_18;
TAIL_13_18 = 0;
    __splitter_13_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_61_62; __y++) {
  BUFFER_61_62[__y] = BUFFER_61_62[__y + TAIL_61_62];
}
HEAD_61_62 -= TAIL_61_62;
TAIL_61_62 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_61_66; __y++) {
  BUFFER_61_66[__y] = BUFFER_61_66[__y + TAIL_61_66];
}
HEAD_61_66 -= TAIL_61_66;
TAIL_61_66 = 0;
    __splitter_61_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_19_20; __y++) {
  BUFFER_19_20[__y] = BUFFER_19_20[__y + TAIL_19_20];
}
HEAD_19_20 -= TAIL_19_20;
TAIL_19_20 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_19_24; __y++) {
  BUFFER_19_24[__y] = BUFFER_19_24[__y + TAIL_19_24];
}
HEAD_19_24 -= TAIL_19_24;
TAIL_19_24 = 0;
    __splitter_19_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_25_26; __y++) {
  BUFFER_25_26[__y] = BUFFER_25_26[__y + TAIL_25_26];
}
HEAD_25_26 -= TAIL_25_26;
TAIL_25_26 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_25_30; __y++) {
  BUFFER_25_30[__y] = BUFFER_25_30[__y + TAIL_25_30];
}
HEAD_25_30 -= TAIL_25_30;
TAIL_25_30 = 0;
    __splitter_25_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_31_32; __y++) {
  BUFFER_31_32[__y] = BUFFER_31_32[__y + TAIL_31_32];
}
HEAD_31_32 -= TAIL_31_32;
TAIL_31_32 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_31_36; __y++) {
  BUFFER_31_36[__y] = BUFFER_31_36[__y + TAIL_31_36];
}
HEAD_31_36 -= TAIL_31_36;
TAIL_31_36 = 0;
    __splitter_31_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_37_38; __y++) {
  BUFFER_37_38[__y] = BUFFER_37_38[__y + TAIL_37_38];
}
HEAD_37_38 -= TAIL_37_38;
TAIL_37_38 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_37_42; __y++) {
  BUFFER_37_42[__y] = BUFFER_37_42[__y + TAIL_37_42];
}
HEAD_37_42 -= TAIL_37_42;
TAIL_37_42 = 0;
    __splitter_37_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_43_44; __y++) {
  BUFFER_43_44[__y] = BUFFER_43_44[__y + TAIL_43_44];
}
HEAD_43_44 -= TAIL_43_44;
TAIL_43_44 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_43_48; __y++) {
  BUFFER_43_48[__y] = BUFFER_43_48[__y + TAIL_43_48];
}
HEAD_43_48 -= TAIL_43_48;
TAIL_43_48 = 0;
    __splitter_43_work(1 );
for (int __y = 0; __y < __PEEK_BUF_SIZE_49_50; __y++) {
  BUFFER_49_50[__y] = BUFFER_49_50[__y + TAIL_49_50];
}
HEAD_49_50 -= TAIL_49_50;
TAIL_49_50 = 0;
for (int __y = 0; __y < __PEEK_BUF_SIZE_49_54; __y++) {
  BUFFER_49_54[__y] = BUFFER_49_54[__y + TAIL_49_54];
}
HEAD_49_54 -= TAIL_49_54;
TAIL_49_54 = 0;
    __splitter_49_work(1 );
HEAD_14_15 = 0;
TAIL_14_15 = 0;
    work_LowPassFilter__113_97__14(1 );
HEAD_18_15 = 0;
TAIL_18_15 = 0;
    work_LowPassFilter__134_98__18(1 );
HEAD_44_45 = 0;
TAIL_44_45 = 0;
    work_LowPassFilter__378_132__44(1 );
HEAD_48_45 = 0;
TAIL_48_45 = 0;
    work_LowPassFilter__399_133__48(1 );
HEAD_20_21 = 0;
TAIL_20_21 = 0;
    work_LowPassFilter__166_104__20(1 );
HEAD_24_21 = 0;
TAIL_24_21 = 0;
    work_LowPassFilter__187_105__24(1 );
HEAD_50_51 = 0;
TAIL_50_51 = 0;
    work_LowPassFilter__431_139__50(1 );
HEAD_54_51 = 0;
TAIL_54_51 = 0;
    work_LowPassFilter__452_140__54(1 );
HEAD_26_27 = 0;
TAIL_26_27 = 0;
    work_LowPassFilter__219_111__26(1 );
HEAD_30_27 = 0;
TAIL_30_27 = 0;
    work_LowPassFilter__240_112__30(1 );
HEAD_56_57 = 0;
TAIL_56_57 = 0;
    work_LowPassFilter__484_146__56(1 );
HEAD_60_57 = 0;
TAIL_60_57 = 0;
    work_LowPassFilter__505_147__60(1 );
HEAD_32_33 = 0;
TAIL_32_33 = 0;
    work_LowPassFilter__272_118__32(1 );
HEAD_36_33 = 0;
TAIL_36_33 = 0;
    work_LowPassFilter__293_119__36(1 );
HEAD_62_63 = 0;
TAIL_62_63 = 0;
    work_LowPassFilter__537_153__62(1 );
HEAD_66_63 = 0;
TAIL_66_63 = 0;
    work_LowPassFilter__558_154__66(1 );
HEAD_5_6 = 0;
TAIL_5_6 = 0;
    work_LowPassFilter__60_90__5(1 );
HEAD_12_6 = 0;
TAIL_12_6 = 0;
    work_LowPassFilter__81_91__12(1 );
HEAD_38_39 = 0;
TAIL_38_39 = 0;
    work_LowPassFilter__325_125__38(1 );
HEAD_42_39 = 0;
TAIL_42_39 = 0;
    work_LowPassFilter__346_126__42(1 );
HEAD_51_52 = 0;
TAIL_51_52 = 0;
    __joiner_51_work(1 );
HEAD_6_7 = 0;
TAIL_6_7 = 0;
    __joiner_6_work(1 );
HEAD_57_58 = 0;
TAIL_57_58 = 0;
    __joiner_57_work(1 );
HEAD_15_16 = 0;
TAIL_15_16 = 0;
    __joiner_15_work(1 );
HEAD_63_64 = 0;
TAIL_63_64 = 0;
    __joiner_63_work(1 );
HEAD_21_22 = 0;
TAIL_21_22 = 0;
    __joiner_21_work(1 );
HEAD_27_28 = 0;
TAIL_27_28 = 0;
    __joiner_27_work(1 );
HEAD_33_34 = 0;
TAIL_33_34 = 0;
    __joiner_33_work(1 );
HEAD_39_40 = 0;
TAIL_39_40 = 0;
    __joiner_39_work(1 );
HEAD_45_46 = 0;
TAIL_45_46 = 0;
    __joiner_45_work(1 );
HEAD_28_29 = 0;
TAIL_28_29 = 0;
    work_Subtracter__246_113__28(1 );
HEAD_16_17 = 0;
TAIL_16_17 = 0;
    work_Subtracter__140_99__16(1 );
HEAD_58_59 = 0;
TAIL_58_59 = 0;
    work_Subtracter__511_148__58(1 );
HEAD_46_47 = 0;
TAIL_46_47 = 0;
    work_Subtracter__405_134__46(1 );
HEAD_34_35 = 0;
TAIL_34_35 = 0;
    work_Subtracter__299_120__34(1 );
HEAD_22_23 = 0;
TAIL_22_23 = 0;
    work_Subtracter__193_106__22(1 );
HEAD_64_65 = 0;
TAIL_64_65 = 0;
    work_Subtracter__564_155__64(1 );
HEAD_7_8 = 0;
TAIL_7_8 = 0;
    work_Subtracter__87_92__7(1 );
HEAD_52_53 = 0;
TAIL_52_53 = 0;
    work_Subtracter__458_141__52(1 );
HEAD_40_41 = 0;
TAIL_40_41 = 0;
    work_Subtracter__352_127__40(1 );
HEAD_41_9 = 0;
TAIL_41_9 = 0;
    work_Amplify__357_128__41(1 );
HEAD_29_9 = 0;
TAIL_29_9 = 0;
    work_Amplify__251_114__29(1 );
HEAD_17_9 = 0;
TAIL_17_9 = 0;
    work_Amplify__145_100__17(1 );
HEAD_59_9 = 0;
TAIL_59_9 = 0;
    work_Amplify__516_149__59(1 );
HEAD_47_9 = 0;
TAIL_47_9 = 0;
    work_Amplify__410_135__47(1 );
HEAD_35_9 = 0;
TAIL_35_9 = 0;
    work_Amplify__304_121__35(1 );
HEAD_23_9 = 0;
TAIL_23_9 = 0;
    work_Amplify__198_107__23(1 );
HEAD_65_9 = 0;
TAIL_65_9 = 0;
    work_Amplify__569_156__65(1 );
HEAD_8_9 = 0;
TAIL_8_9 = 0;
    work_Amplify__92_93__8(1 );
HEAD_53_9 = 0;
TAIL_53_9 = 0;
    work_Amplify__463_142__53(1 );
HEAD_9_10 = 0;
TAIL_9_10 = 0;
    __joiner_9_work(1 );
HEAD_10_11 = 0;
TAIL_10_11 = 0;
    work_AnonFilter_a0__574_157__10(1 );
    work_FloatPrinter__578_158__11(1 );
  }
if (__timer_enabled) {
  }


  return 0;
}

// moved or inserted by concat_cluster_threads.pl
#include <message.h>
message *__msg_stack_24;
message *__msg_stack_20;
message *__msg_stack_1;
message *__msg_stack_5;
message *__msg_stack_2;
message *__msg_stack_38;
message *__msg_stack_56;
message *__msg_stack_64;
message *__msg_stack_13;
message *__msg_stack_51;
message *__msg_stack_11;
message *__msg_stack_29;
message *__msg_stack_12;
message *__msg_stack_28;
message *__msg_stack_63;
message *__msg_stack_14;
message *__msg_stack_21;
message *__msg_stack_27;
message *__msg_stack_44;
message *__msg_stack_39;
message *__msg_stack_66;
message *__msg_stack_53;
message *__msg_stack_19;
message *__msg_stack_3;
message *__msg_stack_57;
message *__msg_stack_31;
message *__msg_stack_47;
message *__msg_stack_15;
message *__msg_stack_45;
message *__msg_stack_17;
message *__msg_stack_55;
message *__msg_stack_26;
message *__msg_stack_46;
message *__msg_stack_7;
message *__msg_stack_43;
message *__msg_stack_16;
message *__msg_stack_23;
message *__msg_stack_35;
message *__msg_stack_59;
message *__msg_stack_30;
message *__msg_stack_34;
message *__msg_stack_9;
message *__msg_stack_48;
message *__msg_stack_54;
message *__msg_stack_10;
message *__msg_stack_36;
message *__msg_stack_0;
message *__msg_stack_52;
message *__msg_stack_58;
message *__msg_stack_41;
message *__msg_stack_62;
message *__msg_stack_18;
message *__msg_stack_4;
message *__msg_stack_42;
message *__msg_stack_37;
message *__msg_stack_49;
message *__msg_stack_60;
message *__msg_stack_65;
message *__msg_stack_61;
message *__msg_stack_32;
message *__msg_stack_22;
message *__msg_stack_50;
message *__msg_stack_33;
message *__msg_stack_40;
message *__msg_stack_25;
message *__msg_stack_8;
message *__msg_stack_6;

// end of moved or inserted by concat_cluster_threads.pl

// peek: 0 pop: 0 push 1
// init counts: 379 steady counts: 5

// ClusterFusion isEliminated: false


#include <mysocket.h>
#include <sdep.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "global.h"

int __number_of_iterations_0;
int __counter_0 = 0;
int __steady_0 = 0;
int __tmp_0 = 0;
int __tmp2_0 = 0;
int *__state_flag_0 = NULL;
thread_info *__thread_0 = NULL;



float x__0__0 = 0.0f;
void save_file_pointer__0(object_write_buffer *buf);
void load_file_pointer__0(object_write_buffer *buf);

inline void check_status__0() {
  check_thread_status(__state_flag_0, __thread_0);
}

void check_status_during_io__0() {
  check_thread_status_during_io(__state_flag_0, __thread_0);
}

void __init_thread_info_0(thread_info *info) {
  __state_flag_0 = info->get_state_flag();
}

thread_info *__get_thread_info_0() {
  if (__thread_0 != NULL) return __thread_0;
  __thread_0 = new thread_info(0, check_status_during_io__0);
  __init_thread_info_0(__thread_0);
  return __thread_0;
}

void __declare_sockets_0() {
  init_instance::add_outgoing(0,1, DATA_SOCKET);
}

void __init_sockets_0(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_0() {
}

void __peek_sockets_0() {
}

 
void init_FloatOneSource__4_82__0();
inline void check_status__0();

void work_FloatOneSource__4_82__0(int);



inline void __push_0_1(float data) {
BUFFER_0_1[HEAD_0_1]=data;
HEAD_0_1++;
}



 
void init_FloatOneSource__4_82__0(){
  ((x__0__0) = ((float)0.0))/*float*/;
}
void save_file_pointer__0(object_write_buffer *buf) {}
void load_file_pointer__0(object_write_buffer *buf) {}
 
void work_FloatOneSource__4_82__0(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp41__3 = 0.0f;/* float */

      // mark begin: SIRFilter FloatOneSource

      (__tmp41__3 = ((x__0__0)++))/*float*/;
      __push_0_1(__tmp41__3);
      // mark end: SIRFilter FloatOneSource

    }
  }
}

// peek: 64 pop: 5 push 1
// init counts: 64 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_1;
int __counter_1 = 0;
int __steady_1 = 0;
int __tmp_1 = 0;
int __tmp2_1 = 0;
int *__state_flag_1 = NULL;
thread_info *__thread_1 = NULL;



float coeff__5__1[64] = {0};
void save_peek_buffer__1(object_write_buffer *buf);
void load_peek_buffer__1(object_write_buffer *buf);
void save_file_pointer__1(object_write_buffer *buf);
void load_file_pointer__1(object_write_buffer *buf);

inline void check_status__1() {
  check_thread_status(__state_flag_1, __thread_1);
}

void check_status_during_io__1() {
  check_thread_status_during_io(__state_flag_1, __thread_1);
}

void __init_thread_info_1(thread_info *info) {
  __state_flag_1 = info->get_state_flag();
}

thread_info *__get_thread_info_1() {
  if (__thread_1 != NULL) return __thread_1;
  __thread_1 = new thread_info(1, check_status_during_io__1);
  __init_thread_info_1(__thread_1);
  return __thread_1;
}

void __declare_sockets_1() {
  init_instance::add_incoming(0,1, DATA_SOCKET);
  init_instance::add_outgoing(1,2, DATA_SOCKET);
}

void __init_sockets_1(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_1() {
}

void __peek_sockets_1() {
}

 
void init_LowPassFilter__25_84__1();
inline void check_status__1();

void work_LowPassFilter__25_84__1(int);


inline float __pop_0_1() {
float res=BUFFER_0_1[TAIL_0_1];
TAIL_0_1++;
return res;
}

inline void __pop_0_1(int n) {
TAIL_0_1+=n;
}

inline float __peek_0_1(int offs) {
return BUFFER_0_1[TAIL_0_1+offs];
}



inline void __push_1_2(float data) {
BUFFER_1_2[HEAD_1_2]=data;
HEAD_1_2++;
}



 
void init_LowPassFilter__25_84__1(){
  int i__13 = 0;/* int */
  float __tmp2__14 = 0.0f;/* float */
  float __tmp3__15 = 0.0f;/* float */
  float __tmp4__16 = 0.0f;/* float */
  float __tmp5__17 = 0.0f;/* float */
  double __tmp6__18 = 0.0f;/* double */
  double __tmp7__19 = 0.0f;/* double */
  float __tmp8__20 = 0.0f;/* float */
  float __tmp9__21 = 0.0f;/* float */
  float __tmp10__22 = 0.0f;/* float */
  double __tmp11__23 = 0.0f;/* double */
  double __tmp12__24 = 0.0f;/* double */

  for ((i__13 = 0)/*int*/; (i__13 < 64); (i__13++)) {if (((((float)(i__13)) - ((float)31.5)) == ((float)0.0))) {(((coeff__5__1)[(int)i__13]) = ((float)0.864))/*float*/; } else {{
      (__tmp7__19 = ((double)((((float)2.7143362) * (((float)(i__13)) - ((float)31.5))))))/*double*/;
      (__tmp6__18 = sinf(__tmp7__19))/*double*/;
      (__tmp5__17 = ((float)(__tmp6__18)))/*float*/;
      (__tmp4__16 = (__tmp5__17 / ((float)3.1415927)))/*float*/;
      (__tmp3__15 = (__tmp4__16 / (((float)(i__13)) - ((float)31.5))))/*float*/;
      (__tmp12__24 = ((double)(((((float)6.2831855) * ((float)(i__13))) / ((float)63.0)))))/*double*/;
      (__tmp11__23 = cosf(__tmp12__24))/*double*/;
      (__tmp10__22 = ((float)(__tmp11__23)))/*float*/;
      (__tmp9__21 = (((float)0.46) * __tmp10__22))/*float*/;
      (__tmp8__20 = (((float)0.54) - __tmp9__21))/*float*/;
      (__tmp2__14 = (__tmp3__15 * __tmp8__20))/*float*/;
      (((coeff__5__1)[(int)i__13]) = __tmp2__14)/*float*/;
    }}
  }
}
void save_file_pointer__1(object_write_buffer *buf) {}
void load_file_pointer__1(object_write_buffer *buf) {}
 
void work_LowPassFilter__25_84__1(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__8 = 0.0f;/* float */
      float __tmp42__9 = 0.0f;/* float */
      float __tmp43__10 = 0.0f;/* float */
      int i__conflict__0__11 = 0;/* int */
      int i__12 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__8 = ((float)0.0))/*float*/;
      for ((i__conflict__0__11 = 0)/*int*/; (i__conflict__0__11 < 64); (i__conflict__0__11++)) {{
          __peek_0_1(i__conflict__0__11);
          __tmp43__10 = BUFFER_0_1[TAIL_0_1+i__conflict__0__11];
;
          (__tmp42__9 = (__tmp43__10 * ((coeff__5__1)[(int)i__conflict__0__11])))/*float*/;
          (sum__8 = (sum__8 + __tmp42__9))/*float*/;
        }
      }
      __push_1_2(sum__8);
      for ((i__12 = 0)/*int*/; (i__12 < 4); (i__12++)) {__pop_0_1();
      }
      __pop_0_1();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// peek: 10 pop: 10 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_10;
int __counter_10 = 0;
int __steady_10 = 0;
int __tmp_10 = 0;
int __tmp2_10 = 0;
int *__state_flag_10 = NULL;
thread_info *__thread_10 = NULL;



void save_peek_buffer__10(object_write_buffer *buf);
void load_peek_buffer__10(object_write_buffer *buf);
void save_file_pointer__10(object_write_buffer *buf);
void load_file_pointer__10(object_write_buffer *buf);

inline void check_status__10() {
  check_thread_status(__state_flag_10, __thread_10);
}

void check_status_during_io__10() {
  check_thread_status_during_io(__state_flag_10, __thread_10);
}

void __init_thread_info_10(thread_info *info) {
  __state_flag_10 = info->get_state_flag();
}

thread_info *__get_thread_info_10() {
  if (__thread_10 != NULL) return __thread_10;
  __thread_10 = new thread_info(10, check_status_during_io__10);
  __init_thread_info_10(__thread_10);
  return __thread_10;
}

void __declare_sockets_10() {
  init_instance::add_incoming(9,10, DATA_SOCKET);
  init_instance::add_outgoing(10,11, DATA_SOCKET);
}

void __init_sockets_10(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_10() {
}

void __peek_sockets_10() {
}

 
void init_AnonFilter_a0__574_157__10();
inline void check_status__10();

void work_AnonFilter_a0__574_157__10(int);


inline float __pop_9_10() {
float res=BUFFER_9_10[TAIL_9_10];
TAIL_9_10++;
return res;
}

inline void __pop_9_10(int n) {
TAIL_9_10+=n;
}

inline float __peek_9_10(int offs) {
return BUFFER_9_10[TAIL_9_10+offs];
}



inline void __push_10_11(float data) {
BUFFER_10_11[HEAD_10_11]=data;
HEAD_10_11++;
}



 
void init_AnonFilter_a0__574_157__10(){
}
void save_file_pointer__10(object_write_buffer *buf) {}
void load_file_pointer__10(object_write_buffer *buf) {}
 
void work_AnonFilter_a0__574_157__10(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__572 = 0.0f;/* float */
      int i__573 = 0;/* int */
      float __tmp74 = 0.0f;/* float */
      float __tmp75 = 0.0f;/* float */

      // mark begin: SIRFilter AnonFilter_a0

      (sum__572 = ((float)0.0))/*float*/;
      for ((i__573 = 0)/*int*/; (i__573 < 10); (i__573++)) {{
          __tmp75 = BUFFER_9_10[TAIL_9_10]; TAIL_9_10++;
;
          (__tmp74 = (sum__572 + __tmp75))/*float*/;
          (sum__572 = __tmp74)/*float*/;
        }
      }
      __push_10_11(sum__572);
      // mark end: SIRFilter AnonFilter_a0

    }
  }
}

// peek: 1 pop: 1 push 0
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_11;
int __counter_11 = 0;
int __steady_11 = 0;
int __tmp_11 = 0;
int __tmp2_11 = 0;
int *__state_flag_11 = NULL;
thread_info *__thread_11 = NULL;



void save_peek_buffer__11(object_write_buffer *buf);
void load_peek_buffer__11(object_write_buffer *buf);
void save_file_pointer__11(object_write_buffer *buf);
void load_file_pointer__11(object_write_buffer *buf);

inline void check_status__11() {
  check_thread_status(__state_flag_11, __thread_11);
}

void check_status_during_io__11() {
  check_thread_status_during_io(__state_flag_11, __thread_11);
}

void __init_thread_info_11(thread_info *info) {
  __state_flag_11 = info->get_state_flag();
}

thread_info *__get_thread_info_11() {
  if (__thread_11 != NULL) return __thread_11;
  __thread_11 = new thread_info(11, check_status_during_io__11);
  __init_thread_info_11(__thread_11);
  return __thread_11;
}

void __declare_sockets_11() {
  init_instance::add_incoming(10,11, DATA_SOCKET);
}

void __init_sockets_11(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_11() {
}

void __peek_sockets_11() {
}

 
void init_FloatPrinter__578_158__11();
inline void check_status__11();

void work_FloatPrinter__578_158__11(int);


inline float __pop_10_11() {
float res=BUFFER_10_11[TAIL_10_11];
TAIL_10_11++;
return res;
}

inline void __pop_10_11(int n) {
TAIL_10_11+=n;
}

inline float __peek_10_11(int offs) {
return BUFFER_10_11[TAIL_10_11+offs];
}


 
void init_FloatPrinter__578_158__11(){
}
void save_file_pointer__11(object_write_buffer *buf) {}
void load_file_pointer__11(object_write_buffer *buf) {}
 
void work_FloatPrinter__578_158__11(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__577 = 0.0f;/* float */

      // mark begin: SIRFilter FloatPrinter

      v__577 = BUFFER_10_11[TAIL_10_11]; TAIL_10_11++;
;

      // TIMER_PRINT_CODE: __print_sink__ += (int)(v__577); 

      // mark end: SIRFilter FloatPrinter

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_12;
int __counter_12 = 0;
int __steady_12 = 0;
int __tmp_12 = 0;
int __tmp2_12 = 0;
int *__state_flag_12 = NULL;
thread_info *__thread_12 = NULL;



float coeff__61__12[64] = {0};
void save_peek_buffer__12(object_write_buffer *buf);
void load_peek_buffer__12(object_write_buffer *buf);
void save_file_pointer__12(object_write_buffer *buf);
void load_file_pointer__12(object_write_buffer *buf);

inline void check_status__12() {
  check_thread_status(__state_flag_12, __thread_12);
}

void check_status_during_io__12() {
  check_thread_status_during_io(__state_flag_12, __thread_12);
}

void __init_thread_info_12(thread_info *info) {
  __state_flag_12 = info->get_state_flag();
}

thread_info *__get_thread_info_12() {
  if (__thread_12 != NULL) return __thread_12;
  __thread_12 = new thread_info(12, check_status_during_io__12);
  __init_thread_info_12(__thread_12);
  return __thread_12;
}

void __declare_sockets_12() {
  init_instance::add_incoming(4,12, DATA_SOCKET);
  init_instance::add_outgoing(12,6, DATA_SOCKET);
}

void __init_sockets_12(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_12() {
}

void __peek_sockets_12() {
}

 
void init_LowPassFilter__81_91__12();
inline void check_status__12();

void work_LowPassFilter__81_91__12(int);


inline float __pop_4_12() {
float res=BUFFER_4_12[TAIL_4_12];
TAIL_4_12++;
return res;
}

inline void __pop_4_12(int n) {
TAIL_4_12+=n;
}

inline float __peek_4_12(int offs) {
return BUFFER_4_12[TAIL_4_12+offs];
}



inline void __push_12_6(float data) {
BUFFER_12_6[HEAD_12_6]=data;
HEAD_12_6++;
}



 
void init_LowPassFilter__81_91__12(){
  int i__69 = 0;/* int */
  float __tmp30__70 = 0.0f;/* float */
  float __tmp31__71 = 0.0f;/* float */
  float __tmp32__72 = 0.0f;/* float */
  float __tmp33__73 = 0.0f;/* float */
  double __tmp34__74 = 0.0f;/* double */
  double __tmp35__75 = 0.0f;/* double */
  float __tmp36__76 = 0.0f;/* float */
  float __tmp37__77 = 0.0f;/* float */
  float __tmp38__78 = 0.0f;/* float */
  double __tmp39__79 = 0.0f;/* double */
  double __tmp40__80 = 0.0f;/* double */

  for ((i__69 = 0)/*int*/; (i__69 < 64); (i__69++)) {if (((((float)(i__69)) - ((float)31.5)) == ((float)0.0))) {(((coeff__61__12)[(int)i__69]) = ((float)6.2225394E-7))/*float*/; } else {{
      (__tmp35__75 = ((double)((((float)1.9548684E-6) * (((float)(i__69)) - ((float)31.5))))))/*double*/;
      (__tmp34__74 = sinf(__tmp35__75))/*double*/;
      (__tmp33__73 = ((float)(__tmp34__74)))/*float*/;
      (__tmp32__72 = (__tmp33__73 / ((float)3.1415927)))/*float*/;
      (__tmp31__71 = (__tmp32__72 / (((float)(i__69)) - ((float)31.5))))/*float*/;
      (__tmp40__80 = ((double)(((((float)6.2831855) * ((float)(i__69))) / ((float)63.0)))))/*double*/;
      (__tmp39__79 = cosf(__tmp40__80))/*double*/;
      (__tmp38__78 = ((float)(__tmp39__79)))/*float*/;
      (__tmp37__77 = (((float)0.46) * __tmp38__78))/*float*/;
      (__tmp36__76 = (((float)0.54) - __tmp37__77))/*float*/;
      (__tmp30__70 = (__tmp31__71 * __tmp36__76))/*float*/;
      (((coeff__61__12)[(int)i__69]) = __tmp30__70)/*float*/;
    }}
  }
}
void save_file_pointer__12(object_write_buffer *buf) {}
void load_file_pointer__12(object_write_buffer *buf) {}
 
void work_LowPassFilter__81_91__12(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__64 = 0.0f;/* float */
      float __tmp49__65 = 0.0f;/* float */
      float __tmp50__66 = 0.0f;/* float */
      int i__conflict__0__67 = 0;/* int */
      int i__68 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__64 = ((float)0.0))/*float*/;
      for ((i__conflict__0__67 = 0)/*int*/; (i__conflict__0__67 < 64); (i__conflict__0__67++)) {{
          __peek_4_12(i__conflict__0__67);
          __tmp50__66 = BUFFER_4_12[TAIL_4_12+i__conflict__0__67];
;
          (__tmp49__65 = (__tmp50__66 * ((coeff__61__12)[(int)i__conflict__0__67])))/*float*/;
          (sum__64 = (sum__64 + __tmp49__65))/*float*/;
        }
      }
      __push_12_6(sum__64);
      for ((i__68 = 0)/*int*/; (i__68 < 0); (i__68++)) {__pop_4_12();
      }
      __pop_4_12();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_13;
int __counter_13 = 0;
int __steady_13 = 0;
int __tmp_13 = 0;
int __tmp2_13 = 0;
int *__state_flag_13 = NULL;
thread_info *__thread_13 = NULL;



inline void check_status__13() {
  check_thread_status(__state_flag_13, __thread_13);
}

void check_status_during_io__13() {
  check_thread_status_during_io(__state_flag_13, __thread_13);
}

void __init_thread_info_13(thread_info *info) {
  __state_flag_13 = info->get_state_flag();
}

thread_info *__get_thread_info_13() {
  if (__thread_13 != NULL) return __thread_13;
  __thread_13 = new thread_info(13, check_status_during_io__13);
  __init_thread_info_13(__thread_13);
  return __thread_13;
}

void __declare_sockets_13() {
  init_instance::add_incoming(3,13, DATA_SOCKET);
  init_instance::add_outgoing(13,14, DATA_SOCKET);
  init_instance::add_outgoing(13,18, DATA_SOCKET);
}

void __init_sockets_13(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_13() {
}

void __peek_sockets_13() {
}

inline float __pop_3_13() {
float res=BUFFER_3_13[TAIL_3_13];
TAIL_3_13++;
return res;
}


inline void __push_13_14(float data) {
BUFFER_13_14[HEAD_13_14]=data;
HEAD_13_14++;
}



inline void __push_13_18(float data) {
BUFFER_13_18[HEAD_13_18]=data;
HEAD_13_18++;
}



void __splitter_13_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_13[TAIL_3_13]; TAIL_3_13++;
;
  __push_13_14(tmp);
  __push_13_18(tmp);
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_14;
int __counter_14 = 0;
int __steady_14 = 0;
int __tmp_14 = 0;
int __tmp2_14 = 0;
int *__state_flag_14 = NULL;
thread_info *__thread_14 = NULL;



float coeff__93__14[64] = {0};
void save_peek_buffer__14(object_write_buffer *buf);
void load_peek_buffer__14(object_write_buffer *buf);
void save_file_pointer__14(object_write_buffer *buf);
void load_file_pointer__14(object_write_buffer *buf);

inline void check_status__14() {
  check_thread_status(__state_flag_14, __thread_14);
}

void check_status_during_io__14() {
  check_thread_status_during_io(__state_flag_14, __thread_14);
}

void __init_thread_info_14(thread_info *info) {
  __state_flag_14 = info->get_state_flag();
}

thread_info *__get_thread_info_14() {
  if (__thread_14 != NULL) return __thread_14;
  __thread_14 = new thread_info(14, check_status_during_io__14);
  __init_thread_info_14(__thread_14);
  return __thread_14;
}

void __declare_sockets_14() {
  init_instance::add_incoming(13,14, DATA_SOCKET);
  init_instance::add_outgoing(14,15, DATA_SOCKET);
}

void __init_sockets_14(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_14() {
}

void __peek_sockets_14() {
}

 
void init_LowPassFilter__113_97__14();
inline void check_status__14();

void work_LowPassFilter__113_97__14(int);


inline float __pop_13_14() {
float res=BUFFER_13_14[TAIL_13_14];
TAIL_13_14++;
return res;
}

inline void __pop_13_14(int n) {
TAIL_13_14+=n;
}

inline float __peek_13_14(int offs) {
return BUFFER_13_14[TAIL_13_14+offs];
}



inline void __push_14_15(float data) {
BUFFER_14_15[HEAD_14_15]=data;
HEAD_14_15++;
}



 
void init_LowPassFilter__113_97__14(){
  int i__101 = 0;/* int */
  float __tmp19__102 = 0.0f;/* float */
  float __tmp20__103 = 0.0f;/* float */
  float __tmp21__104 = 0.0f;/* float */
  float __tmp22__105 = 0.0f;/* float */
  double __tmp23__106 = 0.0f;/* double */
  double __tmp24__107 = 0.0f;/* double */
  float __tmp25__108 = 0.0f;/* float */
  float __tmp26__109 = 0.0f;/* float */
  float __tmp27__110 = 0.0f;/* float */
  double __tmp28__111 = 0.0f;/* double */
  double __tmp29__112 = 0.0f;/* double */

  for ((i__101 = 0)/*int*/; (i__101 < 64); (i__101++)) {if (((((float)(i__101)) - ((float)31.5)) == ((float)0.0))) {(((coeff__93__14)[(int)i__101]) = ((float)6.2225394E-7))/*float*/; } else {{
      (__tmp24__107 = ((double)((((float)1.9548684E-6) * (((float)(i__101)) - ((float)31.5))))))/*double*/;
      (__tmp23__106 = sinf(__tmp24__107))/*double*/;
      (__tmp22__105 = ((float)(__tmp23__106)))/*float*/;
      (__tmp21__104 = (__tmp22__105 / ((float)3.1415927)))/*float*/;
      (__tmp20__103 = (__tmp21__104 / (((float)(i__101)) - ((float)31.5))))/*float*/;
      (__tmp29__112 = ((double)(((((float)6.2831855) * ((float)(i__101))) / ((float)63.0)))))/*double*/;
      (__tmp28__111 = cosf(__tmp29__112))/*double*/;
      (__tmp27__110 = ((float)(__tmp28__111)))/*float*/;
      (__tmp26__109 = (((float)0.46) * __tmp27__110))/*float*/;
      (__tmp25__108 = (((float)0.54) - __tmp26__109))/*float*/;
      (__tmp19__102 = (__tmp20__103 * __tmp25__108))/*float*/;
      (((coeff__93__14)[(int)i__101]) = __tmp19__102)/*float*/;
    }}
  }
}
void save_file_pointer__14(object_write_buffer *buf) {}
void load_file_pointer__14(object_write_buffer *buf) {}
 
void work_LowPassFilter__113_97__14(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__96 = 0.0f;/* float */
      float __tmp47__97 = 0.0f;/* float */
      float __tmp48__98 = 0.0f;/* float */
      int i__conflict__0__99 = 0;/* int */
      int i__100 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__96 = ((float)0.0))/*float*/;
      for ((i__conflict__0__99 = 0)/*int*/; (i__conflict__0__99 < 64); (i__conflict__0__99++)) {{
          __peek_13_14(i__conflict__0__99);
          __tmp48__98 = BUFFER_13_14[TAIL_13_14+i__conflict__0__99];
;
          (__tmp47__97 = (__tmp48__98 * ((coeff__93__14)[(int)i__conflict__0__99])))/*float*/;
          (sum__96 = (sum__96 + __tmp47__97))/*float*/;
        }
      }
      __push_14_15(sum__96);
      for ((i__100 = 0)/*int*/; (i__100 < 0); (i__100++)) {__pop_13_14();
      }
      __pop_13_14();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_15;
int __counter_15 = 0;
int __steady_15 = 0;
int __tmp_15 = 0;
int __tmp2_15 = 0;
int *__state_flag_15 = NULL;
thread_info *__thread_15 = NULL;



inline void check_status__15() {
  check_thread_status(__state_flag_15, __thread_15);
}

void check_status_during_io__15() {
  check_thread_status_during_io(__state_flag_15, __thread_15);
}

void __init_thread_info_15(thread_info *info) {
  __state_flag_15 = info->get_state_flag();
}

thread_info *__get_thread_info_15() {
  if (__thread_15 != NULL) return __thread_15;
  __thread_15 = new thread_info(15, check_status_during_io__15);
  __init_thread_info_15(__thread_15);
  return __thread_15;
}

void __declare_sockets_15() {
  init_instance::add_incoming(14,15, DATA_SOCKET);
  init_instance::add_incoming(18,15, DATA_SOCKET);
  init_instance::add_outgoing(15,16, DATA_SOCKET);
}

void __init_sockets_15(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_15() {
}

void __peek_sockets_15() {
}


inline void __push_15_16(float data) {
BUFFER_15_16[HEAD_15_16]=data;
HEAD_15_16++;
}


inline float __pop_14_15() {
float res=BUFFER_14_15[TAIL_14_15];
TAIL_14_15++;
return res;
}

inline float __pop_18_15() {
float res=BUFFER_18_15[TAIL_18_15];
TAIL_18_15++;
return res;
}


void __joiner_15_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_15_16(__pop_14_15());
    __push_15_16(__pop_18_15());
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_16;
int __counter_16 = 0;
int __steady_16 = 0;
int __tmp_16 = 0;
int __tmp2_16 = 0;
int *__state_flag_16 = NULL;
thread_info *__thread_16 = NULL;



void save_peek_buffer__16(object_write_buffer *buf);
void load_peek_buffer__16(object_write_buffer *buf);
void save_file_pointer__16(object_write_buffer *buf);
void load_file_pointer__16(object_write_buffer *buf);

inline void check_status__16() {
  check_thread_status(__state_flag_16, __thread_16);
}

void check_status_during_io__16() {
  check_thread_status_during_io(__state_flag_16, __thread_16);
}

void __init_thread_info_16(thread_info *info) {
  __state_flag_16 = info->get_state_flag();
}

thread_info *__get_thread_info_16() {
  if (__thread_16 != NULL) return __thread_16;
  __thread_16 = new thread_info(16, check_status_during_io__16);
  __init_thread_info_16(__thread_16);
  return __thread_16;
}

void __declare_sockets_16() {
  init_instance::add_incoming(15,16, DATA_SOCKET);
  init_instance::add_outgoing(16,17, DATA_SOCKET);
}

void __init_sockets_16(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_16() {
}

void __peek_sockets_16() {
}

 
void init_Subtracter__140_99__16();
inline void check_status__16();

void work_Subtracter__140_99__16(int);


inline float __pop_15_16() {
float res=BUFFER_15_16[TAIL_15_16];
TAIL_15_16++;
return res;
}

inline void __pop_15_16(int n) {
TAIL_15_16+=n;
}

inline float __peek_15_16(int offs) {
return BUFFER_15_16[TAIL_15_16+offs];
}



inline void __push_16_17(float data) {
BUFFER_16_17[HEAD_16_17]=data;
HEAD_16_17++;
}



 
void init_Subtracter__140_99__16(){
}
void save_file_pointer__16(object_write_buffer *buf) {}
void load_file_pointer__16(object_write_buffer *buf) {}
 
void work_Subtracter__140_99__16(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__137 = 0.0f;/* float */
      float p0__138 = 0.0f;/* float */
      float v__139 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__137 = BUFFER_15_16[TAIL_15_16+1];
;
      p0__138 = BUFFER_15_16[TAIL_15_16+0];
;
      (v__139 = (p1__137 - p0__138))/*float*/;
      __push_16_17(v__139);
      __pop_15_16(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_17;
int __counter_17 = 0;
int __steady_17 = 0;
int __tmp_17 = 0;
int __tmp2_17 = 0;
int *__state_flag_17 = NULL;
thread_info *__thread_17 = NULL;



void save_peek_buffer__17(object_write_buffer *buf);
void load_peek_buffer__17(object_write_buffer *buf);
void save_file_pointer__17(object_write_buffer *buf);
void load_file_pointer__17(object_write_buffer *buf);

inline void check_status__17() {
  check_thread_status(__state_flag_17, __thread_17);
}

void check_status_during_io__17() {
  check_thread_status_during_io(__state_flag_17, __thread_17);
}

void __init_thread_info_17(thread_info *info) {
  __state_flag_17 = info->get_state_flag();
}

thread_info *__get_thread_info_17() {
  if (__thread_17 != NULL) return __thread_17;
  __thread_17 = new thread_info(17, check_status_during_io__17);
  __init_thread_info_17(__thread_17);
  return __thread_17;
}

void __declare_sockets_17() {
  init_instance::add_incoming(16,17, DATA_SOCKET);
  init_instance::add_outgoing(17,9, DATA_SOCKET);
}

void __init_sockets_17(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_17() {
}

void __peek_sockets_17() {
}

 
void init_Amplify__145_100__17();
inline void check_status__17();

void work_Amplify__145_100__17(int);


inline float __pop_16_17() {
float res=BUFFER_16_17[TAIL_16_17];
TAIL_16_17++;
return res;
}

inline void __pop_16_17(int n) {
TAIL_16_17+=n;
}

inline float __peek_16_17(int offs) {
return BUFFER_16_17[TAIL_16_17+offs];
}



inline void __push_17_9(float data) {
BUFFER_17_9[HEAD_17_9]=data;
HEAD_17_9++;
}



 
void init_Amplify__145_100__17(){
}
void save_file_pointer__17(object_write_buffer *buf) {}
void load_file_pointer__17(object_write_buffer *buf) {}
 
void work_Amplify__145_100__17(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__143 = 0.0f;/* float */
      float __tmp52__144 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__144 = BUFFER_16_17[TAIL_16_17]; TAIL_16_17++;
;
      (__tmp51__143 = (__tmp52__144 * ((float)1.3)))/*float*/;
      __push_17_9(__tmp51__143);
      // mark end: SIRFilter Amplify

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_18;
int __counter_18 = 0;
int __steady_18 = 0;
int __tmp_18 = 0;
int __tmp2_18 = 0;
int *__state_flag_18 = NULL;
thread_info *__thread_18 = NULL;



float coeff__114__18[64] = {0};
void save_peek_buffer__18(object_write_buffer *buf);
void load_peek_buffer__18(object_write_buffer *buf);
void save_file_pointer__18(object_write_buffer *buf);
void load_file_pointer__18(object_write_buffer *buf);

inline void check_status__18() {
  check_thread_status(__state_flag_18, __thread_18);
}

void check_status_during_io__18() {
  check_thread_status_during_io(__state_flag_18, __thread_18);
}

void __init_thread_info_18(thread_info *info) {
  __state_flag_18 = info->get_state_flag();
}

thread_info *__get_thread_info_18() {
  if (__thread_18 != NULL) return __thread_18;
  __thread_18 = new thread_info(18, check_status_during_io__18);
  __init_thread_info_18(__thread_18);
  return __thread_18;
}

void __declare_sockets_18() {
  init_instance::add_incoming(13,18, DATA_SOCKET);
  init_instance::add_outgoing(18,15, DATA_SOCKET);
}

void __init_sockets_18(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_18() {
}

void __peek_sockets_18() {
}

 
void init_LowPassFilter__134_98__18();
inline void check_status__18();

void work_LowPassFilter__134_98__18(int);


inline float __pop_13_18() {
float res=BUFFER_13_18[TAIL_13_18];
TAIL_13_18++;
return res;
}

inline void __pop_13_18(int n) {
TAIL_13_18+=n;
}

inline float __peek_13_18(int offs) {
return BUFFER_13_18[TAIL_13_18+offs];
}



inline void __push_18_15(float data) {
BUFFER_18_15[HEAD_18_15]=data;
HEAD_18_15++;
}



 
void init_LowPassFilter__134_98__18(){
  int i__122 = 0;/* int */
  float __tmp30__123 = 0.0f;/* float */
  float __tmp31__124 = 0.0f;/* float */
  float __tmp32__125 = 0.0f;/* float */
  float __tmp33__126 = 0.0f;/* float */
  double __tmp34__127 = 0.0f;/* double */
  double __tmp35__128 = 0.0f;/* double */
  float __tmp36__129 = 0.0f;/* float */
  float __tmp37__130 = 0.0f;/* float */
  float __tmp38__131 = 0.0f;/* float */
  double __tmp39__132 = 0.0f;/* double */
  double __tmp40__133 = 0.0f;/* double */

  for ((i__122 = 0)/*int*/; (i__122 < 64); (i__122++)) {if (((((float)(i__122)) - ((float)31.5)) == ((float)0.0))) {(((coeff__114__18)[(int)i__122]) = ((float)8.8E-7))/*float*/; } else {{
      (__tmp35__128 = ((double)((((float)2.7646017E-6) * (((float)(i__122)) - ((float)31.5))))))/*double*/;
      (__tmp34__127 = sinf(__tmp35__128))/*double*/;
      (__tmp33__126 = ((float)(__tmp34__127)))/*float*/;
      (__tmp32__125 = (__tmp33__126 / ((float)3.1415927)))/*float*/;
      (__tmp31__124 = (__tmp32__125 / (((float)(i__122)) - ((float)31.5))))/*float*/;
      (__tmp40__133 = ((double)(((((float)6.2831855) * ((float)(i__122))) / ((float)63.0)))))/*double*/;
      (__tmp39__132 = cosf(__tmp40__133))/*double*/;
      (__tmp38__131 = ((float)(__tmp39__132)))/*float*/;
      (__tmp37__130 = (((float)0.46) * __tmp38__131))/*float*/;
      (__tmp36__129 = (((float)0.54) - __tmp37__130))/*float*/;
      (__tmp30__123 = (__tmp31__124 * __tmp36__129))/*float*/;
      (((coeff__114__18)[(int)i__122]) = __tmp30__123)/*float*/;
    }}
  }
}
void save_file_pointer__18(object_write_buffer *buf) {}
void load_file_pointer__18(object_write_buffer *buf) {}
 
void work_LowPassFilter__134_98__18(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__117 = 0.0f;/* float */
      float __tmp49__118 = 0.0f;/* float */
      float __tmp50__119 = 0.0f;/* float */
      int i__conflict__0__120 = 0;/* int */
      int i__121 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__117 = ((float)0.0))/*float*/;
      for ((i__conflict__0__120 = 0)/*int*/; (i__conflict__0__120 < 64); (i__conflict__0__120++)) {{
          __peek_13_18(i__conflict__0__120);
          __tmp50__119 = BUFFER_13_18[TAIL_13_18+i__conflict__0__120];
;
          (__tmp49__118 = (__tmp50__119 * ((coeff__114__18)[(int)i__conflict__0__120])))/*float*/;
          (sum__117 = (sum__117 + __tmp49__118))/*float*/;
        }
      }
      __push_18_15(sum__117);
      for ((i__121 = 0)/*int*/; (i__121 < 0); (i__121++)) {__pop_13_18();
      }
      __pop_13_18();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_19;
int __counter_19 = 0;
int __steady_19 = 0;
int __tmp_19 = 0;
int __tmp2_19 = 0;
int *__state_flag_19 = NULL;
thread_info *__thread_19 = NULL;



inline void check_status__19() {
  check_thread_status(__state_flag_19, __thread_19);
}

void check_status_during_io__19() {
  check_thread_status_during_io(__state_flag_19, __thread_19);
}

void __init_thread_info_19(thread_info *info) {
  __state_flag_19 = info->get_state_flag();
}

thread_info *__get_thread_info_19() {
  if (__thread_19 != NULL) return __thread_19;
  __thread_19 = new thread_info(19, check_status_during_io__19);
  __init_thread_info_19(__thread_19);
  return __thread_19;
}

void __declare_sockets_19() {
  init_instance::add_incoming(3,19, DATA_SOCKET);
  init_instance::add_outgoing(19,20, DATA_SOCKET);
  init_instance::add_outgoing(19,24, DATA_SOCKET);
}

void __init_sockets_19(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_19() {
}

void __peek_sockets_19() {
}

inline float __pop_3_19() {
float res=BUFFER_3_19[TAIL_3_19];
TAIL_3_19++;
return res;
}


inline void __push_19_20(float data) {
BUFFER_19_20[HEAD_19_20]=data;
HEAD_19_20++;
}



inline void __push_19_24(float data) {
BUFFER_19_24[HEAD_19_24]=data;
HEAD_19_24++;
}



void __splitter_19_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_19[TAIL_3_19]; TAIL_3_19++;
;
  __push_19_20(tmp);
  __push_19_24(tmp);
  }
}


// peek: 2 pop: 1 push 1
// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_2;
int __counter_2 = 0;
int __steady_2 = 0;
int __tmp_2 = 0;
int __tmp2_2 = 0;
int *__state_flag_2 = NULL;
thread_info *__thread_2 = NULL;



void save_peek_buffer__2(object_write_buffer *buf);
void load_peek_buffer__2(object_write_buffer *buf);
void save_file_pointer__2(object_write_buffer *buf);
void load_file_pointer__2(object_write_buffer *buf);

inline void check_status__2() {
  check_thread_status(__state_flag_2, __thread_2);
}

void check_status_during_io__2() {
  check_thread_status_during_io(__state_flag_2, __thread_2);
}

void __init_thread_info_2(thread_info *info) {
  __state_flag_2 = info->get_state_flag();
}

thread_info *__get_thread_info_2() {
  if (__thread_2 != NULL) return __thread_2;
  __thread_2 = new thread_info(2, check_status_during_io__2);
  __init_thread_info_2(__thread_2);
  return __thread_2;
}

void __declare_sockets_2() {
  init_instance::add_incoming(1,2, DATA_SOCKET);
  init_instance::add_outgoing(2,3, DATA_SOCKET);
}

void __init_sockets_2(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_2() {
}

void __peek_sockets_2() {
}

 
void init_FMDemodulator__39_85__2();
inline void check_status__2();

void work_FMDemodulator__39_85__2(int);


inline float __pop_1_2() {
float res=BUFFER_1_2[TAIL_1_2];
TAIL_1_2++;
return res;
}

inline void __pop_1_2(int n) {
TAIL_1_2+=n;
}

inline float __peek_1_2(int offs) {
return BUFFER_1_2[TAIL_1_2+offs];
}



inline void __push_2_3(float data) {
BUFFER_2_3[HEAD_2_3]=data;
HEAD_2_3++;
}



 
void init_FMDemodulator__39_85__2(){
}
void save_file_pointer__2(object_write_buffer *buf) {}
void load_file_pointer__2(object_write_buffer *buf) {}
 
void work_FMDemodulator__39_85__2(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float temp1__28 = 0.0f;/* float */
      float __tmp44__29 = 0.0f;/* float */
      float __tmp45__30 = 0.0f;/* float */
      float __tmp46__31 = 0.0f;/* float */
      float temp2__32 = 0.0f;/* float */
      float __tmp13__33 = 0.0f;/* float */
      float __tmp14__34 = 0.0f;/* float */
      double __tmp15__35 = 0.0f;/* double */
      double __tmp16__36 = 0.0f;/* double */
      double __tmp17__37 = 0.0f;/* double */
      double __tmp18__38 = 0.0f;/* double */

      // mark begin: SIRFilter FMDemodulator

      __tmp45__30 = BUFFER_1_2[TAIL_1_2+0];
;
      __tmp46__31 = BUFFER_1_2[TAIL_1_2+1];
;
      (__tmp44__29 = (__tmp45__30 * __tmp46__31))/*float*/;
      (temp1__28 = ((float)(__tmp44__29)))/*float*/;
      (__tmp16__36 = ((double)(temp1__28)))/*double*/;
      (__tmp15__35 = atanf(__tmp16__36))/*double*/;
      (__tmp14__34 = ((float)(__tmp15__35)))/*float*/;
      (__tmp13__33 = (((float)2.14859168E8) * __tmp14__34))/*float*/;
      (temp2__32 = ((float)(__tmp13__33)))/*float*/;
      (__tmp18__38 = ((double)(temp1__28)))/*double*/;
      (__tmp17__37 = atanf(__tmp18__38))/*double*/;
      ((float)(__tmp17__37));
      __pop_1_2();
      __push_2_3(temp2__32);
      // mark end: SIRFilter FMDemodulator

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_20;
int __counter_20 = 0;
int __steady_20 = 0;
int __tmp_20 = 0;
int __tmp2_20 = 0;
int *__state_flag_20 = NULL;
thread_info *__thread_20 = NULL;



float coeff__146__20[64] = {0};
void save_peek_buffer__20(object_write_buffer *buf);
void load_peek_buffer__20(object_write_buffer *buf);
void save_file_pointer__20(object_write_buffer *buf);
void load_file_pointer__20(object_write_buffer *buf);

inline void check_status__20() {
  check_thread_status(__state_flag_20, __thread_20);
}

void check_status_during_io__20() {
  check_thread_status_during_io(__state_flag_20, __thread_20);
}

void __init_thread_info_20(thread_info *info) {
  __state_flag_20 = info->get_state_flag();
}

thread_info *__get_thread_info_20() {
  if (__thread_20 != NULL) return __thread_20;
  __thread_20 = new thread_info(20, check_status_during_io__20);
  __init_thread_info_20(__thread_20);
  return __thread_20;
}

void __declare_sockets_20() {
  init_instance::add_incoming(19,20, DATA_SOCKET);
  init_instance::add_outgoing(20,21, DATA_SOCKET);
}

void __init_sockets_20(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_20() {
}

void __peek_sockets_20() {
}

 
void init_LowPassFilter__166_104__20();
inline void check_status__20();

void work_LowPassFilter__166_104__20(int);


inline float __pop_19_20() {
float res=BUFFER_19_20[TAIL_19_20];
TAIL_19_20++;
return res;
}

inline void __pop_19_20(int n) {
TAIL_19_20+=n;
}

inline float __peek_19_20(int offs) {
return BUFFER_19_20[TAIL_19_20+offs];
}



inline void __push_20_21(float data) {
BUFFER_20_21[HEAD_20_21]=data;
HEAD_20_21++;
}



 
void init_LowPassFilter__166_104__20(){
  int i__154 = 0;/* int */
  float __tmp19__155 = 0.0f;/* float */
  float __tmp20__156 = 0.0f;/* float */
  float __tmp21__157 = 0.0f;/* float */
  float __tmp22__158 = 0.0f;/* float */
  double __tmp23__159 = 0.0f;/* double */
  double __tmp24__160 = 0.0f;/* double */
  float __tmp25__161 = 0.0f;/* float */
  float __tmp26__162 = 0.0f;/* float */
  float __tmp27__163 = 0.0f;/* float */
  double __tmp28__164 = 0.0f;/* double */
  double __tmp29__165 = 0.0f;/* double */

  for ((i__154 = 0)/*int*/; (i__154 < 64); (i__154++)) {if (((((float)(i__154)) - ((float)31.5)) == ((float)0.0))) {(((coeff__146__20)[(int)i__154]) = ((float)8.8E-7))/*float*/; } else {{
      (__tmp24__160 = ((double)((((float)2.7646017E-6) * (((float)(i__154)) - ((float)31.5))))))/*double*/;
      (__tmp23__159 = sinf(__tmp24__160))/*double*/;
      (__tmp22__158 = ((float)(__tmp23__159)))/*float*/;
      (__tmp21__157 = (__tmp22__158 / ((float)3.1415927)))/*float*/;
      (__tmp20__156 = (__tmp21__157 / (((float)(i__154)) - ((float)31.5))))/*float*/;
      (__tmp29__165 = ((double)(((((float)6.2831855) * ((float)(i__154))) / ((float)63.0)))))/*double*/;
      (__tmp28__164 = cosf(__tmp29__165))/*double*/;
      (__tmp27__163 = ((float)(__tmp28__164)))/*float*/;
      (__tmp26__162 = (((float)0.46) * __tmp27__163))/*float*/;
      (__tmp25__161 = (((float)0.54) - __tmp26__162))/*float*/;
      (__tmp19__155 = (__tmp20__156 * __tmp25__161))/*float*/;
      (((coeff__146__20)[(int)i__154]) = __tmp19__155)/*float*/;
    }}
  }
}
void save_file_pointer__20(object_write_buffer *buf) {}
void load_file_pointer__20(object_write_buffer *buf) {}
 
void work_LowPassFilter__166_104__20(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__149 = 0.0f;/* float */
      float __tmp47__150 = 0.0f;/* float */
      float __tmp48__151 = 0.0f;/* float */
      int i__conflict__0__152 = 0;/* int */
      int i__153 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__149 = ((float)0.0))/*float*/;
      for ((i__conflict__0__152 = 0)/*int*/; (i__conflict__0__152 < 64); (i__conflict__0__152++)) {{
          __peek_19_20(i__conflict__0__152);
          __tmp48__151 = BUFFER_19_20[TAIL_19_20+i__conflict__0__152];
;
          (__tmp47__150 = (__tmp48__151 * ((coeff__146__20)[(int)i__conflict__0__152])))/*float*/;
          (sum__149 = (sum__149 + __tmp47__150))/*float*/;
        }
      }
      __push_20_21(sum__149);
      for ((i__153 = 0)/*int*/; (i__153 < 0); (i__153++)) {__pop_19_20();
      }
      __pop_19_20();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_21;
int __counter_21 = 0;
int __steady_21 = 0;
int __tmp_21 = 0;
int __tmp2_21 = 0;
int *__state_flag_21 = NULL;
thread_info *__thread_21 = NULL;



inline void check_status__21() {
  check_thread_status(__state_flag_21, __thread_21);
}

void check_status_during_io__21() {
  check_thread_status_during_io(__state_flag_21, __thread_21);
}

void __init_thread_info_21(thread_info *info) {
  __state_flag_21 = info->get_state_flag();
}

thread_info *__get_thread_info_21() {
  if (__thread_21 != NULL) return __thread_21;
  __thread_21 = new thread_info(21, check_status_during_io__21);
  __init_thread_info_21(__thread_21);
  return __thread_21;
}

void __declare_sockets_21() {
  init_instance::add_incoming(20,21, DATA_SOCKET);
  init_instance::add_incoming(24,21, DATA_SOCKET);
  init_instance::add_outgoing(21,22, DATA_SOCKET);
}

void __init_sockets_21(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_21() {
}

void __peek_sockets_21() {
}


inline void __push_21_22(float data) {
BUFFER_21_22[HEAD_21_22]=data;
HEAD_21_22++;
}


inline float __pop_20_21() {
float res=BUFFER_20_21[TAIL_20_21];
TAIL_20_21++;
return res;
}

inline float __pop_24_21() {
float res=BUFFER_24_21[TAIL_24_21];
TAIL_24_21++;
return res;
}


void __joiner_21_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_21_22(__pop_20_21());
    __push_21_22(__pop_24_21());
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_22;
int __counter_22 = 0;
int __steady_22 = 0;
int __tmp_22 = 0;
int __tmp2_22 = 0;
int *__state_flag_22 = NULL;
thread_info *__thread_22 = NULL;



void save_peek_buffer__22(object_write_buffer *buf);
void load_peek_buffer__22(object_write_buffer *buf);
void save_file_pointer__22(object_write_buffer *buf);
void load_file_pointer__22(object_write_buffer *buf);

inline void check_status__22() {
  check_thread_status(__state_flag_22, __thread_22);
}

void check_status_during_io__22() {
  check_thread_status_during_io(__state_flag_22, __thread_22);
}

void __init_thread_info_22(thread_info *info) {
  __state_flag_22 = info->get_state_flag();
}

thread_info *__get_thread_info_22() {
  if (__thread_22 != NULL) return __thread_22;
  __thread_22 = new thread_info(22, check_status_during_io__22);
  __init_thread_info_22(__thread_22);
  return __thread_22;
}

void __declare_sockets_22() {
  init_instance::add_incoming(21,22, DATA_SOCKET);
  init_instance::add_outgoing(22,23, DATA_SOCKET);
}

void __init_sockets_22(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_22() {
}

void __peek_sockets_22() {
}

 
void init_Subtracter__193_106__22();
inline void check_status__22();

void work_Subtracter__193_106__22(int);


inline float __pop_21_22() {
float res=BUFFER_21_22[TAIL_21_22];
TAIL_21_22++;
return res;
}

inline void __pop_21_22(int n) {
TAIL_21_22+=n;
}

inline float __peek_21_22(int offs) {
return BUFFER_21_22[TAIL_21_22+offs];
}



inline void __push_22_23(float data) {
BUFFER_22_23[HEAD_22_23]=data;
HEAD_22_23++;
}



 
void init_Subtracter__193_106__22(){
}
void save_file_pointer__22(object_write_buffer *buf) {}
void load_file_pointer__22(object_write_buffer *buf) {}
 
void work_Subtracter__193_106__22(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__190 = 0.0f;/* float */
      float p0__191 = 0.0f;/* float */
      float v__192 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__190 = BUFFER_21_22[TAIL_21_22+1];
;
      p0__191 = BUFFER_21_22[TAIL_21_22+0];
;
      (v__192 = (p1__190 - p0__191))/*float*/;
      __push_22_23(v__192);
      __pop_21_22(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_23;
int __counter_23 = 0;
int __steady_23 = 0;
int __tmp_23 = 0;
int __tmp2_23 = 0;
int *__state_flag_23 = NULL;
thread_info *__thread_23 = NULL;



void save_peek_buffer__23(object_write_buffer *buf);
void load_peek_buffer__23(object_write_buffer *buf);
void save_file_pointer__23(object_write_buffer *buf);
void load_file_pointer__23(object_write_buffer *buf);

inline void check_status__23() {
  check_thread_status(__state_flag_23, __thread_23);
}

void check_status_during_io__23() {
  check_thread_status_during_io(__state_flag_23, __thread_23);
}

void __init_thread_info_23(thread_info *info) {
  __state_flag_23 = info->get_state_flag();
}

thread_info *__get_thread_info_23() {
  if (__thread_23 != NULL) return __thread_23;
  __thread_23 = new thread_info(23, check_status_during_io__23);
  __init_thread_info_23(__thread_23);
  return __thread_23;
}

void __declare_sockets_23() {
  init_instance::add_incoming(22,23, DATA_SOCKET);
  init_instance::add_outgoing(23,9, DATA_SOCKET);
}

void __init_sockets_23(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_23() {
}

void __peek_sockets_23() {
}

 
void init_Amplify__198_107__23();
inline void check_status__23();

void work_Amplify__198_107__23(int);


inline float __pop_22_23() {
float res=BUFFER_22_23[TAIL_22_23];
TAIL_22_23++;
return res;
}

inline void __pop_22_23(int n) {
TAIL_22_23+=n;
}

inline float __peek_22_23(int offs) {
return BUFFER_22_23[TAIL_22_23+offs];
}



inline void __push_23_9(float data) {
BUFFER_23_9[HEAD_23_9]=data;
HEAD_23_9++;
}



 
void init_Amplify__198_107__23(){
}
void save_file_pointer__23(object_write_buffer *buf) {}
void load_file_pointer__23(object_write_buffer *buf) {}
 
void work_Amplify__198_107__23(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__196 = 0.0f;/* float */
      float __tmp52__197 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__197 = BUFFER_22_23[TAIL_22_23]; TAIL_22_23++;
;
      (__tmp51__196 = (__tmp52__197 * ((float)1.5)))/*float*/;
      __push_23_9(__tmp51__196);
      // mark end: SIRFilter Amplify

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_24;
int __counter_24 = 0;
int __steady_24 = 0;
int __tmp_24 = 0;
int __tmp2_24 = 0;
int *__state_flag_24 = NULL;
thread_info *__thread_24 = NULL;



float coeff__167__24[64] = {0};
void save_peek_buffer__24(object_write_buffer *buf);
void load_peek_buffer__24(object_write_buffer *buf);
void save_file_pointer__24(object_write_buffer *buf);
void load_file_pointer__24(object_write_buffer *buf);

inline void check_status__24() {
  check_thread_status(__state_flag_24, __thread_24);
}

void check_status_during_io__24() {
  check_thread_status_during_io(__state_flag_24, __thread_24);
}

void __init_thread_info_24(thread_info *info) {
  __state_flag_24 = info->get_state_flag();
}

thread_info *__get_thread_info_24() {
  if (__thread_24 != NULL) return __thread_24;
  __thread_24 = new thread_info(24, check_status_during_io__24);
  __init_thread_info_24(__thread_24);
  return __thread_24;
}

void __declare_sockets_24() {
  init_instance::add_incoming(19,24, DATA_SOCKET);
  init_instance::add_outgoing(24,21, DATA_SOCKET);
}

void __init_sockets_24(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_24() {
}

void __peek_sockets_24() {
}

 
void init_LowPassFilter__187_105__24();
inline void check_status__24();

void work_LowPassFilter__187_105__24(int);


inline float __pop_19_24() {
float res=BUFFER_19_24[TAIL_19_24];
TAIL_19_24++;
return res;
}

inline void __pop_19_24(int n) {
TAIL_19_24+=n;
}

inline float __peek_19_24(int offs) {
return BUFFER_19_24[TAIL_19_24+offs];
}



inline void __push_24_21(float data) {
BUFFER_24_21[HEAD_24_21]=data;
HEAD_24_21++;
}



 
void init_LowPassFilter__187_105__24(){
  int i__175 = 0;/* int */
  float __tmp30__176 = 0.0f;/* float */
  float __tmp31__177 = 0.0f;/* float */
  float __tmp32__178 = 0.0f;/* float */
  float __tmp33__179 = 0.0f;/* float */
  double __tmp34__180 = 0.0f;/* double */
  double __tmp35__181 = 0.0f;/* double */
  float __tmp36__182 = 0.0f;/* float */
  float __tmp37__183 = 0.0f;/* float */
  float __tmp38__184 = 0.0f;/* float */
  double __tmp39__185 = 0.0f;/* double */
  double __tmp40__186 = 0.0f;/* double */

  for ((i__175 = 0)/*int*/; (i__175 < 64); (i__175++)) {if (((((float)(i__175)) - ((float)31.5)) == ((float)0.0))) {(((coeff__167__24)[(int)i__175]) = ((float)1.2445082E-6))/*float*/; } else {{
      (__tmp35__181 = ((double)((((float)3.909738E-6) * (((float)(i__175)) - ((float)31.5))))))/*double*/;
      (__tmp34__180 = sinf(__tmp35__181))/*double*/;
      (__tmp33__179 = ((float)(__tmp34__180)))/*float*/;
      (__tmp32__178 = (__tmp33__179 / ((float)3.1415927)))/*float*/;
      (__tmp31__177 = (__tmp32__178 / (((float)(i__175)) - ((float)31.5))))/*float*/;
      (__tmp40__186 = ((double)(((((float)6.2831855) * ((float)(i__175))) / ((float)63.0)))))/*double*/;
      (__tmp39__185 = cosf(__tmp40__186))/*double*/;
      (__tmp38__184 = ((float)(__tmp39__185)))/*float*/;
      (__tmp37__183 = (((float)0.46) * __tmp38__184))/*float*/;
      (__tmp36__182 = (((float)0.54) - __tmp37__183))/*float*/;
      (__tmp30__176 = (__tmp31__177 * __tmp36__182))/*float*/;
      (((coeff__167__24)[(int)i__175]) = __tmp30__176)/*float*/;
    }}
  }
}
void save_file_pointer__24(object_write_buffer *buf) {}
void load_file_pointer__24(object_write_buffer *buf) {}
 
void work_LowPassFilter__187_105__24(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__170 = 0.0f;/* float */
      float __tmp49__171 = 0.0f;/* float */
      float __tmp50__172 = 0.0f;/* float */
      int i__conflict__0__173 = 0;/* int */
      int i__174 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__170 = ((float)0.0))/*float*/;
      for ((i__conflict__0__173 = 0)/*int*/; (i__conflict__0__173 < 64); (i__conflict__0__173++)) {{
          __peek_19_24(i__conflict__0__173);
          __tmp50__172 = BUFFER_19_24[TAIL_19_24+i__conflict__0__173];
;
          (__tmp49__171 = (__tmp50__172 * ((coeff__167__24)[(int)i__conflict__0__173])))/*float*/;
          (sum__170 = (sum__170 + __tmp49__171))/*float*/;
        }
      }
      __push_24_21(sum__170);
      for ((i__174 = 0)/*int*/; (i__174 < 0); (i__174++)) {__pop_19_24();
      }
      __pop_19_24();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_25;
int __counter_25 = 0;
int __steady_25 = 0;
int __tmp_25 = 0;
int __tmp2_25 = 0;
int *__state_flag_25 = NULL;
thread_info *__thread_25 = NULL;



inline void check_status__25() {
  check_thread_status(__state_flag_25, __thread_25);
}

void check_status_during_io__25() {
  check_thread_status_during_io(__state_flag_25, __thread_25);
}

void __init_thread_info_25(thread_info *info) {
  __state_flag_25 = info->get_state_flag();
}

thread_info *__get_thread_info_25() {
  if (__thread_25 != NULL) return __thread_25;
  __thread_25 = new thread_info(25, check_status_during_io__25);
  __init_thread_info_25(__thread_25);
  return __thread_25;
}

void __declare_sockets_25() {
  init_instance::add_incoming(3,25, DATA_SOCKET);
  init_instance::add_outgoing(25,26, DATA_SOCKET);
  init_instance::add_outgoing(25,30, DATA_SOCKET);
}

void __init_sockets_25(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_25() {
}

void __peek_sockets_25() {
}

inline float __pop_3_25() {
float res=BUFFER_3_25[TAIL_3_25];
TAIL_3_25++;
return res;
}


inline void __push_25_26(float data) {
BUFFER_25_26[HEAD_25_26]=data;
HEAD_25_26++;
}



inline void __push_25_30(float data) {
BUFFER_25_30[HEAD_25_30]=data;
HEAD_25_30++;
}



void __splitter_25_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_25[TAIL_3_25]; TAIL_3_25++;
;
  __push_25_26(tmp);
  __push_25_30(tmp);
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_26;
int __counter_26 = 0;
int __steady_26 = 0;
int __tmp_26 = 0;
int __tmp2_26 = 0;
int *__state_flag_26 = NULL;
thread_info *__thread_26 = NULL;



float coeff__199__26[64] = {0};
void save_peek_buffer__26(object_write_buffer *buf);
void load_peek_buffer__26(object_write_buffer *buf);
void save_file_pointer__26(object_write_buffer *buf);
void load_file_pointer__26(object_write_buffer *buf);

inline void check_status__26() {
  check_thread_status(__state_flag_26, __thread_26);
}

void check_status_during_io__26() {
  check_thread_status_during_io(__state_flag_26, __thread_26);
}

void __init_thread_info_26(thread_info *info) {
  __state_flag_26 = info->get_state_flag();
}

thread_info *__get_thread_info_26() {
  if (__thread_26 != NULL) return __thread_26;
  __thread_26 = new thread_info(26, check_status_during_io__26);
  __init_thread_info_26(__thread_26);
  return __thread_26;
}

void __declare_sockets_26() {
  init_instance::add_incoming(25,26, DATA_SOCKET);
  init_instance::add_outgoing(26,27, DATA_SOCKET);
}

void __init_sockets_26(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_26() {
}

void __peek_sockets_26() {
}

 
void init_LowPassFilter__219_111__26();
inline void check_status__26();

void work_LowPassFilter__219_111__26(int);


inline float __pop_25_26() {
float res=BUFFER_25_26[TAIL_25_26];
TAIL_25_26++;
return res;
}

inline void __pop_25_26(int n) {
TAIL_25_26+=n;
}

inline float __peek_25_26(int offs) {
return BUFFER_25_26[TAIL_25_26+offs];
}



inline void __push_26_27(float data) {
BUFFER_26_27[HEAD_26_27]=data;
HEAD_26_27++;
}



 
void init_LowPassFilter__219_111__26(){
  int i__207 = 0;/* int */
  float __tmp19__208 = 0.0f;/* float */
  float __tmp20__209 = 0.0f;/* float */
  float __tmp21__210 = 0.0f;/* float */
  float __tmp22__211 = 0.0f;/* float */
  double __tmp23__212 = 0.0f;/* double */
  double __tmp24__213 = 0.0f;/* double */
  float __tmp25__214 = 0.0f;/* float */
  float __tmp26__215 = 0.0f;/* float */
  float __tmp27__216 = 0.0f;/* float */
  double __tmp28__217 = 0.0f;/* double */
  double __tmp29__218 = 0.0f;/* double */

  for ((i__207 = 0)/*int*/; (i__207 < 64); (i__207++)) {if (((((float)(i__207)) - ((float)31.5)) == ((float)0.0))) {(((coeff__199__26)[(int)i__207]) = ((float)1.2445082E-6))/*float*/; } else {{
      (__tmp24__213 = ((double)((((float)3.909738E-6) * (((float)(i__207)) - ((float)31.5))))))/*double*/;
      (__tmp23__212 = sinf(__tmp24__213))/*double*/;
      (__tmp22__211 = ((float)(__tmp23__212)))/*float*/;
      (__tmp21__210 = (__tmp22__211 / ((float)3.1415927)))/*float*/;
      (__tmp20__209 = (__tmp21__210 / (((float)(i__207)) - ((float)31.5))))/*float*/;
      (__tmp29__218 = ((double)(((((float)6.2831855) * ((float)(i__207))) / ((float)63.0)))))/*double*/;
      (__tmp28__217 = cosf(__tmp29__218))/*double*/;
      (__tmp27__216 = ((float)(__tmp28__217)))/*float*/;
      (__tmp26__215 = (((float)0.46) * __tmp27__216))/*float*/;
      (__tmp25__214 = (((float)0.54) - __tmp26__215))/*float*/;
      (__tmp19__208 = (__tmp20__209 * __tmp25__214))/*float*/;
      (((coeff__199__26)[(int)i__207]) = __tmp19__208)/*float*/;
    }}
  }
}
void save_file_pointer__26(object_write_buffer *buf) {}
void load_file_pointer__26(object_write_buffer *buf) {}
 
void work_LowPassFilter__219_111__26(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__202 = 0.0f;/* float */
      float __tmp47__203 = 0.0f;/* float */
      float __tmp48__204 = 0.0f;/* float */
      int i__conflict__0__205 = 0;/* int */
      int i__206 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__202 = ((float)0.0))/*float*/;
      for ((i__conflict__0__205 = 0)/*int*/; (i__conflict__0__205 < 64); (i__conflict__0__205++)) {{
          __peek_25_26(i__conflict__0__205);
          __tmp48__204 = BUFFER_25_26[TAIL_25_26+i__conflict__0__205];
;
          (__tmp47__203 = (__tmp48__204 * ((coeff__199__26)[(int)i__conflict__0__205])))/*float*/;
          (sum__202 = (sum__202 + __tmp47__203))/*float*/;
        }
      }
      __push_26_27(sum__202);
      for ((i__206 = 0)/*int*/; (i__206 < 0); (i__206++)) {__pop_25_26();
      }
      __pop_25_26();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_27;
int __counter_27 = 0;
int __steady_27 = 0;
int __tmp_27 = 0;
int __tmp2_27 = 0;
int *__state_flag_27 = NULL;
thread_info *__thread_27 = NULL;



inline void check_status__27() {
  check_thread_status(__state_flag_27, __thread_27);
}

void check_status_during_io__27() {
  check_thread_status_during_io(__state_flag_27, __thread_27);
}

void __init_thread_info_27(thread_info *info) {
  __state_flag_27 = info->get_state_flag();
}

thread_info *__get_thread_info_27() {
  if (__thread_27 != NULL) return __thread_27;
  __thread_27 = new thread_info(27, check_status_during_io__27);
  __init_thread_info_27(__thread_27);
  return __thread_27;
}

void __declare_sockets_27() {
  init_instance::add_incoming(26,27, DATA_SOCKET);
  init_instance::add_incoming(30,27, DATA_SOCKET);
  init_instance::add_outgoing(27,28, DATA_SOCKET);
}

void __init_sockets_27(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_27() {
}

void __peek_sockets_27() {
}


inline void __push_27_28(float data) {
BUFFER_27_28[HEAD_27_28]=data;
HEAD_27_28++;
}


inline float __pop_26_27() {
float res=BUFFER_26_27[TAIL_26_27];
TAIL_26_27++;
return res;
}

inline float __pop_30_27() {
float res=BUFFER_30_27[TAIL_30_27];
TAIL_30_27++;
return res;
}


void __joiner_27_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_27_28(__pop_26_27());
    __push_27_28(__pop_30_27());
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_28;
int __counter_28 = 0;
int __steady_28 = 0;
int __tmp_28 = 0;
int __tmp2_28 = 0;
int *__state_flag_28 = NULL;
thread_info *__thread_28 = NULL;



void save_peek_buffer__28(object_write_buffer *buf);
void load_peek_buffer__28(object_write_buffer *buf);
void save_file_pointer__28(object_write_buffer *buf);
void load_file_pointer__28(object_write_buffer *buf);

inline void check_status__28() {
  check_thread_status(__state_flag_28, __thread_28);
}

void check_status_during_io__28() {
  check_thread_status_during_io(__state_flag_28, __thread_28);
}

void __init_thread_info_28(thread_info *info) {
  __state_flag_28 = info->get_state_flag();
}

thread_info *__get_thread_info_28() {
  if (__thread_28 != NULL) return __thread_28;
  __thread_28 = new thread_info(28, check_status_during_io__28);
  __init_thread_info_28(__thread_28);
  return __thread_28;
}

void __declare_sockets_28() {
  init_instance::add_incoming(27,28, DATA_SOCKET);
  init_instance::add_outgoing(28,29, DATA_SOCKET);
}

void __init_sockets_28(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_28() {
}

void __peek_sockets_28() {
}

 
void init_Subtracter__246_113__28();
inline void check_status__28();

void work_Subtracter__246_113__28(int);


inline float __pop_27_28() {
float res=BUFFER_27_28[TAIL_27_28];
TAIL_27_28++;
return res;
}

inline void __pop_27_28(int n) {
TAIL_27_28+=n;
}

inline float __peek_27_28(int offs) {
return BUFFER_27_28[TAIL_27_28+offs];
}



inline void __push_28_29(float data) {
BUFFER_28_29[HEAD_28_29]=data;
HEAD_28_29++;
}



 
void init_Subtracter__246_113__28(){
}
void save_file_pointer__28(object_write_buffer *buf) {}
void load_file_pointer__28(object_write_buffer *buf) {}
 
void work_Subtracter__246_113__28(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__243 = 0.0f;/* float */
      float p0__244 = 0.0f;/* float */
      float v__245 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__243 = BUFFER_27_28[TAIL_27_28+1];
;
      p0__244 = BUFFER_27_28[TAIL_27_28+0];
;
      (v__245 = (p1__243 - p0__244))/*float*/;
      __push_28_29(v__245);
      __pop_27_28(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_29;
int __counter_29 = 0;
int __steady_29 = 0;
int __tmp_29 = 0;
int __tmp2_29 = 0;
int *__state_flag_29 = NULL;
thread_info *__thread_29 = NULL;



void save_peek_buffer__29(object_write_buffer *buf);
void load_peek_buffer__29(object_write_buffer *buf);
void save_file_pointer__29(object_write_buffer *buf);
void load_file_pointer__29(object_write_buffer *buf);

inline void check_status__29() {
  check_thread_status(__state_flag_29, __thread_29);
}

void check_status_during_io__29() {
  check_thread_status_during_io(__state_flag_29, __thread_29);
}

void __init_thread_info_29(thread_info *info) {
  __state_flag_29 = info->get_state_flag();
}

thread_info *__get_thread_info_29() {
  if (__thread_29 != NULL) return __thread_29;
  __thread_29 = new thread_info(29, check_status_during_io__29);
  __init_thread_info_29(__thread_29);
  return __thread_29;
}

void __declare_sockets_29() {
  init_instance::add_incoming(28,29, DATA_SOCKET);
  init_instance::add_outgoing(29,9, DATA_SOCKET);
}

void __init_sockets_29(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_29() {
}

void __peek_sockets_29() {
}

 
void init_Amplify__251_114__29();
inline void check_status__29();

void work_Amplify__251_114__29(int);


inline float __pop_28_29() {
float res=BUFFER_28_29[TAIL_28_29];
TAIL_28_29++;
return res;
}

inline void __pop_28_29(int n) {
TAIL_28_29+=n;
}

inline float __peek_28_29(int offs) {
return BUFFER_28_29[TAIL_28_29+offs];
}



inline void __push_29_9(float data) {
BUFFER_29_9[HEAD_29_9]=data;
HEAD_29_9++;
}



 
void init_Amplify__251_114__29(){
}
void save_file_pointer__29(object_write_buffer *buf) {}
void load_file_pointer__29(object_write_buffer *buf) {}
 
void work_Amplify__251_114__29(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__249 = 0.0f;/* float */
      float __tmp52__250 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__250 = BUFFER_28_29[TAIL_28_29]; TAIL_28_29++;
;
      (__tmp51__249 = (__tmp52__250 * ((float)1.7)))/*float*/;
      __push_29_9(__tmp51__249);
      // mark end: SIRFilter Amplify

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_3;
int __counter_3 = 0;
int __steady_3 = 0;
int __tmp_3 = 0;
int __tmp2_3 = 0;
int *__state_flag_3 = NULL;
thread_info *__thread_3 = NULL;



inline void check_status__3() {
  check_thread_status(__state_flag_3, __thread_3);
}

void check_status_during_io__3() {
  check_thread_status_during_io(__state_flag_3, __thread_3);
}

void __init_thread_info_3(thread_info *info) {
  __state_flag_3 = info->get_state_flag();
}

thread_info *__get_thread_info_3() {
  if (__thread_3 != NULL) return __thread_3;
  __thread_3 = new thread_info(3, check_status_during_io__3);
  __init_thread_info_3(__thread_3);
  return __thread_3;
}

void __declare_sockets_3() {
  init_instance::add_incoming(2,3, DATA_SOCKET);
  init_instance::add_outgoing(3,4, DATA_SOCKET);
  init_instance::add_outgoing(3,13, DATA_SOCKET);
  init_instance::add_outgoing(3,19, DATA_SOCKET);
  init_instance::add_outgoing(3,25, DATA_SOCKET);
  init_instance::add_outgoing(3,31, DATA_SOCKET);
  init_instance::add_outgoing(3,37, DATA_SOCKET);
  init_instance::add_outgoing(3,43, DATA_SOCKET);
  init_instance::add_outgoing(3,49, DATA_SOCKET);
  init_instance::add_outgoing(3,55, DATA_SOCKET);
  init_instance::add_outgoing(3,61, DATA_SOCKET);
}

void __init_sockets_3(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_3() {
}

void __peek_sockets_3() {
}

inline float __pop_2_3() {
float res=BUFFER_2_3[TAIL_2_3];
TAIL_2_3++;
return res;
}


inline void __push_3_4(float data) {
BUFFER_3_4[HEAD_3_4]=data;
HEAD_3_4++;
}



inline void __push_3_13(float data) {
BUFFER_3_13[HEAD_3_13]=data;
HEAD_3_13++;
}



inline void __push_3_19(float data) {
BUFFER_3_19[HEAD_3_19]=data;
HEAD_3_19++;
}



inline void __push_3_25(float data) {
BUFFER_3_25[HEAD_3_25]=data;
HEAD_3_25++;
}



inline void __push_3_31(float data) {
BUFFER_3_31[HEAD_3_31]=data;
HEAD_3_31++;
}



inline void __push_3_37(float data) {
BUFFER_3_37[HEAD_3_37]=data;
HEAD_3_37++;
}



inline void __push_3_43(float data) {
BUFFER_3_43[HEAD_3_43]=data;
HEAD_3_43++;
}



inline void __push_3_49(float data) {
BUFFER_3_49[HEAD_3_49]=data;
HEAD_3_49++;
}



inline void __push_3_55(float data) {
BUFFER_3_55[HEAD_3_55]=data;
HEAD_3_55++;
}



inline void __push_3_61(float data) {
BUFFER_3_61[HEAD_3_61]=data;
HEAD_3_61++;
}



void __splitter_3_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_2_3[TAIL_2_3]; TAIL_2_3++;
;
  __push_3_4(tmp);
  __push_3_13(tmp);
  __push_3_19(tmp);
  __push_3_25(tmp);
  __push_3_31(tmp);
  __push_3_37(tmp);
  __push_3_43(tmp);
  __push_3_49(tmp);
  __push_3_55(tmp);
  __push_3_61(tmp);
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_30;
int __counter_30 = 0;
int __steady_30 = 0;
int __tmp_30 = 0;
int __tmp2_30 = 0;
int *__state_flag_30 = NULL;
thread_info *__thread_30 = NULL;



float coeff__220__30[64] = {0};
void save_peek_buffer__30(object_write_buffer *buf);
void load_peek_buffer__30(object_write_buffer *buf);
void save_file_pointer__30(object_write_buffer *buf);
void load_file_pointer__30(object_write_buffer *buf);

inline void check_status__30() {
  check_thread_status(__state_flag_30, __thread_30);
}

void check_status_during_io__30() {
  check_thread_status_during_io(__state_flag_30, __thread_30);
}

void __init_thread_info_30(thread_info *info) {
  __state_flag_30 = info->get_state_flag();
}

thread_info *__get_thread_info_30() {
  if (__thread_30 != NULL) return __thread_30;
  __thread_30 = new thread_info(30, check_status_during_io__30);
  __init_thread_info_30(__thread_30);
  return __thread_30;
}

void __declare_sockets_30() {
  init_instance::add_incoming(25,30, DATA_SOCKET);
  init_instance::add_outgoing(30,27, DATA_SOCKET);
}

void __init_sockets_30(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_30() {
}

void __peek_sockets_30() {
}

 
void init_LowPassFilter__240_112__30();
inline void check_status__30();

void work_LowPassFilter__240_112__30(int);


inline float __pop_25_30() {
float res=BUFFER_25_30[TAIL_25_30];
TAIL_25_30++;
return res;
}

inline void __pop_25_30(int n) {
TAIL_25_30+=n;
}

inline float __peek_25_30(int offs) {
return BUFFER_25_30[TAIL_25_30+offs];
}



inline void __push_30_27(float data) {
BUFFER_30_27[HEAD_30_27]=data;
HEAD_30_27++;
}



 
void init_LowPassFilter__240_112__30(){
  int i__228 = 0;/* int */
  float __tmp30__229 = 0.0f;/* float */
  float __tmp31__230 = 0.0f;/* float */
  float __tmp32__231 = 0.0f;/* float */
  float __tmp33__232 = 0.0f;/* float */
  double __tmp34__233 = 0.0f;/* double */
  double __tmp35__234 = 0.0f;/* double */
  float __tmp36__235 = 0.0f;/* float */
  float __tmp37__236 = 0.0f;/* float */
  float __tmp38__237 = 0.0f;/* float */
  double __tmp39__238 = 0.0f;/* double */
  double __tmp40__239 = 0.0f;/* double */

  for ((i__228 = 0)/*int*/; (i__228 < 64); (i__228++)) {if (((((float)(i__228)) - ((float)31.5)) == ((float)0.0))) {(((coeff__220__30)[(int)i__228]) = ((float)1.76E-6))/*float*/; } else {{
      (__tmp35__234 = ((double)((((float)5.5292035E-6) * (((float)(i__228)) - ((float)31.5))))))/*double*/;
      (__tmp34__233 = sinf(__tmp35__234))/*double*/;
      (__tmp33__232 = ((float)(__tmp34__233)))/*float*/;
      (__tmp32__231 = (__tmp33__232 / ((float)3.1415927)))/*float*/;
      (__tmp31__230 = (__tmp32__231 / (((float)(i__228)) - ((float)31.5))))/*float*/;
      (__tmp40__239 = ((double)(((((float)6.2831855) * ((float)(i__228))) / ((float)63.0)))))/*double*/;
      (__tmp39__238 = cosf(__tmp40__239))/*double*/;
      (__tmp38__237 = ((float)(__tmp39__238)))/*float*/;
      (__tmp37__236 = (((float)0.46) * __tmp38__237))/*float*/;
      (__tmp36__235 = (((float)0.54) - __tmp37__236))/*float*/;
      (__tmp30__229 = (__tmp31__230 * __tmp36__235))/*float*/;
      (((coeff__220__30)[(int)i__228]) = __tmp30__229)/*float*/;
    }}
  }
}
void save_file_pointer__30(object_write_buffer *buf) {}
void load_file_pointer__30(object_write_buffer *buf) {}
 
void work_LowPassFilter__240_112__30(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__223 = 0.0f;/* float */
      float __tmp49__224 = 0.0f;/* float */
      float __tmp50__225 = 0.0f;/* float */
      int i__conflict__0__226 = 0;/* int */
      int i__227 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__223 = ((float)0.0))/*float*/;
      for ((i__conflict__0__226 = 0)/*int*/; (i__conflict__0__226 < 64); (i__conflict__0__226++)) {{
          __peek_25_30(i__conflict__0__226);
          __tmp50__225 = BUFFER_25_30[TAIL_25_30+i__conflict__0__226];
;
          (__tmp49__224 = (__tmp50__225 * ((coeff__220__30)[(int)i__conflict__0__226])))/*float*/;
          (sum__223 = (sum__223 + __tmp49__224))/*float*/;
        }
      }
      __push_30_27(sum__223);
      for ((i__227 = 0)/*int*/; (i__227 < 0); (i__227++)) {__pop_25_30();
      }
      __pop_25_30();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_31;
int __counter_31 = 0;
int __steady_31 = 0;
int __tmp_31 = 0;
int __tmp2_31 = 0;
int *__state_flag_31 = NULL;
thread_info *__thread_31 = NULL;



inline void check_status__31() {
  check_thread_status(__state_flag_31, __thread_31);
}

void check_status_during_io__31() {
  check_thread_status_during_io(__state_flag_31, __thread_31);
}

void __init_thread_info_31(thread_info *info) {
  __state_flag_31 = info->get_state_flag();
}

thread_info *__get_thread_info_31() {
  if (__thread_31 != NULL) return __thread_31;
  __thread_31 = new thread_info(31, check_status_during_io__31);
  __init_thread_info_31(__thread_31);
  return __thread_31;
}

void __declare_sockets_31() {
  init_instance::add_incoming(3,31, DATA_SOCKET);
  init_instance::add_outgoing(31,32, DATA_SOCKET);
  init_instance::add_outgoing(31,36, DATA_SOCKET);
}

void __init_sockets_31(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_31() {
}

void __peek_sockets_31() {
}

inline float __pop_3_31() {
float res=BUFFER_3_31[TAIL_3_31];
TAIL_3_31++;
return res;
}


inline void __push_31_32(float data) {
BUFFER_31_32[HEAD_31_32]=data;
HEAD_31_32++;
}



inline void __push_31_36(float data) {
BUFFER_31_36[HEAD_31_36]=data;
HEAD_31_36++;
}



void __splitter_31_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_31[TAIL_3_31]; TAIL_3_31++;
;
  __push_31_32(tmp);
  __push_31_36(tmp);
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_32;
int __counter_32 = 0;
int __steady_32 = 0;
int __tmp_32 = 0;
int __tmp2_32 = 0;
int *__state_flag_32 = NULL;
thread_info *__thread_32 = NULL;



float coeff__252__32[64] = {0};
void save_peek_buffer__32(object_write_buffer *buf);
void load_peek_buffer__32(object_write_buffer *buf);
void save_file_pointer__32(object_write_buffer *buf);
void load_file_pointer__32(object_write_buffer *buf);

inline void check_status__32() {
  check_thread_status(__state_flag_32, __thread_32);
}

void check_status_during_io__32() {
  check_thread_status_during_io(__state_flag_32, __thread_32);
}

void __init_thread_info_32(thread_info *info) {
  __state_flag_32 = info->get_state_flag();
}

thread_info *__get_thread_info_32() {
  if (__thread_32 != NULL) return __thread_32;
  __thread_32 = new thread_info(32, check_status_during_io__32);
  __init_thread_info_32(__thread_32);
  return __thread_32;
}

void __declare_sockets_32() {
  init_instance::add_incoming(31,32, DATA_SOCKET);
  init_instance::add_outgoing(32,33, DATA_SOCKET);
}

void __init_sockets_32(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_32() {
}

void __peek_sockets_32() {
}

 
void init_LowPassFilter__272_118__32();
inline void check_status__32();

void work_LowPassFilter__272_118__32(int);


inline float __pop_31_32() {
float res=BUFFER_31_32[TAIL_31_32];
TAIL_31_32++;
return res;
}

inline void __pop_31_32(int n) {
TAIL_31_32+=n;
}

inline float __peek_31_32(int offs) {
return BUFFER_31_32[TAIL_31_32+offs];
}



inline void __push_32_33(float data) {
BUFFER_32_33[HEAD_32_33]=data;
HEAD_32_33++;
}



 
void init_LowPassFilter__272_118__32(){
  int i__260 = 0;/* int */
  float __tmp19__261 = 0.0f;/* float */
  float __tmp20__262 = 0.0f;/* float */
  float __tmp21__263 = 0.0f;/* float */
  float __tmp22__264 = 0.0f;/* float */
  double __tmp23__265 = 0.0f;/* double */
  double __tmp24__266 = 0.0f;/* double */
  float __tmp25__267 = 0.0f;/* float */
  float __tmp26__268 = 0.0f;/* float */
  float __tmp27__269 = 0.0f;/* float */
  double __tmp28__270 = 0.0f;/* double */
  double __tmp29__271 = 0.0f;/* double */

  for ((i__260 = 0)/*int*/; (i__260 < 64); (i__260++)) {if (((((float)(i__260)) - ((float)31.5)) == ((float)0.0))) {(((coeff__252__32)[(int)i__260]) = ((float)1.76E-6))/*float*/; } else {{
      (__tmp24__266 = ((double)((((float)5.5292035E-6) * (((float)(i__260)) - ((float)31.5))))))/*double*/;
      (__tmp23__265 = sinf(__tmp24__266))/*double*/;
      (__tmp22__264 = ((float)(__tmp23__265)))/*float*/;
      (__tmp21__263 = (__tmp22__264 / ((float)3.1415927)))/*float*/;
      (__tmp20__262 = (__tmp21__263 / (((float)(i__260)) - ((float)31.5))))/*float*/;
      (__tmp29__271 = ((double)(((((float)6.2831855) * ((float)(i__260))) / ((float)63.0)))))/*double*/;
      (__tmp28__270 = cosf(__tmp29__271))/*double*/;
      (__tmp27__269 = ((float)(__tmp28__270)))/*float*/;
      (__tmp26__268 = (((float)0.46) * __tmp27__269))/*float*/;
      (__tmp25__267 = (((float)0.54) - __tmp26__268))/*float*/;
      (__tmp19__261 = (__tmp20__262 * __tmp25__267))/*float*/;
      (((coeff__252__32)[(int)i__260]) = __tmp19__261)/*float*/;
    }}
  }
}
void save_file_pointer__32(object_write_buffer *buf) {}
void load_file_pointer__32(object_write_buffer *buf) {}
 
void work_LowPassFilter__272_118__32(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__255 = 0.0f;/* float */
      float __tmp47__256 = 0.0f;/* float */
      float __tmp48__257 = 0.0f;/* float */
      int i__conflict__0__258 = 0;/* int */
      int i__259 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__255 = ((float)0.0))/*float*/;
      for ((i__conflict__0__258 = 0)/*int*/; (i__conflict__0__258 < 64); (i__conflict__0__258++)) {{
          __peek_31_32(i__conflict__0__258);
          __tmp48__257 = BUFFER_31_32[TAIL_31_32+i__conflict__0__258];
;
          (__tmp47__256 = (__tmp48__257 * ((coeff__252__32)[(int)i__conflict__0__258])))/*float*/;
          (sum__255 = (sum__255 + __tmp47__256))/*float*/;
        }
      }
      __push_32_33(sum__255);
      for ((i__259 = 0)/*int*/; (i__259 < 0); (i__259++)) {__pop_31_32();
      }
      __pop_31_32();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_33;
int __counter_33 = 0;
int __steady_33 = 0;
int __tmp_33 = 0;
int __tmp2_33 = 0;
int *__state_flag_33 = NULL;
thread_info *__thread_33 = NULL;



inline void check_status__33() {
  check_thread_status(__state_flag_33, __thread_33);
}

void check_status_during_io__33() {
  check_thread_status_during_io(__state_flag_33, __thread_33);
}

void __init_thread_info_33(thread_info *info) {
  __state_flag_33 = info->get_state_flag();
}

thread_info *__get_thread_info_33() {
  if (__thread_33 != NULL) return __thread_33;
  __thread_33 = new thread_info(33, check_status_during_io__33);
  __init_thread_info_33(__thread_33);
  return __thread_33;
}

void __declare_sockets_33() {
  init_instance::add_incoming(32,33, DATA_SOCKET);
  init_instance::add_incoming(36,33, DATA_SOCKET);
  init_instance::add_outgoing(33,34, DATA_SOCKET);
}

void __init_sockets_33(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_33() {
}

void __peek_sockets_33() {
}


inline void __push_33_34(float data) {
BUFFER_33_34[HEAD_33_34]=data;
HEAD_33_34++;
}


inline float __pop_32_33() {
float res=BUFFER_32_33[TAIL_32_33];
TAIL_32_33++;
return res;
}

inline float __pop_36_33() {
float res=BUFFER_36_33[TAIL_36_33];
TAIL_36_33++;
return res;
}


void __joiner_33_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_33_34(__pop_32_33());
    __push_33_34(__pop_36_33());
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_34;
int __counter_34 = 0;
int __steady_34 = 0;
int __tmp_34 = 0;
int __tmp2_34 = 0;
int *__state_flag_34 = NULL;
thread_info *__thread_34 = NULL;



void save_peek_buffer__34(object_write_buffer *buf);
void load_peek_buffer__34(object_write_buffer *buf);
void save_file_pointer__34(object_write_buffer *buf);
void load_file_pointer__34(object_write_buffer *buf);

inline void check_status__34() {
  check_thread_status(__state_flag_34, __thread_34);
}

void check_status_during_io__34() {
  check_thread_status_during_io(__state_flag_34, __thread_34);
}

void __init_thread_info_34(thread_info *info) {
  __state_flag_34 = info->get_state_flag();
}

thread_info *__get_thread_info_34() {
  if (__thread_34 != NULL) return __thread_34;
  __thread_34 = new thread_info(34, check_status_during_io__34);
  __init_thread_info_34(__thread_34);
  return __thread_34;
}

void __declare_sockets_34() {
  init_instance::add_incoming(33,34, DATA_SOCKET);
  init_instance::add_outgoing(34,35, DATA_SOCKET);
}

void __init_sockets_34(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_34() {
}

void __peek_sockets_34() {
}

 
void init_Subtracter__299_120__34();
inline void check_status__34();

void work_Subtracter__299_120__34(int);


inline float __pop_33_34() {
float res=BUFFER_33_34[TAIL_33_34];
TAIL_33_34++;
return res;
}

inline void __pop_33_34(int n) {
TAIL_33_34+=n;
}

inline float __peek_33_34(int offs) {
return BUFFER_33_34[TAIL_33_34+offs];
}



inline void __push_34_35(float data) {
BUFFER_34_35[HEAD_34_35]=data;
HEAD_34_35++;
}



 
void init_Subtracter__299_120__34(){
}
void save_file_pointer__34(object_write_buffer *buf) {}
void load_file_pointer__34(object_write_buffer *buf) {}
 
void work_Subtracter__299_120__34(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__296 = 0.0f;/* float */
      float p0__297 = 0.0f;/* float */
      float v__298 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__296 = BUFFER_33_34[TAIL_33_34+1];
;
      p0__297 = BUFFER_33_34[TAIL_33_34+0];
;
      (v__298 = (p1__296 - p0__297))/*float*/;
      __push_34_35(v__298);
      __pop_33_34(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_35;
int __counter_35 = 0;
int __steady_35 = 0;
int __tmp_35 = 0;
int __tmp2_35 = 0;
int *__state_flag_35 = NULL;
thread_info *__thread_35 = NULL;



void save_peek_buffer__35(object_write_buffer *buf);
void load_peek_buffer__35(object_write_buffer *buf);
void save_file_pointer__35(object_write_buffer *buf);
void load_file_pointer__35(object_write_buffer *buf);

inline void check_status__35() {
  check_thread_status(__state_flag_35, __thread_35);
}

void check_status_during_io__35() {
  check_thread_status_during_io(__state_flag_35, __thread_35);
}

void __init_thread_info_35(thread_info *info) {
  __state_flag_35 = info->get_state_flag();
}

thread_info *__get_thread_info_35() {
  if (__thread_35 != NULL) return __thread_35;
  __thread_35 = new thread_info(35, check_status_during_io__35);
  __init_thread_info_35(__thread_35);
  return __thread_35;
}

void __declare_sockets_35() {
  init_instance::add_incoming(34,35, DATA_SOCKET);
  init_instance::add_outgoing(35,9, DATA_SOCKET);
}

void __init_sockets_35(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_35() {
}

void __peek_sockets_35() {
}

 
void init_Amplify__304_121__35();
inline void check_status__35();

void work_Amplify__304_121__35(int);


inline float __pop_34_35() {
float res=BUFFER_34_35[TAIL_34_35];
TAIL_34_35++;
return res;
}

inline void __pop_34_35(int n) {
TAIL_34_35+=n;
}

inline float __peek_34_35(int offs) {
return BUFFER_34_35[TAIL_34_35+offs];
}



inline void __push_35_9(float data) {
BUFFER_35_9[HEAD_35_9]=data;
HEAD_35_9++;
}



 
void init_Amplify__304_121__35(){
}
void save_file_pointer__35(object_write_buffer *buf) {}
void load_file_pointer__35(object_write_buffer *buf) {}
 
void work_Amplify__304_121__35(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__302 = 0.0f;/* float */
      float __tmp52__303 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__303 = BUFFER_34_35[TAIL_34_35]; TAIL_34_35++;
;
      (__tmp51__302 = (__tmp52__303 * ((float)1.9)))/*float*/;
      __push_35_9(__tmp51__302);
      // mark end: SIRFilter Amplify

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_36;
int __counter_36 = 0;
int __steady_36 = 0;
int __tmp_36 = 0;
int __tmp2_36 = 0;
int *__state_flag_36 = NULL;
thread_info *__thread_36 = NULL;



float coeff__273__36[64] = {0};
void save_peek_buffer__36(object_write_buffer *buf);
void load_peek_buffer__36(object_write_buffer *buf);
void save_file_pointer__36(object_write_buffer *buf);
void load_file_pointer__36(object_write_buffer *buf);

inline void check_status__36() {
  check_thread_status(__state_flag_36, __thread_36);
}

void check_status_during_io__36() {
  check_thread_status_during_io(__state_flag_36, __thread_36);
}

void __init_thread_info_36(thread_info *info) {
  __state_flag_36 = info->get_state_flag();
}

thread_info *__get_thread_info_36() {
  if (__thread_36 != NULL) return __thread_36;
  __thread_36 = new thread_info(36, check_status_during_io__36);
  __init_thread_info_36(__thread_36);
  return __thread_36;
}

void __declare_sockets_36() {
  init_instance::add_incoming(31,36, DATA_SOCKET);
  init_instance::add_outgoing(36,33, DATA_SOCKET);
}

void __init_sockets_36(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_36() {
}

void __peek_sockets_36() {
}

 
void init_LowPassFilter__293_119__36();
inline void check_status__36();

void work_LowPassFilter__293_119__36(int);


inline float __pop_31_36() {
float res=BUFFER_31_36[TAIL_31_36];
TAIL_31_36++;
return res;
}

inline void __pop_31_36(int n) {
TAIL_31_36+=n;
}

inline float __peek_31_36(int offs) {
return BUFFER_31_36[TAIL_31_36+offs];
}



inline void __push_36_33(float data) {
BUFFER_36_33[HEAD_36_33]=data;
HEAD_36_33++;
}



 
void init_LowPassFilter__293_119__36(){
  int i__281 = 0;/* int */
  float __tmp30__282 = 0.0f;/* float */
  float __tmp31__283 = 0.0f;/* float */
  float __tmp32__284 = 0.0f;/* float */
  float __tmp33__285 = 0.0f;/* float */
  double __tmp34__286 = 0.0f;/* double */
  double __tmp35__287 = 0.0f;/* double */
  float __tmp36__288 = 0.0f;/* float */
  float __tmp37__289 = 0.0f;/* float */
  float __tmp38__290 = 0.0f;/* float */
  double __tmp39__291 = 0.0f;/* double */
  double __tmp40__292 = 0.0f;/* double */

  for ((i__281 = 0)/*int*/; (i__281 < 64); (i__281++)) {if (((((float)(i__281)) - ((float)31.5)) == ((float)0.0))) {(((coeff__273__36)[(int)i__281]) = ((float)2.4890157E-6))/*float*/; } else {{
      (__tmp35__287 = ((double)((((float)7.8194735E-6) * (((float)(i__281)) - ((float)31.5))))))/*double*/;
      (__tmp34__286 = sinf(__tmp35__287))/*double*/;
      (__tmp33__285 = ((float)(__tmp34__286)))/*float*/;
      (__tmp32__284 = (__tmp33__285 / ((float)3.1415927)))/*float*/;
      (__tmp31__283 = (__tmp32__284 / (((float)(i__281)) - ((float)31.5))))/*float*/;
      (__tmp40__292 = ((double)(((((float)6.2831855) * ((float)(i__281))) / ((float)63.0)))))/*double*/;
      (__tmp39__291 = cosf(__tmp40__292))/*double*/;
      (__tmp38__290 = ((float)(__tmp39__291)))/*float*/;
      (__tmp37__289 = (((float)0.46) * __tmp38__290))/*float*/;
      (__tmp36__288 = (((float)0.54) - __tmp37__289))/*float*/;
      (__tmp30__282 = (__tmp31__283 * __tmp36__288))/*float*/;
      (((coeff__273__36)[(int)i__281]) = __tmp30__282)/*float*/;
    }}
  }
}
void save_file_pointer__36(object_write_buffer *buf) {}
void load_file_pointer__36(object_write_buffer *buf) {}
 
void work_LowPassFilter__293_119__36(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__276 = 0.0f;/* float */
      float __tmp49__277 = 0.0f;/* float */
      float __tmp50__278 = 0.0f;/* float */
      int i__conflict__0__279 = 0;/* int */
      int i__280 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__276 = ((float)0.0))/*float*/;
      for ((i__conflict__0__279 = 0)/*int*/; (i__conflict__0__279 < 64); (i__conflict__0__279++)) {{
          __peek_31_36(i__conflict__0__279);
          __tmp50__278 = BUFFER_31_36[TAIL_31_36+i__conflict__0__279];
;
          (__tmp49__277 = (__tmp50__278 * ((coeff__273__36)[(int)i__conflict__0__279])))/*float*/;
          (sum__276 = (sum__276 + __tmp49__277))/*float*/;
        }
      }
      __push_36_33(sum__276);
      for ((i__280 = 0)/*int*/; (i__280 < 0); (i__280++)) {__pop_31_36();
      }
      __pop_31_36();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_37;
int __counter_37 = 0;
int __steady_37 = 0;
int __tmp_37 = 0;
int __tmp2_37 = 0;
int *__state_flag_37 = NULL;
thread_info *__thread_37 = NULL;



inline void check_status__37() {
  check_thread_status(__state_flag_37, __thread_37);
}

void check_status_during_io__37() {
  check_thread_status_during_io(__state_flag_37, __thread_37);
}

void __init_thread_info_37(thread_info *info) {
  __state_flag_37 = info->get_state_flag();
}

thread_info *__get_thread_info_37() {
  if (__thread_37 != NULL) return __thread_37;
  __thread_37 = new thread_info(37, check_status_during_io__37);
  __init_thread_info_37(__thread_37);
  return __thread_37;
}

void __declare_sockets_37() {
  init_instance::add_incoming(3,37, DATA_SOCKET);
  init_instance::add_outgoing(37,38, DATA_SOCKET);
  init_instance::add_outgoing(37,42, DATA_SOCKET);
}

void __init_sockets_37(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_37() {
}

void __peek_sockets_37() {
}

inline float __pop_3_37() {
float res=BUFFER_3_37[TAIL_3_37];
TAIL_3_37++;
return res;
}


inline void __push_37_38(float data) {
BUFFER_37_38[HEAD_37_38]=data;
HEAD_37_38++;
}



inline void __push_37_42(float data) {
BUFFER_37_42[HEAD_37_42]=data;
HEAD_37_42++;
}



void __splitter_37_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_37[TAIL_3_37]; TAIL_3_37++;
;
  __push_37_38(tmp);
  __push_37_42(tmp);
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_38;
int __counter_38 = 0;
int __steady_38 = 0;
int __tmp_38 = 0;
int __tmp2_38 = 0;
int *__state_flag_38 = NULL;
thread_info *__thread_38 = NULL;



float coeff__305__38[64] = {0};
void save_peek_buffer__38(object_write_buffer *buf);
void load_peek_buffer__38(object_write_buffer *buf);
void save_file_pointer__38(object_write_buffer *buf);
void load_file_pointer__38(object_write_buffer *buf);

inline void check_status__38() {
  check_thread_status(__state_flag_38, __thread_38);
}

void check_status_during_io__38() {
  check_thread_status_during_io(__state_flag_38, __thread_38);
}

void __init_thread_info_38(thread_info *info) {
  __state_flag_38 = info->get_state_flag();
}

thread_info *__get_thread_info_38() {
  if (__thread_38 != NULL) return __thread_38;
  __thread_38 = new thread_info(38, check_status_during_io__38);
  __init_thread_info_38(__thread_38);
  return __thread_38;
}

void __declare_sockets_38() {
  init_instance::add_incoming(37,38, DATA_SOCKET);
  init_instance::add_outgoing(38,39, DATA_SOCKET);
}

void __init_sockets_38(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_38() {
}

void __peek_sockets_38() {
}

 
void init_LowPassFilter__325_125__38();
inline void check_status__38();

void work_LowPassFilter__325_125__38(int);


inline float __pop_37_38() {
float res=BUFFER_37_38[TAIL_37_38];
TAIL_37_38++;
return res;
}

inline void __pop_37_38(int n) {
TAIL_37_38+=n;
}

inline float __peek_37_38(int offs) {
return BUFFER_37_38[TAIL_37_38+offs];
}



inline void __push_38_39(float data) {
BUFFER_38_39[HEAD_38_39]=data;
HEAD_38_39++;
}



 
void init_LowPassFilter__325_125__38(){
  int i__313 = 0;/* int */
  float __tmp19__314 = 0.0f;/* float */
  float __tmp20__315 = 0.0f;/* float */
  float __tmp21__316 = 0.0f;/* float */
  float __tmp22__317 = 0.0f;/* float */
  double __tmp23__318 = 0.0f;/* double */
  double __tmp24__319 = 0.0f;/* double */
  float __tmp25__320 = 0.0f;/* float */
  float __tmp26__321 = 0.0f;/* float */
  float __tmp27__322 = 0.0f;/* float */
  double __tmp28__323 = 0.0f;/* double */
  double __tmp29__324 = 0.0f;/* double */

  for ((i__313 = 0)/*int*/; (i__313 < 64); (i__313++)) {if (((((float)(i__313)) - ((float)31.5)) == ((float)0.0))) {(((coeff__305__38)[(int)i__313]) = ((float)2.4890157E-6))/*float*/; } else {{
      (__tmp24__319 = ((double)((((float)7.8194735E-6) * (((float)(i__313)) - ((float)31.5))))))/*double*/;
      (__tmp23__318 = sinf(__tmp24__319))/*double*/;
      (__tmp22__317 = ((float)(__tmp23__318)))/*float*/;
      (__tmp21__316 = (__tmp22__317 / ((float)3.1415927)))/*float*/;
      (__tmp20__315 = (__tmp21__316 / (((float)(i__313)) - ((float)31.5))))/*float*/;
      (__tmp29__324 = ((double)(((((float)6.2831855) * ((float)(i__313))) / ((float)63.0)))))/*double*/;
      (__tmp28__323 = cosf(__tmp29__324))/*double*/;
      (__tmp27__322 = ((float)(__tmp28__323)))/*float*/;
      (__tmp26__321 = (((float)0.46) * __tmp27__322))/*float*/;
      (__tmp25__320 = (((float)0.54) - __tmp26__321))/*float*/;
      (__tmp19__314 = (__tmp20__315 * __tmp25__320))/*float*/;
      (((coeff__305__38)[(int)i__313]) = __tmp19__314)/*float*/;
    }}
  }
}
void save_file_pointer__38(object_write_buffer *buf) {}
void load_file_pointer__38(object_write_buffer *buf) {}
 
void work_LowPassFilter__325_125__38(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__308 = 0.0f;/* float */
      float __tmp47__309 = 0.0f;/* float */
      float __tmp48__310 = 0.0f;/* float */
      int i__conflict__0__311 = 0;/* int */
      int i__312 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__308 = ((float)0.0))/*float*/;
      for ((i__conflict__0__311 = 0)/*int*/; (i__conflict__0__311 < 64); (i__conflict__0__311++)) {{
          __peek_37_38(i__conflict__0__311);
          __tmp48__310 = BUFFER_37_38[TAIL_37_38+i__conflict__0__311];
;
          (__tmp47__309 = (__tmp48__310 * ((coeff__305__38)[(int)i__conflict__0__311])))/*float*/;
          (sum__308 = (sum__308 + __tmp47__309))/*float*/;
        }
      }
      __push_38_39(sum__308);
      for ((i__312 = 0)/*int*/; (i__312 < 0); (i__312++)) {__pop_37_38();
      }
      __pop_37_38();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_39;
int __counter_39 = 0;
int __steady_39 = 0;
int __tmp_39 = 0;
int __tmp2_39 = 0;
int *__state_flag_39 = NULL;
thread_info *__thread_39 = NULL;



inline void check_status__39() {
  check_thread_status(__state_flag_39, __thread_39);
}

void check_status_during_io__39() {
  check_thread_status_during_io(__state_flag_39, __thread_39);
}

void __init_thread_info_39(thread_info *info) {
  __state_flag_39 = info->get_state_flag();
}

thread_info *__get_thread_info_39() {
  if (__thread_39 != NULL) return __thread_39;
  __thread_39 = new thread_info(39, check_status_during_io__39);
  __init_thread_info_39(__thread_39);
  return __thread_39;
}

void __declare_sockets_39() {
  init_instance::add_incoming(38,39, DATA_SOCKET);
  init_instance::add_incoming(42,39, DATA_SOCKET);
  init_instance::add_outgoing(39,40, DATA_SOCKET);
}

void __init_sockets_39(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_39() {
}

void __peek_sockets_39() {
}


inline void __push_39_40(float data) {
BUFFER_39_40[HEAD_39_40]=data;
HEAD_39_40++;
}


inline float __pop_38_39() {
float res=BUFFER_38_39[TAIL_38_39];
TAIL_38_39++;
return res;
}

inline float __pop_42_39() {
float res=BUFFER_42_39[TAIL_42_39];
TAIL_42_39++;
return res;
}


void __joiner_39_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_39_40(__pop_38_39());
    __push_39_40(__pop_42_39());
  }
}


// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_4;
int __counter_4 = 0;
int __steady_4 = 0;
int __tmp_4 = 0;
int __tmp2_4 = 0;
int *__state_flag_4 = NULL;
thread_info *__thread_4 = NULL;



inline void check_status__4() {
  check_thread_status(__state_flag_4, __thread_4);
}

void check_status_during_io__4() {
  check_thread_status_during_io(__state_flag_4, __thread_4);
}

void __init_thread_info_4(thread_info *info) {
  __state_flag_4 = info->get_state_flag();
}

thread_info *__get_thread_info_4() {
  if (__thread_4 != NULL) return __thread_4;
  __thread_4 = new thread_info(4, check_status_during_io__4);
  __init_thread_info_4(__thread_4);
  return __thread_4;
}

void __declare_sockets_4() {
  init_instance::add_incoming(3,4, DATA_SOCKET);
  init_instance::add_outgoing(4,5, DATA_SOCKET);
  init_instance::add_outgoing(4,12, DATA_SOCKET);
}

void __init_sockets_4(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_4() {
}

void __peek_sockets_4() {
}

inline float __pop_3_4() {
float res=BUFFER_3_4[TAIL_3_4];
TAIL_3_4++;
return res;
}


inline void __push_4_5(float data) {
BUFFER_4_5[HEAD_4_5]=data;
HEAD_4_5++;
}



inline void __push_4_12(float data) {
BUFFER_4_12[HEAD_4_12]=data;
HEAD_4_12++;
}



void __splitter_4_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_4[TAIL_3_4]; TAIL_3_4++;
;
  __push_4_5(tmp);
  __push_4_12(tmp);
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_40;
int __counter_40 = 0;
int __steady_40 = 0;
int __tmp_40 = 0;
int __tmp2_40 = 0;
int *__state_flag_40 = NULL;
thread_info *__thread_40 = NULL;



void save_peek_buffer__40(object_write_buffer *buf);
void load_peek_buffer__40(object_write_buffer *buf);
void save_file_pointer__40(object_write_buffer *buf);
void load_file_pointer__40(object_write_buffer *buf);

inline void check_status__40() {
  check_thread_status(__state_flag_40, __thread_40);
}

void check_status_during_io__40() {
  check_thread_status_during_io(__state_flag_40, __thread_40);
}

void __init_thread_info_40(thread_info *info) {
  __state_flag_40 = info->get_state_flag();
}

thread_info *__get_thread_info_40() {
  if (__thread_40 != NULL) return __thread_40;
  __thread_40 = new thread_info(40, check_status_during_io__40);
  __init_thread_info_40(__thread_40);
  return __thread_40;
}

void __declare_sockets_40() {
  init_instance::add_incoming(39,40, DATA_SOCKET);
  init_instance::add_outgoing(40,41, DATA_SOCKET);
}

void __init_sockets_40(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_40() {
}

void __peek_sockets_40() {
}

 
void init_Subtracter__352_127__40();
inline void check_status__40();

void work_Subtracter__352_127__40(int);


inline float __pop_39_40() {
float res=BUFFER_39_40[TAIL_39_40];
TAIL_39_40++;
return res;
}

inline void __pop_39_40(int n) {
TAIL_39_40+=n;
}

inline float __peek_39_40(int offs) {
return BUFFER_39_40[TAIL_39_40+offs];
}



inline void __push_40_41(float data) {
BUFFER_40_41[HEAD_40_41]=data;
HEAD_40_41++;
}



 
void init_Subtracter__352_127__40(){
}
void save_file_pointer__40(object_write_buffer *buf) {}
void load_file_pointer__40(object_write_buffer *buf) {}
 
void work_Subtracter__352_127__40(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__349 = 0.0f;/* float */
      float p0__350 = 0.0f;/* float */
      float v__351 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__349 = BUFFER_39_40[TAIL_39_40+1];
;
      p0__350 = BUFFER_39_40[TAIL_39_40+0];
;
      (v__351 = (p1__349 - p0__350))/*float*/;
      __push_40_41(v__351);
      __pop_39_40(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_41;
int __counter_41 = 0;
int __steady_41 = 0;
int __tmp_41 = 0;
int __tmp2_41 = 0;
int *__state_flag_41 = NULL;
thread_info *__thread_41 = NULL;



void save_peek_buffer__41(object_write_buffer *buf);
void load_peek_buffer__41(object_write_buffer *buf);
void save_file_pointer__41(object_write_buffer *buf);
void load_file_pointer__41(object_write_buffer *buf);

inline void check_status__41() {
  check_thread_status(__state_flag_41, __thread_41);
}

void check_status_during_io__41() {
  check_thread_status_during_io(__state_flag_41, __thread_41);
}

void __init_thread_info_41(thread_info *info) {
  __state_flag_41 = info->get_state_flag();
}

thread_info *__get_thread_info_41() {
  if (__thread_41 != NULL) return __thread_41;
  __thread_41 = new thread_info(41, check_status_during_io__41);
  __init_thread_info_41(__thread_41);
  return __thread_41;
}

void __declare_sockets_41() {
  init_instance::add_incoming(40,41, DATA_SOCKET);
  init_instance::add_outgoing(41,9, DATA_SOCKET);
}

void __init_sockets_41(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_41() {
}

void __peek_sockets_41() {
}

 
void init_Amplify__357_128__41();
inline void check_status__41();

void work_Amplify__357_128__41(int);


inline float __pop_40_41() {
float res=BUFFER_40_41[TAIL_40_41];
TAIL_40_41++;
return res;
}

inline void __pop_40_41(int n) {
TAIL_40_41+=n;
}

inline float __peek_40_41(int offs) {
return BUFFER_40_41[TAIL_40_41+offs];
}



inline void __push_41_9(float data) {
BUFFER_41_9[HEAD_41_9]=data;
HEAD_41_9++;
}



 
void init_Amplify__357_128__41(){
}
void save_file_pointer__41(object_write_buffer *buf) {}
void load_file_pointer__41(object_write_buffer *buf) {}
 
void work_Amplify__357_128__41(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__355 = 0.0f;/* float */
      float __tmp52__356 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__356 = BUFFER_40_41[TAIL_40_41]; TAIL_40_41++;
;
      (__tmp51__355 = (__tmp52__356 * ((float)1.9)))/*float*/;
      __push_41_9(__tmp51__355);
      // mark end: SIRFilter Amplify

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_42;
int __counter_42 = 0;
int __steady_42 = 0;
int __tmp_42 = 0;
int __tmp2_42 = 0;
int *__state_flag_42 = NULL;
thread_info *__thread_42 = NULL;



float coeff__326__42[64] = {0};
void save_peek_buffer__42(object_write_buffer *buf);
void load_peek_buffer__42(object_write_buffer *buf);
void save_file_pointer__42(object_write_buffer *buf);
void load_file_pointer__42(object_write_buffer *buf);

inline void check_status__42() {
  check_thread_status(__state_flag_42, __thread_42);
}

void check_status_during_io__42() {
  check_thread_status_during_io(__state_flag_42, __thread_42);
}

void __init_thread_info_42(thread_info *info) {
  __state_flag_42 = info->get_state_flag();
}

thread_info *__get_thread_info_42() {
  if (__thread_42 != NULL) return __thread_42;
  __thread_42 = new thread_info(42, check_status_during_io__42);
  __init_thread_info_42(__thread_42);
  return __thread_42;
}

void __declare_sockets_42() {
  init_instance::add_incoming(37,42, DATA_SOCKET);
  init_instance::add_outgoing(42,39, DATA_SOCKET);
}

void __init_sockets_42(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_42() {
}

void __peek_sockets_42() {
}

 
void init_LowPassFilter__346_126__42();
inline void check_status__42();

void work_LowPassFilter__346_126__42(int);


inline float __pop_37_42() {
float res=BUFFER_37_42[TAIL_37_42];
TAIL_37_42++;
return res;
}

inline void __pop_37_42(int n) {
TAIL_37_42+=n;
}

inline float __peek_37_42(int offs) {
return BUFFER_37_42[TAIL_37_42+offs];
}



inline void __push_42_39(float data) {
BUFFER_42_39[HEAD_42_39]=data;
HEAD_42_39++;
}



 
void init_LowPassFilter__346_126__42(){
  int i__334 = 0;/* int */
  float __tmp30__335 = 0.0f;/* float */
  float __tmp31__336 = 0.0f;/* float */
  float __tmp32__337 = 0.0f;/* float */
  float __tmp33__338 = 0.0f;/* float */
  double __tmp34__339 = 0.0f;/* double */
  double __tmp35__340 = 0.0f;/* double */
  float __tmp36__341 = 0.0f;/* float */
  float __tmp37__342 = 0.0f;/* float */
  float __tmp38__343 = 0.0f;/* float */
  double __tmp39__344 = 0.0f;/* double */
  double __tmp40__345 = 0.0f;/* double */

  for ((i__334 = 0)/*int*/; (i__334 < 64); (i__334++)) {if (((((float)(i__334)) - ((float)31.5)) == ((float)0.0))) {(((coeff__326__42)[(int)i__334]) = ((float)3.52E-6))/*float*/; } else {{
      (__tmp35__340 = ((double)((((float)1.1058407E-5) * (((float)(i__334)) - ((float)31.5))))))/*double*/;
      (__tmp34__339 = sinf(__tmp35__340))/*double*/;
      (__tmp33__338 = ((float)(__tmp34__339)))/*float*/;
      (__tmp32__337 = (__tmp33__338 / ((float)3.1415927)))/*float*/;
      (__tmp31__336 = (__tmp32__337 / (((float)(i__334)) - ((float)31.5))))/*float*/;
      (__tmp40__345 = ((double)(((((float)6.2831855) * ((float)(i__334))) / ((float)63.0)))))/*double*/;
      (__tmp39__344 = cosf(__tmp40__345))/*double*/;
      (__tmp38__343 = ((float)(__tmp39__344)))/*float*/;
      (__tmp37__342 = (((float)0.46) * __tmp38__343))/*float*/;
      (__tmp36__341 = (((float)0.54) - __tmp37__342))/*float*/;
      (__tmp30__335 = (__tmp31__336 * __tmp36__341))/*float*/;
      (((coeff__326__42)[(int)i__334]) = __tmp30__335)/*float*/;
    }}
  }
}
void save_file_pointer__42(object_write_buffer *buf) {}
void load_file_pointer__42(object_write_buffer *buf) {}
 
void work_LowPassFilter__346_126__42(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__329 = 0.0f;/* float */
      float __tmp49__330 = 0.0f;/* float */
      float __tmp50__331 = 0.0f;/* float */
      int i__conflict__0__332 = 0;/* int */
      int i__333 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__329 = ((float)0.0))/*float*/;
      for ((i__conflict__0__332 = 0)/*int*/; (i__conflict__0__332 < 64); (i__conflict__0__332++)) {{
          __peek_37_42(i__conflict__0__332);
          __tmp50__331 = BUFFER_37_42[TAIL_37_42+i__conflict__0__332];
;
          (__tmp49__330 = (__tmp50__331 * ((coeff__326__42)[(int)i__conflict__0__332])))/*float*/;
          (sum__329 = (sum__329 + __tmp49__330))/*float*/;
        }
      }
      __push_42_39(sum__329);
      for ((i__333 = 0)/*int*/; (i__333 < 0); (i__333++)) {__pop_37_42();
      }
      __pop_37_42();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_43;
int __counter_43 = 0;
int __steady_43 = 0;
int __tmp_43 = 0;
int __tmp2_43 = 0;
int *__state_flag_43 = NULL;
thread_info *__thread_43 = NULL;



inline void check_status__43() {
  check_thread_status(__state_flag_43, __thread_43);
}

void check_status_during_io__43() {
  check_thread_status_during_io(__state_flag_43, __thread_43);
}

void __init_thread_info_43(thread_info *info) {
  __state_flag_43 = info->get_state_flag();
}

thread_info *__get_thread_info_43() {
  if (__thread_43 != NULL) return __thread_43;
  __thread_43 = new thread_info(43, check_status_during_io__43);
  __init_thread_info_43(__thread_43);
  return __thread_43;
}

void __declare_sockets_43() {
  init_instance::add_incoming(3,43, DATA_SOCKET);
  init_instance::add_outgoing(43,44, DATA_SOCKET);
  init_instance::add_outgoing(43,48, DATA_SOCKET);
}

void __init_sockets_43(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_43() {
}

void __peek_sockets_43() {
}

inline float __pop_3_43() {
float res=BUFFER_3_43[TAIL_3_43];
TAIL_3_43++;
return res;
}


inline void __push_43_44(float data) {
BUFFER_43_44[HEAD_43_44]=data;
HEAD_43_44++;
}



inline void __push_43_48(float data) {
BUFFER_43_48[HEAD_43_48]=data;
HEAD_43_48++;
}



void __splitter_43_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_43[TAIL_3_43]; TAIL_3_43++;
;
  __push_43_44(tmp);
  __push_43_48(tmp);
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_44;
int __counter_44 = 0;
int __steady_44 = 0;
int __tmp_44 = 0;
int __tmp2_44 = 0;
int *__state_flag_44 = NULL;
thread_info *__thread_44 = NULL;



float coeff__358__44[64] = {0};
void save_peek_buffer__44(object_write_buffer *buf);
void load_peek_buffer__44(object_write_buffer *buf);
void save_file_pointer__44(object_write_buffer *buf);
void load_file_pointer__44(object_write_buffer *buf);

inline void check_status__44() {
  check_thread_status(__state_flag_44, __thread_44);
}

void check_status_during_io__44() {
  check_thread_status_during_io(__state_flag_44, __thread_44);
}

void __init_thread_info_44(thread_info *info) {
  __state_flag_44 = info->get_state_flag();
}

thread_info *__get_thread_info_44() {
  if (__thread_44 != NULL) return __thread_44;
  __thread_44 = new thread_info(44, check_status_during_io__44);
  __init_thread_info_44(__thread_44);
  return __thread_44;
}

void __declare_sockets_44() {
  init_instance::add_incoming(43,44, DATA_SOCKET);
  init_instance::add_outgoing(44,45, DATA_SOCKET);
}

void __init_sockets_44(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_44() {
}

void __peek_sockets_44() {
}

 
void init_LowPassFilter__378_132__44();
inline void check_status__44();

void work_LowPassFilter__378_132__44(int);


inline float __pop_43_44() {
float res=BUFFER_43_44[TAIL_43_44];
TAIL_43_44++;
return res;
}

inline void __pop_43_44(int n) {
TAIL_43_44+=n;
}

inline float __peek_43_44(int offs) {
return BUFFER_43_44[TAIL_43_44+offs];
}



inline void __push_44_45(float data) {
BUFFER_44_45[HEAD_44_45]=data;
HEAD_44_45++;
}



 
void init_LowPassFilter__378_132__44(){
  int i__366 = 0;/* int */
  float __tmp19__367 = 0.0f;/* float */
  float __tmp20__368 = 0.0f;/* float */
  float __tmp21__369 = 0.0f;/* float */
  float __tmp22__370 = 0.0f;/* float */
  double __tmp23__371 = 0.0f;/* double */
  double __tmp24__372 = 0.0f;/* double */
  float __tmp25__373 = 0.0f;/* float */
  float __tmp26__374 = 0.0f;/* float */
  float __tmp27__375 = 0.0f;/* float */
  double __tmp28__376 = 0.0f;/* double */
  double __tmp29__377 = 0.0f;/* double */

  for ((i__366 = 0)/*int*/; (i__366 < 64); (i__366++)) {if (((((float)(i__366)) - ((float)31.5)) == ((float)0.0))) {(((coeff__358__44)[(int)i__366]) = ((float)3.52E-6))/*float*/; } else {{
      (__tmp24__372 = ((double)((((float)1.1058407E-5) * (((float)(i__366)) - ((float)31.5))))))/*double*/;
      (__tmp23__371 = sinf(__tmp24__372))/*double*/;
      (__tmp22__370 = ((float)(__tmp23__371)))/*float*/;
      (__tmp21__369 = (__tmp22__370 / ((float)3.1415927)))/*float*/;
      (__tmp20__368 = (__tmp21__369 / (((float)(i__366)) - ((float)31.5))))/*float*/;
      (__tmp29__377 = ((double)(((((float)6.2831855) * ((float)(i__366))) / ((float)63.0)))))/*double*/;
      (__tmp28__376 = cosf(__tmp29__377))/*double*/;
      (__tmp27__375 = ((float)(__tmp28__376)))/*float*/;
      (__tmp26__374 = (((float)0.46) * __tmp27__375))/*float*/;
      (__tmp25__373 = (((float)0.54) - __tmp26__374))/*float*/;
      (__tmp19__367 = (__tmp20__368 * __tmp25__373))/*float*/;
      (((coeff__358__44)[(int)i__366]) = __tmp19__367)/*float*/;
    }}
  }
}
void save_file_pointer__44(object_write_buffer *buf) {}
void load_file_pointer__44(object_write_buffer *buf) {}
 
void work_LowPassFilter__378_132__44(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__361 = 0.0f;/* float */
      float __tmp47__362 = 0.0f;/* float */
      float __tmp48__363 = 0.0f;/* float */
      int i__conflict__0__364 = 0;/* int */
      int i__365 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__361 = ((float)0.0))/*float*/;
      for ((i__conflict__0__364 = 0)/*int*/; (i__conflict__0__364 < 64); (i__conflict__0__364++)) {{
          __peek_43_44(i__conflict__0__364);
          __tmp48__363 = BUFFER_43_44[TAIL_43_44+i__conflict__0__364];
;
          (__tmp47__362 = (__tmp48__363 * ((coeff__358__44)[(int)i__conflict__0__364])))/*float*/;
          (sum__361 = (sum__361 + __tmp47__362))/*float*/;
        }
      }
      __push_44_45(sum__361);
      for ((i__365 = 0)/*int*/; (i__365 < 0); (i__365++)) {__pop_43_44();
      }
      __pop_43_44();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_45;
int __counter_45 = 0;
int __steady_45 = 0;
int __tmp_45 = 0;
int __tmp2_45 = 0;
int *__state_flag_45 = NULL;
thread_info *__thread_45 = NULL;



inline void check_status__45() {
  check_thread_status(__state_flag_45, __thread_45);
}

void check_status_during_io__45() {
  check_thread_status_during_io(__state_flag_45, __thread_45);
}

void __init_thread_info_45(thread_info *info) {
  __state_flag_45 = info->get_state_flag();
}

thread_info *__get_thread_info_45() {
  if (__thread_45 != NULL) return __thread_45;
  __thread_45 = new thread_info(45, check_status_during_io__45);
  __init_thread_info_45(__thread_45);
  return __thread_45;
}

void __declare_sockets_45() {
  init_instance::add_incoming(44,45, DATA_SOCKET);
  init_instance::add_incoming(48,45, DATA_SOCKET);
  init_instance::add_outgoing(45,46, DATA_SOCKET);
}

void __init_sockets_45(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_45() {
}

void __peek_sockets_45() {
}


inline void __push_45_46(float data) {
BUFFER_45_46[HEAD_45_46]=data;
HEAD_45_46++;
}


inline float __pop_44_45() {
float res=BUFFER_44_45[TAIL_44_45];
TAIL_44_45++;
return res;
}

inline float __pop_48_45() {
float res=BUFFER_48_45[TAIL_48_45];
TAIL_48_45++;
return res;
}


void __joiner_45_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_45_46(__pop_44_45());
    __push_45_46(__pop_48_45());
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_46;
int __counter_46 = 0;
int __steady_46 = 0;
int __tmp_46 = 0;
int __tmp2_46 = 0;
int *__state_flag_46 = NULL;
thread_info *__thread_46 = NULL;



void save_peek_buffer__46(object_write_buffer *buf);
void load_peek_buffer__46(object_write_buffer *buf);
void save_file_pointer__46(object_write_buffer *buf);
void load_file_pointer__46(object_write_buffer *buf);

inline void check_status__46() {
  check_thread_status(__state_flag_46, __thread_46);
}

void check_status_during_io__46() {
  check_thread_status_during_io(__state_flag_46, __thread_46);
}

void __init_thread_info_46(thread_info *info) {
  __state_flag_46 = info->get_state_flag();
}

thread_info *__get_thread_info_46() {
  if (__thread_46 != NULL) return __thread_46;
  __thread_46 = new thread_info(46, check_status_during_io__46);
  __init_thread_info_46(__thread_46);
  return __thread_46;
}

void __declare_sockets_46() {
  init_instance::add_incoming(45,46, DATA_SOCKET);
  init_instance::add_outgoing(46,47, DATA_SOCKET);
}

void __init_sockets_46(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_46() {
}

void __peek_sockets_46() {
}

 
void init_Subtracter__405_134__46();
inline void check_status__46();

void work_Subtracter__405_134__46(int);


inline float __pop_45_46() {
float res=BUFFER_45_46[TAIL_45_46];
TAIL_45_46++;
return res;
}

inline void __pop_45_46(int n) {
TAIL_45_46+=n;
}

inline float __peek_45_46(int offs) {
return BUFFER_45_46[TAIL_45_46+offs];
}



inline void __push_46_47(float data) {
BUFFER_46_47[HEAD_46_47]=data;
HEAD_46_47++;
}



 
void init_Subtracter__405_134__46(){
}
void save_file_pointer__46(object_write_buffer *buf) {}
void load_file_pointer__46(object_write_buffer *buf) {}
 
void work_Subtracter__405_134__46(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__402 = 0.0f;/* float */
      float p0__403 = 0.0f;/* float */
      float v__404 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__402 = BUFFER_45_46[TAIL_45_46+1];
;
      p0__403 = BUFFER_45_46[TAIL_45_46+0];
;
      (v__404 = (p1__402 - p0__403))/*float*/;
      __push_46_47(v__404);
      __pop_45_46(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_47;
int __counter_47 = 0;
int __steady_47 = 0;
int __tmp_47 = 0;
int __tmp2_47 = 0;
int *__state_flag_47 = NULL;
thread_info *__thread_47 = NULL;



void save_peek_buffer__47(object_write_buffer *buf);
void load_peek_buffer__47(object_write_buffer *buf);
void save_file_pointer__47(object_write_buffer *buf);
void load_file_pointer__47(object_write_buffer *buf);

inline void check_status__47() {
  check_thread_status(__state_flag_47, __thread_47);
}

void check_status_during_io__47() {
  check_thread_status_during_io(__state_flag_47, __thread_47);
}

void __init_thread_info_47(thread_info *info) {
  __state_flag_47 = info->get_state_flag();
}

thread_info *__get_thread_info_47() {
  if (__thread_47 != NULL) return __thread_47;
  __thread_47 = new thread_info(47, check_status_during_io__47);
  __init_thread_info_47(__thread_47);
  return __thread_47;
}

void __declare_sockets_47() {
  init_instance::add_incoming(46,47, DATA_SOCKET);
  init_instance::add_outgoing(47,9, DATA_SOCKET);
}

void __init_sockets_47(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_47() {
}

void __peek_sockets_47() {
}

 
void init_Amplify__410_135__47();
inline void check_status__47();

void work_Amplify__410_135__47(int);


inline float __pop_46_47() {
float res=BUFFER_46_47[TAIL_46_47];
TAIL_46_47++;
return res;
}

inline void __pop_46_47(int n) {
TAIL_46_47+=n;
}

inline float __peek_46_47(int offs) {
return BUFFER_46_47[TAIL_46_47+offs];
}



inline void __push_47_9(float data) {
BUFFER_47_9[HEAD_47_9]=data;
HEAD_47_9++;
}



 
void init_Amplify__410_135__47(){
}
void save_file_pointer__47(object_write_buffer *buf) {}
void load_file_pointer__47(object_write_buffer *buf) {}
 
void work_Amplify__410_135__47(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__408 = 0.0f;/* float */
      float __tmp52__409 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__409 = BUFFER_46_47[TAIL_46_47]; TAIL_46_47++;
;
      (__tmp51__408 = (__tmp52__409 * ((float)1.7)))/*float*/;
      __push_47_9(__tmp51__408);
      // mark end: SIRFilter Amplify

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_48;
int __counter_48 = 0;
int __steady_48 = 0;
int __tmp_48 = 0;
int __tmp2_48 = 0;
int *__state_flag_48 = NULL;
thread_info *__thread_48 = NULL;



float coeff__379__48[64] = {0};
void save_peek_buffer__48(object_write_buffer *buf);
void load_peek_buffer__48(object_write_buffer *buf);
void save_file_pointer__48(object_write_buffer *buf);
void load_file_pointer__48(object_write_buffer *buf);

inline void check_status__48() {
  check_thread_status(__state_flag_48, __thread_48);
}

void check_status_during_io__48() {
  check_thread_status_during_io(__state_flag_48, __thread_48);
}

void __init_thread_info_48(thread_info *info) {
  __state_flag_48 = info->get_state_flag();
}

thread_info *__get_thread_info_48() {
  if (__thread_48 != NULL) return __thread_48;
  __thread_48 = new thread_info(48, check_status_during_io__48);
  __init_thread_info_48(__thread_48);
  return __thread_48;
}

void __declare_sockets_48() {
  init_instance::add_incoming(43,48, DATA_SOCKET);
  init_instance::add_outgoing(48,45, DATA_SOCKET);
}

void __init_sockets_48(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_48() {
}

void __peek_sockets_48() {
}

 
void init_LowPassFilter__399_133__48();
inline void check_status__48();

void work_LowPassFilter__399_133__48(int);


inline float __pop_43_48() {
float res=BUFFER_43_48[TAIL_43_48];
TAIL_43_48++;
return res;
}

inline void __pop_43_48(int n) {
TAIL_43_48+=n;
}

inline float __peek_43_48(int offs) {
return BUFFER_43_48[TAIL_43_48+offs];
}



inline void __push_48_45(float data) {
BUFFER_48_45[HEAD_48_45]=data;
HEAD_48_45++;
}



 
void init_LowPassFilter__399_133__48(){
  int i__387 = 0;/* int */
  float __tmp30__388 = 0.0f;/* float */
  float __tmp31__389 = 0.0f;/* float */
  float __tmp32__390 = 0.0f;/* float */
  float __tmp33__391 = 0.0f;/* float */
  double __tmp34__392 = 0.0f;/* double */
  double __tmp35__393 = 0.0f;/* double */
  float __tmp36__394 = 0.0f;/* float */
  float __tmp37__395 = 0.0f;/* float */
  float __tmp38__396 = 0.0f;/* float */
  double __tmp39__397 = 0.0f;/* double */
  double __tmp40__398 = 0.0f;/* double */

  for ((i__387 = 0)/*int*/; (i__387 < 64); (i__387++)) {if (((((float)(i__387)) - ((float)31.5)) == ((float)0.0))) {(((coeff__379__48)[(int)i__387]) = ((float)4.978033E-6))/*float*/; } else {{
      (__tmp35__393 = ((double)((((float)1.5638952E-5) * (((float)(i__387)) - ((float)31.5))))))/*double*/;
      (__tmp34__392 = sinf(__tmp35__393))/*double*/;
      (__tmp33__391 = ((float)(__tmp34__392)))/*float*/;
      (__tmp32__390 = (__tmp33__391 / ((float)3.1415927)))/*float*/;
      (__tmp31__389 = (__tmp32__390 / (((float)(i__387)) - ((float)31.5))))/*float*/;
      (__tmp40__398 = ((double)(((((float)6.2831855) * ((float)(i__387))) / ((float)63.0)))))/*double*/;
      (__tmp39__397 = cosf(__tmp40__398))/*double*/;
      (__tmp38__396 = ((float)(__tmp39__397)))/*float*/;
      (__tmp37__395 = (((float)0.46) * __tmp38__396))/*float*/;
      (__tmp36__394 = (((float)0.54) - __tmp37__395))/*float*/;
      (__tmp30__388 = (__tmp31__389 * __tmp36__394))/*float*/;
      (((coeff__379__48)[(int)i__387]) = __tmp30__388)/*float*/;
    }}
  }
}
void save_file_pointer__48(object_write_buffer *buf) {}
void load_file_pointer__48(object_write_buffer *buf) {}
 
void work_LowPassFilter__399_133__48(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__382 = 0.0f;/* float */
      float __tmp49__383 = 0.0f;/* float */
      float __tmp50__384 = 0.0f;/* float */
      int i__conflict__0__385 = 0;/* int */
      int i__386 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__382 = ((float)0.0))/*float*/;
      for ((i__conflict__0__385 = 0)/*int*/; (i__conflict__0__385 < 64); (i__conflict__0__385++)) {{
          __peek_43_48(i__conflict__0__385);
          __tmp50__384 = BUFFER_43_48[TAIL_43_48+i__conflict__0__385];
;
          (__tmp49__383 = (__tmp50__384 * ((coeff__379__48)[(int)i__conflict__0__385])))/*float*/;
          (sum__382 = (sum__382 + __tmp49__383))/*float*/;
        }
      }
      __push_48_45(sum__382);
      for ((i__386 = 0)/*int*/; (i__386 < 0); (i__386++)) {__pop_43_48();
      }
      __pop_43_48();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_49;
int __counter_49 = 0;
int __steady_49 = 0;
int __tmp_49 = 0;
int __tmp2_49 = 0;
int *__state_flag_49 = NULL;
thread_info *__thread_49 = NULL;



inline void check_status__49() {
  check_thread_status(__state_flag_49, __thread_49);
}

void check_status_during_io__49() {
  check_thread_status_during_io(__state_flag_49, __thread_49);
}

void __init_thread_info_49(thread_info *info) {
  __state_flag_49 = info->get_state_flag();
}

thread_info *__get_thread_info_49() {
  if (__thread_49 != NULL) return __thread_49;
  __thread_49 = new thread_info(49, check_status_during_io__49);
  __init_thread_info_49(__thread_49);
  return __thread_49;
}

void __declare_sockets_49() {
  init_instance::add_incoming(3,49, DATA_SOCKET);
  init_instance::add_outgoing(49,50, DATA_SOCKET);
  init_instance::add_outgoing(49,54, DATA_SOCKET);
}

void __init_sockets_49(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_49() {
}

void __peek_sockets_49() {
}

inline float __pop_3_49() {
float res=BUFFER_3_49[TAIL_3_49];
TAIL_3_49++;
return res;
}


inline void __push_49_50(float data) {
BUFFER_49_50[HEAD_49_50]=data;
HEAD_49_50++;
}



inline void __push_49_54(float data) {
BUFFER_49_54[HEAD_49_54]=data;
HEAD_49_54++;
}



void __splitter_49_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_49[TAIL_3_49]; TAIL_3_49++;
;
  __push_49_50(tmp);
  __push_49_54(tmp);
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_5;
int __counter_5 = 0;
int __steady_5 = 0;
int __tmp_5 = 0;
int __tmp2_5 = 0;
int *__state_flag_5 = NULL;
thread_info *__thread_5 = NULL;



float coeff__40__5[64] = {0};
void save_peek_buffer__5(object_write_buffer *buf);
void load_peek_buffer__5(object_write_buffer *buf);
void save_file_pointer__5(object_write_buffer *buf);
void load_file_pointer__5(object_write_buffer *buf);

inline void check_status__5() {
  check_thread_status(__state_flag_5, __thread_5);
}

void check_status_during_io__5() {
  check_thread_status_during_io(__state_flag_5, __thread_5);
}

void __init_thread_info_5(thread_info *info) {
  __state_flag_5 = info->get_state_flag();
}

thread_info *__get_thread_info_5() {
  if (__thread_5 != NULL) return __thread_5;
  __thread_5 = new thread_info(5, check_status_during_io__5);
  __init_thread_info_5(__thread_5);
  return __thread_5;
}

void __declare_sockets_5() {
  init_instance::add_incoming(4,5, DATA_SOCKET);
  init_instance::add_outgoing(5,6, DATA_SOCKET);
}

void __init_sockets_5(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_5() {
}

void __peek_sockets_5() {
}

 
void init_LowPassFilter__60_90__5();
inline void check_status__5();

void work_LowPassFilter__60_90__5(int);


inline float __pop_4_5() {
float res=BUFFER_4_5[TAIL_4_5];
TAIL_4_5++;
return res;
}

inline void __pop_4_5(int n) {
TAIL_4_5+=n;
}

inline float __peek_4_5(int offs) {
return BUFFER_4_5[TAIL_4_5+offs];
}



inline void __push_5_6(float data) {
BUFFER_5_6[HEAD_5_6]=data;
HEAD_5_6++;
}



 
void init_LowPassFilter__60_90__5(){
  int i__48 = 0;/* int */
  float __tmp19__49 = 0.0f;/* float */
  float __tmp20__50 = 0.0f;/* float */
  float __tmp21__51 = 0.0f;/* float */
  float __tmp22__52 = 0.0f;/* float */
  double __tmp23__53 = 0.0f;/* double */
  double __tmp24__54 = 0.0f;/* double */
  float __tmp25__55 = 0.0f;/* float */
  float __tmp26__56 = 0.0f;/* float */
  float __tmp27__57 = 0.0f;/* float */
  double __tmp28__58 = 0.0f;/* double */
  double __tmp29__59 = 0.0f;/* double */

  for ((i__48 = 0)/*int*/; (i__48 < 64); (i__48++)) {if (((((float)(i__48)) - ((float)31.5)) == ((float)0.0))) {(((coeff__40__5)[(int)i__48]) = ((float)4.4E-7))/*float*/; } else {{
      (__tmp24__54 = ((double)((((float)1.3823009E-6) * (((float)(i__48)) - ((float)31.5))))))/*double*/;
      (__tmp23__53 = sinf(__tmp24__54))/*double*/;
      (__tmp22__52 = ((float)(__tmp23__53)))/*float*/;
      (__tmp21__51 = (__tmp22__52 / ((float)3.1415927)))/*float*/;
      (__tmp20__50 = (__tmp21__51 / (((float)(i__48)) - ((float)31.5))))/*float*/;
      (__tmp29__59 = ((double)(((((float)6.2831855) * ((float)(i__48))) / ((float)63.0)))))/*double*/;
      (__tmp28__58 = cosf(__tmp29__59))/*double*/;
      (__tmp27__57 = ((float)(__tmp28__58)))/*float*/;
      (__tmp26__56 = (((float)0.46) * __tmp27__57))/*float*/;
      (__tmp25__55 = (((float)0.54) - __tmp26__56))/*float*/;
      (__tmp19__49 = (__tmp20__50 * __tmp25__55))/*float*/;
      (((coeff__40__5)[(int)i__48]) = __tmp19__49)/*float*/;
    }}
  }
}
void save_file_pointer__5(object_write_buffer *buf) {}
void load_file_pointer__5(object_write_buffer *buf) {}
 
void work_LowPassFilter__60_90__5(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__43 = 0.0f;/* float */
      float __tmp47__44 = 0.0f;/* float */
      float __tmp48__45 = 0.0f;/* float */
      int i__conflict__0__46 = 0;/* int */
      int i__47 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__43 = ((float)0.0))/*float*/;
      for ((i__conflict__0__46 = 0)/*int*/; (i__conflict__0__46 < 64); (i__conflict__0__46++)) {{
          __peek_4_5(i__conflict__0__46);
          __tmp48__45 = BUFFER_4_5[TAIL_4_5+i__conflict__0__46];
;
          (__tmp47__44 = (__tmp48__45 * ((coeff__40__5)[(int)i__conflict__0__46])))/*float*/;
          (sum__43 = (sum__43 + __tmp47__44))/*float*/;
        }
      }
      __push_5_6(sum__43);
      for ((i__47 = 0)/*int*/; (i__47 < 0); (i__47++)) {__pop_4_5();
      }
      __pop_4_5();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_50;
int __counter_50 = 0;
int __steady_50 = 0;
int __tmp_50 = 0;
int __tmp2_50 = 0;
int *__state_flag_50 = NULL;
thread_info *__thread_50 = NULL;



float coeff__411__50[64] = {0};
void save_peek_buffer__50(object_write_buffer *buf);
void load_peek_buffer__50(object_write_buffer *buf);
void save_file_pointer__50(object_write_buffer *buf);
void load_file_pointer__50(object_write_buffer *buf);

inline void check_status__50() {
  check_thread_status(__state_flag_50, __thread_50);
}

void check_status_during_io__50() {
  check_thread_status_during_io(__state_flag_50, __thread_50);
}

void __init_thread_info_50(thread_info *info) {
  __state_flag_50 = info->get_state_flag();
}

thread_info *__get_thread_info_50() {
  if (__thread_50 != NULL) return __thread_50;
  __thread_50 = new thread_info(50, check_status_during_io__50);
  __init_thread_info_50(__thread_50);
  return __thread_50;
}

void __declare_sockets_50() {
  init_instance::add_incoming(49,50, DATA_SOCKET);
  init_instance::add_outgoing(50,51, DATA_SOCKET);
}

void __init_sockets_50(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_50() {
}

void __peek_sockets_50() {
}

 
void init_LowPassFilter__431_139__50();
inline void check_status__50();

void work_LowPassFilter__431_139__50(int);


inline float __pop_49_50() {
float res=BUFFER_49_50[TAIL_49_50];
TAIL_49_50++;
return res;
}

inline void __pop_49_50(int n) {
TAIL_49_50+=n;
}

inline float __peek_49_50(int offs) {
return BUFFER_49_50[TAIL_49_50+offs];
}



inline void __push_50_51(float data) {
BUFFER_50_51[HEAD_50_51]=data;
HEAD_50_51++;
}



 
void init_LowPassFilter__431_139__50(){
  int i__419 = 0;/* int */
  float __tmp19__420 = 0.0f;/* float */
  float __tmp20__421 = 0.0f;/* float */
  float __tmp21__422 = 0.0f;/* float */
  float __tmp22__423 = 0.0f;/* float */
  double __tmp23__424 = 0.0f;/* double */
  double __tmp24__425 = 0.0f;/* double */
  float __tmp25__426 = 0.0f;/* float */
  float __tmp26__427 = 0.0f;/* float */
  float __tmp27__428 = 0.0f;/* float */
  double __tmp28__429 = 0.0f;/* double */
  double __tmp29__430 = 0.0f;/* double */

  for ((i__419 = 0)/*int*/; (i__419 < 64); (i__419++)) {if (((((float)(i__419)) - ((float)31.5)) == ((float)0.0))) {(((coeff__411__50)[(int)i__419]) = ((float)4.978033E-6))/*float*/; } else {{
      (__tmp24__425 = ((double)((((float)1.5638952E-5) * (((float)(i__419)) - ((float)31.5))))))/*double*/;
      (__tmp23__424 = sinf(__tmp24__425))/*double*/;
      (__tmp22__423 = ((float)(__tmp23__424)))/*float*/;
      (__tmp21__422 = (__tmp22__423 / ((float)3.1415927)))/*float*/;
      (__tmp20__421 = (__tmp21__422 / (((float)(i__419)) - ((float)31.5))))/*float*/;
      (__tmp29__430 = ((double)(((((float)6.2831855) * ((float)(i__419))) / ((float)63.0)))))/*double*/;
      (__tmp28__429 = cosf(__tmp29__430))/*double*/;
      (__tmp27__428 = ((float)(__tmp28__429)))/*float*/;
      (__tmp26__427 = (((float)0.46) * __tmp27__428))/*float*/;
      (__tmp25__426 = (((float)0.54) - __tmp26__427))/*float*/;
      (__tmp19__420 = (__tmp20__421 * __tmp25__426))/*float*/;
      (((coeff__411__50)[(int)i__419]) = __tmp19__420)/*float*/;
    }}
  }
}
void save_file_pointer__50(object_write_buffer *buf) {}
void load_file_pointer__50(object_write_buffer *buf) {}
 
void work_LowPassFilter__431_139__50(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__414 = 0.0f;/* float */
      float __tmp47__415 = 0.0f;/* float */
      float __tmp48__416 = 0.0f;/* float */
      int i__conflict__0__417 = 0;/* int */
      int i__418 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__414 = ((float)0.0))/*float*/;
      for ((i__conflict__0__417 = 0)/*int*/; (i__conflict__0__417 < 64); (i__conflict__0__417++)) {{
          __peek_49_50(i__conflict__0__417);
          __tmp48__416 = BUFFER_49_50[TAIL_49_50+i__conflict__0__417];
;
          (__tmp47__415 = (__tmp48__416 * ((coeff__411__50)[(int)i__conflict__0__417])))/*float*/;
          (sum__414 = (sum__414 + __tmp47__415))/*float*/;
        }
      }
      __push_50_51(sum__414);
      for ((i__418 = 0)/*int*/; (i__418 < 0); (i__418++)) {__pop_49_50();
      }
      __pop_49_50();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_51;
int __counter_51 = 0;
int __steady_51 = 0;
int __tmp_51 = 0;
int __tmp2_51 = 0;
int *__state_flag_51 = NULL;
thread_info *__thread_51 = NULL;



inline void check_status__51() {
  check_thread_status(__state_flag_51, __thread_51);
}

void check_status_during_io__51() {
  check_thread_status_during_io(__state_flag_51, __thread_51);
}

void __init_thread_info_51(thread_info *info) {
  __state_flag_51 = info->get_state_flag();
}

thread_info *__get_thread_info_51() {
  if (__thread_51 != NULL) return __thread_51;
  __thread_51 = new thread_info(51, check_status_during_io__51);
  __init_thread_info_51(__thread_51);
  return __thread_51;
}

void __declare_sockets_51() {
  init_instance::add_incoming(50,51, DATA_SOCKET);
  init_instance::add_incoming(54,51, DATA_SOCKET);
  init_instance::add_outgoing(51,52, DATA_SOCKET);
}

void __init_sockets_51(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_51() {
}

void __peek_sockets_51() {
}


inline void __push_51_52(float data) {
BUFFER_51_52[HEAD_51_52]=data;
HEAD_51_52++;
}


inline float __pop_50_51() {
float res=BUFFER_50_51[TAIL_50_51];
TAIL_50_51++;
return res;
}

inline float __pop_54_51() {
float res=BUFFER_54_51[TAIL_54_51];
TAIL_54_51++;
return res;
}


void __joiner_51_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_51_52(__pop_50_51());
    __push_51_52(__pop_54_51());
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_52;
int __counter_52 = 0;
int __steady_52 = 0;
int __tmp_52 = 0;
int __tmp2_52 = 0;
int *__state_flag_52 = NULL;
thread_info *__thread_52 = NULL;



void save_peek_buffer__52(object_write_buffer *buf);
void load_peek_buffer__52(object_write_buffer *buf);
void save_file_pointer__52(object_write_buffer *buf);
void load_file_pointer__52(object_write_buffer *buf);

inline void check_status__52() {
  check_thread_status(__state_flag_52, __thread_52);
}

void check_status_during_io__52() {
  check_thread_status_during_io(__state_flag_52, __thread_52);
}

void __init_thread_info_52(thread_info *info) {
  __state_flag_52 = info->get_state_flag();
}

thread_info *__get_thread_info_52() {
  if (__thread_52 != NULL) return __thread_52;
  __thread_52 = new thread_info(52, check_status_during_io__52);
  __init_thread_info_52(__thread_52);
  return __thread_52;
}

void __declare_sockets_52() {
  init_instance::add_incoming(51,52, DATA_SOCKET);
  init_instance::add_outgoing(52,53, DATA_SOCKET);
}

void __init_sockets_52(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_52() {
}

void __peek_sockets_52() {
}

 
void init_Subtracter__458_141__52();
inline void check_status__52();

void work_Subtracter__458_141__52(int);


inline float __pop_51_52() {
float res=BUFFER_51_52[TAIL_51_52];
TAIL_51_52++;
return res;
}

inline void __pop_51_52(int n) {
TAIL_51_52+=n;
}

inline float __peek_51_52(int offs) {
return BUFFER_51_52[TAIL_51_52+offs];
}



inline void __push_52_53(float data) {
BUFFER_52_53[HEAD_52_53]=data;
HEAD_52_53++;
}



 
void init_Subtracter__458_141__52(){
}
void save_file_pointer__52(object_write_buffer *buf) {}
void load_file_pointer__52(object_write_buffer *buf) {}
 
void work_Subtracter__458_141__52(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__455 = 0.0f;/* float */
      float p0__456 = 0.0f;/* float */
      float v__457 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__455 = BUFFER_51_52[TAIL_51_52+1];
;
      p0__456 = BUFFER_51_52[TAIL_51_52+0];
;
      (v__457 = (p1__455 - p0__456))/*float*/;
      __push_52_53(v__457);
      __pop_51_52(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_53;
int __counter_53 = 0;
int __steady_53 = 0;
int __tmp_53 = 0;
int __tmp2_53 = 0;
int *__state_flag_53 = NULL;
thread_info *__thread_53 = NULL;



void save_peek_buffer__53(object_write_buffer *buf);
void load_peek_buffer__53(object_write_buffer *buf);
void save_file_pointer__53(object_write_buffer *buf);
void load_file_pointer__53(object_write_buffer *buf);

inline void check_status__53() {
  check_thread_status(__state_flag_53, __thread_53);
}

void check_status_during_io__53() {
  check_thread_status_during_io(__state_flag_53, __thread_53);
}

void __init_thread_info_53(thread_info *info) {
  __state_flag_53 = info->get_state_flag();
}

thread_info *__get_thread_info_53() {
  if (__thread_53 != NULL) return __thread_53;
  __thread_53 = new thread_info(53, check_status_during_io__53);
  __init_thread_info_53(__thread_53);
  return __thread_53;
}

void __declare_sockets_53() {
  init_instance::add_incoming(52,53, DATA_SOCKET);
  init_instance::add_outgoing(53,9, DATA_SOCKET);
}

void __init_sockets_53(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_53() {
}

void __peek_sockets_53() {
}

 
void init_Amplify__463_142__53();
inline void check_status__53();

void work_Amplify__463_142__53(int);


inline float __pop_52_53() {
float res=BUFFER_52_53[TAIL_52_53];
TAIL_52_53++;
return res;
}

inline void __pop_52_53(int n) {
TAIL_52_53+=n;
}

inline float __peek_52_53(int offs) {
return BUFFER_52_53[TAIL_52_53+offs];
}



inline void __push_53_9(float data) {
BUFFER_53_9[HEAD_53_9]=data;
HEAD_53_9++;
}



 
void init_Amplify__463_142__53(){
}
void save_file_pointer__53(object_write_buffer *buf) {}
void load_file_pointer__53(object_write_buffer *buf) {}
 
void work_Amplify__463_142__53(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__461 = 0.0f;/* float */
      float __tmp52__462 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__462 = BUFFER_52_53[TAIL_52_53]; TAIL_52_53++;
;
      (__tmp51__461 = (__tmp52__462 * ((float)1.5)))/*float*/;
      __push_53_9(__tmp51__461);
      // mark end: SIRFilter Amplify

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_54;
int __counter_54 = 0;
int __steady_54 = 0;
int __tmp_54 = 0;
int __tmp2_54 = 0;
int *__state_flag_54 = NULL;
thread_info *__thread_54 = NULL;



float coeff__432__54[64] = {0};
void save_peek_buffer__54(object_write_buffer *buf);
void load_peek_buffer__54(object_write_buffer *buf);
void save_file_pointer__54(object_write_buffer *buf);
void load_file_pointer__54(object_write_buffer *buf);

inline void check_status__54() {
  check_thread_status(__state_flag_54, __thread_54);
}

void check_status_during_io__54() {
  check_thread_status_during_io(__state_flag_54, __thread_54);
}

void __init_thread_info_54(thread_info *info) {
  __state_flag_54 = info->get_state_flag();
}

thread_info *__get_thread_info_54() {
  if (__thread_54 != NULL) return __thread_54;
  __thread_54 = new thread_info(54, check_status_during_io__54);
  __init_thread_info_54(__thread_54);
  return __thread_54;
}

void __declare_sockets_54() {
  init_instance::add_incoming(49,54, DATA_SOCKET);
  init_instance::add_outgoing(54,51, DATA_SOCKET);
}

void __init_sockets_54(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_54() {
}

void __peek_sockets_54() {
}

 
void init_LowPassFilter__452_140__54();
inline void check_status__54();

void work_LowPassFilter__452_140__54(int);


inline float __pop_49_54() {
float res=BUFFER_49_54[TAIL_49_54];
TAIL_49_54++;
return res;
}

inline void __pop_49_54(int n) {
TAIL_49_54+=n;
}

inline float __peek_49_54(int offs) {
return BUFFER_49_54[TAIL_49_54+offs];
}



inline void __push_54_51(float data) {
BUFFER_54_51[HEAD_54_51]=data;
HEAD_54_51++;
}



 
void init_LowPassFilter__452_140__54(){
  int i__440 = 0;/* int */
  float __tmp30__441 = 0.0f;/* float */
  float __tmp31__442 = 0.0f;/* float */
  float __tmp32__443 = 0.0f;/* float */
  float __tmp33__444 = 0.0f;/* float */
  double __tmp34__445 = 0.0f;/* double */
  double __tmp35__446 = 0.0f;/* double */
  float __tmp36__447 = 0.0f;/* float */
  float __tmp37__448 = 0.0f;/* float */
  float __tmp38__449 = 0.0f;/* float */
  double __tmp39__450 = 0.0f;/* double */
  double __tmp40__451 = 0.0f;/* double */

  for ((i__440 = 0)/*int*/; (i__440 < 64); (i__440++)) {if (((((float)(i__440)) - ((float)31.5)) == ((float)0.0))) {(((coeff__432__54)[(int)i__440]) = ((float)7.04E-6))/*float*/; } else {{
      (__tmp35__446 = ((double)((((float)2.2116814E-5) * (((float)(i__440)) - ((float)31.5))))))/*double*/;
      (__tmp34__445 = sinf(__tmp35__446))/*double*/;
      (__tmp33__444 = ((float)(__tmp34__445)))/*float*/;
      (__tmp32__443 = (__tmp33__444 / ((float)3.1415927)))/*float*/;
      (__tmp31__442 = (__tmp32__443 / (((float)(i__440)) - ((float)31.5))))/*float*/;
      (__tmp40__451 = ((double)(((((float)6.2831855) * ((float)(i__440))) / ((float)63.0)))))/*double*/;
      (__tmp39__450 = cosf(__tmp40__451))/*double*/;
      (__tmp38__449 = ((float)(__tmp39__450)))/*float*/;
      (__tmp37__448 = (((float)0.46) * __tmp38__449))/*float*/;
      (__tmp36__447 = (((float)0.54) - __tmp37__448))/*float*/;
      (__tmp30__441 = (__tmp31__442 * __tmp36__447))/*float*/;
      (((coeff__432__54)[(int)i__440]) = __tmp30__441)/*float*/;
    }}
  }
}
void save_file_pointer__54(object_write_buffer *buf) {}
void load_file_pointer__54(object_write_buffer *buf) {}
 
void work_LowPassFilter__452_140__54(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__435 = 0.0f;/* float */
      float __tmp49__436 = 0.0f;/* float */
      float __tmp50__437 = 0.0f;/* float */
      int i__conflict__0__438 = 0;/* int */
      int i__439 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__435 = ((float)0.0))/*float*/;
      for ((i__conflict__0__438 = 0)/*int*/; (i__conflict__0__438 < 64); (i__conflict__0__438++)) {{
          __peek_49_54(i__conflict__0__438);
          __tmp50__437 = BUFFER_49_54[TAIL_49_54+i__conflict__0__438];
;
          (__tmp49__436 = (__tmp50__437 * ((coeff__432__54)[(int)i__conflict__0__438])))/*float*/;
          (sum__435 = (sum__435 + __tmp49__436))/*float*/;
        }
      }
      __push_54_51(sum__435);
      for ((i__439 = 0)/*int*/; (i__439 < 0); (i__439++)) {__pop_49_54();
      }
      __pop_49_54();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_55;
int __counter_55 = 0;
int __steady_55 = 0;
int __tmp_55 = 0;
int __tmp2_55 = 0;
int *__state_flag_55 = NULL;
thread_info *__thread_55 = NULL;



inline void check_status__55() {
  check_thread_status(__state_flag_55, __thread_55);
}

void check_status_during_io__55() {
  check_thread_status_during_io(__state_flag_55, __thread_55);
}

void __init_thread_info_55(thread_info *info) {
  __state_flag_55 = info->get_state_flag();
}

thread_info *__get_thread_info_55() {
  if (__thread_55 != NULL) return __thread_55;
  __thread_55 = new thread_info(55, check_status_during_io__55);
  __init_thread_info_55(__thread_55);
  return __thread_55;
}

void __declare_sockets_55() {
  init_instance::add_incoming(3,55, DATA_SOCKET);
  init_instance::add_outgoing(55,56, DATA_SOCKET);
  init_instance::add_outgoing(55,60, DATA_SOCKET);
}

void __init_sockets_55(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_55() {
}

void __peek_sockets_55() {
}

inline float __pop_3_55() {
float res=BUFFER_3_55[TAIL_3_55];
TAIL_3_55++;
return res;
}


inline void __push_55_56(float data) {
BUFFER_55_56[HEAD_55_56]=data;
HEAD_55_56++;
}



inline void __push_55_60(float data) {
BUFFER_55_60[HEAD_55_60]=data;
HEAD_55_60++;
}



void __splitter_55_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_55[TAIL_3_55]; TAIL_3_55++;
;
  __push_55_56(tmp);
  __push_55_60(tmp);
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_56;
int __counter_56 = 0;
int __steady_56 = 0;
int __tmp_56 = 0;
int __tmp2_56 = 0;
int *__state_flag_56 = NULL;
thread_info *__thread_56 = NULL;



float coeff__464__56[64] = {0};
void save_peek_buffer__56(object_write_buffer *buf);
void load_peek_buffer__56(object_write_buffer *buf);
void save_file_pointer__56(object_write_buffer *buf);
void load_file_pointer__56(object_write_buffer *buf);

inline void check_status__56() {
  check_thread_status(__state_flag_56, __thread_56);
}

void check_status_during_io__56() {
  check_thread_status_during_io(__state_flag_56, __thread_56);
}

void __init_thread_info_56(thread_info *info) {
  __state_flag_56 = info->get_state_flag();
}

thread_info *__get_thread_info_56() {
  if (__thread_56 != NULL) return __thread_56;
  __thread_56 = new thread_info(56, check_status_during_io__56);
  __init_thread_info_56(__thread_56);
  return __thread_56;
}

void __declare_sockets_56() {
  init_instance::add_incoming(55,56, DATA_SOCKET);
  init_instance::add_outgoing(56,57, DATA_SOCKET);
}

void __init_sockets_56(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_56() {
}

void __peek_sockets_56() {
}

 
void init_LowPassFilter__484_146__56();
inline void check_status__56();

void work_LowPassFilter__484_146__56(int);


inline float __pop_55_56() {
float res=BUFFER_55_56[TAIL_55_56];
TAIL_55_56++;
return res;
}

inline void __pop_55_56(int n) {
TAIL_55_56+=n;
}

inline float __peek_55_56(int offs) {
return BUFFER_55_56[TAIL_55_56+offs];
}



inline void __push_56_57(float data) {
BUFFER_56_57[HEAD_56_57]=data;
HEAD_56_57++;
}



 
void init_LowPassFilter__484_146__56(){
  int i__472 = 0;/* int */
  float __tmp19__473 = 0.0f;/* float */
  float __tmp20__474 = 0.0f;/* float */
  float __tmp21__475 = 0.0f;/* float */
  float __tmp22__476 = 0.0f;/* float */
  double __tmp23__477 = 0.0f;/* double */
  double __tmp24__478 = 0.0f;/* double */
  float __tmp25__479 = 0.0f;/* float */
  float __tmp26__480 = 0.0f;/* float */
  float __tmp27__481 = 0.0f;/* float */
  double __tmp28__482 = 0.0f;/* double */
  double __tmp29__483 = 0.0f;/* double */

  for ((i__472 = 0)/*int*/; (i__472 < 64); (i__472++)) {if (((((float)(i__472)) - ((float)31.5)) == ((float)0.0))) {(((coeff__464__56)[(int)i__472]) = ((float)7.04E-6))/*float*/; } else {{
      (__tmp24__478 = ((double)((((float)2.2116814E-5) * (((float)(i__472)) - ((float)31.5))))))/*double*/;
      (__tmp23__477 = sinf(__tmp24__478))/*double*/;
      (__tmp22__476 = ((float)(__tmp23__477)))/*float*/;
      (__tmp21__475 = (__tmp22__476 / ((float)3.1415927)))/*float*/;
      (__tmp20__474 = (__tmp21__475 / (((float)(i__472)) - ((float)31.5))))/*float*/;
      (__tmp29__483 = ((double)(((((float)6.2831855) * ((float)(i__472))) / ((float)63.0)))))/*double*/;
      (__tmp28__482 = cosf(__tmp29__483))/*double*/;
      (__tmp27__481 = ((float)(__tmp28__482)))/*float*/;
      (__tmp26__480 = (((float)0.46) * __tmp27__481))/*float*/;
      (__tmp25__479 = (((float)0.54) - __tmp26__480))/*float*/;
      (__tmp19__473 = (__tmp20__474 * __tmp25__479))/*float*/;
      (((coeff__464__56)[(int)i__472]) = __tmp19__473)/*float*/;
    }}
  }
}
void save_file_pointer__56(object_write_buffer *buf) {}
void load_file_pointer__56(object_write_buffer *buf) {}
 
void work_LowPassFilter__484_146__56(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__467 = 0.0f;/* float */
      float __tmp47__468 = 0.0f;/* float */
      float __tmp48__469 = 0.0f;/* float */
      int i__conflict__0__470 = 0;/* int */
      int i__471 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__467 = ((float)0.0))/*float*/;
      for ((i__conflict__0__470 = 0)/*int*/; (i__conflict__0__470 < 64); (i__conflict__0__470++)) {{
          __peek_55_56(i__conflict__0__470);
          __tmp48__469 = BUFFER_55_56[TAIL_55_56+i__conflict__0__470];
;
          (__tmp47__468 = (__tmp48__469 * ((coeff__464__56)[(int)i__conflict__0__470])))/*float*/;
          (sum__467 = (sum__467 + __tmp47__468))/*float*/;
        }
      }
      __push_56_57(sum__467);
      for ((i__471 = 0)/*int*/; (i__471 < 0); (i__471++)) {__pop_55_56();
      }
      __pop_55_56();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_57;
int __counter_57 = 0;
int __steady_57 = 0;
int __tmp_57 = 0;
int __tmp2_57 = 0;
int *__state_flag_57 = NULL;
thread_info *__thread_57 = NULL;



inline void check_status__57() {
  check_thread_status(__state_flag_57, __thread_57);
}

void check_status_during_io__57() {
  check_thread_status_during_io(__state_flag_57, __thread_57);
}

void __init_thread_info_57(thread_info *info) {
  __state_flag_57 = info->get_state_flag();
}

thread_info *__get_thread_info_57() {
  if (__thread_57 != NULL) return __thread_57;
  __thread_57 = new thread_info(57, check_status_during_io__57);
  __init_thread_info_57(__thread_57);
  return __thread_57;
}

void __declare_sockets_57() {
  init_instance::add_incoming(56,57, DATA_SOCKET);
  init_instance::add_incoming(60,57, DATA_SOCKET);
  init_instance::add_outgoing(57,58, DATA_SOCKET);
}

void __init_sockets_57(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_57() {
}

void __peek_sockets_57() {
}


inline void __push_57_58(float data) {
BUFFER_57_58[HEAD_57_58]=data;
HEAD_57_58++;
}


inline float __pop_56_57() {
float res=BUFFER_56_57[TAIL_56_57];
TAIL_56_57++;
return res;
}

inline float __pop_60_57() {
float res=BUFFER_60_57[TAIL_60_57];
TAIL_60_57++;
return res;
}


void __joiner_57_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_57_58(__pop_56_57());
    __push_57_58(__pop_60_57());
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_58;
int __counter_58 = 0;
int __steady_58 = 0;
int __tmp_58 = 0;
int __tmp2_58 = 0;
int *__state_flag_58 = NULL;
thread_info *__thread_58 = NULL;



void save_peek_buffer__58(object_write_buffer *buf);
void load_peek_buffer__58(object_write_buffer *buf);
void save_file_pointer__58(object_write_buffer *buf);
void load_file_pointer__58(object_write_buffer *buf);

inline void check_status__58() {
  check_thread_status(__state_flag_58, __thread_58);
}

void check_status_during_io__58() {
  check_thread_status_during_io(__state_flag_58, __thread_58);
}

void __init_thread_info_58(thread_info *info) {
  __state_flag_58 = info->get_state_flag();
}

thread_info *__get_thread_info_58() {
  if (__thread_58 != NULL) return __thread_58;
  __thread_58 = new thread_info(58, check_status_during_io__58);
  __init_thread_info_58(__thread_58);
  return __thread_58;
}

void __declare_sockets_58() {
  init_instance::add_incoming(57,58, DATA_SOCKET);
  init_instance::add_outgoing(58,59, DATA_SOCKET);
}

void __init_sockets_58(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_58() {
}

void __peek_sockets_58() {
}

 
void init_Subtracter__511_148__58();
inline void check_status__58();

void work_Subtracter__511_148__58(int);


inline float __pop_57_58() {
float res=BUFFER_57_58[TAIL_57_58];
TAIL_57_58++;
return res;
}

inline void __pop_57_58(int n) {
TAIL_57_58+=n;
}

inline float __peek_57_58(int offs) {
return BUFFER_57_58[TAIL_57_58+offs];
}



inline void __push_58_59(float data) {
BUFFER_58_59[HEAD_58_59]=data;
HEAD_58_59++;
}



 
void init_Subtracter__511_148__58(){
}
void save_file_pointer__58(object_write_buffer *buf) {}
void load_file_pointer__58(object_write_buffer *buf) {}
 
void work_Subtracter__511_148__58(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__508 = 0.0f;/* float */
      float p0__509 = 0.0f;/* float */
      float v__510 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__508 = BUFFER_57_58[TAIL_57_58+1];
;
      p0__509 = BUFFER_57_58[TAIL_57_58+0];
;
      (v__510 = (p1__508 - p0__509))/*float*/;
      __push_58_59(v__510);
      __pop_57_58(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_59;
int __counter_59 = 0;
int __steady_59 = 0;
int __tmp_59 = 0;
int __tmp2_59 = 0;
int *__state_flag_59 = NULL;
thread_info *__thread_59 = NULL;



void save_peek_buffer__59(object_write_buffer *buf);
void load_peek_buffer__59(object_write_buffer *buf);
void save_file_pointer__59(object_write_buffer *buf);
void load_file_pointer__59(object_write_buffer *buf);

inline void check_status__59() {
  check_thread_status(__state_flag_59, __thread_59);
}

void check_status_during_io__59() {
  check_thread_status_during_io(__state_flag_59, __thread_59);
}

void __init_thread_info_59(thread_info *info) {
  __state_flag_59 = info->get_state_flag();
}

thread_info *__get_thread_info_59() {
  if (__thread_59 != NULL) return __thread_59;
  __thread_59 = new thread_info(59, check_status_during_io__59);
  __init_thread_info_59(__thread_59);
  return __thread_59;
}

void __declare_sockets_59() {
  init_instance::add_incoming(58,59, DATA_SOCKET);
  init_instance::add_outgoing(59,9, DATA_SOCKET);
}

void __init_sockets_59(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_59() {
}

void __peek_sockets_59() {
}

 
void init_Amplify__516_149__59();
inline void check_status__59();

void work_Amplify__516_149__59(int);


inline float __pop_58_59() {
float res=BUFFER_58_59[TAIL_58_59];
TAIL_58_59++;
return res;
}

inline void __pop_58_59(int n) {
TAIL_58_59+=n;
}

inline float __peek_58_59(int offs) {
return BUFFER_58_59[TAIL_58_59+offs];
}



inline void __push_59_9(float data) {
BUFFER_59_9[HEAD_59_9]=data;
HEAD_59_9++;
}



 
void init_Amplify__516_149__59(){
}
void save_file_pointer__59(object_write_buffer *buf) {}
void load_file_pointer__59(object_write_buffer *buf) {}
 
void work_Amplify__516_149__59(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__514 = 0.0f;/* float */
      float __tmp52__515 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__515 = BUFFER_58_59[TAIL_58_59]; TAIL_58_59++;
;
      (__tmp51__514 = (__tmp52__515 * ((float)1.3)))/*float*/;
      __push_59_9(__tmp51__514);
      // mark end: SIRFilter Amplify

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_6;
int __counter_6 = 0;
int __steady_6 = 0;
int __tmp_6 = 0;
int __tmp2_6 = 0;
int *__state_flag_6 = NULL;
thread_info *__thread_6 = NULL;



inline void check_status__6() {
  check_thread_status(__state_flag_6, __thread_6);
}

void check_status_during_io__6() {
  check_thread_status_during_io(__state_flag_6, __thread_6);
}

void __init_thread_info_6(thread_info *info) {
  __state_flag_6 = info->get_state_flag();
}

thread_info *__get_thread_info_6() {
  if (__thread_6 != NULL) return __thread_6;
  __thread_6 = new thread_info(6, check_status_during_io__6);
  __init_thread_info_6(__thread_6);
  return __thread_6;
}

void __declare_sockets_6() {
  init_instance::add_incoming(5,6, DATA_SOCKET);
  init_instance::add_incoming(12,6, DATA_SOCKET);
  init_instance::add_outgoing(6,7, DATA_SOCKET);
}

void __init_sockets_6(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_6() {
}

void __peek_sockets_6() {
}


inline void __push_6_7(float data) {
BUFFER_6_7[HEAD_6_7]=data;
HEAD_6_7++;
}


inline float __pop_5_6() {
float res=BUFFER_5_6[TAIL_5_6];
TAIL_5_6++;
return res;
}

inline float __pop_12_6() {
float res=BUFFER_12_6[TAIL_12_6];
TAIL_12_6++;
return res;
}


void __joiner_6_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_6_7(__pop_5_6());
    __push_6_7(__pop_12_6());
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_60;
int __counter_60 = 0;
int __steady_60 = 0;
int __tmp_60 = 0;
int __tmp2_60 = 0;
int *__state_flag_60 = NULL;
thread_info *__thread_60 = NULL;



float coeff__485__60[64] = {0};
void save_peek_buffer__60(object_write_buffer *buf);
void load_peek_buffer__60(object_write_buffer *buf);
void save_file_pointer__60(object_write_buffer *buf);
void load_file_pointer__60(object_write_buffer *buf);

inline void check_status__60() {
  check_thread_status(__state_flag_60, __thread_60);
}

void check_status_during_io__60() {
  check_thread_status_during_io(__state_flag_60, __thread_60);
}

void __init_thread_info_60(thread_info *info) {
  __state_flag_60 = info->get_state_flag();
}

thread_info *__get_thread_info_60() {
  if (__thread_60 != NULL) return __thread_60;
  __thread_60 = new thread_info(60, check_status_during_io__60);
  __init_thread_info_60(__thread_60);
  return __thread_60;
}

void __declare_sockets_60() {
  init_instance::add_incoming(55,60, DATA_SOCKET);
  init_instance::add_outgoing(60,57, DATA_SOCKET);
}

void __init_sockets_60(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_60() {
}

void __peek_sockets_60() {
}

 
void init_LowPassFilter__505_147__60();
inline void check_status__60();

void work_LowPassFilter__505_147__60(int);


inline float __pop_55_60() {
float res=BUFFER_55_60[TAIL_55_60];
TAIL_55_60++;
return res;
}

inline void __pop_55_60(int n) {
TAIL_55_60+=n;
}

inline float __peek_55_60(int offs) {
return BUFFER_55_60[TAIL_55_60+offs];
}



inline void __push_60_57(float data) {
BUFFER_60_57[HEAD_60_57]=data;
HEAD_60_57++;
}



 
void init_LowPassFilter__505_147__60(){
  int i__493 = 0;/* int */
  float __tmp30__494 = 0.0f;/* float */
  float __tmp31__495 = 0.0f;/* float */
  float __tmp32__496 = 0.0f;/* float */
  float __tmp33__497 = 0.0f;/* float */
  double __tmp34__498 = 0.0f;/* double */
  double __tmp35__499 = 0.0f;/* double */
  float __tmp36__500 = 0.0f;/* float */
  float __tmp37__501 = 0.0f;/* float */
  float __tmp38__502 = 0.0f;/* float */
  double __tmp39__503 = 0.0f;/* double */
  double __tmp40__504 = 0.0f;/* double */

  for ((i__493 = 0)/*int*/; (i__493 < 64); (i__493++)) {if (((((float)(i__493)) - ((float)31.5)) == ((float)0.0))) {(((coeff__485__60)[(int)i__493]) = ((float)9.956063E-6))/*float*/; } else {{
      (__tmp35__499 = ((double)((((float)3.1277894E-5) * (((float)(i__493)) - ((float)31.5))))))/*double*/;
      (__tmp34__498 = sinf(__tmp35__499))/*double*/;
      (__tmp33__497 = ((float)(__tmp34__498)))/*float*/;
      (__tmp32__496 = (__tmp33__497 / ((float)3.1415927)))/*float*/;
      (__tmp31__495 = (__tmp32__496 / (((float)(i__493)) - ((float)31.5))))/*float*/;
      (__tmp40__504 = ((double)(((((float)6.2831855) * ((float)(i__493))) / ((float)63.0)))))/*double*/;
      (__tmp39__503 = cosf(__tmp40__504))/*double*/;
      (__tmp38__502 = ((float)(__tmp39__503)))/*float*/;
      (__tmp37__501 = (((float)0.46) * __tmp38__502))/*float*/;
      (__tmp36__500 = (((float)0.54) - __tmp37__501))/*float*/;
      (__tmp30__494 = (__tmp31__495 * __tmp36__500))/*float*/;
      (((coeff__485__60)[(int)i__493]) = __tmp30__494)/*float*/;
    }}
  }
}
void save_file_pointer__60(object_write_buffer *buf) {}
void load_file_pointer__60(object_write_buffer *buf) {}
 
void work_LowPassFilter__505_147__60(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__488 = 0.0f;/* float */
      float __tmp49__489 = 0.0f;/* float */
      float __tmp50__490 = 0.0f;/* float */
      int i__conflict__0__491 = 0;/* int */
      int i__492 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__488 = ((float)0.0))/*float*/;
      for ((i__conflict__0__491 = 0)/*int*/; (i__conflict__0__491 < 64); (i__conflict__0__491++)) {{
          __peek_55_60(i__conflict__0__491);
          __tmp50__490 = BUFFER_55_60[TAIL_55_60+i__conflict__0__491];
;
          (__tmp49__489 = (__tmp50__490 * ((coeff__485__60)[(int)i__conflict__0__491])))/*float*/;
          (sum__488 = (sum__488 + __tmp49__489))/*float*/;
        }
      }
      __push_60_57(sum__488);
      for ((i__492 = 0)/*int*/; (i__492 < 0); (i__492++)) {__pop_55_60();
      }
      __pop_55_60();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 63 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_61;
int __counter_61 = 0;
int __steady_61 = 0;
int __tmp_61 = 0;
int __tmp2_61 = 0;
int *__state_flag_61 = NULL;
thread_info *__thread_61 = NULL;



inline void check_status__61() {
  check_thread_status(__state_flag_61, __thread_61);
}

void check_status_during_io__61() {
  check_thread_status_during_io(__state_flag_61, __thread_61);
}

void __init_thread_info_61(thread_info *info) {
  __state_flag_61 = info->get_state_flag();
}

thread_info *__get_thread_info_61() {
  if (__thread_61 != NULL) return __thread_61;
  __thread_61 = new thread_info(61, check_status_during_io__61);
  __init_thread_info_61(__thread_61);
  return __thread_61;
}

void __declare_sockets_61() {
  init_instance::add_incoming(3,61, DATA_SOCKET);
  init_instance::add_outgoing(61,62, DATA_SOCKET);
  init_instance::add_outgoing(61,66, DATA_SOCKET);
}

void __init_sockets_61(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_61() {
}

void __peek_sockets_61() {
}

inline float __pop_3_61() {
float res=BUFFER_3_61[TAIL_3_61];
TAIL_3_61++;
return res;
}


inline void __push_61_62(float data) {
BUFFER_61_62[HEAD_61_62]=data;
HEAD_61_62++;
}



inline void __push_61_66(float data) {
BUFFER_61_66[HEAD_61_66]=data;
HEAD_61_66++;
}



void __splitter_61_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_3_61[TAIL_3_61]; TAIL_3_61++;
;
  __push_61_62(tmp);
  __push_61_66(tmp);
  }
}


// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_62;
int __counter_62 = 0;
int __steady_62 = 0;
int __tmp_62 = 0;
int __tmp2_62 = 0;
int *__state_flag_62 = NULL;
thread_info *__thread_62 = NULL;



float coeff__517__62[64] = {0};
void save_peek_buffer__62(object_write_buffer *buf);
void load_peek_buffer__62(object_write_buffer *buf);
void save_file_pointer__62(object_write_buffer *buf);
void load_file_pointer__62(object_write_buffer *buf);

inline void check_status__62() {
  check_thread_status(__state_flag_62, __thread_62);
}

void check_status_during_io__62() {
  check_thread_status_during_io(__state_flag_62, __thread_62);
}

void __init_thread_info_62(thread_info *info) {
  __state_flag_62 = info->get_state_flag();
}

thread_info *__get_thread_info_62() {
  if (__thread_62 != NULL) return __thread_62;
  __thread_62 = new thread_info(62, check_status_during_io__62);
  __init_thread_info_62(__thread_62);
  return __thread_62;
}

void __declare_sockets_62() {
  init_instance::add_incoming(61,62, DATA_SOCKET);
  init_instance::add_outgoing(62,63, DATA_SOCKET);
}

void __init_sockets_62(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_62() {
}

void __peek_sockets_62() {
}

 
void init_LowPassFilter__537_153__62();
inline void check_status__62();

void work_LowPassFilter__537_153__62(int);


inline float __pop_61_62() {
float res=BUFFER_61_62[TAIL_61_62];
TAIL_61_62++;
return res;
}

inline void __pop_61_62(int n) {
TAIL_61_62+=n;
}

inline float __peek_61_62(int offs) {
return BUFFER_61_62[TAIL_61_62+offs];
}



inline void __push_62_63(float data) {
BUFFER_62_63[HEAD_62_63]=data;
HEAD_62_63++;
}



 
void init_LowPassFilter__537_153__62(){
  int i__525 = 0;/* int */
  float __tmp19__526 = 0.0f;/* float */
  float __tmp20__527 = 0.0f;/* float */
  float __tmp21__528 = 0.0f;/* float */
  float __tmp22__529 = 0.0f;/* float */
  double __tmp23__530 = 0.0f;/* double */
  double __tmp24__531 = 0.0f;/* double */
  float __tmp25__532 = 0.0f;/* float */
  float __tmp26__533 = 0.0f;/* float */
  float __tmp27__534 = 0.0f;/* float */
  double __tmp28__535 = 0.0f;/* double */
  double __tmp29__536 = 0.0f;/* double */

  for ((i__525 = 0)/*int*/; (i__525 < 64); (i__525++)) {if (((((float)(i__525)) - ((float)31.5)) == ((float)0.0))) {(((coeff__517__62)[(int)i__525]) = ((float)9.956063E-6))/*float*/; } else {{
      (__tmp24__531 = ((double)((((float)3.1277894E-5) * (((float)(i__525)) - ((float)31.5))))))/*double*/;
      (__tmp23__530 = sinf(__tmp24__531))/*double*/;
      (__tmp22__529 = ((float)(__tmp23__530)))/*float*/;
      (__tmp21__528 = (__tmp22__529 / ((float)3.1415927)))/*float*/;
      (__tmp20__527 = (__tmp21__528 / (((float)(i__525)) - ((float)31.5))))/*float*/;
      (__tmp29__536 = ((double)(((((float)6.2831855) * ((float)(i__525))) / ((float)63.0)))))/*double*/;
      (__tmp28__535 = cosf(__tmp29__536))/*double*/;
      (__tmp27__534 = ((float)(__tmp28__535)))/*float*/;
      (__tmp26__533 = (((float)0.46) * __tmp27__534))/*float*/;
      (__tmp25__532 = (((float)0.54) - __tmp26__533))/*float*/;
      (__tmp19__526 = (__tmp20__527 * __tmp25__532))/*float*/;
      (((coeff__517__62)[(int)i__525]) = __tmp19__526)/*float*/;
    }}
  }
}
void save_file_pointer__62(object_write_buffer *buf) {}
void load_file_pointer__62(object_write_buffer *buf) {}
 
void work_LowPassFilter__537_153__62(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__520 = 0.0f;/* float */
      float __tmp47__521 = 0.0f;/* float */
      float __tmp48__522 = 0.0f;/* float */
      int i__conflict__0__523 = 0;/* int */
      int i__524 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__520 = ((float)0.0))/*float*/;
      for ((i__conflict__0__523 = 0)/*int*/; (i__conflict__0__523 < 64); (i__conflict__0__523++)) {{
          __peek_61_62(i__conflict__0__523);
          __tmp48__522 = BUFFER_61_62[TAIL_61_62+i__conflict__0__523];
;
          (__tmp47__521 = (__tmp48__522 * ((coeff__517__62)[(int)i__conflict__0__523])))/*float*/;
          (sum__520 = (sum__520 + __tmp47__521))/*float*/;
        }
      }
      __push_62_63(sum__520);
      for ((i__524 = 0)/*int*/; (i__524 < 0); (i__524++)) {__pop_61_62();
      }
      __pop_61_62();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_63;
int __counter_63 = 0;
int __steady_63 = 0;
int __tmp_63 = 0;
int __tmp2_63 = 0;
int *__state_flag_63 = NULL;
thread_info *__thread_63 = NULL;



inline void check_status__63() {
  check_thread_status(__state_flag_63, __thread_63);
}

void check_status_during_io__63() {
  check_thread_status_during_io(__state_flag_63, __thread_63);
}

void __init_thread_info_63(thread_info *info) {
  __state_flag_63 = info->get_state_flag();
}

thread_info *__get_thread_info_63() {
  if (__thread_63 != NULL) return __thread_63;
  __thread_63 = new thread_info(63, check_status_during_io__63);
  __init_thread_info_63(__thread_63);
  return __thread_63;
}

void __declare_sockets_63() {
  init_instance::add_incoming(62,63, DATA_SOCKET);
  init_instance::add_incoming(66,63, DATA_SOCKET);
  init_instance::add_outgoing(63,64, DATA_SOCKET);
}

void __init_sockets_63(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_63() {
}

void __peek_sockets_63() {
}


inline void __push_63_64(float data) {
BUFFER_63_64[HEAD_63_64]=data;
HEAD_63_64++;
}


inline float __pop_62_63() {
float res=BUFFER_62_63[TAIL_62_63];
TAIL_62_63++;
return res;
}

inline float __pop_66_63() {
float res=BUFFER_66_63[TAIL_66_63];
TAIL_66_63++;
return res;
}


void __joiner_63_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_63_64(__pop_62_63());
    __push_63_64(__pop_66_63());
  }
}


// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_64;
int __counter_64 = 0;
int __steady_64 = 0;
int __tmp_64 = 0;
int __tmp2_64 = 0;
int *__state_flag_64 = NULL;
thread_info *__thread_64 = NULL;



void save_peek_buffer__64(object_write_buffer *buf);
void load_peek_buffer__64(object_write_buffer *buf);
void save_file_pointer__64(object_write_buffer *buf);
void load_file_pointer__64(object_write_buffer *buf);

inline void check_status__64() {
  check_thread_status(__state_flag_64, __thread_64);
}

void check_status_during_io__64() {
  check_thread_status_during_io(__state_flag_64, __thread_64);
}

void __init_thread_info_64(thread_info *info) {
  __state_flag_64 = info->get_state_flag();
}

thread_info *__get_thread_info_64() {
  if (__thread_64 != NULL) return __thread_64;
  __thread_64 = new thread_info(64, check_status_during_io__64);
  __init_thread_info_64(__thread_64);
  return __thread_64;
}

void __declare_sockets_64() {
  init_instance::add_incoming(63,64, DATA_SOCKET);
  init_instance::add_outgoing(64,65, DATA_SOCKET);
}

void __init_sockets_64(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_64() {
}

void __peek_sockets_64() {
}

 
void init_Subtracter__564_155__64();
inline void check_status__64();

void work_Subtracter__564_155__64(int);


inline float __pop_63_64() {
float res=BUFFER_63_64[TAIL_63_64];
TAIL_63_64++;
return res;
}

inline void __pop_63_64(int n) {
TAIL_63_64+=n;
}

inline float __peek_63_64(int offs) {
return BUFFER_63_64[TAIL_63_64+offs];
}



inline void __push_64_65(float data) {
BUFFER_64_65[HEAD_64_65]=data;
HEAD_64_65++;
}



 
void init_Subtracter__564_155__64(){
}
void save_file_pointer__64(object_write_buffer *buf) {}
void load_file_pointer__64(object_write_buffer *buf) {}
 
void work_Subtracter__564_155__64(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__561 = 0.0f;/* float */
      float p0__562 = 0.0f;/* float */
      float v__563 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__561 = BUFFER_63_64[TAIL_63_64+1];
;
      p0__562 = BUFFER_63_64[TAIL_63_64+0];
;
      (v__563 = (p1__561 - p0__562))/*float*/;
      __push_64_65(v__563);
      __pop_63_64(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_65;
int __counter_65 = 0;
int __steady_65 = 0;
int __tmp_65 = 0;
int __tmp2_65 = 0;
int *__state_flag_65 = NULL;
thread_info *__thread_65 = NULL;



void save_peek_buffer__65(object_write_buffer *buf);
void load_peek_buffer__65(object_write_buffer *buf);
void save_file_pointer__65(object_write_buffer *buf);
void load_file_pointer__65(object_write_buffer *buf);

inline void check_status__65() {
  check_thread_status(__state_flag_65, __thread_65);
}

void check_status_during_io__65() {
  check_thread_status_during_io(__state_flag_65, __thread_65);
}

void __init_thread_info_65(thread_info *info) {
  __state_flag_65 = info->get_state_flag();
}

thread_info *__get_thread_info_65() {
  if (__thread_65 != NULL) return __thread_65;
  __thread_65 = new thread_info(65, check_status_during_io__65);
  __init_thread_info_65(__thread_65);
  return __thread_65;
}

void __declare_sockets_65() {
  init_instance::add_incoming(64,65, DATA_SOCKET);
  init_instance::add_outgoing(65,9, DATA_SOCKET);
}

void __init_sockets_65(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_65() {
}

void __peek_sockets_65() {
}

 
void init_Amplify__569_156__65();
inline void check_status__65();

void work_Amplify__569_156__65(int);


inline float __pop_64_65() {
float res=BUFFER_64_65[TAIL_64_65];
TAIL_64_65++;
return res;
}

inline void __pop_64_65(int n) {
TAIL_64_65+=n;
}

inline float __peek_64_65(int offs) {
return BUFFER_64_65[TAIL_64_65+offs];
}



inline void __push_65_9(float data) {
BUFFER_65_9[HEAD_65_9]=data;
HEAD_65_9++;
}



 
void init_Amplify__569_156__65(){
}
void save_file_pointer__65(object_write_buffer *buf) {}
void load_file_pointer__65(object_write_buffer *buf) {}
 
void work_Amplify__569_156__65(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__567 = 0.0f;/* float */
      float __tmp52__568 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__568 = BUFFER_64_65[TAIL_64_65]; TAIL_64_65++;
;
      (__tmp51__567 = (__tmp52__568 * ((float)1.1)))/*float*/;
      __push_65_9(__tmp51__567);
      // mark end: SIRFilter Amplify

    }
  }
}

// peek: 64 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_66;
int __counter_66 = 0;
int __steady_66 = 0;
int __tmp_66 = 0;
int __tmp2_66 = 0;
int *__state_flag_66 = NULL;
thread_info *__thread_66 = NULL;



float coeff__538__66[64] = {0};
void save_peek_buffer__66(object_write_buffer *buf);
void load_peek_buffer__66(object_write_buffer *buf);
void save_file_pointer__66(object_write_buffer *buf);
void load_file_pointer__66(object_write_buffer *buf);

inline void check_status__66() {
  check_thread_status(__state_flag_66, __thread_66);
}

void check_status_during_io__66() {
  check_thread_status_during_io(__state_flag_66, __thread_66);
}

void __init_thread_info_66(thread_info *info) {
  __state_flag_66 = info->get_state_flag();
}

thread_info *__get_thread_info_66() {
  if (__thread_66 != NULL) return __thread_66;
  __thread_66 = new thread_info(66, check_status_during_io__66);
  __init_thread_info_66(__thread_66);
  return __thread_66;
}

void __declare_sockets_66() {
  init_instance::add_incoming(61,66, DATA_SOCKET);
  init_instance::add_outgoing(66,63, DATA_SOCKET);
}

void __init_sockets_66(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_66() {
}

void __peek_sockets_66() {
}

 
void init_LowPassFilter__558_154__66();
inline void check_status__66();

void work_LowPassFilter__558_154__66(int);


inline float __pop_61_66() {
float res=BUFFER_61_66[TAIL_61_66];
TAIL_61_66++;
return res;
}

inline void __pop_61_66(int n) {
TAIL_61_66+=n;
}

inline float __peek_61_66(int offs) {
return BUFFER_61_66[TAIL_61_66+offs];
}



inline void __push_66_63(float data) {
BUFFER_66_63[HEAD_66_63]=data;
HEAD_66_63++;
}



 
void init_LowPassFilter__558_154__66(){
  int i__546 = 0;/* int */
  float __tmp30__547 = 0.0f;/* float */
  float __tmp31__548 = 0.0f;/* float */
  float __tmp32__549 = 0.0f;/* float */
  float __tmp33__550 = 0.0f;/* float */
  double __tmp34__551 = 0.0f;/* double */
  double __tmp35__552 = 0.0f;/* double */
  float __tmp36__553 = 0.0f;/* float */
  float __tmp37__554 = 0.0f;/* float */
  float __tmp38__555 = 0.0f;/* float */
  double __tmp39__556 = 0.0f;/* double */
  double __tmp40__557 = 0.0f;/* double */

  for ((i__546 = 0)/*int*/; (i__546 < 64); (i__546++)) {if (((((float)(i__546)) - ((float)31.5)) == ((float)0.0))) {(((coeff__538__66)[(int)i__546]) = ((float)1.408E-5))/*float*/; } else {{
      (__tmp35__552 = ((double)((((float)4.4233628E-5) * (((float)(i__546)) - ((float)31.5))))))/*double*/;
      (__tmp34__551 = sinf(__tmp35__552))/*double*/;
      (__tmp33__550 = ((float)(__tmp34__551)))/*float*/;
      (__tmp32__549 = (__tmp33__550 / ((float)3.1415927)))/*float*/;
      (__tmp31__548 = (__tmp32__549 / (((float)(i__546)) - ((float)31.5))))/*float*/;
      (__tmp40__557 = ((double)(((((float)6.2831855) * ((float)(i__546))) / ((float)63.0)))))/*double*/;
      (__tmp39__556 = cosf(__tmp40__557))/*double*/;
      (__tmp38__555 = ((float)(__tmp39__556)))/*float*/;
      (__tmp37__554 = (((float)0.46) * __tmp38__555))/*float*/;
      (__tmp36__553 = (((float)0.54) - __tmp37__554))/*float*/;
      (__tmp30__547 = (__tmp31__548 * __tmp36__553))/*float*/;
      (((coeff__538__66)[(int)i__546]) = __tmp30__547)/*float*/;
    }}
  }
}
void save_file_pointer__66(object_write_buffer *buf) {}
void load_file_pointer__66(object_write_buffer *buf) {}
 
void work_LowPassFilter__558_154__66(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__541 = 0.0f;/* float */
      float __tmp49__542 = 0.0f;/* float */
      float __tmp50__543 = 0.0f;/* float */
      int i__conflict__0__544 = 0;/* int */
      int i__545 = 0;/* int */

      // mark begin: SIRFilter LowPassFilter

      (sum__541 = ((float)0.0))/*float*/;
      for ((i__conflict__0__544 = 0)/*int*/; (i__conflict__0__544 < 64); (i__conflict__0__544++)) {{
          __peek_61_66(i__conflict__0__544);
          __tmp50__543 = BUFFER_61_66[TAIL_61_66+i__conflict__0__544];
;
          (__tmp49__542 = (__tmp50__543 * ((coeff__538__66)[(int)i__conflict__0__544])))/*float*/;
          (sum__541 = (sum__541 + __tmp49__542))/*float*/;
        }
      }
      __push_66_63(sum__541);
      for ((i__545 = 0)/*int*/; (i__545 < 0); (i__545++)) {__pop_61_66();
      }
      __pop_61_66();
      // mark end: SIRFilter LowPassFilter

    }
  }
}

// peek: 2 pop: 2 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_7;
int __counter_7 = 0;
int __steady_7 = 0;
int __tmp_7 = 0;
int __tmp2_7 = 0;
int *__state_flag_7 = NULL;
thread_info *__thread_7 = NULL;



void save_peek_buffer__7(object_write_buffer *buf);
void load_peek_buffer__7(object_write_buffer *buf);
void save_file_pointer__7(object_write_buffer *buf);
void load_file_pointer__7(object_write_buffer *buf);

inline void check_status__7() {
  check_thread_status(__state_flag_7, __thread_7);
}

void check_status_during_io__7() {
  check_thread_status_during_io(__state_flag_7, __thread_7);
}

void __init_thread_info_7(thread_info *info) {
  __state_flag_7 = info->get_state_flag();
}

thread_info *__get_thread_info_7() {
  if (__thread_7 != NULL) return __thread_7;
  __thread_7 = new thread_info(7, check_status_during_io__7);
  __init_thread_info_7(__thread_7);
  return __thread_7;
}

void __declare_sockets_7() {
  init_instance::add_incoming(6,7, DATA_SOCKET);
  init_instance::add_outgoing(7,8, DATA_SOCKET);
}

void __init_sockets_7(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_7() {
}

void __peek_sockets_7() {
}

 
void init_Subtracter__87_92__7();
inline void check_status__7();

void work_Subtracter__87_92__7(int);


inline float __pop_6_7() {
float res=BUFFER_6_7[TAIL_6_7];
TAIL_6_7++;
return res;
}

inline void __pop_6_7(int n) {
TAIL_6_7+=n;
}

inline float __peek_6_7(int offs) {
return BUFFER_6_7[TAIL_6_7+offs];
}



inline void __push_7_8(float data) {
BUFFER_7_8[HEAD_7_8]=data;
HEAD_7_8++;
}



 
void init_Subtracter__87_92__7(){
}
void save_file_pointer__7(object_write_buffer *buf) {}
void load_file_pointer__7(object_write_buffer *buf) {}
 
void work_Subtracter__87_92__7(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float p1__84 = 0.0f;/* float */
      float p0__85 = 0.0f;/* float */
      float v__86 = 0.0f;/* float */

      // mark begin: SIRFilter Subtracter

      p1__84 = BUFFER_6_7[TAIL_6_7+1];
;
      p0__85 = BUFFER_6_7[TAIL_6_7+0];
;
      (v__86 = (p1__84 - p0__85))/*float*/;
      __push_7_8(v__86);
      __pop_6_7(2);
      // mark end: SIRFilter Subtracter

    }
  }
}

// peek: 1 pop: 1 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_8;
int __counter_8 = 0;
int __steady_8 = 0;
int __tmp_8 = 0;
int __tmp2_8 = 0;
int *__state_flag_8 = NULL;
thread_info *__thread_8 = NULL;



void save_peek_buffer__8(object_write_buffer *buf);
void load_peek_buffer__8(object_write_buffer *buf);
void save_file_pointer__8(object_write_buffer *buf);
void load_file_pointer__8(object_write_buffer *buf);

inline void check_status__8() {
  check_thread_status(__state_flag_8, __thread_8);
}

void check_status_during_io__8() {
  check_thread_status_during_io(__state_flag_8, __thread_8);
}

void __init_thread_info_8(thread_info *info) {
  __state_flag_8 = info->get_state_flag();
}

thread_info *__get_thread_info_8() {
  if (__thread_8 != NULL) return __thread_8;
  __thread_8 = new thread_info(8, check_status_during_io__8);
  __init_thread_info_8(__thread_8);
  return __thread_8;
}

void __declare_sockets_8() {
  init_instance::add_incoming(7,8, DATA_SOCKET);
  init_instance::add_outgoing(8,9, DATA_SOCKET);
}

void __init_sockets_8(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_8() {
}

void __peek_sockets_8() {
}

 
void init_Amplify__92_93__8();
inline void check_status__8();

void work_Amplify__92_93__8(int);


inline float __pop_7_8() {
float res=BUFFER_7_8[TAIL_7_8];
TAIL_7_8++;
return res;
}

inline void __pop_7_8(int n) {
TAIL_7_8+=n;
}

inline float __peek_7_8(int offs) {
return BUFFER_7_8[TAIL_7_8+offs];
}



inline void __push_8_9(float data) {
BUFFER_8_9[HEAD_8_9]=data;
HEAD_8_9++;
}



 
void init_Amplify__92_93__8(){
}
void save_file_pointer__8(object_write_buffer *buf) {}
void load_file_pointer__8(object_write_buffer *buf) {}
 
void work_Amplify__92_93__8(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp51__90 = 0.0f;/* float */
      float __tmp52__91 = 0.0f;/* float */

      // mark begin: SIRFilter Amplify

      __tmp52__91 = BUFFER_7_8[TAIL_7_8]; TAIL_7_8++;
;
      (__tmp51__90 = (__tmp52__91 * ((float)1.1)))/*float*/;
      __push_8_9(__tmp51__90);
      // mark end: SIRFilter Amplify

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_9;
int __counter_9 = 0;
int __steady_9 = 0;
int __tmp_9 = 0;
int __tmp2_9 = 0;
int *__state_flag_9 = NULL;
thread_info *__thread_9 = NULL;



inline void check_status__9() {
  check_thread_status(__state_flag_9, __thread_9);
}

void check_status_during_io__9() {
  check_thread_status_during_io(__state_flag_9, __thread_9);
}

void __init_thread_info_9(thread_info *info) {
  __state_flag_9 = info->get_state_flag();
}

thread_info *__get_thread_info_9() {
  if (__thread_9 != NULL) return __thread_9;
  __thread_9 = new thread_info(9, check_status_during_io__9);
  __init_thread_info_9(__thread_9);
  return __thread_9;
}

void __declare_sockets_9() {
  init_instance::add_incoming(8,9, DATA_SOCKET);
  init_instance::add_incoming(17,9, DATA_SOCKET);
  init_instance::add_incoming(23,9, DATA_SOCKET);
  init_instance::add_incoming(29,9, DATA_SOCKET);
  init_instance::add_incoming(35,9, DATA_SOCKET);
  init_instance::add_incoming(41,9, DATA_SOCKET);
  init_instance::add_incoming(47,9, DATA_SOCKET);
  init_instance::add_incoming(53,9, DATA_SOCKET);
  init_instance::add_incoming(59,9, DATA_SOCKET);
  init_instance::add_incoming(65,9, DATA_SOCKET);
  init_instance::add_outgoing(9,10, DATA_SOCKET);
}

void __init_sockets_9(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_9() {
}

void __peek_sockets_9() {
}


inline void __push_9_10(float data) {
BUFFER_9_10[HEAD_9_10]=data;
HEAD_9_10++;
}


inline float __pop_8_9() {
float res=BUFFER_8_9[TAIL_8_9];
TAIL_8_9++;
return res;
}

inline float __pop_17_9() {
float res=BUFFER_17_9[TAIL_17_9];
TAIL_17_9++;
return res;
}

inline float __pop_23_9() {
float res=BUFFER_23_9[TAIL_23_9];
TAIL_23_9++;
return res;
}

inline float __pop_29_9() {
float res=BUFFER_29_9[TAIL_29_9];
TAIL_29_9++;
return res;
}

inline float __pop_35_9() {
float res=BUFFER_35_9[TAIL_35_9];
TAIL_35_9++;
return res;
}

inline float __pop_41_9() {
float res=BUFFER_41_9[TAIL_41_9];
TAIL_41_9++;
return res;
}

inline float __pop_47_9() {
float res=BUFFER_47_9[TAIL_47_9];
TAIL_47_9++;
return res;
}

inline float __pop_53_9() {
float res=BUFFER_53_9[TAIL_53_9];
TAIL_53_9++;
return res;
}

inline float __pop_59_9() {
float res=BUFFER_59_9[TAIL_59_9];
TAIL_59_9++;
return res;
}

inline float __pop_65_9() {
float res=BUFFER_65_9[TAIL_65_9];
TAIL_65_9++;
return res;
}


void __joiner_9_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_9_10(__pop_8_9());
    __push_9_10(__pop_17_9());
    __push_9_10(__pop_23_9());
    __push_9_10(__pop_29_9());
    __push_9_10(__pop_35_9());
    __push_9_10(__pop_41_9());
    __push_9_10(__pop_47_9());
    __push_9_10(__pop_53_9());
    __push_9_10(__pop_59_9());
    __push_9_10(__pop_65_9());
  }
}


