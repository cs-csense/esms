#ifndef __FUSION_H
#define __FUSION_H

#define max(A,B) (((A)>(B))?(A):(B))
#define pow2ceil(A) ((A<=2)?(2): (((A<=4)?(4): (((A<=8)?(8): (((A<=16)?(16): (((A<=32)?(32): (((A<=64)?(64): (((A<=128)?(128): (((A<=256)?(256):(((A<=1024)?(1024):(((A<=4096)?(4096):(((A<=16384)?(16384):(((A<=65536)?(65536):(((A<=131072)?(131072):(((A<=262144)?(262144):(((A<=524288)?(524288):(((A<=1048576)?(1048576):(((A<=2097152)?(2097152):(((A<=4194304)?(4194304):(((A<=8388608)?(8388608):(((A<=16777216)?(16777216):(((A<=33554432)?(33554432):(((A<=67108864)?(67108864):(((A<=134217728)?(134217728):(((A<=268435456)?(268435456):(((A<=536870912)?(536870912):(1073741824))))))))))))))))))))))))))))))))))))))))))))))))))
//destination peeks: 59 extra items
#define __PEEK_BUF_SIZE_0_1 59
#define __BUF_SIZE_MASK_0_1 (pow2ceil(379+59)-1)

//destination peeks: 1 extra items
#define __PEEK_BUF_SIZE_1_2 1
#define __BUF_SIZE_MASK_1_2 (pow2ceil(64+1)-1)

#define __BUF_SIZE_MASK_2_3 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_4 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_13 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_19 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_25 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_31 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_37 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_43 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_49 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_55 (pow2ceil(63+0)-1)

#define __BUF_SIZE_MASK_3_61 (pow2ceil(63+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_4_5 63
#define __BUF_SIZE_MASK_4_5 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_4_12 63
#define __BUF_SIZE_MASK_4_12 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_5_6 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_6_7 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_7_8 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_8_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_9_10 (pow2ceil(10+0)-1)

#define __BUF_SIZE_MASK_10_11 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_12_6 (pow2ceil(1+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_13_14 63
#define __BUF_SIZE_MASK_13_14 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_13_18 63
#define __BUF_SIZE_MASK_13_18 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_14_15 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_15_16 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_16_17 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_17_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_18_15 (pow2ceil(1+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_19_20 63
#define __BUF_SIZE_MASK_19_20 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_19_24 63
#define __BUF_SIZE_MASK_19_24 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_20_21 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_21_22 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_22_23 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_23_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_24_21 (pow2ceil(1+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_25_26 63
#define __BUF_SIZE_MASK_25_26 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_25_30 63
#define __BUF_SIZE_MASK_25_30 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_26_27 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_27_28 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_28_29 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_29_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_30_27 (pow2ceil(1+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_31_32 63
#define __BUF_SIZE_MASK_31_32 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_31_36 63
#define __BUF_SIZE_MASK_31_36 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_32_33 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_33_34 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_34_35 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_35_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_36_33 (pow2ceil(1+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_37_38 63
#define __BUF_SIZE_MASK_37_38 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_37_42 63
#define __BUF_SIZE_MASK_37_42 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_38_39 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_39_40 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_40_41 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_41_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_42_39 (pow2ceil(1+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_43_44 63
#define __BUF_SIZE_MASK_43_44 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_43_48 63
#define __BUF_SIZE_MASK_43_48 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_44_45 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_45_46 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_46_47 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_47_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_48_45 (pow2ceil(1+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_49_50 63
#define __BUF_SIZE_MASK_49_50 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_49_54 63
#define __BUF_SIZE_MASK_49_54 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_50_51 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_51_52 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_52_53 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_53_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_54_51 (pow2ceil(1+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_55_56 63
#define __BUF_SIZE_MASK_55_56 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_55_60 63
#define __BUF_SIZE_MASK_55_60 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_56_57 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_57_58 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_58_59 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_59_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_60_57 (pow2ceil(1+0)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_61_62 63
#define __BUF_SIZE_MASK_61_62 (pow2ceil(64+63)-1)

//destination peeks: 63 extra items
#define __PEEK_BUF_SIZE_61_66 63
#define __BUF_SIZE_MASK_61_66 (pow2ceil(64+63)-1)

#define __BUF_SIZE_MASK_62_63 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_63_64 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_64_65 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_65_9 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_66_63 (pow2ceil(1+0)-1)

#endif
