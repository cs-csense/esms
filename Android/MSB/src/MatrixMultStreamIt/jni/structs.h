#ifndef __STRUCTS_H
#define __STRUCTS_H
typedef struct ___Complex {
	float real;
	float imag;
} Complex;
typedef struct ___float2 {
	float x;
	float y;
} float2;
typedef struct ___float3 {
	float x;
	float y;
	float z;
} float3;
typedef struct ___float4 {
	float x;
	float y;
	float z;
	float w;
} float4;
typedef int bit;
#ifndef round
#define round(x) (floor((x)+0.5))
#endif
#endif // __STRUCTS_H
