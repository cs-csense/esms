#ifndef __FUSION_H
#define __FUSION_H

#define max(A,B) (((A)>(B))?(A):(B))
#define pow2ceil(A) ((A<=2)?(2): (((A<=4)?(4): (((A<=8)?(8): (((A<=16)?(16): (((A<=32)?(32): (((A<=64)?(64): (((A<=128)?(128): (((A<=256)?(256):(((A<=1024)?(1024):(((A<=4096)?(4096):(((A<=16384)?(16384):(((A<=65536)?(65536):(((A<=131072)?(131072):(((A<=262144)?(262144):(((A<=524288)?(524288):(((A<=1048576)?(1048576):(((A<=2097152)?(2097152):(((A<=4194304)?(4194304):(((A<=8388608)?(8388608):(((A<=16777216)?(16777216):(((A<=33554432)?(33554432):(((A<=67108864)?(67108864):(((A<=134217728)?(134217728):(((A<=268435456)?(268435456):(((A<=536870912)?(536870912):(1073741824))))))))))))))))))))))))))))))))))))))))))))))))))
#define __BUF_SIZE_MASK_0_1 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_1_2 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_1_286 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_1_290 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_1_294 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_1_298 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_1_302 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_1_306 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_1_310 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_2_3 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_2_285 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_3_4 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_4_5 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_5_6 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_6_7 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_6_264 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_6_267 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_6_270 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_6_273 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_6_276 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_6_279 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_6_282 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_7_8 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_8_9 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_9_10 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_10_11 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_11_12 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_11_234 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_11_244 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_11_254 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_12_13 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_12_230 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_13_14 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_13_229 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_14_15 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_15_16 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_16_17 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_17_18 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_18_19 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_18_196 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_18_207 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_18_218 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_19_20 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_20_21 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_21_22 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_22_23 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_22_193 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_23_24 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_24_25 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_25_26 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_26_27 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_27_28 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_28_29 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_28_179 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_29_30 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_29_173 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_30_31 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_30_170 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_30_171 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_30_172 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_31_32 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_32_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_33_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_34_35 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_35_36 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_35_159 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_36_37 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_37_38 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_38_39 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_39_40 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_39_156 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_40_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_41_42 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_42_43 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_43_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_44_45 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_45_46 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_45_142 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_46_47 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_46_133 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_46_136 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_46_139 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_47_48 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_48_49 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_49_50 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_50_51 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_51_52 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_52_53 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_52_123 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_53_54 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_53_116 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_53_117 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_53_118 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_53_119 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_53_120 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_53_121 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_53_122 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_54_55 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_55_56 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_56_57 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_57_58 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_58_59 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_59_60 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_60_61 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_60_105 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_61_62 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_62_63 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_63_64 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_64_65 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_64_102 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_65_66 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_66_67 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_67_68 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_68_69 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_69_70 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_70_71 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_70_88 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_71_72 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_71_79 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_71_82 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_71_85 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_72_73 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_74 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_74_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_75_76 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_76_77 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_77_78 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_79_80 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_80_81 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_81_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_82_83 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_83_84 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_84_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_85_86 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_86_87 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_87_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_88_89 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_88_93 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_88_96 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_88_99 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_89_90 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_90_91 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_91_92 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_92_76 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_93_94 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_94_95 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_95_92 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_96_97 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_97_98 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_98_92 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_99_100 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_100_101 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_101_92 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_102_103 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_103_104 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_104_68 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_105_106 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_106_107 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_107_108 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_108_109 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_108_113 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_109_110 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_110_111 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_111_112 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_112_69 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_113_114 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_114_115 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_115_112 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_116_55 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_117_55 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_118_55 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_119_55 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_120_55 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_121_55 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_122_55 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_123_124 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_123_126 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_123_127 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_123_128 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_123_129 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_123_130 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_123_131 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_123_132 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_124_125 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_125_56 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_126_125 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_127_125 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_128_125 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_129_125 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_130_125 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_131_125 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_132_125 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_133_134 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_134_135 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_135_50 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_136_137 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_137_138 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_138_50 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_139_140 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_140_141 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_141_50 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_142_143 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_142_147 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_142_150 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_142_153 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_143_144 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_144_145 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_145_146 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_146_51 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_147_148 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_148_149 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_149_146 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_150_151 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_151_152 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_152_146 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_153_154 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_154_155 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_155_146 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_156_157 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_157_158 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_158_43 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_159_160 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_160_161 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_161_162 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_162_163 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_162_167 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_163_164 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_164_165 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_165_166 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_166_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_167_168 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_168_169 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_169_166 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_170_32 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_171_32 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_172_32 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_173_174 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_173_176 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_173_177 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_173_178 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_174_175 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_175_33 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_176_175 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_177_175 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_178_175 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_179_180 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_179_187 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_180_181 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_180_184 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_180_185 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_180_186 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_181_182 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_182_183 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_183_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_184_182 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_185_182 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_186_182 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_187_188 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_187_190 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_187_191 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_187_192 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_188_189 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_189_183 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_190_189 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_191_189 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_192_189 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_193_194 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_194_195 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_195_26 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_196_197 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_197_198 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_198_199 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_199_200 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_199_204 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_200_201 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_201_202 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_202_203 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_203_27 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_204_205 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_205_206 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_206_203 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_207_208 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_208_209 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_209_210 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_210_211 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_210_215 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_211_212 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_212_213 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_213_214 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_214_27 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_215_216 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_216_217 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_217_214 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_218_219 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_219_220 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_220_221 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_221_222 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_221_226 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_222_223 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_223_224 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_224_225 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_225_27 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_226_227 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_227_228 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_228_225 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_229_15 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_230_231 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_230_233 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_231_232 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_232_16 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_233_232 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_234_235 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_234_240 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_235_236 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_235_239 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_236_237 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_237_238 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_238_17 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_239_237 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_240_241 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_240_243 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_241_242 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_242_238 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_243_242 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_244_245 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_244_250 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_245_246 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_245_249 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_246_247 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_247_248 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_248_17 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_249_247 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_250_251 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_250_253 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_251_252 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_252_248 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_253_252 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_254_255 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_254_260 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_255_256 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_255_259 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_256_257 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_257_258 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_258_17 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_259_257 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_260_261 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_260_263 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_261_262 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_262_258 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_263_262 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_264_265 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_265_266 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_266_10 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_267_268 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_268_269 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_269_10 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_270_271 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_271_272 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_272_10 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_273_274 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_274_275 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_275_10 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_276_277 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_277_278 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_278_10 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_279_280 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_280_281 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_281_10 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_282_283 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_283_284 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_284_10 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_285_4 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_286_287 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_286_289 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_287_288 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_288_5 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_289_288 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_290_291 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_290_293 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_291_292 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_292_5 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_293_292 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_294_295 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_294_297 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_295_296 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_296_5 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_297_296 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_298_299 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_298_301 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_299_300 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_300_5 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_301_300 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_302_303 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_302_305 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_303_304 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_304_5 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_305_304 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_306_307 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_306_309 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_307_308 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_308_5 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_309_308 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_310_311 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_310_313 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_311_312 (pow2ceil(2+0)-1)

#define __BUF_SIZE_MASK_312_5 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_313_312 (pow2ceil(2+0)-1)

#endif
