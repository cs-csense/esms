package esms.msa2;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

import esms.profiling.FTraceProfiler;
import esms.profiling.ftrace.FTraceEvent;
import esms.runtime.Environment;
import esms.runtime.Program;
import esms.util.Log;
import esms.util.WakeLocker;

public class MSAService extends Service {
    public static final String TAG = MSAService.class.getSimpleName();
    public class MSABinder extends Binder {
        MainActivity mActivity;
        MSAService getService() {
            return MSAService.this;
        }
        MSABinder setActivity(MainActivity activity) {
            mActivity = activity;
            return this;
        }

        public void toast(String msg) {
            mActivity.toast(msg);
        }

        public void notify(final String status) {
            mActivity.notify(status);
        }
    }
    private final MSABinder mBinder = new MSABinder();
    private String TK;
    private String BENCHMARK;

    public MSAService() {
        String[] pkg = BuildConfig.APPLICATION_ID.split("\\.");
        TK = pkg[0].toUpperCase();
        BENCHMARK = pkg[2];
        Log.i(TAG, "Toolkit: %s, Benchmark: %s", TK, BENCHMARK);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service Starting", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }

    private class MSATask extends AsyncTask<String, Integer, Integer> {
        private Program msa;
        private FTraceProfiler ftracer;
        private WakeLocker locker;

        @Override
        protected void onPreExecute() {
            ftracer = new FTraceProfiler();
            ftracer.register(FTraceEvent.POWER);
            ftracer.register(FTraceEvent.CPU_FREQ);
            ftracer.startService();
            locker = new WakeLocker(mBinder.mActivity);
            locker.acquire();
        }

        @Override
        protected Integer doInBackground(String... args) {
            msa = new esms(args);
            ftracer.startProfiling();
            msa.start().join(2000);
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            msa.report();
            ftracer.stopProfiling();
            try {
                ftracer.save(Environment.file("ftrace.log"));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            ftracer.stopService();
            locker.release();
            switch (result) {
                case 0:
                    mBinder.toast("MSA is done...");
                    break;
                default:
                    mBinder.toast("MSA unexpectedly returns " + result);
            }
            msa = null;
            ftracer = null;
            mBinder.notify("Stopped");
        }

        protected void onCancelled (Integer result) {
            ftracer.stopProfiling();
            ftracer.stopService();
            locker.release();
            msa = null;
            task = null;
            ftracer = null;
            locker = null;
            mBinder.notify("Canceled");
        }
    }

    private MSATask task;
    public void invoke() {
        if(task == null) task = new MSATask();
        switch (task.getStatus()) {
            case PENDING:
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
                String iterations = settings.getString(SettingsActivity.KEY_PREF_ITERATIONS, "500");
                String remote = settings.getString(SettingsActivity.KEY_PREF_REMOTE, "alacran.cs.uiowa.edu");
                Log.i(getClass().getSimpleName(), "iterations: %s, remote: %s\n", iterations, remote);
                task.execute("-i", iterations, "-scaling", "1", "-remote", remote);
                mBinder.toast("Start profiling MSA...");
                mBinder.notify("Running");
                break;
            case RUNNING:
                mBinder.toast("MSA is still running, cancel it");
                task.cancel(true);
                break;
            case FINISHED:
                mBinder.toast("MSA is done, gonna restart...");
                task = null;
                invoke();
        }
    }
}
