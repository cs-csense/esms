package esms.msa2;

import android.app.Application;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.RequiresDevice;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Arrays;

import esms.profiling.CPUWorker;
import esms.profiling.FTraceProfiler;
import esms.profiling.WorkerPool;
import esms.profiling.ftrace.FTraceEvent;
import esms.runtime.Environment;
import esms.usage.SystemMonitor;
import esms.util.Log;
import esms.util.WakeLocker;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(Parameterized.class)
public class ApplicationTest extends ApplicationTestCase<Application> {
    private static final Context CTX = InstrumentationRegistry.getInstrumentation().getTargetContext();
    private static final String[] PKG = InstrumentationRegistry.getContext().getPackageName().split("\\.");
    private static final String BENCHMARK = PKG[PKG.length > 1 ? PKG.length - 2 : 0];
    private static final String TAG = PKG.length > 1 ? PKG[1].toUpperCase() : PKG[0];
    static {
        try {
            System.loadLibrary("MSL");
            Log.i(TAG, "libMSL loaded");
        } catch (UnsatisfiedLinkError e) {
            throw new RuntimeException(e);
        }
    }

    private static final String REMOTE = "alacran.cs.uiowa.edu";
    //private static final String REMOTE = "192.168.1.6";
    //private static final String REMOTE = "192.168.0.102";
    private static final int ITERATIONS = 16;
    private static final int SCALING = 1;
    private static final int MSZ = 64;
    private static final int WK = 64;
    private static WakeLocker locker = new WakeLocker(InstrumentationRegistry.getContext());
    private static FTraceProfiler ftracer = new FTraceProfiler(32 * 1024);
    private static SystemMonitor monitor = SystemMonitor.instance(CTX);

    @Parameterized.Parameters
    public static Iterable<Object[]> params() {
        //return Arrays.asList(new Object[][]{{0}, {4}, {8}, {16}, {24}}); //
        return Arrays.asList(new Object[][]{{0}, {8}, {16}, {24}}); //
        //return Arrays.asList(new Object[][]{{0}});
    }

    @BeforeClass
    public static void setUpSpec() throws InterruptedException {
        locker.acquire();
        Environment.put("Context", CTX);
        monitor.monitor(SystemMonitor.Event.ALL, 250);
        ftracer.register(FTraceEvent.SCHED_SWITCH);
        ftracer.register(FTraceEvent.CPU_FREQ);
        ftracer.register(FTraceEvent.NET);
        ftracer.register(FTraceEvent.BLK_RQ_ISSUE);
        ftracer.register(FTraceEvent.BLK_RQ_COMPLETE);
        ftracer.startService();
        ftracer.startProfiling();
        monitor.start();
        Thread.sleep(10000);
    }

    @AfterClass
    public static void tearDownSpec() throws InterruptedException, IOException {
        monitor.stop();
        monitor.save(Environment.file("system.log"));
        ftracer.stopProfiling();
        ftracer.save(Environment.file("ftrace.log"));
        ftracer.stopService();
        locker.release();
    }

    @Rule
    public TestName INFO = new TestName();
    private String CFG;
    private WorkerPool workers;
    private esms app;
    private int N;
    public ApplicationTest(int workers) {
        super(Application.class);
        N = workers;
        CFG = BENCHMARK + "-" + N;
    }

    @Before
    public void setUp() throws Exception {
        Log.i(TAG, "Running " + CFG + " in " + CTX.getPackageName());
        workers = new WorkerPool();
        for(int i = 0; i < N; i++)
            workers.add(new CPUWorker(MSZ, WK * ITERATIONS));
        app = new esms(new String[] { "-b", CFG, "-i", String.valueOf(ITERATIONS), "-scaling", String.valueOf(SCALING), "-remote", REMOTE });
        ftracer.wait(5000);
        ftracer.mark(CFG + "-start-" + ftracer.timestamp());
    }

    @After
    public void tearDown() throws Exception {
        ftracer.mark(CFG + "-stop-" + ftracer.timestamp());
        ftracer.wait(5000);
    }

    @Test
    @LargeTest
    @RequiresDevice
    public void testApplication() throws Exception {
        workers.start();
        app.start().join(0);
        app.report();
        workers.stop();
    }
}