package esms.msa2;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Debug;
import android.os.Looper;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.RequiresDevice;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import esms.msl.AudioSourceOld;
import esms.msl.VideoSource;
import esms.profiling.SystemProfiler;
import esms.runtime.Program;
import esms.usage.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.List;

import esms.profiling.FTraceProfiler;
import esms.profiling.ftrace.FTraceEvent;
import esms.profiling.ftrace.FTracer;
import esms.profiling.TrepnProfiler;
import esms.runtime.Environment;
import esms.util.Formatter;
import esms.util.SuperSU;
import esms.util.Log;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
public class LibraryTest extends ApplicationTestCase<Application> {
    private static final Context CTX = InstrumentationRegistry.getInstrumentation().getTargetContext();
    private static final String[] PKG = CTX.getPackageName().split("\\.");
    private static final String BENCHMARK = PKG[PKG.length > 1 ? PKG.length - 2 : 0];
    private static final String TAG = PKG.length > 1 ? PKG[1].toUpperCase() : PKG[0];
    static {
        try {
            System.loadLibrary("MSL");
            Log.i(TAG, "libMSL loaded");
        } catch (UnsatisfiedLinkError e) {
            throw new RuntimeException(e);
        }
    }

    @Rule
    public TestName INFO = new TestName();
    public LibraryTest() {
        super(Application.class);
    }

    @BeforeClass
    public static void setupSpec() {
        Environment.put("Context", CTX);
    }

    @Before
    public void setUp() {
        Log.i(TAG, Formatter.title(INFO.getMethodName()));
    }

    @After
    public void tearDown() {
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testDebugfs() throws IOException {
        List<String> output = SuperSU.run(false, "cat", Environment.file("cpufreq.sh"));
        if(output == null || output.isEmpty()) {
            Log.i(TAG, "No output: %s\n", output);
        } else {
            for (String line : output)
                Log.i(TAG, line);
        }
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testRUsage() throws InterruptedException {
        RUsage ru1 = new RUsage().get();
        long dbg1 = Debug.threadCpuTimeNanos();
        long thrd1 = SystemClock.currentThreadTimeMillis();
        Log.i(TAG, ru1.header());
        Log.i(TAG, ru1);
        Log.i(TAG, "dbg1: %dns, thrd1: %dms\n", dbg1, thrd1);

        Thread.sleep(2000);
        RUsage ru2 = new RUsage().get();
        long dbg2 = Debug.threadCpuTimeNanos();
        long thrd2 = SystemClock.currentThreadTimeMillis();
        Log.i(TAG, ru2);
        Log.i(TAG, ru2.diff(ru1));
        Log.i(TAG, "dbg2: %dns, diff: %dns, thrd2: %dms, diff: %dms\n", dbg2, dbg2-dbg1, thrd2, thrd2-thrd1);
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testCPUStat() throws IOException, InterruptedException {
        CPUStat stat1 = new CPUStat().get();
        Thread.sleep(2000);
        CPUStat stat2 = new CPUStat().get();
        CPUStat diff = stat2.diff(stat1);
        assertEquals(stat1.size(), stat2.size());
        for(int i = 0; i < stat1.size(); i++) {
            Log.i(TAG, stat1.get(i).header());
            Log.i(TAG, stat1.get(i));
            Log.i(TAG, stat2.get(i));
        }
        Log.i(TAG, diff);
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testBlockStat() throws InterruptedException {
        BlockStat stat1 = new BlockStat().get();
        Thread.sleep(2000);
        BlockStat stat2 = new BlockStat().get();
        Usage diff = stat2.diff(stat1);
        Log.i(TAG, stat1.header());
        Log.i(TAG, stat1);
        Log.i(TAG, stat2);
        Log.i(TAG, diff);
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testNetStat() throws InterruptedException {
        Log.i(TAG, "=============== %s ===============\n", NetStat.WIFI);
        NetStat stat1 = new NetStat(NetStat.WIFI).get();
        Thread.sleep(2000);
        NetStat stat2 = new NetStat(NetStat.WIFI).get();
        Usage diff = stat2.diff(stat1);
        Log.i(TAG, stat1.header());
        Log.i(TAG, stat1);
        Log.i(TAG, stat2);
        Log.i(TAG, diff);

        Log.i(TAG, "=============== %s ===============\n", NetStat.CELL);
        stat1 = new NetStat(NetStat.CELL).get();
        Thread.sleep(2000);
        stat2 = new NetStat(NetStat.CELL).get();
        diff = stat2.diff(stat1);
        Log.i(TAG, stat1.header());
        Log.i(TAG, stat1);
        Log.i(TAG, stat2);
        Log.i(TAG, diff);
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testWifiUsage() throws InterruptedException {
        Formatter.title(INFO.getMethodName());
        Usage stat1 = new WifiUsage(CTX).get();
        Thread.sleep(2000);
        Usage stat2 = new WifiUsage(CTX).get();
        Usage diff = stat2.diff(stat1);
        Log.i(TAG, stat1.header());
        Log.i(TAG, stat1);
        Log.i(TAG, stat2);
        Log.i(TAG, diff);
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testCellUsage() throws InterruptedException {
        Log.i(TAG, Formatter.title(INFO.getMethodName()));
        Looper.prepare();
        Usage stat1 = new CellUsage(CTX).get();
        Thread.sleep(2000);
        Usage stat2 = new CellUsage(CTX).get();
        Usage diff = stat2.diff(stat1);
        Log.i(TAG, stat1.header());
        Log.i(TAG, stat1);
        Log.i(TAG, stat2);
        Log.i(TAG, diff);
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testDisplayUsage() throws InterruptedException {
        Log.i(TAG, Formatter.title(INFO.getMethodName()));
        Usage stat1 = new DisplayUsage(CTX).get();
        Thread.sleep(2000);
        Usage stat2 = new DisplayUsage(CTX).get();
        Usage diff = stat2.diff(stat1);
        Log.i(TAG, stat1.header());
        Log.i(TAG, stat1);
        Log.i(TAG, stat2);
        Log.i(TAG, diff);
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testEnergyUsage() throws InterruptedException {
        Usage stat1 = new EnergyUsage(CTX).get();
        Thread.sleep(2000);
        Usage stat2 = new EnergyUsage(CTX).get();
        Usage diff = stat2.diff(stat1);
        Log.i(TAG, stat1.header());
        Log.i(TAG, stat1);
        Log.i(TAG, stat2);
        Log.i(TAG, diff);
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testSystemProfiler() throws InterruptedException {
        Looper.prepare();
        SystemProfiler profiler = new SystemProfiler(CTX);
        profiler.profile();
        Thread.sleep(2000);
        profiler.profile();
        File file = Program.file("system-test.log");
        profiler.save(file);
        assertTrue(file.exists());
        assertFalse(Program.file("system.log").exists());
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testSystemMonitor() throws InterruptedException {
        Looper.prepare();
        SystemMonitor monitor = SystemMonitor.instance(CTX);
        monitor.monitor(SystemMonitor.Event.CPU, 250);
        monitor.monitor(SystemMonitor.Event.Block, 250);
        monitor.monitor(SystemMonitor.Event.Cell, 250);
        monitor.monitor(SystemMonitor.Event.ALL, 250);
        monitor.history(5);
        monitor.start();
        Thread.sleep(2000);
        monitor.stop();
        Log.i(TAG, monitor);
        File file = Program.file("system-monitor.log");
        monitor.save(file);
        assertTrue(file.exists());
        assertFalse(Program.file("system.log").exists());
        assertEquals(monitor, SystemMonitor.instance(CTX));
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testFTraceInstance() throws IOException, InterruptedException {
        FTracer instance = new FTracer();
        File trace = new File(Environment.getStoragePath(), "trace.log");
        if(SuperSU.available()) {
            assertTrue(instance.clock(FTracer.Clock.global));
            assertTrue(instance.clock() == FTracer.Clock.global);
            instance.overwrite(false);
            assertFalse(instance.overwrite());

            int size = 16 * 1024;
            assertTrue(instance.bufferSizeInKBs(size));
            assertEquals(size, instance.bufferSizeInKBs());
            assertTrue(instance.clear());

            instance.register(FTraceEvent.POWER);
            instance.register(FTraceEvent.CPU_FREQ);
            assertTrue(instance.enable());
            assertTrue(instance.isEnabled(FTraceEvent.POWER));
            assertTrue(instance.isEnabled(FTraceEvent.CPU_FREQ));

            Thread.sleep(5000);
            assertTrue(instance.disable());

            instance.save(trace.getAbsolutePath());
            assertTrue(trace.exists());
            assertTrue(trace.length() > 0);

            assertTrue(instance.clock(FTracer.Clock.local));
            assertTrue(instance.overwrite(true));
        }
    }

    @Test
    @SmallTest
    @RequiresDevice
    public void testFTraceProfiler() throws Exception {
        FTraceProfiler profiler = new FTraceProfiler();
        profiler.startService();
        profiler.register(FTraceEvent.POWER);
        profiler.register(FTraceEvent.CPU_FREQ);
        profiler.register(FTraceEvent.CPU_IDLE);
        profiler.startProfiling();
        profiler.wait(3000);
        profiler.stopProfiling();
        File log = Environment.file("ftrace.log");
        profiler.save(log);
        profiler.stopService();
        assertTrue(log.exists());
        assertTrue(log.length() > 423);
    }

    private enum State {
        READY, PROFILING, PROFILED
    }

    @Ignore
    @Test
    @SmallTest
    @RequiresDevice
    public void testTrepnProfiler() {
        TrepnProfiler profiler = new TrepnProfiler(InstrumentationRegistry.getContext());
        profiler.startService();
        profiler.startProfiling();
        profiler.update(State.READY.ordinal(), State.READY.name());
        profiler.update(State.PROFILING.ordinal(), State.PROFILING.name());
        profiler.wait(5000);

        profiler.update(State.PROFILED.ordinal(), State.PROFILED.name());
        profiler.stopProfiling();
        profiler.stopService();
        Log.d(TAG, profiler.toString());
    }

    @Ignore
    @Test
    @SmallTest
    @RequiresDevice
    public void testAudioSource() throws IOException {
        AudioSourceOld src = new AudioSourceOld(AudioSourceOld.SRC.VOICE_RECOGNITION, AudioSourceOld.Rate.SAMPLE_RATE_16K, AudioSourceOld.ChannelConfig.MONO, AudioSourceOld.Encoding.PCM_16BIT, 8192);
        src.open();
        src.start();
        for(int i = 0; i < 5; i++) {
            ByteBuffer samples = src.read();
            Log.d(TAG, String.format("read audio frame[%d] of %d bytes", i, samples.remaining()));
            src.release();
        }
        src.stop();
        src.close();
    }

    @Ignore
    @Test
    @SmallTest
    @RequiresDevice
    public void testVideoSource() throws IOException {
        VideoSource src = new VideoSource(InstrumentationRegistry.getContext(), VideoSource.Lens.LENS_FACING_FRONT, 640, 480, VideoSource.Format.JPEG, 5);
        src.open();
        src.start();
        for(int i = 0; i < 5; i++) {
            List<ByteBuffer> buffers = src.acquire();
            new FileOutputStream(new File(Environment.getStoragePath(), "image" + i + ".jpg")).getChannel().write(buffers.get(0));
            Log.d(TAG, String.format("read video frame[%d] with %d planes", i, buffers.size()));
            src.release();
        }
        src.stop();
        src.close();
    }
}