#ifndef __FUSION_H
#define __FUSION_H

#define max(A,B) (((A)>(B))?(A):(B))
#define pow2ceil(A) ((A<=2)?(2): (((A<=4)?(4): (((A<=8)?(8): (((A<=16)?(16): (((A<=32)?(32): (((A<=64)?(64): (((A<=128)?(128): (((A<=256)?(256):(((A<=1024)?(1024):(((A<=4096)?(4096):(((A<=16384)?(16384):(((A<=65536)?(65536):(((A<=131072)?(131072):(((A<=262144)?(262144):(((A<=524288)?(524288):(((A<=1048576)?(1048576):(((A<=2097152)?(2097152):(((A<=4194304)?(4194304):(((A<=8388608)?(8388608):(((A<=16777216)?(16777216):(((A<=33554432)?(33554432):(((A<=67108864)?(67108864):(((A<=134217728)?(134217728):(((A<=268435456)?(268435456):(((A<=536870912)?(536870912):(1073741824))))))))))))))))))))))))))))))))))))))))))))))))))
//destination peeks: 128 extra items
#define __PEEK_BUF_SIZE_0_1 128
#define __BUF_SIZE_MASK_0_1 (pow2ceil(256+128)-1)

#define __BUF_SIZE_MASK_1_2 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_2_3 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_2_10 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_3_4 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_4_5 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_5_6 (pow2ceil(129+0)-1)

#define __BUF_SIZE_MASK_6_7 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_7_8 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_8_9 (pow2ceil(21+0)-1)

#define __BUF_SIZE_MASK_10_11 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_11_12 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_12_13 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_13_14 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_14_15 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_15_16 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_15_554 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_16_17 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_17_18 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_18_19 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_19_20 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_19_551 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_20_21 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_21_22 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_22_23 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_23_24 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_24_25 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_25_26 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_25_537 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_26_27 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_26_528 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_26_531 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_26_534 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_27_28 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_28_29 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_29_30 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_30_31 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_31_32 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_32_33 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_32_502 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_33_34 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_33_481 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_33_484 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_33_487 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_33_490 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_33_493 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_33_496 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_33_499 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_34_35 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_35_36 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_36_37 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_37_38 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_38_39 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_39_40 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_39_431 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_40_41 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_386 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_389 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_392 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_395 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_398 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_401 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_404 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_407 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_410 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_413 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_416 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_419 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_422 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_425 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_40_428 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_41_42 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_42_43 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_43_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_44_45 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_45_46 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_46_47 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_46_288 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_47_48 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_195 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_198 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_201 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_204 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_207 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_210 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_213 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_216 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_219 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_222 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_225 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_228 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_231 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_234 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_237 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_240 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_243 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_246 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_249 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_252 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_255 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_258 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_261 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_264 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_267 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_270 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_273 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_276 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_279 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_282 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_47_285 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_48_49 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_49_50 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_50_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_51_52 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_52_53 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_53_54 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_53_129 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_54_55 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_66 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_67 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_68 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_69 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_70 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_71 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_72 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_73 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_74 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_76 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_77 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_78 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_79 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_80 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_81 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_82 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_83 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_84 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_85 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_86 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_87 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_88 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_89 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_90 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_91 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_92 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_93 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_94 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_95 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_96 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_97 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_98 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_99 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_100 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_101 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_102 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_103 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_104 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_105 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_106 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_107 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_108 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_109 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_110 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_111 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_112 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_113 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_114 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_115 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_116 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_117 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_118 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_119 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_120 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_121 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_123 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_124 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_125 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_126 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_127 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_54_128 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_55_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_56_57 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_57_58 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_58_59 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_59_60 (pow2ceil(129+0)-1)

#define __BUF_SIZE_MASK_60_61 (pow2ceil(129+0)-1)

#define __BUF_SIZE_MASK_60_64 (pow2ceil(129+0)-1)

#define __BUF_SIZE_MASK_61_62 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_62_63 (pow2ceil(21+0)-1)

#define __BUF_SIZE_MASK_63_8 (pow2ceil(20+0)-1)

#define __BUF_SIZE_MASK_64_65 (pow2ceil(20+0)-1)

#define __BUF_SIZE_MASK_65_62 (pow2ceil(20+0)-1)

#define __BUF_SIZE_MASK_66_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_67_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_68_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_69_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_70_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_71_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_72_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_74_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_75_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_76_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_77_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_78_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_79_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_80_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_81_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_82_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_83_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_84_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_85_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_86_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_87_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_88_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_89_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_90_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_91_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_92_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_93_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_94_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_95_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_96_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_97_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_98_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_99_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_100_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_101_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_102_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_103_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_104_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_105_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_106_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_107_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_108_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_109_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_110_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_111_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_112_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_113_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_114_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_115_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_116_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_117_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_118_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_119_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_121_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_122_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_123_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_124_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_125_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_126_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_127_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_128_56 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_130 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_132 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_133 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_134 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_135 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_136 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_137 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_138 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_139 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_140 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_141 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_142 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_143 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_144 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_145 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_146 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_147 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_148 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_149 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_150 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_151 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_152 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_153 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_154 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_155 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_156 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_157 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_158 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_159 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_160 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_161 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_162 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_163 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_164 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_165 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_166 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_167 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_168 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_169 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_170 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_171 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_172 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_173 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_174 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_175 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_176 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_177 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_178 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_179 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_180 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_181 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_182 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_183 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_184 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_185 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_186 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_187 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_188 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_189 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_190 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_191 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_192 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_193 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_194 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_130_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_131_57 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_132_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_133_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_134_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_135_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_136_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_137_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_138_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_139_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_140_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_141_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_142_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_143_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_144_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_145_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_146_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_147_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_148_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_149_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_150_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_151_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_152_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_153_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_154_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_155_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_156_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_157_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_158_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_159_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_160_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_161_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_162_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_163_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_164_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_165_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_166_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_167_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_168_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_169_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_170_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_171_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_172_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_173_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_174_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_175_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_176_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_177_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_178_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_179_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_180_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_181_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_182_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_183_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_184_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_185_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_186_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_187_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_188_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_189_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_190_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_191_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_192_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_193_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_194_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_195_196 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_196_197 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_197_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_198_199 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_199_200 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_200_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_201_202 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_202_203 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_203_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_204_205 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_205_206 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_206_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_207_208 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_208_209 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_209_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_210_211 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_211_212 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_212_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_213_214 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_214_215 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_215_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_216_217 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_217_218 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_218_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_219_220 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_220_221 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_221_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_222_223 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_223_224 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_224_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_225_226 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_226_227 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_227_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_228_229 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_229_230 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_230_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_231_232 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_232_233 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_233_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_234_235 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_235_236 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_236_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_237_238 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_238_239 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_239_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_240_241 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_241_242 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_242_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_243_244 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_244_245 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_245_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_246_247 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_247_248 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_248_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_249_250 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_250_251 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_251_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_252_253 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_253_254 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_254_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_255_256 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_256_257 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_257_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_258_259 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_259_260 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_260_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_261_262 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_262_263 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_263_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_264_265 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_265_266 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_266_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_267_268 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_268_269 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_269_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_270_271 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_271_272 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_272_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_273_274 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_274_275 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_275_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_276_277 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_277_278 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_278_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_280 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_280_281 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_281_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_282_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_283_284 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_284_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_285_286 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_286_287 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_287_51 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_289 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_293 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_296 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_299 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_302 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_305 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_308 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_311 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_314 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_317 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_320 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_323 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_326 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_329 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_332 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_335 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_338 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_341 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_344 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_347 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_350 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_353 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_356 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_359 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_362 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_365 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_368 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_371 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_374 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_377 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_380 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_383 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_289_290 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_290_291 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_291_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_292_52 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_293_294 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_294_295 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_295_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_296_297 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_297_298 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_298_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_299_300 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_300_301 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_301_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_302_303 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_303_304 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_304_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_305_306 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_306_307 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_307_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_308_309 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_309_310 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_310_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_311_312 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_312_313 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_313_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_314_315 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_315_316 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_316_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_317_318 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_318_319 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_319_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_320_321 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_321_322 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_322_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_323_324 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_324_325 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_325_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_326_327 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_327_328 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_328_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_329_330 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_330_331 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_331_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_332_333 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_333_334 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_334_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_335_336 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_336_337 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_337_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_338_339 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_339_340 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_340_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_341_342 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_342_343 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_343_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_344_345 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_345_346 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_346_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_347_348 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_348_349 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_349_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_350_351 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_351_352 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_352_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_353_354 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_354_355 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_355_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_356_357 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_357_358 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_358_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_359_360 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_360_361 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_361_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_362_363 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_363_364 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_364_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_365_366 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_366_367 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_367_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_368_369 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_369_370 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_370_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_371_372 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_372_373 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_373_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_374_375 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_375_376 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_376_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_377_378 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_378_379 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_379_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_380_381 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_381_382 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_382_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_383_384 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_384_385 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_385_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_386_387 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_387_388 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_388_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_389_390 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_390_391 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_391_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_392_393 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_393_394 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_394_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_395_396 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_396_397 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_397_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_398_399 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_399_400 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_400_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_401_402 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_402_403 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_403_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_404_405 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_405_406 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_406_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_407_408 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_408_409 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_409_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_410_411 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_411_412 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_412_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_413_414 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_414_415 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_415_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_416_417 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_417_418 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_418_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_419_420 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_420_421 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_421_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_423 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_423_424 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_424_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_425_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_426_427 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_427_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_428_429 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_429_430 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_430_44 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_432 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_436 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_439 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_442 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_445 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_448 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_451 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_454 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_457 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_460 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_463 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_466 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_469 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_472 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_475 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_478 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_432_433 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_433_434 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_434_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_435_45 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_436_437 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_437_438 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_438_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_439_440 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_440_441 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_441_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_442_443 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_443_444 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_444_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_445_446 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_446_447 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_447_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_448_449 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_449_450 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_450_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_451_452 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_452_453 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_453_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_454_455 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_455_456 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_456_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_457_458 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_458_459 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_459_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_460_461 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_461_462 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_462_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_463_464 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_464_465 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_465_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_466_467 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_467_468 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_468_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_469_470 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_470_471 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_471_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_472_473 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_473_474 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_474_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_475_476 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_476_477 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_477_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_478_479 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_479_480 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_480_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_481_482 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_482_483 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_483_37 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_484_485 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_485_486 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_486_37 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_487_488 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_488_489 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_489_37 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_490_491 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_491_492 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_492_37 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_493_494 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_494_495 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_495_37 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_496_497 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_497_498 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_498_37 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_499_500 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_500_501 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_501_37 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_502_503 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_502_507 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_502_510 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_502_513 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_502_516 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_502_519 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_502_522 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_502_525 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_503_504 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_504_505 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_505_506 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_506_38 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_507_508 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_508_509 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_509_506 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_510_511 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_511_512 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_512_506 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_513_514 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_514_515 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_515_506 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_516_517 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_517_518 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_518_506 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_519_520 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_520_521 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_521_506 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_522_523 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_523_524 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_524_506 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_525_526 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_526_527 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_527_506 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_528_529 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_529_530 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_530_30 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_531_532 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_532_533 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_533_30 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_534_535 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_535_536 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_536_30 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_537_538 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_537_542 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_537_545 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_537_548 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_538_539 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_539_540 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_540_541 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_541_31 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_542_543 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_543_544 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_544_541 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_545_546 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_546_547 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_547_541 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_548_549 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_549_550 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_550_541 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_551_552 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_552_553 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_553_23 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_554_555 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_555_556 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_556_557 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_557_558 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_557_562 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_558_559 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_559_560 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_560_561 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_561_24 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_562_563 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_563_564 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_564_561 (pow2ceil(128+0)-1)

#endif
