#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<math.h>
#include<time.h>
#ifdef __MACH__
#include <sys/resource.h>
#endif

typedef bool boolean;
typedef FILE* FP;

static char SF0[17596];

int head_AR0 = 0;
int head_incr_AR0 = 1;
static inline void push_AR0_void(float value) {
}

static inline void push_AR0_zero(float value) {
	((float*)SF0)[head_AR0] = value;
}

static inline void push_AR0_incr(float value) {
	((float*)SF0)[head_AR0++] = value;
}

static inline void push_AR0_decr(float value) {
	((float*)SF0)[head_AR0--] = value;
}

static inline void push_AR0(float value) {
	((float*)SF0)[head_AR0] = value;
	head_AR0 += head_incr_AR0;
}

int head_AR = 0;
static inline void push_AR_indirect(int index, float value) {
	((float*)SF0)[index] = value;
}

int tail_AR0 = 0;
int tail_incr_AR0 = 1;
static inline float peek_AR0(int offset) {
	return ((float*)SF0)[tail_AR0 + offset * tail_incr_AR0];
}

static inline float pop_AR0_zero() {
	return ((float*)SF0)[tail_AR0];
}

static inline float pop_AR0_incr() {
	return ((float*)SF0)[tail_AR0++];
}

static inline float pop_AR0_decr() {
	return ((float*)SF0)[tail_AR0--];
}

static inline float pop_AR0() {
	float value = ((float*)SF0)[tail_AR0];
	tail_AR0 += tail_incr_AR0;
	return value;
}

int tail_AR1 = 0;
int tail_incr_AR1 = 1;
static inline float peek_AR1(int offset) {
	return ((float*)SF0)[tail_AR1 + offset * tail_incr_AR1];
}

static inline float pop_AR1_zero() {
	return ((float*)SF0)[tail_AR1];
}

static inline float pop_AR1_incr() {
	return ((float*)SF0)[tail_AR1++];
}

static inline float pop_AR1_decr() {
	return ((float*)SF0)[tail_AR1--];
}

static inline float pop_AR1() {
	float value = ((float*)SF0)[tail_AR1];
	tail_AR1 += tail_incr_AR1;
	return value;
}

int tail_AR = 0;
static inline float peek_pop_AR_indirect(int index) {
	return ((float*)SF0)[index];
}

static inline void resetAR_src0_init_g0() {
	head_AR0 = 0;
	head_incr_AR0 = 1;
}

static inline void resetAR_src0_init_g0i() {
}

static inline void resetAR_GetAvg0_init_g0() {
	tail_AR0 = 0;
	tail_incr_AR0 = 1;
	head_AR0 = 271;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg0_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg0_init_g0() {
	tail_AR0 = 271;
	tail_incr_AR0 = 1;
	head_AR0 = 271;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg0_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum0_init_g0() {
	tail_AR0 = 271;
	tail_incr_AR0 = 1;
	head_AR0 = 528;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum0_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums0_init_g0() {
	tail_AR0 = 271;
	tail_incr_AR0 = 1;
	head_AR0 = 271;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums0_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division0_init_g0() {
	tail_AR0 = 528;
	tail_incr_AR0 = 1;
	tail_AR1 = 271;
	tail_incr_AR1 = 1;
	head_AR0 = 528;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division0_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg1_init_g0() {
	tail_AR0 = 1;
	tail_incr_AR0 = 1;
	head_AR0 = 529;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg1_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg1_init_g0() {
	tail_AR0 = 529;
	tail_incr_AR0 = 1;
	head_AR0 = 529;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg1_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum1_init_g0() {
	tail_AR0 = 529;
	tail_incr_AR0 = 1;
	head_AR0 = 786;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum1_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums1_init_g0() {
	tail_AR0 = 529;
	tail_incr_AR0 = 1;
	head_AR0 = 529;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums1_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division1_init_g0() {
	tail_AR0 = 786;
	tail_incr_AR0 = 1;
	tail_AR1 = 529;
	tail_incr_AR1 = 1;
	head_AR0 = 786;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division1_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg2_init_g0() {
	tail_AR0 = 2;
	tail_incr_AR0 = 1;
	head_AR0 = 787;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg2_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg2_init_g0() {
	tail_AR0 = 787;
	tail_incr_AR0 = 1;
	head_AR0 = 787;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg2_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum2_init_g0() {
	tail_AR0 = 787;
	tail_incr_AR0 = 1;
	head_AR0 = 1044;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum2_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums2_init_g0() {
	tail_AR0 = 787;
	tail_incr_AR0 = 1;
	head_AR0 = 787;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums2_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division2_init_g0() {
	tail_AR0 = 1044;
	tail_incr_AR0 = 1;
	tail_AR1 = 787;
	tail_incr_AR1 = 1;
	head_AR0 = 1044;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division2_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg3_init_g0() {
	tail_AR0 = 3;
	tail_incr_AR0 = 1;
	head_AR0 = 1045;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg3_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg3_init_g0() {
	tail_AR0 = 1045;
	tail_incr_AR0 = 1;
	head_AR0 = 1045;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg3_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum3_init_g0() {
	tail_AR0 = 1045;
	tail_incr_AR0 = 1;
	head_AR0 = 1302;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum3_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums3_init_g0() {
	tail_AR0 = 1045;
	tail_incr_AR0 = 1;
	head_AR0 = 1045;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums3_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division3_init_g0() {
	tail_AR0 = 1302;
	tail_incr_AR0 = 1;
	tail_AR1 = 1045;
	tail_incr_AR1 = 1;
	head_AR0 = 1302;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division3_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg4_init_g0() {
	tail_AR0 = 4;
	tail_incr_AR0 = 1;
	head_AR0 = 1303;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg4_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg4_init_g0() {
	tail_AR0 = 1303;
	tail_incr_AR0 = 1;
	head_AR0 = 1303;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg4_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum4_init_g0() {
	tail_AR0 = 1303;
	tail_incr_AR0 = 1;
	head_AR0 = 1560;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum4_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums4_init_g0() {
	tail_AR0 = 1303;
	tail_incr_AR0 = 1;
	head_AR0 = 1303;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums4_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division4_init_g0() {
	tail_AR0 = 1560;
	tail_incr_AR0 = 1;
	tail_AR1 = 1303;
	tail_incr_AR1 = 1;
	head_AR0 = 1560;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division4_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg5_init_g0() {
	tail_AR0 = 5;
	tail_incr_AR0 = 1;
	head_AR0 = 1561;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg5_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg5_init_g0() {
	tail_AR0 = 1561;
	tail_incr_AR0 = 1;
	head_AR0 = 1561;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg5_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum5_init_g0() {
	tail_AR0 = 1561;
	tail_incr_AR0 = 1;
	head_AR0 = 1818;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum5_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums5_init_g0() {
	tail_AR0 = 1561;
	tail_incr_AR0 = 1;
	head_AR0 = 1561;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums5_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division5_init_g0() {
	tail_AR0 = 1818;
	tail_incr_AR0 = 1;
	tail_AR1 = 1561;
	tail_incr_AR1 = 1;
	head_AR0 = 1818;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division5_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg6_init_g0() {
	tail_AR0 = 6;
	tail_incr_AR0 = 1;
	head_AR0 = 1819;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg6_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg6_init_g0() {
	tail_AR0 = 1819;
	tail_incr_AR0 = 1;
	head_AR0 = 1819;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg6_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum6_init_g0() {
	tail_AR0 = 1819;
	tail_incr_AR0 = 1;
	head_AR0 = 2076;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum6_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums6_init_g0() {
	tail_AR0 = 1819;
	tail_incr_AR0 = 1;
	head_AR0 = 1819;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums6_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division6_init_g0() {
	tail_AR0 = 2076;
	tail_incr_AR0 = 1;
	tail_AR1 = 1819;
	tail_incr_AR1 = 1;
	head_AR0 = 2076;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division6_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg7_init_g0() {
	tail_AR0 = 7;
	tail_incr_AR0 = 1;
	head_AR0 = 2077;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg7_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg7_init_g0() {
	tail_AR0 = 2077;
	tail_incr_AR0 = 1;
	head_AR0 = 2077;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg7_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum7_init_g0() {
	tail_AR0 = 2077;
	tail_incr_AR0 = 1;
	head_AR0 = 2334;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum7_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums7_init_g0() {
	tail_AR0 = 2077;
	tail_incr_AR0 = 1;
	head_AR0 = 2077;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums7_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division7_init_g0() {
	tail_AR0 = 2334;
	tail_incr_AR0 = 1;
	tail_AR1 = 2077;
	tail_incr_AR1 = 1;
	head_AR0 = 2334;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division7_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg8_init_g0() {
	tail_AR0 = 8;
	tail_incr_AR0 = 1;
	head_AR0 = 2335;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg8_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg8_init_g0() {
	tail_AR0 = 2335;
	tail_incr_AR0 = 1;
	head_AR0 = 2335;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg8_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum8_init_g0() {
	tail_AR0 = 2335;
	tail_incr_AR0 = 1;
	head_AR0 = 2592;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum8_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums8_init_g0() {
	tail_AR0 = 2335;
	tail_incr_AR0 = 1;
	head_AR0 = 2335;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums8_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division8_init_g0() {
	tail_AR0 = 2592;
	tail_incr_AR0 = 1;
	tail_AR1 = 2335;
	tail_incr_AR1 = 1;
	head_AR0 = 2592;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division8_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg9_init_g0() {
	tail_AR0 = 9;
	tail_incr_AR0 = 1;
	head_AR0 = 2593;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg9_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg9_init_g0() {
	tail_AR0 = 2593;
	tail_incr_AR0 = 1;
	head_AR0 = 2593;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg9_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum9_init_g0() {
	tail_AR0 = 2593;
	tail_incr_AR0 = 1;
	head_AR0 = 2850;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum9_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums9_init_g0() {
	tail_AR0 = 2593;
	tail_incr_AR0 = 1;
	head_AR0 = 2593;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums9_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division9_init_g0() {
	tail_AR0 = 2850;
	tail_incr_AR0 = 1;
	tail_AR1 = 2593;
	tail_incr_AR1 = 1;
	head_AR0 = 2850;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division9_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg10_init_g0() {
	tail_AR0 = 10;
	tail_incr_AR0 = 1;
	head_AR0 = 2851;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg10_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg10_init_g0() {
	tail_AR0 = 2851;
	tail_incr_AR0 = 1;
	head_AR0 = 2851;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg10_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum10_init_g0() {
	tail_AR0 = 2851;
	tail_incr_AR0 = 1;
	head_AR0 = 3108;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum10_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums10_init_g0() {
	tail_AR0 = 2851;
	tail_incr_AR0 = 1;
	head_AR0 = 2851;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums10_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division10_init_g0() {
	tail_AR0 = 3108;
	tail_incr_AR0 = 1;
	tail_AR1 = 2851;
	tail_incr_AR1 = 1;
	head_AR0 = 3108;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division10_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg11_init_g0() {
	tail_AR0 = 11;
	tail_incr_AR0 = 1;
	head_AR0 = 3109;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg11_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg11_init_g0() {
	tail_AR0 = 3109;
	tail_incr_AR0 = 1;
	head_AR0 = 3109;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg11_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum11_init_g0() {
	tail_AR0 = 3109;
	tail_incr_AR0 = 1;
	head_AR0 = 3366;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum11_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums11_init_g0() {
	tail_AR0 = 3109;
	tail_incr_AR0 = 1;
	head_AR0 = 3109;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums11_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division11_init_g0() {
	tail_AR0 = 3366;
	tail_incr_AR0 = 1;
	tail_AR1 = 3109;
	tail_incr_AR1 = 1;
	head_AR0 = 3366;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division11_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg12_init_g0() {
	tail_AR0 = 12;
	tail_incr_AR0 = 1;
	head_AR0 = 3367;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg12_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg12_init_g0() {
	tail_AR0 = 3367;
	tail_incr_AR0 = 1;
	head_AR0 = 3367;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg12_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum12_init_g0() {
	tail_AR0 = 3367;
	tail_incr_AR0 = 1;
	head_AR0 = 3624;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum12_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums12_init_g0() {
	tail_AR0 = 3367;
	tail_incr_AR0 = 1;
	head_AR0 = 3367;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums12_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division12_init_g0() {
	tail_AR0 = 3624;
	tail_incr_AR0 = 1;
	tail_AR1 = 3367;
	tail_incr_AR1 = 1;
	head_AR0 = 3624;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division12_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg13_init_g0() {
	tail_AR0 = 13;
	tail_incr_AR0 = 1;
	head_AR0 = 3625;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg13_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg13_init_g0() {
	tail_AR0 = 3625;
	tail_incr_AR0 = 1;
	head_AR0 = 3625;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg13_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum13_init_g0() {
	tail_AR0 = 3625;
	tail_incr_AR0 = 1;
	head_AR0 = 3882;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum13_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums13_init_g0() {
	tail_AR0 = 3625;
	tail_incr_AR0 = 1;
	head_AR0 = 3625;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums13_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division13_init_g0() {
	tail_AR0 = 3882;
	tail_incr_AR0 = 1;
	tail_AR1 = 3625;
	tail_incr_AR1 = 1;
	head_AR0 = 3882;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division13_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg14_init_g0() {
	tail_AR0 = 14;
	tail_incr_AR0 = 1;
	head_AR0 = 3883;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg14_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg14_init_g0() {
	tail_AR0 = 3883;
	tail_incr_AR0 = 1;
	head_AR0 = 3883;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg14_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum14_init_g0() {
	tail_AR0 = 3883;
	tail_incr_AR0 = 1;
	head_AR0 = 4140;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum14_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums14_init_g0() {
	tail_AR0 = 3883;
	tail_incr_AR0 = 1;
	head_AR0 = 3883;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums14_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division14_init_g0() {
	tail_AR0 = 4140;
	tail_incr_AR0 = 1;
	tail_AR1 = 3883;
	tail_incr_AR1 = 1;
	head_AR0 = 4140;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division14_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg15_init_g0() {
	tail_AR0 = 15;
	tail_incr_AR0 = 1;
	head_AR0 = 4141;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg15_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg15_init_g0() {
	tail_AR0 = 4141;
	tail_incr_AR0 = 1;
	head_AR0 = 4141;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg15_init_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum15_init_g0() {
	tail_AR0 = 4141;
	tail_incr_AR0 = 1;
	head_AR0 = 4398;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum15_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums15_init_g0() {
	tail_AR0 = 4141;
	tail_incr_AR0 = 1;
	head_AR0 = 4141;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums15_init_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division15_init_g0() {
	tail_AR0 = 4398;
	tail_incr_AR0 = 1;
	tail_AR1 = 4141;
	tail_incr_AR1 = 1;
	head_AR0 = 4398;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division15_init_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_SecondCounter0_init_g0() {
	tail_AR0 = 528;
	tail_incr_AR0 = 1;
}

static inline void resetAR_SecondCounter0_init_g0i() {
	tail_AR0 += 257;
}

static inline void resetAR_src0_steady_g0() {
	head_AR0 = 255;
	head_incr_AR0 = 1;
}

static inline void resetAR_src0_steady_g0i() {
}

static inline void resetAR_GetAvg0_steady_g0() {
	tail_AR0 = 0;
	tail_incr_AR0 = 1;
	head_AR0 = 271;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg0_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg0_steady_g0() {
	tail_AR0 = 271;
	tail_incr_AR0 = 1;
	head_AR0 = 271;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg0_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum0_steady_g0() {
	tail_AR0 = 271;
	tail_incr_AR0 = 1;
	head_AR0 = 528;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum0_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums0_steady_g0() {
	tail_AR0 = 271;
	tail_incr_AR0 = 1;
	head_AR0 = 271;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums0_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division0_steady_g0() {
	tail_AR0 = 528;
	tail_incr_AR0 = 1;
	tail_AR1 = 271;
	tail_incr_AR1 = 1;
	head_AR0 = 528;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division0_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg1_steady_g0() {
	tail_AR0 = 1;
	tail_incr_AR0 = 1;
	head_AR0 = 529;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg1_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg1_steady_g0() {
	tail_AR0 = 529;
	tail_incr_AR0 = 1;
	head_AR0 = 529;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg1_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum1_steady_g0() {
	tail_AR0 = 529;
	tail_incr_AR0 = 1;
	head_AR0 = 786;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum1_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums1_steady_g0() {
	tail_AR0 = 529;
	tail_incr_AR0 = 1;
	head_AR0 = 529;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums1_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division1_steady_g0() {
	tail_AR0 = 786;
	tail_incr_AR0 = 1;
	tail_AR1 = 529;
	tail_incr_AR1 = 1;
	head_AR0 = 786;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division1_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg2_steady_g0() {
	tail_AR0 = 2;
	tail_incr_AR0 = 1;
	head_AR0 = 787;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg2_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg2_steady_g0() {
	tail_AR0 = 787;
	tail_incr_AR0 = 1;
	head_AR0 = 787;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg2_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum2_steady_g0() {
	tail_AR0 = 787;
	tail_incr_AR0 = 1;
	head_AR0 = 1044;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum2_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums2_steady_g0() {
	tail_AR0 = 787;
	tail_incr_AR0 = 1;
	head_AR0 = 787;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums2_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division2_steady_g0() {
	tail_AR0 = 1044;
	tail_incr_AR0 = 1;
	tail_AR1 = 787;
	tail_incr_AR1 = 1;
	head_AR0 = 1044;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division2_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg3_steady_g0() {
	tail_AR0 = 3;
	tail_incr_AR0 = 1;
	head_AR0 = 1045;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg3_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg3_steady_g0() {
	tail_AR0 = 1045;
	tail_incr_AR0 = 1;
	head_AR0 = 1045;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg3_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum3_steady_g0() {
	tail_AR0 = 1045;
	tail_incr_AR0 = 1;
	head_AR0 = 1302;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum3_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums3_steady_g0() {
	tail_AR0 = 1045;
	tail_incr_AR0 = 1;
	head_AR0 = 1045;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums3_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division3_steady_g0() {
	tail_AR0 = 1302;
	tail_incr_AR0 = 1;
	tail_AR1 = 1045;
	tail_incr_AR1 = 1;
	head_AR0 = 1302;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division3_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg4_steady_g0() {
	tail_AR0 = 4;
	tail_incr_AR0 = 1;
	head_AR0 = 1303;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg4_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg4_steady_g0() {
	tail_AR0 = 1303;
	tail_incr_AR0 = 1;
	head_AR0 = 1303;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg4_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum4_steady_g0() {
	tail_AR0 = 1303;
	tail_incr_AR0 = 1;
	head_AR0 = 1560;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum4_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums4_steady_g0() {
	tail_AR0 = 1303;
	tail_incr_AR0 = 1;
	head_AR0 = 1303;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums4_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division4_steady_g0() {
	tail_AR0 = 1560;
	tail_incr_AR0 = 1;
	tail_AR1 = 1303;
	tail_incr_AR1 = 1;
	head_AR0 = 1560;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division4_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg5_steady_g0() {
	tail_AR0 = 5;
	tail_incr_AR0 = 1;
	head_AR0 = 1561;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg5_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg5_steady_g0() {
	tail_AR0 = 1561;
	tail_incr_AR0 = 1;
	head_AR0 = 1561;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg5_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum5_steady_g0() {
	tail_AR0 = 1561;
	tail_incr_AR0 = 1;
	head_AR0 = 1818;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum5_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums5_steady_g0() {
	tail_AR0 = 1561;
	tail_incr_AR0 = 1;
	head_AR0 = 1561;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums5_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division5_steady_g0() {
	tail_AR0 = 1818;
	tail_incr_AR0 = 1;
	tail_AR1 = 1561;
	tail_incr_AR1 = 1;
	head_AR0 = 1818;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division5_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg6_steady_g0() {
	tail_AR0 = 6;
	tail_incr_AR0 = 1;
	head_AR0 = 1819;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg6_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg6_steady_g0() {
	tail_AR0 = 1819;
	tail_incr_AR0 = 1;
	head_AR0 = 1819;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg6_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum6_steady_g0() {
	tail_AR0 = 1819;
	tail_incr_AR0 = 1;
	head_AR0 = 2076;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum6_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums6_steady_g0() {
	tail_AR0 = 1819;
	tail_incr_AR0 = 1;
	head_AR0 = 1819;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums6_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division6_steady_g0() {
	tail_AR0 = 2076;
	tail_incr_AR0 = 1;
	tail_AR1 = 1819;
	tail_incr_AR1 = 1;
	head_AR0 = 2076;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division6_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg7_steady_g0() {
	tail_AR0 = 7;
	tail_incr_AR0 = 1;
	head_AR0 = 2077;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg7_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg7_steady_g0() {
	tail_AR0 = 2077;
	tail_incr_AR0 = 1;
	head_AR0 = 2077;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg7_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum7_steady_g0() {
	tail_AR0 = 2077;
	tail_incr_AR0 = 1;
	head_AR0 = 2334;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum7_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums7_steady_g0() {
	tail_AR0 = 2077;
	tail_incr_AR0 = 1;
	head_AR0 = 2077;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums7_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division7_steady_g0() {
	tail_AR0 = 2334;
	tail_incr_AR0 = 1;
	tail_AR1 = 2077;
	tail_incr_AR1 = 1;
	head_AR0 = 2334;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division7_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg8_steady_g0() {
	tail_AR0 = 8;
	tail_incr_AR0 = 1;
	head_AR0 = 2335;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg8_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg8_steady_g0() {
	tail_AR0 = 2335;
	tail_incr_AR0 = 1;
	head_AR0 = 2335;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg8_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum8_steady_g0() {
	tail_AR0 = 2335;
	tail_incr_AR0 = 1;
	head_AR0 = 2592;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum8_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums8_steady_g0() {
	tail_AR0 = 2335;
	tail_incr_AR0 = 1;
	head_AR0 = 2335;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums8_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division8_steady_g0() {
	tail_AR0 = 2592;
	tail_incr_AR0 = 1;
	tail_AR1 = 2335;
	tail_incr_AR1 = 1;
	head_AR0 = 2592;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division8_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg9_steady_g0() {
	tail_AR0 = 9;
	tail_incr_AR0 = 1;
	head_AR0 = 2593;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg9_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg9_steady_g0() {
	tail_AR0 = 2593;
	tail_incr_AR0 = 1;
	head_AR0 = 2593;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg9_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum9_steady_g0() {
	tail_AR0 = 2593;
	tail_incr_AR0 = 1;
	head_AR0 = 2850;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum9_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums9_steady_g0() {
	tail_AR0 = 2593;
	tail_incr_AR0 = 1;
	head_AR0 = 2593;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums9_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division9_steady_g0() {
	tail_AR0 = 2850;
	tail_incr_AR0 = 1;
	tail_AR1 = 2593;
	tail_incr_AR1 = 1;
	head_AR0 = 2850;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division9_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg10_steady_g0() {
	tail_AR0 = 10;
	tail_incr_AR0 = 1;
	head_AR0 = 2851;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg10_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg10_steady_g0() {
	tail_AR0 = 2851;
	tail_incr_AR0 = 1;
	head_AR0 = 2851;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg10_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum10_steady_g0() {
	tail_AR0 = 2851;
	tail_incr_AR0 = 1;
	head_AR0 = 3108;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum10_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums10_steady_g0() {
	tail_AR0 = 2851;
	tail_incr_AR0 = 1;
	head_AR0 = 2851;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums10_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division10_steady_g0() {
	tail_AR0 = 3108;
	tail_incr_AR0 = 1;
	tail_AR1 = 2851;
	tail_incr_AR1 = 1;
	head_AR0 = 3108;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division10_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg11_steady_g0() {
	tail_AR0 = 11;
	tail_incr_AR0 = 1;
	head_AR0 = 3109;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg11_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg11_steady_g0() {
	tail_AR0 = 3109;
	tail_incr_AR0 = 1;
	head_AR0 = 3109;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg11_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum11_steady_g0() {
	tail_AR0 = 3109;
	tail_incr_AR0 = 1;
	head_AR0 = 3366;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum11_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums11_steady_g0() {
	tail_AR0 = 3109;
	tail_incr_AR0 = 1;
	head_AR0 = 3109;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums11_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division11_steady_g0() {
	tail_AR0 = 3366;
	tail_incr_AR0 = 1;
	tail_AR1 = 3109;
	tail_incr_AR1 = 1;
	head_AR0 = 3366;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division11_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg12_steady_g0() {
	tail_AR0 = 12;
	tail_incr_AR0 = 1;
	head_AR0 = 3367;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg12_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg12_steady_g0() {
	tail_AR0 = 3367;
	tail_incr_AR0 = 1;
	head_AR0 = 3367;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg12_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum12_steady_g0() {
	tail_AR0 = 3367;
	tail_incr_AR0 = 1;
	head_AR0 = 3624;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum12_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums12_steady_g0() {
	tail_AR0 = 3367;
	tail_incr_AR0 = 1;
	head_AR0 = 3367;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums12_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division12_steady_g0() {
	tail_AR0 = 3624;
	tail_incr_AR0 = 1;
	tail_AR1 = 3367;
	tail_incr_AR1 = 1;
	head_AR0 = 3624;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division12_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg13_steady_g0() {
	tail_AR0 = 13;
	tail_incr_AR0 = 1;
	head_AR0 = 3625;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg13_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg13_steady_g0() {
	tail_AR0 = 3625;
	tail_incr_AR0 = 1;
	head_AR0 = 3625;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg13_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum13_steady_g0() {
	tail_AR0 = 3625;
	tail_incr_AR0 = 1;
	head_AR0 = 3882;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum13_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums13_steady_g0() {
	tail_AR0 = 3625;
	tail_incr_AR0 = 1;
	head_AR0 = 3625;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums13_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division13_steady_g0() {
	tail_AR0 = 3882;
	tail_incr_AR0 = 1;
	tail_AR1 = 3625;
	tail_incr_AR1 = 1;
	head_AR0 = 3882;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division13_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg14_steady_g0() {
	tail_AR0 = 14;
	tail_incr_AR0 = 1;
	head_AR0 = 3883;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg14_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg14_steady_g0() {
	tail_AR0 = 3883;
	tail_incr_AR0 = 1;
	head_AR0 = 3883;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg14_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum14_steady_g0() {
	tail_AR0 = 3883;
	tail_incr_AR0 = 1;
	head_AR0 = 4140;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum14_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums14_steady_g0() {
	tail_AR0 = 3883;
	tail_incr_AR0 = 1;
	head_AR0 = 3883;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums14_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division14_steady_g0() {
	tail_AR0 = 4140;
	tail_incr_AR0 = 1;
	tail_AR1 = 3883;
	tail_incr_AR1 = 1;
	head_AR0 = 4140;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division14_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_GetAvg15_steady_g0() {
	tail_AR0 = 15;
	tail_incr_AR0 = 1;
	head_AR0 = 4141;
	head_incr_AR0 = 1;
}

static inline void resetAR_GetAvg15_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -257;
}

static inline void resetAR_DataMinusAvg15_steady_g0() {
	tail_AR0 = 4141;
	tail_incr_AR0 = 1;
	head_AR0 = 4141;
	head_incr_AR0 = 1;
}

static inline void resetAR_DataMinusAvg15_steady_g0i() {
	tail_AR0 += -257;
	head_AR0 += -256;
}

static inline void resetAR_DenomDataSum15_steady_g0() {
	tail_AR0 = 4141;
	tail_incr_AR0 = 1;
	head_AR0 = 4398;
	head_incr_AR0 = 1;
}

static inline void resetAR_DenomDataSum15_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -1;
}

static inline void resetAR_PatternSums15_steady_g0() {
	tail_AR0 = 4141;
	tail_incr_AR0 = 1;
	head_AR0 = 4141;
	head_incr_AR0 = 1;
}

static inline void resetAR_PatternSums15_steady_g0i() {
	tail_AR0 += -256;
	head_AR0 += -2;
}

static inline void resetAR_Division15_steady_g0() {
	tail_AR0 = 4398;
	tail_incr_AR0 = 1;
	tail_AR1 = 4141;
	tail_incr_AR1 = 1;
	head_AR0 = 4398;
	head_incr_AR0 = 1;
}

static inline void resetAR_Division15_steady_g0i() {
	tail_AR0 += -1;
	tail_AR1 += -2;
	head_AR0 += -1;
}

static inline void resetAR_SecondCounter0_steady_g0() {
	tail_AR0 = 528;
	tail_incr_AR0 = 1;
}

static inline void resetAR_SecondCounter0_steady_g0i() {
	tail_AR0 += 257;
}

char src0_remainder[1020];
static inline void Framer0_save_init() {
	memcpy(src0_remainder + 0, SF0 + 64, 1020);
}

static inline void Framer0_save_steady() {
	memcpy(src0_remainder + 0, SF0 + 64, 956);
	memcpy(src0_remainder + 956, SF0 + 1020, 64);
}

static inline void Framer0_load_steady() {
	memcpy(SF0 + 0, src0_remainder + 0, 956);
	memcpy(SF0 + 956, src0_remainder + 956, 64);
}


float FRAMES_PER_SECOND = 44100.0f;
int PATTERN_SIZE = 256;
float PATTERN_AVG;
float pattern[256];
static inline void TheGlobal0_init() {
	float sineValue;
	int i;
	float pos;
	float sum = 0;
	for(i = 0; (i)<(PATTERN_SIZE); (i)++) {
		pos = ((((float)i))/(((float)PATTERN_SIZE)))*(0.05f);
		sineValue = ((float)cosf((((3.141592653589793f)*(2.0f))*(((20000.0f)*(pos))+(500.0f)))*(pos)));
		sum += sineValue;
		pattern[i] = sineValue;
	}

	PATTERN_AVG = (sum)/(((float)PATTERN_SIZE));
}

static short* samples;

float src0_short2float(int sample) {
	return ((float)sample);
}

static inline void src0_work0() {
    push_AR0_incr(src0_short2float(*samples++));
}

static inline void GetAvg0_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg0_avg;
int DataMinusAvg0_i;
static inline void DataMinusAvg0_work0() {
	DataMinusAvg0_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg0_i = 0; (DataMinusAvg0_i)<(256); (DataMinusAvg0_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg0_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum0_data;
int DenomDataSum0_i;
static inline void DenomDataSum0_work0() {
	float sum = 0;
	for(DenomDataSum0_i = 0; (DenomDataSum0_i)<(256); (DenomDataSum0_i)++) {
		DenomDataSum0_data = pop_AR0_incr();
		sum += (DenomDataSum0_data)*(DenomDataSum0_data);
	}

	push_AR0_incr(sum);
}

float PatternSums0_numerSum;
float PatternSums0_denomPatternSum;
float PatternSums0_data;
float PatternSums0_patternData;
int PatternSums0_i;
static inline void PatternSums0_work0() {
	PatternSums0_numerSum = 0;
	PatternSums0_denomPatternSum = 0;
	for(PatternSums0_i = 0; (PatternSums0_i)<(256); (PatternSums0_i)++) {
		PatternSums0_patternData = (pattern[PatternSums0_i])-(PATTERN_AVG);
		PatternSums0_data = pop_AR0_incr();
		PatternSums0_numerSum += (PatternSums0_data)*(PatternSums0_patternData);
		PatternSums0_denomPatternSum += (PatternSums0_patternData)*(PatternSums0_patternData);
	}

	push_AR0_incr(PatternSums0_denomPatternSum);
	push_AR0_incr(PatternSums0_numerSum);
}

float Division0_numer;
float Division0_denom_x;
float Division0_denom_y;
float Division0_denom;
static inline void Division0_work0() {
	float v = 0;
	Division0_denom_x = pop_AR0_incr();
	Division0_denom_y = pop_AR1_incr();
	Division0_denom = sqrtf((Division0_denom_x)*(Division0_denom_y));
	Division0_numer = pop_AR1_incr();
	if((Division0_denom)!=(0)) {
		v = (Division0_numer)/(Division0_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg1_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg1_avg;
int DataMinusAvg1_i;
static inline void DataMinusAvg1_work0() {
	DataMinusAvg1_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg1_i = 0; (DataMinusAvg1_i)<(256); (DataMinusAvg1_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg1_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum1_data;
int DenomDataSum1_i;
static inline void DenomDataSum1_work0() {
	float sum = 0;
	for(DenomDataSum1_i = 0; (DenomDataSum1_i)<(256); (DenomDataSum1_i)++) {
		DenomDataSum1_data = pop_AR0_incr();
		sum += (DenomDataSum1_data)*(DenomDataSum1_data);
	}

	push_AR0_incr(sum);
}

float PatternSums1_numerSum;
float PatternSums1_denomPatternSum;
float PatternSums1_data;
float PatternSums1_patternData;
int PatternSums1_i;
static inline void PatternSums1_work0() {
	PatternSums1_numerSum = 0;
	PatternSums1_denomPatternSum = 0;
	for(PatternSums1_i = 0; (PatternSums1_i)<(256); (PatternSums1_i)++) {
		PatternSums1_patternData = (pattern[PatternSums1_i])-(PATTERN_AVG);
		PatternSums1_data = pop_AR0_incr();
		PatternSums1_numerSum += (PatternSums1_data)*(PatternSums1_patternData);
		PatternSums1_denomPatternSum += (PatternSums1_patternData)*(PatternSums1_patternData);
	}

	push_AR0_incr(PatternSums1_denomPatternSum);
	push_AR0_incr(PatternSums1_numerSum);
}

float Division1_numer;
float Division1_denom_x;
float Division1_denom_y;
float Division1_denom;
static inline void Division1_work0() {
	float v = 0;
	Division1_denom_x = pop_AR0_incr();
	Division1_denom_y = pop_AR1_incr();
	Division1_denom = sqrtf((Division1_denom_x)*(Division1_denom_y));
	Division1_numer = pop_AR1_incr();
	if((Division1_denom)!=(0)) {
		v = (Division1_numer)/(Division1_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg2_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg2_avg;
int DataMinusAvg2_i;
static inline void DataMinusAvg2_work0() {
	DataMinusAvg2_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg2_i = 0; (DataMinusAvg2_i)<(256); (DataMinusAvg2_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg2_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum2_data;
int DenomDataSum2_i;
static inline void DenomDataSum2_work0() {
	float sum = 0;
	for(DenomDataSum2_i = 0; (DenomDataSum2_i)<(256); (DenomDataSum2_i)++) {
		DenomDataSum2_data = pop_AR0_incr();
		sum += (DenomDataSum2_data)*(DenomDataSum2_data);
	}

	push_AR0_incr(sum);
}

float PatternSums2_numerSum;
float PatternSums2_denomPatternSum;
float PatternSums2_data;
float PatternSums2_patternData;
int PatternSums2_i;
static inline void PatternSums2_work0() {
	PatternSums2_numerSum = 0;
	PatternSums2_denomPatternSum = 0;
	for(PatternSums2_i = 0; (PatternSums2_i)<(256); (PatternSums2_i)++) {
		PatternSums2_patternData = (pattern[PatternSums2_i])-(PATTERN_AVG);
		PatternSums2_data = pop_AR0_incr();
		PatternSums2_numerSum += (PatternSums2_data)*(PatternSums2_patternData);
		PatternSums2_denomPatternSum += (PatternSums2_patternData)*(PatternSums2_patternData);
	}

	push_AR0_incr(PatternSums2_denomPatternSum);
	push_AR0_incr(PatternSums2_numerSum);
}

float Division2_numer;
float Division2_denom_x;
float Division2_denom_y;
float Division2_denom;
static inline void Division2_work0() {
	float v = 0;
	Division2_denom_x = pop_AR0_incr();
	Division2_denom_y = pop_AR1_incr();
	Division2_denom = sqrtf((Division2_denom_x)*(Division2_denom_y));
	Division2_numer = pop_AR1_incr();
	if((Division2_denom)!=(0)) {
		v = (Division2_numer)/(Division2_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg3_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg3_avg;
int DataMinusAvg3_i;
static inline void DataMinusAvg3_work0() {
	DataMinusAvg3_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg3_i = 0; (DataMinusAvg3_i)<(256); (DataMinusAvg3_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg3_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum3_data;
int DenomDataSum3_i;
static inline void DenomDataSum3_work0() {
	float sum = 0;
	for(DenomDataSum3_i = 0; (DenomDataSum3_i)<(256); (DenomDataSum3_i)++) {
		DenomDataSum3_data = pop_AR0_incr();
		sum += (DenomDataSum3_data)*(DenomDataSum3_data);
	}

	push_AR0_incr(sum);
}

float PatternSums3_numerSum;
float PatternSums3_denomPatternSum;
float PatternSums3_data;
float PatternSums3_patternData;
int PatternSums3_i;
static inline void PatternSums3_work0() {
	PatternSums3_numerSum = 0;
	PatternSums3_denomPatternSum = 0;
	for(PatternSums3_i = 0; (PatternSums3_i)<(256); (PatternSums3_i)++) {
		PatternSums3_patternData = (pattern[PatternSums3_i])-(PATTERN_AVG);
		PatternSums3_data = pop_AR0_incr();
		PatternSums3_numerSum += (PatternSums3_data)*(PatternSums3_patternData);
		PatternSums3_denomPatternSum += (PatternSums3_patternData)*(PatternSums3_patternData);
	}

	push_AR0_incr(PatternSums3_denomPatternSum);
	push_AR0_incr(PatternSums3_numerSum);
}

float Division3_numer;
float Division3_denom_x;
float Division3_denom_y;
float Division3_denom;
static inline void Division3_work0() {
	float v = 0;
	Division3_denom_x = pop_AR0_incr();
	Division3_denom_y = pop_AR1_incr();
	Division3_denom = sqrtf((Division3_denom_x)*(Division3_denom_y));
	Division3_numer = pop_AR1_incr();
	if((Division3_denom)!=(0)) {
		v = (Division3_numer)/(Division3_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg4_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg4_avg;
int DataMinusAvg4_i;
static inline void DataMinusAvg4_work0() {
	DataMinusAvg4_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg4_i = 0; (DataMinusAvg4_i)<(256); (DataMinusAvg4_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg4_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum4_data;
int DenomDataSum4_i;
static inline void DenomDataSum4_work0() {
	float sum = 0;
	for(DenomDataSum4_i = 0; (DenomDataSum4_i)<(256); (DenomDataSum4_i)++) {
		DenomDataSum4_data = pop_AR0_incr();
		sum += (DenomDataSum4_data)*(DenomDataSum4_data);
	}

	push_AR0_incr(sum);
}

float PatternSums4_numerSum;
float PatternSums4_denomPatternSum;
float PatternSums4_data;
float PatternSums4_patternData;
int PatternSums4_i;
static inline void PatternSums4_work0() {
	PatternSums4_numerSum = 0;
	PatternSums4_denomPatternSum = 0;
	for(PatternSums4_i = 0; (PatternSums4_i)<(256); (PatternSums4_i)++) {
		PatternSums4_patternData = (pattern[PatternSums4_i])-(PATTERN_AVG);
		PatternSums4_data = pop_AR0_incr();
		PatternSums4_numerSum += (PatternSums4_data)*(PatternSums4_patternData);
		PatternSums4_denomPatternSum += (PatternSums4_patternData)*(PatternSums4_patternData);
	}

	push_AR0_incr(PatternSums4_denomPatternSum);
	push_AR0_incr(PatternSums4_numerSum);
}

float Division4_numer;
float Division4_denom_x;
float Division4_denom_y;
float Division4_denom;
static inline void Division4_work0() {
	float v = 0;
	Division4_denom_x = pop_AR0_incr();
	Division4_denom_y = pop_AR1_incr();
	Division4_denom = sqrtf((Division4_denom_x)*(Division4_denom_y));
	Division4_numer = pop_AR1_incr();
	if((Division4_denom)!=(0)) {
		v = (Division4_numer)/(Division4_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg5_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg5_avg;
int DataMinusAvg5_i;
static inline void DataMinusAvg5_work0() {
	DataMinusAvg5_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg5_i = 0; (DataMinusAvg5_i)<(256); (DataMinusAvg5_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg5_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum5_data;
int DenomDataSum5_i;
static inline void DenomDataSum5_work0() {
	float sum = 0;
	for(DenomDataSum5_i = 0; (DenomDataSum5_i)<(256); (DenomDataSum5_i)++) {
		DenomDataSum5_data = pop_AR0_incr();
		sum += (DenomDataSum5_data)*(DenomDataSum5_data);
	}

	push_AR0_incr(sum);
}

float PatternSums5_numerSum;
float PatternSums5_denomPatternSum;
float PatternSums5_data;
float PatternSums5_patternData;
int PatternSums5_i;
static inline void PatternSums5_work0() {
	PatternSums5_numerSum = 0;
	PatternSums5_denomPatternSum = 0;
	for(PatternSums5_i = 0; (PatternSums5_i)<(256); (PatternSums5_i)++) {
		PatternSums5_patternData = (pattern[PatternSums5_i])-(PATTERN_AVG);
		PatternSums5_data = pop_AR0_incr();
		PatternSums5_numerSum += (PatternSums5_data)*(PatternSums5_patternData);
		PatternSums5_denomPatternSum += (PatternSums5_patternData)*(PatternSums5_patternData);
	}

	push_AR0_incr(PatternSums5_denomPatternSum);
	push_AR0_incr(PatternSums5_numerSum);
}

float Division5_numer;
float Division5_denom_x;
float Division5_denom_y;
float Division5_denom;
static inline void Division5_work0() {
	float v = 0;
	Division5_denom_x = pop_AR0_incr();
	Division5_denom_y = pop_AR1_incr();
	Division5_denom = sqrtf((Division5_denom_x)*(Division5_denom_y));
	Division5_numer = pop_AR1_incr();
	if((Division5_denom)!=(0)) {
		v = (Division5_numer)/(Division5_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg6_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg6_avg;
int DataMinusAvg6_i;
static inline void DataMinusAvg6_work0() {
	DataMinusAvg6_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg6_i = 0; (DataMinusAvg6_i)<(256); (DataMinusAvg6_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg6_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum6_data;
int DenomDataSum6_i;
static inline void DenomDataSum6_work0() {
	float sum = 0;
	for(DenomDataSum6_i = 0; (DenomDataSum6_i)<(256); (DenomDataSum6_i)++) {
		DenomDataSum6_data = pop_AR0_incr();
		sum += (DenomDataSum6_data)*(DenomDataSum6_data);
	}

	push_AR0_incr(sum);
}

float PatternSums6_numerSum;
float PatternSums6_denomPatternSum;
float PatternSums6_data;
float PatternSums6_patternData;
int PatternSums6_i;
static inline void PatternSums6_work0() {
	PatternSums6_numerSum = 0;
	PatternSums6_denomPatternSum = 0;
	for(PatternSums6_i = 0; (PatternSums6_i)<(256); (PatternSums6_i)++) {
		PatternSums6_patternData = (pattern[PatternSums6_i])-(PATTERN_AVG);
		PatternSums6_data = pop_AR0_incr();
		PatternSums6_numerSum += (PatternSums6_data)*(PatternSums6_patternData);
		PatternSums6_denomPatternSum += (PatternSums6_patternData)*(PatternSums6_patternData);
	}

	push_AR0_incr(PatternSums6_denomPatternSum);
	push_AR0_incr(PatternSums6_numerSum);
}

float Division6_numer;
float Division6_denom_x;
float Division6_denom_y;
float Division6_denom;
static inline void Division6_work0() {
	float v = 0;
	Division6_denom_x = pop_AR0_incr();
	Division6_denom_y = pop_AR1_incr();
	Division6_denom = sqrtf((Division6_denom_x)*(Division6_denom_y));
	Division6_numer = pop_AR1_incr();
	if((Division6_denom)!=(0)) {
		v = (Division6_numer)/(Division6_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg7_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg7_avg;
int DataMinusAvg7_i;
static inline void DataMinusAvg7_work0() {
	DataMinusAvg7_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg7_i = 0; (DataMinusAvg7_i)<(256); (DataMinusAvg7_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg7_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum7_data;
int DenomDataSum7_i;
static inline void DenomDataSum7_work0() {
	float sum = 0;
	for(DenomDataSum7_i = 0; (DenomDataSum7_i)<(256); (DenomDataSum7_i)++) {
		DenomDataSum7_data = pop_AR0_incr();
		sum += (DenomDataSum7_data)*(DenomDataSum7_data);
	}

	push_AR0_incr(sum);
}

float PatternSums7_numerSum;
float PatternSums7_denomPatternSum;
float PatternSums7_data;
float PatternSums7_patternData;
int PatternSums7_i;
static inline void PatternSums7_work0() {
	PatternSums7_numerSum = 0;
	PatternSums7_denomPatternSum = 0;
	for(PatternSums7_i = 0; (PatternSums7_i)<(256); (PatternSums7_i)++) {
		PatternSums7_patternData = (pattern[PatternSums7_i])-(PATTERN_AVG);
		PatternSums7_data = pop_AR0_incr();
		PatternSums7_numerSum += (PatternSums7_data)*(PatternSums7_patternData);
		PatternSums7_denomPatternSum += (PatternSums7_patternData)*(PatternSums7_patternData);
	}

	push_AR0_incr(PatternSums7_denomPatternSum);
	push_AR0_incr(PatternSums7_numerSum);
}

float Division7_numer;
float Division7_denom_x;
float Division7_denom_y;
float Division7_denom;
static inline void Division7_work0() {
	float v = 0;
	Division7_denom_x = pop_AR0_incr();
	Division7_denom_y = pop_AR1_incr();
	Division7_denom = sqrtf((Division7_denom_x)*(Division7_denom_y));
	Division7_numer = pop_AR1_incr();
	if((Division7_denom)!=(0)) {
		v = (Division7_numer)/(Division7_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg8_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg8_avg;
int DataMinusAvg8_i;
static inline void DataMinusAvg8_work0() {
	DataMinusAvg8_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg8_i = 0; (DataMinusAvg8_i)<(256); (DataMinusAvg8_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg8_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum8_data;
int DenomDataSum8_i;
static inline void DenomDataSum8_work0() {
	float sum = 0;
	for(DenomDataSum8_i = 0; (DenomDataSum8_i)<(256); (DenomDataSum8_i)++) {
		DenomDataSum8_data = pop_AR0_incr();
		sum += (DenomDataSum8_data)*(DenomDataSum8_data);
	}

	push_AR0_incr(sum);
}

float PatternSums8_numerSum;
float PatternSums8_denomPatternSum;
float PatternSums8_data;
float PatternSums8_patternData;
int PatternSums8_i;
static inline void PatternSums8_work0() {
	PatternSums8_numerSum = 0;
	PatternSums8_denomPatternSum = 0;
	for(PatternSums8_i = 0; (PatternSums8_i)<(256); (PatternSums8_i)++) {
		PatternSums8_patternData = (pattern[PatternSums8_i])-(PATTERN_AVG);
		PatternSums8_data = pop_AR0_incr();
		PatternSums8_numerSum += (PatternSums8_data)*(PatternSums8_patternData);
		PatternSums8_denomPatternSum += (PatternSums8_patternData)*(PatternSums8_patternData);
	}

	push_AR0_incr(PatternSums8_denomPatternSum);
	push_AR0_incr(PatternSums8_numerSum);
}

float Division8_numer;
float Division8_denom_x;
float Division8_denom_y;
float Division8_denom;
static inline void Division8_work0() {
	float v = 0;
	Division8_denom_x = pop_AR0_incr();
	Division8_denom_y = pop_AR1_incr();
	Division8_denom = sqrtf((Division8_denom_x)*(Division8_denom_y));
	Division8_numer = pop_AR1_incr();
	if((Division8_denom)!=(0)) {
		v = (Division8_numer)/(Division8_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg9_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg9_avg;
int DataMinusAvg9_i;
static inline void DataMinusAvg9_work0() {
	DataMinusAvg9_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg9_i = 0; (DataMinusAvg9_i)<(256); (DataMinusAvg9_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg9_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum9_data;
int DenomDataSum9_i;
static inline void DenomDataSum9_work0() {
	float sum = 0;
	for(DenomDataSum9_i = 0; (DenomDataSum9_i)<(256); (DenomDataSum9_i)++) {
		DenomDataSum9_data = pop_AR0_incr();
		sum += (DenomDataSum9_data)*(DenomDataSum9_data);
	}

	push_AR0_incr(sum);
}

float PatternSums9_numerSum;
float PatternSums9_denomPatternSum;
float PatternSums9_data;
float PatternSums9_patternData;
int PatternSums9_i;
static inline void PatternSums9_work0() {
	PatternSums9_numerSum = 0;
	PatternSums9_denomPatternSum = 0;
	for(PatternSums9_i = 0; (PatternSums9_i)<(256); (PatternSums9_i)++) {
		PatternSums9_patternData = (pattern[PatternSums9_i])-(PATTERN_AVG);
		PatternSums9_data = pop_AR0_incr();
		PatternSums9_numerSum += (PatternSums9_data)*(PatternSums9_patternData);
		PatternSums9_denomPatternSum += (PatternSums9_patternData)*(PatternSums9_patternData);
	}

	push_AR0_incr(PatternSums9_denomPatternSum);
	push_AR0_incr(PatternSums9_numerSum);
}

float Division9_numer;
float Division9_denom_x;
float Division9_denom_y;
float Division9_denom;
static inline void Division9_work0() {
	float v = 0;
	Division9_denom_x = pop_AR0_incr();
	Division9_denom_y = pop_AR1_incr();
	Division9_denom = sqrtf((Division9_denom_x)*(Division9_denom_y));
	Division9_numer = pop_AR1_incr();
	if((Division9_denom)!=(0)) {
		v = (Division9_numer)/(Division9_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg10_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg10_avg;
int DataMinusAvg10_i;
static inline void DataMinusAvg10_work0() {
	DataMinusAvg10_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg10_i = 0; (DataMinusAvg10_i)<(256); (DataMinusAvg10_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg10_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum10_data;
int DenomDataSum10_i;
static inline void DenomDataSum10_work0() {
	float sum = 0;
	for(DenomDataSum10_i = 0; (DenomDataSum10_i)<(256); (DenomDataSum10_i)++) {
		DenomDataSum10_data = pop_AR0_incr();
		sum += (DenomDataSum10_data)*(DenomDataSum10_data);
	}

	push_AR0_incr(sum);
}

float PatternSums10_numerSum;
float PatternSums10_denomPatternSum;
float PatternSums10_data;
float PatternSums10_patternData;
int PatternSums10_i;
static inline void PatternSums10_work0() {
	PatternSums10_numerSum = 0;
	PatternSums10_denomPatternSum = 0;
	for(PatternSums10_i = 0; (PatternSums10_i)<(256); (PatternSums10_i)++) {
		PatternSums10_patternData = (pattern[PatternSums10_i])-(PATTERN_AVG);
		PatternSums10_data = pop_AR0_incr();
		PatternSums10_numerSum += (PatternSums10_data)*(PatternSums10_patternData);
		PatternSums10_denomPatternSum += (PatternSums10_patternData)*(PatternSums10_patternData);
	}

	push_AR0_incr(PatternSums10_denomPatternSum);
	push_AR0_incr(PatternSums10_numerSum);
}

float Division10_numer;
float Division10_denom_x;
float Division10_denom_y;
float Division10_denom;
static inline void Division10_work0() {
	float v = 0;
	Division10_denom_x = pop_AR0_incr();
	Division10_denom_y = pop_AR1_incr();
	Division10_denom = sqrtf((Division10_denom_x)*(Division10_denom_y));
	Division10_numer = pop_AR1_incr();
	if((Division10_denom)!=(0)) {
		v = (Division10_numer)/(Division10_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg11_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg11_avg;
int DataMinusAvg11_i;
static inline void DataMinusAvg11_work0() {
	DataMinusAvg11_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg11_i = 0; (DataMinusAvg11_i)<(256); (DataMinusAvg11_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg11_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum11_data;
int DenomDataSum11_i;
static inline void DenomDataSum11_work0() {
	float sum = 0;
	for(DenomDataSum11_i = 0; (DenomDataSum11_i)<(256); (DenomDataSum11_i)++) {
		DenomDataSum11_data = pop_AR0_incr();
		sum += (DenomDataSum11_data)*(DenomDataSum11_data);
	}

	push_AR0_incr(sum);
}

float PatternSums11_numerSum;
float PatternSums11_denomPatternSum;
float PatternSums11_data;
float PatternSums11_patternData;
int PatternSums11_i;
static inline void PatternSums11_work0() {
	PatternSums11_numerSum = 0;
	PatternSums11_denomPatternSum = 0;
	for(PatternSums11_i = 0; (PatternSums11_i)<(256); (PatternSums11_i)++) {
		PatternSums11_patternData = (pattern[PatternSums11_i])-(PATTERN_AVG);
		PatternSums11_data = pop_AR0_incr();
		PatternSums11_numerSum += (PatternSums11_data)*(PatternSums11_patternData);
		PatternSums11_denomPatternSum += (PatternSums11_patternData)*(PatternSums11_patternData);
	}

	push_AR0_incr(PatternSums11_denomPatternSum);
	push_AR0_incr(PatternSums11_numerSum);
}

float Division11_numer;
float Division11_denom_x;
float Division11_denom_y;
float Division11_denom;
static inline void Division11_work0() {
	float v = 0;
	Division11_denom_x = pop_AR0_incr();
	Division11_denom_y = pop_AR1_incr();
	Division11_denom = sqrtf((Division11_denom_x)*(Division11_denom_y));
	Division11_numer = pop_AR1_incr();
	if((Division11_denom)!=(0)) {
		v = (Division11_numer)/(Division11_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg12_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg12_avg;
int DataMinusAvg12_i;
static inline void DataMinusAvg12_work0() {
	DataMinusAvg12_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg12_i = 0; (DataMinusAvg12_i)<(256); (DataMinusAvg12_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg12_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum12_data;
int DenomDataSum12_i;
static inline void DenomDataSum12_work0() {
	float sum = 0;
	for(DenomDataSum12_i = 0; (DenomDataSum12_i)<(256); (DenomDataSum12_i)++) {
		DenomDataSum12_data = pop_AR0_incr();
		sum += (DenomDataSum12_data)*(DenomDataSum12_data);
	}

	push_AR0_incr(sum);
}

float PatternSums12_numerSum;
float PatternSums12_denomPatternSum;
float PatternSums12_data;
float PatternSums12_patternData;
int PatternSums12_i;
static inline void PatternSums12_work0() {
	PatternSums12_numerSum = 0;
	PatternSums12_denomPatternSum = 0;
	for(PatternSums12_i = 0; (PatternSums12_i)<(256); (PatternSums12_i)++) {
		PatternSums12_patternData = (pattern[PatternSums12_i])-(PATTERN_AVG);
		PatternSums12_data = pop_AR0_incr();
		PatternSums12_numerSum += (PatternSums12_data)*(PatternSums12_patternData);
		PatternSums12_denomPatternSum += (PatternSums12_patternData)*(PatternSums12_patternData);
	}

	push_AR0_incr(PatternSums12_denomPatternSum);
	push_AR0_incr(PatternSums12_numerSum);
}

float Division12_numer;
float Division12_denom_x;
float Division12_denom_y;
float Division12_denom;
static inline void Division12_work0() {
	float v = 0;
	Division12_denom_x = pop_AR0_incr();
	Division12_denom_y = pop_AR1_incr();
	Division12_denom = sqrtf((Division12_denom_x)*(Division12_denom_y));
	Division12_numer = pop_AR1_incr();
	if((Division12_denom)!=(0)) {
		v = (Division12_numer)/(Division12_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg13_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg13_avg;
int DataMinusAvg13_i;
static inline void DataMinusAvg13_work0() {
	DataMinusAvg13_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg13_i = 0; (DataMinusAvg13_i)<(256); (DataMinusAvg13_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg13_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum13_data;
int DenomDataSum13_i;
static inline void DenomDataSum13_work0() {
	float sum = 0;
	for(DenomDataSum13_i = 0; (DenomDataSum13_i)<(256); (DenomDataSum13_i)++) {
		DenomDataSum13_data = pop_AR0_incr();
		sum += (DenomDataSum13_data)*(DenomDataSum13_data);
	}

	push_AR0_incr(sum);
}

float PatternSums13_numerSum;
float PatternSums13_denomPatternSum;
float PatternSums13_data;
float PatternSums13_patternData;
int PatternSums13_i;
static inline void PatternSums13_work0() {
	PatternSums13_numerSum = 0;
	PatternSums13_denomPatternSum = 0;
	for(PatternSums13_i = 0; (PatternSums13_i)<(256); (PatternSums13_i)++) {
		PatternSums13_patternData = (pattern[PatternSums13_i])-(PATTERN_AVG);
		PatternSums13_data = pop_AR0_incr();
		PatternSums13_numerSum += (PatternSums13_data)*(PatternSums13_patternData);
		PatternSums13_denomPatternSum += (PatternSums13_patternData)*(PatternSums13_patternData);
	}

	push_AR0_incr(PatternSums13_denomPatternSum);
	push_AR0_incr(PatternSums13_numerSum);
}

float Division13_numer;
float Division13_denom_x;
float Division13_denom_y;
float Division13_denom;
static inline void Division13_work0() {
	float v = 0;
	Division13_denom_x = pop_AR0_incr();
	Division13_denom_y = pop_AR1_incr();
	Division13_denom = sqrtf((Division13_denom_x)*(Division13_denom_y));
	Division13_numer = pop_AR1_incr();
	if((Division13_denom)!=(0)) {
		v = (Division13_numer)/(Division13_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg14_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg14_avg;
int DataMinusAvg14_i;
static inline void DataMinusAvg14_work0() {
	DataMinusAvg14_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg14_i = 0; (DataMinusAvg14_i)<(256); (DataMinusAvg14_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg14_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum14_data;
int DenomDataSum14_i;
static inline void DenomDataSum14_work0() {
	float sum = 0;
	for(DenomDataSum14_i = 0; (DenomDataSum14_i)<(256); (DenomDataSum14_i)++) {
		DenomDataSum14_data = pop_AR0_incr();
		sum += (DenomDataSum14_data)*(DenomDataSum14_data);
	}

	push_AR0_incr(sum);
}

float PatternSums14_numerSum;
float PatternSums14_denomPatternSum;
float PatternSums14_data;
float PatternSums14_patternData;
int PatternSums14_i;
static inline void PatternSums14_work0() {
	PatternSums14_numerSum = 0;
	PatternSums14_denomPatternSum = 0;
	for(PatternSums14_i = 0; (PatternSums14_i)<(256); (PatternSums14_i)++) {
		PatternSums14_patternData = (pattern[PatternSums14_i])-(PATTERN_AVG);
		PatternSums14_data = pop_AR0_incr();
		PatternSums14_numerSum += (PatternSums14_data)*(PatternSums14_patternData);
		PatternSums14_denomPatternSum += (PatternSums14_patternData)*(PatternSums14_patternData);
	}

	push_AR0_incr(PatternSums14_denomPatternSum);
	push_AR0_incr(PatternSums14_numerSum);
}

float Division14_numer;
float Division14_denom_x;
float Division14_denom_y;
float Division14_denom;
static inline void Division14_work0() {
	float v = 0;
	Division14_denom_x = pop_AR0_incr();
	Division14_denom_y = pop_AR1_incr();
	Division14_denom = sqrtf((Division14_denom_x)*(Division14_denom_y));
	Division14_numer = pop_AR1_incr();
	if((Division14_denom)!=(0)) {
		v = (Division14_numer)/(Division14_denom);
	}

	push_AR0_incr(v);
}

static inline void GetAvg15_work0() {
	int i;
	float Sum = 0;
	float val;
	for(i = 0; (i)<(256); (i)++) {
		val = pop_AR0_incr();
		Sum += val;
		push_AR0_incr(val);
	}

	push_AR0_incr((Sum)/(PATTERN_SIZE));
}

float DataMinusAvg15_avg;
int DataMinusAvg15_i;
static inline void DataMinusAvg15_work0() {
	DataMinusAvg15_avg = peek_AR0(PATTERN_SIZE);
	for(DataMinusAvg15_i = 0; (DataMinusAvg15_i)<(256); (DataMinusAvg15_i)++) {
		float v = pop_AR0_incr();
		push_AR0_incr((v)-(DataMinusAvg15_avg));
	}

	pop_AR0_incr();
}

float DenomDataSum15_data;
int DenomDataSum15_i;
static inline void DenomDataSum15_work0() {
	float sum = 0;
	for(DenomDataSum15_i = 0; (DenomDataSum15_i)<(256); (DenomDataSum15_i)++) {
		DenomDataSum15_data = pop_AR0_incr();
		sum += (DenomDataSum15_data)*(DenomDataSum15_data);
	}

	push_AR0_incr(sum);
}

float PatternSums15_numerSum;
float PatternSums15_denomPatternSum;
float PatternSums15_data;
float PatternSums15_patternData;
int PatternSums15_i;
static inline void PatternSums15_work0() {
	PatternSums15_numerSum = 0;
	PatternSums15_denomPatternSum = 0;
	for(PatternSums15_i = 0; (PatternSums15_i)<(256); (PatternSums15_i)++) {
		PatternSums15_patternData = (pattern[PatternSums15_i])-(PATTERN_AVG);
		PatternSums15_data = pop_AR0_incr();
		PatternSums15_numerSum += (PatternSums15_data)*(PatternSums15_patternData);
		PatternSums15_denomPatternSum += (PatternSums15_patternData)*(PatternSums15_patternData);
	}

	push_AR0_incr(PatternSums15_denomPatternSum);
	push_AR0_incr(PatternSums15_numerSum);
}

float Division15_numer;
float Division15_denom_x;
float Division15_denom_y;
float Division15_denom;
static inline void Division15_work0() {
	float v = 0;
	Division15_denom_x = pop_AR0_incr();
	Division15_denom_y = pop_AR1_incr();
	Division15_denom = sqrtf((Division15_denom_x)*(Division15_denom_y));
	Division15_numer = pop_AR1_incr();
	if((Division15_denom)!=(0)) {
		v = (Division15_numer)/(Division15_denom);
	}

	push_AR0_incr(v);
}

float SecondCounter0_i = 0;
float SecondCounter0_data;
static inline void SecondCounter0_work0() {
	SecondCounter0_data = pop_AR0_incr();
	(SecondCounter0_i)++;
}

void initialization(void* data) {
    samples = (short*)data;
    // ========== Filter Initialization ==========
    TheGlobal0_init();
    // ========== Initialization Period ==========
    resetAR_src0_init_g0();
    for(int i = 0; i < 271; ++i) {
        src0_work0();
        resetAR_src0_init_g0i();
    }

    Framer0_save_init();
    resetAR_GetAvg0_init_g0();
    GetAvg0_work0();
    resetAR_DataMinusAvg0_init_g0();
    DataMinusAvg0_work0();
    resetAR_DenomDataSum0_init_g0();
    DenomDataSum0_work0();
    resetAR_PatternSums0_init_g0();
    PatternSums0_work0();
    resetAR_Division0_init_g0();
    Division0_work0();
    resetAR_GetAvg1_init_g0();
    GetAvg1_work0();
    resetAR_DataMinusAvg1_init_g0();
    DataMinusAvg1_work0();
    resetAR_DenomDataSum1_init_g0();
    DenomDataSum1_work0();
    resetAR_PatternSums1_init_g0();
    PatternSums1_work0();
    resetAR_Division1_init_g0();
    Division1_work0();
    resetAR_GetAvg2_init_g0();
    GetAvg2_work0();
    resetAR_DataMinusAvg2_init_g0();
    DataMinusAvg2_work0();
    resetAR_DenomDataSum2_init_g0();
    DenomDataSum2_work0();
    resetAR_PatternSums2_init_g0();
    PatternSums2_work0();
    resetAR_Division2_init_g0();
    Division2_work0();
    resetAR_GetAvg3_init_g0();
    GetAvg3_work0();
    resetAR_DataMinusAvg3_init_g0();
    DataMinusAvg3_work0();
    resetAR_DenomDataSum3_init_g0();
    DenomDataSum3_work0();
    resetAR_PatternSums3_init_g0();
    PatternSums3_work0();
    resetAR_Division3_init_g0();
    Division3_work0();
    resetAR_GetAvg4_init_g0();
    GetAvg4_work0();
    resetAR_DataMinusAvg4_init_g0();
    DataMinusAvg4_work0();
    resetAR_DenomDataSum4_init_g0();
    DenomDataSum4_work0();
    resetAR_PatternSums4_init_g0();
    PatternSums4_work0();
    resetAR_Division4_init_g0();
    Division4_work0();
    resetAR_GetAvg5_init_g0();
    GetAvg5_work0();
    resetAR_DataMinusAvg5_init_g0();
    DataMinusAvg5_work0();
    resetAR_DenomDataSum5_init_g0();
    DenomDataSum5_work0();
    resetAR_PatternSums5_init_g0();
    PatternSums5_work0();
    resetAR_Division5_init_g0();
    Division5_work0();
    resetAR_GetAvg6_init_g0();
    GetAvg6_work0();
    resetAR_DataMinusAvg6_init_g0();
    DataMinusAvg6_work0();
    resetAR_DenomDataSum6_init_g0();
    DenomDataSum6_work0();
    resetAR_PatternSums6_init_g0();
    PatternSums6_work0();
    resetAR_Division6_init_g0();
    Division6_work0();
    resetAR_GetAvg7_init_g0();
    GetAvg7_work0();
    resetAR_DataMinusAvg7_init_g0();
    DataMinusAvg7_work0();
    resetAR_DenomDataSum7_init_g0();
    DenomDataSum7_work0();
    resetAR_PatternSums7_init_g0();
    PatternSums7_work0();
    resetAR_Division7_init_g0();
    Division7_work0();
    resetAR_GetAvg8_init_g0();
    GetAvg8_work0();
    resetAR_DataMinusAvg8_init_g0();
    DataMinusAvg8_work0();
    resetAR_DenomDataSum8_init_g0();
    DenomDataSum8_work0();
    resetAR_PatternSums8_init_g0();
    PatternSums8_work0();
    resetAR_Division8_init_g0();
    Division8_work0();
    resetAR_GetAvg9_init_g0();
    GetAvg9_work0();
    resetAR_DataMinusAvg9_init_g0();
    DataMinusAvg9_work0();
    resetAR_DenomDataSum9_init_g0();
    DenomDataSum9_work0();
    resetAR_PatternSums9_init_g0();
    PatternSums9_work0();
    resetAR_Division9_init_g0();
    Division9_work0();
    resetAR_GetAvg10_init_g0();
    GetAvg10_work0();
    resetAR_DataMinusAvg10_init_g0();
    DataMinusAvg10_work0();
    resetAR_DenomDataSum10_init_g0();
    DenomDataSum10_work0();
    resetAR_PatternSums10_init_g0();
    PatternSums10_work0();
    resetAR_Division10_init_g0();
    Division10_work0();
    resetAR_GetAvg11_init_g0();
    GetAvg11_work0();
    resetAR_DataMinusAvg11_init_g0();
    DataMinusAvg11_work0();
    resetAR_DenomDataSum11_init_g0();
    DenomDataSum11_work0();
    resetAR_PatternSums11_init_g0();
    PatternSums11_work0();
    resetAR_Division11_init_g0();
    Division11_work0();
    resetAR_GetAvg12_init_g0();
    GetAvg12_work0();
    resetAR_DataMinusAvg12_init_g0();
    DataMinusAvg12_work0();
    resetAR_DenomDataSum12_init_g0();
    DenomDataSum12_work0();
    resetAR_PatternSums12_init_g0();
    PatternSums12_work0();
    resetAR_Division12_init_g0();
    Division12_work0();
    resetAR_GetAvg13_init_g0();
    GetAvg13_work0();
    resetAR_DataMinusAvg13_init_g0();
    DataMinusAvg13_work0();
    resetAR_DenomDataSum13_init_g0();
    DenomDataSum13_work0();
    resetAR_PatternSums13_init_g0();
    PatternSums13_work0();
    resetAR_Division13_init_g0();
    Division13_work0();
    resetAR_GetAvg14_init_g0();
    GetAvg14_work0();
    resetAR_DataMinusAvg14_init_g0();
    DataMinusAvg14_work0();
    resetAR_DenomDataSum14_init_g0();
    DenomDataSum14_work0();
    resetAR_PatternSums14_init_g0();
    PatternSums14_work0();
    resetAR_Division14_init_g0();
    Division14_work0();
    resetAR_GetAvg15_init_g0();
    GetAvg15_work0();
    resetAR_DataMinusAvg15_init_g0();
    DataMinusAvg15_work0();
    resetAR_DenomDataSum15_init_g0();
    DenomDataSum15_work0();
    resetAR_PatternSums15_init_g0();
    PatternSums15_work0();
    resetAR_Division15_init_g0();
    Division15_work0();
    resetAR_SecondCounter0_init_g0();
    for(int i = 0; i < 16; ++i) {
        SecondCounter0_work0();
        resetAR_SecondCounter0_init_g0i();
    }
}

static inline void iteration() {
    resetAR_src0_steady_g0();
    for(int i = 0; i < 16; ++i) {
        src0_work0();
        resetAR_src0_steady_g0i();
    }

    Framer0_load_steady();
    Framer0_save_steady();
    resetAR_GetAvg0_steady_g0();
    GetAvg0_work0();
    resetAR_DataMinusAvg0_steady_g0();
    DataMinusAvg0_work0();
    resetAR_DenomDataSum0_steady_g0();
    DenomDataSum0_work0();
    resetAR_PatternSums0_steady_g0();
    PatternSums0_work0();
    resetAR_Division0_steady_g0();
    Division0_work0();
    resetAR_GetAvg1_steady_g0();
    GetAvg1_work0();
    resetAR_DataMinusAvg1_steady_g0();
    DataMinusAvg1_work0();
    resetAR_DenomDataSum1_steady_g0();
    DenomDataSum1_work0();
    resetAR_PatternSums1_steady_g0();
    PatternSums1_work0();
    resetAR_Division1_steady_g0();
    Division1_work0();
    resetAR_GetAvg2_steady_g0();
    GetAvg2_work0();
    resetAR_DataMinusAvg2_steady_g0();
    DataMinusAvg2_work0();
    resetAR_DenomDataSum2_steady_g0();
    DenomDataSum2_work0();
    resetAR_PatternSums2_steady_g0();
    PatternSums2_work0();
    resetAR_Division2_steady_g0();
    Division2_work0();
    resetAR_GetAvg3_steady_g0();
    GetAvg3_work0();
    resetAR_DataMinusAvg3_steady_g0();
    DataMinusAvg3_work0();
    resetAR_DenomDataSum3_steady_g0();
    DenomDataSum3_work0();
    resetAR_PatternSums3_steady_g0();
    PatternSums3_work0();
    resetAR_Division3_steady_g0();
    Division3_work0();
    resetAR_GetAvg4_steady_g0();
    GetAvg4_work0();
    resetAR_DataMinusAvg4_steady_g0();
    DataMinusAvg4_work0();
    resetAR_DenomDataSum4_steady_g0();
    DenomDataSum4_work0();
    resetAR_PatternSums4_steady_g0();
    PatternSums4_work0();
    resetAR_Division4_steady_g0();
    Division4_work0();
    resetAR_GetAvg5_steady_g0();
    GetAvg5_work0();
    resetAR_DataMinusAvg5_steady_g0();
    DataMinusAvg5_work0();
    resetAR_DenomDataSum5_steady_g0();
    DenomDataSum5_work0();
    resetAR_PatternSums5_steady_g0();
    PatternSums5_work0();
    resetAR_Division5_steady_g0();
    Division5_work0();
    resetAR_GetAvg6_steady_g0();
    GetAvg6_work0();
    resetAR_DataMinusAvg6_steady_g0();
    DataMinusAvg6_work0();
    resetAR_DenomDataSum6_steady_g0();
    DenomDataSum6_work0();
    resetAR_PatternSums6_steady_g0();
    PatternSums6_work0();
    resetAR_Division6_steady_g0();
    Division6_work0();
    resetAR_GetAvg7_steady_g0();
    GetAvg7_work0();
    resetAR_DataMinusAvg7_steady_g0();
    DataMinusAvg7_work0();
    resetAR_DenomDataSum7_steady_g0();
    DenomDataSum7_work0();
    resetAR_PatternSums7_steady_g0();
    PatternSums7_work0();
    resetAR_Division7_steady_g0();
    Division7_work0();
    resetAR_GetAvg8_steady_g0();
    GetAvg8_work0();
    resetAR_DataMinusAvg8_steady_g0();
    DataMinusAvg8_work0();
    resetAR_DenomDataSum8_steady_g0();
    DenomDataSum8_work0();
    resetAR_PatternSums8_steady_g0();
    PatternSums8_work0();
    resetAR_Division8_steady_g0();
    Division8_work0();
    resetAR_GetAvg9_steady_g0();
    GetAvg9_work0();
    resetAR_DataMinusAvg9_steady_g0();
    DataMinusAvg9_work0();
    resetAR_DenomDataSum9_steady_g0();
    DenomDataSum9_work0();
    resetAR_PatternSums9_steady_g0();
    PatternSums9_work0();
    resetAR_Division9_steady_g0();
    Division9_work0();
    resetAR_GetAvg10_steady_g0();
    GetAvg10_work0();
    resetAR_DataMinusAvg10_steady_g0();
    DataMinusAvg10_work0();
    resetAR_DenomDataSum10_steady_g0();
    DenomDataSum10_work0();
    resetAR_PatternSums10_steady_g0();
    PatternSums10_work0();
    resetAR_Division10_steady_g0();
    Division10_work0();
    resetAR_GetAvg11_steady_g0();
    GetAvg11_work0();
    resetAR_DataMinusAvg11_steady_g0();
    DataMinusAvg11_work0();
    resetAR_DenomDataSum11_steady_g0();
    DenomDataSum11_work0();
    resetAR_PatternSums11_steady_g0();
    PatternSums11_work0();
    resetAR_Division11_steady_g0();
    Division11_work0();
    resetAR_GetAvg12_steady_g0();
    GetAvg12_work0();
    resetAR_DataMinusAvg12_steady_g0();
    DataMinusAvg12_work0();
    resetAR_DenomDataSum12_steady_g0();
    DenomDataSum12_work0();
    resetAR_PatternSums12_steady_g0();
    PatternSums12_work0();
    resetAR_Division12_steady_g0();
    Division12_work0();
    resetAR_GetAvg13_steady_g0();
    GetAvg13_work0();
    resetAR_DataMinusAvg13_steady_g0();
    DataMinusAvg13_work0();
    resetAR_DenomDataSum13_steady_g0();
    DenomDataSum13_work0();
    resetAR_PatternSums13_steady_g0();
    PatternSums13_work0();
    resetAR_Division13_steady_g0();
    Division13_work0();
    resetAR_GetAvg14_steady_g0();
    GetAvg14_work0();
    resetAR_DataMinusAvg14_steady_g0();
    DataMinusAvg14_work0();
    resetAR_DenomDataSum14_steady_g0();
    DenomDataSum14_work0();
    resetAR_PatternSums14_steady_g0();
    PatternSums14_work0();
    resetAR_Division14_steady_g0();
    Division14_work0();
    resetAR_GetAvg15_steady_g0();
    GetAvg15_work0();
    resetAR_DataMinusAvg15_steady_g0();
    DataMinusAvg15_work0();
    resetAR_DenomDataSum15_steady_g0();
    DenomDataSum15_work0();
    resetAR_PatternSums15_steady_g0();
    PatternSums15_work0();
    resetAR_Division15_steady_g0();
    Division15_work0();
    resetAR_SecondCounter0_steady_g0();
    for(int i = 0; i < 16; ++i) {
        SecondCounter0_work0();
        resetAR_SecondCounter0_steady_g0i();
    }
}

void iterations(int periods, void* data) {
    samples = (short*)data;
    for(int p = 0; p < periods; p++)
        iteration();
}

int main(int argc, const char* argv[]) {
	// ========== Steady State w/o Scaling ==========
	// ========== Filter Finalization ==========
}

