#ifndef __FUSION_H
#define __FUSION_H

#define max(A,B) (((A)>(B))?(A):(B))
#define pow2ceil(A) ((A<=2)?(2): (((A<=4)?(4): (((A<=8)?(8): (((A<=16)?(16): (((A<=32)?(32): (((A<=64)?(64): (((A<=128)?(128): (((A<=256)?(256):(((A<=1024)?(1024):(((A<=4096)?(4096):(((A<=16384)?(16384):(((A<=65536)?(65536):(((A<=131072)?(131072):(((A<=262144)?(262144):(((A<=524288)?(524288):(((A<=1048576)?(1048576):(((A<=2097152)?(2097152):(((A<=4194304)?(4194304):(((A<=8388608)?(8388608):(((A<=16777216)?(16777216):(((A<=33554432)?(33554432):(((A<=67108864)?(67108864):(((A<=134217728)?(134217728):(((A<=268435456)?(268435456):(((A<=536870912)?(536870912):(1073741824))))))))))))))))))))))))))))))))))))))))))))))))))
//destination peeks: 128 extra items
#define __PEEK_BUF_SIZE_0_1 128
#define __BUF_SIZE_MASK_0_1 (pow2ceil(256+128)-1)

#define __BUF_SIZE_MASK_1_2 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_2_3 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_3_4 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_4_5 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_5_6 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_5_545 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_6_7 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_7_8 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_8_9 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_9_10 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_9_542 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_10_11 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_11_12 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_12_13 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_13_14 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_14_15 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_15_16 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_15_528 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_16_17 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_16_519 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_16_522 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_16_525 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_17_18 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_18_19 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_19_20 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_20_21 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_21_22 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_22_23 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_22_493 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_23_24 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_23_472 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_23_475 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_23_478 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_23_481 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_23_484 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_23_487 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_23_490 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_24_25 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_25_26 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_26_27 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_27_28 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_28_29 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_29_30 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_29_422 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_30_31 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_377 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_380 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_383 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_386 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_389 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_392 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_395 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_398 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_401 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_404 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_407 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_410 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_413 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_416 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_30_419 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_31_32 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_32_33 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_33_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_34_35 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_35_36 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_36_37 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_36_279 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_37_38 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_186 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_189 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_192 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_195 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_198 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_201 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_204 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_207 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_210 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_213 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_216 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_219 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_222 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_225 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_228 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_231 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_234 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_237 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_240 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_243 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_246 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_249 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_252 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_255 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_258 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_261 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_264 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_267 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_270 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_273 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_37_276 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_38_39 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_39_40 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_40_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_41_42 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_42_43 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_43_44 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_43_120 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_44_45 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_57 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_58 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_59 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_60 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_61 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_62 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_63 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_64 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_65 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_66 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_67 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_68 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_69 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_70 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_71 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_72 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_73 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_74 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_75 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_76 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_77 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_78 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_79 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_80 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_81 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_82 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_83 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_84 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_85 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_86 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_87 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_88 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_89 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_90 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_91 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_92 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_93 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_94 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_95 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_96 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_97 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_98 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_99 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_100 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_101 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_102 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_103 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_104 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_105 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_106 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_107 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_108 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_109 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_110 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_111 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_112 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_113 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_114 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_115 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_116 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_117 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_118 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_44_119 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_45_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_46_47 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_47_48 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_48_49 (pow2ceil(512+0)-1)

#define __BUF_SIZE_MASK_49_50 (pow2ceil(129+0)-1)

#define __BUF_SIZE_MASK_50_51 (pow2ceil(129+0)-1)

#define __BUF_SIZE_MASK_50_55 (pow2ceil(129+0)-1)

#define __BUF_SIZE_MASK_51_52 (pow2ceil(1+0)-1)

#define __BUF_SIZE_MASK_52_53 (pow2ceil(17+0)-1)

#define __BUF_SIZE_MASK_53_54 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_55_56 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_56_52 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_57_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_58_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_59_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_60_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_61_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_62_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_63_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_64_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_65_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_66_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_67_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_68_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_69_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_70_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_71_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_72_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_73_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_74_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_75_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_76_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_77_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_78_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_79_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_80_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_81_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_82_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_83_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_84_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_85_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_86_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_87_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_88_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_89_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_90_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_91_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_92_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_93_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_94_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_95_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_96_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_97_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_98_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_99_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_100_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_101_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_102_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_103_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_104_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_105_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_106_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_107_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_108_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_109_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_110_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_111_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_112_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_113_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_114_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_115_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_116_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_117_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_118_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_119_46 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_121 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_123 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_124 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_125 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_126 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_127 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_128 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_129 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_130 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_131 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_132 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_133 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_134 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_135 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_136 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_137 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_138 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_139 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_140 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_141 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_142 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_143 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_144 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_145 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_146 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_147 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_148 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_149 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_150 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_151 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_152 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_153 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_154 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_155 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_156 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_157 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_158 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_159 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_160 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_161 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_162 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_163 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_164 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_165 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_166 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_167 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_168 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_169 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_170 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_171 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_172 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_173 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_174 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_175 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_176 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_177 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_178 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_179 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_180 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_181 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_182 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_183 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_184 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_120_185 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_121_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_122_47 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_123_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_124_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_125_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_126_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_127_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_128_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_129_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_130_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_131_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_132_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_133_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_134_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_135_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_136_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_137_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_138_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_139_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_140_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_141_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_142_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_143_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_144_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_145_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_146_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_147_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_148_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_149_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_150_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_151_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_152_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_153_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_154_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_155_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_156_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_157_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_158_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_159_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_160_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_161_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_162_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_163_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_164_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_165_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_166_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_167_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_168_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_169_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_170_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_171_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_172_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_173_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_174_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_175_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_176_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_177_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_178_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_179_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_180_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_181_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_182_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_183_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_184_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_185_122 (pow2ceil(4+0)-1)

#define __BUF_SIZE_MASK_186_187 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_187_188 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_188_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_189_190 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_190_191 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_191_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_192_193 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_193_194 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_194_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_195_196 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_196_197 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_197_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_198_199 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_199_200 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_200_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_201_202 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_202_203 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_203_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_204_205 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_205_206 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_206_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_207_208 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_208_209 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_209_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_210_211 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_211_212 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_212_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_213_214 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_214_215 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_215_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_216_217 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_217_218 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_218_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_219_220 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_220_221 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_221_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_222_223 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_223_224 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_224_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_225_226 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_226_227 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_227_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_228_229 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_229_230 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_230_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_231_232 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_232_233 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_233_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_234_235 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_235_236 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_236_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_237_238 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_238_239 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_239_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_240_241 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_241_242 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_242_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_243_244 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_244_245 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_245_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_246_247 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_247_248 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_248_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_249_250 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_250_251 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_251_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_252_253 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_253_254 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_254_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_255_256 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_256_257 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_257_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_258_259 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_259_260 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_260_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_261_262 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_262_263 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_263_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_264_265 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_265_266 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_266_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_267_268 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_268_269 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_269_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_270_271 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_271_272 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_272_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_273_274 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_274_275 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_275_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_276_277 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_277_278 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_278_41 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_280 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_284 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_287 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_290 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_293 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_296 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_299 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_302 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_305 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_308 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_311 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_314 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_317 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_320 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_323 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_326 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_329 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_332 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_335 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_338 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_341 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_344 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_347 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_350 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_353 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_356 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_359 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_362 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_365 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_368 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_371 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_279_374 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_280_281 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_281_282 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_282_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_283_42 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_284_285 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_285_286 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_286_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_287_288 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_288_289 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_289_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_290_291 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_291_292 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_292_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_293_294 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_294_295 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_295_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_296_297 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_297_298 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_298_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_299_300 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_300_301 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_301_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_302_303 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_303_304 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_304_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_305_306 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_306_307 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_307_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_308_309 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_309_310 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_310_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_311_312 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_312_313 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_313_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_314_315 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_315_316 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_316_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_317_318 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_318_319 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_319_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_320_321 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_321_322 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_322_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_323_324 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_324_325 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_325_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_326_327 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_327_328 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_328_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_329_330 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_330_331 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_331_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_332_333 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_333_334 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_334_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_335_336 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_336_337 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_337_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_338_339 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_339_340 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_340_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_341_342 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_342_343 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_343_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_344_345 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_345_346 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_346_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_347_348 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_348_349 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_349_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_350_351 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_351_352 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_352_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_353_354 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_354_355 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_355_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_356_357 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_357_358 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_358_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_359_360 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_360_361 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_361_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_362_363 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_363_364 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_364_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_365_366 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_366_367 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_367_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_368_369 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_369_370 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_370_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_371_372 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_372_373 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_373_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_374_375 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_375_376 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_376_283 (pow2ceil(8+0)-1)

#define __BUF_SIZE_MASK_377_378 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_378_379 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_379_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_380_381 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_381_382 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_382_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_383_384 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_384_385 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_385_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_386_387 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_387_388 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_388_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_389_390 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_390_391 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_391_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_392_393 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_393_394 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_394_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_395_396 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_396_397 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_397_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_398_399 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_399_400 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_400_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_401_402 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_402_403 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_403_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_404_405 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_405_406 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_406_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_407_408 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_408_409 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_409_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_410_411 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_411_412 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_412_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_413_414 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_414_415 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_415_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_416_417 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_417_418 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_418_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_419_420 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_420_421 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_421_34 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_423 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_427 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_430 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_433 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_436 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_439 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_442 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_445 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_448 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_451 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_454 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_457 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_460 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_463 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_466 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_422_469 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_423_424 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_424_425 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_425_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_426_35 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_427_428 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_428_429 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_429_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_430_431 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_431_432 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_432_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_433_434 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_434_435 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_435_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_436_437 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_437_438 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_438_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_439_440 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_440_441 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_441_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_442_443 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_443_444 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_444_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_445_446 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_446_447 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_447_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_448_449 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_449_450 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_450_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_451_452 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_452_453 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_453_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_454_455 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_455_456 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_456_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_457_458 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_458_459 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_459_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_460_461 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_461_462 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_462_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_463_464 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_464_465 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_465_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_466_467 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_467_468 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_468_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_469_470 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_470_471 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_471_426 (pow2ceil(16+0)-1)

#define __BUF_SIZE_MASK_472_473 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_473_474 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_474_27 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_475_476 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_476_477 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_477_27 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_478_479 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_479_480 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_480_27 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_481_482 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_482_483 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_483_27 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_484_485 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_485_486 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_486_27 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_487_488 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_488_489 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_489_27 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_490_491 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_491_492 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_492_27 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_493_494 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_493_498 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_493_501 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_493_504 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_493_507 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_493_510 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_493_513 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_493_516 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_494_495 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_495_496 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_496_497 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_497_28 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_498_499 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_499_500 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_500_497 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_501_502 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_502_503 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_503_497 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_504_505 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_505_506 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_506_497 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_507_508 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_508_509 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_509_497 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_510_511 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_511_512 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_512_497 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_513_514 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_514_515 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_515_497 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_516_517 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_517_518 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_518_497 (pow2ceil(32+0)-1)

#define __BUF_SIZE_MASK_519_520 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_520_521 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_521_20 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_522_523 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_523_524 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_524_20 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_525_526 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_526_527 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_527_20 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_528_529 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_528_533 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_528_536 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_528_539 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_529_530 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_530_531 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_531_532 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_532_21 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_533_534 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_534_535 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_535_532 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_536_537 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_537_538 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_538_532 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_539_540 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_540_541 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_541_532 (pow2ceil(64+0)-1)

#define __BUF_SIZE_MASK_542_543 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_543_544 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_544_13 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_545_546 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_546_547 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_547_548 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_548_549 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_548_553 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_549_550 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_550_551 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_551_552 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_552_14 (pow2ceil(256+0)-1)

#define __BUF_SIZE_MASK_553_554 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_554_555 (pow2ceil(128+0)-1)

#define __BUF_SIZE_MASK_555_552 (pow2ceil(128+0)-1)

#endif
