#include <jni.h>
#include <stdio.h>
#include "log.h"

extern void initialization(void* data);
extern void iterations(int periods, void* data);

extern "C" JNIEXPORT void JNICALL Java_esms_msl_AndroidBenchmarkBuilder_initialization(JNIEnv* env, jobject thiz, jobject buf) {
    void* data = env->GetDirectBufferAddress(buf);
//    LOGD(TAG, "initialization data at 0x%X\n", (unsigned int)data);
    initialization(data);
}

extern "C" JNIEXPORT void JNICALL Java_esms_msl_AndroidBenchmarkBuilder_iterations(JNIEnv* env, jobject thiz, jint periods, jobject buf) {
    void* data = env->GetDirectBufferAddress(buf);
//    LOGD(TAG, "iteration data at 0x%X for %d times\n", (unsigned int)data, periods);
    iterations(periods, data);
}

/*
short* samples;

#include "log.h"

void initialization(void* data) {
    samples = (short*)data;

}

static inline void iteration() {

}

void iterations(int periods, void* data) {
    samples = (short*)data;
    for(int p = 0; p < periods; p++)
        iteration();
}
*/