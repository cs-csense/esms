package esms.msa2;

import android.app.Application;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.RequiresDevice;
import android.support.test.runner.AndroidJUnit4;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import esms.msl.AndroidBenchmarkBuilder;
import esms.profiling.StandaloneProfiler;
import esms.profiling.ResourceUsageProfiler;
import esms.profiling.TrepnProfiler;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
public class ApplicationTest extends ApplicationTestCase<Application> {
    private static final int SAMPLEING_RATE = Integer.parseInt(System.getProperty("SAMPLING_RATE", "44100"));
    private AndroidBenchmarkBuilder AndroidBenchmarkBuilder;

    public ApplicationTest() {
        super(Application.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        String[] pkg = InstrumentationRegistry.getContext().getPackageName().split("\\.");
        String benchmark = pkg[2];
        String tk = pkg[0];
//        AndroidBenchmarkBuilder = new AndroidBenchmarkBuilder(tk, benchmark, new ResourceUsageProfiler());
//        AndroidBenchmarkBuilder = new AndroidBenchmarkBuilder(tk, benchmark, new TrepnProfiler(InstrumentationRegistry.getContext()));
        List<StandaloneProfiler> profilers = new ArrayList<>();
        profilers.add(new ResourceUsageProfiler());
        profilers.add(new TrepnProfiler(InstrumentationRegistry.getContext()));
        AndroidBenchmarkBuilder = new AndroidBenchmarkBuilder(tk, benchmark, profilers);
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    @LargeTest
    @RequiresDevice
    public void testApplication() throws Exception {
        // StreamIt cacheopt may not run againt large buffer scaling 64.
        int[] scalings = new int[] { 1,16,32,64 };
//        int[] scalings = new int[] { 32 };
        for(int i: scalings) {
            System.gc();
            AndroidBenchmarkBuilder.run(60, SAMPLEING_RATE, i);
        }
    }
}