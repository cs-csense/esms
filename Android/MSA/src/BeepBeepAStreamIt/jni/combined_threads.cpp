#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <netsocket.h>
#include <node_server.h>
#include <init_instance.h>
#include <master_server.h>
#include <save_state.h>
#include <save_manager.h>
#include <delete_chkpts.h>
#include <object_write_buffer.h>
#include <read_setup.h>
#include <ccp.h>
#include <streamit_random.h>
#include "structs.h"
#include "fusion.h"

int __max_iteration;
int __timer_enabled = 0;
int __frequency_of_chkpts;
volatile int __vol;


float BUFFER_0_1[__BUF_SIZE_MASK_0_1 + 1];
int HEAD_0_1 = 0;
int TAIL_0_1 = 0;
float BUFFER_1_2[__BUF_SIZE_MASK_1_2 + 1];
int HEAD_1_2 = 0;
int TAIL_1_2 = 0;
float BUFFER_2_3[__BUF_SIZE_MASK_2_3 + 1];
int HEAD_2_3 = 0;
int TAIL_2_3 = 0;
float BUFFER_2_12[__BUF_SIZE_MASK_2_12 + 1];
int HEAD_2_12 = 0;
int TAIL_2_12 = 0;
float BUFFER_2_19[__BUF_SIZE_MASK_2_19 + 1];
int HEAD_2_19 = 0;
int TAIL_2_19 = 0;
float BUFFER_2_26[__BUF_SIZE_MASK_2_26 + 1];
int HEAD_2_26 = 0;
int TAIL_2_26 = 0;
float BUFFER_2_33[__BUF_SIZE_MASK_2_33 + 1];
int HEAD_2_33 = 0;
int TAIL_2_33 = 0;
float BUFFER_2_40[__BUF_SIZE_MASK_2_40 + 1];
int HEAD_2_40 = 0;
int TAIL_2_40 = 0;
float BUFFER_2_47[__BUF_SIZE_MASK_2_47 + 1];
int HEAD_2_47 = 0;
int TAIL_2_47 = 0;
float BUFFER_2_54[__BUF_SIZE_MASK_2_54 + 1];
int HEAD_2_54 = 0;
int TAIL_2_54 = 0;
float BUFFER_2_61[__BUF_SIZE_MASK_2_61 + 1];
int HEAD_2_61 = 0;
int TAIL_2_61 = 0;
float BUFFER_2_68[__BUF_SIZE_MASK_2_68 + 1];
int HEAD_2_68 = 0;
int TAIL_2_68 = 0;
float BUFFER_2_75[__BUF_SIZE_MASK_2_75 + 1];
int HEAD_2_75 = 0;
int TAIL_2_75 = 0;
float BUFFER_2_82[__BUF_SIZE_MASK_2_82 + 1];
int HEAD_2_82 = 0;
int TAIL_2_82 = 0;
float BUFFER_2_89[__BUF_SIZE_MASK_2_89 + 1];
int HEAD_2_89 = 0;
int TAIL_2_89 = 0;
float BUFFER_2_96[__BUF_SIZE_MASK_2_96 + 1];
int HEAD_2_96 = 0;
int TAIL_2_96 = 0;
float BUFFER_2_103[__BUF_SIZE_MASK_2_103 + 1];
int HEAD_2_103 = 0;
int TAIL_2_103 = 0;
float BUFFER_2_110[__BUF_SIZE_MASK_2_110 + 1];
int HEAD_2_110 = 0;
int TAIL_2_110 = 0;
float BUFFER_3_4[__BUF_SIZE_MASK_3_4 + 1];
int HEAD_3_4 = 0;
int TAIL_3_4 = 0;
float BUFFER_4_5[__BUF_SIZE_MASK_4_5 + 1];
int HEAD_4_5 = 0;
int TAIL_4_5 = 0;
float BUFFER_5_6[__BUF_SIZE_MASK_5_6 + 1];
int HEAD_5_6 = 0;
int TAIL_5_6 = 0;
float BUFFER_5_11[__BUF_SIZE_MASK_5_11 + 1];
int HEAD_5_11 = 0;
int TAIL_5_11 = 0;
float BUFFER_6_7[__BUF_SIZE_MASK_6_7 + 1];
int HEAD_6_7 = 0;
int TAIL_6_7 = 0;
float BUFFER_7_8[__BUF_SIZE_MASK_7_8 + 1];
int HEAD_7_8 = 0;
int TAIL_7_8 = 0;
float BUFFER_8_9[__BUF_SIZE_MASK_8_9 + 1];
int HEAD_8_9 = 0;
int TAIL_8_9 = 0;
float BUFFER_9_10[__BUF_SIZE_MASK_9_10 + 1];
int HEAD_9_10 = 0;
int TAIL_9_10 = 0;
float BUFFER_11_7[__BUF_SIZE_MASK_11_7 + 1];
int HEAD_11_7 = 0;
int TAIL_11_7 = 0;
float BUFFER_12_13[__BUF_SIZE_MASK_12_13 + 1];
int HEAD_12_13 = 0;
int TAIL_12_13 = 0;
float BUFFER_13_14[__BUF_SIZE_MASK_13_14 + 1];
int HEAD_13_14 = 0;
int TAIL_13_14 = 0;
float BUFFER_14_15[__BUF_SIZE_MASK_14_15 + 1];
int HEAD_14_15 = 0;
int TAIL_14_15 = 0;
float BUFFER_14_18[__BUF_SIZE_MASK_14_18 + 1];
int HEAD_14_18 = 0;
int TAIL_14_18 = 0;
float BUFFER_15_16[__BUF_SIZE_MASK_15_16 + 1];
int HEAD_15_16 = 0;
int TAIL_15_16 = 0;
float BUFFER_16_17[__BUF_SIZE_MASK_16_17 + 1];
int HEAD_16_17 = 0;
int TAIL_16_17 = 0;
float BUFFER_17_9[__BUF_SIZE_MASK_17_9 + 1];
int HEAD_17_9 = 0;
int TAIL_17_9 = 0;
float BUFFER_18_16[__BUF_SIZE_MASK_18_16 + 1];
int HEAD_18_16 = 0;
int TAIL_18_16 = 0;
float BUFFER_19_20[__BUF_SIZE_MASK_19_20 + 1];
int HEAD_19_20 = 0;
int TAIL_19_20 = 0;
float BUFFER_20_21[__BUF_SIZE_MASK_20_21 + 1];
int HEAD_20_21 = 0;
int TAIL_20_21 = 0;
float BUFFER_21_22[__BUF_SIZE_MASK_21_22 + 1];
int HEAD_21_22 = 0;
int TAIL_21_22 = 0;
float BUFFER_21_25[__BUF_SIZE_MASK_21_25 + 1];
int HEAD_21_25 = 0;
int TAIL_21_25 = 0;
float BUFFER_22_23[__BUF_SIZE_MASK_22_23 + 1];
int HEAD_22_23 = 0;
int TAIL_22_23 = 0;
float BUFFER_23_24[__BUF_SIZE_MASK_23_24 + 1];
int HEAD_23_24 = 0;
int TAIL_23_24 = 0;
float BUFFER_24_9[__BUF_SIZE_MASK_24_9 + 1];
int HEAD_24_9 = 0;
int TAIL_24_9 = 0;
float BUFFER_25_23[__BUF_SIZE_MASK_25_23 + 1];
int HEAD_25_23 = 0;
int TAIL_25_23 = 0;
float BUFFER_26_27[__BUF_SIZE_MASK_26_27 + 1];
int HEAD_26_27 = 0;
int TAIL_26_27 = 0;
float BUFFER_27_28[__BUF_SIZE_MASK_27_28 + 1];
int HEAD_27_28 = 0;
int TAIL_27_28 = 0;
float BUFFER_28_29[__BUF_SIZE_MASK_28_29 + 1];
int HEAD_28_29 = 0;
int TAIL_28_29 = 0;
float BUFFER_28_32[__BUF_SIZE_MASK_28_32 + 1];
int HEAD_28_32 = 0;
int TAIL_28_32 = 0;
float BUFFER_29_30[__BUF_SIZE_MASK_29_30 + 1];
int HEAD_29_30 = 0;
int TAIL_29_30 = 0;
float BUFFER_30_31[__BUF_SIZE_MASK_30_31 + 1];
int HEAD_30_31 = 0;
int TAIL_30_31 = 0;
float BUFFER_31_9[__BUF_SIZE_MASK_31_9 + 1];
int HEAD_31_9 = 0;
int TAIL_31_9 = 0;
float BUFFER_32_30[__BUF_SIZE_MASK_32_30 + 1];
int HEAD_32_30 = 0;
int TAIL_32_30 = 0;
float BUFFER_33_34[__BUF_SIZE_MASK_33_34 + 1];
int HEAD_33_34 = 0;
int TAIL_33_34 = 0;
float BUFFER_34_35[__BUF_SIZE_MASK_34_35 + 1];
int HEAD_34_35 = 0;
int TAIL_34_35 = 0;
float BUFFER_35_36[__BUF_SIZE_MASK_35_36 + 1];
int HEAD_35_36 = 0;
int TAIL_35_36 = 0;
float BUFFER_35_39[__BUF_SIZE_MASK_35_39 + 1];
int HEAD_35_39 = 0;
int TAIL_35_39 = 0;
float BUFFER_36_37[__BUF_SIZE_MASK_36_37 + 1];
int HEAD_36_37 = 0;
int TAIL_36_37 = 0;
float BUFFER_37_38[__BUF_SIZE_MASK_37_38 + 1];
int HEAD_37_38 = 0;
int TAIL_37_38 = 0;
float BUFFER_38_9[__BUF_SIZE_MASK_38_9 + 1];
int HEAD_38_9 = 0;
int TAIL_38_9 = 0;
float BUFFER_39_37[__BUF_SIZE_MASK_39_37 + 1];
int HEAD_39_37 = 0;
int TAIL_39_37 = 0;
float BUFFER_40_41[__BUF_SIZE_MASK_40_41 + 1];
int HEAD_40_41 = 0;
int TAIL_40_41 = 0;
float BUFFER_41_42[__BUF_SIZE_MASK_41_42 + 1];
int HEAD_41_42 = 0;
int TAIL_41_42 = 0;
float BUFFER_42_43[__BUF_SIZE_MASK_42_43 + 1];
int HEAD_42_43 = 0;
int TAIL_42_43 = 0;
float BUFFER_42_46[__BUF_SIZE_MASK_42_46 + 1];
int HEAD_42_46 = 0;
int TAIL_42_46 = 0;
float BUFFER_43_44[__BUF_SIZE_MASK_43_44 + 1];
int HEAD_43_44 = 0;
int TAIL_43_44 = 0;
float BUFFER_44_45[__BUF_SIZE_MASK_44_45 + 1];
int HEAD_44_45 = 0;
int TAIL_44_45 = 0;
float BUFFER_45_9[__BUF_SIZE_MASK_45_9 + 1];
int HEAD_45_9 = 0;
int TAIL_45_9 = 0;
float BUFFER_46_44[__BUF_SIZE_MASK_46_44 + 1];
int HEAD_46_44 = 0;
int TAIL_46_44 = 0;
float BUFFER_47_48[__BUF_SIZE_MASK_47_48 + 1];
int HEAD_47_48 = 0;
int TAIL_47_48 = 0;
float BUFFER_48_49[__BUF_SIZE_MASK_48_49 + 1];
int HEAD_48_49 = 0;
int TAIL_48_49 = 0;
float BUFFER_49_50[__BUF_SIZE_MASK_49_50 + 1];
int HEAD_49_50 = 0;
int TAIL_49_50 = 0;
float BUFFER_49_53[__BUF_SIZE_MASK_49_53 + 1];
int HEAD_49_53 = 0;
int TAIL_49_53 = 0;
float BUFFER_50_51[__BUF_SIZE_MASK_50_51 + 1];
int HEAD_50_51 = 0;
int TAIL_50_51 = 0;
float BUFFER_51_52[__BUF_SIZE_MASK_51_52 + 1];
int HEAD_51_52 = 0;
int TAIL_51_52 = 0;
float BUFFER_52_9[__BUF_SIZE_MASK_52_9 + 1];
int HEAD_52_9 = 0;
int TAIL_52_9 = 0;
float BUFFER_53_51[__BUF_SIZE_MASK_53_51 + 1];
int HEAD_53_51 = 0;
int TAIL_53_51 = 0;
float BUFFER_54_55[__BUF_SIZE_MASK_54_55 + 1];
int HEAD_54_55 = 0;
int TAIL_54_55 = 0;
float BUFFER_55_56[__BUF_SIZE_MASK_55_56 + 1];
int HEAD_55_56 = 0;
int TAIL_55_56 = 0;
float BUFFER_56_57[__BUF_SIZE_MASK_56_57 + 1];
int HEAD_56_57 = 0;
int TAIL_56_57 = 0;
float BUFFER_56_60[__BUF_SIZE_MASK_56_60 + 1];
int HEAD_56_60 = 0;
int TAIL_56_60 = 0;
float BUFFER_57_58[__BUF_SIZE_MASK_57_58 + 1];
int HEAD_57_58 = 0;
int TAIL_57_58 = 0;
float BUFFER_58_59[__BUF_SIZE_MASK_58_59 + 1];
int HEAD_58_59 = 0;
int TAIL_58_59 = 0;
float BUFFER_59_9[__BUF_SIZE_MASK_59_9 + 1];
int HEAD_59_9 = 0;
int TAIL_59_9 = 0;
float BUFFER_60_58[__BUF_SIZE_MASK_60_58 + 1];
int HEAD_60_58 = 0;
int TAIL_60_58 = 0;
float BUFFER_61_62[__BUF_SIZE_MASK_61_62 + 1];
int HEAD_61_62 = 0;
int TAIL_61_62 = 0;
float BUFFER_62_63[__BUF_SIZE_MASK_62_63 + 1];
int HEAD_62_63 = 0;
int TAIL_62_63 = 0;
float BUFFER_63_64[__BUF_SIZE_MASK_63_64 + 1];
int HEAD_63_64 = 0;
int TAIL_63_64 = 0;
float BUFFER_63_67[__BUF_SIZE_MASK_63_67 + 1];
int HEAD_63_67 = 0;
int TAIL_63_67 = 0;
float BUFFER_64_65[__BUF_SIZE_MASK_64_65 + 1];
int HEAD_64_65 = 0;
int TAIL_64_65 = 0;
float BUFFER_65_66[__BUF_SIZE_MASK_65_66 + 1];
int HEAD_65_66 = 0;
int TAIL_65_66 = 0;
float BUFFER_66_9[__BUF_SIZE_MASK_66_9 + 1];
int HEAD_66_9 = 0;
int TAIL_66_9 = 0;
float BUFFER_67_65[__BUF_SIZE_MASK_67_65 + 1];
int HEAD_67_65 = 0;
int TAIL_67_65 = 0;
float BUFFER_68_69[__BUF_SIZE_MASK_68_69 + 1];
int HEAD_68_69 = 0;
int TAIL_68_69 = 0;
float BUFFER_69_70[__BUF_SIZE_MASK_69_70 + 1];
int HEAD_69_70 = 0;
int TAIL_69_70 = 0;
float BUFFER_70_71[__BUF_SIZE_MASK_70_71 + 1];
int HEAD_70_71 = 0;
int TAIL_70_71 = 0;
float BUFFER_70_74[__BUF_SIZE_MASK_70_74 + 1];
int HEAD_70_74 = 0;
int TAIL_70_74 = 0;
float BUFFER_71_72[__BUF_SIZE_MASK_71_72 + 1];
int HEAD_71_72 = 0;
int TAIL_71_72 = 0;
float BUFFER_72_73[__BUF_SIZE_MASK_72_73 + 1];
int HEAD_72_73 = 0;
int TAIL_72_73 = 0;
float BUFFER_73_9[__BUF_SIZE_MASK_73_9 + 1];
int HEAD_73_9 = 0;
int TAIL_73_9 = 0;
float BUFFER_74_72[__BUF_SIZE_MASK_74_72 + 1];
int HEAD_74_72 = 0;
int TAIL_74_72 = 0;
float BUFFER_75_76[__BUF_SIZE_MASK_75_76 + 1];
int HEAD_75_76 = 0;
int TAIL_75_76 = 0;
float BUFFER_76_77[__BUF_SIZE_MASK_76_77 + 1];
int HEAD_76_77 = 0;
int TAIL_76_77 = 0;
float BUFFER_77_78[__BUF_SIZE_MASK_77_78 + 1];
int HEAD_77_78 = 0;
int TAIL_77_78 = 0;
float BUFFER_77_81[__BUF_SIZE_MASK_77_81 + 1];
int HEAD_77_81 = 0;
int TAIL_77_81 = 0;
float BUFFER_78_79[__BUF_SIZE_MASK_78_79 + 1];
int HEAD_78_79 = 0;
int TAIL_78_79 = 0;
float BUFFER_79_80[__BUF_SIZE_MASK_79_80 + 1];
int HEAD_79_80 = 0;
int TAIL_79_80 = 0;
float BUFFER_80_9[__BUF_SIZE_MASK_80_9 + 1];
int HEAD_80_9 = 0;
int TAIL_80_9 = 0;
float BUFFER_81_79[__BUF_SIZE_MASK_81_79 + 1];
int HEAD_81_79 = 0;
int TAIL_81_79 = 0;
float BUFFER_82_83[__BUF_SIZE_MASK_82_83 + 1];
int HEAD_82_83 = 0;
int TAIL_82_83 = 0;
float BUFFER_83_84[__BUF_SIZE_MASK_83_84 + 1];
int HEAD_83_84 = 0;
int TAIL_83_84 = 0;
float BUFFER_84_85[__BUF_SIZE_MASK_84_85 + 1];
int HEAD_84_85 = 0;
int TAIL_84_85 = 0;
float BUFFER_84_88[__BUF_SIZE_MASK_84_88 + 1];
int HEAD_84_88 = 0;
int TAIL_84_88 = 0;
float BUFFER_85_86[__BUF_SIZE_MASK_85_86 + 1];
int HEAD_85_86 = 0;
int TAIL_85_86 = 0;
float BUFFER_86_87[__BUF_SIZE_MASK_86_87 + 1];
int HEAD_86_87 = 0;
int TAIL_86_87 = 0;
float BUFFER_87_9[__BUF_SIZE_MASK_87_9 + 1];
int HEAD_87_9 = 0;
int TAIL_87_9 = 0;
float BUFFER_88_86[__BUF_SIZE_MASK_88_86 + 1];
int HEAD_88_86 = 0;
int TAIL_88_86 = 0;
float BUFFER_89_90[__BUF_SIZE_MASK_89_90 + 1];
int HEAD_89_90 = 0;
int TAIL_89_90 = 0;
float BUFFER_90_91[__BUF_SIZE_MASK_90_91 + 1];
int HEAD_90_91 = 0;
int TAIL_90_91 = 0;
float BUFFER_91_92[__BUF_SIZE_MASK_91_92 + 1];
int HEAD_91_92 = 0;
int TAIL_91_92 = 0;
float BUFFER_91_95[__BUF_SIZE_MASK_91_95 + 1];
int HEAD_91_95 = 0;
int TAIL_91_95 = 0;
float BUFFER_92_93[__BUF_SIZE_MASK_92_93 + 1];
int HEAD_92_93 = 0;
int TAIL_92_93 = 0;
float BUFFER_93_94[__BUF_SIZE_MASK_93_94 + 1];
int HEAD_93_94 = 0;
int TAIL_93_94 = 0;
float BUFFER_94_9[__BUF_SIZE_MASK_94_9 + 1];
int HEAD_94_9 = 0;
int TAIL_94_9 = 0;
float BUFFER_95_93[__BUF_SIZE_MASK_95_93 + 1];
int HEAD_95_93 = 0;
int TAIL_95_93 = 0;
float BUFFER_96_97[__BUF_SIZE_MASK_96_97 + 1];
int HEAD_96_97 = 0;
int TAIL_96_97 = 0;
float BUFFER_97_98[__BUF_SIZE_MASK_97_98 + 1];
int HEAD_97_98 = 0;
int TAIL_97_98 = 0;
float BUFFER_98_99[__BUF_SIZE_MASK_98_99 + 1];
int HEAD_98_99 = 0;
int TAIL_98_99 = 0;
float BUFFER_98_102[__BUF_SIZE_MASK_98_102 + 1];
int HEAD_98_102 = 0;
int TAIL_98_102 = 0;
float BUFFER_99_100[__BUF_SIZE_MASK_99_100 + 1];
int HEAD_99_100 = 0;
int TAIL_99_100 = 0;
float BUFFER_100_101[__BUF_SIZE_MASK_100_101 + 1];
int HEAD_100_101 = 0;
int TAIL_100_101 = 0;
float BUFFER_101_9[__BUF_SIZE_MASK_101_9 + 1];
int HEAD_101_9 = 0;
int TAIL_101_9 = 0;
float BUFFER_102_100[__BUF_SIZE_MASK_102_100 + 1];
int HEAD_102_100 = 0;
int TAIL_102_100 = 0;
float BUFFER_103_104[__BUF_SIZE_MASK_103_104 + 1];
int HEAD_103_104 = 0;
int TAIL_103_104 = 0;
float BUFFER_104_105[__BUF_SIZE_MASK_104_105 + 1];
int HEAD_104_105 = 0;
int TAIL_104_105 = 0;
float BUFFER_105_106[__BUF_SIZE_MASK_105_106 + 1];
int HEAD_105_106 = 0;
int TAIL_105_106 = 0;
float BUFFER_105_109[__BUF_SIZE_MASK_105_109 + 1];
int HEAD_105_109 = 0;
int TAIL_105_109 = 0;
float BUFFER_106_107[__BUF_SIZE_MASK_106_107 + 1];
int HEAD_106_107 = 0;
int TAIL_106_107 = 0;
float BUFFER_107_108[__BUF_SIZE_MASK_107_108 + 1];
int HEAD_107_108 = 0;
int TAIL_107_108 = 0;
float BUFFER_108_9[__BUF_SIZE_MASK_108_9 + 1];
int HEAD_108_9 = 0;
int TAIL_108_9 = 0;
float BUFFER_109_107[__BUF_SIZE_MASK_109_107 + 1];
int HEAD_109_107 = 0;
int TAIL_109_107 = 0;
float BUFFER_110_111[__BUF_SIZE_MASK_110_111 + 1];
int HEAD_110_111 = 0;
int TAIL_110_111 = 0;
float BUFFER_111_112[__BUF_SIZE_MASK_111_112 + 1];
int HEAD_111_112 = 0;
int TAIL_111_112 = 0;
float BUFFER_112_113[__BUF_SIZE_MASK_112_113 + 1];
int HEAD_112_113 = 0;
int TAIL_112_113 = 0;
float BUFFER_112_116[__BUF_SIZE_MASK_112_116 + 1];
int HEAD_112_116 = 0;
int TAIL_112_116 = 0;
float BUFFER_113_114[__BUF_SIZE_MASK_113_114 + 1];
int HEAD_113_114 = 0;
int TAIL_113_114 = 0;
float BUFFER_114_115[__BUF_SIZE_MASK_114_115 + 1];
int HEAD_114_115 = 0;
int TAIL_114_115 = 0;
float BUFFER_115_9[__BUF_SIZE_MASK_115_9 + 1];
int HEAD_115_9 = 0;
int TAIL_115_9 = 0;
float BUFFER_116_114[__BUF_SIZE_MASK_116_114 + 1];
int HEAD_116_114 = 0;
int TAIL_116_114 = 0;
void init_src__5_57__0();
void work_src__5_57__0(int);
void init_Framer__10_58__1();
void work_Framer__10_58__1(int);
void __splitter_2_work(int);
void init_GetAvg__17_60__3();
void work_GetAvg__17_60__3(int);
void init_DataMinusAvg__25_62__4();
void work_DataMinusAvg__25_62__4(int);
void __splitter_5_work(int);
void init_DenomDataSum__31_64__6();
void work_DenomDataSum__31_64__6(int);
void __joiner_7_work(int);
void init_Division__51_66__8();
void work_Division__51_66__8(int);
void __joiner_9_work(int);
void init_SecondCounter__671_187__10();
void work_SecondCounter__671_187__10(int);
void init_PatternSums__40_65__11();
void work_PatternSums__40_65__11(int);
void init_GetAvg__58_68__12();
void work_GetAvg__58_68__12(int);
void init_DataMinusAvg__66_70__13();
void work_DataMinusAvg__66_70__13(int);
void __splitter_14_work(int);
void init_DenomDataSum__72_72__15();
void work_DenomDataSum__72_72__15(int);
void __joiner_16_work(int);
void init_Division__92_74__17();
void work_Division__92_74__17(int);
void init_PatternSums__81_73__18();
void work_PatternSums__81_73__18(int);
void init_GetAvg__99_76__19();
void work_GetAvg__99_76__19(int);
void init_DataMinusAvg__107_78__20();
void work_DataMinusAvg__107_78__20(int);
void __splitter_21_work(int);
void init_DenomDataSum__113_80__22();
void work_DenomDataSum__113_80__22(int);
void __joiner_23_work(int);
void init_Division__133_82__24();
void work_Division__133_82__24(int);
void init_PatternSums__122_81__25();
void work_PatternSums__122_81__25(int);
void init_GetAvg__140_84__26();
void work_GetAvg__140_84__26(int);
void init_DataMinusAvg__148_86__27();
void work_DataMinusAvg__148_86__27(int);
void __splitter_28_work(int);
void init_DenomDataSum__154_88__29();
void work_DenomDataSum__154_88__29(int);
void __joiner_30_work(int);
void init_Division__174_90__31();
void work_Division__174_90__31(int);
void init_PatternSums__163_89__32();
void work_PatternSums__163_89__32(int);
void init_GetAvg__181_92__33();
void work_GetAvg__181_92__33(int);
void init_DataMinusAvg__189_94__34();
void work_DataMinusAvg__189_94__34(int);
void __splitter_35_work(int);
void init_DenomDataSum__195_96__36();
void work_DenomDataSum__195_96__36(int);
void __joiner_37_work(int);
void init_Division__215_98__38();
void work_Division__215_98__38(int);
void init_PatternSums__204_97__39();
void work_PatternSums__204_97__39(int);
void init_GetAvg__222_100__40();
void work_GetAvg__222_100__40(int);
void init_DataMinusAvg__230_102__41();
void work_DataMinusAvg__230_102__41(int);
void __splitter_42_work(int);
void init_DenomDataSum__236_104__43();
void work_DenomDataSum__236_104__43(int);
void __joiner_44_work(int);
void init_Division__256_106__45();
void work_Division__256_106__45(int);
void init_PatternSums__245_105__46();
void work_PatternSums__245_105__46(int);
void init_GetAvg__263_108__47();
void work_GetAvg__263_108__47(int);
void init_DataMinusAvg__271_110__48();
void work_DataMinusAvg__271_110__48(int);
void __splitter_49_work(int);
void init_DenomDataSum__277_112__50();
void work_DenomDataSum__277_112__50(int);
void __joiner_51_work(int);
void init_Division__297_114__52();
void work_Division__297_114__52(int);
void init_PatternSums__286_113__53();
void work_PatternSums__286_113__53(int);
void init_GetAvg__304_116__54();
void work_GetAvg__304_116__54(int);
void init_DataMinusAvg__312_118__55();
void work_DataMinusAvg__312_118__55(int);
void __splitter_56_work(int);
void init_DenomDataSum__318_120__57();
void work_DenomDataSum__318_120__57(int);
void __joiner_58_work(int);
void init_Division__338_122__59();
void work_Division__338_122__59(int);
void init_PatternSums__327_121__60();
void work_PatternSums__327_121__60(int);
void init_GetAvg__345_124__61();
void work_GetAvg__345_124__61(int);
void init_DataMinusAvg__353_126__62();
void work_DataMinusAvg__353_126__62(int);
void __splitter_63_work(int);
void init_DenomDataSum__359_128__64();
void work_DenomDataSum__359_128__64(int);
void __joiner_65_work(int);
void init_Division__379_130__66();
void work_Division__379_130__66(int);
void init_PatternSums__368_129__67();
void work_PatternSums__368_129__67(int);
void init_GetAvg__386_132__68();
void work_GetAvg__386_132__68(int);
void init_DataMinusAvg__394_134__69();
void work_DataMinusAvg__394_134__69(int);
void __splitter_70_work(int);
void init_DenomDataSum__400_136__71();
void work_DenomDataSum__400_136__71(int);
void __joiner_72_work(int);
void init_Division__420_138__73();
void work_Division__420_138__73(int);
void init_PatternSums__409_137__74();
void work_PatternSums__409_137__74(int);
void init_GetAvg__427_140__75();
void work_GetAvg__427_140__75(int);
void init_DataMinusAvg__435_142__76();
void work_DataMinusAvg__435_142__76(int);
void __splitter_77_work(int);
void init_DenomDataSum__441_144__78();
void work_DenomDataSum__441_144__78(int);
void __joiner_79_work(int);
void init_Division__461_146__80();
void work_Division__461_146__80(int);
void init_PatternSums__450_145__81();
void work_PatternSums__450_145__81(int);
void init_GetAvg__468_148__82();
void work_GetAvg__468_148__82(int);
void init_DataMinusAvg__476_150__83();
void work_DataMinusAvg__476_150__83(int);
void __splitter_84_work(int);
void init_DenomDataSum__482_152__85();
void work_DenomDataSum__482_152__85(int);
void __joiner_86_work(int);
void init_Division__502_154__87();
void work_Division__502_154__87(int);
void init_PatternSums__491_153__88();
void work_PatternSums__491_153__88(int);
void init_GetAvg__509_156__89();
void work_GetAvg__509_156__89(int);
void init_DataMinusAvg__517_158__90();
void work_DataMinusAvg__517_158__90(int);
void __splitter_91_work(int);
void init_DenomDataSum__523_160__92();
void work_DenomDataSum__523_160__92(int);
void __joiner_93_work(int);
void init_Division__543_162__94();
void work_Division__543_162__94(int);
void init_PatternSums__532_161__95();
void work_PatternSums__532_161__95(int);
void init_GetAvg__550_164__96();
void work_GetAvg__550_164__96(int);
void init_DataMinusAvg__558_166__97();
void work_DataMinusAvg__558_166__97(int);
void __splitter_98_work(int);
void init_DenomDataSum__564_168__99();
void work_DenomDataSum__564_168__99(int);
void __joiner_100_work(int);
void init_Division__584_170__101();
void work_Division__584_170__101(int);
void init_PatternSums__573_169__102();
void work_PatternSums__573_169__102(int);
void init_GetAvg__591_172__103();
void work_GetAvg__591_172__103(int);
void init_DataMinusAvg__599_174__104();
void work_DataMinusAvg__599_174__104(int);
void __splitter_105_work(int);
void init_DenomDataSum__605_176__106();
void work_DenomDataSum__605_176__106(int);
void __joiner_107_work(int);
void init_Division__625_178__108();
void work_Division__625_178__108(int);
void init_PatternSums__614_177__109();
void work_PatternSums__614_177__109(int);
void init_GetAvg__632_180__110();
void work_GetAvg__632_180__110(int);
void init_DataMinusAvg__640_182__111();
void work_DataMinusAvg__640_182__111(int);
void __splitter_112_work(int);
void init_DenomDataSum__646_184__113();
void work_DenomDataSum__646_184__113(int);
void __joiner_114_work(int);
void init_Division__666_186__115();
void work_Division__666_186__115(int);
void init_PatternSums__655_185__116();
void work_PatternSums__655_185__116(int);

static short* samples;

#include "log.h"

void initialization(void* data) {
    samples = (short*)data;
    init_src__5_57__0();   work_src__5_57__0(255);
    init_Framer__10_58__1();
    init_GetAvg__58_68__12();
    init_GetAvg__222_100__40();
    init_GetAvg__386_132__68();
    init_GetAvg__550_164__96();
    init_GetAvg__99_76__19();
    init_GetAvg__263_108__47();
    init_GetAvg__427_140__75();
    init_GetAvg__591_172__103();
    init_GetAvg__140_84__26();
    init_GetAvg__304_116__54();
    init_GetAvg__468_148__82();
    init_GetAvg__632_180__110();
    init_GetAvg__17_60__3();
    init_GetAvg__181_92__33();
    init_GetAvg__345_124__61();
    init_GetAvg__509_156__89();
    init_DataMinusAvg__66_70__13();
    init_DataMinusAvg__230_102__41();
    init_DataMinusAvg__394_134__69();
    init_DataMinusAvg__558_166__97();
    init_DataMinusAvg__107_78__20();
    init_DataMinusAvg__271_110__48();
    init_DataMinusAvg__435_142__76();
    init_DataMinusAvg__599_174__104();
    init_DataMinusAvg__148_86__27();
    init_DataMinusAvg__312_118__55();
    init_DataMinusAvg__476_150__83();
    init_DataMinusAvg__640_182__111();
    init_DataMinusAvg__25_62__4();
    init_DataMinusAvg__189_94__34();
    init_DataMinusAvg__353_126__62();
    init_DataMinusAvg__517_158__90();
    init_DenomDataSum__31_64__6();
    init_DenomDataSum__359_128__64();
    init_PatternSums__40_65__11();
    init_PatternSums__368_129__67();
    init_DenomDataSum__72_72__15();
    init_DenomDataSum__400_136__71();
    init_PatternSums__81_73__18();
    init_PatternSums__409_137__74();
    init_DenomDataSum__113_80__22();
    init_DenomDataSum__441_144__78();
    init_PatternSums__122_81__25();
    init_PatternSums__450_145__81();
    init_DenomDataSum__154_88__29();
    init_DenomDataSum__482_152__85();
    init_PatternSums__163_89__32();
    init_PatternSums__491_153__88();
    init_DenomDataSum__195_96__36();
    init_DenomDataSum__523_160__92();
    init_PatternSums__204_97__39();
    init_PatternSums__532_161__95();
    init_DenomDataSum__236_104__43();
    init_DenomDataSum__564_168__99();
    init_PatternSums__245_105__46();
    init_PatternSums__573_169__102();
    init_DenomDataSum__277_112__50();
    init_DenomDataSum__605_176__106();
    init_PatternSums__286_113__53();
    init_PatternSums__614_177__109();
    init_DenomDataSum__318_120__57();
    init_DenomDataSum__646_184__113();
    init_PatternSums__327_121__60();
    init_PatternSums__655_185__116();
    init_Division__51_66__8();
    init_Division__215_98__38();
    init_Division__379_130__66();
    init_Division__543_162__94();
    init_Division__92_74__17();
    init_Division__256_106__45();
    init_Division__420_138__73();
    init_Division__584_170__101();
    init_Division__133_82__24();
    init_Division__297_114__52();
    init_Division__461_146__80();
    init_Division__625_178__108();
    init_Division__174_90__31();
    init_Division__338_122__59();
    init_Division__502_154__87();
    init_Division__666_186__115();
    init_SecondCounter__671_187__10();
}

static inline void iteration() {
    for (int __y = 0; __y < __PEEK_BUF_SIZE_0_1; __y++) {
      BUFFER_0_1[__y] = BUFFER_0_1[__y + TAIL_0_1];
    }
    HEAD_0_1 -= TAIL_0_1;
    TAIL_0_1 = 0;
        work_src__5_57__0(16 );
    HEAD_1_2 = 0;
    TAIL_1_2 = 0;
        work_Framer__10_58__1(16 );
    HEAD_2_3 = 0;
    TAIL_2_3 = 0;
    HEAD_2_12 = 0;
    TAIL_2_12 = 0;
    HEAD_2_19 = 0;
    TAIL_2_19 = 0;
    HEAD_2_26 = 0;
    TAIL_2_26 = 0;
    HEAD_2_33 = 0;
    TAIL_2_33 = 0;
    HEAD_2_40 = 0;
    TAIL_2_40 = 0;
    HEAD_2_47 = 0;
    TAIL_2_47 = 0;
    HEAD_2_54 = 0;
    TAIL_2_54 = 0;
    HEAD_2_61 = 0;
    TAIL_2_61 = 0;
    HEAD_2_68 = 0;
    TAIL_2_68 = 0;
    HEAD_2_75 = 0;
    TAIL_2_75 = 0;
    HEAD_2_82 = 0;
    TAIL_2_82 = 0;
    HEAD_2_89 = 0;
    TAIL_2_89 = 0;
    HEAD_2_96 = 0;
    TAIL_2_96 = 0;
    HEAD_2_103 = 0;
    TAIL_2_103 = 0;
    HEAD_2_110 = 0;
    TAIL_2_110 = 0;
        __splitter_2_work(1 );
    HEAD_12_13 = 0;
    TAIL_12_13 = 0;
        work_GetAvg__58_68__12(1 );
    HEAD_40_41 = 0;
    TAIL_40_41 = 0;
        work_GetAvg__222_100__40(1 );
    HEAD_68_69 = 0;
    TAIL_68_69 = 0;
        work_GetAvg__386_132__68(1 );
    HEAD_96_97 = 0;
    TAIL_96_97 = 0;
        work_GetAvg__550_164__96(1 );
    HEAD_19_20 = 0;
    TAIL_19_20 = 0;
        work_GetAvg__99_76__19(1 );
    HEAD_47_48 = 0;
    TAIL_47_48 = 0;
        work_GetAvg__263_108__47(1 );
    HEAD_75_76 = 0;
    TAIL_75_76 = 0;
        work_GetAvg__427_140__75(1 );
    HEAD_103_104 = 0;
    TAIL_103_104 = 0;
        work_GetAvg__591_172__103(1 );
    HEAD_26_27 = 0;
    TAIL_26_27 = 0;
        work_GetAvg__140_84__26(1 );
    HEAD_54_55 = 0;
    TAIL_54_55 = 0;
        work_GetAvg__304_116__54(1 );
    HEAD_82_83 = 0;
    TAIL_82_83 = 0;
        work_GetAvg__468_148__82(1 );
    HEAD_110_111 = 0;
    TAIL_110_111 = 0;
        work_GetAvg__632_180__110(1 );
    HEAD_3_4 = 0;
    TAIL_3_4 = 0;
        work_GetAvg__17_60__3(1 );
    HEAD_33_34 = 0;
    TAIL_33_34 = 0;
        work_GetAvg__181_92__33(1 );
    HEAD_61_62 = 0;
    TAIL_61_62 = 0;
        work_GetAvg__345_124__61(1 );
    HEAD_89_90 = 0;
    TAIL_89_90 = 0;
        work_GetAvg__509_156__89(1 );
    HEAD_13_14 = 0;
    TAIL_13_14 = 0;
        work_DataMinusAvg__66_70__13(1 );
    HEAD_41_42 = 0;
    TAIL_41_42 = 0;
        work_DataMinusAvg__230_102__41(1 );
    HEAD_69_70 = 0;
    TAIL_69_70 = 0;
        work_DataMinusAvg__394_134__69(1 );
    HEAD_97_98 = 0;
    TAIL_97_98 = 0;
        work_DataMinusAvg__558_166__97(1 );
    HEAD_20_21 = 0;
    TAIL_20_21 = 0;
        work_DataMinusAvg__107_78__20(1 );
    HEAD_48_49 = 0;
    TAIL_48_49 = 0;
        work_DataMinusAvg__271_110__48(1 );
    HEAD_76_77 = 0;
    TAIL_76_77 = 0;
        work_DataMinusAvg__435_142__76(1 );
    HEAD_104_105 = 0;
    TAIL_104_105 = 0;
        work_DataMinusAvg__599_174__104(1 );
    HEAD_27_28 = 0;
    TAIL_27_28 = 0;
        work_DataMinusAvg__148_86__27(1 );
    HEAD_55_56 = 0;
    TAIL_55_56 = 0;
        work_DataMinusAvg__312_118__55(1 );
    HEAD_83_84 = 0;
    TAIL_83_84 = 0;
        work_DataMinusAvg__476_150__83(1 );
    HEAD_111_112 = 0;
    TAIL_111_112 = 0;
        work_DataMinusAvg__640_182__111(1 );
    HEAD_4_5 = 0;
    TAIL_4_5 = 0;
        work_DataMinusAvg__25_62__4(1 );
    HEAD_34_35 = 0;
    TAIL_34_35 = 0;
        work_DataMinusAvg__189_94__34(1 );
    HEAD_62_63 = 0;
    TAIL_62_63 = 0;
        work_DataMinusAvg__353_126__62(1 );
    HEAD_90_91 = 0;
    TAIL_90_91 = 0;
        work_DataMinusAvg__517_158__90(1 );
    HEAD_14_15 = 0;
    TAIL_14_15 = 0;
    HEAD_14_18 = 0;
    TAIL_14_18 = 0;
        __splitter_14_work(256 );
    HEAD_21_22 = 0;
    TAIL_21_22 = 0;
    HEAD_21_25 = 0;
    TAIL_21_25 = 0;
        __splitter_21_work(256 );
    HEAD_28_29 = 0;
    TAIL_28_29 = 0;
    HEAD_28_32 = 0;
    TAIL_28_32 = 0;
        __splitter_28_work(256 );
    HEAD_35_36 = 0;
    TAIL_35_36 = 0;
    HEAD_35_39 = 0;
    TAIL_35_39 = 0;
        __splitter_35_work(256 );
    HEAD_42_43 = 0;
    TAIL_42_43 = 0;
    HEAD_42_46 = 0;
    TAIL_42_46 = 0;
        __splitter_42_work(256 );
    HEAD_49_50 = 0;
    TAIL_49_50 = 0;
    HEAD_49_53 = 0;
    TAIL_49_53 = 0;
        __splitter_49_work(256 );
    HEAD_56_57 = 0;
    TAIL_56_57 = 0;
    HEAD_56_60 = 0;
    TAIL_56_60 = 0;
        __splitter_56_work(256 );
    HEAD_63_64 = 0;
    TAIL_63_64 = 0;
    HEAD_63_67 = 0;
    TAIL_63_67 = 0;
        __splitter_63_work(256 );
    HEAD_70_71 = 0;
    TAIL_70_71 = 0;
    HEAD_70_74 = 0;
    TAIL_70_74 = 0;
        __splitter_70_work(256 );
    HEAD_77_78 = 0;
    TAIL_77_78 = 0;
    HEAD_77_81 = 0;
    TAIL_77_81 = 0;
        __splitter_77_work(256 );
    HEAD_84_85 = 0;
    TAIL_84_85 = 0;
    HEAD_84_88 = 0;
    TAIL_84_88 = 0;
        __splitter_84_work(256 );
    HEAD_91_92 = 0;
    TAIL_91_92 = 0;
    HEAD_91_95 = 0;
    TAIL_91_95 = 0;
        __splitter_91_work(256 );
    HEAD_98_99 = 0;
    TAIL_98_99 = 0;
    HEAD_98_102 = 0;
    TAIL_98_102 = 0;
        __splitter_98_work(256 );
    HEAD_105_106 = 0;
    TAIL_105_106 = 0;
    HEAD_105_109 = 0;
    TAIL_105_109 = 0;
        __splitter_105_work(256 );
    HEAD_112_113 = 0;
    TAIL_112_113 = 0;
    HEAD_112_116 = 0;
    TAIL_112_116 = 0;
        __splitter_112_work(256 );
    HEAD_5_6 = 0;
    TAIL_5_6 = 0;
    HEAD_5_11 = 0;
    TAIL_5_11 = 0;
        __splitter_5_work(256 );
    HEAD_6_7 = 0;
    TAIL_6_7 = 0;
        work_DenomDataSum__31_64__6(1 );
    HEAD_64_65 = 0;
    TAIL_64_65 = 0;
        work_DenomDataSum__359_128__64(1 );
    HEAD_11_7 = 0;
    TAIL_11_7 = 0;
        work_PatternSums__40_65__11(1 );
    HEAD_67_65 = 0;
    TAIL_67_65 = 0;
        work_PatternSums__368_129__67(1 );
    HEAD_15_16 = 0;
    TAIL_15_16 = 0;
        work_DenomDataSum__72_72__15(1 );
    HEAD_71_72 = 0;
    TAIL_71_72 = 0;
        work_DenomDataSum__400_136__71(1 );
    HEAD_18_16 = 0;
    TAIL_18_16 = 0;
        work_PatternSums__81_73__18(1 );
    HEAD_74_72 = 0;
    TAIL_74_72 = 0;
        work_PatternSums__409_137__74(1 );
    HEAD_22_23 = 0;
    TAIL_22_23 = 0;
        work_DenomDataSum__113_80__22(1 );
    HEAD_78_79 = 0;
    TAIL_78_79 = 0;
        work_DenomDataSum__441_144__78(1 );
    HEAD_25_23 = 0;
    TAIL_25_23 = 0;
        work_PatternSums__122_81__25(1 );
    HEAD_81_79 = 0;
    TAIL_81_79 = 0;
        work_PatternSums__450_145__81(1 );
    HEAD_29_30 = 0;
    TAIL_29_30 = 0;
        work_DenomDataSum__154_88__29(1 );
    HEAD_85_86 = 0;
    TAIL_85_86 = 0;
        work_DenomDataSum__482_152__85(1 );
    HEAD_32_30 = 0;
    TAIL_32_30 = 0;
        work_PatternSums__163_89__32(1 );
    HEAD_88_86 = 0;
    TAIL_88_86 = 0;
        work_PatternSums__491_153__88(1 );
    HEAD_36_37 = 0;
    TAIL_36_37 = 0;
        work_DenomDataSum__195_96__36(1 );
    HEAD_92_93 = 0;
    TAIL_92_93 = 0;
        work_DenomDataSum__523_160__92(1 );
    HEAD_39_37 = 0;
    TAIL_39_37 = 0;
        work_PatternSums__204_97__39(1 );
    HEAD_95_93 = 0;
    TAIL_95_93 = 0;
        work_PatternSums__532_161__95(1 );
    HEAD_43_44 = 0;
    TAIL_43_44 = 0;
        work_DenomDataSum__236_104__43(1 );
    HEAD_99_100 = 0;
    TAIL_99_100 = 0;
        work_DenomDataSum__564_168__99(1 );
    HEAD_46_44 = 0;
    TAIL_46_44 = 0;
        work_PatternSums__245_105__46(1 );
    HEAD_102_100 = 0;
    TAIL_102_100 = 0;
        work_PatternSums__573_169__102(1 );
    HEAD_50_51 = 0;
    TAIL_50_51 = 0;
        work_DenomDataSum__277_112__50(1 );
    HEAD_106_107 = 0;
    TAIL_106_107 = 0;
        work_DenomDataSum__605_176__106(1 );
    HEAD_53_51 = 0;
    TAIL_53_51 = 0;
        work_PatternSums__286_113__53(1 );
    HEAD_109_107 = 0;
    TAIL_109_107 = 0;
        work_PatternSums__614_177__109(1 );
    HEAD_57_58 = 0;
    TAIL_57_58 = 0;
        work_DenomDataSum__318_120__57(1 );
    HEAD_113_114 = 0;
    TAIL_113_114 = 0;
        work_DenomDataSum__646_184__113(1 );
    HEAD_60_58 = 0;
    TAIL_60_58 = 0;
        work_PatternSums__327_121__60(1 );
    HEAD_116_114 = 0;
    TAIL_116_114 = 0;
        work_PatternSums__655_185__116(1 );
    HEAD_16_17 = 0;
    TAIL_16_17 = 0;
        __joiner_16_work(1 );
    HEAD_23_24 = 0;
    TAIL_23_24 = 0;
        __joiner_23_work(1 );
    HEAD_30_31 = 0;
    TAIL_30_31 = 0;
        __joiner_30_work(1 );
    HEAD_37_38 = 0;
    TAIL_37_38 = 0;
        __joiner_37_work(1 );
    HEAD_44_45 = 0;
    TAIL_44_45 = 0;
        __joiner_44_work(1 );
    HEAD_51_52 = 0;
    TAIL_51_52 = 0;
        __joiner_51_work(1 );
    HEAD_58_59 = 0;
    TAIL_58_59 = 0;
        __joiner_58_work(1 );
    HEAD_65_66 = 0;
    TAIL_65_66 = 0;
        __joiner_65_work(1 );
    HEAD_72_73 = 0;
    TAIL_72_73 = 0;
        __joiner_72_work(1 );
    HEAD_79_80 = 0;
    TAIL_79_80 = 0;
        __joiner_79_work(1 );
    HEAD_86_87 = 0;
    TAIL_86_87 = 0;
        __joiner_86_work(1 );
    HEAD_93_94 = 0;
    TAIL_93_94 = 0;
        __joiner_93_work(1 );
    HEAD_100_101 = 0;
    TAIL_100_101 = 0;
        __joiner_100_work(1 );
    HEAD_107_108 = 0;
    TAIL_107_108 = 0;
        __joiner_107_work(1 );
    HEAD_114_115 = 0;
    TAIL_114_115 = 0;
        __joiner_114_work(1 );
    HEAD_7_8 = 0;
    TAIL_7_8 = 0;
        __joiner_7_work(1 );
    HEAD_8_9 = 0;
    TAIL_8_9 = 0;
        work_Division__51_66__8(1 );
    HEAD_38_9 = 0;
    TAIL_38_9 = 0;
        work_Division__215_98__38(1 );
    HEAD_66_9 = 0;
    TAIL_66_9 = 0;
        work_Division__379_130__66(1 );
    HEAD_94_9 = 0;
    TAIL_94_9 = 0;
        work_Division__543_162__94(1 );
    HEAD_17_9 = 0;
    TAIL_17_9 = 0;
        work_Division__92_74__17(1 );
    HEAD_45_9 = 0;
    TAIL_45_9 = 0;
        work_Division__256_106__45(1 );
    HEAD_73_9 = 0;
    TAIL_73_9 = 0;
        work_Division__420_138__73(1 );
    HEAD_101_9 = 0;
    TAIL_101_9 = 0;
        work_Division__584_170__101(1 );
    HEAD_24_9 = 0;
    TAIL_24_9 = 0;
        work_Division__133_82__24(1 );
    HEAD_52_9 = 0;
    TAIL_52_9 = 0;
        work_Division__297_114__52(1 );
    HEAD_80_9 = 0;
    TAIL_80_9 = 0;
        work_Division__461_146__80(1 );
    HEAD_108_9 = 0;
    TAIL_108_9 = 0;
        work_Division__625_178__108(1 );
    HEAD_31_9 = 0;
    TAIL_31_9 = 0;
        work_Division__174_90__31(1 );
    HEAD_59_9 = 0;
    TAIL_59_9 = 0;
        work_Division__338_122__59(1 );
    HEAD_87_9 = 0;
    TAIL_87_9 = 0;
        work_Division__502_154__87(1 );
    HEAD_115_9 = 0;
    TAIL_115_9 = 0;
        work_Division__666_186__115(1 );
    HEAD_9_10 = 0;
    TAIL_9_10 = 0;
        __joiner_9_work(1 );
        work_SecondCounter__671_187__10(16 );
}

void iterations(int periods, void* data) {
    samples = (short*)data;
    for(int p = 0; p < periods; p++)
        iteration();
}

int main(int argc, char **argv) {
  read_setup::read_setup_file();
  __max_iteration = read_setup::max_iteration;
  for (int a = 1; a < argc; a++) {
    if (argc > a + 1 && strcmp(argv[a], "-i") == 0) {
      int tmp;
      sscanf(argv[a + 1], "%d", &tmp);
#ifdef VERBOSE
#endif
      __max_iteration = tmp;
    }
    if (strcmp(argv[a], "-t") == 0) {
#ifdef VERBOSE
#endif
      __timer_enabled = 1;
    }
  }
// number of phases: 11


  // ============= Initialization =============



  // ============= Steady State =============

  if (__timer_enabled) {
  }
  for (int n = 0; n < (__max_iteration  ); n++) {

  }
if (__timer_enabled) {
  }


  return 0;
}

// moved or inserted by concat_cluster_threads.pl
#include <message.h>
message *__msg_stack_97;
message *__msg_stack_38;
message *__msg_stack_103;
message *__msg_stack_105;
message *__msg_stack_10;
message *__msg_stack_70;
message *__msg_stack_81;
message *__msg_stack_42;
message *__msg_stack_84;
message *__msg_stack_91;
message *__msg_stack_9;
message *__msg_stack_56;
message *__msg_stack_47;
message *__msg_stack_31;
message *__msg_stack_83;
message *__msg_stack_107;
message *__msg_stack_95;
message *__msg_stack_102;
message *__msg_stack_65;
message *__msg_stack_55;
message *__msg_stack_14;
message *__msg_stack_101;
message *__msg_stack_77;
message *__msg_stack_19;
message *__msg_stack_2;
message *__msg_stack_66;
message *__msg_stack_52;
message *__msg_stack_23;
message *__msg_stack_100;
message *__msg_stack_73;
message *__msg_stack_30;
message *__msg_stack_87;
message *__msg_stack_7;
message *__msg_stack_6;
message *__msg_stack_78;
message *__msg_stack_63;
message *__msg_stack_0;
message *__msg_stack_90;
message *__msg_stack_27;
message *__msg_stack_86;
message *__msg_stack_76;
message *__msg_stack_75;
message *__msg_stack_11;
message *__msg_stack_13;
message *__msg_stack_18;
message *__msg_stack_93;
message *__msg_stack_85;
message *__msg_stack_82;
message *__msg_stack_57;
message *__msg_stack_43;
message *__msg_stack_60;
message *__msg_stack_37;
message *__msg_stack_114;
message *__msg_stack_108;
message *__msg_stack_34;
message *__msg_stack_113;
message *__msg_stack_112;
message *__msg_stack_3;
message *__msg_stack_64;
message *__msg_stack_24;
message *__msg_stack_106;
message *__msg_stack_59;
message *__msg_stack_46;
message *__msg_stack_116;
message *__msg_stack_40;
message *__msg_stack_69;
message *__msg_stack_99;
message *__msg_stack_49;
message *__msg_stack_1;
message *__msg_stack_111;
message *__msg_stack_67;
message *__msg_stack_15;
message *__msg_stack_5;
message *__msg_stack_45;
message *__msg_stack_71;
message *__msg_stack_17;
message *__msg_stack_92;
message *__msg_stack_21;
message *__msg_stack_74;
message *__msg_stack_58;
message *__msg_stack_41;
message *__msg_stack_48;
message *__msg_stack_44;
message *__msg_stack_36;
message *__msg_stack_26;
message *__msg_stack_29;
message *__msg_stack_89;
message *__msg_stack_68;
message *__msg_stack_62;
message *__msg_stack_35;
message *__msg_stack_50;
message *__msg_stack_25;
message *__msg_stack_20;
message *__msg_stack_4;
message *__msg_stack_110;
message *__msg_stack_98;
message *__msg_stack_53;
message *__msg_stack_80;
message *__msg_stack_61;
message *__msg_stack_33;
message *__msg_stack_94;
message *__msg_stack_8;
message *__msg_stack_109;
message *__msg_stack_32;
message *__msg_stack_12;
message *__msg_stack_115;
message *__msg_stack_28;
message *__msg_stack_104;
message *__msg_stack_51;
message *__msg_stack_16;
message *__msg_stack_88;
message *__msg_stack_79;
message *__msg_stack_54;
message *__msg_stack_22;
message *__msg_stack_39;
message *__msg_stack_96;
message *__msg_stack_72;

// end of moved or inserted by concat_cluster_threads.pl

// peek: 0 pop: 0 push 1
// init counts: 255 steady counts: 16

// ClusterFusion isEliminated: false


#include <mysocket.h>
#include <sdep.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "global.h"

int __number_of_iterations_0;
int __counter_0 = 0;
int __steady_0 = 0;
int __tmp_0 = 0;
int __tmp2_0 = 0;
int *__state_flag_0 = NULL;
thread_info *__thread_0 = NULL;



void save_file_pointer__0(object_write_buffer *buf);
void load_file_pointer__0(object_write_buffer *buf);

inline void check_status__0() {
  check_thread_status(__state_flag_0, __thread_0);
}

void check_status_during_io__0() {
  check_thread_status_during_io(__state_flag_0, __thread_0);
}

void __init_thread_info_0(thread_info *info) {
  __state_flag_0 = info->get_state_flag();
}

thread_info *__get_thread_info_0() {
  if (__thread_0 != NULL) return __thread_0;
  __thread_0 = new thread_info(0, check_status_during_io__0);
  __init_thread_info_0(__thread_0);
  return __thread_0;
}

void __declare_sockets_0() {
  init_instance::add_outgoing(0,1, DATA_SOCKET);
}

void __init_sockets_0(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_0() {
}

void __peek_sockets_0() {
}

 
float short2float__0__0(int sample__3); 
void init_src__5_57__0();
inline void check_status__0();

void work_src__5_57__0(int);



inline void __push_0_1(float data) {
BUFFER_0_1[HEAD_0_1]=data;
HEAD_0_1++;
}



 
float short2float__0__0(int sample__3){
  return ((float)(sample__3));
}
 
void init_src__5_57__0(){
}
void save_file_pointer__0(object_write_buffer *buf) {}
void load_file_pointer__0(object_write_buffer *buf) {}
 
void work_src__5_57__0(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp2__4 = 0.0f;/* float */

      // mark begin: SIRFilter src

      (__tmp2__4 = short2float__0__0(*samples++))/*float*/;
      __push_0_1(__tmp2__4);
      // mark end: SIRFilter src

    }
  }
}

// peek: 256 pop: 1 push 256
// init counts: 0 steady counts: 16

// ClusterFusion isEliminated: false



int __number_of_iterations_1;
int __counter_1 = 0;
int __steady_1 = 0;
int __tmp_1 = 0;
int __tmp2_1 = 0;
int *__state_flag_1 = NULL;
thread_info *__thread_1 = NULL;



int i__6__1 = 0;
void save_peek_buffer__1(object_write_buffer *buf);
void load_peek_buffer__1(object_write_buffer *buf);
void save_file_pointer__1(object_write_buffer *buf);
void load_file_pointer__1(object_write_buffer *buf);

inline void check_status__1() {
  check_thread_status(__state_flag_1, __thread_1);
}

void check_status_during_io__1() {
  check_thread_status_during_io(__state_flag_1, __thread_1);
}

void __init_thread_info_1(thread_info *info) {
  __state_flag_1 = info->get_state_flag();
}

thread_info *__get_thread_info_1() {
  if (__thread_1 != NULL) return __thread_1;
  __thread_1 = new thread_info(1, check_status_during_io__1);
  __init_thread_info_1(__thread_1);
  return __thread_1;
}

void __declare_sockets_1() {
  init_instance::add_incoming(0,1, DATA_SOCKET);
  init_instance::add_outgoing(1,2, DATA_SOCKET);
}

void __init_sockets_1(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_1() {
}

void __peek_sockets_1() {
}

 
void init_Framer__10_58__1();
inline void check_status__1();

void work_Framer__10_58__1(int);


inline float __pop_0_1() {
float res=BUFFER_0_1[TAIL_0_1];
TAIL_0_1++;
return res;
}

inline void __pop_0_1(int n) {
TAIL_0_1+=n;
}

inline float __peek_0_1(int offs) {
return BUFFER_0_1[TAIL_0_1+offs];
}



inline void __push_1_2(float data) {
BUFFER_1_2[HEAD_1_2]=data;
HEAD_1_2++;
}



 
void init_Framer__10_58__1(){
}
void save_file_pointer__1(object_write_buffer *buf) {}
void load_file_pointer__1(object_write_buffer *buf) {}
 
void work_Framer__10_58__1(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp7__9 = 0.0f;/* float */

      // mark begin: SIRFilter Framer

      for (((i__6__1) = 0)/*int*/; ((i__6__1) < 256); ((i__6__1)++)) {{
          __tmp7__9 = BUFFER_0_1[TAIL_0_1+(i__6__1)];
;
          __push_1_2(__tmp7__9);
        }
      }
      __pop_0_1();
      // mark end: SIRFilter Framer

    }
  }
}

// peek: 1 pop: 1 push 0
// init counts: 0 steady counts: 16

// ClusterFusion isEliminated: false



int __number_of_iterations_10;
int __counter_10 = 0;
int __steady_10 = 0;
int __tmp_10 = 0;
int __tmp2_10 = 0;
int *__state_flag_10 = NULL;
thread_info *__thread_10 = NULL;



float i__667__10 = 0.0f;
float data__668__10 = 0.0f;
void save_peek_buffer__10(object_write_buffer *buf);
void load_peek_buffer__10(object_write_buffer *buf);
void save_file_pointer__10(object_write_buffer *buf);
void load_file_pointer__10(object_write_buffer *buf);

inline void check_status__10() {
  check_thread_status(__state_flag_10, __thread_10);
}

void check_status_during_io__10() {
  check_thread_status_during_io(__state_flag_10, __thread_10);
}

void __init_thread_info_10(thread_info *info) {
  __state_flag_10 = info->get_state_flag();
}

thread_info *__get_thread_info_10() {
  if (__thread_10 != NULL) return __thread_10;
  __thread_10 = new thread_info(10, check_status_during_io__10);
  __init_thread_info_10(__thread_10);
  return __thread_10;
}

void __declare_sockets_10() {
  init_instance::add_incoming(9,10, DATA_SOCKET);
}

void __init_sockets_10(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_10() {
}

void __peek_sockets_10() {
}

 
void init_SecondCounter__671_187__10();
inline void check_status__10();

void work_SecondCounter__671_187__10(int);


inline float __pop_9_10() {
float res=BUFFER_9_10[TAIL_9_10];
TAIL_9_10++;
return res;
}

inline void __pop_9_10(int n) {
TAIL_9_10+=n;
}

inline float __peek_9_10(int offs) {
return BUFFER_9_10[TAIL_9_10+offs];
}


 
void init_SecondCounter__671_187__10(){
  ((i__667__10) = ((float)0.0))/*float*/;
}
void save_file_pointer__10(object_write_buffer *buf) {}
void load_file_pointer__10(object_write_buffer *buf) {}
 
void work_SecondCounter__671_187__10(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter SecondCounter

      (data__668__10) = BUFFER_9_10[TAIL_9_10]; TAIL_9_10++;
;

      // TIMER_PRINT_CODE: __print_sink__ += (int)((data__668__10)); 

      ((i__667__10)++);
      // mark end: SIRFilter SecondCounter

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_100;
int __counter_100 = 0;
int __steady_100 = 0;
int __tmp_100 = 0;
int __tmp2_100 = 0;
int *__state_flag_100 = NULL;
thread_info *__thread_100 = NULL;



inline void check_status__100() {
  check_thread_status(__state_flag_100, __thread_100);
}

void check_status_during_io__100() {
  check_thread_status_during_io(__state_flag_100, __thread_100);
}

void __init_thread_info_100(thread_info *info) {
  __state_flag_100 = info->get_state_flag();
}

thread_info *__get_thread_info_100() {
  if (__thread_100 != NULL) return __thread_100;
  __thread_100 = new thread_info(100, check_status_during_io__100);
  __init_thread_info_100(__thread_100);
  return __thread_100;
}

void __declare_sockets_100() {
  init_instance::add_incoming(99,100, DATA_SOCKET);
  init_instance::add_incoming(102,100, DATA_SOCKET);
  init_instance::add_outgoing(100,101, DATA_SOCKET);
}

void __init_sockets_100(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_100() {
}

void __peek_sockets_100() {
}


inline void __push_100_101(float data) {
BUFFER_100_101[HEAD_100_101]=data;
HEAD_100_101++;
}


inline float __pop_99_100() {
float res=BUFFER_99_100[TAIL_99_100];
TAIL_99_100++;
return res;
}

inline float __pop_102_100() {
float res=BUFFER_102_100[TAIL_102_100];
TAIL_102_100++;
return res;
}


void __joiner_100_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_100_101(__pop_99_100());
    __push_100_101(__pop_102_100());
    __push_100_101(__pop_102_100());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_101;
int __counter_101 = 0;
int __steady_101 = 0;
int __tmp_101 = 0;
int __tmp2_101 = 0;
int *__state_flag_101 = NULL;
thread_info *__thread_101 = NULL;



float numer__574__101 = 0.0f;
float denom_x__575__101 = 0.0f;
float denom_y__576__101 = 0.0f;
float denom__577__101 = 0.0f;
void save_peek_buffer__101(object_write_buffer *buf);
void load_peek_buffer__101(object_write_buffer *buf);
void save_file_pointer__101(object_write_buffer *buf);
void load_file_pointer__101(object_write_buffer *buf);

inline void check_status__101() {
  check_thread_status(__state_flag_101, __thread_101);
}

void check_status_during_io__101() {
  check_thread_status_during_io(__state_flag_101, __thread_101);
}

void __init_thread_info_101(thread_info *info) {
  __state_flag_101 = info->get_state_flag();
}

thread_info *__get_thread_info_101() {
  if (__thread_101 != NULL) return __thread_101;
  __thread_101 = new thread_info(101, check_status_during_io__101);
  __init_thread_info_101(__thread_101);
  return __thread_101;
}

void __declare_sockets_101() {
  init_instance::add_incoming(100,101, DATA_SOCKET);
  init_instance::add_outgoing(101,9, DATA_SOCKET);
}

void __init_sockets_101(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_101() {
}

void __peek_sockets_101() {
}

 
void init_Division__584_170__101();
inline void check_status__101();

void work_Division__584_170__101(int);


inline float __pop_100_101() {
float res=BUFFER_100_101[TAIL_100_101];
TAIL_100_101++;
return res;
}

inline void __pop_100_101(int n) {
TAIL_100_101+=n;
}

inline float __peek_100_101(int offs) {
return BUFFER_100_101[TAIL_100_101+offs];
}



inline void __push_101_9(float data) {
BUFFER_101_9[HEAD_101_9]=data;
HEAD_101_9++;
}



 
void init_Division__584_170__101(){
}
void save_file_pointer__101(object_write_buffer *buf) {}
void load_file_pointer__101(object_write_buffer *buf) {}
 
void work_Division__584_170__101(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__580 = 0.0f;/* float */
      float __tmp4__581 = 0.0f;/* float */
      double __tmp5__582 = 0.0f;/* double */
      double __tmp6__583 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__580 = ((float)0.0))/*float*/;
      (denom_x__575__101) = BUFFER_100_101[TAIL_100_101]; TAIL_100_101++;
;
      (denom_y__576__101) = BUFFER_100_101[TAIL_100_101]; TAIL_100_101++;
;
      (__tmp6__583 = ((double)(((denom_x__575__101) * (denom_y__576__101)))))/*double*/;
      (__tmp5__582 = sqrtf(__tmp6__583))/*double*/;
      (__tmp4__581 = ((float)(__tmp5__582)))/*float*/;
      ((denom__577__101) = __tmp4__581)/*float*/;
      (numer__574__101) = BUFFER_100_101[TAIL_100_101]; TAIL_100_101++;
;

      if (((denom__577__101) != ((float)0.0))) {(v__580 = ((numer__574__101) / (denom__577__101)))/*float*/;
      } else {}
      __push_101_9(v__580);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_102;
int __counter_102 = 0;
int __steady_102 = 0;
int __tmp_102 = 0;
int __tmp2_102 = 0;
int *__state_flag_102 = NULL;
thread_info *__thread_102 = NULL;



float _TheGlobal__pattern__6____102[256] = {0};
float numerSum__566__102 = 0.0f;
float denomPatternSum__567__102 = 0.0f;
float data__568__102 = 0.0f;
float patternData__569__102 = 0.0f;
int i__570__102 = 0;
void save_peek_buffer__102(object_write_buffer *buf);
void load_peek_buffer__102(object_write_buffer *buf);
void save_file_pointer__102(object_write_buffer *buf);
void load_file_pointer__102(object_write_buffer *buf);

inline void check_status__102() {
  check_thread_status(__state_flag_102, __thread_102);
}

void check_status_during_io__102() {
  check_thread_status_during_io(__state_flag_102, __thread_102);
}

void __init_thread_info_102(thread_info *info) {
  __state_flag_102 = info->get_state_flag();
}

thread_info *__get_thread_info_102() {
  if (__thread_102 != NULL) return __thread_102;
  __thread_102 = new thread_info(102, check_status_during_io__102);
  __init_thread_info_102(__thread_102);
  return __thread_102;
}

void __declare_sockets_102() {
  init_instance::add_incoming(98,102, DATA_SOCKET);
  init_instance::add_outgoing(102,100, DATA_SOCKET);
}

void __init_sockets_102(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_102() {
}

void __peek_sockets_102() {
}

 
void init_PatternSums__573_169__102();
inline void check_status__102();

void work_PatternSums__573_169__102(int);


inline float __pop_98_102() {
float res=BUFFER_98_102[TAIL_98_102];
TAIL_98_102++;
return res;
}

inline void __pop_98_102(int n) {
TAIL_98_102+=n;
}

inline float __peek_98_102(int offs) {
return BUFFER_98_102[TAIL_98_102+offs];
}



inline void __push_102_100(float data) {
BUFFER_102_100[HEAD_102_100]=data;
HEAD_102_100++;
}



 
void init_PatternSums__573_169__102(){
  (((_TheGlobal__pattern__6____102)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____102)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__102(object_write_buffer *buf) {}
void load_file_pointer__102(object_write_buffer *buf) {}
 
void work_PatternSums__573_169__102(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__566__102) = ((float)0.0))/*float*/;
      ((denomPatternSum__567__102) = ((float)0.0))/*float*/;
      for (((i__570__102) = 0)/*int*/; ((i__570__102) < 256); ((i__570__102)++)) {{
          ((patternData__569__102) = (((_TheGlobal__pattern__6____102)[(int)(i__570__102)]) - ((float)1.6013207E-4)))/*float*/;
          (data__568__102) = BUFFER_98_102[TAIL_98_102]; TAIL_98_102++;
;
          ((numerSum__566__102) = ((numerSum__566__102) + ((data__568__102) * (patternData__569__102))))/*float*/;
          ((denomPatternSum__567__102) = ((denomPatternSum__567__102) + ((patternData__569__102) * (patternData__569__102))))/*float*/;
        }
      }
      __push_102_100((denomPatternSum__567__102));
      __push_102_100((numerSum__566__102));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_103;
int __counter_103 = 0;
int __steady_103 = 0;
int __tmp_103 = 0;
int __tmp2_103 = 0;
int *__state_flag_103 = NULL;
thread_info *__thread_103 = NULL;



void save_peek_buffer__103(object_write_buffer *buf);
void load_peek_buffer__103(object_write_buffer *buf);
void save_file_pointer__103(object_write_buffer *buf);
void load_file_pointer__103(object_write_buffer *buf);

inline void check_status__103() {
  check_thread_status(__state_flag_103, __thread_103);
}

void check_status_during_io__103() {
  check_thread_status_during_io(__state_flag_103, __thread_103);
}

void __init_thread_info_103(thread_info *info) {
  __state_flag_103 = info->get_state_flag();
}

thread_info *__get_thread_info_103() {
  if (__thread_103 != NULL) return __thread_103;
  __thread_103 = new thread_info(103, check_status_during_io__103);
  __init_thread_info_103(__thread_103);
  return __thread_103;
}

void __declare_sockets_103() {
  init_instance::add_incoming(2,103, DATA_SOCKET);
  init_instance::add_outgoing(103,104, DATA_SOCKET);
}

void __init_sockets_103(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_103() {
}

void __peek_sockets_103() {
}

 
void init_GetAvg__591_172__103();
inline void check_status__103();

void work_GetAvg__591_172__103(int);


inline float __pop_2_103() {
float res=BUFFER_2_103[TAIL_2_103];
TAIL_2_103++;
return res;
}

inline void __pop_2_103(int n) {
TAIL_2_103+=n;
}

inline float __peek_2_103(int offs) {
return BUFFER_2_103[TAIL_2_103+offs];
}



inline void __push_103_104(float data) {
BUFFER_103_104[HEAD_103_104]=data;
HEAD_103_104++;
}



 
void init_GetAvg__591_172__103(){
}
void save_file_pointer__103(object_write_buffer *buf) {}
void load_file_pointer__103(object_write_buffer *buf) {}
 
void work_GetAvg__591_172__103(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__587 = 0;/* int */
      float Sum__588 = 0.0f;/* float */
      float val__589 = 0.0f;/* float */
      float __tmp8__590 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__588 = ((float)0.0))/*float*/;
      for ((i__587 = 0)/*int*/; (i__587 < 256); (i__587++)) {{
          val__589 = BUFFER_2_103[TAIL_2_103]; TAIL_2_103++;
;
          (Sum__588 = (Sum__588 + val__589))/*float*/;
          __push_103_104(val__589);
        }
      }
      (__tmp8__590 = (Sum__588 / ((float)256.0)))/*float*/;
      __push_103_104(__tmp8__590);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_104;
int __counter_104 = 0;
int __steady_104 = 0;
int __tmp_104 = 0;
int __tmp2_104 = 0;
int *__state_flag_104 = NULL;
thread_info *__thread_104 = NULL;



float avg__592__104 = 0.0f;
int i__593__104 = 0;
void save_peek_buffer__104(object_write_buffer *buf);
void load_peek_buffer__104(object_write_buffer *buf);
void save_file_pointer__104(object_write_buffer *buf);
void load_file_pointer__104(object_write_buffer *buf);

inline void check_status__104() {
  check_thread_status(__state_flag_104, __thread_104);
}

void check_status_during_io__104() {
  check_thread_status_during_io(__state_flag_104, __thread_104);
}

void __init_thread_info_104(thread_info *info) {
  __state_flag_104 = info->get_state_flag();
}

thread_info *__get_thread_info_104() {
  if (__thread_104 != NULL) return __thread_104;
  __thread_104 = new thread_info(104, check_status_during_io__104);
  __init_thread_info_104(__thread_104);
  return __thread_104;
}

void __declare_sockets_104() {
  init_instance::add_incoming(103,104, DATA_SOCKET);
  init_instance::add_outgoing(104,105, DATA_SOCKET);
}

void __init_sockets_104(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_104() {
}

void __peek_sockets_104() {
}

 
void init_DataMinusAvg__599_174__104();
inline void check_status__104();

void work_DataMinusAvg__599_174__104(int);


inline float __pop_103_104() {
float res=BUFFER_103_104[TAIL_103_104];
TAIL_103_104++;
return res;
}

inline void __pop_103_104(int n) {
TAIL_103_104+=n;
}

inline float __peek_103_104(int offs) {
return BUFFER_103_104[TAIL_103_104+offs];
}



inline void __push_104_105(float data) {
BUFFER_104_105[HEAD_104_105]=data;
HEAD_104_105++;
}



 
void init_DataMinusAvg__599_174__104(){
}
void save_file_pointer__104(object_write_buffer *buf) {}
void load_file_pointer__104(object_write_buffer *buf) {}
 
void work_DataMinusAvg__599_174__104(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__596 = 0.0f;/* float */
      float v__597 = 0.0f;/* float */
      float __tmp11__598 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__596 = BUFFER_103_104[TAIL_103_104+256];
;
      ((avg__592__104) = __tmp9__596)/*float*/;
      for (((i__593__104) = 0)/*int*/; ((i__593__104) < 256); ((i__593__104)++)) {{
          v__597 = BUFFER_103_104[TAIL_103_104]; TAIL_103_104++;
;
          (__tmp11__598 = (v__597 - (avg__592__104)))/*float*/;
          __push_104_105(__tmp11__598);
        }
      }
      __pop_103_104();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_105;
int __counter_105 = 0;
int __steady_105 = 0;
int __tmp_105 = 0;
int __tmp2_105 = 0;
int *__state_flag_105 = NULL;
thread_info *__thread_105 = NULL;



inline void check_status__105() {
  check_thread_status(__state_flag_105, __thread_105);
}

void check_status_during_io__105() {
  check_thread_status_during_io(__state_flag_105, __thread_105);
}

void __init_thread_info_105(thread_info *info) {
  __state_flag_105 = info->get_state_flag();
}

thread_info *__get_thread_info_105() {
  if (__thread_105 != NULL) return __thread_105;
  __thread_105 = new thread_info(105, check_status_during_io__105);
  __init_thread_info_105(__thread_105);
  return __thread_105;
}

void __declare_sockets_105() {
  init_instance::add_incoming(104,105, DATA_SOCKET);
  init_instance::add_outgoing(105,106, DATA_SOCKET);
  init_instance::add_outgoing(105,109, DATA_SOCKET);
}

void __init_sockets_105(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_105() {
}

void __peek_sockets_105() {
}

inline float __pop_104_105() {
float res=BUFFER_104_105[TAIL_104_105];
TAIL_104_105++;
return res;
}


inline void __push_105_106(float data) {
BUFFER_105_106[HEAD_105_106]=data;
HEAD_105_106++;
}



inline void __push_105_109(float data) {
BUFFER_105_109[HEAD_105_109]=data;
HEAD_105_109++;
}



void __splitter_105_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_104_105[TAIL_104_105]; TAIL_104_105++;
;
  __push_105_106(tmp);
  __push_105_109(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_106;
int __counter_106 = 0;
int __steady_106 = 0;
int __tmp_106 = 0;
int __tmp2_106 = 0;
int *__state_flag_106 = NULL;
thread_info *__thread_106 = NULL;



float data__600__106 = 0.0f;
int i__601__106 = 0;
void save_peek_buffer__106(object_write_buffer *buf);
void load_peek_buffer__106(object_write_buffer *buf);
void save_file_pointer__106(object_write_buffer *buf);
void load_file_pointer__106(object_write_buffer *buf);

inline void check_status__106() {
  check_thread_status(__state_flag_106, __thread_106);
}

void check_status_during_io__106() {
  check_thread_status_during_io(__state_flag_106, __thread_106);
}

void __init_thread_info_106(thread_info *info) {
  __state_flag_106 = info->get_state_flag();
}

thread_info *__get_thread_info_106() {
  if (__thread_106 != NULL) return __thread_106;
  __thread_106 = new thread_info(106, check_status_during_io__106);
  __init_thread_info_106(__thread_106);
  return __thread_106;
}

void __declare_sockets_106() {
  init_instance::add_incoming(105,106, DATA_SOCKET);
  init_instance::add_outgoing(106,107, DATA_SOCKET);
}

void __init_sockets_106(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_106() {
}

void __peek_sockets_106() {
}

 
void init_DenomDataSum__605_176__106();
inline void check_status__106();

void work_DenomDataSum__605_176__106(int);


inline float __pop_105_106() {
float res=BUFFER_105_106[TAIL_105_106];
TAIL_105_106++;
return res;
}

inline void __pop_105_106(int n) {
TAIL_105_106+=n;
}

inline float __peek_105_106(int offs) {
return BUFFER_105_106[TAIL_105_106+offs];
}



inline void __push_106_107(float data) {
BUFFER_106_107[HEAD_106_107]=data;
HEAD_106_107++;
}



 
void init_DenomDataSum__605_176__106(){
}
void save_file_pointer__106(object_write_buffer *buf) {}
void load_file_pointer__106(object_write_buffer *buf) {}
 
void work_DenomDataSum__605_176__106(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__604 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__604 = ((float)0.0))/*float*/;
      for (((i__601__106) = 0)/*int*/; ((i__601__106) < 256); ((i__601__106)++)) {{
          (data__600__106) = BUFFER_105_106[TAIL_105_106]; TAIL_105_106++;
;
          (sum__604 = (sum__604 + ((data__600__106) * (data__600__106))))/*float*/;
        }
      }
      __push_106_107(sum__604);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_107;
int __counter_107 = 0;
int __steady_107 = 0;
int __tmp_107 = 0;
int __tmp2_107 = 0;
int *__state_flag_107 = NULL;
thread_info *__thread_107 = NULL;



inline void check_status__107() {
  check_thread_status(__state_flag_107, __thread_107);
}

void check_status_during_io__107() {
  check_thread_status_during_io(__state_flag_107, __thread_107);
}

void __init_thread_info_107(thread_info *info) {
  __state_flag_107 = info->get_state_flag();
}

thread_info *__get_thread_info_107() {
  if (__thread_107 != NULL) return __thread_107;
  __thread_107 = new thread_info(107, check_status_during_io__107);
  __init_thread_info_107(__thread_107);
  return __thread_107;
}

void __declare_sockets_107() {
  init_instance::add_incoming(106,107, DATA_SOCKET);
  init_instance::add_incoming(109,107, DATA_SOCKET);
  init_instance::add_outgoing(107,108, DATA_SOCKET);
}

void __init_sockets_107(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_107() {
}

void __peek_sockets_107() {
}


inline void __push_107_108(float data) {
BUFFER_107_108[HEAD_107_108]=data;
HEAD_107_108++;
}


inline float __pop_106_107() {
float res=BUFFER_106_107[TAIL_106_107];
TAIL_106_107++;
return res;
}

inline float __pop_109_107() {
float res=BUFFER_109_107[TAIL_109_107];
TAIL_109_107++;
return res;
}


void __joiner_107_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_107_108(__pop_106_107());
    __push_107_108(__pop_109_107());
    __push_107_108(__pop_109_107());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_108;
int __counter_108 = 0;
int __steady_108 = 0;
int __tmp_108 = 0;
int __tmp2_108 = 0;
int *__state_flag_108 = NULL;
thread_info *__thread_108 = NULL;



float numer__615__108 = 0.0f;
float denom_x__616__108 = 0.0f;
float denom_y__617__108 = 0.0f;
float denom__618__108 = 0.0f;
void save_peek_buffer__108(object_write_buffer *buf);
void load_peek_buffer__108(object_write_buffer *buf);
void save_file_pointer__108(object_write_buffer *buf);
void load_file_pointer__108(object_write_buffer *buf);

inline void check_status__108() {
  check_thread_status(__state_flag_108, __thread_108);
}

void check_status_during_io__108() {
  check_thread_status_during_io(__state_flag_108, __thread_108);
}

void __init_thread_info_108(thread_info *info) {
  __state_flag_108 = info->get_state_flag();
}

thread_info *__get_thread_info_108() {
  if (__thread_108 != NULL) return __thread_108;
  __thread_108 = new thread_info(108, check_status_during_io__108);
  __init_thread_info_108(__thread_108);
  return __thread_108;
}

void __declare_sockets_108() {
  init_instance::add_incoming(107,108, DATA_SOCKET);
  init_instance::add_outgoing(108,9, DATA_SOCKET);
}

void __init_sockets_108(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_108() {
}

void __peek_sockets_108() {
}

 
void init_Division__625_178__108();
inline void check_status__108();

void work_Division__625_178__108(int);


inline float __pop_107_108() {
float res=BUFFER_107_108[TAIL_107_108];
TAIL_107_108++;
return res;
}

inline void __pop_107_108(int n) {
TAIL_107_108+=n;
}

inline float __peek_107_108(int offs) {
return BUFFER_107_108[TAIL_107_108+offs];
}



inline void __push_108_9(float data) {
BUFFER_108_9[HEAD_108_9]=data;
HEAD_108_9++;
}



 
void init_Division__625_178__108(){
}
void save_file_pointer__108(object_write_buffer *buf) {}
void load_file_pointer__108(object_write_buffer *buf) {}
 
void work_Division__625_178__108(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__621 = 0.0f;/* float */
      float __tmp4__622 = 0.0f;/* float */
      double __tmp5__623 = 0.0f;/* double */
      double __tmp6__624 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__621 = ((float)0.0))/*float*/;
      (denom_x__616__108) = BUFFER_107_108[TAIL_107_108]; TAIL_107_108++;
;
      (denom_y__617__108) = BUFFER_107_108[TAIL_107_108]; TAIL_107_108++;
;
      (__tmp6__624 = ((double)(((denom_x__616__108) * (denom_y__617__108)))))/*double*/;
      (__tmp5__623 = sqrtf(__tmp6__624))/*double*/;
      (__tmp4__622 = ((float)(__tmp5__623)))/*float*/;
      ((denom__618__108) = __tmp4__622)/*float*/;
      (numer__615__108) = BUFFER_107_108[TAIL_107_108]; TAIL_107_108++;
;

      if (((denom__618__108) != ((float)0.0))) {(v__621 = ((numer__615__108) / (denom__618__108)))/*float*/;
      } else {}
      __push_108_9(v__621);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_109;
int __counter_109 = 0;
int __steady_109 = 0;
int __tmp_109 = 0;
int __tmp2_109 = 0;
int *__state_flag_109 = NULL;
thread_info *__thread_109 = NULL;



float _TheGlobal__pattern__6____109[256] = {0};
float numerSum__607__109 = 0.0f;
float denomPatternSum__608__109 = 0.0f;
float data__609__109 = 0.0f;
float patternData__610__109 = 0.0f;
int i__611__109 = 0;
void save_peek_buffer__109(object_write_buffer *buf);
void load_peek_buffer__109(object_write_buffer *buf);
void save_file_pointer__109(object_write_buffer *buf);
void load_file_pointer__109(object_write_buffer *buf);

inline void check_status__109() {
  check_thread_status(__state_flag_109, __thread_109);
}

void check_status_during_io__109() {
  check_thread_status_during_io(__state_flag_109, __thread_109);
}

void __init_thread_info_109(thread_info *info) {
  __state_flag_109 = info->get_state_flag();
}

thread_info *__get_thread_info_109() {
  if (__thread_109 != NULL) return __thread_109;
  __thread_109 = new thread_info(109, check_status_during_io__109);
  __init_thread_info_109(__thread_109);
  return __thread_109;
}

void __declare_sockets_109() {
  init_instance::add_incoming(105,109, DATA_SOCKET);
  init_instance::add_outgoing(109,107, DATA_SOCKET);
}

void __init_sockets_109(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_109() {
}

void __peek_sockets_109() {
}

 
void init_PatternSums__614_177__109();
inline void check_status__109();

void work_PatternSums__614_177__109(int);


inline float __pop_105_109() {
float res=BUFFER_105_109[TAIL_105_109];
TAIL_105_109++;
return res;
}

inline void __pop_105_109(int n) {
TAIL_105_109+=n;
}

inline float __peek_105_109(int offs) {
return BUFFER_105_109[TAIL_105_109+offs];
}



inline void __push_109_107(float data) {
BUFFER_109_107[HEAD_109_107]=data;
HEAD_109_107++;
}



 
void init_PatternSums__614_177__109(){
  (((_TheGlobal__pattern__6____109)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____109)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__109(object_write_buffer *buf) {}
void load_file_pointer__109(object_write_buffer *buf) {}
 
void work_PatternSums__614_177__109(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__607__109) = ((float)0.0))/*float*/;
      ((denomPatternSum__608__109) = ((float)0.0))/*float*/;
      for (((i__611__109) = 0)/*int*/; ((i__611__109) < 256); ((i__611__109)++)) {{
          ((patternData__610__109) = (((_TheGlobal__pattern__6____109)[(int)(i__611__109)]) - ((float)1.6013207E-4)))/*float*/;
          (data__609__109) = BUFFER_105_109[TAIL_105_109]; TAIL_105_109++;
;
          ((numerSum__607__109) = ((numerSum__607__109) + ((data__609__109) * (patternData__610__109))))/*float*/;
          ((denomPatternSum__608__109) = ((denomPatternSum__608__109) + ((patternData__610__109) * (patternData__610__109))))/*float*/;
        }
      }
      __push_109_107((denomPatternSum__608__109));
      __push_109_107((numerSum__607__109));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_11;
int __counter_11 = 0;
int __steady_11 = 0;
int __tmp_11 = 0;
int __tmp2_11 = 0;
int *__state_flag_11 = NULL;
thread_info *__thread_11 = NULL;



float _TheGlobal__pattern__6____11[256] = {0};
float numerSum__33__11 = 0.0f;
float denomPatternSum__34__11 = 0.0f;
float data__35__11 = 0.0f;
float patternData__36__11 = 0.0f;
int i__37__11 = 0;
void save_peek_buffer__11(object_write_buffer *buf);
void load_peek_buffer__11(object_write_buffer *buf);
void save_file_pointer__11(object_write_buffer *buf);
void load_file_pointer__11(object_write_buffer *buf);

inline void check_status__11() {
  check_thread_status(__state_flag_11, __thread_11);
}

void check_status_during_io__11() {
  check_thread_status_during_io(__state_flag_11, __thread_11);
}

void __init_thread_info_11(thread_info *info) {
  __state_flag_11 = info->get_state_flag();
}

thread_info *__get_thread_info_11() {
  if (__thread_11 != NULL) return __thread_11;
  __thread_11 = new thread_info(11, check_status_during_io__11);
  __init_thread_info_11(__thread_11);
  return __thread_11;
}

void __declare_sockets_11() {
  init_instance::add_incoming(5,11, DATA_SOCKET);
  init_instance::add_outgoing(11,7, DATA_SOCKET);
}

void __init_sockets_11(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_11() {
}

void __peek_sockets_11() {
}

 
void init_PatternSums__40_65__11();
inline void check_status__11();

void work_PatternSums__40_65__11(int);


inline float __pop_5_11() {
float res=BUFFER_5_11[TAIL_5_11];
TAIL_5_11++;
return res;
}

inline void __pop_5_11(int n) {
TAIL_5_11+=n;
}

inline float __peek_5_11(int offs) {
return BUFFER_5_11[TAIL_5_11+offs];
}



inline void __push_11_7(float data) {
BUFFER_11_7[HEAD_11_7]=data;
HEAD_11_7++;
}



 
void init_PatternSums__40_65__11(){
  (((_TheGlobal__pattern__6____11)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____11)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__11(object_write_buffer *buf) {}
void load_file_pointer__11(object_write_buffer *buf) {}
 
void work_PatternSums__40_65__11(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__33__11) = ((float)0.0))/*float*/;
      ((denomPatternSum__34__11) = ((float)0.0))/*float*/;
      for (((i__37__11) = 0)/*int*/; ((i__37__11) < 256); ((i__37__11)++)) {{
          ((patternData__36__11) = (((_TheGlobal__pattern__6____11)[(int)(i__37__11)]) - ((float)1.6013207E-4)))/*float*/;
          (data__35__11) = BUFFER_5_11[TAIL_5_11]; TAIL_5_11++;
;
          ((numerSum__33__11) = ((numerSum__33__11) + ((data__35__11) * (patternData__36__11))))/*float*/;
          ((denomPatternSum__34__11) = ((denomPatternSum__34__11) + ((patternData__36__11) * (patternData__36__11))))/*float*/;
        }
      }
      __push_11_7((denomPatternSum__34__11));
      __push_11_7((numerSum__33__11));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_110;
int __counter_110 = 0;
int __steady_110 = 0;
int __tmp_110 = 0;
int __tmp2_110 = 0;
int *__state_flag_110 = NULL;
thread_info *__thread_110 = NULL;



void save_peek_buffer__110(object_write_buffer *buf);
void load_peek_buffer__110(object_write_buffer *buf);
void save_file_pointer__110(object_write_buffer *buf);
void load_file_pointer__110(object_write_buffer *buf);

inline void check_status__110() {
  check_thread_status(__state_flag_110, __thread_110);
}

void check_status_during_io__110() {
  check_thread_status_during_io(__state_flag_110, __thread_110);
}

void __init_thread_info_110(thread_info *info) {
  __state_flag_110 = info->get_state_flag();
}

thread_info *__get_thread_info_110() {
  if (__thread_110 != NULL) return __thread_110;
  __thread_110 = new thread_info(110, check_status_during_io__110);
  __init_thread_info_110(__thread_110);
  return __thread_110;
}

void __declare_sockets_110() {
  init_instance::add_incoming(2,110, DATA_SOCKET);
  init_instance::add_outgoing(110,111, DATA_SOCKET);
}

void __init_sockets_110(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_110() {
}

void __peek_sockets_110() {
}

 
void init_GetAvg__632_180__110();
inline void check_status__110();

void work_GetAvg__632_180__110(int);


inline float __pop_2_110() {
float res=BUFFER_2_110[TAIL_2_110];
TAIL_2_110++;
return res;
}

inline void __pop_2_110(int n) {
TAIL_2_110+=n;
}

inline float __peek_2_110(int offs) {
return BUFFER_2_110[TAIL_2_110+offs];
}



inline void __push_110_111(float data) {
BUFFER_110_111[HEAD_110_111]=data;
HEAD_110_111++;
}



 
void init_GetAvg__632_180__110(){
}
void save_file_pointer__110(object_write_buffer *buf) {}
void load_file_pointer__110(object_write_buffer *buf) {}
 
void work_GetAvg__632_180__110(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__628 = 0;/* int */
      float Sum__629 = 0.0f;/* float */
      float val__630 = 0.0f;/* float */
      float __tmp8__631 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__629 = ((float)0.0))/*float*/;
      for ((i__628 = 0)/*int*/; (i__628 < 256); (i__628++)) {{
          val__630 = BUFFER_2_110[TAIL_2_110]; TAIL_2_110++;
;
          (Sum__629 = (Sum__629 + val__630))/*float*/;
          __push_110_111(val__630);
        }
      }
      (__tmp8__631 = (Sum__629 / ((float)256.0)))/*float*/;
      __push_110_111(__tmp8__631);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_111;
int __counter_111 = 0;
int __steady_111 = 0;
int __tmp_111 = 0;
int __tmp2_111 = 0;
int *__state_flag_111 = NULL;
thread_info *__thread_111 = NULL;



float avg__633__111 = 0.0f;
int i__634__111 = 0;
void save_peek_buffer__111(object_write_buffer *buf);
void load_peek_buffer__111(object_write_buffer *buf);
void save_file_pointer__111(object_write_buffer *buf);
void load_file_pointer__111(object_write_buffer *buf);

inline void check_status__111() {
  check_thread_status(__state_flag_111, __thread_111);
}

void check_status_during_io__111() {
  check_thread_status_during_io(__state_flag_111, __thread_111);
}

void __init_thread_info_111(thread_info *info) {
  __state_flag_111 = info->get_state_flag();
}

thread_info *__get_thread_info_111() {
  if (__thread_111 != NULL) return __thread_111;
  __thread_111 = new thread_info(111, check_status_during_io__111);
  __init_thread_info_111(__thread_111);
  return __thread_111;
}

void __declare_sockets_111() {
  init_instance::add_incoming(110,111, DATA_SOCKET);
  init_instance::add_outgoing(111,112, DATA_SOCKET);
}

void __init_sockets_111(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_111() {
}

void __peek_sockets_111() {
}

 
void init_DataMinusAvg__640_182__111();
inline void check_status__111();

void work_DataMinusAvg__640_182__111(int);


inline float __pop_110_111() {
float res=BUFFER_110_111[TAIL_110_111];
TAIL_110_111++;
return res;
}

inline void __pop_110_111(int n) {
TAIL_110_111+=n;
}

inline float __peek_110_111(int offs) {
return BUFFER_110_111[TAIL_110_111+offs];
}



inline void __push_111_112(float data) {
BUFFER_111_112[HEAD_111_112]=data;
HEAD_111_112++;
}



 
void init_DataMinusAvg__640_182__111(){
}
void save_file_pointer__111(object_write_buffer *buf) {}
void load_file_pointer__111(object_write_buffer *buf) {}
 
void work_DataMinusAvg__640_182__111(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__637 = 0.0f;/* float */
      float v__638 = 0.0f;/* float */
      float __tmp11__639 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__637 = BUFFER_110_111[TAIL_110_111+256];
;
      ((avg__633__111) = __tmp9__637)/*float*/;
      for (((i__634__111) = 0)/*int*/; ((i__634__111) < 256); ((i__634__111)++)) {{
          v__638 = BUFFER_110_111[TAIL_110_111]; TAIL_110_111++;
;
          (__tmp11__639 = (v__638 - (avg__633__111)))/*float*/;
          __push_111_112(__tmp11__639);
        }
      }
      __pop_110_111();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_112;
int __counter_112 = 0;
int __steady_112 = 0;
int __tmp_112 = 0;
int __tmp2_112 = 0;
int *__state_flag_112 = NULL;
thread_info *__thread_112 = NULL;



inline void check_status__112() {
  check_thread_status(__state_flag_112, __thread_112);
}

void check_status_during_io__112() {
  check_thread_status_during_io(__state_flag_112, __thread_112);
}

void __init_thread_info_112(thread_info *info) {
  __state_flag_112 = info->get_state_flag();
}

thread_info *__get_thread_info_112() {
  if (__thread_112 != NULL) return __thread_112;
  __thread_112 = new thread_info(112, check_status_during_io__112);
  __init_thread_info_112(__thread_112);
  return __thread_112;
}

void __declare_sockets_112() {
  init_instance::add_incoming(111,112, DATA_SOCKET);
  init_instance::add_outgoing(112,113, DATA_SOCKET);
  init_instance::add_outgoing(112,116, DATA_SOCKET);
}

void __init_sockets_112(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_112() {
}

void __peek_sockets_112() {
}

inline float __pop_111_112() {
float res=BUFFER_111_112[TAIL_111_112];
TAIL_111_112++;
return res;
}


inline void __push_112_113(float data) {
BUFFER_112_113[HEAD_112_113]=data;
HEAD_112_113++;
}



inline void __push_112_116(float data) {
BUFFER_112_116[HEAD_112_116]=data;
HEAD_112_116++;
}



void __splitter_112_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_111_112[TAIL_111_112]; TAIL_111_112++;
;
  __push_112_113(tmp);
  __push_112_116(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_113;
int __counter_113 = 0;
int __steady_113 = 0;
int __tmp_113 = 0;
int __tmp2_113 = 0;
int *__state_flag_113 = NULL;
thread_info *__thread_113 = NULL;



float data__641__113 = 0.0f;
int i__642__113 = 0;
void save_peek_buffer__113(object_write_buffer *buf);
void load_peek_buffer__113(object_write_buffer *buf);
void save_file_pointer__113(object_write_buffer *buf);
void load_file_pointer__113(object_write_buffer *buf);

inline void check_status__113() {
  check_thread_status(__state_flag_113, __thread_113);
}

void check_status_during_io__113() {
  check_thread_status_during_io(__state_flag_113, __thread_113);
}

void __init_thread_info_113(thread_info *info) {
  __state_flag_113 = info->get_state_flag();
}

thread_info *__get_thread_info_113() {
  if (__thread_113 != NULL) return __thread_113;
  __thread_113 = new thread_info(113, check_status_during_io__113);
  __init_thread_info_113(__thread_113);
  return __thread_113;
}

void __declare_sockets_113() {
  init_instance::add_incoming(112,113, DATA_SOCKET);
  init_instance::add_outgoing(113,114, DATA_SOCKET);
}

void __init_sockets_113(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_113() {
}

void __peek_sockets_113() {
}

 
void init_DenomDataSum__646_184__113();
inline void check_status__113();

void work_DenomDataSum__646_184__113(int);


inline float __pop_112_113() {
float res=BUFFER_112_113[TAIL_112_113];
TAIL_112_113++;
return res;
}

inline void __pop_112_113(int n) {
TAIL_112_113+=n;
}

inline float __peek_112_113(int offs) {
return BUFFER_112_113[TAIL_112_113+offs];
}



inline void __push_113_114(float data) {
BUFFER_113_114[HEAD_113_114]=data;
HEAD_113_114++;
}



 
void init_DenomDataSum__646_184__113(){
}
void save_file_pointer__113(object_write_buffer *buf) {}
void load_file_pointer__113(object_write_buffer *buf) {}
 
void work_DenomDataSum__646_184__113(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__645 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__645 = ((float)0.0))/*float*/;
      for (((i__642__113) = 0)/*int*/; ((i__642__113) < 256); ((i__642__113)++)) {{
          (data__641__113) = BUFFER_112_113[TAIL_112_113]; TAIL_112_113++;
;
          (sum__645 = (sum__645 + ((data__641__113) * (data__641__113))))/*float*/;
        }
      }
      __push_113_114(sum__645);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_114;
int __counter_114 = 0;
int __steady_114 = 0;
int __tmp_114 = 0;
int __tmp2_114 = 0;
int *__state_flag_114 = NULL;
thread_info *__thread_114 = NULL;



inline void check_status__114() {
  check_thread_status(__state_flag_114, __thread_114);
}

void check_status_during_io__114() {
  check_thread_status_during_io(__state_flag_114, __thread_114);
}

void __init_thread_info_114(thread_info *info) {
  __state_flag_114 = info->get_state_flag();
}

thread_info *__get_thread_info_114() {
  if (__thread_114 != NULL) return __thread_114;
  __thread_114 = new thread_info(114, check_status_during_io__114);
  __init_thread_info_114(__thread_114);
  return __thread_114;
}

void __declare_sockets_114() {
  init_instance::add_incoming(113,114, DATA_SOCKET);
  init_instance::add_incoming(116,114, DATA_SOCKET);
  init_instance::add_outgoing(114,115, DATA_SOCKET);
}

void __init_sockets_114(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_114() {
}

void __peek_sockets_114() {
}


inline void __push_114_115(float data) {
BUFFER_114_115[HEAD_114_115]=data;
HEAD_114_115++;
}


inline float __pop_113_114() {
float res=BUFFER_113_114[TAIL_113_114];
TAIL_113_114++;
return res;
}

inline float __pop_116_114() {
float res=BUFFER_116_114[TAIL_116_114];
TAIL_116_114++;
return res;
}


void __joiner_114_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_114_115(__pop_113_114());
    __push_114_115(__pop_116_114());
    __push_114_115(__pop_116_114());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_115;
int __counter_115 = 0;
int __steady_115 = 0;
int __tmp_115 = 0;
int __tmp2_115 = 0;
int *__state_flag_115 = NULL;
thread_info *__thread_115 = NULL;



float numer__656__115 = 0.0f;
float denom_x__657__115 = 0.0f;
float denom_y__658__115 = 0.0f;
float denom__659__115 = 0.0f;
void save_peek_buffer__115(object_write_buffer *buf);
void load_peek_buffer__115(object_write_buffer *buf);
void save_file_pointer__115(object_write_buffer *buf);
void load_file_pointer__115(object_write_buffer *buf);

inline void check_status__115() {
  check_thread_status(__state_flag_115, __thread_115);
}

void check_status_during_io__115() {
  check_thread_status_during_io(__state_flag_115, __thread_115);
}

void __init_thread_info_115(thread_info *info) {
  __state_flag_115 = info->get_state_flag();
}

thread_info *__get_thread_info_115() {
  if (__thread_115 != NULL) return __thread_115;
  __thread_115 = new thread_info(115, check_status_during_io__115);
  __init_thread_info_115(__thread_115);
  return __thread_115;
}

void __declare_sockets_115() {
  init_instance::add_incoming(114,115, DATA_SOCKET);
  init_instance::add_outgoing(115,9, DATA_SOCKET);
}

void __init_sockets_115(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_115() {
}

void __peek_sockets_115() {
}

 
void init_Division__666_186__115();
inline void check_status__115();

void work_Division__666_186__115(int);


inline float __pop_114_115() {
float res=BUFFER_114_115[TAIL_114_115];
TAIL_114_115++;
return res;
}

inline void __pop_114_115(int n) {
TAIL_114_115+=n;
}

inline float __peek_114_115(int offs) {
return BUFFER_114_115[TAIL_114_115+offs];
}



inline void __push_115_9(float data) {
BUFFER_115_9[HEAD_115_9]=data;
HEAD_115_9++;
}



 
void init_Division__666_186__115(){
}
void save_file_pointer__115(object_write_buffer *buf) {}
void load_file_pointer__115(object_write_buffer *buf) {}
 
void work_Division__666_186__115(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__662 = 0.0f;/* float */
      float __tmp4__663 = 0.0f;/* float */
      double __tmp5__664 = 0.0f;/* double */
      double __tmp6__665 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__662 = ((float)0.0))/*float*/;
      (denom_x__657__115) = BUFFER_114_115[TAIL_114_115]; TAIL_114_115++;
;
      (denom_y__658__115) = BUFFER_114_115[TAIL_114_115]; TAIL_114_115++;
;
      (__tmp6__665 = ((double)(((denom_x__657__115) * (denom_y__658__115)))))/*double*/;
      (__tmp5__664 = sqrtf(__tmp6__665))/*double*/;
      (__tmp4__663 = ((float)(__tmp5__664)))/*float*/;
      ((denom__659__115) = __tmp4__663)/*float*/;
      (numer__656__115) = BUFFER_114_115[TAIL_114_115]; TAIL_114_115++;
;

      if (((denom__659__115) != ((float)0.0))) {(v__662 = ((numer__656__115) / (denom__659__115)))/*float*/;
      } else {}
      __push_115_9(v__662);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_116;
int __counter_116 = 0;
int __steady_116 = 0;
int __tmp_116 = 0;
int __tmp2_116 = 0;
int *__state_flag_116 = NULL;
thread_info *__thread_116 = NULL;



float _TheGlobal__pattern__6____116[256] = {0};
float numerSum__648__116 = 0.0f;
float denomPatternSum__649__116 = 0.0f;
float data__650__116 = 0.0f;
float patternData__651__116 = 0.0f;
int i__652__116 = 0;
void save_peek_buffer__116(object_write_buffer *buf);
void load_peek_buffer__116(object_write_buffer *buf);
void save_file_pointer__116(object_write_buffer *buf);
void load_file_pointer__116(object_write_buffer *buf);

inline void check_status__116() {
  check_thread_status(__state_flag_116, __thread_116);
}

void check_status_during_io__116() {
  check_thread_status_during_io(__state_flag_116, __thread_116);
}

void __init_thread_info_116(thread_info *info) {
  __state_flag_116 = info->get_state_flag();
}

thread_info *__get_thread_info_116() {
  if (__thread_116 != NULL) return __thread_116;
  __thread_116 = new thread_info(116, check_status_during_io__116);
  __init_thread_info_116(__thread_116);
  return __thread_116;
}

void __declare_sockets_116() {
  init_instance::add_incoming(112,116, DATA_SOCKET);
  init_instance::add_outgoing(116,114, DATA_SOCKET);
}

void __init_sockets_116(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_116() {
}

void __peek_sockets_116() {
}

 
void init_PatternSums__655_185__116();
inline void check_status__116();

void work_PatternSums__655_185__116(int);


inline float __pop_112_116() {
float res=BUFFER_112_116[TAIL_112_116];
TAIL_112_116++;
return res;
}

inline void __pop_112_116(int n) {
TAIL_112_116+=n;
}

inline float __peek_112_116(int offs) {
return BUFFER_112_116[TAIL_112_116+offs];
}



inline void __push_116_114(float data) {
BUFFER_116_114[HEAD_116_114]=data;
HEAD_116_114++;
}



 
void init_PatternSums__655_185__116(){
  (((_TheGlobal__pattern__6____116)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____116)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__116(object_write_buffer *buf) {}
void load_file_pointer__116(object_write_buffer *buf) {}
 
void work_PatternSums__655_185__116(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__648__116) = ((float)0.0))/*float*/;
      ((denomPatternSum__649__116) = ((float)0.0))/*float*/;
      for (((i__652__116) = 0)/*int*/; ((i__652__116) < 256); ((i__652__116)++)) {{
          ((patternData__651__116) = (((_TheGlobal__pattern__6____116)[(int)(i__652__116)]) - ((float)1.6013207E-4)))/*float*/;
          (data__650__116) = BUFFER_112_116[TAIL_112_116]; TAIL_112_116++;
;
          ((numerSum__648__116) = ((numerSum__648__116) + ((data__650__116) * (patternData__651__116))))/*float*/;
          ((denomPatternSum__649__116) = ((denomPatternSum__649__116) + ((patternData__651__116) * (patternData__651__116))))/*float*/;
        }
      }
      __push_116_114((denomPatternSum__649__116));
      __push_116_114((numerSum__648__116));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_12;
int __counter_12 = 0;
int __steady_12 = 0;
int __tmp_12 = 0;
int __tmp2_12 = 0;
int *__state_flag_12 = NULL;
thread_info *__thread_12 = NULL;



void save_peek_buffer__12(object_write_buffer *buf);
void load_peek_buffer__12(object_write_buffer *buf);
void save_file_pointer__12(object_write_buffer *buf);
void load_file_pointer__12(object_write_buffer *buf);

inline void check_status__12() {
  check_thread_status(__state_flag_12, __thread_12);
}

void check_status_during_io__12() {
  check_thread_status_during_io(__state_flag_12, __thread_12);
}

void __init_thread_info_12(thread_info *info) {
  __state_flag_12 = info->get_state_flag();
}

thread_info *__get_thread_info_12() {
  if (__thread_12 != NULL) return __thread_12;
  __thread_12 = new thread_info(12, check_status_during_io__12);
  __init_thread_info_12(__thread_12);
  return __thread_12;
}

void __declare_sockets_12() {
  init_instance::add_incoming(2,12, DATA_SOCKET);
  init_instance::add_outgoing(12,13, DATA_SOCKET);
}

void __init_sockets_12(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_12() {
}

void __peek_sockets_12() {
}

 
void init_GetAvg__58_68__12();
inline void check_status__12();

void work_GetAvg__58_68__12(int);


inline float __pop_2_12() {
float res=BUFFER_2_12[TAIL_2_12];
TAIL_2_12++;
return res;
}

inline void __pop_2_12(int n) {
TAIL_2_12+=n;
}

inline float __peek_2_12(int offs) {
return BUFFER_2_12[TAIL_2_12+offs];
}



inline void __push_12_13(float data) {
BUFFER_12_13[HEAD_12_13]=data;
HEAD_12_13++;
}



 
void init_GetAvg__58_68__12(){
}
void save_file_pointer__12(object_write_buffer *buf) {}
void load_file_pointer__12(object_write_buffer *buf) {}
 
void work_GetAvg__58_68__12(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__54 = 0;/* int */
      float Sum__55 = 0.0f;/* float */
      float val__56 = 0.0f;/* float */
      float __tmp8__57 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__55 = ((float)0.0))/*float*/;
      for ((i__54 = 0)/*int*/; (i__54 < 256); (i__54++)) {{
          val__56 = BUFFER_2_12[TAIL_2_12]; TAIL_2_12++;
;
          (Sum__55 = (Sum__55 + val__56))/*float*/;
          __push_12_13(val__56);
        }
      }
      (__tmp8__57 = (Sum__55 / ((float)256.0)))/*float*/;
      __push_12_13(__tmp8__57);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_13;
int __counter_13 = 0;
int __steady_13 = 0;
int __tmp_13 = 0;
int __tmp2_13 = 0;
int *__state_flag_13 = NULL;
thread_info *__thread_13 = NULL;



float avg__59__13 = 0.0f;
int i__60__13 = 0;
void save_peek_buffer__13(object_write_buffer *buf);
void load_peek_buffer__13(object_write_buffer *buf);
void save_file_pointer__13(object_write_buffer *buf);
void load_file_pointer__13(object_write_buffer *buf);

inline void check_status__13() {
  check_thread_status(__state_flag_13, __thread_13);
}

void check_status_during_io__13() {
  check_thread_status_during_io(__state_flag_13, __thread_13);
}

void __init_thread_info_13(thread_info *info) {
  __state_flag_13 = info->get_state_flag();
}

thread_info *__get_thread_info_13() {
  if (__thread_13 != NULL) return __thread_13;
  __thread_13 = new thread_info(13, check_status_during_io__13);
  __init_thread_info_13(__thread_13);
  return __thread_13;
}

void __declare_sockets_13() {
  init_instance::add_incoming(12,13, DATA_SOCKET);
  init_instance::add_outgoing(13,14, DATA_SOCKET);
}

void __init_sockets_13(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_13() {
}

void __peek_sockets_13() {
}

 
void init_DataMinusAvg__66_70__13();
inline void check_status__13();

void work_DataMinusAvg__66_70__13(int);


inline float __pop_12_13() {
float res=BUFFER_12_13[TAIL_12_13];
TAIL_12_13++;
return res;
}

inline void __pop_12_13(int n) {
TAIL_12_13+=n;
}

inline float __peek_12_13(int offs) {
return BUFFER_12_13[TAIL_12_13+offs];
}



inline void __push_13_14(float data) {
BUFFER_13_14[HEAD_13_14]=data;
HEAD_13_14++;
}



 
void init_DataMinusAvg__66_70__13(){
}
void save_file_pointer__13(object_write_buffer *buf) {}
void load_file_pointer__13(object_write_buffer *buf) {}
 
void work_DataMinusAvg__66_70__13(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__63 = 0.0f;/* float */
      float v__64 = 0.0f;/* float */
      float __tmp11__65 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__63 = BUFFER_12_13[TAIL_12_13+256];
;
      ((avg__59__13) = __tmp9__63)/*float*/;
      for (((i__60__13) = 0)/*int*/; ((i__60__13) < 256); ((i__60__13)++)) {{
          v__64 = BUFFER_12_13[TAIL_12_13]; TAIL_12_13++;
;
          (__tmp11__65 = (v__64 - (avg__59__13)))/*float*/;
          __push_13_14(__tmp11__65);
        }
      }
      __pop_12_13();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_14;
int __counter_14 = 0;
int __steady_14 = 0;
int __tmp_14 = 0;
int __tmp2_14 = 0;
int *__state_flag_14 = NULL;
thread_info *__thread_14 = NULL;



inline void check_status__14() {
  check_thread_status(__state_flag_14, __thread_14);
}

void check_status_during_io__14() {
  check_thread_status_during_io(__state_flag_14, __thread_14);
}

void __init_thread_info_14(thread_info *info) {
  __state_flag_14 = info->get_state_flag();
}

thread_info *__get_thread_info_14() {
  if (__thread_14 != NULL) return __thread_14;
  __thread_14 = new thread_info(14, check_status_during_io__14);
  __init_thread_info_14(__thread_14);
  return __thread_14;
}

void __declare_sockets_14() {
  init_instance::add_incoming(13,14, DATA_SOCKET);
  init_instance::add_outgoing(14,15, DATA_SOCKET);
  init_instance::add_outgoing(14,18, DATA_SOCKET);
}

void __init_sockets_14(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_14() {
}

void __peek_sockets_14() {
}

inline float __pop_13_14() {
float res=BUFFER_13_14[TAIL_13_14];
TAIL_13_14++;
return res;
}


inline void __push_14_15(float data) {
BUFFER_14_15[HEAD_14_15]=data;
HEAD_14_15++;
}



inline void __push_14_18(float data) {
BUFFER_14_18[HEAD_14_18]=data;
HEAD_14_18++;
}



void __splitter_14_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_13_14[TAIL_13_14]; TAIL_13_14++;
;
  __push_14_15(tmp);
  __push_14_18(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_15;
int __counter_15 = 0;
int __steady_15 = 0;
int __tmp_15 = 0;
int __tmp2_15 = 0;
int *__state_flag_15 = NULL;
thread_info *__thread_15 = NULL;



float data__67__15 = 0.0f;
int i__68__15 = 0;
void save_peek_buffer__15(object_write_buffer *buf);
void load_peek_buffer__15(object_write_buffer *buf);
void save_file_pointer__15(object_write_buffer *buf);
void load_file_pointer__15(object_write_buffer *buf);

inline void check_status__15() {
  check_thread_status(__state_flag_15, __thread_15);
}

void check_status_during_io__15() {
  check_thread_status_during_io(__state_flag_15, __thread_15);
}

void __init_thread_info_15(thread_info *info) {
  __state_flag_15 = info->get_state_flag();
}

thread_info *__get_thread_info_15() {
  if (__thread_15 != NULL) return __thread_15;
  __thread_15 = new thread_info(15, check_status_during_io__15);
  __init_thread_info_15(__thread_15);
  return __thread_15;
}

void __declare_sockets_15() {
  init_instance::add_incoming(14,15, DATA_SOCKET);
  init_instance::add_outgoing(15,16, DATA_SOCKET);
}

void __init_sockets_15(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_15() {
}

void __peek_sockets_15() {
}

 
void init_DenomDataSum__72_72__15();
inline void check_status__15();

void work_DenomDataSum__72_72__15(int);


inline float __pop_14_15() {
float res=BUFFER_14_15[TAIL_14_15];
TAIL_14_15++;
return res;
}

inline void __pop_14_15(int n) {
TAIL_14_15+=n;
}

inline float __peek_14_15(int offs) {
return BUFFER_14_15[TAIL_14_15+offs];
}



inline void __push_15_16(float data) {
BUFFER_15_16[HEAD_15_16]=data;
HEAD_15_16++;
}



 
void init_DenomDataSum__72_72__15(){
}
void save_file_pointer__15(object_write_buffer *buf) {}
void load_file_pointer__15(object_write_buffer *buf) {}
 
void work_DenomDataSum__72_72__15(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__71 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__71 = ((float)0.0))/*float*/;
      for (((i__68__15) = 0)/*int*/; ((i__68__15) < 256); ((i__68__15)++)) {{
          (data__67__15) = BUFFER_14_15[TAIL_14_15]; TAIL_14_15++;
;
          (sum__71 = (sum__71 + ((data__67__15) * (data__67__15))))/*float*/;
        }
      }
      __push_15_16(sum__71);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_16;
int __counter_16 = 0;
int __steady_16 = 0;
int __tmp_16 = 0;
int __tmp2_16 = 0;
int *__state_flag_16 = NULL;
thread_info *__thread_16 = NULL;



inline void check_status__16() {
  check_thread_status(__state_flag_16, __thread_16);
}

void check_status_during_io__16() {
  check_thread_status_during_io(__state_flag_16, __thread_16);
}

void __init_thread_info_16(thread_info *info) {
  __state_flag_16 = info->get_state_flag();
}

thread_info *__get_thread_info_16() {
  if (__thread_16 != NULL) return __thread_16;
  __thread_16 = new thread_info(16, check_status_during_io__16);
  __init_thread_info_16(__thread_16);
  return __thread_16;
}

void __declare_sockets_16() {
  init_instance::add_incoming(15,16, DATA_SOCKET);
  init_instance::add_incoming(18,16, DATA_SOCKET);
  init_instance::add_outgoing(16,17, DATA_SOCKET);
}

void __init_sockets_16(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_16() {
}

void __peek_sockets_16() {
}


inline void __push_16_17(float data) {
BUFFER_16_17[HEAD_16_17]=data;
HEAD_16_17++;
}


inline float __pop_15_16() {
float res=BUFFER_15_16[TAIL_15_16];
TAIL_15_16++;
return res;
}

inline float __pop_18_16() {
float res=BUFFER_18_16[TAIL_18_16];
TAIL_18_16++;
return res;
}


void __joiner_16_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_16_17(__pop_15_16());
    __push_16_17(__pop_18_16());
    __push_16_17(__pop_18_16());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_17;
int __counter_17 = 0;
int __steady_17 = 0;
int __tmp_17 = 0;
int __tmp2_17 = 0;
int *__state_flag_17 = NULL;
thread_info *__thread_17 = NULL;



float numer__82__17 = 0.0f;
float denom_x__83__17 = 0.0f;
float denom_y__84__17 = 0.0f;
float denom__85__17 = 0.0f;
void save_peek_buffer__17(object_write_buffer *buf);
void load_peek_buffer__17(object_write_buffer *buf);
void save_file_pointer__17(object_write_buffer *buf);
void load_file_pointer__17(object_write_buffer *buf);

inline void check_status__17() {
  check_thread_status(__state_flag_17, __thread_17);
}

void check_status_during_io__17() {
  check_thread_status_during_io(__state_flag_17, __thread_17);
}

void __init_thread_info_17(thread_info *info) {
  __state_flag_17 = info->get_state_flag();
}

thread_info *__get_thread_info_17() {
  if (__thread_17 != NULL) return __thread_17;
  __thread_17 = new thread_info(17, check_status_during_io__17);
  __init_thread_info_17(__thread_17);
  return __thread_17;
}

void __declare_sockets_17() {
  init_instance::add_incoming(16,17, DATA_SOCKET);
  init_instance::add_outgoing(17,9, DATA_SOCKET);
}

void __init_sockets_17(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_17() {
}

void __peek_sockets_17() {
}

 
void init_Division__92_74__17();
inline void check_status__17();

void work_Division__92_74__17(int);


inline float __pop_16_17() {
float res=BUFFER_16_17[TAIL_16_17];
TAIL_16_17++;
return res;
}

inline void __pop_16_17(int n) {
TAIL_16_17+=n;
}

inline float __peek_16_17(int offs) {
return BUFFER_16_17[TAIL_16_17+offs];
}



inline void __push_17_9(float data) {
BUFFER_17_9[HEAD_17_9]=data;
HEAD_17_9++;
}



 
void init_Division__92_74__17(){
}
void save_file_pointer__17(object_write_buffer *buf) {}
void load_file_pointer__17(object_write_buffer *buf) {}
 
void work_Division__92_74__17(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__88 = 0.0f;/* float */
      float __tmp4__89 = 0.0f;/* float */
      double __tmp5__90 = 0.0f;/* double */
      double __tmp6__91 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__88 = ((float)0.0))/*float*/;
      (denom_x__83__17) = BUFFER_16_17[TAIL_16_17]; TAIL_16_17++;
;
      (denom_y__84__17) = BUFFER_16_17[TAIL_16_17]; TAIL_16_17++;
;
      (__tmp6__91 = ((double)(((denom_x__83__17) * (denom_y__84__17)))))/*double*/;
      (__tmp5__90 = sqrtf(__tmp6__91))/*double*/;
      (__tmp4__89 = ((float)(__tmp5__90)))/*float*/;
      ((denom__85__17) = __tmp4__89)/*float*/;
      (numer__82__17) = BUFFER_16_17[TAIL_16_17]; TAIL_16_17++;
;

      if (((denom__85__17) != ((float)0.0))) {(v__88 = ((numer__82__17) / (denom__85__17)))/*float*/;
      } else {}
      __push_17_9(v__88);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_18;
int __counter_18 = 0;
int __steady_18 = 0;
int __tmp_18 = 0;
int __tmp2_18 = 0;
int *__state_flag_18 = NULL;
thread_info *__thread_18 = NULL;



float _TheGlobal__pattern__6____18[256] = {0};
float numerSum__74__18 = 0.0f;
float denomPatternSum__75__18 = 0.0f;
float data__76__18 = 0.0f;
float patternData__77__18 = 0.0f;
int i__78__18 = 0;
void save_peek_buffer__18(object_write_buffer *buf);
void load_peek_buffer__18(object_write_buffer *buf);
void save_file_pointer__18(object_write_buffer *buf);
void load_file_pointer__18(object_write_buffer *buf);

inline void check_status__18() {
  check_thread_status(__state_flag_18, __thread_18);
}

void check_status_during_io__18() {
  check_thread_status_during_io(__state_flag_18, __thread_18);
}

void __init_thread_info_18(thread_info *info) {
  __state_flag_18 = info->get_state_flag();
}

thread_info *__get_thread_info_18() {
  if (__thread_18 != NULL) return __thread_18;
  __thread_18 = new thread_info(18, check_status_during_io__18);
  __init_thread_info_18(__thread_18);
  return __thread_18;
}

void __declare_sockets_18() {
  init_instance::add_incoming(14,18, DATA_SOCKET);
  init_instance::add_outgoing(18,16, DATA_SOCKET);
}

void __init_sockets_18(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_18() {
}

void __peek_sockets_18() {
}

 
void init_PatternSums__81_73__18();
inline void check_status__18();

void work_PatternSums__81_73__18(int);


inline float __pop_14_18() {
float res=BUFFER_14_18[TAIL_14_18];
TAIL_14_18++;
return res;
}

inline void __pop_14_18(int n) {
TAIL_14_18+=n;
}

inline float __peek_14_18(int offs) {
return BUFFER_14_18[TAIL_14_18+offs];
}



inline void __push_18_16(float data) {
BUFFER_18_16[HEAD_18_16]=data;
HEAD_18_16++;
}



 
void init_PatternSums__81_73__18(){
  (((_TheGlobal__pattern__6____18)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____18)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__18(object_write_buffer *buf) {}
void load_file_pointer__18(object_write_buffer *buf) {}
 
void work_PatternSums__81_73__18(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__74__18) = ((float)0.0))/*float*/;
      ((denomPatternSum__75__18) = ((float)0.0))/*float*/;
      for (((i__78__18) = 0)/*int*/; ((i__78__18) < 256); ((i__78__18)++)) {{
          ((patternData__77__18) = (((_TheGlobal__pattern__6____18)[(int)(i__78__18)]) - ((float)1.6013207E-4)))/*float*/;
          (data__76__18) = BUFFER_14_18[TAIL_14_18]; TAIL_14_18++;
;
          ((numerSum__74__18) = ((numerSum__74__18) + ((data__76__18) * (patternData__77__18))))/*float*/;
          ((denomPatternSum__75__18) = ((denomPatternSum__75__18) + ((patternData__77__18) * (patternData__77__18))))/*float*/;
        }
      }
      __push_18_16((denomPatternSum__75__18));
      __push_18_16((numerSum__74__18));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_19;
int __counter_19 = 0;
int __steady_19 = 0;
int __tmp_19 = 0;
int __tmp2_19 = 0;
int *__state_flag_19 = NULL;
thread_info *__thread_19 = NULL;



void save_peek_buffer__19(object_write_buffer *buf);
void load_peek_buffer__19(object_write_buffer *buf);
void save_file_pointer__19(object_write_buffer *buf);
void load_file_pointer__19(object_write_buffer *buf);

inline void check_status__19() {
  check_thread_status(__state_flag_19, __thread_19);
}

void check_status_during_io__19() {
  check_thread_status_during_io(__state_flag_19, __thread_19);
}

void __init_thread_info_19(thread_info *info) {
  __state_flag_19 = info->get_state_flag();
}

thread_info *__get_thread_info_19() {
  if (__thread_19 != NULL) return __thread_19;
  __thread_19 = new thread_info(19, check_status_during_io__19);
  __init_thread_info_19(__thread_19);
  return __thread_19;
}

void __declare_sockets_19() {
  init_instance::add_incoming(2,19, DATA_SOCKET);
  init_instance::add_outgoing(19,20, DATA_SOCKET);
}

void __init_sockets_19(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_19() {
}

void __peek_sockets_19() {
}

 
void init_GetAvg__99_76__19();
inline void check_status__19();

void work_GetAvg__99_76__19(int);


inline float __pop_2_19() {
float res=BUFFER_2_19[TAIL_2_19];
TAIL_2_19++;
return res;
}

inline void __pop_2_19(int n) {
TAIL_2_19+=n;
}

inline float __peek_2_19(int offs) {
return BUFFER_2_19[TAIL_2_19+offs];
}



inline void __push_19_20(float data) {
BUFFER_19_20[HEAD_19_20]=data;
HEAD_19_20++;
}



 
void init_GetAvg__99_76__19(){
}
void save_file_pointer__19(object_write_buffer *buf) {}
void load_file_pointer__19(object_write_buffer *buf) {}
 
void work_GetAvg__99_76__19(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__95 = 0;/* int */
      float Sum__96 = 0.0f;/* float */
      float val__97 = 0.0f;/* float */
      float __tmp8__98 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__96 = ((float)0.0))/*float*/;
      for ((i__95 = 0)/*int*/; (i__95 < 256); (i__95++)) {{
          val__97 = BUFFER_2_19[TAIL_2_19]; TAIL_2_19++;
;
          (Sum__96 = (Sum__96 + val__97))/*float*/;
          __push_19_20(val__97);
        }
      }
      (__tmp8__98 = (Sum__96 / ((float)256.0)))/*float*/;
      __push_19_20(__tmp8__98);
      // mark end: SIRFilter GetAvg

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_2;
int __counter_2 = 0;
int __steady_2 = 0;
int __tmp_2 = 0;
int __tmp2_2 = 0;
int *__state_flag_2 = NULL;
thread_info *__thread_2 = NULL;



inline void check_status__2() {
  check_thread_status(__state_flag_2, __thread_2);
}

void check_status_during_io__2() {
  check_thread_status_during_io(__state_flag_2, __thread_2);
}

void __init_thread_info_2(thread_info *info) {
  __state_flag_2 = info->get_state_flag();
}

thread_info *__get_thread_info_2() {
  if (__thread_2 != NULL) return __thread_2;
  __thread_2 = new thread_info(2, check_status_during_io__2);
  __init_thread_info_2(__thread_2);
  return __thread_2;
}

void __declare_sockets_2() {
  init_instance::add_incoming(1,2, DATA_SOCKET);
  init_instance::add_outgoing(2,3, DATA_SOCKET);
  init_instance::add_outgoing(2,12, DATA_SOCKET);
  init_instance::add_outgoing(2,19, DATA_SOCKET);
  init_instance::add_outgoing(2,26, DATA_SOCKET);
  init_instance::add_outgoing(2,33, DATA_SOCKET);
  init_instance::add_outgoing(2,40, DATA_SOCKET);
  init_instance::add_outgoing(2,47, DATA_SOCKET);
  init_instance::add_outgoing(2,54, DATA_SOCKET);
  init_instance::add_outgoing(2,61, DATA_SOCKET);
  init_instance::add_outgoing(2,68, DATA_SOCKET);
  init_instance::add_outgoing(2,75, DATA_SOCKET);
  init_instance::add_outgoing(2,82, DATA_SOCKET);
  init_instance::add_outgoing(2,89, DATA_SOCKET);
  init_instance::add_outgoing(2,96, DATA_SOCKET);
  init_instance::add_outgoing(2,103, DATA_SOCKET);
  init_instance::add_outgoing(2,110, DATA_SOCKET);
}

void __init_sockets_2(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_2() {
}

void __peek_sockets_2() {
}

inline float __pop_1_2() {
float res=BUFFER_1_2[TAIL_1_2];
TAIL_1_2++;
return res;
}


inline void __push_2_3(float data) {
BUFFER_2_3[HEAD_2_3]=data;
HEAD_2_3++;
}



inline void __push_2_12(float data) {
BUFFER_2_12[HEAD_2_12]=data;
HEAD_2_12++;
}



inline void __push_2_19(float data) {
BUFFER_2_19[HEAD_2_19]=data;
HEAD_2_19++;
}



inline void __push_2_26(float data) {
BUFFER_2_26[HEAD_2_26]=data;
HEAD_2_26++;
}



inline void __push_2_33(float data) {
BUFFER_2_33[HEAD_2_33]=data;
HEAD_2_33++;
}



inline void __push_2_40(float data) {
BUFFER_2_40[HEAD_2_40]=data;
HEAD_2_40++;
}



inline void __push_2_47(float data) {
BUFFER_2_47[HEAD_2_47]=data;
HEAD_2_47++;
}



inline void __push_2_54(float data) {
BUFFER_2_54[HEAD_2_54]=data;
HEAD_2_54++;
}



inline void __push_2_61(float data) {
BUFFER_2_61[HEAD_2_61]=data;
HEAD_2_61++;
}



inline void __push_2_68(float data) {
BUFFER_2_68[HEAD_2_68]=data;
HEAD_2_68++;
}



inline void __push_2_75(float data) {
BUFFER_2_75[HEAD_2_75]=data;
HEAD_2_75++;
}



inline void __push_2_82(float data) {
BUFFER_2_82[HEAD_2_82]=data;
HEAD_2_82++;
}



inline void __push_2_89(float data) {
BUFFER_2_89[HEAD_2_89]=data;
HEAD_2_89++;
}



inline void __push_2_96(float data) {
BUFFER_2_96[HEAD_2_96]=data;
HEAD_2_96++;
}



inline void __push_2_103(float data) {
BUFFER_2_103[HEAD_2_103]=data;
HEAD_2_103++;
}



inline void __push_2_110(float data) {
BUFFER_2_110[HEAD_2_110]=data;
HEAD_2_110++;
}



void __splitter_2_work(int ____n) {
  for (;____n > 0; ____n--) {
  for (int __k = 0; __k < 32; __k++) {
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_12(__pop_1_2());
__push_2_12(__pop_1_2());
__push_2_12(__pop_1_2());
__push_2_12(__pop_1_2());
__push_2_12(__pop_1_2());
__push_2_12(__pop_1_2());
__push_2_12(__pop_1_2());
__push_2_12(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_19(__pop_1_2());
__push_2_19(__pop_1_2());
__push_2_19(__pop_1_2());
__push_2_19(__pop_1_2());
__push_2_19(__pop_1_2());
__push_2_19(__pop_1_2());
__push_2_19(__pop_1_2());
__push_2_19(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_26(__pop_1_2());
__push_2_26(__pop_1_2());
__push_2_26(__pop_1_2());
__push_2_26(__pop_1_2());
__push_2_26(__pop_1_2());
__push_2_26(__pop_1_2());
__push_2_26(__pop_1_2());
__push_2_26(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_33(__pop_1_2());
__push_2_33(__pop_1_2());
__push_2_33(__pop_1_2());
__push_2_33(__pop_1_2());
__push_2_33(__pop_1_2());
__push_2_33(__pop_1_2());
__push_2_33(__pop_1_2());
__push_2_33(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_40(__pop_1_2());
__push_2_40(__pop_1_2());
__push_2_40(__pop_1_2());
__push_2_40(__pop_1_2());
__push_2_40(__pop_1_2());
__push_2_40(__pop_1_2());
__push_2_40(__pop_1_2());
__push_2_40(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_54(__pop_1_2());
__push_2_54(__pop_1_2());
__push_2_54(__pop_1_2());
__push_2_54(__pop_1_2());
__push_2_54(__pop_1_2());
__push_2_54(__pop_1_2());
__push_2_54(__pop_1_2());
__push_2_54(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_61(__pop_1_2());
__push_2_61(__pop_1_2());
__push_2_61(__pop_1_2());
__push_2_61(__pop_1_2());
__push_2_61(__pop_1_2());
__push_2_61(__pop_1_2());
__push_2_61(__pop_1_2());
__push_2_61(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_68(__pop_1_2());
__push_2_68(__pop_1_2());
__push_2_68(__pop_1_2());
__push_2_68(__pop_1_2());
__push_2_68(__pop_1_2());
__push_2_68(__pop_1_2());
__push_2_68(__pop_1_2());
__push_2_68(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_75(__pop_1_2());
__push_2_75(__pop_1_2());
__push_2_75(__pop_1_2());
__push_2_75(__pop_1_2());
__push_2_75(__pop_1_2());
__push_2_75(__pop_1_2());
__push_2_75(__pop_1_2());
__push_2_75(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_82(__pop_1_2());
__push_2_82(__pop_1_2());
__push_2_82(__pop_1_2());
__push_2_82(__pop_1_2());
__push_2_82(__pop_1_2());
__push_2_82(__pop_1_2());
__push_2_82(__pop_1_2());
__push_2_82(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_96(__pop_1_2());
__push_2_96(__pop_1_2());
__push_2_96(__pop_1_2());
__push_2_96(__pop_1_2());
__push_2_96(__pop_1_2());
__push_2_96(__pop_1_2());
__push_2_96(__pop_1_2());
__push_2_96(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_103(__pop_1_2());
__push_2_103(__pop_1_2());
__push_2_103(__pop_1_2());
__push_2_103(__pop_1_2());
__push_2_103(__pop_1_2());
__push_2_103(__pop_1_2());
__push_2_103(__pop_1_2());
__push_2_103(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_110(__pop_1_2());
__push_2_110(__pop_1_2());
__push_2_110(__pop_1_2());
__push_2_110(__pop_1_2());
__push_2_110(__pop_1_2());
__push_2_110(__pop_1_2());
__push_2_110(__pop_1_2());
__push_2_110(__pop_1_2());
  }
  }
}


// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_20;
int __counter_20 = 0;
int __steady_20 = 0;
int __tmp_20 = 0;
int __tmp2_20 = 0;
int *__state_flag_20 = NULL;
thread_info *__thread_20 = NULL;



float avg__100__20 = 0.0f;
int i__101__20 = 0;
void save_peek_buffer__20(object_write_buffer *buf);
void load_peek_buffer__20(object_write_buffer *buf);
void save_file_pointer__20(object_write_buffer *buf);
void load_file_pointer__20(object_write_buffer *buf);

inline void check_status__20() {
  check_thread_status(__state_flag_20, __thread_20);
}

void check_status_during_io__20() {
  check_thread_status_during_io(__state_flag_20, __thread_20);
}

void __init_thread_info_20(thread_info *info) {
  __state_flag_20 = info->get_state_flag();
}

thread_info *__get_thread_info_20() {
  if (__thread_20 != NULL) return __thread_20;
  __thread_20 = new thread_info(20, check_status_during_io__20);
  __init_thread_info_20(__thread_20);
  return __thread_20;
}

void __declare_sockets_20() {
  init_instance::add_incoming(19,20, DATA_SOCKET);
  init_instance::add_outgoing(20,21, DATA_SOCKET);
}

void __init_sockets_20(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_20() {
}

void __peek_sockets_20() {
}

 
void init_DataMinusAvg__107_78__20();
inline void check_status__20();

void work_DataMinusAvg__107_78__20(int);


inline float __pop_19_20() {
float res=BUFFER_19_20[TAIL_19_20];
TAIL_19_20++;
return res;
}

inline void __pop_19_20(int n) {
TAIL_19_20+=n;
}

inline float __peek_19_20(int offs) {
return BUFFER_19_20[TAIL_19_20+offs];
}



inline void __push_20_21(float data) {
BUFFER_20_21[HEAD_20_21]=data;
HEAD_20_21++;
}



 
void init_DataMinusAvg__107_78__20(){
}
void save_file_pointer__20(object_write_buffer *buf) {}
void load_file_pointer__20(object_write_buffer *buf) {}
 
void work_DataMinusAvg__107_78__20(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__104 = 0.0f;/* float */
      float v__105 = 0.0f;/* float */
      float __tmp11__106 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__104 = BUFFER_19_20[TAIL_19_20+256];
;
      ((avg__100__20) = __tmp9__104)/*float*/;
      for (((i__101__20) = 0)/*int*/; ((i__101__20) < 256); ((i__101__20)++)) {{
          v__105 = BUFFER_19_20[TAIL_19_20]; TAIL_19_20++;
;
          (__tmp11__106 = (v__105 - (avg__100__20)))/*float*/;
          __push_20_21(__tmp11__106);
        }
      }
      __pop_19_20();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_21;
int __counter_21 = 0;
int __steady_21 = 0;
int __tmp_21 = 0;
int __tmp2_21 = 0;
int *__state_flag_21 = NULL;
thread_info *__thread_21 = NULL;



inline void check_status__21() {
  check_thread_status(__state_flag_21, __thread_21);
}

void check_status_during_io__21() {
  check_thread_status_during_io(__state_flag_21, __thread_21);
}

void __init_thread_info_21(thread_info *info) {
  __state_flag_21 = info->get_state_flag();
}

thread_info *__get_thread_info_21() {
  if (__thread_21 != NULL) return __thread_21;
  __thread_21 = new thread_info(21, check_status_during_io__21);
  __init_thread_info_21(__thread_21);
  return __thread_21;
}

void __declare_sockets_21() {
  init_instance::add_incoming(20,21, DATA_SOCKET);
  init_instance::add_outgoing(21,22, DATA_SOCKET);
  init_instance::add_outgoing(21,25, DATA_SOCKET);
}

void __init_sockets_21(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_21() {
}

void __peek_sockets_21() {
}

inline float __pop_20_21() {
float res=BUFFER_20_21[TAIL_20_21];
TAIL_20_21++;
return res;
}


inline void __push_21_22(float data) {
BUFFER_21_22[HEAD_21_22]=data;
HEAD_21_22++;
}



inline void __push_21_25(float data) {
BUFFER_21_25[HEAD_21_25]=data;
HEAD_21_25++;
}



void __splitter_21_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_20_21[TAIL_20_21]; TAIL_20_21++;
;
  __push_21_22(tmp);
  __push_21_25(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_22;
int __counter_22 = 0;
int __steady_22 = 0;
int __tmp_22 = 0;
int __tmp2_22 = 0;
int *__state_flag_22 = NULL;
thread_info *__thread_22 = NULL;



float data__108__22 = 0.0f;
int i__109__22 = 0;
void save_peek_buffer__22(object_write_buffer *buf);
void load_peek_buffer__22(object_write_buffer *buf);
void save_file_pointer__22(object_write_buffer *buf);
void load_file_pointer__22(object_write_buffer *buf);

inline void check_status__22() {
  check_thread_status(__state_flag_22, __thread_22);
}

void check_status_during_io__22() {
  check_thread_status_during_io(__state_flag_22, __thread_22);
}

void __init_thread_info_22(thread_info *info) {
  __state_flag_22 = info->get_state_flag();
}

thread_info *__get_thread_info_22() {
  if (__thread_22 != NULL) return __thread_22;
  __thread_22 = new thread_info(22, check_status_during_io__22);
  __init_thread_info_22(__thread_22);
  return __thread_22;
}

void __declare_sockets_22() {
  init_instance::add_incoming(21,22, DATA_SOCKET);
  init_instance::add_outgoing(22,23, DATA_SOCKET);
}

void __init_sockets_22(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_22() {
}

void __peek_sockets_22() {
}

 
void init_DenomDataSum__113_80__22();
inline void check_status__22();

void work_DenomDataSum__113_80__22(int);


inline float __pop_21_22() {
float res=BUFFER_21_22[TAIL_21_22];
TAIL_21_22++;
return res;
}

inline void __pop_21_22(int n) {
TAIL_21_22+=n;
}

inline float __peek_21_22(int offs) {
return BUFFER_21_22[TAIL_21_22+offs];
}



inline void __push_22_23(float data) {
BUFFER_22_23[HEAD_22_23]=data;
HEAD_22_23++;
}



 
void init_DenomDataSum__113_80__22(){
}
void save_file_pointer__22(object_write_buffer *buf) {}
void load_file_pointer__22(object_write_buffer *buf) {}
 
void work_DenomDataSum__113_80__22(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__112 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__112 = ((float)0.0))/*float*/;
      for (((i__109__22) = 0)/*int*/; ((i__109__22) < 256); ((i__109__22)++)) {{
          (data__108__22) = BUFFER_21_22[TAIL_21_22]; TAIL_21_22++;
;
          (sum__112 = (sum__112 + ((data__108__22) * (data__108__22))))/*float*/;
        }
      }
      __push_22_23(sum__112);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_23;
int __counter_23 = 0;
int __steady_23 = 0;
int __tmp_23 = 0;
int __tmp2_23 = 0;
int *__state_flag_23 = NULL;
thread_info *__thread_23 = NULL;



inline void check_status__23() {
  check_thread_status(__state_flag_23, __thread_23);
}

void check_status_during_io__23() {
  check_thread_status_during_io(__state_flag_23, __thread_23);
}

void __init_thread_info_23(thread_info *info) {
  __state_flag_23 = info->get_state_flag();
}

thread_info *__get_thread_info_23() {
  if (__thread_23 != NULL) return __thread_23;
  __thread_23 = new thread_info(23, check_status_during_io__23);
  __init_thread_info_23(__thread_23);
  return __thread_23;
}

void __declare_sockets_23() {
  init_instance::add_incoming(22,23, DATA_SOCKET);
  init_instance::add_incoming(25,23, DATA_SOCKET);
  init_instance::add_outgoing(23,24, DATA_SOCKET);
}

void __init_sockets_23(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_23() {
}

void __peek_sockets_23() {
}


inline void __push_23_24(float data) {
BUFFER_23_24[HEAD_23_24]=data;
HEAD_23_24++;
}


inline float __pop_22_23() {
float res=BUFFER_22_23[TAIL_22_23];
TAIL_22_23++;
return res;
}

inline float __pop_25_23() {
float res=BUFFER_25_23[TAIL_25_23];
TAIL_25_23++;
return res;
}


void __joiner_23_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_23_24(__pop_22_23());
    __push_23_24(__pop_25_23());
    __push_23_24(__pop_25_23());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_24;
int __counter_24 = 0;
int __steady_24 = 0;
int __tmp_24 = 0;
int __tmp2_24 = 0;
int *__state_flag_24 = NULL;
thread_info *__thread_24 = NULL;



float numer__123__24 = 0.0f;
float denom_x__124__24 = 0.0f;
float denom_y__125__24 = 0.0f;
float denom__126__24 = 0.0f;
void save_peek_buffer__24(object_write_buffer *buf);
void load_peek_buffer__24(object_write_buffer *buf);
void save_file_pointer__24(object_write_buffer *buf);
void load_file_pointer__24(object_write_buffer *buf);

inline void check_status__24() {
  check_thread_status(__state_flag_24, __thread_24);
}

void check_status_during_io__24() {
  check_thread_status_during_io(__state_flag_24, __thread_24);
}

void __init_thread_info_24(thread_info *info) {
  __state_flag_24 = info->get_state_flag();
}

thread_info *__get_thread_info_24() {
  if (__thread_24 != NULL) return __thread_24;
  __thread_24 = new thread_info(24, check_status_during_io__24);
  __init_thread_info_24(__thread_24);
  return __thread_24;
}

void __declare_sockets_24() {
  init_instance::add_incoming(23,24, DATA_SOCKET);
  init_instance::add_outgoing(24,9, DATA_SOCKET);
}

void __init_sockets_24(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_24() {
}

void __peek_sockets_24() {
}

 
void init_Division__133_82__24();
inline void check_status__24();

void work_Division__133_82__24(int);


inline float __pop_23_24() {
float res=BUFFER_23_24[TAIL_23_24];
TAIL_23_24++;
return res;
}

inline void __pop_23_24(int n) {
TAIL_23_24+=n;
}

inline float __peek_23_24(int offs) {
return BUFFER_23_24[TAIL_23_24+offs];
}



inline void __push_24_9(float data) {
BUFFER_24_9[HEAD_24_9]=data;
HEAD_24_9++;
}



 
void init_Division__133_82__24(){
}
void save_file_pointer__24(object_write_buffer *buf) {}
void load_file_pointer__24(object_write_buffer *buf) {}
 
void work_Division__133_82__24(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__129 = 0.0f;/* float */
      float __tmp4__130 = 0.0f;/* float */
      double __tmp5__131 = 0.0f;/* double */
      double __tmp6__132 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__129 = ((float)0.0))/*float*/;
      (denom_x__124__24) = BUFFER_23_24[TAIL_23_24]; TAIL_23_24++;
;
      (denom_y__125__24) = BUFFER_23_24[TAIL_23_24]; TAIL_23_24++;
;
      (__tmp6__132 = ((double)(((denom_x__124__24) * (denom_y__125__24)))))/*double*/;
      (__tmp5__131 = sqrtf(__tmp6__132))/*double*/;
      (__tmp4__130 = ((float)(__tmp5__131)))/*float*/;
      ((denom__126__24) = __tmp4__130)/*float*/;
      (numer__123__24) = BUFFER_23_24[TAIL_23_24]; TAIL_23_24++;
;

      if (((denom__126__24) != ((float)0.0))) {(v__129 = ((numer__123__24) / (denom__126__24)))/*float*/;
      } else {}
      __push_24_9(v__129);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_25;
int __counter_25 = 0;
int __steady_25 = 0;
int __tmp_25 = 0;
int __tmp2_25 = 0;
int *__state_flag_25 = NULL;
thread_info *__thread_25 = NULL;



float _TheGlobal__pattern__6____25[256] = {0};
float numerSum__115__25 = 0.0f;
float denomPatternSum__116__25 = 0.0f;
float data__117__25 = 0.0f;
float patternData__118__25 = 0.0f;
int i__119__25 = 0;
void save_peek_buffer__25(object_write_buffer *buf);
void load_peek_buffer__25(object_write_buffer *buf);
void save_file_pointer__25(object_write_buffer *buf);
void load_file_pointer__25(object_write_buffer *buf);

inline void check_status__25() {
  check_thread_status(__state_flag_25, __thread_25);
}

void check_status_during_io__25() {
  check_thread_status_during_io(__state_flag_25, __thread_25);
}

void __init_thread_info_25(thread_info *info) {
  __state_flag_25 = info->get_state_flag();
}

thread_info *__get_thread_info_25() {
  if (__thread_25 != NULL) return __thread_25;
  __thread_25 = new thread_info(25, check_status_during_io__25);
  __init_thread_info_25(__thread_25);
  return __thread_25;
}

void __declare_sockets_25() {
  init_instance::add_incoming(21,25, DATA_SOCKET);
  init_instance::add_outgoing(25,23, DATA_SOCKET);
}

void __init_sockets_25(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_25() {
}

void __peek_sockets_25() {
}

 
void init_PatternSums__122_81__25();
inline void check_status__25();

void work_PatternSums__122_81__25(int);


inline float __pop_21_25() {
float res=BUFFER_21_25[TAIL_21_25];
TAIL_21_25++;
return res;
}

inline void __pop_21_25(int n) {
TAIL_21_25+=n;
}

inline float __peek_21_25(int offs) {
return BUFFER_21_25[TAIL_21_25+offs];
}



inline void __push_25_23(float data) {
BUFFER_25_23[HEAD_25_23]=data;
HEAD_25_23++;
}



 
void init_PatternSums__122_81__25(){
  (((_TheGlobal__pattern__6____25)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____25)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__25(object_write_buffer *buf) {}
void load_file_pointer__25(object_write_buffer *buf) {}
 
void work_PatternSums__122_81__25(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__115__25) = ((float)0.0))/*float*/;
      ((denomPatternSum__116__25) = ((float)0.0))/*float*/;
      for (((i__119__25) = 0)/*int*/; ((i__119__25) < 256); ((i__119__25)++)) {{
          ((patternData__118__25) = (((_TheGlobal__pattern__6____25)[(int)(i__119__25)]) - ((float)1.6013207E-4)))/*float*/;
          (data__117__25) = BUFFER_21_25[TAIL_21_25]; TAIL_21_25++;
;
          ((numerSum__115__25) = ((numerSum__115__25) + ((data__117__25) * (patternData__118__25))))/*float*/;
          ((denomPatternSum__116__25) = ((denomPatternSum__116__25) + ((patternData__118__25) * (patternData__118__25))))/*float*/;
        }
      }
      __push_25_23((denomPatternSum__116__25));
      __push_25_23((numerSum__115__25));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_26;
int __counter_26 = 0;
int __steady_26 = 0;
int __tmp_26 = 0;
int __tmp2_26 = 0;
int *__state_flag_26 = NULL;
thread_info *__thread_26 = NULL;



void save_peek_buffer__26(object_write_buffer *buf);
void load_peek_buffer__26(object_write_buffer *buf);
void save_file_pointer__26(object_write_buffer *buf);
void load_file_pointer__26(object_write_buffer *buf);

inline void check_status__26() {
  check_thread_status(__state_flag_26, __thread_26);
}

void check_status_during_io__26() {
  check_thread_status_during_io(__state_flag_26, __thread_26);
}

void __init_thread_info_26(thread_info *info) {
  __state_flag_26 = info->get_state_flag();
}

thread_info *__get_thread_info_26() {
  if (__thread_26 != NULL) return __thread_26;
  __thread_26 = new thread_info(26, check_status_during_io__26);
  __init_thread_info_26(__thread_26);
  return __thread_26;
}

void __declare_sockets_26() {
  init_instance::add_incoming(2,26, DATA_SOCKET);
  init_instance::add_outgoing(26,27, DATA_SOCKET);
}

void __init_sockets_26(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_26() {
}

void __peek_sockets_26() {
}

 
void init_GetAvg__140_84__26();
inline void check_status__26();

void work_GetAvg__140_84__26(int);


inline float __pop_2_26() {
float res=BUFFER_2_26[TAIL_2_26];
TAIL_2_26++;
return res;
}

inline void __pop_2_26(int n) {
TAIL_2_26+=n;
}

inline float __peek_2_26(int offs) {
return BUFFER_2_26[TAIL_2_26+offs];
}



inline void __push_26_27(float data) {
BUFFER_26_27[HEAD_26_27]=data;
HEAD_26_27++;
}



 
void init_GetAvg__140_84__26(){
}
void save_file_pointer__26(object_write_buffer *buf) {}
void load_file_pointer__26(object_write_buffer *buf) {}
 
void work_GetAvg__140_84__26(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__136 = 0;/* int */
      float Sum__137 = 0.0f;/* float */
      float val__138 = 0.0f;/* float */
      float __tmp8__139 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__137 = ((float)0.0))/*float*/;
      for ((i__136 = 0)/*int*/; (i__136 < 256); (i__136++)) {{
          val__138 = BUFFER_2_26[TAIL_2_26]; TAIL_2_26++;
;
          (Sum__137 = (Sum__137 + val__138))/*float*/;
          __push_26_27(val__138);
        }
      }
      (__tmp8__139 = (Sum__137 / ((float)256.0)))/*float*/;
      __push_26_27(__tmp8__139);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_27;
int __counter_27 = 0;
int __steady_27 = 0;
int __tmp_27 = 0;
int __tmp2_27 = 0;
int *__state_flag_27 = NULL;
thread_info *__thread_27 = NULL;



float avg__141__27 = 0.0f;
int i__142__27 = 0;
void save_peek_buffer__27(object_write_buffer *buf);
void load_peek_buffer__27(object_write_buffer *buf);
void save_file_pointer__27(object_write_buffer *buf);
void load_file_pointer__27(object_write_buffer *buf);

inline void check_status__27() {
  check_thread_status(__state_flag_27, __thread_27);
}

void check_status_during_io__27() {
  check_thread_status_during_io(__state_flag_27, __thread_27);
}

void __init_thread_info_27(thread_info *info) {
  __state_flag_27 = info->get_state_flag();
}

thread_info *__get_thread_info_27() {
  if (__thread_27 != NULL) return __thread_27;
  __thread_27 = new thread_info(27, check_status_during_io__27);
  __init_thread_info_27(__thread_27);
  return __thread_27;
}

void __declare_sockets_27() {
  init_instance::add_incoming(26,27, DATA_SOCKET);
  init_instance::add_outgoing(27,28, DATA_SOCKET);
}

void __init_sockets_27(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_27() {
}

void __peek_sockets_27() {
}

 
void init_DataMinusAvg__148_86__27();
inline void check_status__27();

void work_DataMinusAvg__148_86__27(int);


inline float __pop_26_27() {
float res=BUFFER_26_27[TAIL_26_27];
TAIL_26_27++;
return res;
}

inline void __pop_26_27(int n) {
TAIL_26_27+=n;
}

inline float __peek_26_27(int offs) {
return BUFFER_26_27[TAIL_26_27+offs];
}



inline void __push_27_28(float data) {
BUFFER_27_28[HEAD_27_28]=data;
HEAD_27_28++;
}



 
void init_DataMinusAvg__148_86__27(){
}
void save_file_pointer__27(object_write_buffer *buf) {}
void load_file_pointer__27(object_write_buffer *buf) {}
 
void work_DataMinusAvg__148_86__27(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__145 = 0.0f;/* float */
      float v__146 = 0.0f;/* float */
      float __tmp11__147 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__145 = BUFFER_26_27[TAIL_26_27+256];
;
      ((avg__141__27) = __tmp9__145)/*float*/;
      for (((i__142__27) = 0)/*int*/; ((i__142__27) < 256); ((i__142__27)++)) {{
          v__146 = BUFFER_26_27[TAIL_26_27]; TAIL_26_27++;
;
          (__tmp11__147 = (v__146 - (avg__141__27)))/*float*/;
          __push_27_28(__tmp11__147);
        }
      }
      __pop_26_27();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_28;
int __counter_28 = 0;
int __steady_28 = 0;
int __tmp_28 = 0;
int __tmp2_28 = 0;
int *__state_flag_28 = NULL;
thread_info *__thread_28 = NULL;



inline void check_status__28() {
  check_thread_status(__state_flag_28, __thread_28);
}

void check_status_during_io__28() {
  check_thread_status_during_io(__state_flag_28, __thread_28);
}

void __init_thread_info_28(thread_info *info) {
  __state_flag_28 = info->get_state_flag();
}

thread_info *__get_thread_info_28() {
  if (__thread_28 != NULL) return __thread_28;
  __thread_28 = new thread_info(28, check_status_during_io__28);
  __init_thread_info_28(__thread_28);
  return __thread_28;
}

void __declare_sockets_28() {
  init_instance::add_incoming(27,28, DATA_SOCKET);
  init_instance::add_outgoing(28,29, DATA_SOCKET);
  init_instance::add_outgoing(28,32, DATA_SOCKET);
}

void __init_sockets_28(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_28() {
}

void __peek_sockets_28() {
}

inline float __pop_27_28() {
float res=BUFFER_27_28[TAIL_27_28];
TAIL_27_28++;
return res;
}


inline void __push_28_29(float data) {
BUFFER_28_29[HEAD_28_29]=data;
HEAD_28_29++;
}



inline void __push_28_32(float data) {
BUFFER_28_32[HEAD_28_32]=data;
HEAD_28_32++;
}



void __splitter_28_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_27_28[TAIL_27_28]; TAIL_27_28++;
;
  __push_28_29(tmp);
  __push_28_32(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_29;
int __counter_29 = 0;
int __steady_29 = 0;
int __tmp_29 = 0;
int __tmp2_29 = 0;
int *__state_flag_29 = NULL;
thread_info *__thread_29 = NULL;



float data__149__29 = 0.0f;
int i__150__29 = 0;
void save_peek_buffer__29(object_write_buffer *buf);
void load_peek_buffer__29(object_write_buffer *buf);
void save_file_pointer__29(object_write_buffer *buf);
void load_file_pointer__29(object_write_buffer *buf);

inline void check_status__29() {
  check_thread_status(__state_flag_29, __thread_29);
}

void check_status_during_io__29() {
  check_thread_status_during_io(__state_flag_29, __thread_29);
}

void __init_thread_info_29(thread_info *info) {
  __state_flag_29 = info->get_state_flag();
}

thread_info *__get_thread_info_29() {
  if (__thread_29 != NULL) return __thread_29;
  __thread_29 = new thread_info(29, check_status_during_io__29);
  __init_thread_info_29(__thread_29);
  return __thread_29;
}

void __declare_sockets_29() {
  init_instance::add_incoming(28,29, DATA_SOCKET);
  init_instance::add_outgoing(29,30, DATA_SOCKET);
}

void __init_sockets_29(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_29() {
}

void __peek_sockets_29() {
}

 
void init_DenomDataSum__154_88__29();
inline void check_status__29();

void work_DenomDataSum__154_88__29(int);


inline float __pop_28_29() {
float res=BUFFER_28_29[TAIL_28_29];
TAIL_28_29++;
return res;
}

inline void __pop_28_29(int n) {
TAIL_28_29+=n;
}

inline float __peek_28_29(int offs) {
return BUFFER_28_29[TAIL_28_29+offs];
}



inline void __push_29_30(float data) {
BUFFER_29_30[HEAD_29_30]=data;
HEAD_29_30++;
}



 
void init_DenomDataSum__154_88__29(){
}
void save_file_pointer__29(object_write_buffer *buf) {}
void load_file_pointer__29(object_write_buffer *buf) {}
 
void work_DenomDataSum__154_88__29(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__153 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__153 = ((float)0.0))/*float*/;
      for (((i__150__29) = 0)/*int*/; ((i__150__29) < 256); ((i__150__29)++)) {{
          (data__149__29) = BUFFER_28_29[TAIL_28_29]; TAIL_28_29++;
;
          (sum__153 = (sum__153 + ((data__149__29) * (data__149__29))))/*float*/;
        }
      }
      __push_29_30(sum__153);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_3;
int __counter_3 = 0;
int __steady_3 = 0;
int __tmp_3 = 0;
int __tmp2_3 = 0;
int *__state_flag_3 = NULL;
thread_info *__thread_3 = NULL;



void save_peek_buffer__3(object_write_buffer *buf);
void load_peek_buffer__3(object_write_buffer *buf);
void save_file_pointer__3(object_write_buffer *buf);
void load_file_pointer__3(object_write_buffer *buf);

inline void check_status__3() {
  check_thread_status(__state_flag_3, __thread_3);
}

void check_status_during_io__3() {
  check_thread_status_during_io(__state_flag_3, __thread_3);
}

void __init_thread_info_3(thread_info *info) {
  __state_flag_3 = info->get_state_flag();
}

thread_info *__get_thread_info_3() {
  if (__thread_3 != NULL) return __thread_3;
  __thread_3 = new thread_info(3, check_status_during_io__3);
  __init_thread_info_3(__thread_3);
  return __thread_3;
}

void __declare_sockets_3() {
  init_instance::add_incoming(2,3, DATA_SOCKET);
  init_instance::add_outgoing(3,4, DATA_SOCKET);
}

void __init_sockets_3(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_3() {
}

void __peek_sockets_3() {
}

 
void init_GetAvg__17_60__3();
inline void check_status__3();

void work_GetAvg__17_60__3(int);


inline float __pop_2_3() {
float res=BUFFER_2_3[TAIL_2_3];
TAIL_2_3++;
return res;
}

inline void __pop_2_3(int n) {
TAIL_2_3+=n;
}

inline float __peek_2_3(int offs) {
return BUFFER_2_3[TAIL_2_3+offs];
}



inline void __push_3_4(float data) {
BUFFER_3_4[HEAD_3_4]=data;
HEAD_3_4++;
}



 
void init_GetAvg__17_60__3(){
}
void save_file_pointer__3(object_write_buffer *buf) {}
void load_file_pointer__3(object_write_buffer *buf) {}
 
void work_GetAvg__17_60__3(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__13 = 0;/* int */
      float Sum__14 = 0.0f;/* float */
      float val__15 = 0.0f;/* float */
      float __tmp8__16 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__14 = ((float)0.0))/*float*/;
      for ((i__13 = 0)/*int*/; (i__13 < 256); (i__13++)) {{
          val__15 = BUFFER_2_3[TAIL_2_3]; TAIL_2_3++;
;
          (Sum__14 = (Sum__14 + val__15))/*float*/;
          __push_3_4(val__15);
        }
      }
      (__tmp8__16 = (Sum__14 / ((float)256.0)))/*float*/;
      __push_3_4(__tmp8__16);
      // mark end: SIRFilter GetAvg

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_30;
int __counter_30 = 0;
int __steady_30 = 0;
int __tmp_30 = 0;
int __tmp2_30 = 0;
int *__state_flag_30 = NULL;
thread_info *__thread_30 = NULL;



inline void check_status__30() {
  check_thread_status(__state_flag_30, __thread_30);
}

void check_status_during_io__30() {
  check_thread_status_during_io(__state_flag_30, __thread_30);
}

void __init_thread_info_30(thread_info *info) {
  __state_flag_30 = info->get_state_flag();
}

thread_info *__get_thread_info_30() {
  if (__thread_30 != NULL) return __thread_30;
  __thread_30 = new thread_info(30, check_status_during_io__30);
  __init_thread_info_30(__thread_30);
  return __thread_30;
}

void __declare_sockets_30() {
  init_instance::add_incoming(29,30, DATA_SOCKET);
  init_instance::add_incoming(32,30, DATA_SOCKET);
  init_instance::add_outgoing(30,31, DATA_SOCKET);
}

void __init_sockets_30(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_30() {
}

void __peek_sockets_30() {
}


inline void __push_30_31(float data) {
BUFFER_30_31[HEAD_30_31]=data;
HEAD_30_31++;
}


inline float __pop_29_30() {
float res=BUFFER_29_30[TAIL_29_30];
TAIL_29_30++;
return res;
}

inline float __pop_32_30() {
float res=BUFFER_32_30[TAIL_32_30];
TAIL_32_30++;
return res;
}


void __joiner_30_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_30_31(__pop_29_30());
    __push_30_31(__pop_32_30());
    __push_30_31(__pop_32_30());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_31;
int __counter_31 = 0;
int __steady_31 = 0;
int __tmp_31 = 0;
int __tmp2_31 = 0;
int *__state_flag_31 = NULL;
thread_info *__thread_31 = NULL;



float numer__164__31 = 0.0f;
float denom_x__165__31 = 0.0f;
float denom_y__166__31 = 0.0f;
float denom__167__31 = 0.0f;
void save_peek_buffer__31(object_write_buffer *buf);
void load_peek_buffer__31(object_write_buffer *buf);
void save_file_pointer__31(object_write_buffer *buf);
void load_file_pointer__31(object_write_buffer *buf);

inline void check_status__31() {
  check_thread_status(__state_flag_31, __thread_31);
}

void check_status_during_io__31() {
  check_thread_status_during_io(__state_flag_31, __thread_31);
}

void __init_thread_info_31(thread_info *info) {
  __state_flag_31 = info->get_state_flag();
}

thread_info *__get_thread_info_31() {
  if (__thread_31 != NULL) return __thread_31;
  __thread_31 = new thread_info(31, check_status_during_io__31);
  __init_thread_info_31(__thread_31);
  return __thread_31;
}

void __declare_sockets_31() {
  init_instance::add_incoming(30,31, DATA_SOCKET);
  init_instance::add_outgoing(31,9, DATA_SOCKET);
}

void __init_sockets_31(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_31() {
}

void __peek_sockets_31() {
}

 
void init_Division__174_90__31();
inline void check_status__31();

void work_Division__174_90__31(int);


inline float __pop_30_31() {
float res=BUFFER_30_31[TAIL_30_31];
TAIL_30_31++;
return res;
}

inline void __pop_30_31(int n) {
TAIL_30_31+=n;
}

inline float __peek_30_31(int offs) {
return BUFFER_30_31[TAIL_30_31+offs];
}



inline void __push_31_9(float data) {
BUFFER_31_9[HEAD_31_9]=data;
HEAD_31_9++;
}



 
void init_Division__174_90__31(){
}
void save_file_pointer__31(object_write_buffer *buf) {}
void load_file_pointer__31(object_write_buffer *buf) {}
 
void work_Division__174_90__31(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__170 = 0.0f;/* float */
      float __tmp4__171 = 0.0f;/* float */
      double __tmp5__172 = 0.0f;/* double */
      double __tmp6__173 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__170 = ((float)0.0))/*float*/;
      (denom_x__165__31) = BUFFER_30_31[TAIL_30_31]; TAIL_30_31++;
;
      (denom_y__166__31) = BUFFER_30_31[TAIL_30_31]; TAIL_30_31++;
;
      (__tmp6__173 = ((double)(((denom_x__165__31) * (denom_y__166__31)))))/*double*/;
      (__tmp5__172 = sqrtf(__tmp6__173))/*double*/;
      (__tmp4__171 = ((float)(__tmp5__172)))/*float*/;
      ((denom__167__31) = __tmp4__171)/*float*/;
      (numer__164__31) = BUFFER_30_31[TAIL_30_31]; TAIL_30_31++;
;

      if (((denom__167__31) != ((float)0.0))) {(v__170 = ((numer__164__31) / (denom__167__31)))/*float*/;
      } else {}
      __push_31_9(v__170);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_32;
int __counter_32 = 0;
int __steady_32 = 0;
int __tmp_32 = 0;
int __tmp2_32 = 0;
int *__state_flag_32 = NULL;
thread_info *__thread_32 = NULL;



float _TheGlobal__pattern__6____32[256] = {0};
float numerSum__156__32 = 0.0f;
float denomPatternSum__157__32 = 0.0f;
float data__158__32 = 0.0f;
float patternData__159__32 = 0.0f;
int i__160__32 = 0;
void save_peek_buffer__32(object_write_buffer *buf);
void load_peek_buffer__32(object_write_buffer *buf);
void save_file_pointer__32(object_write_buffer *buf);
void load_file_pointer__32(object_write_buffer *buf);

inline void check_status__32() {
  check_thread_status(__state_flag_32, __thread_32);
}

void check_status_during_io__32() {
  check_thread_status_during_io(__state_flag_32, __thread_32);
}

void __init_thread_info_32(thread_info *info) {
  __state_flag_32 = info->get_state_flag();
}

thread_info *__get_thread_info_32() {
  if (__thread_32 != NULL) return __thread_32;
  __thread_32 = new thread_info(32, check_status_during_io__32);
  __init_thread_info_32(__thread_32);
  return __thread_32;
}

void __declare_sockets_32() {
  init_instance::add_incoming(28,32, DATA_SOCKET);
  init_instance::add_outgoing(32,30, DATA_SOCKET);
}

void __init_sockets_32(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_32() {
}

void __peek_sockets_32() {
}

 
void init_PatternSums__163_89__32();
inline void check_status__32();

void work_PatternSums__163_89__32(int);


inline float __pop_28_32() {
float res=BUFFER_28_32[TAIL_28_32];
TAIL_28_32++;
return res;
}

inline void __pop_28_32(int n) {
TAIL_28_32+=n;
}

inline float __peek_28_32(int offs) {
return BUFFER_28_32[TAIL_28_32+offs];
}



inline void __push_32_30(float data) {
BUFFER_32_30[HEAD_32_30]=data;
HEAD_32_30++;
}



 
void init_PatternSums__163_89__32(){
  (((_TheGlobal__pattern__6____32)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____32)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__32(object_write_buffer *buf) {}
void load_file_pointer__32(object_write_buffer *buf) {}
 
void work_PatternSums__163_89__32(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__156__32) = ((float)0.0))/*float*/;
      ((denomPatternSum__157__32) = ((float)0.0))/*float*/;
      for (((i__160__32) = 0)/*int*/; ((i__160__32) < 256); ((i__160__32)++)) {{
          ((patternData__159__32) = (((_TheGlobal__pattern__6____32)[(int)(i__160__32)]) - ((float)1.6013207E-4)))/*float*/;
          (data__158__32) = BUFFER_28_32[TAIL_28_32]; TAIL_28_32++;
;
          ((numerSum__156__32) = ((numerSum__156__32) + ((data__158__32) * (patternData__159__32))))/*float*/;
          ((denomPatternSum__157__32) = ((denomPatternSum__157__32) + ((patternData__159__32) * (patternData__159__32))))/*float*/;
        }
      }
      __push_32_30((denomPatternSum__157__32));
      __push_32_30((numerSum__156__32));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_33;
int __counter_33 = 0;
int __steady_33 = 0;
int __tmp_33 = 0;
int __tmp2_33 = 0;
int *__state_flag_33 = NULL;
thread_info *__thread_33 = NULL;



void save_peek_buffer__33(object_write_buffer *buf);
void load_peek_buffer__33(object_write_buffer *buf);
void save_file_pointer__33(object_write_buffer *buf);
void load_file_pointer__33(object_write_buffer *buf);

inline void check_status__33() {
  check_thread_status(__state_flag_33, __thread_33);
}

void check_status_during_io__33() {
  check_thread_status_during_io(__state_flag_33, __thread_33);
}

void __init_thread_info_33(thread_info *info) {
  __state_flag_33 = info->get_state_flag();
}

thread_info *__get_thread_info_33() {
  if (__thread_33 != NULL) return __thread_33;
  __thread_33 = new thread_info(33, check_status_during_io__33);
  __init_thread_info_33(__thread_33);
  return __thread_33;
}

void __declare_sockets_33() {
  init_instance::add_incoming(2,33, DATA_SOCKET);
  init_instance::add_outgoing(33,34, DATA_SOCKET);
}

void __init_sockets_33(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_33() {
}

void __peek_sockets_33() {
}

 
void init_GetAvg__181_92__33();
inline void check_status__33();

void work_GetAvg__181_92__33(int);


inline float __pop_2_33() {
float res=BUFFER_2_33[TAIL_2_33];
TAIL_2_33++;
return res;
}

inline void __pop_2_33(int n) {
TAIL_2_33+=n;
}

inline float __peek_2_33(int offs) {
return BUFFER_2_33[TAIL_2_33+offs];
}



inline void __push_33_34(float data) {
BUFFER_33_34[HEAD_33_34]=data;
HEAD_33_34++;
}



 
void init_GetAvg__181_92__33(){
}
void save_file_pointer__33(object_write_buffer *buf) {}
void load_file_pointer__33(object_write_buffer *buf) {}
 
void work_GetAvg__181_92__33(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__177 = 0;/* int */
      float Sum__178 = 0.0f;/* float */
      float val__179 = 0.0f;/* float */
      float __tmp8__180 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__178 = ((float)0.0))/*float*/;
      for ((i__177 = 0)/*int*/; (i__177 < 256); (i__177++)) {{
          val__179 = BUFFER_2_33[TAIL_2_33]; TAIL_2_33++;
;
          (Sum__178 = (Sum__178 + val__179))/*float*/;
          __push_33_34(val__179);
        }
      }
      (__tmp8__180 = (Sum__178 / ((float)256.0)))/*float*/;
      __push_33_34(__tmp8__180);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_34;
int __counter_34 = 0;
int __steady_34 = 0;
int __tmp_34 = 0;
int __tmp2_34 = 0;
int *__state_flag_34 = NULL;
thread_info *__thread_34 = NULL;



float avg__182__34 = 0.0f;
int i__183__34 = 0;
void save_peek_buffer__34(object_write_buffer *buf);
void load_peek_buffer__34(object_write_buffer *buf);
void save_file_pointer__34(object_write_buffer *buf);
void load_file_pointer__34(object_write_buffer *buf);

inline void check_status__34() {
  check_thread_status(__state_flag_34, __thread_34);
}

void check_status_during_io__34() {
  check_thread_status_during_io(__state_flag_34, __thread_34);
}

void __init_thread_info_34(thread_info *info) {
  __state_flag_34 = info->get_state_flag();
}

thread_info *__get_thread_info_34() {
  if (__thread_34 != NULL) return __thread_34;
  __thread_34 = new thread_info(34, check_status_during_io__34);
  __init_thread_info_34(__thread_34);
  return __thread_34;
}

void __declare_sockets_34() {
  init_instance::add_incoming(33,34, DATA_SOCKET);
  init_instance::add_outgoing(34,35, DATA_SOCKET);
}

void __init_sockets_34(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_34() {
}

void __peek_sockets_34() {
}

 
void init_DataMinusAvg__189_94__34();
inline void check_status__34();

void work_DataMinusAvg__189_94__34(int);


inline float __pop_33_34() {
float res=BUFFER_33_34[TAIL_33_34];
TAIL_33_34++;
return res;
}

inline void __pop_33_34(int n) {
TAIL_33_34+=n;
}

inline float __peek_33_34(int offs) {
return BUFFER_33_34[TAIL_33_34+offs];
}



inline void __push_34_35(float data) {
BUFFER_34_35[HEAD_34_35]=data;
HEAD_34_35++;
}



 
void init_DataMinusAvg__189_94__34(){
}
void save_file_pointer__34(object_write_buffer *buf) {}
void load_file_pointer__34(object_write_buffer *buf) {}
 
void work_DataMinusAvg__189_94__34(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__186 = 0.0f;/* float */
      float v__187 = 0.0f;/* float */
      float __tmp11__188 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__186 = BUFFER_33_34[TAIL_33_34+256];
;
      ((avg__182__34) = __tmp9__186)/*float*/;
      for (((i__183__34) = 0)/*int*/; ((i__183__34) < 256); ((i__183__34)++)) {{
          v__187 = BUFFER_33_34[TAIL_33_34]; TAIL_33_34++;
;
          (__tmp11__188 = (v__187 - (avg__182__34)))/*float*/;
          __push_34_35(__tmp11__188);
        }
      }
      __pop_33_34();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_35;
int __counter_35 = 0;
int __steady_35 = 0;
int __tmp_35 = 0;
int __tmp2_35 = 0;
int *__state_flag_35 = NULL;
thread_info *__thread_35 = NULL;



inline void check_status__35() {
  check_thread_status(__state_flag_35, __thread_35);
}

void check_status_during_io__35() {
  check_thread_status_during_io(__state_flag_35, __thread_35);
}

void __init_thread_info_35(thread_info *info) {
  __state_flag_35 = info->get_state_flag();
}

thread_info *__get_thread_info_35() {
  if (__thread_35 != NULL) return __thread_35;
  __thread_35 = new thread_info(35, check_status_during_io__35);
  __init_thread_info_35(__thread_35);
  return __thread_35;
}

void __declare_sockets_35() {
  init_instance::add_incoming(34,35, DATA_SOCKET);
  init_instance::add_outgoing(35,36, DATA_SOCKET);
  init_instance::add_outgoing(35,39, DATA_SOCKET);
}

void __init_sockets_35(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_35() {
}

void __peek_sockets_35() {
}

inline float __pop_34_35() {
float res=BUFFER_34_35[TAIL_34_35];
TAIL_34_35++;
return res;
}


inline void __push_35_36(float data) {
BUFFER_35_36[HEAD_35_36]=data;
HEAD_35_36++;
}



inline void __push_35_39(float data) {
BUFFER_35_39[HEAD_35_39]=data;
HEAD_35_39++;
}



void __splitter_35_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_34_35[TAIL_34_35]; TAIL_34_35++;
;
  __push_35_36(tmp);
  __push_35_39(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_36;
int __counter_36 = 0;
int __steady_36 = 0;
int __tmp_36 = 0;
int __tmp2_36 = 0;
int *__state_flag_36 = NULL;
thread_info *__thread_36 = NULL;



float data__190__36 = 0.0f;
int i__191__36 = 0;
void save_peek_buffer__36(object_write_buffer *buf);
void load_peek_buffer__36(object_write_buffer *buf);
void save_file_pointer__36(object_write_buffer *buf);
void load_file_pointer__36(object_write_buffer *buf);

inline void check_status__36() {
  check_thread_status(__state_flag_36, __thread_36);
}

void check_status_during_io__36() {
  check_thread_status_during_io(__state_flag_36, __thread_36);
}

void __init_thread_info_36(thread_info *info) {
  __state_flag_36 = info->get_state_flag();
}

thread_info *__get_thread_info_36() {
  if (__thread_36 != NULL) return __thread_36;
  __thread_36 = new thread_info(36, check_status_during_io__36);
  __init_thread_info_36(__thread_36);
  return __thread_36;
}

void __declare_sockets_36() {
  init_instance::add_incoming(35,36, DATA_SOCKET);
  init_instance::add_outgoing(36,37, DATA_SOCKET);
}

void __init_sockets_36(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_36() {
}

void __peek_sockets_36() {
}

 
void init_DenomDataSum__195_96__36();
inline void check_status__36();

void work_DenomDataSum__195_96__36(int);


inline float __pop_35_36() {
float res=BUFFER_35_36[TAIL_35_36];
TAIL_35_36++;
return res;
}

inline void __pop_35_36(int n) {
TAIL_35_36+=n;
}

inline float __peek_35_36(int offs) {
return BUFFER_35_36[TAIL_35_36+offs];
}



inline void __push_36_37(float data) {
BUFFER_36_37[HEAD_36_37]=data;
HEAD_36_37++;
}



 
void init_DenomDataSum__195_96__36(){
}
void save_file_pointer__36(object_write_buffer *buf) {}
void load_file_pointer__36(object_write_buffer *buf) {}
 
void work_DenomDataSum__195_96__36(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__194 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__194 = ((float)0.0))/*float*/;
      for (((i__191__36) = 0)/*int*/; ((i__191__36) < 256); ((i__191__36)++)) {{
          (data__190__36) = BUFFER_35_36[TAIL_35_36]; TAIL_35_36++;
;
          (sum__194 = (sum__194 + ((data__190__36) * (data__190__36))))/*float*/;
        }
      }
      __push_36_37(sum__194);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_37;
int __counter_37 = 0;
int __steady_37 = 0;
int __tmp_37 = 0;
int __tmp2_37 = 0;
int *__state_flag_37 = NULL;
thread_info *__thread_37 = NULL;



inline void check_status__37() {
  check_thread_status(__state_flag_37, __thread_37);
}

void check_status_during_io__37() {
  check_thread_status_during_io(__state_flag_37, __thread_37);
}

void __init_thread_info_37(thread_info *info) {
  __state_flag_37 = info->get_state_flag();
}

thread_info *__get_thread_info_37() {
  if (__thread_37 != NULL) return __thread_37;
  __thread_37 = new thread_info(37, check_status_during_io__37);
  __init_thread_info_37(__thread_37);
  return __thread_37;
}

void __declare_sockets_37() {
  init_instance::add_incoming(36,37, DATA_SOCKET);
  init_instance::add_incoming(39,37, DATA_SOCKET);
  init_instance::add_outgoing(37,38, DATA_SOCKET);
}

void __init_sockets_37(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_37() {
}

void __peek_sockets_37() {
}


inline void __push_37_38(float data) {
BUFFER_37_38[HEAD_37_38]=data;
HEAD_37_38++;
}


inline float __pop_36_37() {
float res=BUFFER_36_37[TAIL_36_37];
TAIL_36_37++;
return res;
}

inline float __pop_39_37() {
float res=BUFFER_39_37[TAIL_39_37];
TAIL_39_37++;
return res;
}


void __joiner_37_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_37_38(__pop_36_37());
    __push_37_38(__pop_39_37());
    __push_37_38(__pop_39_37());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_38;
int __counter_38 = 0;
int __steady_38 = 0;
int __tmp_38 = 0;
int __tmp2_38 = 0;
int *__state_flag_38 = NULL;
thread_info *__thread_38 = NULL;



float numer__205__38 = 0.0f;
float denom_x__206__38 = 0.0f;
float denom_y__207__38 = 0.0f;
float denom__208__38 = 0.0f;
void save_peek_buffer__38(object_write_buffer *buf);
void load_peek_buffer__38(object_write_buffer *buf);
void save_file_pointer__38(object_write_buffer *buf);
void load_file_pointer__38(object_write_buffer *buf);

inline void check_status__38() {
  check_thread_status(__state_flag_38, __thread_38);
}

void check_status_during_io__38() {
  check_thread_status_during_io(__state_flag_38, __thread_38);
}

void __init_thread_info_38(thread_info *info) {
  __state_flag_38 = info->get_state_flag();
}

thread_info *__get_thread_info_38() {
  if (__thread_38 != NULL) return __thread_38;
  __thread_38 = new thread_info(38, check_status_during_io__38);
  __init_thread_info_38(__thread_38);
  return __thread_38;
}

void __declare_sockets_38() {
  init_instance::add_incoming(37,38, DATA_SOCKET);
  init_instance::add_outgoing(38,9, DATA_SOCKET);
}

void __init_sockets_38(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_38() {
}

void __peek_sockets_38() {
}

 
void init_Division__215_98__38();
inline void check_status__38();

void work_Division__215_98__38(int);


inline float __pop_37_38() {
float res=BUFFER_37_38[TAIL_37_38];
TAIL_37_38++;
return res;
}

inline void __pop_37_38(int n) {
TAIL_37_38+=n;
}

inline float __peek_37_38(int offs) {
return BUFFER_37_38[TAIL_37_38+offs];
}



inline void __push_38_9(float data) {
BUFFER_38_9[HEAD_38_9]=data;
HEAD_38_9++;
}



 
void init_Division__215_98__38(){
}
void save_file_pointer__38(object_write_buffer *buf) {}
void load_file_pointer__38(object_write_buffer *buf) {}
 
void work_Division__215_98__38(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__211 = 0.0f;/* float */
      float __tmp4__212 = 0.0f;/* float */
      double __tmp5__213 = 0.0f;/* double */
      double __tmp6__214 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__211 = ((float)0.0))/*float*/;
      (denom_x__206__38) = BUFFER_37_38[TAIL_37_38]; TAIL_37_38++;
;
      (denom_y__207__38) = BUFFER_37_38[TAIL_37_38]; TAIL_37_38++;
;
      (__tmp6__214 = ((double)(((denom_x__206__38) * (denom_y__207__38)))))/*double*/;
      (__tmp5__213 = sqrtf(__tmp6__214))/*double*/;
      (__tmp4__212 = ((float)(__tmp5__213)))/*float*/;
      ((denom__208__38) = __tmp4__212)/*float*/;
      (numer__205__38) = BUFFER_37_38[TAIL_37_38]; TAIL_37_38++;
;

      if (((denom__208__38) != ((float)0.0))) {(v__211 = ((numer__205__38) / (denom__208__38)))/*float*/;
      } else {}
      __push_38_9(v__211);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_39;
int __counter_39 = 0;
int __steady_39 = 0;
int __tmp_39 = 0;
int __tmp2_39 = 0;
int *__state_flag_39 = NULL;
thread_info *__thread_39 = NULL;



float _TheGlobal__pattern__6____39[256] = {0};
float numerSum__197__39 = 0.0f;
float denomPatternSum__198__39 = 0.0f;
float data__199__39 = 0.0f;
float patternData__200__39 = 0.0f;
int i__201__39 = 0;
void save_peek_buffer__39(object_write_buffer *buf);
void load_peek_buffer__39(object_write_buffer *buf);
void save_file_pointer__39(object_write_buffer *buf);
void load_file_pointer__39(object_write_buffer *buf);

inline void check_status__39() {
  check_thread_status(__state_flag_39, __thread_39);
}

void check_status_during_io__39() {
  check_thread_status_during_io(__state_flag_39, __thread_39);
}

void __init_thread_info_39(thread_info *info) {
  __state_flag_39 = info->get_state_flag();
}

thread_info *__get_thread_info_39() {
  if (__thread_39 != NULL) return __thread_39;
  __thread_39 = new thread_info(39, check_status_during_io__39);
  __init_thread_info_39(__thread_39);
  return __thread_39;
}

void __declare_sockets_39() {
  init_instance::add_incoming(35,39, DATA_SOCKET);
  init_instance::add_outgoing(39,37, DATA_SOCKET);
}

void __init_sockets_39(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_39() {
}

void __peek_sockets_39() {
}

 
void init_PatternSums__204_97__39();
inline void check_status__39();

void work_PatternSums__204_97__39(int);


inline float __pop_35_39() {
float res=BUFFER_35_39[TAIL_35_39];
TAIL_35_39++;
return res;
}

inline void __pop_35_39(int n) {
TAIL_35_39+=n;
}

inline float __peek_35_39(int offs) {
return BUFFER_35_39[TAIL_35_39+offs];
}



inline void __push_39_37(float data) {
BUFFER_39_37[HEAD_39_37]=data;
HEAD_39_37++;
}



 
void init_PatternSums__204_97__39(){
  (((_TheGlobal__pattern__6____39)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____39)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__39(object_write_buffer *buf) {}
void load_file_pointer__39(object_write_buffer *buf) {}
 
void work_PatternSums__204_97__39(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__197__39) = ((float)0.0))/*float*/;
      ((denomPatternSum__198__39) = ((float)0.0))/*float*/;
      for (((i__201__39) = 0)/*int*/; ((i__201__39) < 256); ((i__201__39)++)) {{
          ((patternData__200__39) = (((_TheGlobal__pattern__6____39)[(int)(i__201__39)]) - ((float)1.6013207E-4)))/*float*/;
          (data__199__39) = BUFFER_35_39[TAIL_35_39]; TAIL_35_39++;
;
          ((numerSum__197__39) = ((numerSum__197__39) + ((data__199__39) * (patternData__200__39))))/*float*/;
          ((denomPatternSum__198__39) = ((denomPatternSum__198__39) + ((patternData__200__39) * (patternData__200__39))))/*float*/;
        }
      }
      __push_39_37((denomPatternSum__198__39));
      __push_39_37((numerSum__197__39));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_4;
int __counter_4 = 0;
int __steady_4 = 0;
int __tmp_4 = 0;
int __tmp2_4 = 0;
int *__state_flag_4 = NULL;
thread_info *__thread_4 = NULL;



float avg__18__4 = 0.0f;
int i__19__4 = 0;
void save_peek_buffer__4(object_write_buffer *buf);
void load_peek_buffer__4(object_write_buffer *buf);
void save_file_pointer__4(object_write_buffer *buf);
void load_file_pointer__4(object_write_buffer *buf);

inline void check_status__4() {
  check_thread_status(__state_flag_4, __thread_4);
}

void check_status_during_io__4() {
  check_thread_status_during_io(__state_flag_4, __thread_4);
}

void __init_thread_info_4(thread_info *info) {
  __state_flag_4 = info->get_state_flag();
}

thread_info *__get_thread_info_4() {
  if (__thread_4 != NULL) return __thread_4;
  __thread_4 = new thread_info(4, check_status_during_io__4);
  __init_thread_info_4(__thread_4);
  return __thread_4;
}

void __declare_sockets_4() {
  init_instance::add_incoming(3,4, DATA_SOCKET);
  init_instance::add_outgoing(4,5, DATA_SOCKET);
}

void __init_sockets_4(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_4() {
}

void __peek_sockets_4() {
}

 
void init_DataMinusAvg__25_62__4();
inline void check_status__4();

void work_DataMinusAvg__25_62__4(int);


inline float __pop_3_4() {
float res=BUFFER_3_4[TAIL_3_4];
TAIL_3_4++;
return res;
}

inline void __pop_3_4(int n) {
TAIL_3_4+=n;
}

inline float __peek_3_4(int offs) {
return BUFFER_3_4[TAIL_3_4+offs];
}



inline void __push_4_5(float data) {
BUFFER_4_5[HEAD_4_5]=data;
HEAD_4_5++;
}



 
void init_DataMinusAvg__25_62__4(){
}
void save_file_pointer__4(object_write_buffer *buf) {}
void load_file_pointer__4(object_write_buffer *buf) {}
 
void work_DataMinusAvg__25_62__4(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__22 = 0.0f;/* float */
      float v__23 = 0.0f;/* float */
      float __tmp11__24 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__22 = BUFFER_3_4[TAIL_3_4+256];
;
      ((avg__18__4) = __tmp9__22)/*float*/;
      for (((i__19__4) = 0)/*int*/; ((i__19__4) < 256); ((i__19__4)++)) {{
          v__23 = BUFFER_3_4[TAIL_3_4]; TAIL_3_4++;
;
          (__tmp11__24 = (v__23 - (avg__18__4)))/*float*/;
          __push_4_5(__tmp11__24);
        }
      }
      __pop_3_4();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_40;
int __counter_40 = 0;
int __steady_40 = 0;
int __tmp_40 = 0;
int __tmp2_40 = 0;
int *__state_flag_40 = NULL;
thread_info *__thread_40 = NULL;



void save_peek_buffer__40(object_write_buffer *buf);
void load_peek_buffer__40(object_write_buffer *buf);
void save_file_pointer__40(object_write_buffer *buf);
void load_file_pointer__40(object_write_buffer *buf);

inline void check_status__40() {
  check_thread_status(__state_flag_40, __thread_40);
}

void check_status_during_io__40() {
  check_thread_status_during_io(__state_flag_40, __thread_40);
}

void __init_thread_info_40(thread_info *info) {
  __state_flag_40 = info->get_state_flag();
}

thread_info *__get_thread_info_40() {
  if (__thread_40 != NULL) return __thread_40;
  __thread_40 = new thread_info(40, check_status_during_io__40);
  __init_thread_info_40(__thread_40);
  return __thread_40;
}

void __declare_sockets_40() {
  init_instance::add_incoming(2,40, DATA_SOCKET);
  init_instance::add_outgoing(40,41, DATA_SOCKET);
}

void __init_sockets_40(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_40() {
}

void __peek_sockets_40() {
}

 
void init_GetAvg__222_100__40();
inline void check_status__40();

void work_GetAvg__222_100__40(int);


inline float __pop_2_40() {
float res=BUFFER_2_40[TAIL_2_40];
TAIL_2_40++;
return res;
}

inline void __pop_2_40(int n) {
TAIL_2_40+=n;
}

inline float __peek_2_40(int offs) {
return BUFFER_2_40[TAIL_2_40+offs];
}



inline void __push_40_41(float data) {
BUFFER_40_41[HEAD_40_41]=data;
HEAD_40_41++;
}



 
void init_GetAvg__222_100__40(){
}
void save_file_pointer__40(object_write_buffer *buf) {}
void load_file_pointer__40(object_write_buffer *buf) {}
 
void work_GetAvg__222_100__40(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__218 = 0;/* int */
      float Sum__219 = 0.0f;/* float */
      float val__220 = 0.0f;/* float */
      float __tmp8__221 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__219 = ((float)0.0))/*float*/;
      for ((i__218 = 0)/*int*/; (i__218 < 256); (i__218++)) {{
          val__220 = BUFFER_2_40[TAIL_2_40]; TAIL_2_40++;
;
          (Sum__219 = (Sum__219 + val__220))/*float*/;
          __push_40_41(val__220);
        }
      }
      (__tmp8__221 = (Sum__219 / ((float)256.0)))/*float*/;
      __push_40_41(__tmp8__221);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_41;
int __counter_41 = 0;
int __steady_41 = 0;
int __tmp_41 = 0;
int __tmp2_41 = 0;
int *__state_flag_41 = NULL;
thread_info *__thread_41 = NULL;



float avg__223__41 = 0.0f;
int i__224__41 = 0;
void save_peek_buffer__41(object_write_buffer *buf);
void load_peek_buffer__41(object_write_buffer *buf);
void save_file_pointer__41(object_write_buffer *buf);
void load_file_pointer__41(object_write_buffer *buf);

inline void check_status__41() {
  check_thread_status(__state_flag_41, __thread_41);
}

void check_status_during_io__41() {
  check_thread_status_during_io(__state_flag_41, __thread_41);
}

void __init_thread_info_41(thread_info *info) {
  __state_flag_41 = info->get_state_flag();
}

thread_info *__get_thread_info_41() {
  if (__thread_41 != NULL) return __thread_41;
  __thread_41 = new thread_info(41, check_status_during_io__41);
  __init_thread_info_41(__thread_41);
  return __thread_41;
}

void __declare_sockets_41() {
  init_instance::add_incoming(40,41, DATA_SOCKET);
  init_instance::add_outgoing(41,42, DATA_SOCKET);
}

void __init_sockets_41(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_41() {
}

void __peek_sockets_41() {
}

 
void init_DataMinusAvg__230_102__41();
inline void check_status__41();

void work_DataMinusAvg__230_102__41(int);


inline float __pop_40_41() {
float res=BUFFER_40_41[TAIL_40_41];
TAIL_40_41++;
return res;
}

inline void __pop_40_41(int n) {
TAIL_40_41+=n;
}

inline float __peek_40_41(int offs) {
return BUFFER_40_41[TAIL_40_41+offs];
}



inline void __push_41_42(float data) {
BUFFER_41_42[HEAD_41_42]=data;
HEAD_41_42++;
}



 
void init_DataMinusAvg__230_102__41(){
}
void save_file_pointer__41(object_write_buffer *buf) {}
void load_file_pointer__41(object_write_buffer *buf) {}
 
void work_DataMinusAvg__230_102__41(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__227 = 0.0f;/* float */
      float v__228 = 0.0f;/* float */
      float __tmp11__229 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__227 = BUFFER_40_41[TAIL_40_41+256];
;
      ((avg__223__41) = __tmp9__227)/*float*/;
      for (((i__224__41) = 0)/*int*/; ((i__224__41) < 256); ((i__224__41)++)) {{
          v__228 = BUFFER_40_41[TAIL_40_41]; TAIL_40_41++;
;
          (__tmp11__229 = (v__228 - (avg__223__41)))/*float*/;
          __push_41_42(__tmp11__229);
        }
      }
      __pop_40_41();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_42;
int __counter_42 = 0;
int __steady_42 = 0;
int __tmp_42 = 0;
int __tmp2_42 = 0;
int *__state_flag_42 = NULL;
thread_info *__thread_42 = NULL;



inline void check_status__42() {
  check_thread_status(__state_flag_42, __thread_42);
}

void check_status_during_io__42() {
  check_thread_status_during_io(__state_flag_42, __thread_42);
}

void __init_thread_info_42(thread_info *info) {
  __state_flag_42 = info->get_state_flag();
}

thread_info *__get_thread_info_42() {
  if (__thread_42 != NULL) return __thread_42;
  __thread_42 = new thread_info(42, check_status_during_io__42);
  __init_thread_info_42(__thread_42);
  return __thread_42;
}

void __declare_sockets_42() {
  init_instance::add_incoming(41,42, DATA_SOCKET);
  init_instance::add_outgoing(42,43, DATA_SOCKET);
  init_instance::add_outgoing(42,46, DATA_SOCKET);
}

void __init_sockets_42(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_42() {
}

void __peek_sockets_42() {
}

inline float __pop_41_42() {
float res=BUFFER_41_42[TAIL_41_42];
TAIL_41_42++;
return res;
}


inline void __push_42_43(float data) {
BUFFER_42_43[HEAD_42_43]=data;
HEAD_42_43++;
}



inline void __push_42_46(float data) {
BUFFER_42_46[HEAD_42_46]=data;
HEAD_42_46++;
}



void __splitter_42_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_41_42[TAIL_41_42]; TAIL_41_42++;
;
  __push_42_43(tmp);
  __push_42_46(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_43;
int __counter_43 = 0;
int __steady_43 = 0;
int __tmp_43 = 0;
int __tmp2_43 = 0;
int *__state_flag_43 = NULL;
thread_info *__thread_43 = NULL;



float data__231__43 = 0.0f;
int i__232__43 = 0;
void save_peek_buffer__43(object_write_buffer *buf);
void load_peek_buffer__43(object_write_buffer *buf);
void save_file_pointer__43(object_write_buffer *buf);
void load_file_pointer__43(object_write_buffer *buf);

inline void check_status__43() {
  check_thread_status(__state_flag_43, __thread_43);
}

void check_status_during_io__43() {
  check_thread_status_during_io(__state_flag_43, __thread_43);
}

void __init_thread_info_43(thread_info *info) {
  __state_flag_43 = info->get_state_flag();
}

thread_info *__get_thread_info_43() {
  if (__thread_43 != NULL) return __thread_43;
  __thread_43 = new thread_info(43, check_status_during_io__43);
  __init_thread_info_43(__thread_43);
  return __thread_43;
}

void __declare_sockets_43() {
  init_instance::add_incoming(42,43, DATA_SOCKET);
  init_instance::add_outgoing(43,44, DATA_SOCKET);
}

void __init_sockets_43(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_43() {
}

void __peek_sockets_43() {
}

 
void init_DenomDataSum__236_104__43();
inline void check_status__43();

void work_DenomDataSum__236_104__43(int);


inline float __pop_42_43() {
float res=BUFFER_42_43[TAIL_42_43];
TAIL_42_43++;
return res;
}

inline void __pop_42_43(int n) {
TAIL_42_43+=n;
}

inline float __peek_42_43(int offs) {
return BUFFER_42_43[TAIL_42_43+offs];
}



inline void __push_43_44(float data) {
BUFFER_43_44[HEAD_43_44]=data;
HEAD_43_44++;
}



 
void init_DenomDataSum__236_104__43(){
}
void save_file_pointer__43(object_write_buffer *buf) {}
void load_file_pointer__43(object_write_buffer *buf) {}
 
void work_DenomDataSum__236_104__43(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__235 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__235 = ((float)0.0))/*float*/;
      for (((i__232__43) = 0)/*int*/; ((i__232__43) < 256); ((i__232__43)++)) {{
          (data__231__43) = BUFFER_42_43[TAIL_42_43]; TAIL_42_43++;
;
          (sum__235 = (sum__235 + ((data__231__43) * (data__231__43))))/*float*/;
        }
      }
      __push_43_44(sum__235);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_44;
int __counter_44 = 0;
int __steady_44 = 0;
int __tmp_44 = 0;
int __tmp2_44 = 0;
int *__state_flag_44 = NULL;
thread_info *__thread_44 = NULL;



inline void check_status__44() {
  check_thread_status(__state_flag_44, __thread_44);
}

void check_status_during_io__44() {
  check_thread_status_during_io(__state_flag_44, __thread_44);
}

void __init_thread_info_44(thread_info *info) {
  __state_flag_44 = info->get_state_flag();
}

thread_info *__get_thread_info_44() {
  if (__thread_44 != NULL) return __thread_44;
  __thread_44 = new thread_info(44, check_status_during_io__44);
  __init_thread_info_44(__thread_44);
  return __thread_44;
}

void __declare_sockets_44() {
  init_instance::add_incoming(43,44, DATA_SOCKET);
  init_instance::add_incoming(46,44, DATA_SOCKET);
  init_instance::add_outgoing(44,45, DATA_SOCKET);
}

void __init_sockets_44(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_44() {
}

void __peek_sockets_44() {
}


inline void __push_44_45(float data) {
BUFFER_44_45[HEAD_44_45]=data;
HEAD_44_45++;
}


inline float __pop_43_44() {
float res=BUFFER_43_44[TAIL_43_44];
TAIL_43_44++;
return res;
}

inline float __pop_46_44() {
float res=BUFFER_46_44[TAIL_46_44];
TAIL_46_44++;
return res;
}


void __joiner_44_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_44_45(__pop_43_44());
    __push_44_45(__pop_46_44());
    __push_44_45(__pop_46_44());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_45;
int __counter_45 = 0;
int __steady_45 = 0;
int __tmp_45 = 0;
int __tmp2_45 = 0;
int *__state_flag_45 = NULL;
thread_info *__thread_45 = NULL;



float numer__246__45 = 0.0f;
float denom_x__247__45 = 0.0f;
float denom_y__248__45 = 0.0f;
float denom__249__45 = 0.0f;
void save_peek_buffer__45(object_write_buffer *buf);
void load_peek_buffer__45(object_write_buffer *buf);
void save_file_pointer__45(object_write_buffer *buf);
void load_file_pointer__45(object_write_buffer *buf);

inline void check_status__45() {
  check_thread_status(__state_flag_45, __thread_45);
}

void check_status_during_io__45() {
  check_thread_status_during_io(__state_flag_45, __thread_45);
}

void __init_thread_info_45(thread_info *info) {
  __state_flag_45 = info->get_state_flag();
}

thread_info *__get_thread_info_45() {
  if (__thread_45 != NULL) return __thread_45;
  __thread_45 = new thread_info(45, check_status_during_io__45);
  __init_thread_info_45(__thread_45);
  return __thread_45;
}

void __declare_sockets_45() {
  init_instance::add_incoming(44,45, DATA_SOCKET);
  init_instance::add_outgoing(45,9, DATA_SOCKET);
}

void __init_sockets_45(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_45() {
}

void __peek_sockets_45() {
}

 
void init_Division__256_106__45();
inline void check_status__45();

void work_Division__256_106__45(int);


inline float __pop_44_45() {
float res=BUFFER_44_45[TAIL_44_45];
TAIL_44_45++;
return res;
}

inline void __pop_44_45(int n) {
TAIL_44_45+=n;
}

inline float __peek_44_45(int offs) {
return BUFFER_44_45[TAIL_44_45+offs];
}



inline void __push_45_9(float data) {
BUFFER_45_9[HEAD_45_9]=data;
HEAD_45_9++;
}



 
void init_Division__256_106__45(){
}
void save_file_pointer__45(object_write_buffer *buf) {}
void load_file_pointer__45(object_write_buffer *buf) {}
 
void work_Division__256_106__45(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__252 = 0.0f;/* float */
      float __tmp4__253 = 0.0f;/* float */
      double __tmp5__254 = 0.0f;/* double */
      double __tmp6__255 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__252 = ((float)0.0))/*float*/;
      (denom_x__247__45) = BUFFER_44_45[TAIL_44_45]; TAIL_44_45++;
;
      (denom_y__248__45) = BUFFER_44_45[TAIL_44_45]; TAIL_44_45++;
;
      (__tmp6__255 = ((double)(((denom_x__247__45) * (denom_y__248__45)))))/*double*/;
      (__tmp5__254 = sqrtf(__tmp6__255))/*double*/;
      (__tmp4__253 = ((float)(__tmp5__254)))/*float*/;
      ((denom__249__45) = __tmp4__253)/*float*/;
      (numer__246__45) = BUFFER_44_45[TAIL_44_45]; TAIL_44_45++;
;

      if (((denom__249__45) != ((float)0.0))) {(v__252 = ((numer__246__45) / (denom__249__45)))/*float*/;
      } else {}
      __push_45_9(v__252);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_46;
int __counter_46 = 0;
int __steady_46 = 0;
int __tmp_46 = 0;
int __tmp2_46 = 0;
int *__state_flag_46 = NULL;
thread_info *__thread_46 = NULL;



float _TheGlobal__pattern__6____46[256] = {0};
float numerSum__238__46 = 0.0f;
float denomPatternSum__239__46 = 0.0f;
float data__240__46 = 0.0f;
float patternData__241__46 = 0.0f;
int i__242__46 = 0;
void save_peek_buffer__46(object_write_buffer *buf);
void load_peek_buffer__46(object_write_buffer *buf);
void save_file_pointer__46(object_write_buffer *buf);
void load_file_pointer__46(object_write_buffer *buf);

inline void check_status__46() {
  check_thread_status(__state_flag_46, __thread_46);
}

void check_status_during_io__46() {
  check_thread_status_during_io(__state_flag_46, __thread_46);
}

void __init_thread_info_46(thread_info *info) {
  __state_flag_46 = info->get_state_flag();
}

thread_info *__get_thread_info_46() {
  if (__thread_46 != NULL) return __thread_46;
  __thread_46 = new thread_info(46, check_status_during_io__46);
  __init_thread_info_46(__thread_46);
  return __thread_46;
}

void __declare_sockets_46() {
  init_instance::add_incoming(42,46, DATA_SOCKET);
  init_instance::add_outgoing(46,44, DATA_SOCKET);
}

void __init_sockets_46(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_46() {
}

void __peek_sockets_46() {
}

 
void init_PatternSums__245_105__46();
inline void check_status__46();

void work_PatternSums__245_105__46(int);


inline float __pop_42_46() {
float res=BUFFER_42_46[TAIL_42_46];
TAIL_42_46++;
return res;
}

inline void __pop_42_46(int n) {
TAIL_42_46+=n;
}

inline float __peek_42_46(int offs) {
return BUFFER_42_46[TAIL_42_46+offs];
}



inline void __push_46_44(float data) {
BUFFER_46_44[HEAD_46_44]=data;
HEAD_46_44++;
}



 
void init_PatternSums__245_105__46(){
  (((_TheGlobal__pattern__6____46)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__46(object_write_buffer *buf) {}
void load_file_pointer__46(object_write_buffer *buf) {}
 
void work_PatternSums__245_105__46(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__238__46) = ((float)0.0))/*float*/;
      ((denomPatternSum__239__46) = ((float)0.0))/*float*/;
      for (((i__242__46) = 0)/*int*/; ((i__242__46) < 256); ((i__242__46)++)) {{
          ((patternData__241__46) = (((_TheGlobal__pattern__6____46)[(int)(i__242__46)]) - ((float)1.6013207E-4)))/*float*/;
          (data__240__46) = BUFFER_42_46[TAIL_42_46]; TAIL_42_46++;
;
          ((numerSum__238__46) = ((numerSum__238__46) + ((data__240__46) * (patternData__241__46))))/*float*/;
          ((denomPatternSum__239__46) = ((denomPatternSum__239__46) + ((patternData__241__46) * (patternData__241__46))))/*float*/;
        }
      }
      __push_46_44((denomPatternSum__239__46));
      __push_46_44((numerSum__238__46));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_47;
int __counter_47 = 0;
int __steady_47 = 0;
int __tmp_47 = 0;
int __tmp2_47 = 0;
int *__state_flag_47 = NULL;
thread_info *__thread_47 = NULL;



void save_peek_buffer__47(object_write_buffer *buf);
void load_peek_buffer__47(object_write_buffer *buf);
void save_file_pointer__47(object_write_buffer *buf);
void load_file_pointer__47(object_write_buffer *buf);

inline void check_status__47() {
  check_thread_status(__state_flag_47, __thread_47);
}

void check_status_during_io__47() {
  check_thread_status_during_io(__state_flag_47, __thread_47);
}

void __init_thread_info_47(thread_info *info) {
  __state_flag_47 = info->get_state_flag();
}

thread_info *__get_thread_info_47() {
  if (__thread_47 != NULL) return __thread_47;
  __thread_47 = new thread_info(47, check_status_during_io__47);
  __init_thread_info_47(__thread_47);
  return __thread_47;
}

void __declare_sockets_47() {
  init_instance::add_incoming(2,47, DATA_SOCKET);
  init_instance::add_outgoing(47,48, DATA_SOCKET);
}

void __init_sockets_47(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_47() {
}

void __peek_sockets_47() {
}

 
void init_GetAvg__263_108__47();
inline void check_status__47();

void work_GetAvg__263_108__47(int);


inline float __pop_2_47() {
float res=BUFFER_2_47[TAIL_2_47];
TAIL_2_47++;
return res;
}

inline void __pop_2_47(int n) {
TAIL_2_47+=n;
}

inline float __peek_2_47(int offs) {
return BUFFER_2_47[TAIL_2_47+offs];
}



inline void __push_47_48(float data) {
BUFFER_47_48[HEAD_47_48]=data;
HEAD_47_48++;
}



 
void init_GetAvg__263_108__47(){
}
void save_file_pointer__47(object_write_buffer *buf) {}
void load_file_pointer__47(object_write_buffer *buf) {}
 
void work_GetAvg__263_108__47(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__259 = 0;/* int */
      float Sum__260 = 0.0f;/* float */
      float val__261 = 0.0f;/* float */
      float __tmp8__262 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__260 = ((float)0.0))/*float*/;
      for ((i__259 = 0)/*int*/; (i__259 < 256); (i__259++)) {{
          val__261 = BUFFER_2_47[TAIL_2_47]; TAIL_2_47++;
;
          (Sum__260 = (Sum__260 + val__261))/*float*/;
          __push_47_48(val__261);
        }
      }
      (__tmp8__262 = (Sum__260 / ((float)256.0)))/*float*/;
      __push_47_48(__tmp8__262);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_48;
int __counter_48 = 0;
int __steady_48 = 0;
int __tmp_48 = 0;
int __tmp2_48 = 0;
int *__state_flag_48 = NULL;
thread_info *__thread_48 = NULL;



float avg__264__48 = 0.0f;
int i__265__48 = 0;
void save_peek_buffer__48(object_write_buffer *buf);
void load_peek_buffer__48(object_write_buffer *buf);
void save_file_pointer__48(object_write_buffer *buf);
void load_file_pointer__48(object_write_buffer *buf);

inline void check_status__48() {
  check_thread_status(__state_flag_48, __thread_48);
}

void check_status_during_io__48() {
  check_thread_status_during_io(__state_flag_48, __thread_48);
}

void __init_thread_info_48(thread_info *info) {
  __state_flag_48 = info->get_state_flag();
}

thread_info *__get_thread_info_48() {
  if (__thread_48 != NULL) return __thread_48;
  __thread_48 = new thread_info(48, check_status_during_io__48);
  __init_thread_info_48(__thread_48);
  return __thread_48;
}

void __declare_sockets_48() {
  init_instance::add_incoming(47,48, DATA_SOCKET);
  init_instance::add_outgoing(48,49, DATA_SOCKET);
}

void __init_sockets_48(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_48() {
}

void __peek_sockets_48() {
}

 
void init_DataMinusAvg__271_110__48();
inline void check_status__48();

void work_DataMinusAvg__271_110__48(int);


inline float __pop_47_48() {
float res=BUFFER_47_48[TAIL_47_48];
TAIL_47_48++;
return res;
}

inline void __pop_47_48(int n) {
TAIL_47_48+=n;
}

inline float __peek_47_48(int offs) {
return BUFFER_47_48[TAIL_47_48+offs];
}



inline void __push_48_49(float data) {
BUFFER_48_49[HEAD_48_49]=data;
HEAD_48_49++;
}



 
void init_DataMinusAvg__271_110__48(){
}
void save_file_pointer__48(object_write_buffer *buf) {}
void load_file_pointer__48(object_write_buffer *buf) {}
 
void work_DataMinusAvg__271_110__48(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__268 = 0.0f;/* float */
      float v__269 = 0.0f;/* float */
      float __tmp11__270 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__268 = BUFFER_47_48[TAIL_47_48+256];
;
      ((avg__264__48) = __tmp9__268)/*float*/;
      for (((i__265__48) = 0)/*int*/; ((i__265__48) < 256); ((i__265__48)++)) {{
          v__269 = BUFFER_47_48[TAIL_47_48]; TAIL_47_48++;
;
          (__tmp11__270 = (v__269 - (avg__264__48)))/*float*/;
          __push_48_49(__tmp11__270);
        }
      }
      __pop_47_48();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_49;
int __counter_49 = 0;
int __steady_49 = 0;
int __tmp_49 = 0;
int __tmp2_49 = 0;
int *__state_flag_49 = NULL;
thread_info *__thread_49 = NULL;



inline void check_status__49() {
  check_thread_status(__state_flag_49, __thread_49);
}

void check_status_during_io__49() {
  check_thread_status_during_io(__state_flag_49, __thread_49);
}

void __init_thread_info_49(thread_info *info) {
  __state_flag_49 = info->get_state_flag();
}

thread_info *__get_thread_info_49() {
  if (__thread_49 != NULL) return __thread_49;
  __thread_49 = new thread_info(49, check_status_during_io__49);
  __init_thread_info_49(__thread_49);
  return __thread_49;
}

void __declare_sockets_49() {
  init_instance::add_incoming(48,49, DATA_SOCKET);
  init_instance::add_outgoing(49,50, DATA_SOCKET);
  init_instance::add_outgoing(49,53, DATA_SOCKET);
}

void __init_sockets_49(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_49() {
}

void __peek_sockets_49() {
}

inline float __pop_48_49() {
float res=BUFFER_48_49[TAIL_48_49];
TAIL_48_49++;
return res;
}


inline void __push_49_50(float data) {
BUFFER_49_50[HEAD_49_50]=data;
HEAD_49_50++;
}



inline void __push_49_53(float data) {
BUFFER_49_53[HEAD_49_53]=data;
HEAD_49_53++;
}



void __splitter_49_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_48_49[TAIL_48_49]; TAIL_48_49++;
;
  __push_49_50(tmp);
  __push_49_53(tmp);
  }
}


// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_5;
int __counter_5 = 0;
int __steady_5 = 0;
int __tmp_5 = 0;
int __tmp2_5 = 0;
int *__state_flag_5 = NULL;
thread_info *__thread_5 = NULL;



inline void check_status__5() {
  check_thread_status(__state_flag_5, __thread_5);
}

void check_status_during_io__5() {
  check_thread_status_during_io(__state_flag_5, __thread_5);
}

void __init_thread_info_5(thread_info *info) {
  __state_flag_5 = info->get_state_flag();
}

thread_info *__get_thread_info_5() {
  if (__thread_5 != NULL) return __thread_5;
  __thread_5 = new thread_info(5, check_status_during_io__5);
  __init_thread_info_5(__thread_5);
  return __thread_5;
}

void __declare_sockets_5() {
  init_instance::add_incoming(4,5, DATA_SOCKET);
  init_instance::add_outgoing(5,6, DATA_SOCKET);
  init_instance::add_outgoing(5,11, DATA_SOCKET);
}

void __init_sockets_5(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_5() {
}

void __peek_sockets_5() {
}

inline float __pop_4_5() {
float res=BUFFER_4_5[TAIL_4_5];
TAIL_4_5++;
return res;
}


inline void __push_5_6(float data) {
BUFFER_5_6[HEAD_5_6]=data;
HEAD_5_6++;
}



inline void __push_5_11(float data) {
BUFFER_5_11[HEAD_5_11]=data;
HEAD_5_11++;
}



void __splitter_5_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_4_5[TAIL_4_5]; TAIL_4_5++;
;
  __push_5_6(tmp);
  __push_5_11(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_50;
int __counter_50 = 0;
int __steady_50 = 0;
int __tmp_50 = 0;
int __tmp2_50 = 0;
int *__state_flag_50 = NULL;
thread_info *__thread_50 = NULL;



float data__272__50 = 0.0f;
int i__273__50 = 0;
void save_peek_buffer__50(object_write_buffer *buf);
void load_peek_buffer__50(object_write_buffer *buf);
void save_file_pointer__50(object_write_buffer *buf);
void load_file_pointer__50(object_write_buffer *buf);

inline void check_status__50() {
  check_thread_status(__state_flag_50, __thread_50);
}

void check_status_during_io__50() {
  check_thread_status_during_io(__state_flag_50, __thread_50);
}

void __init_thread_info_50(thread_info *info) {
  __state_flag_50 = info->get_state_flag();
}

thread_info *__get_thread_info_50() {
  if (__thread_50 != NULL) return __thread_50;
  __thread_50 = new thread_info(50, check_status_during_io__50);
  __init_thread_info_50(__thread_50);
  return __thread_50;
}

void __declare_sockets_50() {
  init_instance::add_incoming(49,50, DATA_SOCKET);
  init_instance::add_outgoing(50,51, DATA_SOCKET);
}

void __init_sockets_50(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_50() {
}

void __peek_sockets_50() {
}

 
void init_DenomDataSum__277_112__50();
inline void check_status__50();

void work_DenomDataSum__277_112__50(int);


inline float __pop_49_50() {
float res=BUFFER_49_50[TAIL_49_50];
TAIL_49_50++;
return res;
}

inline void __pop_49_50(int n) {
TAIL_49_50+=n;
}

inline float __peek_49_50(int offs) {
return BUFFER_49_50[TAIL_49_50+offs];
}



inline void __push_50_51(float data) {
BUFFER_50_51[HEAD_50_51]=data;
HEAD_50_51++;
}



 
void init_DenomDataSum__277_112__50(){
}
void save_file_pointer__50(object_write_buffer *buf) {}
void load_file_pointer__50(object_write_buffer *buf) {}
 
void work_DenomDataSum__277_112__50(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__276 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__276 = ((float)0.0))/*float*/;
      for (((i__273__50) = 0)/*int*/; ((i__273__50) < 256); ((i__273__50)++)) {{
          (data__272__50) = BUFFER_49_50[TAIL_49_50]; TAIL_49_50++;
;
          (sum__276 = (sum__276 + ((data__272__50) * (data__272__50))))/*float*/;
        }
      }
      __push_50_51(sum__276);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_51;
int __counter_51 = 0;
int __steady_51 = 0;
int __tmp_51 = 0;
int __tmp2_51 = 0;
int *__state_flag_51 = NULL;
thread_info *__thread_51 = NULL;



inline void check_status__51() {
  check_thread_status(__state_flag_51, __thread_51);
}

void check_status_during_io__51() {
  check_thread_status_during_io(__state_flag_51, __thread_51);
}

void __init_thread_info_51(thread_info *info) {
  __state_flag_51 = info->get_state_flag();
}

thread_info *__get_thread_info_51() {
  if (__thread_51 != NULL) return __thread_51;
  __thread_51 = new thread_info(51, check_status_during_io__51);
  __init_thread_info_51(__thread_51);
  return __thread_51;
}

void __declare_sockets_51() {
  init_instance::add_incoming(50,51, DATA_SOCKET);
  init_instance::add_incoming(53,51, DATA_SOCKET);
  init_instance::add_outgoing(51,52, DATA_SOCKET);
}

void __init_sockets_51(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_51() {
}

void __peek_sockets_51() {
}


inline void __push_51_52(float data) {
BUFFER_51_52[HEAD_51_52]=data;
HEAD_51_52++;
}


inline float __pop_50_51() {
float res=BUFFER_50_51[TAIL_50_51];
TAIL_50_51++;
return res;
}

inline float __pop_53_51() {
float res=BUFFER_53_51[TAIL_53_51];
TAIL_53_51++;
return res;
}


void __joiner_51_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_51_52(__pop_50_51());
    __push_51_52(__pop_53_51());
    __push_51_52(__pop_53_51());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_52;
int __counter_52 = 0;
int __steady_52 = 0;
int __tmp_52 = 0;
int __tmp2_52 = 0;
int *__state_flag_52 = NULL;
thread_info *__thread_52 = NULL;



float numer__287__52 = 0.0f;
float denom_x__288__52 = 0.0f;
float denom_y__289__52 = 0.0f;
float denom__290__52 = 0.0f;
void save_peek_buffer__52(object_write_buffer *buf);
void load_peek_buffer__52(object_write_buffer *buf);
void save_file_pointer__52(object_write_buffer *buf);
void load_file_pointer__52(object_write_buffer *buf);

inline void check_status__52() {
  check_thread_status(__state_flag_52, __thread_52);
}

void check_status_during_io__52() {
  check_thread_status_during_io(__state_flag_52, __thread_52);
}

void __init_thread_info_52(thread_info *info) {
  __state_flag_52 = info->get_state_flag();
}

thread_info *__get_thread_info_52() {
  if (__thread_52 != NULL) return __thread_52;
  __thread_52 = new thread_info(52, check_status_during_io__52);
  __init_thread_info_52(__thread_52);
  return __thread_52;
}

void __declare_sockets_52() {
  init_instance::add_incoming(51,52, DATA_SOCKET);
  init_instance::add_outgoing(52,9, DATA_SOCKET);
}

void __init_sockets_52(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_52() {
}

void __peek_sockets_52() {
}

 
void init_Division__297_114__52();
inline void check_status__52();

void work_Division__297_114__52(int);


inline float __pop_51_52() {
float res=BUFFER_51_52[TAIL_51_52];
TAIL_51_52++;
return res;
}

inline void __pop_51_52(int n) {
TAIL_51_52+=n;
}

inline float __peek_51_52(int offs) {
return BUFFER_51_52[TAIL_51_52+offs];
}



inline void __push_52_9(float data) {
BUFFER_52_9[HEAD_52_9]=data;
HEAD_52_9++;
}



 
void init_Division__297_114__52(){
}
void save_file_pointer__52(object_write_buffer *buf) {}
void load_file_pointer__52(object_write_buffer *buf) {}
 
void work_Division__297_114__52(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__293 = 0.0f;/* float */
      float __tmp4__294 = 0.0f;/* float */
      double __tmp5__295 = 0.0f;/* double */
      double __tmp6__296 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__293 = ((float)0.0))/*float*/;
      (denom_x__288__52) = BUFFER_51_52[TAIL_51_52]; TAIL_51_52++;
;
      (denom_y__289__52) = BUFFER_51_52[TAIL_51_52]; TAIL_51_52++;
;
      (__tmp6__296 = ((double)(((denom_x__288__52) * (denom_y__289__52)))))/*double*/;
      (__tmp5__295 = sqrtf(__tmp6__296))/*double*/;
      (__tmp4__294 = ((float)(__tmp5__295)))/*float*/;
      ((denom__290__52) = __tmp4__294)/*float*/;
      (numer__287__52) = BUFFER_51_52[TAIL_51_52]; TAIL_51_52++;
;

      if (((denom__290__52) != ((float)0.0))) {(v__293 = ((numer__287__52) / (denom__290__52)))/*float*/;
      } else {}
      __push_52_9(v__293);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_53;
int __counter_53 = 0;
int __steady_53 = 0;
int __tmp_53 = 0;
int __tmp2_53 = 0;
int *__state_flag_53 = NULL;
thread_info *__thread_53 = NULL;



float _TheGlobal__pattern__6____53[256] = {0};
float numerSum__279__53 = 0.0f;
float denomPatternSum__280__53 = 0.0f;
float data__281__53 = 0.0f;
float patternData__282__53 = 0.0f;
int i__283__53 = 0;
void save_peek_buffer__53(object_write_buffer *buf);
void load_peek_buffer__53(object_write_buffer *buf);
void save_file_pointer__53(object_write_buffer *buf);
void load_file_pointer__53(object_write_buffer *buf);

inline void check_status__53() {
  check_thread_status(__state_flag_53, __thread_53);
}

void check_status_during_io__53() {
  check_thread_status_during_io(__state_flag_53, __thread_53);
}

void __init_thread_info_53(thread_info *info) {
  __state_flag_53 = info->get_state_flag();
}

thread_info *__get_thread_info_53() {
  if (__thread_53 != NULL) return __thread_53;
  __thread_53 = new thread_info(53, check_status_during_io__53);
  __init_thread_info_53(__thread_53);
  return __thread_53;
}

void __declare_sockets_53() {
  init_instance::add_incoming(49,53, DATA_SOCKET);
  init_instance::add_outgoing(53,51, DATA_SOCKET);
}

void __init_sockets_53(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_53() {
}

void __peek_sockets_53() {
}

 
void init_PatternSums__286_113__53();
inline void check_status__53();

void work_PatternSums__286_113__53(int);


inline float __pop_49_53() {
float res=BUFFER_49_53[TAIL_49_53];
TAIL_49_53++;
return res;
}

inline void __pop_49_53(int n) {
TAIL_49_53+=n;
}

inline float __peek_49_53(int offs) {
return BUFFER_49_53[TAIL_49_53+offs];
}



inline void __push_53_51(float data) {
BUFFER_53_51[HEAD_53_51]=data;
HEAD_53_51++;
}



 
void init_PatternSums__286_113__53(){
  (((_TheGlobal__pattern__6____53)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____53)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__53(object_write_buffer *buf) {}
void load_file_pointer__53(object_write_buffer *buf) {}
 
void work_PatternSums__286_113__53(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__279__53) = ((float)0.0))/*float*/;
      ((denomPatternSum__280__53) = ((float)0.0))/*float*/;
      for (((i__283__53) = 0)/*int*/; ((i__283__53) < 256); ((i__283__53)++)) {{
          ((patternData__282__53) = (((_TheGlobal__pattern__6____53)[(int)(i__283__53)]) - ((float)1.6013207E-4)))/*float*/;
          (data__281__53) = BUFFER_49_53[TAIL_49_53]; TAIL_49_53++;
;
          ((numerSum__279__53) = ((numerSum__279__53) + ((data__281__53) * (patternData__282__53))))/*float*/;
          ((denomPatternSum__280__53) = ((denomPatternSum__280__53) + ((patternData__282__53) * (patternData__282__53))))/*float*/;
        }
      }
      __push_53_51((denomPatternSum__280__53));
      __push_53_51((numerSum__279__53));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_54;
int __counter_54 = 0;
int __steady_54 = 0;
int __tmp_54 = 0;
int __tmp2_54 = 0;
int *__state_flag_54 = NULL;
thread_info *__thread_54 = NULL;



void save_peek_buffer__54(object_write_buffer *buf);
void load_peek_buffer__54(object_write_buffer *buf);
void save_file_pointer__54(object_write_buffer *buf);
void load_file_pointer__54(object_write_buffer *buf);

inline void check_status__54() {
  check_thread_status(__state_flag_54, __thread_54);
}

void check_status_during_io__54() {
  check_thread_status_during_io(__state_flag_54, __thread_54);
}

void __init_thread_info_54(thread_info *info) {
  __state_flag_54 = info->get_state_flag();
}

thread_info *__get_thread_info_54() {
  if (__thread_54 != NULL) return __thread_54;
  __thread_54 = new thread_info(54, check_status_during_io__54);
  __init_thread_info_54(__thread_54);
  return __thread_54;
}

void __declare_sockets_54() {
  init_instance::add_incoming(2,54, DATA_SOCKET);
  init_instance::add_outgoing(54,55, DATA_SOCKET);
}

void __init_sockets_54(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_54() {
}

void __peek_sockets_54() {
}

 
void init_GetAvg__304_116__54();
inline void check_status__54();

void work_GetAvg__304_116__54(int);


inline float __pop_2_54() {
float res=BUFFER_2_54[TAIL_2_54];
TAIL_2_54++;
return res;
}

inline void __pop_2_54(int n) {
TAIL_2_54+=n;
}

inline float __peek_2_54(int offs) {
return BUFFER_2_54[TAIL_2_54+offs];
}



inline void __push_54_55(float data) {
BUFFER_54_55[HEAD_54_55]=data;
HEAD_54_55++;
}



 
void init_GetAvg__304_116__54(){
}
void save_file_pointer__54(object_write_buffer *buf) {}
void load_file_pointer__54(object_write_buffer *buf) {}
 
void work_GetAvg__304_116__54(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__300 = 0;/* int */
      float Sum__301 = 0.0f;/* float */
      float val__302 = 0.0f;/* float */
      float __tmp8__303 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__301 = ((float)0.0))/*float*/;
      for ((i__300 = 0)/*int*/; (i__300 < 256); (i__300++)) {{
          val__302 = BUFFER_2_54[TAIL_2_54]; TAIL_2_54++;
;
          (Sum__301 = (Sum__301 + val__302))/*float*/;
          __push_54_55(val__302);
        }
      }
      (__tmp8__303 = (Sum__301 / ((float)256.0)))/*float*/;
      __push_54_55(__tmp8__303);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_55;
int __counter_55 = 0;
int __steady_55 = 0;
int __tmp_55 = 0;
int __tmp2_55 = 0;
int *__state_flag_55 = NULL;
thread_info *__thread_55 = NULL;



float avg__305__55 = 0.0f;
int i__306__55 = 0;
void save_peek_buffer__55(object_write_buffer *buf);
void load_peek_buffer__55(object_write_buffer *buf);
void save_file_pointer__55(object_write_buffer *buf);
void load_file_pointer__55(object_write_buffer *buf);

inline void check_status__55() {
  check_thread_status(__state_flag_55, __thread_55);
}

void check_status_during_io__55() {
  check_thread_status_during_io(__state_flag_55, __thread_55);
}

void __init_thread_info_55(thread_info *info) {
  __state_flag_55 = info->get_state_flag();
}

thread_info *__get_thread_info_55() {
  if (__thread_55 != NULL) return __thread_55;
  __thread_55 = new thread_info(55, check_status_during_io__55);
  __init_thread_info_55(__thread_55);
  return __thread_55;
}

void __declare_sockets_55() {
  init_instance::add_incoming(54,55, DATA_SOCKET);
  init_instance::add_outgoing(55,56, DATA_SOCKET);
}

void __init_sockets_55(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_55() {
}

void __peek_sockets_55() {
}

 
void init_DataMinusAvg__312_118__55();
inline void check_status__55();

void work_DataMinusAvg__312_118__55(int);


inline float __pop_54_55() {
float res=BUFFER_54_55[TAIL_54_55];
TAIL_54_55++;
return res;
}

inline void __pop_54_55(int n) {
TAIL_54_55+=n;
}

inline float __peek_54_55(int offs) {
return BUFFER_54_55[TAIL_54_55+offs];
}



inline void __push_55_56(float data) {
BUFFER_55_56[HEAD_55_56]=data;
HEAD_55_56++;
}



 
void init_DataMinusAvg__312_118__55(){
}
void save_file_pointer__55(object_write_buffer *buf) {}
void load_file_pointer__55(object_write_buffer *buf) {}
 
void work_DataMinusAvg__312_118__55(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__309 = 0.0f;/* float */
      float v__310 = 0.0f;/* float */
      float __tmp11__311 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__309 = BUFFER_54_55[TAIL_54_55+256];
;
      ((avg__305__55) = __tmp9__309)/*float*/;
      for (((i__306__55) = 0)/*int*/; ((i__306__55) < 256); ((i__306__55)++)) {{
          v__310 = BUFFER_54_55[TAIL_54_55]; TAIL_54_55++;
;
          (__tmp11__311 = (v__310 - (avg__305__55)))/*float*/;
          __push_55_56(__tmp11__311);
        }
      }
      __pop_54_55();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_56;
int __counter_56 = 0;
int __steady_56 = 0;
int __tmp_56 = 0;
int __tmp2_56 = 0;
int *__state_flag_56 = NULL;
thread_info *__thread_56 = NULL;



inline void check_status__56() {
  check_thread_status(__state_flag_56, __thread_56);
}

void check_status_during_io__56() {
  check_thread_status_during_io(__state_flag_56, __thread_56);
}

void __init_thread_info_56(thread_info *info) {
  __state_flag_56 = info->get_state_flag();
}

thread_info *__get_thread_info_56() {
  if (__thread_56 != NULL) return __thread_56;
  __thread_56 = new thread_info(56, check_status_during_io__56);
  __init_thread_info_56(__thread_56);
  return __thread_56;
}

void __declare_sockets_56() {
  init_instance::add_incoming(55,56, DATA_SOCKET);
  init_instance::add_outgoing(56,57, DATA_SOCKET);
  init_instance::add_outgoing(56,60, DATA_SOCKET);
}

void __init_sockets_56(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_56() {
}

void __peek_sockets_56() {
}

inline float __pop_55_56() {
float res=BUFFER_55_56[TAIL_55_56];
TAIL_55_56++;
return res;
}


inline void __push_56_57(float data) {
BUFFER_56_57[HEAD_56_57]=data;
HEAD_56_57++;
}



inline void __push_56_60(float data) {
BUFFER_56_60[HEAD_56_60]=data;
HEAD_56_60++;
}



void __splitter_56_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_55_56[TAIL_55_56]; TAIL_55_56++;
;
  __push_56_57(tmp);
  __push_56_60(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_57;
int __counter_57 = 0;
int __steady_57 = 0;
int __tmp_57 = 0;
int __tmp2_57 = 0;
int *__state_flag_57 = NULL;
thread_info *__thread_57 = NULL;



float data__313__57 = 0.0f;
int i__314__57 = 0;
void save_peek_buffer__57(object_write_buffer *buf);
void load_peek_buffer__57(object_write_buffer *buf);
void save_file_pointer__57(object_write_buffer *buf);
void load_file_pointer__57(object_write_buffer *buf);

inline void check_status__57() {
  check_thread_status(__state_flag_57, __thread_57);
}

void check_status_during_io__57() {
  check_thread_status_during_io(__state_flag_57, __thread_57);
}

void __init_thread_info_57(thread_info *info) {
  __state_flag_57 = info->get_state_flag();
}

thread_info *__get_thread_info_57() {
  if (__thread_57 != NULL) return __thread_57;
  __thread_57 = new thread_info(57, check_status_during_io__57);
  __init_thread_info_57(__thread_57);
  return __thread_57;
}

void __declare_sockets_57() {
  init_instance::add_incoming(56,57, DATA_SOCKET);
  init_instance::add_outgoing(57,58, DATA_SOCKET);
}

void __init_sockets_57(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_57() {
}

void __peek_sockets_57() {
}

 
void init_DenomDataSum__318_120__57();
inline void check_status__57();

void work_DenomDataSum__318_120__57(int);


inline float __pop_56_57() {
float res=BUFFER_56_57[TAIL_56_57];
TAIL_56_57++;
return res;
}

inline void __pop_56_57(int n) {
TAIL_56_57+=n;
}

inline float __peek_56_57(int offs) {
return BUFFER_56_57[TAIL_56_57+offs];
}



inline void __push_57_58(float data) {
BUFFER_57_58[HEAD_57_58]=data;
HEAD_57_58++;
}



 
void init_DenomDataSum__318_120__57(){
}
void save_file_pointer__57(object_write_buffer *buf) {}
void load_file_pointer__57(object_write_buffer *buf) {}
 
void work_DenomDataSum__318_120__57(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__317 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__317 = ((float)0.0))/*float*/;
      for (((i__314__57) = 0)/*int*/; ((i__314__57) < 256); ((i__314__57)++)) {{
          (data__313__57) = BUFFER_56_57[TAIL_56_57]; TAIL_56_57++;
;
          (sum__317 = (sum__317 + ((data__313__57) * (data__313__57))))/*float*/;
        }
      }
      __push_57_58(sum__317);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_58;
int __counter_58 = 0;
int __steady_58 = 0;
int __tmp_58 = 0;
int __tmp2_58 = 0;
int *__state_flag_58 = NULL;
thread_info *__thread_58 = NULL;



inline void check_status__58() {
  check_thread_status(__state_flag_58, __thread_58);
}

void check_status_during_io__58() {
  check_thread_status_during_io(__state_flag_58, __thread_58);
}

void __init_thread_info_58(thread_info *info) {
  __state_flag_58 = info->get_state_flag();
}

thread_info *__get_thread_info_58() {
  if (__thread_58 != NULL) return __thread_58;
  __thread_58 = new thread_info(58, check_status_during_io__58);
  __init_thread_info_58(__thread_58);
  return __thread_58;
}

void __declare_sockets_58() {
  init_instance::add_incoming(57,58, DATA_SOCKET);
  init_instance::add_incoming(60,58, DATA_SOCKET);
  init_instance::add_outgoing(58,59, DATA_SOCKET);
}

void __init_sockets_58(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_58() {
}

void __peek_sockets_58() {
}


inline void __push_58_59(float data) {
BUFFER_58_59[HEAD_58_59]=data;
HEAD_58_59++;
}


inline float __pop_57_58() {
float res=BUFFER_57_58[TAIL_57_58];
TAIL_57_58++;
return res;
}

inline float __pop_60_58() {
float res=BUFFER_60_58[TAIL_60_58];
TAIL_60_58++;
return res;
}


void __joiner_58_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_58_59(__pop_57_58());
    __push_58_59(__pop_60_58());
    __push_58_59(__pop_60_58());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_59;
int __counter_59 = 0;
int __steady_59 = 0;
int __tmp_59 = 0;
int __tmp2_59 = 0;
int *__state_flag_59 = NULL;
thread_info *__thread_59 = NULL;



float numer__328__59 = 0.0f;
float denom_x__329__59 = 0.0f;
float denom_y__330__59 = 0.0f;
float denom__331__59 = 0.0f;
void save_peek_buffer__59(object_write_buffer *buf);
void load_peek_buffer__59(object_write_buffer *buf);
void save_file_pointer__59(object_write_buffer *buf);
void load_file_pointer__59(object_write_buffer *buf);

inline void check_status__59() {
  check_thread_status(__state_flag_59, __thread_59);
}

void check_status_during_io__59() {
  check_thread_status_during_io(__state_flag_59, __thread_59);
}

void __init_thread_info_59(thread_info *info) {
  __state_flag_59 = info->get_state_flag();
}

thread_info *__get_thread_info_59() {
  if (__thread_59 != NULL) return __thread_59;
  __thread_59 = new thread_info(59, check_status_during_io__59);
  __init_thread_info_59(__thread_59);
  return __thread_59;
}

void __declare_sockets_59() {
  init_instance::add_incoming(58,59, DATA_SOCKET);
  init_instance::add_outgoing(59,9, DATA_SOCKET);
}

void __init_sockets_59(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_59() {
}

void __peek_sockets_59() {
}

 
void init_Division__338_122__59();
inline void check_status__59();

void work_Division__338_122__59(int);


inline float __pop_58_59() {
float res=BUFFER_58_59[TAIL_58_59];
TAIL_58_59++;
return res;
}

inline void __pop_58_59(int n) {
TAIL_58_59+=n;
}

inline float __peek_58_59(int offs) {
return BUFFER_58_59[TAIL_58_59+offs];
}



inline void __push_59_9(float data) {
BUFFER_59_9[HEAD_59_9]=data;
HEAD_59_9++;
}



 
void init_Division__338_122__59(){
}
void save_file_pointer__59(object_write_buffer *buf) {}
void load_file_pointer__59(object_write_buffer *buf) {}
 
void work_Division__338_122__59(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__334 = 0.0f;/* float */
      float __tmp4__335 = 0.0f;/* float */
      double __tmp5__336 = 0.0f;/* double */
      double __tmp6__337 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__334 = ((float)0.0))/*float*/;
      (denom_x__329__59) = BUFFER_58_59[TAIL_58_59]; TAIL_58_59++;
;
      (denom_y__330__59) = BUFFER_58_59[TAIL_58_59]; TAIL_58_59++;
;
      (__tmp6__337 = ((double)(((denom_x__329__59) * (denom_y__330__59)))))/*double*/;
      (__tmp5__336 = sqrtf(__tmp6__337))/*double*/;
      (__tmp4__335 = ((float)(__tmp5__336)))/*float*/;
      ((denom__331__59) = __tmp4__335)/*float*/;
      (numer__328__59) = BUFFER_58_59[TAIL_58_59]; TAIL_58_59++;
;

      if (((denom__331__59) != ((float)0.0))) {(v__334 = ((numer__328__59) / (denom__331__59)))/*float*/;
      } else {}
      __push_59_9(v__334);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_6;
int __counter_6 = 0;
int __steady_6 = 0;
int __tmp_6 = 0;
int __tmp2_6 = 0;
int *__state_flag_6 = NULL;
thread_info *__thread_6 = NULL;



float data__26__6 = 0.0f;
int i__27__6 = 0;
void save_peek_buffer__6(object_write_buffer *buf);
void load_peek_buffer__6(object_write_buffer *buf);
void save_file_pointer__6(object_write_buffer *buf);
void load_file_pointer__6(object_write_buffer *buf);

inline void check_status__6() {
  check_thread_status(__state_flag_6, __thread_6);
}

void check_status_during_io__6() {
  check_thread_status_during_io(__state_flag_6, __thread_6);
}

void __init_thread_info_6(thread_info *info) {
  __state_flag_6 = info->get_state_flag();
}

thread_info *__get_thread_info_6() {
  if (__thread_6 != NULL) return __thread_6;
  __thread_6 = new thread_info(6, check_status_during_io__6);
  __init_thread_info_6(__thread_6);
  return __thread_6;
}

void __declare_sockets_6() {
  init_instance::add_incoming(5,6, DATA_SOCKET);
  init_instance::add_outgoing(6,7, DATA_SOCKET);
}

void __init_sockets_6(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_6() {
}

void __peek_sockets_6() {
}

 
void init_DenomDataSum__31_64__6();
inline void check_status__6();

void work_DenomDataSum__31_64__6(int);


inline float __pop_5_6() {
float res=BUFFER_5_6[TAIL_5_6];
TAIL_5_6++;
return res;
}

inline void __pop_5_6(int n) {
TAIL_5_6+=n;
}

inline float __peek_5_6(int offs) {
return BUFFER_5_6[TAIL_5_6+offs];
}



inline void __push_6_7(float data) {
BUFFER_6_7[HEAD_6_7]=data;
HEAD_6_7++;
}



 
void init_DenomDataSum__31_64__6(){
}
void save_file_pointer__6(object_write_buffer *buf) {}
void load_file_pointer__6(object_write_buffer *buf) {}
 
void work_DenomDataSum__31_64__6(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__30 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__30 = ((float)0.0))/*float*/;
      for (((i__27__6) = 0)/*int*/; ((i__27__6) < 256); ((i__27__6)++)) {{
          (data__26__6) = BUFFER_5_6[TAIL_5_6]; TAIL_5_6++;
;
          (sum__30 = (sum__30 + ((data__26__6) * (data__26__6))))/*float*/;
        }
      }
      __push_6_7(sum__30);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_60;
int __counter_60 = 0;
int __steady_60 = 0;
int __tmp_60 = 0;
int __tmp2_60 = 0;
int *__state_flag_60 = NULL;
thread_info *__thread_60 = NULL;



float _TheGlobal__pattern__6____60[256] = {0};
float numerSum__320__60 = 0.0f;
float denomPatternSum__321__60 = 0.0f;
float data__322__60 = 0.0f;
float patternData__323__60 = 0.0f;
int i__324__60 = 0;
void save_peek_buffer__60(object_write_buffer *buf);
void load_peek_buffer__60(object_write_buffer *buf);
void save_file_pointer__60(object_write_buffer *buf);
void load_file_pointer__60(object_write_buffer *buf);

inline void check_status__60() {
  check_thread_status(__state_flag_60, __thread_60);
}

void check_status_during_io__60() {
  check_thread_status_during_io(__state_flag_60, __thread_60);
}

void __init_thread_info_60(thread_info *info) {
  __state_flag_60 = info->get_state_flag();
}

thread_info *__get_thread_info_60() {
  if (__thread_60 != NULL) return __thread_60;
  __thread_60 = new thread_info(60, check_status_during_io__60);
  __init_thread_info_60(__thread_60);
  return __thread_60;
}

void __declare_sockets_60() {
  init_instance::add_incoming(56,60, DATA_SOCKET);
  init_instance::add_outgoing(60,58, DATA_SOCKET);
}

void __init_sockets_60(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_60() {
}

void __peek_sockets_60() {
}

 
void init_PatternSums__327_121__60();
inline void check_status__60();

void work_PatternSums__327_121__60(int);


inline float __pop_56_60() {
float res=BUFFER_56_60[TAIL_56_60];
TAIL_56_60++;
return res;
}

inline void __pop_56_60(int n) {
TAIL_56_60+=n;
}

inline float __peek_56_60(int offs) {
return BUFFER_56_60[TAIL_56_60+offs];
}



inline void __push_60_58(float data) {
BUFFER_60_58[HEAD_60_58]=data;
HEAD_60_58++;
}



 
void init_PatternSums__327_121__60(){
  (((_TheGlobal__pattern__6____60)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____60)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__60(object_write_buffer *buf) {}
void load_file_pointer__60(object_write_buffer *buf) {}
 
void work_PatternSums__327_121__60(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__320__60) = ((float)0.0))/*float*/;
      ((denomPatternSum__321__60) = ((float)0.0))/*float*/;
      for (((i__324__60) = 0)/*int*/; ((i__324__60) < 256); ((i__324__60)++)) {{
          ((patternData__323__60) = (((_TheGlobal__pattern__6____60)[(int)(i__324__60)]) - ((float)1.6013207E-4)))/*float*/;
          (data__322__60) = BUFFER_56_60[TAIL_56_60]; TAIL_56_60++;
;
          ((numerSum__320__60) = ((numerSum__320__60) + ((data__322__60) * (patternData__323__60))))/*float*/;
          ((denomPatternSum__321__60) = ((denomPatternSum__321__60) + ((patternData__323__60) * (patternData__323__60))))/*float*/;
        }
      }
      __push_60_58((denomPatternSum__321__60));
      __push_60_58((numerSum__320__60));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_61;
int __counter_61 = 0;
int __steady_61 = 0;
int __tmp_61 = 0;
int __tmp2_61 = 0;
int *__state_flag_61 = NULL;
thread_info *__thread_61 = NULL;



void save_peek_buffer__61(object_write_buffer *buf);
void load_peek_buffer__61(object_write_buffer *buf);
void save_file_pointer__61(object_write_buffer *buf);
void load_file_pointer__61(object_write_buffer *buf);

inline void check_status__61() {
  check_thread_status(__state_flag_61, __thread_61);
}

void check_status_during_io__61() {
  check_thread_status_during_io(__state_flag_61, __thread_61);
}

void __init_thread_info_61(thread_info *info) {
  __state_flag_61 = info->get_state_flag();
}

thread_info *__get_thread_info_61() {
  if (__thread_61 != NULL) return __thread_61;
  __thread_61 = new thread_info(61, check_status_during_io__61);
  __init_thread_info_61(__thread_61);
  return __thread_61;
}

void __declare_sockets_61() {
  init_instance::add_incoming(2,61, DATA_SOCKET);
  init_instance::add_outgoing(61,62, DATA_SOCKET);
}

void __init_sockets_61(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_61() {
}

void __peek_sockets_61() {
}

 
void init_GetAvg__345_124__61();
inline void check_status__61();

void work_GetAvg__345_124__61(int);


inline float __pop_2_61() {
float res=BUFFER_2_61[TAIL_2_61];
TAIL_2_61++;
return res;
}

inline void __pop_2_61(int n) {
TAIL_2_61+=n;
}

inline float __peek_2_61(int offs) {
return BUFFER_2_61[TAIL_2_61+offs];
}



inline void __push_61_62(float data) {
BUFFER_61_62[HEAD_61_62]=data;
HEAD_61_62++;
}



 
void init_GetAvg__345_124__61(){
}
void save_file_pointer__61(object_write_buffer *buf) {}
void load_file_pointer__61(object_write_buffer *buf) {}
 
void work_GetAvg__345_124__61(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__341 = 0;/* int */
      float Sum__342 = 0.0f;/* float */
      float val__343 = 0.0f;/* float */
      float __tmp8__344 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__342 = ((float)0.0))/*float*/;
      for ((i__341 = 0)/*int*/; (i__341 < 256); (i__341++)) {{
          val__343 = BUFFER_2_61[TAIL_2_61]; TAIL_2_61++;
;
          (Sum__342 = (Sum__342 + val__343))/*float*/;
          __push_61_62(val__343);
        }
      }
      (__tmp8__344 = (Sum__342 / ((float)256.0)))/*float*/;
      __push_61_62(__tmp8__344);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_62;
int __counter_62 = 0;
int __steady_62 = 0;
int __tmp_62 = 0;
int __tmp2_62 = 0;
int *__state_flag_62 = NULL;
thread_info *__thread_62 = NULL;



float avg__346__62 = 0.0f;
int i__347__62 = 0;
void save_peek_buffer__62(object_write_buffer *buf);
void load_peek_buffer__62(object_write_buffer *buf);
void save_file_pointer__62(object_write_buffer *buf);
void load_file_pointer__62(object_write_buffer *buf);

inline void check_status__62() {
  check_thread_status(__state_flag_62, __thread_62);
}

void check_status_during_io__62() {
  check_thread_status_during_io(__state_flag_62, __thread_62);
}

void __init_thread_info_62(thread_info *info) {
  __state_flag_62 = info->get_state_flag();
}

thread_info *__get_thread_info_62() {
  if (__thread_62 != NULL) return __thread_62;
  __thread_62 = new thread_info(62, check_status_during_io__62);
  __init_thread_info_62(__thread_62);
  return __thread_62;
}

void __declare_sockets_62() {
  init_instance::add_incoming(61,62, DATA_SOCKET);
  init_instance::add_outgoing(62,63, DATA_SOCKET);
}

void __init_sockets_62(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_62() {
}

void __peek_sockets_62() {
}

 
void init_DataMinusAvg__353_126__62();
inline void check_status__62();

void work_DataMinusAvg__353_126__62(int);


inline float __pop_61_62() {
float res=BUFFER_61_62[TAIL_61_62];
TAIL_61_62++;
return res;
}

inline void __pop_61_62(int n) {
TAIL_61_62+=n;
}

inline float __peek_61_62(int offs) {
return BUFFER_61_62[TAIL_61_62+offs];
}



inline void __push_62_63(float data) {
BUFFER_62_63[HEAD_62_63]=data;
HEAD_62_63++;
}



 
void init_DataMinusAvg__353_126__62(){
}
void save_file_pointer__62(object_write_buffer *buf) {}
void load_file_pointer__62(object_write_buffer *buf) {}
 
void work_DataMinusAvg__353_126__62(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__350 = 0.0f;/* float */
      float v__351 = 0.0f;/* float */
      float __tmp11__352 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__350 = BUFFER_61_62[TAIL_61_62+256];
;
      ((avg__346__62) = __tmp9__350)/*float*/;
      for (((i__347__62) = 0)/*int*/; ((i__347__62) < 256); ((i__347__62)++)) {{
          v__351 = BUFFER_61_62[TAIL_61_62]; TAIL_61_62++;
;
          (__tmp11__352 = (v__351 - (avg__346__62)))/*float*/;
          __push_62_63(__tmp11__352);
        }
      }
      __pop_61_62();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_63;
int __counter_63 = 0;
int __steady_63 = 0;
int __tmp_63 = 0;
int __tmp2_63 = 0;
int *__state_flag_63 = NULL;
thread_info *__thread_63 = NULL;



inline void check_status__63() {
  check_thread_status(__state_flag_63, __thread_63);
}

void check_status_during_io__63() {
  check_thread_status_during_io(__state_flag_63, __thread_63);
}

void __init_thread_info_63(thread_info *info) {
  __state_flag_63 = info->get_state_flag();
}

thread_info *__get_thread_info_63() {
  if (__thread_63 != NULL) return __thread_63;
  __thread_63 = new thread_info(63, check_status_during_io__63);
  __init_thread_info_63(__thread_63);
  return __thread_63;
}

void __declare_sockets_63() {
  init_instance::add_incoming(62,63, DATA_SOCKET);
  init_instance::add_outgoing(63,64, DATA_SOCKET);
  init_instance::add_outgoing(63,67, DATA_SOCKET);
}

void __init_sockets_63(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_63() {
}

void __peek_sockets_63() {
}

inline float __pop_62_63() {
float res=BUFFER_62_63[TAIL_62_63];
TAIL_62_63++;
return res;
}


inline void __push_63_64(float data) {
BUFFER_63_64[HEAD_63_64]=data;
HEAD_63_64++;
}



inline void __push_63_67(float data) {
BUFFER_63_67[HEAD_63_67]=data;
HEAD_63_67++;
}



void __splitter_63_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_62_63[TAIL_62_63]; TAIL_62_63++;
;
  __push_63_64(tmp);
  __push_63_67(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_64;
int __counter_64 = 0;
int __steady_64 = 0;
int __tmp_64 = 0;
int __tmp2_64 = 0;
int *__state_flag_64 = NULL;
thread_info *__thread_64 = NULL;



float data__354__64 = 0.0f;
int i__355__64 = 0;
void save_peek_buffer__64(object_write_buffer *buf);
void load_peek_buffer__64(object_write_buffer *buf);
void save_file_pointer__64(object_write_buffer *buf);
void load_file_pointer__64(object_write_buffer *buf);

inline void check_status__64() {
  check_thread_status(__state_flag_64, __thread_64);
}

void check_status_during_io__64() {
  check_thread_status_during_io(__state_flag_64, __thread_64);
}

void __init_thread_info_64(thread_info *info) {
  __state_flag_64 = info->get_state_flag();
}

thread_info *__get_thread_info_64() {
  if (__thread_64 != NULL) return __thread_64;
  __thread_64 = new thread_info(64, check_status_during_io__64);
  __init_thread_info_64(__thread_64);
  return __thread_64;
}

void __declare_sockets_64() {
  init_instance::add_incoming(63,64, DATA_SOCKET);
  init_instance::add_outgoing(64,65, DATA_SOCKET);
}

void __init_sockets_64(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_64() {
}

void __peek_sockets_64() {
}

 
void init_DenomDataSum__359_128__64();
inline void check_status__64();

void work_DenomDataSum__359_128__64(int);


inline float __pop_63_64() {
float res=BUFFER_63_64[TAIL_63_64];
TAIL_63_64++;
return res;
}

inline void __pop_63_64(int n) {
TAIL_63_64+=n;
}

inline float __peek_63_64(int offs) {
return BUFFER_63_64[TAIL_63_64+offs];
}



inline void __push_64_65(float data) {
BUFFER_64_65[HEAD_64_65]=data;
HEAD_64_65++;
}



 
void init_DenomDataSum__359_128__64(){
}
void save_file_pointer__64(object_write_buffer *buf) {}
void load_file_pointer__64(object_write_buffer *buf) {}
 
void work_DenomDataSum__359_128__64(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__358 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__358 = ((float)0.0))/*float*/;
      for (((i__355__64) = 0)/*int*/; ((i__355__64) < 256); ((i__355__64)++)) {{
          (data__354__64) = BUFFER_63_64[TAIL_63_64]; TAIL_63_64++;
;
          (sum__358 = (sum__358 + ((data__354__64) * (data__354__64))))/*float*/;
        }
      }
      __push_64_65(sum__358);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_65;
int __counter_65 = 0;
int __steady_65 = 0;
int __tmp_65 = 0;
int __tmp2_65 = 0;
int *__state_flag_65 = NULL;
thread_info *__thread_65 = NULL;



inline void check_status__65() {
  check_thread_status(__state_flag_65, __thread_65);
}

void check_status_during_io__65() {
  check_thread_status_during_io(__state_flag_65, __thread_65);
}

void __init_thread_info_65(thread_info *info) {
  __state_flag_65 = info->get_state_flag();
}

thread_info *__get_thread_info_65() {
  if (__thread_65 != NULL) return __thread_65;
  __thread_65 = new thread_info(65, check_status_during_io__65);
  __init_thread_info_65(__thread_65);
  return __thread_65;
}

void __declare_sockets_65() {
  init_instance::add_incoming(64,65, DATA_SOCKET);
  init_instance::add_incoming(67,65, DATA_SOCKET);
  init_instance::add_outgoing(65,66, DATA_SOCKET);
}

void __init_sockets_65(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_65() {
}

void __peek_sockets_65() {
}


inline void __push_65_66(float data) {
BUFFER_65_66[HEAD_65_66]=data;
HEAD_65_66++;
}


inline float __pop_64_65() {
float res=BUFFER_64_65[TAIL_64_65];
TAIL_64_65++;
return res;
}

inline float __pop_67_65() {
float res=BUFFER_67_65[TAIL_67_65];
TAIL_67_65++;
return res;
}


void __joiner_65_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_65_66(__pop_64_65());
    __push_65_66(__pop_67_65());
    __push_65_66(__pop_67_65());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_66;
int __counter_66 = 0;
int __steady_66 = 0;
int __tmp_66 = 0;
int __tmp2_66 = 0;
int *__state_flag_66 = NULL;
thread_info *__thread_66 = NULL;



float numer__369__66 = 0.0f;
float denom_x__370__66 = 0.0f;
float denom_y__371__66 = 0.0f;
float denom__372__66 = 0.0f;
void save_peek_buffer__66(object_write_buffer *buf);
void load_peek_buffer__66(object_write_buffer *buf);
void save_file_pointer__66(object_write_buffer *buf);
void load_file_pointer__66(object_write_buffer *buf);

inline void check_status__66() {
  check_thread_status(__state_flag_66, __thread_66);
}

void check_status_during_io__66() {
  check_thread_status_during_io(__state_flag_66, __thread_66);
}

void __init_thread_info_66(thread_info *info) {
  __state_flag_66 = info->get_state_flag();
}

thread_info *__get_thread_info_66() {
  if (__thread_66 != NULL) return __thread_66;
  __thread_66 = new thread_info(66, check_status_during_io__66);
  __init_thread_info_66(__thread_66);
  return __thread_66;
}

void __declare_sockets_66() {
  init_instance::add_incoming(65,66, DATA_SOCKET);
  init_instance::add_outgoing(66,9, DATA_SOCKET);
}

void __init_sockets_66(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_66() {
}

void __peek_sockets_66() {
}

 
void init_Division__379_130__66();
inline void check_status__66();

void work_Division__379_130__66(int);


inline float __pop_65_66() {
float res=BUFFER_65_66[TAIL_65_66];
TAIL_65_66++;
return res;
}

inline void __pop_65_66(int n) {
TAIL_65_66+=n;
}

inline float __peek_65_66(int offs) {
return BUFFER_65_66[TAIL_65_66+offs];
}



inline void __push_66_9(float data) {
BUFFER_66_9[HEAD_66_9]=data;
HEAD_66_9++;
}



 
void init_Division__379_130__66(){
}
void save_file_pointer__66(object_write_buffer *buf) {}
void load_file_pointer__66(object_write_buffer *buf) {}
 
void work_Division__379_130__66(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__375 = 0.0f;/* float */
      float __tmp4__376 = 0.0f;/* float */
      double __tmp5__377 = 0.0f;/* double */
      double __tmp6__378 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__375 = ((float)0.0))/*float*/;
      (denom_x__370__66) = BUFFER_65_66[TAIL_65_66]; TAIL_65_66++;
;
      (denom_y__371__66) = BUFFER_65_66[TAIL_65_66]; TAIL_65_66++;
;
      (__tmp6__378 = ((double)(((denom_x__370__66) * (denom_y__371__66)))))/*double*/;
      (__tmp5__377 = sqrtf(__tmp6__378))/*double*/;
      (__tmp4__376 = ((float)(__tmp5__377)))/*float*/;
      ((denom__372__66) = __tmp4__376)/*float*/;
      (numer__369__66) = BUFFER_65_66[TAIL_65_66]; TAIL_65_66++;
;

      if (((denom__372__66) != ((float)0.0))) {(v__375 = ((numer__369__66) / (denom__372__66)))/*float*/;
      } else {}
      __push_66_9(v__375);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_67;
int __counter_67 = 0;
int __steady_67 = 0;
int __tmp_67 = 0;
int __tmp2_67 = 0;
int *__state_flag_67 = NULL;
thread_info *__thread_67 = NULL;



float _TheGlobal__pattern__6____67[256] = {0};
float numerSum__361__67 = 0.0f;
float denomPatternSum__362__67 = 0.0f;
float data__363__67 = 0.0f;
float patternData__364__67 = 0.0f;
int i__365__67 = 0;
void save_peek_buffer__67(object_write_buffer *buf);
void load_peek_buffer__67(object_write_buffer *buf);
void save_file_pointer__67(object_write_buffer *buf);
void load_file_pointer__67(object_write_buffer *buf);

inline void check_status__67() {
  check_thread_status(__state_flag_67, __thread_67);
}

void check_status_during_io__67() {
  check_thread_status_during_io(__state_flag_67, __thread_67);
}

void __init_thread_info_67(thread_info *info) {
  __state_flag_67 = info->get_state_flag();
}

thread_info *__get_thread_info_67() {
  if (__thread_67 != NULL) return __thread_67;
  __thread_67 = new thread_info(67, check_status_during_io__67);
  __init_thread_info_67(__thread_67);
  return __thread_67;
}

void __declare_sockets_67() {
  init_instance::add_incoming(63,67, DATA_SOCKET);
  init_instance::add_outgoing(67,65, DATA_SOCKET);
}

void __init_sockets_67(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_67() {
}

void __peek_sockets_67() {
}

 
void init_PatternSums__368_129__67();
inline void check_status__67();

void work_PatternSums__368_129__67(int);


inline float __pop_63_67() {
float res=BUFFER_63_67[TAIL_63_67];
TAIL_63_67++;
return res;
}

inline void __pop_63_67(int n) {
TAIL_63_67+=n;
}

inline float __peek_63_67(int offs) {
return BUFFER_63_67[TAIL_63_67+offs];
}



inline void __push_67_65(float data) {
BUFFER_67_65[HEAD_67_65]=data;
HEAD_67_65++;
}



 
void init_PatternSums__368_129__67(){
  (((_TheGlobal__pattern__6____67)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____67)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__67(object_write_buffer *buf) {}
void load_file_pointer__67(object_write_buffer *buf) {}
 
void work_PatternSums__368_129__67(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__361__67) = ((float)0.0))/*float*/;
      ((denomPatternSum__362__67) = ((float)0.0))/*float*/;
      for (((i__365__67) = 0)/*int*/; ((i__365__67) < 256); ((i__365__67)++)) {{
          ((patternData__364__67) = (((_TheGlobal__pattern__6____67)[(int)(i__365__67)]) - ((float)1.6013207E-4)))/*float*/;
          (data__363__67) = BUFFER_63_67[TAIL_63_67]; TAIL_63_67++;
;
          ((numerSum__361__67) = ((numerSum__361__67) + ((data__363__67) * (patternData__364__67))))/*float*/;
          ((denomPatternSum__362__67) = ((denomPatternSum__362__67) + ((patternData__364__67) * (patternData__364__67))))/*float*/;
        }
      }
      __push_67_65((denomPatternSum__362__67));
      __push_67_65((numerSum__361__67));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_68;
int __counter_68 = 0;
int __steady_68 = 0;
int __tmp_68 = 0;
int __tmp2_68 = 0;
int *__state_flag_68 = NULL;
thread_info *__thread_68 = NULL;



void save_peek_buffer__68(object_write_buffer *buf);
void load_peek_buffer__68(object_write_buffer *buf);
void save_file_pointer__68(object_write_buffer *buf);
void load_file_pointer__68(object_write_buffer *buf);

inline void check_status__68() {
  check_thread_status(__state_flag_68, __thread_68);
}

void check_status_during_io__68() {
  check_thread_status_during_io(__state_flag_68, __thread_68);
}

void __init_thread_info_68(thread_info *info) {
  __state_flag_68 = info->get_state_flag();
}

thread_info *__get_thread_info_68() {
  if (__thread_68 != NULL) return __thread_68;
  __thread_68 = new thread_info(68, check_status_during_io__68);
  __init_thread_info_68(__thread_68);
  return __thread_68;
}

void __declare_sockets_68() {
  init_instance::add_incoming(2,68, DATA_SOCKET);
  init_instance::add_outgoing(68,69, DATA_SOCKET);
}

void __init_sockets_68(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_68() {
}

void __peek_sockets_68() {
}

 
void init_GetAvg__386_132__68();
inline void check_status__68();

void work_GetAvg__386_132__68(int);


inline float __pop_2_68() {
float res=BUFFER_2_68[TAIL_2_68];
TAIL_2_68++;
return res;
}

inline void __pop_2_68(int n) {
TAIL_2_68+=n;
}

inline float __peek_2_68(int offs) {
return BUFFER_2_68[TAIL_2_68+offs];
}



inline void __push_68_69(float data) {
BUFFER_68_69[HEAD_68_69]=data;
HEAD_68_69++;
}



 
void init_GetAvg__386_132__68(){
}
void save_file_pointer__68(object_write_buffer *buf) {}
void load_file_pointer__68(object_write_buffer *buf) {}
 
void work_GetAvg__386_132__68(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__382 = 0;/* int */
      float Sum__383 = 0.0f;/* float */
      float val__384 = 0.0f;/* float */
      float __tmp8__385 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__383 = ((float)0.0))/*float*/;
      for ((i__382 = 0)/*int*/; (i__382 < 256); (i__382++)) {{
          val__384 = BUFFER_2_68[TAIL_2_68]; TAIL_2_68++;
;
          (Sum__383 = (Sum__383 + val__384))/*float*/;
          __push_68_69(val__384);
        }
      }
      (__tmp8__385 = (Sum__383 / ((float)256.0)))/*float*/;
      __push_68_69(__tmp8__385);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_69;
int __counter_69 = 0;
int __steady_69 = 0;
int __tmp_69 = 0;
int __tmp2_69 = 0;
int *__state_flag_69 = NULL;
thread_info *__thread_69 = NULL;



float avg__387__69 = 0.0f;
int i__388__69 = 0;
void save_peek_buffer__69(object_write_buffer *buf);
void load_peek_buffer__69(object_write_buffer *buf);
void save_file_pointer__69(object_write_buffer *buf);
void load_file_pointer__69(object_write_buffer *buf);

inline void check_status__69() {
  check_thread_status(__state_flag_69, __thread_69);
}

void check_status_during_io__69() {
  check_thread_status_during_io(__state_flag_69, __thread_69);
}

void __init_thread_info_69(thread_info *info) {
  __state_flag_69 = info->get_state_flag();
}

thread_info *__get_thread_info_69() {
  if (__thread_69 != NULL) return __thread_69;
  __thread_69 = new thread_info(69, check_status_during_io__69);
  __init_thread_info_69(__thread_69);
  return __thread_69;
}

void __declare_sockets_69() {
  init_instance::add_incoming(68,69, DATA_SOCKET);
  init_instance::add_outgoing(69,70, DATA_SOCKET);
}

void __init_sockets_69(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_69() {
}

void __peek_sockets_69() {
}

 
void init_DataMinusAvg__394_134__69();
inline void check_status__69();

void work_DataMinusAvg__394_134__69(int);


inline float __pop_68_69() {
float res=BUFFER_68_69[TAIL_68_69];
TAIL_68_69++;
return res;
}

inline void __pop_68_69(int n) {
TAIL_68_69+=n;
}

inline float __peek_68_69(int offs) {
return BUFFER_68_69[TAIL_68_69+offs];
}



inline void __push_69_70(float data) {
BUFFER_69_70[HEAD_69_70]=data;
HEAD_69_70++;
}



 
void init_DataMinusAvg__394_134__69(){
}
void save_file_pointer__69(object_write_buffer *buf) {}
void load_file_pointer__69(object_write_buffer *buf) {}
 
void work_DataMinusAvg__394_134__69(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__391 = 0.0f;/* float */
      float v__392 = 0.0f;/* float */
      float __tmp11__393 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__391 = BUFFER_68_69[TAIL_68_69+256];
;
      ((avg__387__69) = __tmp9__391)/*float*/;
      for (((i__388__69) = 0)/*int*/; ((i__388__69) < 256); ((i__388__69)++)) {{
          v__392 = BUFFER_68_69[TAIL_68_69]; TAIL_68_69++;
;
          (__tmp11__393 = (v__392 - (avg__387__69)))/*float*/;
          __push_69_70(__tmp11__393);
        }
      }
      __pop_68_69();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_7;
int __counter_7 = 0;
int __steady_7 = 0;
int __tmp_7 = 0;
int __tmp2_7 = 0;
int *__state_flag_7 = NULL;
thread_info *__thread_7 = NULL;



inline void check_status__7() {
  check_thread_status(__state_flag_7, __thread_7);
}

void check_status_during_io__7() {
  check_thread_status_during_io(__state_flag_7, __thread_7);
}

void __init_thread_info_7(thread_info *info) {
  __state_flag_7 = info->get_state_flag();
}

thread_info *__get_thread_info_7() {
  if (__thread_7 != NULL) return __thread_7;
  __thread_7 = new thread_info(7, check_status_during_io__7);
  __init_thread_info_7(__thread_7);
  return __thread_7;
}

void __declare_sockets_7() {
  init_instance::add_incoming(6,7, DATA_SOCKET);
  init_instance::add_incoming(11,7, DATA_SOCKET);
  init_instance::add_outgoing(7,8, DATA_SOCKET);
}

void __init_sockets_7(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_7() {
}

void __peek_sockets_7() {
}


inline void __push_7_8(float data) {
BUFFER_7_8[HEAD_7_8]=data;
HEAD_7_8++;
}


inline float __pop_6_7() {
float res=BUFFER_6_7[TAIL_6_7];
TAIL_6_7++;
return res;
}

inline float __pop_11_7() {
float res=BUFFER_11_7[TAIL_11_7];
TAIL_11_7++;
return res;
}


void __joiner_7_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_7_8(__pop_6_7());
    __push_7_8(__pop_11_7());
    __push_7_8(__pop_11_7());
  }
}


// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_70;
int __counter_70 = 0;
int __steady_70 = 0;
int __tmp_70 = 0;
int __tmp2_70 = 0;
int *__state_flag_70 = NULL;
thread_info *__thread_70 = NULL;



inline void check_status__70() {
  check_thread_status(__state_flag_70, __thread_70);
}

void check_status_during_io__70() {
  check_thread_status_during_io(__state_flag_70, __thread_70);
}

void __init_thread_info_70(thread_info *info) {
  __state_flag_70 = info->get_state_flag();
}

thread_info *__get_thread_info_70() {
  if (__thread_70 != NULL) return __thread_70;
  __thread_70 = new thread_info(70, check_status_during_io__70);
  __init_thread_info_70(__thread_70);
  return __thread_70;
}

void __declare_sockets_70() {
  init_instance::add_incoming(69,70, DATA_SOCKET);
  init_instance::add_outgoing(70,71, DATA_SOCKET);
  init_instance::add_outgoing(70,74, DATA_SOCKET);
}

void __init_sockets_70(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_70() {
}

void __peek_sockets_70() {
}

inline float __pop_69_70() {
float res=BUFFER_69_70[TAIL_69_70];
TAIL_69_70++;
return res;
}


inline void __push_70_71(float data) {
BUFFER_70_71[HEAD_70_71]=data;
HEAD_70_71++;
}



inline void __push_70_74(float data) {
BUFFER_70_74[HEAD_70_74]=data;
HEAD_70_74++;
}



void __splitter_70_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_69_70[TAIL_69_70]; TAIL_69_70++;
;
  __push_70_71(tmp);
  __push_70_74(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_71;
int __counter_71 = 0;
int __steady_71 = 0;
int __tmp_71 = 0;
int __tmp2_71 = 0;
int *__state_flag_71 = NULL;
thread_info *__thread_71 = NULL;



float data__395__71 = 0.0f;
int i__396__71 = 0;
void save_peek_buffer__71(object_write_buffer *buf);
void load_peek_buffer__71(object_write_buffer *buf);
void save_file_pointer__71(object_write_buffer *buf);
void load_file_pointer__71(object_write_buffer *buf);

inline void check_status__71() {
  check_thread_status(__state_flag_71, __thread_71);
}

void check_status_during_io__71() {
  check_thread_status_during_io(__state_flag_71, __thread_71);
}

void __init_thread_info_71(thread_info *info) {
  __state_flag_71 = info->get_state_flag();
}

thread_info *__get_thread_info_71() {
  if (__thread_71 != NULL) return __thread_71;
  __thread_71 = new thread_info(71, check_status_during_io__71);
  __init_thread_info_71(__thread_71);
  return __thread_71;
}

void __declare_sockets_71() {
  init_instance::add_incoming(70,71, DATA_SOCKET);
  init_instance::add_outgoing(71,72, DATA_SOCKET);
}

void __init_sockets_71(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_71() {
}

void __peek_sockets_71() {
}

 
void init_DenomDataSum__400_136__71();
inline void check_status__71();

void work_DenomDataSum__400_136__71(int);


inline float __pop_70_71() {
float res=BUFFER_70_71[TAIL_70_71];
TAIL_70_71++;
return res;
}

inline void __pop_70_71(int n) {
TAIL_70_71+=n;
}

inline float __peek_70_71(int offs) {
return BUFFER_70_71[TAIL_70_71+offs];
}



inline void __push_71_72(float data) {
BUFFER_71_72[HEAD_71_72]=data;
HEAD_71_72++;
}



 
void init_DenomDataSum__400_136__71(){
}
void save_file_pointer__71(object_write_buffer *buf) {}
void load_file_pointer__71(object_write_buffer *buf) {}
 
void work_DenomDataSum__400_136__71(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__399 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__399 = ((float)0.0))/*float*/;
      for (((i__396__71) = 0)/*int*/; ((i__396__71) < 256); ((i__396__71)++)) {{
          (data__395__71) = BUFFER_70_71[TAIL_70_71]; TAIL_70_71++;
;
          (sum__399 = (sum__399 + ((data__395__71) * (data__395__71))))/*float*/;
        }
      }
      __push_71_72(sum__399);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_72;
int __counter_72 = 0;
int __steady_72 = 0;
int __tmp_72 = 0;
int __tmp2_72 = 0;
int *__state_flag_72 = NULL;
thread_info *__thread_72 = NULL;



inline void check_status__72() {
  check_thread_status(__state_flag_72, __thread_72);
}

void check_status_during_io__72() {
  check_thread_status_during_io(__state_flag_72, __thread_72);
}

void __init_thread_info_72(thread_info *info) {
  __state_flag_72 = info->get_state_flag();
}

thread_info *__get_thread_info_72() {
  if (__thread_72 != NULL) return __thread_72;
  __thread_72 = new thread_info(72, check_status_during_io__72);
  __init_thread_info_72(__thread_72);
  return __thread_72;
}

void __declare_sockets_72() {
  init_instance::add_incoming(71,72, DATA_SOCKET);
  init_instance::add_incoming(74,72, DATA_SOCKET);
  init_instance::add_outgoing(72,73, DATA_SOCKET);
}

void __init_sockets_72(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_72() {
}

void __peek_sockets_72() {
}


inline void __push_72_73(float data) {
BUFFER_72_73[HEAD_72_73]=data;
HEAD_72_73++;
}


inline float __pop_71_72() {
float res=BUFFER_71_72[TAIL_71_72];
TAIL_71_72++;
return res;
}

inline float __pop_74_72() {
float res=BUFFER_74_72[TAIL_74_72];
TAIL_74_72++;
return res;
}


void __joiner_72_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_72_73(__pop_71_72());
    __push_72_73(__pop_74_72());
    __push_72_73(__pop_74_72());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_73;
int __counter_73 = 0;
int __steady_73 = 0;
int __tmp_73 = 0;
int __tmp2_73 = 0;
int *__state_flag_73 = NULL;
thread_info *__thread_73 = NULL;



float numer__410__73 = 0.0f;
float denom_x__411__73 = 0.0f;
float denom_y__412__73 = 0.0f;
float denom__413__73 = 0.0f;
void save_peek_buffer__73(object_write_buffer *buf);
void load_peek_buffer__73(object_write_buffer *buf);
void save_file_pointer__73(object_write_buffer *buf);
void load_file_pointer__73(object_write_buffer *buf);

inline void check_status__73() {
  check_thread_status(__state_flag_73, __thread_73);
}

void check_status_during_io__73() {
  check_thread_status_during_io(__state_flag_73, __thread_73);
}

void __init_thread_info_73(thread_info *info) {
  __state_flag_73 = info->get_state_flag();
}

thread_info *__get_thread_info_73() {
  if (__thread_73 != NULL) return __thread_73;
  __thread_73 = new thread_info(73, check_status_during_io__73);
  __init_thread_info_73(__thread_73);
  return __thread_73;
}

void __declare_sockets_73() {
  init_instance::add_incoming(72,73, DATA_SOCKET);
  init_instance::add_outgoing(73,9, DATA_SOCKET);
}

void __init_sockets_73(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_73() {
}

void __peek_sockets_73() {
}

 
void init_Division__420_138__73();
inline void check_status__73();

void work_Division__420_138__73(int);


inline float __pop_72_73() {
float res=BUFFER_72_73[TAIL_72_73];
TAIL_72_73++;
return res;
}

inline void __pop_72_73(int n) {
TAIL_72_73+=n;
}

inline float __peek_72_73(int offs) {
return BUFFER_72_73[TAIL_72_73+offs];
}



inline void __push_73_9(float data) {
BUFFER_73_9[HEAD_73_9]=data;
HEAD_73_9++;
}



 
void init_Division__420_138__73(){
}
void save_file_pointer__73(object_write_buffer *buf) {}
void load_file_pointer__73(object_write_buffer *buf) {}
 
void work_Division__420_138__73(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__416 = 0.0f;/* float */
      float __tmp4__417 = 0.0f;/* float */
      double __tmp5__418 = 0.0f;/* double */
      double __tmp6__419 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__416 = ((float)0.0))/*float*/;
      (denom_x__411__73) = BUFFER_72_73[TAIL_72_73]; TAIL_72_73++;
;
      (denom_y__412__73) = BUFFER_72_73[TAIL_72_73]; TAIL_72_73++;
;
      (__tmp6__419 = ((double)(((denom_x__411__73) * (denom_y__412__73)))))/*double*/;
      (__tmp5__418 = sqrtf(__tmp6__419))/*double*/;
      (__tmp4__417 = ((float)(__tmp5__418)))/*float*/;
      ((denom__413__73) = __tmp4__417)/*float*/;
      (numer__410__73) = BUFFER_72_73[TAIL_72_73]; TAIL_72_73++;
;

      if (((denom__413__73) != ((float)0.0))) {(v__416 = ((numer__410__73) / (denom__413__73)))/*float*/;
      } else {}
      __push_73_9(v__416);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_74;
int __counter_74 = 0;
int __steady_74 = 0;
int __tmp_74 = 0;
int __tmp2_74 = 0;
int *__state_flag_74 = NULL;
thread_info *__thread_74 = NULL;



float _TheGlobal__pattern__6____74[256] = {0};
float numerSum__402__74 = 0.0f;
float denomPatternSum__403__74 = 0.0f;
float data__404__74 = 0.0f;
float patternData__405__74 = 0.0f;
int i__406__74 = 0;
void save_peek_buffer__74(object_write_buffer *buf);
void load_peek_buffer__74(object_write_buffer *buf);
void save_file_pointer__74(object_write_buffer *buf);
void load_file_pointer__74(object_write_buffer *buf);

inline void check_status__74() {
  check_thread_status(__state_flag_74, __thread_74);
}

void check_status_during_io__74() {
  check_thread_status_during_io(__state_flag_74, __thread_74);
}

void __init_thread_info_74(thread_info *info) {
  __state_flag_74 = info->get_state_flag();
}

thread_info *__get_thread_info_74() {
  if (__thread_74 != NULL) return __thread_74;
  __thread_74 = new thread_info(74, check_status_during_io__74);
  __init_thread_info_74(__thread_74);
  return __thread_74;
}

void __declare_sockets_74() {
  init_instance::add_incoming(70,74, DATA_SOCKET);
  init_instance::add_outgoing(74,72, DATA_SOCKET);
}

void __init_sockets_74(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_74() {
}

void __peek_sockets_74() {
}

 
void init_PatternSums__409_137__74();
inline void check_status__74();

void work_PatternSums__409_137__74(int);


inline float __pop_70_74() {
float res=BUFFER_70_74[TAIL_70_74];
TAIL_70_74++;
return res;
}

inline void __pop_70_74(int n) {
TAIL_70_74+=n;
}

inline float __peek_70_74(int offs) {
return BUFFER_70_74[TAIL_70_74+offs];
}



inline void __push_74_72(float data) {
BUFFER_74_72[HEAD_74_72]=data;
HEAD_74_72++;
}



 
void init_PatternSums__409_137__74(){
  (((_TheGlobal__pattern__6____74)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____74)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__74(object_write_buffer *buf) {}
void load_file_pointer__74(object_write_buffer *buf) {}
 
void work_PatternSums__409_137__74(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__402__74) = ((float)0.0))/*float*/;
      ((denomPatternSum__403__74) = ((float)0.0))/*float*/;
      for (((i__406__74) = 0)/*int*/; ((i__406__74) < 256); ((i__406__74)++)) {{
          ((patternData__405__74) = (((_TheGlobal__pattern__6____74)[(int)(i__406__74)]) - ((float)1.6013207E-4)))/*float*/;
          (data__404__74) = BUFFER_70_74[TAIL_70_74]; TAIL_70_74++;
;
          ((numerSum__402__74) = ((numerSum__402__74) + ((data__404__74) * (patternData__405__74))))/*float*/;
          ((denomPatternSum__403__74) = ((denomPatternSum__403__74) + ((patternData__405__74) * (patternData__405__74))))/*float*/;
        }
      }
      __push_74_72((denomPatternSum__403__74));
      __push_74_72((numerSum__402__74));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_75;
int __counter_75 = 0;
int __steady_75 = 0;
int __tmp_75 = 0;
int __tmp2_75 = 0;
int *__state_flag_75 = NULL;
thread_info *__thread_75 = NULL;



void save_peek_buffer__75(object_write_buffer *buf);
void load_peek_buffer__75(object_write_buffer *buf);
void save_file_pointer__75(object_write_buffer *buf);
void load_file_pointer__75(object_write_buffer *buf);

inline void check_status__75() {
  check_thread_status(__state_flag_75, __thread_75);
}

void check_status_during_io__75() {
  check_thread_status_during_io(__state_flag_75, __thread_75);
}

void __init_thread_info_75(thread_info *info) {
  __state_flag_75 = info->get_state_flag();
}

thread_info *__get_thread_info_75() {
  if (__thread_75 != NULL) return __thread_75;
  __thread_75 = new thread_info(75, check_status_during_io__75);
  __init_thread_info_75(__thread_75);
  return __thread_75;
}

void __declare_sockets_75() {
  init_instance::add_incoming(2,75, DATA_SOCKET);
  init_instance::add_outgoing(75,76, DATA_SOCKET);
}

void __init_sockets_75(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_75() {
}

void __peek_sockets_75() {
}

 
void init_GetAvg__427_140__75();
inline void check_status__75();

void work_GetAvg__427_140__75(int);


inline float __pop_2_75() {
float res=BUFFER_2_75[TAIL_2_75];
TAIL_2_75++;
return res;
}

inline void __pop_2_75(int n) {
TAIL_2_75+=n;
}

inline float __peek_2_75(int offs) {
return BUFFER_2_75[TAIL_2_75+offs];
}



inline void __push_75_76(float data) {
BUFFER_75_76[HEAD_75_76]=data;
HEAD_75_76++;
}



 
void init_GetAvg__427_140__75(){
}
void save_file_pointer__75(object_write_buffer *buf) {}
void load_file_pointer__75(object_write_buffer *buf) {}
 
void work_GetAvg__427_140__75(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__423 = 0;/* int */
      float Sum__424 = 0.0f;/* float */
      float val__425 = 0.0f;/* float */
      float __tmp8__426 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__424 = ((float)0.0))/*float*/;
      for ((i__423 = 0)/*int*/; (i__423 < 256); (i__423++)) {{
          val__425 = BUFFER_2_75[TAIL_2_75]; TAIL_2_75++;
;
          (Sum__424 = (Sum__424 + val__425))/*float*/;
          __push_75_76(val__425);
        }
      }
      (__tmp8__426 = (Sum__424 / ((float)256.0)))/*float*/;
      __push_75_76(__tmp8__426);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_76;
int __counter_76 = 0;
int __steady_76 = 0;
int __tmp_76 = 0;
int __tmp2_76 = 0;
int *__state_flag_76 = NULL;
thread_info *__thread_76 = NULL;



float avg__428__76 = 0.0f;
int i__429__76 = 0;
void save_peek_buffer__76(object_write_buffer *buf);
void load_peek_buffer__76(object_write_buffer *buf);
void save_file_pointer__76(object_write_buffer *buf);
void load_file_pointer__76(object_write_buffer *buf);

inline void check_status__76() {
  check_thread_status(__state_flag_76, __thread_76);
}

void check_status_during_io__76() {
  check_thread_status_during_io(__state_flag_76, __thread_76);
}

void __init_thread_info_76(thread_info *info) {
  __state_flag_76 = info->get_state_flag();
}

thread_info *__get_thread_info_76() {
  if (__thread_76 != NULL) return __thread_76;
  __thread_76 = new thread_info(76, check_status_during_io__76);
  __init_thread_info_76(__thread_76);
  return __thread_76;
}

void __declare_sockets_76() {
  init_instance::add_incoming(75,76, DATA_SOCKET);
  init_instance::add_outgoing(76,77, DATA_SOCKET);
}

void __init_sockets_76(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_76() {
}

void __peek_sockets_76() {
}

 
void init_DataMinusAvg__435_142__76();
inline void check_status__76();

void work_DataMinusAvg__435_142__76(int);


inline float __pop_75_76() {
float res=BUFFER_75_76[TAIL_75_76];
TAIL_75_76++;
return res;
}

inline void __pop_75_76(int n) {
TAIL_75_76+=n;
}

inline float __peek_75_76(int offs) {
return BUFFER_75_76[TAIL_75_76+offs];
}



inline void __push_76_77(float data) {
BUFFER_76_77[HEAD_76_77]=data;
HEAD_76_77++;
}



 
void init_DataMinusAvg__435_142__76(){
}
void save_file_pointer__76(object_write_buffer *buf) {}
void load_file_pointer__76(object_write_buffer *buf) {}
 
void work_DataMinusAvg__435_142__76(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__432 = 0.0f;/* float */
      float v__433 = 0.0f;/* float */
      float __tmp11__434 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__432 = BUFFER_75_76[TAIL_75_76+256];
;
      ((avg__428__76) = __tmp9__432)/*float*/;
      for (((i__429__76) = 0)/*int*/; ((i__429__76) < 256); ((i__429__76)++)) {{
          v__433 = BUFFER_75_76[TAIL_75_76]; TAIL_75_76++;
;
          (__tmp11__434 = (v__433 - (avg__428__76)))/*float*/;
          __push_76_77(__tmp11__434);
        }
      }
      __pop_75_76();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_77;
int __counter_77 = 0;
int __steady_77 = 0;
int __tmp_77 = 0;
int __tmp2_77 = 0;
int *__state_flag_77 = NULL;
thread_info *__thread_77 = NULL;



inline void check_status__77() {
  check_thread_status(__state_flag_77, __thread_77);
}

void check_status_during_io__77() {
  check_thread_status_during_io(__state_flag_77, __thread_77);
}

void __init_thread_info_77(thread_info *info) {
  __state_flag_77 = info->get_state_flag();
}

thread_info *__get_thread_info_77() {
  if (__thread_77 != NULL) return __thread_77;
  __thread_77 = new thread_info(77, check_status_during_io__77);
  __init_thread_info_77(__thread_77);
  return __thread_77;
}

void __declare_sockets_77() {
  init_instance::add_incoming(76,77, DATA_SOCKET);
  init_instance::add_outgoing(77,78, DATA_SOCKET);
  init_instance::add_outgoing(77,81, DATA_SOCKET);
}

void __init_sockets_77(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_77() {
}

void __peek_sockets_77() {
}

inline float __pop_76_77() {
float res=BUFFER_76_77[TAIL_76_77];
TAIL_76_77++;
return res;
}


inline void __push_77_78(float data) {
BUFFER_77_78[HEAD_77_78]=data;
HEAD_77_78++;
}



inline void __push_77_81(float data) {
BUFFER_77_81[HEAD_77_81]=data;
HEAD_77_81++;
}



void __splitter_77_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_76_77[TAIL_76_77]; TAIL_76_77++;
;
  __push_77_78(tmp);
  __push_77_81(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_78;
int __counter_78 = 0;
int __steady_78 = 0;
int __tmp_78 = 0;
int __tmp2_78 = 0;
int *__state_flag_78 = NULL;
thread_info *__thread_78 = NULL;



float data__436__78 = 0.0f;
int i__437__78 = 0;
void save_peek_buffer__78(object_write_buffer *buf);
void load_peek_buffer__78(object_write_buffer *buf);
void save_file_pointer__78(object_write_buffer *buf);
void load_file_pointer__78(object_write_buffer *buf);

inline void check_status__78() {
  check_thread_status(__state_flag_78, __thread_78);
}

void check_status_during_io__78() {
  check_thread_status_during_io(__state_flag_78, __thread_78);
}

void __init_thread_info_78(thread_info *info) {
  __state_flag_78 = info->get_state_flag();
}

thread_info *__get_thread_info_78() {
  if (__thread_78 != NULL) return __thread_78;
  __thread_78 = new thread_info(78, check_status_during_io__78);
  __init_thread_info_78(__thread_78);
  return __thread_78;
}

void __declare_sockets_78() {
  init_instance::add_incoming(77,78, DATA_SOCKET);
  init_instance::add_outgoing(78,79, DATA_SOCKET);
}

void __init_sockets_78(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_78() {
}

void __peek_sockets_78() {
}

 
void init_DenomDataSum__441_144__78();
inline void check_status__78();

void work_DenomDataSum__441_144__78(int);


inline float __pop_77_78() {
float res=BUFFER_77_78[TAIL_77_78];
TAIL_77_78++;
return res;
}

inline void __pop_77_78(int n) {
TAIL_77_78+=n;
}

inline float __peek_77_78(int offs) {
return BUFFER_77_78[TAIL_77_78+offs];
}



inline void __push_78_79(float data) {
BUFFER_78_79[HEAD_78_79]=data;
HEAD_78_79++;
}



 
void init_DenomDataSum__441_144__78(){
}
void save_file_pointer__78(object_write_buffer *buf) {}
void load_file_pointer__78(object_write_buffer *buf) {}
 
void work_DenomDataSum__441_144__78(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__440 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__440 = ((float)0.0))/*float*/;
      for (((i__437__78) = 0)/*int*/; ((i__437__78) < 256); ((i__437__78)++)) {{
          (data__436__78) = BUFFER_77_78[TAIL_77_78]; TAIL_77_78++;
;
          (sum__440 = (sum__440 + ((data__436__78) * (data__436__78))))/*float*/;
        }
      }
      __push_78_79(sum__440);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_79;
int __counter_79 = 0;
int __steady_79 = 0;
int __tmp_79 = 0;
int __tmp2_79 = 0;
int *__state_flag_79 = NULL;
thread_info *__thread_79 = NULL;



inline void check_status__79() {
  check_thread_status(__state_flag_79, __thread_79);
}

void check_status_during_io__79() {
  check_thread_status_during_io(__state_flag_79, __thread_79);
}

void __init_thread_info_79(thread_info *info) {
  __state_flag_79 = info->get_state_flag();
}

thread_info *__get_thread_info_79() {
  if (__thread_79 != NULL) return __thread_79;
  __thread_79 = new thread_info(79, check_status_during_io__79);
  __init_thread_info_79(__thread_79);
  return __thread_79;
}

void __declare_sockets_79() {
  init_instance::add_incoming(78,79, DATA_SOCKET);
  init_instance::add_incoming(81,79, DATA_SOCKET);
  init_instance::add_outgoing(79,80, DATA_SOCKET);
}

void __init_sockets_79(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_79() {
}

void __peek_sockets_79() {
}


inline void __push_79_80(float data) {
BUFFER_79_80[HEAD_79_80]=data;
HEAD_79_80++;
}


inline float __pop_78_79() {
float res=BUFFER_78_79[TAIL_78_79];
TAIL_78_79++;
return res;
}

inline float __pop_81_79() {
float res=BUFFER_81_79[TAIL_81_79];
TAIL_81_79++;
return res;
}


void __joiner_79_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_79_80(__pop_78_79());
    __push_79_80(__pop_81_79());
    __push_79_80(__pop_81_79());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_8;
int __counter_8 = 0;
int __steady_8 = 0;
int __tmp_8 = 0;
int __tmp2_8 = 0;
int *__state_flag_8 = NULL;
thread_info *__thread_8 = NULL;



float numer__41__8 = 0.0f;
float denom_x__42__8 = 0.0f;
float denom_y__43__8 = 0.0f;
float denom__44__8 = 0.0f;
void save_peek_buffer__8(object_write_buffer *buf);
void load_peek_buffer__8(object_write_buffer *buf);
void save_file_pointer__8(object_write_buffer *buf);
void load_file_pointer__8(object_write_buffer *buf);

inline void check_status__8() {
  check_thread_status(__state_flag_8, __thread_8);
}

void check_status_during_io__8() {
  check_thread_status_during_io(__state_flag_8, __thread_8);
}

void __init_thread_info_8(thread_info *info) {
  __state_flag_8 = info->get_state_flag();
}

thread_info *__get_thread_info_8() {
  if (__thread_8 != NULL) return __thread_8;
  __thread_8 = new thread_info(8, check_status_during_io__8);
  __init_thread_info_8(__thread_8);
  return __thread_8;
}

void __declare_sockets_8() {
  init_instance::add_incoming(7,8, DATA_SOCKET);
  init_instance::add_outgoing(8,9, DATA_SOCKET);
}

void __init_sockets_8(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_8() {
}

void __peek_sockets_8() {
}

 
void init_Division__51_66__8();
inline void check_status__8();

void work_Division__51_66__8(int);


inline float __pop_7_8() {
float res=BUFFER_7_8[TAIL_7_8];
TAIL_7_8++;
return res;
}

inline void __pop_7_8(int n) {
TAIL_7_8+=n;
}

inline float __peek_7_8(int offs) {
return BUFFER_7_8[TAIL_7_8+offs];
}



inline void __push_8_9(float data) {
BUFFER_8_9[HEAD_8_9]=data;
HEAD_8_9++;
}



 
void init_Division__51_66__8(){
}
void save_file_pointer__8(object_write_buffer *buf) {}
void load_file_pointer__8(object_write_buffer *buf) {}
 
void work_Division__51_66__8(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__47 = 0.0f;/* float */
      float __tmp4__48 = 0.0f;/* float */
      double __tmp5__49 = 0.0f;/* double */
      double __tmp6__50 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__47 = ((float)0.0))/*float*/;
      (denom_x__42__8) = BUFFER_7_8[TAIL_7_8]; TAIL_7_8++;
;
      (denom_y__43__8) = BUFFER_7_8[TAIL_7_8]; TAIL_7_8++;
;
      (__tmp6__50 = ((double)(((denom_x__42__8) * (denom_y__43__8)))))/*double*/;
      (__tmp5__49 = sqrtf(__tmp6__50))/*double*/;
      (__tmp4__48 = ((float)(__tmp5__49)))/*float*/;
      ((denom__44__8) = __tmp4__48)/*float*/;
      (numer__41__8) = BUFFER_7_8[TAIL_7_8]; TAIL_7_8++;
;

      if (((denom__44__8) != ((float)0.0))) {(v__47 = ((numer__41__8) / (denom__44__8)))/*float*/;
      } else {}
      __push_8_9(v__47);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_80;
int __counter_80 = 0;
int __steady_80 = 0;
int __tmp_80 = 0;
int __tmp2_80 = 0;
int *__state_flag_80 = NULL;
thread_info *__thread_80 = NULL;



float numer__451__80 = 0.0f;
float denom_x__452__80 = 0.0f;
float denom_y__453__80 = 0.0f;
float denom__454__80 = 0.0f;
void save_peek_buffer__80(object_write_buffer *buf);
void load_peek_buffer__80(object_write_buffer *buf);
void save_file_pointer__80(object_write_buffer *buf);
void load_file_pointer__80(object_write_buffer *buf);

inline void check_status__80() {
  check_thread_status(__state_flag_80, __thread_80);
}

void check_status_during_io__80() {
  check_thread_status_during_io(__state_flag_80, __thread_80);
}

void __init_thread_info_80(thread_info *info) {
  __state_flag_80 = info->get_state_flag();
}

thread_info *__get_thread_info_80() {
  if (__thread_80 != NULL) return __thread_80;
  __thread_80 = new thread_info(80, check_status_during_io__80);
  __init_thread_info_80(__thread_80);
  return __thread_80;
}

void __declare_sockets_80() {
  init_instance::add_incoming(79,80, DATA_SOCKET);
  init_instance::add_outgoing(80,9, DATA_SOCKET);
}

void __init_sockets_80(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_80() {
}

void __peek_sockets_80() {
}

 
void init_Division__461_146__80();
inline void check_status__80();

void work_Division__461_146__80(int);


inline float __pop_79_80() {
float res=BUFFER_79_80[TAIL_79_80];
TAIL_79_80++;
return res;
}

inline void __pop_79_80(int n) {
TAIL_79_80+=n;
}

inline float __peek_79_80(int offs) {
return BUFFER_79_80[TAIL_79_80+offs];
}



inline void __push_80_9(float data) {
BUFFER_80_9[HEAD_80_9]=data;
HEAD_80_9++;
}



 
void init_Division__461_146__80(){
}
void save_file_pointer__80(object_write_buffer *buf) {}
void load_file_pointer__80(object_write_buffer *buf) {}
 
void work_Division__461_146__80(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__457 = 0.0f;/* float */
      float __tmp4__458 = 0.0f;/* float */
      double __tmp5__459 = 0.0f;/* double */
      double __tmp6__460 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__457 = ((float)0.0))/*float*/;
      (denom_x__452__80) = BUFFER_79_80[TAIL_79_80]; TAIL_79_80++;
;
      (denom_y__453__80) = BUFFER_79_80[TAIL_79_80]; TAIL_79_80++;
;
      (__tmp6__460 = ((double)(((denom_x__452__80) * (denom_y__453__80)))))/*double*/;
      (__tmp5__459 = sqrtf(__tmp6__460))/*double*/;
      (__tmp4__458 = ((float)(__tmp5__459)))/*float*/;
      ((denom__454__80) = __tmp4__458)/*float*/;
      (numer__451__80) = BUFFER_79_80[TAIL_79_80]; TAIL_79_80++;
;

      if (((denom__454__80) != ((float)0.0))) {(v__457 = ((numer__451__80) / (denom__454__80)))/*float*/;
      } else {}
      __push_80_9(v__457);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_81;
int __counter_81 = 0;
int __steady_81 = 0;
int __tmp_81 = 0;
int __tmp2_81 = 0;
int *__state_flag_81 = NULL;
thread_info *__thread_81 = NULL;



float _TheGlobal__pattern__6____81[256] = {0};
float numerSum__443__81 = 0.0f;
float denomPatternSum__444__81 = 0.0f;
float data__445__81 = 0.0f;
float patternData__446__81 = 0.0f;
int i__447__81 = 0;
void save_peek_buffer__81(object_write_buffer *buf);
void load_peek_buffer__81(object_write_buffer *buf);
void save_file_pointer__81(object_write_buffer *buf);
void load_file_pointer__81(object_write_buffer *buf);

inline void check_status__81() {
  check_thread_status(__state_flag_81, __thread_81);
}

void check_status_during_io__81() {
  check_thread_status_during_io(__state_flag_81, __thread_81);
}

void __init_thread_info_81(thread_info *info) {
  __state_flag_81 = info->get_state_flag();
}

thread_info *__get_thread_info_81() {
  if (__thread_81 != NULL) return __thread_81;
  __thread_81 = new thread_info(81, check_status_during_io__81);
  __init_thread_info_81(__thread_81);
  return __thread_81;
}

void __declare_sockets_81() {
  init_instance::add_incoming(77,81, DATA_SOCKET);
  init_instance::add_outgoing(81,79, DATA_SOCKET);
}

void __init_sockets_81(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_81() {
}

void __peek_sockets_81() {
}

 
void init_PatternSums__450_145__81();
inline void check_status__81();

void work_PatternSums__450_145__81(int);


inline float __pop_77_81() {
float res=BUFFER_77_81[TAIL_77_81];
TAIL_77_81++;
return res;
}

inline void __pop_77_81(int n) {
TAIL_77_81+=n;
}

inline float __peek_77_81(int offs) {
return BUFFER_77_81[TAIL_77_81+offs];
}



inline void __push_81_79(float data) {
BUFFER_81_79[HEAD_81_79]=data;
HEAD_81_79++;
}



 
void init_PatternSums__450_145__81(){
  (((_TheGlobal__pattern__6____81)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____81)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__81(object_write_buffer *buf) {}
void load_file_pointer__81(object_write_buffer *buf) {}
 
void work_PatternSums__450_145__81(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__443__81) = ((float)0.0))/*float*/;
      ((denomPatternSum__444__81) = ((float)0.0))/*float*/;
      for (((i__447__81) = 0)/*int*/; ((i__447__81) < 256); ((i__447__81)++)) {{
          ((patternData__446__81) = (((_TheGlobal__pattern__6____81)[(int)(i__447__81)]) - ((float)1.6013207E-4)))/*float*/;
          (data__445__81) = BUFFER_77_81[TAIL_77_81]; TAIL_77_81++;
;
          ((numerSum__443__81) = ((numerSum__443__81) + ((data__445__81) * (patternData__446__81))))/*float*/;
          ((denomPatternSum__444__81) = ((denomPatternSum__444__81) + ((patternData__446__81) * (patternData__446__81))))/*float*/;
        }
      }
      __push_81_79((denomPatternSum__444__81));
      __push_81_79((numerSum__443__81));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_82;
int __counter_82 = 0;
int __steady_82 = 0;
int __tmp_82 = 0;
int __tmp2_82 = 0;
int *__state_flag_82 = NULL;
thread_info *__thread_82 = NULL;



void save_peek_buffer__82(object_write_buffer *buf);
void load_peek_buffer__82(object_write_buffer *buf);
void save_file_pointer__82(object_write_buffer *buf);
void load_file_pointer__82(object_write_buffer *buf);

inline void check_status__82() {
  check_thread_status(__state_flag_82, __thread_82);
}

void check_status_during_io__82() {
  check_thread_status_during_io(__state_flag_82, __thread_82);
}

void __init_thread_info_82(thread_info *info) {
  __state_flag_82 = info->get_state_flag();
}

thread_info *__get_thread_info_82() {
  if (__thread_82 != NULL) return __thread_82;
  __thread_82 = new thread_info(82, check_status_during_io__82);
  __init_thread_info_82(__thread_82);
  return __thread_82;
}

void __declare_sockets_82() {
  init_instance::add_incoming(2,82, DATA_SOCKET);
  init_instance::add_outgoing(82,83, DATA_SOCKET);
}

void __init_sockets_82(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_82() {
}

void __peek_sockets_82() {
}

 
void init_GetAvg__468_148__82();
inline void check_status__82();

void work_GetAvg__468_148__82(int);


inline float __pop_2_82() {
float res=BUFFER_2_82[TAIL_2_82];
TAIL_2_82++;
return res;
}

inline void __pop_2_82(int n) {
TAIL_2_82+=n;
}

inline float __peek_2_82(int offs) {
return BUFFER_2_82[TAIL_2_82+offs];
}



inline void __push_82_83(float data) {
BUFFER_82_83[HEAD_82_83]=data;
HEAD_82_83++;
}



 
void init_GetAvg__468_148__82(){
}
void save_file_pointer__82(object_write_buffer *buf) {}
void load_file_pointer__82(object_write_buffer *buf) {}
 
void work_GetAvg__468_148__82(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__464 = 0;/* int */
      float Sum__465 = 0.0f;/* float */
      float val__466 = 0.0f;/* float */
      float __tmp8__467 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__465 = ((float)0.0))/*float*/;
      for ((i__464 = 0)/*int*/; (i__464 < 256); (i__464++)) {{
          val__466 = BUFFER_2_82[TAIL_2_82]; TAIL_2_82++;
;
          (Sum__465 = (Sum__465 + val__466))/*float*/;
          __push_82_83(val__466);
        }
      }
      (__tmp8__467 = (Sum__465 / ((float)256.0)))/*float*/;
      __push_82_83(__tmp8__467);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_83;
int __counter_83 = 0;
int __steady_83 = 0;
int __tmp_83 = 0;
int __tmp2_83 = 0;
int *__state_flag_83 = NULL;
thread_info *__thread_83 = NULL;



float avg__469__83 = 0.0f;
int i__470__83 = 0;
void save_peek_buffer__83(object_write_buffer *buf);
void load_peek_buffer__83(object_write_buffer *buf);
void save_file_pointer__83(object_write_buffer *buf);
void load_file_pointer__83(object_write_buffer *buf);

inline void check_status__83() {
  check_thread_status(__state_flag_83, __thread_83);
}

void check_status_during_io__83() {
  check_thread_status_during_io(__state_flag_83, __thread_83);
}

void __init_thread_info_83(thread_info *info) {
  __state_flag_83 = info->get_state_flag();
}

thread_info *__get_thread_info_83() {
  if (__thread_83 != NULL) return __thread_83;
  __thread_83 = new thread_info(83, check_status_during_io__83);
  __init_thread_info_83(__thread_83);
  return __thread_83;
}

void __declare_sockets_83() {
  init_instance::add_incoming(82,83, DATA_SOCKET);
  init_instance::add_outgoing(83,84, DATA_SOCKET);
}

void __init_sockets_83(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_83() {
}

void __peek_sockets_83() {
}

 
void init_DataMinusAvg__476_150__83();
inline void check_status__83();

void work_DataMinusAvg__476_150__83(int);


inline float __pop_82_83() {
float res=BUFFER_82_83[TAIL_82_83];
TAIL_82_83++;
return res;
}

inline void __pop_82_83(int n) {
TAIL_82_83+=n;
}

inline float __peek_82_83(int offs) {
return BUFFER_82_83[TAIL_82_83+offs];
}



inline void __push_83_84(float data) {
BUFFER_83_84[HEAD_83_84]=data;
HEAD_83_84++;
}



 
void init_DataMinusAvg__476_150__83(){
}
void save_file_pointer__83(object_write_buffer *buf) {}
void load_file_pointer__83(object_write_buffer *buf) {}
 
void work_DataMinusAvg__476_150__83(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__473 = 0.0f;/* float */
      float v__474 = 0.0f;/* float */
      float __tmp11__475 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__473 = BUFFER_82_83[TAIL_82_83+256];
;
      ((avg__469__83) = __tmp9__473)/*float*/;
      for (((i__470__83) = 0)/*int*/; ((i__470__83) < 256); ((i__470__83)++)) {{
          v__474 = BUFFER_82_83[TAIL_82_83]; TAIL_82_83++;
;
          (__tmp11__475 = (v__474 - (avg__469__83)))/*float*/;
          __push_83_84(__tmp11__475);
        }
      }
      __pop_82_83();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_84;
int __counter_84 = 0;
int __steady_84 = 0;
int __tmp_84 = 0;
int __tmp2_84 = 0;
int *__state_flag_84 = NULL;
thread_info *__thread_84 = NULL;



inline void check_status__84() {
  check_thread_status(__state_flag_84, __thread_84);
}

void check_status_during_io__84() {
  check_thread_status_during_io(__state_flag_84, __thread_84);
}

void __init_thread_info_84(thread_info *info) {
  __state_flag_84 = info->get_state_flag();
}

thread_info *__get_thread_info_84() {
  if (__thread_84 != NULL) return __thread_84;
  __thread_84 = new thread_info(84, check_status_during_io__84);
  __init_thread_info_84(__thread_84);
  return __thread_84;
}

void __declare_sockets_84() {
  init_instance::add_incoming(83,84, DATA_SOCKET);
  init_instance::add_outgoing(84,85, DATA_SOCKET);
  init_instance::add_outgoing(84,88, DATA_SOCKET);
}

void __init_sockets_84(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_84() {
}

void __peek_sockets_84() {
}

inline float __pop_83_84() {
float res=BUFFER_83_84[TAIL_83_84];
TAIL_83_84++;
return res;
}


inline void __push_84_85(float data) {
BUFFER_84_85[HEAD_84_85]=data;
HEAD_84_85++;
}



inline void __push_84_88(float data) {
BUFFER_84_88[HEAD_84_88]=data;
HEAD_84_88++;
}



void __splitter_84_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_83_84[TAIL_83_84]; TAIL_83_84++;
;
  __push_84_85(tmp);
  __push_84_88(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_85;
int __counter_85 = 0;
int __steady_85 = 0;
int __tmp_85 = 0;
int __tmp2_85 = 0;
int *__state_flag_85 = NULL;
thread_info *__thread_85 = NULL;



float data__477__85 = 0.0f;
int i__478__85 = 0;
void save_peek_buffer__85(object_write_buffer *buf);
void load_peek_buffer__85(object_write_buffer *buf);
void save_file_pointer__85(object_write_buffer *buf);
void load_file_pointer__85(object_write_buffer *buf);

inline void check_status__85() {
  check_thread_status(__state_flag_85, __thread_85);
}

void check_status_during_io__85() {
  check_thread_status_during_io(__state_flag_85, __thread_85);
}

void __init_thread_info_85(thread_info *info) {
  __state_flag_85 = info->get_state_flag();
}

thread_info *__get_thread_info_85() {
  if (__thread_85 != NULL) return __thread_85;
  __thread_85 = new thread_info(85, check_status_during_io__85);
  __init_thread_info_85(__thread_85);
  return __thread_85;
}

void __declare_sockets_85() {
  init_instance::add_incoming(84,85, DATA_SOCKET);
  init_instance::add_outgoing(85,86, DATA_SOCKET);
}

void __init_sockets_85(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_85() {
}

void __peek_sockets_85() {
}

 
void init_DenomDataSum__482_152__85();
inline void check_status__85();

void work_DenomDataSum__482_152__85(int);


inline float __pop_84_85() {
float res=BUFFER_84_85[TAIL_84_85];
TAIL_84_85++;
return res;
}

inline void __pop_84_85(int n) {
TAIL_84_85+=n;
}

inline float __peek_84_85(int offs) {
return BUFFER_84_85[TAIL_84_85+offs];
}



inline void __push_85_86(float data) {
BUFFER_85_86[HEAD_85_86]=data;
HEAD_85_86++;
}



 
void init_DenomDataSum__482_152__85(){
}
void save_file_pointer__85(object_write_buffer *buf) {}
void load_file_pointer__85(object_write_buffer *buf) {}
 
void work_DenomDataSum__482_152__85(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__481 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__481 = ((float)0.0))/*float*/;
      for (((i__478__85) = 0)/*int*/; ((i__478__85) < 256); ((i__478__85)++)) {{
          (data__477__85) = BUFFER_84_85[TAIL_84_85]; TAIL_84_85++;
;
          (sum__481 = (sum__481 + ((data__477__85) * (data__477__85))))/*float*/;
        }
      }
      __push_85_86(sum__481);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_86;
int __counter_86 = 0;
int __steady_86 = 0;
int __tmp_86 = 0;
int __tmp2_86 = 0;
int *__state_flag_86 = NULL;
thread_info *__thread_86 = NULL;



inline void check_status__86() {
  check_thread_status(__state_flag_86, __thread_86);
}

void check_status_during_io__86() {
  check_thread_status_during_io(__state_flag_86, __thread_86);
}

void __init_thread_info_86(thread_info *info) {
  __state_flag_86 = info->get_state_flag();
}

thread_info *__get_thread_info_86() {
  if (__thread_86 != NULL) return __thread_86;
  __thread_86 = new thread_info(86, check_status_during_io__86);
  __init_thread_info_86(__thread_86);
  return __thread_86;
}

void __declare_sockets_86() {
  init_instance::add_incoming(85,86, DATA_SOCKET);
  init_instance::add_incoming(88,86, DATA_SOCKET);
  init_instance::add_outgoing(86,87, DATA_SOCKET);
}

void __init_sockets_86(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_86() {
}

void __peek_sockets_86() {
}


inline void __push_86_87(float data) {
BUFFER_86_87[HEAD_86_87]=data;
HEAD_86_87++;
}


inline float __pop_85_86() {
float res=BUFFER_85_86[TAIL_85_86];
TAIL_85_86++;
return res;
}

inline float __pop_88_86() {
float res=BUFFER_88_86[TAIL_88_86];
TAIL_88_86++;
return res;
}


void __joiner_86_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_86_87(__pop_85_86());
    __push_86_87(__pop_88_86());
    __push_86_87(__pop_88_86());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_87;
int __counter_87 = 0;
int __steady_87 = 0;
int __tmp_87 = 0;
int __tmp2_87 = 0;
int *__state_flag_87 = NULL;
thread_info *__thread_87 = NULL;



float numer__492__87 = 0.0f;
float denom_x__493__87 = 0.0f;
float denom_y__494__87 = 0.0f;
float denom__495__87 = 0.0f;
void save_peek_buffer__87(object_write_buffer *buf);
void load_peek_buffer__87(object_write_buffer *buf);
void save_file_pointer__87(object_write_buffer *buf);
void load_file_pointer__87(object_write_buffer *buf);

inline void check_status__87() {
  check_thread_status(__state_flag_87, __thread_87);
}

void check_status_during_io__87() {
  check_thread_status_during_io(__state_flag_87, __thread_87);
}

void __init_thread_info_87(thread_info *info) {
  __state_flag_87 = info->get_state_flag();
}

thread_info *__get_thread_info_87() {
  if (__thread_87 != NULL) return __thread_87;
  __thread_87 = new thread_info(87, check_status_during_io__87);
  __init_thread_info_87(__thread_87);
  return __thread_87;
}

void __declare_sockets_87() {
  init_instance::add_incoming(86,87, DATA_SOCKET);
  init_instance::add_outgoing(87,9, DATA_SOCKET);
}

void __init_sockets_87(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_87() {
}

void __peek_sockets_87() {
}

 
void init_Division__502_154__87();
inline void check_status__87();

void work_Division__502_154__87(int);


inline float __pop_86_87() {
float res=BUFFER_86_87[TAIL_86_87];
TAIL_86_87++;
return res;
}

inline void __pop_86_87(int n) {
TAIL_86_87+=n;
}

inline float __peek_86_87(int offs) {
return BUFFER_86_87[TAIL_86_87+offs];
}



inline void __push_87_9(float data) {
BUFFER_87_9[HEAD_87_9]=data;
HEAD_87_9++;
}



 
void init_Division__502_154__87(){
}
void save_file_pointer__87(object_write_buffer *buf) {}
void load_file_pointer__87(object_write_buffer *buf) {}
 
void work_Division__502_154__87(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__498 = 0.0f;/* float */
      float __tmp4__499 = 0.0f;/* float */
      double __tmp5__500 = 0.0f;/* double */
      double __tmp6__501 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__498 = ((float)0.0))/*float*/;
      (denom_x__493__87) = BUFFER_86_87[TAIL_86_87]; TAIL_86_87++;
;
      (denom_y__494__87) = BUFFER_86_87[TAIL_86_87]; TAIL_86_87++;
;
      (__tmp6__501 = ((double)(((denom_x__493__87) * (denom_y__494__87)))))/*double*/;
      (__tmp5__500 = sqrtf(__tmp6__501))/*double*/;
      (__tmp4__499 = ((float)(__tmp5__500)))/*float*/;
      ((denom__495__87) = __tmp4__499)/*float*/;
      (numer__492__87) = BUFFER_86_87[TAIL_86_87]; TAIL_86_87++;
;

      if (((denom__495__87) != ((float)0.0))) {(v__498 = ((numer__492__87) / (denom__495__87)))/*float*/;
      } else {}
      __push_87_9(v__498);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_88;
int __counter_88 = 0;
int __steady_88 = 0;
int __tmp_88 = 0;
int __tmp2_88 = 0;
int *__state_flag_88 = NULL;
thread_info *__thread_88 = NULL;



float _TheGlobal__pattern__6____88[256] = {0};
float numerSum__484__88 = 0.0f;
float denomPatternSum__485__88 = 0.0f;
float data__486__88 = 0.0f;
float patternData__487__88 = 0.0f;
int i__488__88 = 0;
void save_peek_buffer__88(object_write_buffer *buf);
void load_peek_buffer__88(object_write_buffer *buf);
void save_file_pointer__88(object_write_buffer *buf);
void load_file_pointer__88(object_write_buffer *buf);

inline void check_status__88() {
  check_thread_status(__state_flag_88, __thread_88);
}

void check_status_during_io__88() {
  check_thread_status_during_io(__state_flag_88, __thread_88);
}

void __init_thread_info_88(thread_info *info) {
  __state_flag_88 = info->get_state_flag();
}

thread_info *__get_thread_info_88() {
  if (__thread_88 != NULL) return __thread_88;
  __thread_88 = new thread_info(88, check_status_during_io__88);
  __init_thread_info_88(__thread_88);
  return __thread_88;
}

void __declare_sockets_88() {
  init_instance::add_incoming(84,88, DATA_SOCKET);
  init_instance::add_outgoing(88,86, DATA_SOCKET);
}

void __init_sockets_88(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_88() {
}

void __peek_sockets_88() {
}

 
void init_PatternSums__491_153__88();
inline void check_status__88();

void work_PatternSums__491_153__88(int);


inline float __pop_84_88() {
float res=BUFFER_84_88[TAIL_84_88];
TAIL_84_88++;
return res;
}

inline void __pop_84_88(int n) {
TAIL_84_88+=n;
}

inline float __peek_84_88(int offs) {
return BUFFER_84_88[TAIL_84_88+offs];
}



inline void __push_88_86(float data) {
BUFFER_88_86[HEAD_88_86]=data;
HEAD_88_86++;
}



 
void init_PatternSums__491_153__88(){
  (((_TheGlobal__pattern__6____88)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__88(object_write_buffer *buf) {}
void load_file_pointer__88(object_write_buffer *buf) {}
 
void work_PatternSums__491_153__88(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__484__88) = ((float)0.0))/*float*/;
      ((denomPatternSum__485__88) = ((float)0.0))/*float*/;
      for (((i__488__88) = 0)/*int*/; ((i__488__88) < 256); ((i__488__88)++)) {{
          ((patternData__487__88) = (((_TheGlobal__pattern__6____88)[(int)(i__488__88)]) - ((float)1.6013207E-4)))/*float*/;
          (data__486__88) = BUFFER_84_88[TAIL_84_88]; TAIL_84_88++;
;
          ((numerSum__484__88) = ((numerSum__484__88) + ((data__486__88) * (patternData__487__88))))/*float*/;
          ((denomPatternSum__485__88) = ((denomPatternSum__485__88) + ((patternData__487__88) * (patternData__487__88))))/*float*/;
        }
      }
      __push_88_86((denomPatternSum__485__88));
      __push_88_86((numerSum__484__88));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_89;
int __counter_89 = 0;
int __steady_89 = 0;
int __tmp_89 = 0;
int __tmp2_89 = 0;
int *__state_flag_89 = NULL;
thread_info *__thread_89 = NULL;



void save_peek_buffer__89(object_write_buffer *buf);
void load_peek_buffer__89(object_write_buffer *buf);
void save_file_pointer__89(object_write_buffer *buf);
void load_file_pointer__89(object_write_buffer *buf);

inline void check_status__89() {
  check_thread_status(__state_flag_89, __thread_89);
}

void check_status_during_io__89() {
  check_thread_status_during_io(__state_flag_89, __thread_89);
}

void __init_thread_info_89(thread_info *info) {
  __state_flag_89 = info->get_state_flag();
}

thread_info *__get_thread_info_89() {
  if (__thread_89 != NULL) return __thread_89;
  __thread_89 = new thread_info(89, check_status_during_io__89);
  __init_thread_info_89(__thread_89);
  return __thread_89;
}

void __declare_sockets_89() {
  init_instance::add_incoming(2,89, DATA_SOCKET);
  init_instance::add_outgoing(89,90, DATA_SOCKET);
}

void __init_sockets_89(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_89() {
}

void __peek_sockets_89() {
}

 
void init_GetAvg__509_156__89();
inline void check_status__89();

void work_GetAvg__509_156__89(int);


inline float __pop_2_89() {
float res=BUFFER_2_89[TAIL_2_89];
TAIL_2_89++;
return res;
}

inline void __pop_2_89(int n) {
TAIL_2_89+=n;
}

inline float __peek_2_89(int offs) {
return BUFFER_2_89[TAIL_2_89+offs];
}



inline void __push_89_90(float data) {
BUFFER_89_90[HEAD_89_90]=data;
HEAD_89_90++;
}



 
void init_GetAvg__509_156__89(){
}
void save_file_pointer__89(object_write_buffer *buf) {}
void load_file_pointer__89(object_write_buffer *buf) {}
 
void work_GetAvg__509_156__89(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__505 = 0;/* int */
      float Sum__506 = 0.0f;/* float */
      float val__507 = 0.0f;/* float */
      float __tmp8__508 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__506 = ((float)0.0))/*float*/;
      for ((i__505 = 0)/*int*/; (i__505 < 256); (i__505++)) {{
          val__507 = BUFFER_2_89[TAIL_2_89]; TAIL_2_89++;
;
          (Sum__506 = (Sum__506 + val__507))/*float*/;
          __push_89_90(val__507);
        }
      }
      (__tmp8__508 = (Sum__506 / ((float)256.0)))/*float*/;
      __push_89_90(__tmp8__508);
      // mark end: SIRFilter GetAvg

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_9;
int __counter_9 = 0;
int __steady_9 = 0;
int __tmp_9 = 0;
int __tmp2_9 = 0;
int *__state_flag_9 = NULL;
thread_info *__thread_9 = NULL;



inline void check_status__9() {
  check_thread_status(__state_flag_9, __thread_9);
}

void check_status_during_io__9() {
  check_thread_status_during_io(__state_flag_9, __thread_9);
}

void __init_thread_info_9(thread_info *info) {
  __state_flag_9 = info->get_state_flag();
}

thread_info *__get_thread_info_9() {
  if (__thread_9 != NULL) return __thread_9;
  __thread_9 = new thread_info(9, check_status_during_io__9);
  __init_thread_info_9(__thread_9);
  return __thread_9;
}

void __declare_sockets_9() {
  init_instance::add_incoming(8,9, DATA_SOCKET);
  init_instance::add_incoming(17,9, DATA_SOCKET);
  init_instance::add_incoming(24,9, DATA_SOCKET);
  init_instance::add_incoming(31,9, DATA_SOCKET);
  init_instance::add_incoming(38,9, DATA_SOCKET);
  init_instance::add_incoming(45,9, DATA_SOCKET);
  init_instance::add_incoming(52,9, DATA_SOCKET);
  init_instance::add_incoming(59,9, DATA_SOCKET);
  init_instance::add_incoming(66,9, DATA_SOCKET);
  init_instance::add_incoming(73,9, DATA_SOCKET);
  init_instance::add_incoming(80,9, DATA_SOCKET);
  init_instance::add_incoming(87,9, DATA_SOCKET);
  init_instance::add_incoming(94,9, DATA_SOCKET);
  init_instance::add_incoming(101,9, DATA_SOCKET);
  init_instance::add_incoming(108,9, DATA_SOCKET);
  init_instance::add_incoming(115,9, DATA_SOCKET);
  init_instance::add_outgoing(9,10, DATA_SOCKET);
}

void __init_sockets_9(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_9() {
}

void __peek_sockets_9() {
}


inline void __push_9_10(float data) {
BUFFER_9_10[HEAD_9_10]=data;
HEAD_9_10++;
}


inline float __pop_8_9() {
float res=BUFFER_8_9[TAIL_8_9];
TAIL_8_9++;
return res;
}

inline float __pop_17_9() {
float res=BUFFER_17_9[TAIL_17_9];
TAIL_17_9++;
return res;
}

inline float __pop_24_9() {
float res=BUFFER_24_9[TAIL_24_9];
TAIL_24_9++;
return res;
}

inline float __pop_31_9() {
float res=BUFFER_31_9[TAIL_31_9];
TAIL_31_9++;
return res;
}

inline float __pop_38_9() {
float res=BUFFER_38_9[TAIL_38_9];
TAIL_38_9++;
return res;
}

inline float __pop_45_9() {
float res=BUFFER_45_9[TAIL_45_9];
TAIL_45_9++;
return res;
}

inline float __pop_52_9() {
float res=BUFFER_52_9[TAIL_52_9];
TAIL_52_9++;
return res;
}

inline float __pop_59_9() {
float res=BUFFER_59_9[TAIL_59_9];
TAIL_59_9++;
return res;
}

inline float __pop_66_9() {
float res=BUFFER_66_9[TAIL_66_9];
TAIL_66_9++;
return res;
}

inline float __pop_73_9() {
float res=BUFFER_73_9[TAIL_73_9];
TAIL_73_9++;
return res;
}

inline float __pop_80_9() {
float res=BUFFER_80_9[TAIL_80_9];
TAIL_80_9++;
return res;
}

inline float __pop_87_9() {
float res=BUFFER_87_9[TAIL_87_9];
TAIL_87_9++;
return res;
}

inline float __pop_94_9() {
float res=BUFFER_94_9[TAIL_94_9];
TAIL_94_9++;
return res;
}

inline float __pop_101_9() {
float res=BUFFER_101_9[TAIL_101_9];
TAIL_101_9++;
return res;
}

inline float __pop_108_9() {
float res=BUFFER_108_9[TAIL_108_9];
TAIL_108_9++;
return res;
}

inline float __pop_115_9() {
float res=BUFFER_115_9[TAIL_115_9];
TAIL_115_9++;
return res;
}


void __joiner_9_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_9_10(__pop_8_9());
    __push_9_10(__pop_17_9());
    __push_9_10(__pop_24_9());
    __push_9_10(__pop_31_9());
    __push_9_10(__pop_38_9());
    __push_9_10(__pop_45_9());
    __push_9_10(__pop_52_9());
    __push_9_10(__pop_59_9());
    __push_9_10(__pop_66_9());
    __push_9_10(__pop_73_9());
    __push_9_10(__pop_80_9());
    __push_9_10(__pop_87_9());
    __push_9_10(__pop_94_9());
    __push_9_10(__pop_101_9());
    __push_9_10(__pop_108_9());
    __push_9_10(__pop_115_9());
  }
}


// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_90;
int __counter_90 = 0;
int __steady_90 = 0;
int __tmp_90 = 0;
int __tmp2_90 = 0;
int *__state_flag_90 = NULL;
thread_info *__thread_90 = NULL;



float avg__510__90 = 0.0f;
int i__511__90 = 0;
void save_peek_buffer__90(object_write_buffer *buf);
void load_peek_buffer__90(object_write_buffer *buf);
void save_file_pointer__90(object_write_buffer *buf);
void load_file_pointer__90(object_write_buffer *buf);

inline void check_status__90() {
  check_thread_status(__state_flag_90, __thread_90);
}

void check_status_during_io__90() {
  check_thread_status_during_io(__state_flag_90, __thread_90);
}

void __init_thread_info_90(thread_info *info) {
  __state_flag_90 = info->get_state_flag();
}

thread_info *__get_thread_info_90() {
  if (__thread_90 != NULL) return __thread_90;
  __thread_90 = new thread_info(90, check_status_during_io__90);
  __init_thread_info_90(__thread_90);
  return __thread_90;
}

void __declare_sockets_90() {
  init_instance::add_incoming(89,90, DATA_SOCKET);
  init_instance::add_outgoing(90,91, DATA_SOCKET);
}

void __init_sockets_90(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_90() {
}

void __peek_sockets_90() {
}

 
void init_DataMinusAvg__517_158__90();
inline void check_status__90();

void work_DataMinusAvg__517_158__90(int);


inline float __pop_89_90() {
float res=BUFFER_89_90[TAIL_89_90];
TAIL_89_90++;
return res;
}

inline void __pop_89_90(int n) {
TAIL_89_90+=n;
}

inline float __peek_89_90(int offs) {
return BUFFER_89_90[TAIL_89_90+offs];
}



inline void __push_90_91(float data) {
BUFFER_90_91[HEAD_90_91]=data;
HEAD_90_91++;
}



 
void init_DataMinusAvg__517_158__90(){
}
void save_file_pointer__90(object_write_buffer *buf) {}
void load_file_pointer__90(object_write_buffer *buf) {}
 
void work_DataMinusAvg__517_158__90(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__514 = 0.0f;/* float */
      float v__515 = 0.0f;/* float */
      float __tmp11__516 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__514 = BUFFER_89_90[TAIL_89_90+256];
;
      ((avg__510__90) = __tmp9__514)/*float*/;
      for (((i__511__90) = 0)/*int*/; ((i__511__90) < 256); ((i__511__90)++)) {{
          v__515 = BUFFER_89_90[TAIL_89_90]; TAIL_89_90++;
;
          (__tmp11__516 = (v__515 - (avg__510__90)))/*float*/;
          __push_90_91(__tmp11__516);
        }
      }
      __pop_89_90();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_91;
int __counter_91 = 0;
int __steady_91 = 0;
int __tmp_91 = 0;
int __tmp2_91 = 0;
int *__state_flag_91 = NULL;
thread_info *__thread_91 = NULL;



inline void check_status__91() {
  check_thread_status(__state_flag_91, __thread_91);
}

void check_status_during_io__91() {
  check_thread_status_during_io(__state_flag_91, __thread_91);
}

void __init_thread_info_91(thread_info *info) {
  __state_flag_91 = info->get_state_flag();
}

thread_info *__get_thread_info_91() {
  if (__thread_91 != NULL) return __thread_91;
  __thread_91 = new thread_info(91, check_status_during_io__91);
  __init_thread_info_91(__thread_91);
  return __thread_91;
}

void __declare_sockets_91() {
  init_instance::add_incoming(90,91, DATA_SOCKET);
  init_instance::add_outgoing(91,92, DATA_SOCKET);
  init_instance::add_outgoing(91,95, DATA_SOCKET);
}

void __init_sockets_91(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_91() {
}

void __peek_sockets_91() {
}

inline float __pop_90_91() {
float res=BUFFER_90_91[TAIL_90_91];
TAIL_90_91++;
return res;
}


inline void __push_91_92(float data) {
BUFFER_91_92[HEAD_91_92]=data;
HEAD_91_92++;
}



inline void __push_91_95(float data) {
BUFFER_91_95[HEAD_91_95]=data;
HEAD_91_95++;
}



void __splitter_91_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_90_91[TAIL_90_91]; TAIL_90_91++;
;
  __push_91_92(tmp);
  __push_91_95(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_92;
int __counter_92 = 0;
int __steady_92 = 0;
int __tmp_92 = 0;
int __tmp2_92 = 0;
int *__state_flag_92 = NULL;
thread_info *__thread_92 = NULL;



float data__518__92 = 0.0f;
int i__519__92 = 0;
void save_peek_buffer__92(object_write_buffer *buf);
void load_peek_buffer__92(object_write_buffer *buf);
void save_file_pointer__92(object_write_buffer *buf);
void load_file_pointer__92(object_write_buffer *buf);

inline void check_status__92() {
  check_thread_status(__state_flag_92, __thread_92);
}

void check_status_during_io__92() {
  check_thread_status_during_io(__state_flag_92, __thread_92);
}

void __init_thread_info_92(thread_info *info) {
  __state_flag_92 = info->get_state_flag();
}

thread_info *__get_thread_info_92() {
  if (__thread_92 != NULL) return __thread_92;
  __thread_92 = new thread_info(92, check_status_during_io__92);
  __init_thread_info_92(__thread_92);
  return __thread_92;
}

void __declare_sockets_92() {
  init_instance::add_incoming(91,92, DATA_SOCKET);
  init_instance::add_outgoing(92,93, DATA_SOCKET);
}

void __init_sockets_92(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_92() {
}

void __peek_sockets_92() {
}

 
void init_DenomDataSum__523_160__92();
inline void check_status__92();

void work_DenomDataSum__523_160__92(int);


inline float __pop_91_92() {
float res=BUFFER_91_92[TAIL_91_92];
TAIL_91_92++;
return res;
}

inline void __pop_91_92(int n) {
TAIL_91_92+=n;
}

inline float __peek_91_92(int offs) {
return BUFFER_91_92[TAIL_91_92+offs];
}



inline void __push_92_93(float data) {
BUFFER_92_93[HEAD_92_93]=data;
HEAD_92_93++;
}



 
void init_DenomDataSum__523_160__92(){
}
void save_file_pointer__92(object_write_buffer *buf) {}
void load_file_pointer__92(object_write_buffer *buf) {}
 
void work_DenomDataSum__523_160__92(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__522 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__522 = ((float)0.0))/*float*/;
      for (((i__519__92) = 0)/*int*/; ((i__519__92) < 256); ((i__519__92)++)) {{
          (data__518__92) = BUFFER_91_92[TAIL_91_92]; TAIL_91_92++;
;
          (sum__522 = (sum__522 + ((data__518__92) * (data__518__92))))/*float*/;
        }
      }
      __push_92_93(sum__522);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_93;
int __counter_93 = 0;
int __steady_93 = 0;
int __tmp_93 = 0;
int __tmp2_93 = 0;
int *__state_flag_93 = NULL;
thread_info *__thread_93 = NULL;



inline void check_status__93() {
  check_thread_status(__state_flag_93, __thread_93);
}

void check_status_during_io__93() {
  check_thread_status_during_io(__state_flag_93, __thread_93);
}

void __init_thread_info_93(thread_info *info) {
  __state_flag_93 = info->get_state_flag();
}

thread_info *__get_thread_info_93() {
  if (__thread_93 != NULL) return __thread_93;
  __thread_93 = new thread_info(93, check_status_during_io__93);
  __init_thread_info_93(__thread_93);
  return __thread_93;
}

void __declare_sockets_93() {
  init_instance::add_incoming(92,93, DATA_SOCKET);
  init_instance::add_incoming(95,93, DATA_SOCKET);
  init_instance::add_outgoing(93,94, DATA_SOCKET);
}

void __init_sockets_93(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_93() {
}

void __peek_sockets_93() {
}


inline void __push_93_94(float data) {
BUFFER_93_94[HEAD_93_94]=data;
HEAD_93_94++;
}


inline float __pop_92_93() {
float res=BUFFER_92_93[TAIL_92_93];
TAIL_92_93++;
return res;
}

inline float __pop_95_93() {
float res=BUFFER_95_93[TAIL_95_93];
TAIL_95_93++;
return res;
}


void __joiner_93_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_93_94(__pop_92_93());
    __push_93_94(__pop_95_93());
    __push_93_94(__pop_95_93());
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_94;
int __counter_94 = 0;
int __steady_94 = 0;
int __tmp_94 = 0;
int __tmp2_94 = 0;
int *__state_flag_94 = NULL;
thread_info *__thread_94 = NULL;



float numer__533__94 = 0.0f;
float denom_x__534__94 = 0.0f;
float denom_y__535__94 = 0.0f;
float denom__536__94 = 0.0f;
void save_peek_buffer__94(object_write_buffer *buf);
void load_peek_buffer__94(object_write_buffer *buf);
void save_file_pointer__94(object_write_buffer *buf);
void load_file_pointer__94(object_write_buffer *buf);

inline void check_status__94() {
  check_thread_status(__state_flag_94, __thread_94);
}

void check_status_during_io__94() {
  check_thread_status_during_io(__state_flag_94, __thread_94);
}

void __init_thread_info_94(thread_info *info) {
  __state_flag_94 = info->get_state_flag();
}

thread_info *__get_thread_info_94() {
  if (__thread_94 != NULL) return __thread_94;
  __thread_94 = new thread_info(94, check_status_during_io__94);
  __init_thread_info_94(__thread_94);
  return __thread_94;
}

void __declare_sockets_94() {
  init_instance::add_incoming(93,94, DATA_SOCKET);
  init_instance::add_outgoing(94,9, DATA_SOCKET);
}

void __init_sockets_94(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_94() {
}

void __peek_sockets_94() {
}

 
void init_Division__543_162__94();
inline void check_status__94();

void work_Division__543_162__94(int);


inline float __pop_93_94() {
float res=BUFFER_93_94[TAIL_93_94];
TAIL_93_94++;
return res;
}

inline void __pop_93_94(int n) {
TAIL_93_94+=n;
}

inline float __peek_93_94(int offs) {
return BUFFER_93_94[TAIL_93_94+offs];
}



inline void __push_94_9(float data) {
BUFFER_94_9[HEAD_94_9]=data;
HEAD_94_9++;
}



 
void init_Division__543_162__94(){
}
void save_file_pointer__94(object_write_buffer *buf) {}
void load_file_pointer__94(object_write_buffer *buf) {}
 
void work_Division__543_162__94(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__539 = 0.0f;/* float */
      float __tmp4__540 = 0.0f;/* float */
      double __tmp5__541 = 0.0f;/* double */
      double __tmp6__542 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__539 = ((float)0.0))/*float*/;
      (denom_x__534__94) = BUFFER_93_94[TAIL_93_94]; TAIL_93_94++;
;
      (denom_y__535__94) = BUFFER_93_94[TAIL_93_94]; TAIL_93_94++;
;
      (__tmp6__542 = ((double)(((denom_x__534__94) * (denom_y__535__94)))))/*double*/;
      (__tmp5__541 = sqrtf(__tmp6__542))/*double*/;
      (__tmp4__540 = ((float)(__tmp5__541)))/*float*/;
      ((denom__536__94) = __tmp4__540)/*float*/;
      (numer__533__94) = BUFFER_93_94[TAIL_93_94]; TAIL_93_94++;
;

      if (((denom__536__94) != ((float)0.0))) {(v__539 = ((numer__533__94) / (denom__536__94)))/*float*/;
      } else {}
      __push_94_9(v__539);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_95;
int __counter_95 = 0;
int __steady_95 = 0;
int __tmp_95 = 0;
int __tmp2_95 = 0;
int *__state_flag_95 = NULL;
thread_info *__thread_95 = NULL;



float _TheGlobal__pattern__6____95[256] = {0};
float numerSum__525__95 = 0.0f;
float denomPatternSum__526__95 = 0.0f;
float data__527__95 = 0.0f;
float patternData__528__95 = 0.0f;
int i__529__95 = 0;
void save_peek_buffer__95(object_write_buffer *buf);
void load_peek_buffer__95(object_write_buffer *buf);
void save_file_pointer__95(object_write_buffer *buf);
void load_file_pointer__95(object_write_buffer *buf);

inline void check_status__95() {
  check_thread_status(__state_flag_95, __thread_95);
}

void check_status_during_io__95() {
  check_thread_status_during_io(__state_flag_95, __thread_95);
}

void __init_thread_info_95(thread_info *info) {
  __state_flag_95 = info->get_state_flag();
}

thread_info *__get_thread_info_95() {
  if (__thread_95 != NULL) return __thread_95;
  __thread_95 = new thread_info(95, check_status_during_io__95);
  __init_thread_info_95(__thread_95);
  return __thread_95;
}

void __declare_sockets_95() {
  init_instance::add_incoming(91,95, DATA_SOCKET);
  init_instance::add_outgoing(95,93, DATA_SOCKET);
}

void __init_sockets_95(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_95() {
}

void __peek_sockets_95() {
}

 
void init_PatternSums__532_161__95();
inline void check_status__95();

void work_PatternSums__532_161__95(int);


inline float __pop_91_95() {
float res=BUFFER_91_95[TAIL_91_95];
TAIL_91_95++;
return res;
}

inline void __pop_91_95(int n) {
TAIL_91_95+=n;
}

inline float __peek_91_95(int offs) {
return BUFFER_91_95[TAIL_91_95+offs];
}



inline void __push_95_93(float data) {
BUFFER_95_93[HEAD_95_93]=data;
HEAD_95_93++;
}



 
void init_PatternSums__532_161__95(){
  (((_TheGlobal__pattern__6____95)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____95)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__95(object_write_buffer *buf) {}
void load_file_pointer__95(object_write_buffer *buf) {}
 
void work_PatternSums__532_161__95(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__525__95) = ((float)0.0))/*float*/;
      ((denomPatternSum__526__95) = ((float)0.0))/*float*/;
      for (((i__529__95) = 0)/*int*/; ((i__529__95) < 256); ((i__529__95)++)) {{
          ((patternData__528__95) = (((_TheGlobal__pattern__6____95)[(int)(i__529__95)]) - ((float)1.6013207E-4)))/*float*/;
          (data__527__95) = BUFFER_91_95[TAIL_91_95]; TAIL_91_95++;
;
          ((numerSum__525__95) = ((numerSum__525__95) + ((data__527__95) * (patternData__528__95))))/*float*/;
          ((denomPatternSum__526__95) = ((denomPatternSum__526__95) + ((patternData__528__95) * (patternData__528__95))))/*float*/;
        }
      }
      __push_95_93((denomPatternSum__526__95));
      __push_95_93((numerSum__525__95));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_96;
int __counter_96 = 0;
int __steady_96 = 0;
int __tmp_96 = 0;
int __tmp2_96 = 0;
int *__state_flag_96 = NULL;
thread_info *__thread_96 = NULL;



void save_peek_buffer__96(object_write_buffer *buf);
void load_peek_buffer__96(object_write_buffer *buf);
void save_file_pointer__96(object_write_buffer *buf);
void load_file_pointer__96(object_write_buffer *buf);

inline void check_status__96() {
  check_thread_status(__state_flag_96, __thread_96);
}

void check_status_during_io__96() {
  check_thread_status_during_io(__state_flag_96, __thread_96);
}

void __init_thread_info_96(thread_info *info) {
  __state_flag_96 = info->get_state_flag();
}

thread_info *__get_thread_info_96() {
  if (__thread_96 != NULL) return __thread_96;
  __thread_96 = new thread_info(96, check_status_during_io__96);
  __init_thread_info_96(__thread_96);
  return __thread_96;
}

void __declare_sockets_96() {
  init_instance::add_incoming(2,96, DATA_SOCKET);
  init_instance::add_outgoing(96,97, DATA_SOCKET);
}

void __init_sockets_96(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_96() {
}

void __peek_sockets_96() {
}

 
void init_GetAvg__550_164__96();
inline void check_status__96();

void work_GetAvg__550_164__96(int);


inline float __pop_2_96() {
float res=BUFFER_2_96[TAIL_2_96];
TAIL_2_96++;
return res;
}

inline void __pop_2_96(int n) {
TAIL_2_96+=n;
}

inline float __peek_2_96(int offs) {
return BUFFER_2_96[TAIL_2_96+offs];
}



inline void __push_96_97(float data) {
BUFFER_96_97[HEAD_96_97]=data;
HEAD_96_97++;
}



 
void init_GetAvg__550_164__96(){
}
void save_file_pointer__96(object_write_buffer *buf) {}
void load_file_pointer__96(object_write_buffer *buf) {}
 
void work_GetAvg__550_164__96(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__546 = 0;/* int */
      float Sum__547 = 0.0f;/* float */
      float val__548 = 0.0f;/* float */
      float __tmp8__549 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__547 = ((float)0.0))/*float*/;
      for ((i__546 = 0)/*int*/; (i__546 < 256); (i__546++)) {{
          val__548 = BUFFER_2_96[TAIL_2_96]; TAIL_2_96++;
;
          (Sum__547 = (Sum__547 + val__548))/*float*/;
          __push_96_97(val__548);
        }
      }
      (__tmp8__549 = (Sum__547 / ((float)256.0)))/*float*/;
      __push_96_97(__tmp8__549);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_97;
int __counter_97 = 0;
int __steady_97 = 0;
int __tmp_97 = 0;
int __tmp2_97 = 0;
int *__state_flag_97 = NULL;
thread_info *__thread_97 = NULL;



float avg__551__97 = 0.0f;
int i__552__97 = 0;
void save_peek_buffer__97(object_write_buffer *buf);
void load_peek_buffer__97(object_write_buffer *buf);
void save_file_pointer__97(object_write_buffer *buf);
void load_file_pointer__97(object_write_buffer *buf);

inline void check_status__97() {
  check_thread_status(__state_flag_97, __thread_97);
}

void check_status_during_io__97() {
  check_thread_status_during_io(__state_flag_97, __thread_97);
}

void __init_thread_info_97(thread_info *info) {
  __state_flag_97 = info->get_state_flag();
}

thread_info *__get_thread_info_97() {
  if (__thread_97 != NULL) return __thread_97;
  __thread_97 = new thread_info(97, check_status_during_io__97);
  __init_thread_info_97(__thread_97);
  return __thread_97;
}

void __declare_sockets_97() {
  init_instance::add_incoming(96,97, DATA_SOCKET);
  init_instance::add_outgoing(97,98, DATA_SOCKET);
}

void __init_sockets_97(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_97() {
}

void __peek_sockets_97() {
}

 
void init_DataMinusAvg__558_166__97();
inline void check_status__97();

void work_DataMinusAvg__558_166__97(int);


inline float __pop_96_97() {
float res=BUFFER_96_97[TAIL_96_97];
TAIL_96_97++;
return res;
}

inline void __pop_96_97(int n) {
TAIL_96_97+=n;
}

inline float __peek_96_97(int offs) {
return BUFFER_96_97[TAIL_96_97+offs];
}



inline void __push_97_98(float data) {
BUFFER_97_98[HEAD_97_98]=data;
HEAD_97_98++;
}



 
void init_DataMinusAvg__558_166__97(){
}
void save_file_pointer__97(object_write_buffer *buf) {}
void load_file_pointer__97(object_write_buffer *buf) {}
 
void work_DataMinusAvg__558_166__97(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__555 = 0.0f;/* float */
      float v__556 = 0.0f;/* float */
      float __tmp11__557 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__555 = BUFFER_96_97[TAIL_96_97+256];
;
      ((avg__551__97) = __tmp9__555)/*float*/;
      for (((i__552__97) = 0)/*int*/; ((i__552__97) < 256); ((i__552__97)++)) {{
          v__556 = BUFFER_96_97[TAIL_96_97]; TAIL_96_97++;
;
          (__tmp11__557 = (v__556 - (avg__551__97)))/*float*/;
          __push_97_98(__tmp11__557);
        }
      }
      __pop_96_97();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_98;
int __counter_98 = 0;
int __steady_98 = 0;
int __tmp_98 = 0;
int __tmp2_98 = 0;
int *__state_flag_98 = NULL;
thread_info *__thread_98 = NULL;



inline void check_status__98() {
  check_thread_status(__state_flag_98, __thread_98);
}

void check_status_during_io__98() {
  check_thread_status_during_io(__state_flag_98, __thread_98);
}

void __init_thread_info_98(thread_info *info) {
  __state_flag_98 = info->get_state_flag();
}

thread_info *__get_thread_info_98() {
  if (__thread_98 != NULL) return __thread_98;
  __thread_98 = new thread_info(98, check_status_during_io__98);
  __init_thread_info_98(__thread_98);
  return __thread_98;
}

void __declare_sockets_98() {
  init_instance::add_incoming(97,98, DATA_SOCKET);
  init_instance::add_outgoing(98,99, DATA_SOCKET);
  init_instance::add_outgoing(98,102, DATA_SOCKET);
}

void __init_sockets_98(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_98() {
}

void __peek_sockets_98() {
}

inline float __pop_97_98() {
float res=BUFFER_97_98[TAIL_97_98];
TAIL_97_98++;
return res;
}


inline void __push_98_99(float data) {
BUFFER_98_99[HEAD_98_99]=data;
HEAD_98_99++;
}



inline void __push_98_102(float data) {
BUFFER_98_102[HEAD_98_102]=data;
HEAD_98_102++;
}



void __splitter_98_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_97_98[TAIL_97_98]; TAIL_97_98++;
;
  __push_98_99(tmp);
  __push_98_102(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_99;
int __counter_99 = 0;
int __steady_99 = 0;
int __tmp_99 = 0;
int __tmp2_99 = 0;
int *__state_flag_99 = NULL;
thread_info *__thread_99 = NULL;



float data__559__99 = 0.0f;
int i__560__99 = 0;
void save_peek_buffer__99(object_write_buffer *buf);
void load_peek_buffer__99(object_write_buffer *buf);
void save_file_pointer__99(object_write_buffer *buf);
void load_file_pointer__99(object_write_buffer *buf);

inline void check_status__99() {
  check_thread_status(__state_flag_99, __thread_99);
}

void check_status_during_io__99() {
  check_thread_status_during_io(__state_flag_99, __thread_99);
}

void __init_thread_info_99(thread_info *info) {
  __state_flag_99 = info->get_state_flag();
}

thread_info *__get_thread_info_99() {
  if (__thread_99 != NULL) return __thread_99;
  __thread_99 = new thread_info(99, check_status_during_io__99);
  __init_thread_info_99(__thread_99);
  return __thread_99;
}

void __declare_sockets_99() {
  init_instance::add_incoming(98,99, DATA_SOCKET);
  init_instance::add_outgoing(99,100, DATA_SOCKET);
}

void __init_sockets_99(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_99() {
}

void __peek_sockets_99() {
}

 
void init_DenomDataSum__564_168__99();
inline void check_status__99();

void work_DenomDataSum__564_168__99(int);


inline float __pop_98_99() {
float res=BUFFER_98_99[TAIL_98_99];
TAIL_98_99++;
return res;
}

inline void __pop_98_99(int n) {
TAIL_98_99+=n;
}

inline float __peek_98_99(int offs) {
return BUFFER_98_99[TAIL_98_99+offs];
}



inline void __push_99_100(float data) {
BUFFER_99_100[HEAD_99_100]=data;
HEAD_99_100++;
}



 
void init_DenomDataSum__564_168__99(){
}
void save_file_pointer__99(object_write_buffer *buf) {}
void load_file_pointer__99(object_write_buffer *buf) {}
 
void work_DenomDataSum__564_168__99(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__563 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__563 = ((float)0.0))/*float*/;
      for (((i__560__99) = 0)/*int*/; ((i__560__99) < 256); ((i__560__99)++)) {{
          (data__559__99) = BUFFER_98_99[TAIL_98_99]; TAIL_98_99++;
;
          (sum__563 = (sum__563 + ((data__559__99) * (data__559__99))))/*float*/;
        }
      }
      __push_99_100(sum__563);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

