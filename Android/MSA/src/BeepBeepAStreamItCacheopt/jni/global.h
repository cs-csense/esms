#ifndef __GLOBAL_H
#define __GLOBAL_H

#include <math.h>
#include "structs.h"
#include <StreamItVectorLib.h>

#define max(A,B) (((A)>(B))?(A):(B))
#define min(A,B) (((A)<(B))?(A):(B))

extern void __global__init();

extern float __global__FRAMES_PER_SECOND;
extern int __global__PATTERN_SIZE;
extern float __global__PATTERN_AVG;
extern float __global__pattern[256];
#endif // __GLOBAL_H
