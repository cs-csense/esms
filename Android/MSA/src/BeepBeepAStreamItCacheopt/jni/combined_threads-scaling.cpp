#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <netsocket.h>
#include <node_server.h>
#include <init_instance.h>
#include <master_server.h>
#include <save_state.h>
#include <save_manager.h>
#include <delete_chkpts.h>
#include <object_write_buffer.h>
#include <read_setup.h>
#include <ccp.h>
#include <streamit_random.h>
#include "structs.h"
#include "fusion.h"

int __max_iteration;
int __timer_enabled = 0;
int __frequency_of_chkpts;
volatile int __vol;


float BUFFER_0_1[__BUF_SIZE_MASK_0_1 + 1];
int HEAD_0_1 = 0;
int TAIL_0_1 = 0;
float BUFFER_1_2[__BUF_SIZE_MASK_1_2 + 1];
int HEAD_1_2 = 0;
int TAIL_1_2 = 0;
float BUFFER_2_3[__BUF_SIZE_MASK_2_3 + 1];
int HEAD_2_3 = 0;
int TAIL_2_3 = 0;
float BUFFER_2_29[__BUF_SIZE_MASK_2_29 + 1];
int HEAD_2_29 = 0;
int TAIL_2_29 = 0;
float BUFFER_2_35[__BUF_SIZE_MASK_2_35 + 1];
int HEAD_2_35 = 0;
int TAIL_2_35 = 0;
float BUFFER_2_41[__BUF_SIZE_MASK_2_41 + 1];
int HEAD_2_41 = 0;
int TAIL_2_41 = 0;
float BUFFER_2_47[__BUF_SIZE_MASK_2_47 + 1];
int HEAD_2_47 = 0;
int TAIL_2_47 = 0;
float BUFFER_2_53[__BUF_SIZE_MASK_2_53 + 1];
int HEAD_2_53 = 0;
int TAIL_2_53 = 0;
float BUFFER_2_59[__BUF_SIZE_MASK_2_59 + 1];
int HEAD_2_59 = 0;
int TAIL_2_59 = 0;
float BUFFER_2_65[__BUF_SIZE_MASK_2_65 + 1];
int HEAD_2_65 = 0;
int TAIL_2_65 = 0;
float BUFFER_2_71[__BUF_SIZE_MASK_2_71 + 1];
int HEAD_2_71 = 0;
int TAIL_2_71 = 0;
float BUFFER_2_77[__BUF_SIZE_MASK_2_77 + 1];
int HEAD_2_77 = 0;
int TAIL_2_77 = 0;
float BUFFER_2_83[__BUF_SIZE_MASK_2_83 + 1];
int HEAD_2_83 = 0;
int TAIL_2_83 = 0;
float BUFFER_2_89[__BUF_SIZE_MASK_2_89 + 1];
int HEAD_2_89 = 0;
int TAIL_2_89 = 0;
float BUFFER_2_95[__BUF_SIZE_MASK_2_95 + 1];
int HEAD_2_95 = 0;
int TAIL_2_95 = 0;
float BUFFER_2_101[__BUF_SIZE_MASK_2_101 + 1];
int HEAD_2_101 = 0;
int TAIL_2_101 = 0;
float BUFFER_2_107[__BUF_SIZE_MASK_2_107 + 1];
int HEAD_2_107 = 0;
int TAIL_2_107 = 0;
float BUFFER_2_113[__BUF_SIZE_MASK_2_113 + 1];
int HEAD_2_113 = 0;
int TAIL_2_113 = 0;
float BUFFER_3_4[__BUF_SIZE_MASK_3_4 + 1];
int HEAD_3_4 = 0;
int TAIL_3_4 = 0;
float BUFFER_4_5[__BUF_SIZE_MASK_4_5 + 1];
int HEAD_4_5 = 0;
int TAIL_4_5 = 0;
float BUFFER_5_6[__BUF_SIZE_MASK_5_6 + 1];
int HEAD_5_6 = 0;
int TAIL_5_6 = 0;
float BUFFER_5_28[__BUF_SIZE_MASK_5_28 + 1];
int HEAD_5_28 = 0;
int TAIL_5_28 = 0;
float BUFFER_6_7[__BUF_SIZE_MASK_6_7 + 1];
int HEAD_6_7 = 0;
int TAIL_6_7 = 0;
float BUFFER_7_8[__BUF_SIZE_MASK_7_8 + 1];
int HEAD_7_8 = 0;
int TAIL_7_8 = 0;
float BUFFER_8_9[__BUF_SIZE_MASK_8_9 + 1];
int HEAD_8_9 = 0;
int TAIL_8_9 = 0;
float BUFFER_9_10[__BUF_SIZE_MASK_9_10 + 1];
int HEAD_9_10 = 0;
int TAIL_9_10 = 0;
float BUFFER_9_13[__BUF_SIZE_MASK_9_13 + 1];
int HEAD_9_13 = 0;
int TAIL_9_13 = 0;
float BUFFER_9_14[__BUF_SIZE_MASK_9_14 + 1];
int HEAD_9_14 = 0;
int TAIL_9_14 = 0;
float BUFFER_9_15[__BUF_SIZE_MASK_9_15 + 1];
int HEAD_9_15 = 0;
int TAIL_9_15 = 0;
float BUFFER_9_16[__BUF_SIZE_MASK_9_16 + 1];
int HEAD_9_16 = 0;
int TAIL_9_16 = 0;
float BUFFER_9_17[__BUF_SIZE_MASK_9_17 + 1];
int HEAD_9_17 = 0;
int TAIL_9_17 = 0;
float BUFFER_9_18[__BUF_SIZE_MASK_9_18 + 1];
int HEAD_9_18 = 0;
int TAIL_9_18 = 0;
float BUFFER_9_19[__BUF_SIZE_MASK_9_19 + 1];
int HEAD_9_19 = 0;
int TAIL_9_19 = 0;
float BUFFER_9_20[__BUF_SIZE_MASK_9_20 + 1];
int HEAD_9_20 = 0;
int TAIL_9_20 = 0;
float BUFFER_9_21[__BUF_SIZE_MASK_9_21 + 1];
int HEAD_9_21 = 0;
int TAIL_9_21 = 0;
float BUFFER_9_22[__BUF_SIZE_MASK_9_22 + 1];
int HEAD_9_22 = 0;
int TAIL_9_22 = 0;
float BUFFER_9_23[__BUF_SIZE_MASK_9_23 + 1];
int HEAD_9_23 = 0;
int TAIL_9_23 = 0;
float BUFFER_9_24[__BUF_SIZE_MASK_9_24 + 1];
int HEAD_9_24 = 0;
int TAIL_9_24 = 0;
float BUFFER_9_25[__BUF_SIZE_MASK_9_25 + 1];
int HEAD_9_25 = 0;
int TAIL_9_25 = 0;
float BUFFER_9_26[__BUF_SIZE_MASK_9_26 + 1];
int HEAD_9_26 = 0;
int TAIL_9_26 = 0;
float BUFFER_9_27[__BUF_SIZE_MASK_9_27 + 1];
int HEAD_9_27 = 0;
int TAIL_9_27 = 0;
float BUFFER_10_11[__BUF_SIZE_MASK_10_11 + 1];
int HEAD_10_11 = 0;
int TAIL_10_11 = 0;
float BUFFER_11_12[__BUF_SIZE_MASK_11_12 + 1];
int HEAD_11_12 = 0;
int TAIL_11_12 = 0;
float BUFFER_13_11[__BUF_SIZE_MASK_13_11 + 1];
int HEAD_13_11 = 0;
int TAIL_13_11 = 0;
float BUFFER_14_11[__BUF_SIZE_MASK_14_11 + 1];
int HEAD_14_11 = 0;
int TAIL_14_11 = 0;
float BUFFER_15_11[__BUF_SIZE_MASK_15_11 + 1];
int HEAD_15_11 = 0;
int TAIL_15_11 = 0;
float BUFFER_16_11[__BUF_SIZE_MASK_16_11 + 1];
int HEAD_16_11 = 0;
int TAIL_16_11 = 0;
float BUFFER_17_11[__BUF_SIZE_MASK_17_11 + 1];
int HEAD_17_11 = 0;
int TAIL_17_11 = 0;
float BUFFER_18_11[__BUF_SIZE_MASK_18_11 + 1];
int HEAD_18_11 = 0;
int TAIL_18_11 = 0;
float BUFFER_19_11[__BUF_SIZE_MASK_19_11 + 1];
int HEAD_19_11 = 0;
int TAIL_19_11 = 0;
float BUFFER_20_11[__BUF_SIZE_MASK_20_11 + 1];
int HEAD_20_11 = 0;
int TAIL_20_11 = 0;
float BUFFER_21_11[__BUF_SIZE_MASK_21_11 + 1];
int HEAD_21_11 = 0;
int TAIL_21_11 = 0;
float BUFFER_22_11[__BUF_SIZE_MASK_22_11 + 1];
int HEAD_22_11 = 0;
int TAIL_22_11 = 0;
float BUFFER_23_11[__BUF_SIZE_MASK_23_11 + 1];
int HEAD_23_11 = 0;
int TAIL_23_11 = 0;
float BUFFER_24_11[__BUF_SIZE_MASK_24_11 + 1];
int HEAD_24_11 = 0;
int TAIL_24_11 = 0;
float BUFFER_25_11[__BUF_SIZE_MASK_25_11 + 1];
int HEAD_25_11 = 0;
int TAIL_25_11 = 0;
float BUFFER_26_11[__BUF_SIZE_MASK_26_11 + 1];
int HEAD_26_11 = 0;
int TAIL_26_11 = 0;
float BUFFER_27_11[__BUF_SIZE_MASK_27_11 + 1];
int HEAD_27_11 = 0;
int TAIL_27_11 = 0;
float BUFFER_28_7[__BUF_SIZE_MASK_28_7 + 1];
int HEAD_28_7 = 0;
int TAIL_28_7 = 0;
float BUFFER_29_30[__BUF_SIZE_MASK_29_30 + 1];
int HEAD_29_30 = 0;
int TAIL_29_30 = 0;
float BUFFER_30_31[__BUF_SIZE_MASK_30_31 + 1];
int HEAD_30_31 = 0;
int TAIL_30_31 = 0;
float BUFFER_31_32[__BUF_SIZE_MASK_31_32 + 1];
int HEAD_31_32 = 0;
int TAIL_31_32 = 0;
float BUFFER_31_34[__BUF_SIZE_MASK_31_34 + 1];
int HEAD_31_34 = 0;
int TAIL_31_34 = 0;
float BUFFER_32_33[__BUF_SIZE_MASK_32_33 + 1];
int HEAD_32_33 = 0;
int TAIL_32_33 = 0;
float BUFFER_33_8[__BUF_SIZE_MASK_33_8 + 1];
int HEAD_33_8 = 0;
int TAIL_33_8 = 0;
float BUFFER_34_33[__BUF_SIZE_MASK_34_33 + 1];
int HEAD_34_33 = 0;
int TAIL_34_33 = 0;
float BUFFER_35_36[__BUF_SIZE_MASK_35_36 + 1];
int HEAD_35_36 = 0;
int TAIL_35_36 = 0;
float BUFFER_36_37[__BUF_SIZE_MASK_36_37 + 1];
int HEAD_36_37 = 0;
int TAIL_36_37 = 0;
float BUFFER_37_38[__BUF_SIZE_MASK_37_38 + 1];
int HEAD_37_38 = 0;
int TAIL_37_38 = 0;
float BUFFER_37_40[__BUF_SIZE_MASK_37_40 + 1];
int HEAD_37_40 = 0;
int TAIL_37_40 = 0;
float BUFFER_38_39[__BUF_SIZE_MASK_38_39 + 1];
int HEAD_38_39 = 0;
int TAIL_38_39 = 0;
float BUFFER_39_8[__BUF_SIZE_MASK_39_8 + 1];
int HEAD_39_8 = 0;
int TAIL_39_8 = 0;
float BUFFER_40_39[__BUF_SIZE_MASK_40_39 + 1];
int HEAD_40_39 = 0;
int TAIL_40_39 = 0;
float BUFFER_41_42[__BUF_SIZE_MASK_41_42 + 1];
int HEAD_41_42 = 0;
int TAIL_41_42 = 0;
float BUFFER_42_43[__BUF_SIZE_MASK_42_43 + 1];
int HEAD_42_43 = 0;
int TAIL_42_43 = 0;
float BUFFER_43_44[__BUF_SIZE_MASK_43_44 + 1];
int HEAD_43_44 = 0;
int TAIL_43_44 = 0;
float BUFFER_43_46[__BUF_SIZE_MASK_43_46 + 1];
int HEAD_43_46 = 0;
int TAIL_43_46 = 0;
float BUFFER_44_45[__BUF_SIZE_MASK_44_45 + 1];
int HEAD_44_45 = 0;
int TAIL_44_45 = 0;
float BUFFER_45_8[__BUF_SIZE_MASK_45_8 + 1];
int HEAD_45_8 = 0;
int TAIL_45_8 = 0;
float BUFFER_46_45[__BUF_SIZE_MASK_46_45 + 1];
int HEAD_46_45 = 0;
int TAIL_46_45 = 0;
float BUFFER_47_48[__BUF_SIZE_MASK_47_48 + 1];
int HEAD_47_48 = 0;
int TAIL_47_48 = 0;
float BUFFER_48_49[__BUF_SIZE_MASK_48_49 + 1];
int HEAD_48_49 = 0;
int TAIL_48_49 = 0;
float BUFFER_49_50[__BUF_SIZE_MASK_49_50 + 1];
int HEAD_49_50 = 0;
int TAIL_49_50 = 0;
float BUFFER_49_52[__BUF_SIZE_MASK_49_52 + 1];
int HEAD_49_52 = 0;
int TAIL_49_52 = 0;
float BUFFER_50_51[__BUF_SIZE_MASK_50_51 + 1];
int HEAD_50_51 = 0;
int TAIL_50_51 = 0;
float BUFFER_51_8[__BUF_SIZE_MASK_51_8 + 1];
int HEAD_51_8 = 0;
int TAIL_51_8 = 0;
float BUFFER_52_51[__BUF_SIZE_MASK_52_51 + 1];
int HEAD_52_51 = 0;
int TAIL_52_51 = 0;
float BUFFER_53_54[__BUF_SIZE_MASK_53_54 + 1];
int HEAD_53_54 = 0;
int TAIL_53_54 = 0;
float BUFFER_54_55[__BUF_SIZE_MASK_54_55 + 1];
int HEAD_54_55 = 0;
int TAIL_54_55 = 0;
float BUFFER_55_56[__BUF_SIZE_MASK_55_56 + 1];
int HEAD_55_56 = 0;
int TAIL_55_56 = 0;
float BUFFER_55_58[__BUF_SIZE_MASK_55_58 + 1];
int HEAD_55_58 = 0;
int TAIL_55_58 = 0;
float BUFFER_56_57[__BUF_SIZE_MASK_56_57 + 1];
int HEAD_56_57 = 0;
int TAIL_56_57 = 0;
float BUFFER_57_8[__BUF_SIZE_MASK_57_8 + 1];
int HEAD_57_8 = 0;
int TAIL_57_8 = 0;
float BUFFER_58_57[__BUF_SIZE_MASK_58_57 + 1];
int HEAD_58_57 = 0;
int TAIL_58_57 = 0;
float BUFFER_59_60[__BUF_SIZE_MASK_59_60 + 1];
int HEAD_59_60 = 0;
int TAIL_59_60 = 0;
float BUFFER_60_61[__BUF_SIZE_MASK_60_61 + 1];
int HEAD_60_61 = 0;
int TAIL_60_61 = 0;
float BUFFER_61_62[__BUF_SIZE_MASK_61_62 + 1];
int HEAD_61_62 = 0;
int TAIL_61_62 = 0;
float BUFFER_61_64[__BUF_SIZE_MASK_61_64 + 1];
int HEAD_61_64 = 0;
int TAIL_61_64 = 0;
float BUFFER_62_63[__BUF_SIZE_MASK_62_63 + 1];
int HEAD_62_63 = 0;
int TAIL_62_63 = 0;
float BUFFER_63_8[__BUF_SIZE_MASK_63_8 + 1];
int HEAD_63_8 = 0;
int TAIL_63_8 = 0;
float BUFFER_64_63[__BUF_SIZE_MASK_64_63 + 1];
int HEAD_64_63 = 0;
int TAIL_64_63 = 0;
float BUFFER_65_66[__BUF_SIZE_MASK_65_66 + 1];
int HEAD_65_66 = 0;
int TAIL_65_66 = 0;
float BUFFER_66_67[__BUF_SIZE_MASK_66_67 + 1];
int HEAD_66_67 = 0;
int TAIL_66_67 = 0;
float BUFFER_67_68[__BUF_SIZE_MASK_67_68 + 1];
int HEAD_67_68 = 0;
int TAIL_67_68 = 0;
float BUFFER_67_70[__BUF_SIZE_MASK_67_70 + 1];
int HEAD_67_70 = 0;
int TAIL_67_70 = 0;
float BUFFER_68_69[__BUF_SIZE_MASK_68_69 + 1];
int HEAD_68_69 = 0;
int TAIL_68_69 = 0;
float BUFFER_69_8[__BUF_SIZE_MASK_69_8 + 1];
int HEAD_69_8 = 0;
int TAIL_69_8 = 0;
float BUFFER_70_69[__BUF_SIZE_MASK_70_69 + 1];
int HEAD_70_69 = 0;
int TAIL_70_69 = 0;
float BUFFER_71_72[__BUF_SIZE_MASK_71_72 + 1];
int HEAD_71_72 = 0;
int TAIL_71_72 = 0;
float BUFFER_72_73[__BUF_SIZE_MASK_72_73 + 1];
int HEAD_72_73 = 0;
int TAIL_72_73 = 0;
float BUFFER_73_74[__BUF_SIZE_MASK_73_74 + 1];
int HEAD_73_74 = 0;
int TAIL_73_74 = 0;
float BUFFER_73_76[__BUF_SIZE_MASK_73_76 + 1];
int HEAD_73_76 = 0;
int TAIL_73_76 = 0;
float BUFFER_74_75[__BUF_SIZE_MASK_74_75 + 1];
int HEAD_74_75 = 0;
int TAIL_74_75 = 0;
float BUFFER_75_8[__BUF_SIZE_MASK_75_8 + 1];
int HEAD_75_8 = 0;
int TAIL_75_8 = 0;
float BUFFER_76_75[__BUF_SIZE_MASK_76_75 + 1];
int HEAD_76_75 = 0;
int TAIL_76_75 = 0;
float BUFFER_77_78[__BUF_SIZE_MASK_77_78 + 1];
int HEAD_77_78 = 0;
int TAIL_77_78 = 0;
float BUFFER_78_79[__BUF_SIZE_MASK_78_79 + 1];
int HEAD_78_79 = 0;
int TAIL_78_79 = 0;
float BUFFER_79_80[__BUF_SIZE_MASK_79_80 + 1];
int HEAD_79_80 = 0;
int TAIL_79_80 = 0;
float BUFFER_79_82[__BUF_SIZE_MASK_79_82 + 1];
int HEAD_79_82 = 0;
int TAIL_79_82 = 0;
float BUFFER_80_81[__BUF_SIZE_MASK_80_81 + 1];
int HEAD_80_81 = 0;
int TAIL_80_81 = 0;
float BUFFER_81_8[__BUF_SIZE_MASK_81_8 + 1];
int HEAD_81_8 = 0;
int TAIL_81_8 = 0;
float BUFFER_82_81[__BUF_SIZE_MASK_82_81 + 1];
int HEAD_82_81 = 0;
int TAIL_82_81 = 0;
float BUFFER_83_84[__BUF_SIZE_MASK_83_84 + 1];
int HEAD_83_84 = 0;
int TAIL_83_84 = 0;
float BUFFER_84_85[__BUF_SIZE_MASK_84_85 + 1];
int HEAD_84_85 = 0;
int TAIL_84_85 = 0;
float BUFFER_85_86[__BUF_SIZE_MASK_85_86 + 1];
int HEAD_85_86 = 0;
int TAIL_85_86 = 0;
float BUFFER_85_88[__BUF_SIZE_MASK_85_88 + 1];
int HEAD_85_88 = 0;
int TAIL_85_88 = 0;
float BUFFER_86_87[__BUF_SIZE_MASK_86_87 + 1];
int HEAD_86_87 = 0;
int TAIL_86_87 = 0;
float BUFFER_87_8[__BUF_SIZE_MASK_87_8 + 1];
int HEAD_87_8 = 0;
int TAIL_87_8 = 0;
float BUFFER_88_87[__BUF_SIZE_MASK_88_87 + 1];
int HEAD_88_87 = 0;
int TAIL_88_87 = 0;
float BUFFER_89_90[__BUF_SIZE_MASK_89_90 + 1];
int HEAD_89_90 = 0;
int TAIL_89_90 = 0;
float BUFFER_90_91[__BUF_SIZE_MASK_90_91 + 1];
int HEAD_90_91 = 0;
int TAIL_90_91 = 0;
float BUFFER_91_92[__BUF_SIZE_MASK_91_92 + 1];
int HEAD_91_92 = 0;
int TAIL_91_92 = 0;
float BUFFER_91_94[__BUF_SIZE_MASK_91_94 + 1];
int HEAD_91_94 = 0;
int TAIL_91_94 = 0;
float BUFFER_92_93[__BUF_SIZE_MASK_92_93 + 1];
int HEAD_92_93 = 0;
int TAIL_92_93 = 0;
float BUFFER_93_8[__BUF_SIZE_MASK_93_8 + 1];
int HEAD_93_8 = 0;
int TAIL_93_8 = 0;
float BUFFER_94_93[__BUF_SIZE_MASK_94_93 + 1];
int HEAD_94_93 = 0;
int TAIL_94_93 = 0;
float BUFFER_95_96[__BUF_SIZE_MASK_95_96 + 1];
int HEAD_95_96 = 0;
int TAIL_95_96 = 0;
float BUFFER_96_97[__BUF_SIZE_MASK_96_97 + 1];
int HEAD_96_97 = 0;
int TAIL_96_97 = 0;
float BUFFER_97_98[__BUF_SIZE_MASK_97_98 + 1];
int HEAD_97_98 = 0;
int TAIL_97_98 = 0;
float BUFFER_97_100[__BUF_SIZE_MASK_97_100 + 1];
int HEAD_97_100 = 0;
int TAIL_97_100 = 0;
float BUFFER_98_99[__BUF_SIZE_MASK_98_99 + 1];
int HEAD_98_99 = 0;
int TAIL_98_99 = 0;
float BUFFER_99_8[__BUF_SIZE_MASK_99_8 + 1];
int HEAD_99_8 = 0;
int TAIL_99_8 = 0;
float BUFFER_100_99[__BUF_SIZE_MASK_100_99 + 1];
int HEAD_100_99 = 0;
int TAIL_100_99 = 0;
float BUFFER_101_102[__BUF_SIZE_MASK_101_102 + 1];
int HEAD_101_102 = 0;
int TAIL_101_102 = 0;
float BUFFER_102_103[__BUF_SIZE_MASK_102_103 + 1];
int HEAD_102_103 = 0;
int TAIL_102_103 = 0;
float BUFFER_103_104[__BUF_SIZE_MASK_103_104 + 1];
int HEAD_103_104 = 0;
int TAIL_103_104 = 0;
float BUFFER_103_106[__BUF_SIZE_MASK_103_106 + 1];
int HEAD_103_106 = 0;
int TAIL_103_106 = 0;
float BUFFER_104_105[__BUF_SIZE_MASK_104_105 + 1];
int HEAD_104_105 = 0;
int TAIL_104_105 = 0;
float BUFFER_105_8[__BUF_SIZE_MASK_105_8 + 1];
int HEAD_105_8 = 0;
int TAIL_105_8 = 0;
float BUFFER_106_105[__BUF_SIZE_MASK_106_105 + 1];
int HEAD_106_105 = 0;
int TAIL_106_105 = 0;
float BUFFER_107_108[__BUF_SIZE_MASK_107_108 + 1];
int HEAD_107_108 = 0;
int TAIL_107_108 = 0;
float BUFFER_108_109[__BUF_SIZE_MASK_108_109 + 1];
int HEAD_108_109 = 0;
int TAIL_108_109 = 0;
float BUFFER_109_110[__BUF_SIZE_MASK_109_110 + 1];
int HEAD_109_110 = 0;
int TAIL_109_110 = 0;
float BUFFER_109_112[__BUF_SIZE_MASK_109_112 + 1];
int HEAD_109_112 = 0;
int TAIL_109_112 = 0;
float BUFFER_110_111[__BUF_SIZE_MASK_110_111 + 1];
int HEAD_110_111 = 0;
int TAIL_110_111 = 0;
float BUFFER_111_8[__BUF_SIZE_MASK_111_8 + 1];
int HEAD_111_8 = 0;
int TAIL_111_8 = 0;
float BUFFER_112_111[__BUF_SIZE_MASK_112_111 + 1];
int HEAD_112_111 = 0;
int TAIL_112_111 = 0;
float BUFFER_113_114[__BUF_SIZE_MASK_113_114 + 1];
int HEAD_113_114 = 0;
int TAIL_113_114 = 0;
float BUFFER_114_115[__BUF_SIZE_MASK_114_115 + 1];
int HEAD_114_115 = 0;
int TAIL_114_115 = 0;
float BUFFER_115_116[__BUF_SIZE_MASK_115_116 + 1];
int HEAD_115_116 = 0;
int TAIL_115_116 = 0;
float BUFFER_115_118[__BUF_SIZE_MASK_115_118 + 1];
int HEAD_115_118 = 0;
int TAIL_115_118 = 0;
float BUFFER_116_117[__BUF_SIZE_MASK_116_117 + 1];
int HEAD_116_117 = 0;
int TAIL_116_117 = 0;
float BUFFER_117_8[__BUF_SIZE_MASK_117_8 + 1];
int HEAD_117_8 = 0;
int TAIL_117_8 = 0;
float BUFFER_118_117[__BUF_SIZE_MASK_118_117 + 1];
int HEAD_118_117 = 0;
int TAIL_118_117 = 0;
void init_src__5_57__0();
void work_src__5_57__0(int);
void init_Framer__10_58__1();
void work_Framer__10_58__1(int);
void __splitter_2_work(int);
void init_GetAvg__17_60__3();
void work_GetAvg__17_60__3(int);
void init_DataMinusAvg__25_62__4();
void work_DataMinusAvg__25_62__4(int);
void __splitter_5_work(int);
void init_DenomDataSum__31_64__6();
void work_DenomDataSum__31_64__6(int);
void __joiner_7_work(int);
void __joiner_8_work(int);
void __splitter_9_work(int);
void init_Division__51_66__10();
void work_Division__51_66__10(int);
void __joiner_11_work(int);
void init_SecondCounter__671_187__12();
void work_SecondCounter__671_187__12(int);
void init_Division__92_74__13();
void work_Division__92_74__13(int);
void init_Division__133_82__14();
void work_Division__133_82__14(int);
void init_Division__174_90__15();
void work_Division__174_90__15(int);
void init_Division__215_98__16();
void work_Division__215_98__16(int);
void init_Division__256_106__17();
void work_Division__256_106__17(int);
void init_Division__297_114__18();
void work_Division__297_114__18(int);
void init_Division__338_122__19();
void work_Division__338_122__19(int);
void init_Division__379_130__20();
void work_Division__379_130__20(int);
void init_Division__420_138__21();
void work_Division__420_138__21(int);
void init_Division__461_146__22();
void work_Division__461_146__22(int);
void init_Division__502_154__23();
void work_Division__502_154__23(int);
void init_Division__543_162__24();
void work_Division__543_162__24(int);
void init_Division__584_170__25();
void work_Division__584_170__25(int);
void init_Division__625_178__26();
void work_Division__625_178__26(int);
void init_Division__666_186__27();
void work_Division__666_186__27(int);
void init_PatternSums__40_65__28();
void work_PatternSums__40_65__28(int);
void init_GetAvg__58_68__29();
void work_GetAvg__58_68__29(int);
void init_DataMinusAvg__66_70__30();
void work_DataMinusAvg__66_70__30(int);
void __splitter_31_work(int);
void init_DenomDataSum__72_72__32();
void work_DenomDataSum__72_72__32(int);
void __joiner_33_work(int);
void init_PatternSums__81_73__34();
void work_PatternSums__81_73__34(int);
void init_GetAvg__99_76__35();
void work_GetAvg__99_76__35(int);
void init_DataMinusAvg__107_78__36();
void work_DataMinusAvg__107_78__36(int);
void __splitter_37_work(int);
void init_DenomDataSum__113_80__38();
void work_DenomDataSum__113_80__38(int);
void __joiner_39_work(int);
void init_PatternSums__122_81__40();
void work_PatternSums__122_81__40(int);
void init_GetAvg__140_84__41();
void work_GetAvg__140_84__41(int);
void init_DataMinusAvg__148_86__42();
void work_DataMinusAvg__148_86__42(int);
void __splitter_43_work(int);
void init_DenomDataSum__154_88__44();
void work_DenomDataSum__154_88__44(int);
void __joiner_45_work(int);
void init_PatternSums__163_89__46();
void work_PatternSums__163_89__46(int);
void init_GetAvg__181_92__47();
void work_GetAvg__181_92__47(int);
void init_DataMinusAvg__189_94__48();
void work_DataMinusAvg__189_94__48(int);
void __splitter_49_work(int);
void init_DenomDataSum__195_96__50();
void work_DenomDataSum__195_96__50(int);
void __joiner_51_work(int);
void init_PatternSums__204_97__52();
void work_PatternSums__204_97__52(int);
void init_GetAvg__222_100__53();
void work_GetAvg__222_100__53(int);
void init_DataMinusAvg__230_102__54();
void work_DataMinusAvg__230_102__54(int);
void __splitter_55_work(int);
void init_DenomDataSum__236_104__56();
void work_DenomDataSum__236_104__56(int);
void __joiner_57_work(int);
void init_PatternSums__245_105__58();
void work_PatternSums__245_105__58(int);
void init_GetAvg__263_108__59();
void work_GetAvg__263_108__59(int);
void init_DataMinusAvg__271_110__60();
void work_DataMinusAvg__271_110__60(int);
void __splitter_61_work(int);
void init_DenomDataSum__277_112__62();
void work_DenomDataSum__277_112__62(int);
void __joiner_63_work(int);
void init_PatternSums__286_113__64();
void work_PatternSums__286_113__64(int);
void init_GetAvg__304_116__65();
void work_GetAvg__304_116__65(int);
void init_DataMinusAvg__312_118__66();
void work_DataMinusAvg__312_118__66(int);
void __splitter_67_work(int);
void init_DenomDataSum__318_120__68();
void work_DenomDataSum__318_120__68(int);
void __joiner_69_work(int);
void init_PatternSums__327_121__70();
void work_PatternSums__327_121__70(int);
void init_GetAvg__345_124__71();
void work_GetAvg__345_124__71(int);
void init_DataMinusAvg__353_126__72();
void work_DataMinusAvg__353_126__72(int);
void __splitter_73_work(int);
void init_DenomDataSum__359_128__74();
void work_DenomDataSum__359_128__74(int);
void __joiner_75_work(int);
void init_PatternSums__368_129__76();
void work_PatternSums__368_129__76(int);
void init_GetAvg__386_132__77();
void work_GetAvg__386_132__77(int);
void init_DataMinusAvg__394_134__78();
void work_DataMinusAvg__394_134__78(int);
void __splitter_79_work(int);
void init_DenomDataSum__400_136__80();
void work_DenomDataSum__400_136__80(int);
void __joiner_81_work(int);
void init_PatternSums__409_137__82();
void work_PatternSums__409_137__82(int);
void init_GetAvg__427_140__83();
void work_GetAvg__427_140__83(int);
void init_DataMinusAvg__435_142__84();
void work_DataMinusAvg__435_142__84(int);
void __splitter_85_work(int);
void init_DenomDataSum__441_144__86();
void work_DenomDataSum__441_144__86(int);
void __joiner_87_work(int);
void init_PatternSums__450_145__88();
void work_PatternSums__450_145__88(int);
void init_GetAvg__468_148__89();
void work_GetAvg__468_148__89(int);
void init_DataMinusAvg__476_150__90();
void work_DataMinusAvg__476_150__90(int);
void __splitter_91_work(int);
void init_DenomDataSum__482_152__92();
void work_DenomDataSum__482_152__92(int);
void __joiner_93_work(int);
void init_PatternSums__491_153__94();
void work_PatternSums__491_153__94(int);
void init_GetAvg__509_156__95();
void work_GetAvg__509_156__95(int);
void init_DataMinusAvg__517_158__96();
void work_DataMinusAvg__517_158__96(int);
void __splitter_97_work(int);
void init_DenomDataSum__523_160__98();
void work_DenomDataSum__523_160__98(int);
void __joiner_99_work(int);
void init_PatternSums__532_161__100();
void work_PatternSums__532_161__100(int);
void init_GetAvg__550_164__101();
void work_GetAvg__550_164__101(int);
void init_DataMinusAvg__558_166__102();
void work_DataMinusAvg__558_166__102(int);
void __splitter_103_work(int);
void init_DenomDataSum__564_168__104();
void work_DenomDataSum__564_168__104(int);
void __joiner_105_work(int);
void init_PatternSums__573_169__106();
void work_PatternSums__573_169__106(int);
void init_GetAvg__591_172__107();
void work_GetAvg__591_172__107(int);
void init_DataMinusAvg__599_174__108();
void work_DataMinusAvg__599_174__108(int);
void __splitter_109_work(int);
void init_DenomDataSum__605_176__110();
void work_DenomDataSum__605_176__110(int);
void __joiner_111_work(int);
void init_PatternSums__614_177__112();
void work_PatternSums__614_177__112(int);
void init_GetAvg__632_180__113();
void work_GetAvg__632_180__113(int);
void init_DataMinusAvg__640_182__114();
void work_DataMinusAvg__640_182__114(int);
void __splitter_115_work(int);
void init_DenomDataSum__646_184__116();
void work_DenomDataSum__646_184__116(int);
void __joiner_117_work(int);
void init_PatternSums__655_185__118();
void work_PatternSums__655_185__118(int);

static short* samples;

#include "log.h"

void initialization(void* data) {
    samples = (short*)data;
    init_src__5_57__0();   work_src__5_57__0(255);
    init_Framer__10_58__1();
    init_GetAvg__58_68__29();
    init_GetAvg__222_100__53();
    init_GetAvg__386_132__77();
    init_GetAvg__550_164__101();
    init_GetAvg__99_76__35();
    init_GetAvg__263_108__59();
    init_GetAvg__427_140__83();
    init_GetAvg__591_172__107();
    init_GetAvg__140_84__41();
    init_GetAvg__304_116__65();
    init_GetAvg__468_148__89();
    init_GetAvg__632_180__113();
    init_GetAvg__17_60__3();
    init_GetAvg__181_92__47();
    init_GetAvg__345_124__71();
    init_GetAvg__509_156__95();
    init_DataMinusAvg__66_70__30();
    init_DataMinusAvg__230_102__54();
    init_DataMinusAvg__394_134__78();
    init_DataMinusAvg__558_166__102();
    init_DataMinusAvg__107_78__36();
    init_DataMinusAvg__271_110__60();
    init_DataMinusAvg__435_142__84();
    init_DataMinusAvg__599_174__108();
    init_DataMinusAvg__148_86__42();
    init_DataMinusAvg__312_118__66();
    init_DataMinusAvg__476_150__90();
    init_DataMinusAvg__640_182__114();
    init_DataMinusAvg__25_62__4();
    init_DataMinusAvg__189_94__48();
    init_DataMinusAvg__353_126__72();
    init_DataMinusAvg__517_158__96();
    init_DenomDataSum__31_64__6();
    init_DenomDataSum__359_128__74();
    init_PatternSums__40_65__28();
    init_PatternSums__368_129__76();
    init_DenomDataSum__72_72__32();
    init_DenomDataSum__400_136__80();
    init_PatternSums__81_73__34();
    init_PatternSums__409_137__82();
    init_DenomDataSum__113_80__38();
    init_DenomDataSum__441_144__86();
    init_PatternSums__122_81__40();
    init_PatternSums__450_145__88();
    init_DenomDataSum__154_88__44();
    init_DenomDataSum__482_152__92();
    init_PatternSums__163_89__46();
    init_PatternSums__491_153__94();
    init_DenomDataSum__195_96__50();
    init_DenomDataSum__523_160__98();
    init_PatternSums__204_97__52();
    init_PatternSums__532_161__100();
    init_DenomDataSum__236_104__56();
    init_DenomDataSum__564_168__104();
    init_PatternSums__245_105__58();
    init_PatternSums__573_169__106();
    init_DenomDataSum__277_112__62();
    init_DenomDataSum__605_176__110();
    init_PatternSums__286_113__64();
    init_PatternSums__614_177__112();
    init_DenomDataSum__318_120__68();
    init_DenomDataSum__646_184__116();
    init_PatternSums__327_121__70();
    init_PatternSums__655_185__118();
    init_Division__51_66__10();
    init_Division__215_98__16();
    init_Division__379_130__20();
    init_Division__543_162__24();
    init_Division__92_74__13();
    init_Division__256_106__17();
    init_Division__420_138__21();
    init_Division__584_170__25();
    init_Division__133_82__14();
    init_Division__297_114__18();
    init_Division__461_146__22();
    init_Division__625_178__26();
    init_Division__174_90__15();
    init_Division__338_122__19();
    init_Division__502_154__23();
    init_Division__666_186__27();
    init_SecondCounter__671_187__12();
}

static inline void iteration() {
    for (int __y = 0; __y < __PEEK_BUF_SIZE_0_1; __y++) {
      BUFFER_0_1[__y] = BUFFER_0_1[__y + TAIL_0_1];
    }

    HEAD_0_1 -= TAIL_0_1;
    TAIL_0_1 = 0;
        work_src__5_57__0(16*__MULT );
    HEAD_1_2 = 0;
    TAIL_1_2 = 0;
        work_Framer__10_58__1(16*__MULT );
    HEAD_2_3 = 0;
    TAIL_2_3 = 0;
    HEAD_2_29 = 0;
    TAIL_2_29 = 0;
    HEAD_2_35 = 0;
    TAIL_2_35 = 0;
    HEAD_2_41 = 0;
    TAIL_2_41 = 0;
    HEAD_2_47 = 0;
    TAIL_2_47 = 0;
    HEAD_2_53 = 0;
    TAIL_2_53 = 0;
    HEAD_2_59 = 0;
    TAIL_2_59 = 0;
    HEAD_2_65 = 0;
    TAIL_2_65 = 0;
    HEAD_2_71 = 0;
    TAIL_2_71 = 0;
    HEAD_2_77 = 0;
    TAIL_2_77 = 0;
    HEAD_2_83 = 0;
    TAIL_2_83 = 0;
    HEAD_2_89 = 0;
    TAIL_2_89 = 0;
    HEAD_2_95 = 0;
    TAIL_2_95 = 0;
    HEAD_2_101 = 0;
    TAIL_2_101 = 0;
    HEAD_2_107 = 0;
    TAIL_2_107 = 0;
    HEAD_2_113 = 0;
    TAIL_2_113 = 0;
        __splitter_2_work(1*__MULT );
    HEAD_29_30 = 0;
    TAIL_29_30 = 0;
        work_GetAvg__58_68__29(1*__MULT );
    HEAD_53_54 = 0;
    TAIL_53_54 = 0;
        work_GetAvg__222_100__53(1*__MULT );
    HEAD_77_78 = 0;
    TAIL_77_78 = 0;
        work_GetAvg__386_132__77(1*__MULT );
    HEAD_101_102 = 0;
    TAIL_101_102 = 0;
        work_GetAvg__550_164__101(1*__MULT );
    HEAD_35_36 = 0;
    TAIL_35_36 = 0;
        work_GetAvg__99_76__35(1*__MULT );
    HEAD_59_60 = 0;
    TAIL_59_60 = 0;
        work_GetAvg__263_108__59(1*__MULT );
    HEAD_83_84 = 0;
    TAIL_83_84 = 0;
        work_GetAvg__427_140__83(1*__MULT );
    HEAD_107_108 = 0;
    TAIL_107_108 = 0;
        work_GetAvg__591_172__107(1*__MULT );
    HEAD_41_42 = 0;
    TAIL_41_42 = 0;
        work_GetAvg__140_84__41(1*__MULT );
    HEAD_65_66 = 0;
    TAIL_65_66 = 0;
        work_GetAvg__304_116__65(1*__MULT );
    HEAD_89_90 = 0;
    TAIL_89_90 = 0;
        work_GetAvg__468_148__89(1*__MULT );
    HEAD_113_114 = 0;
    TAIL_113_114 = 0;
        work_GetAvg__632_180__113(1*__MULT );
    HEAD_3_4 = 0;
    TAIL_3_4 = 0;
        work_GetAvg__17_60__3(1*__MULT );
    HEAD_47_48 = 0;
    TAIL_47_48 = 0;
        work_GetAvg__181_92__47(1*__MULT );
    HEAD_71_72 = 0;
    TAIL_71_72 = 0;
        work_GetAvg__345_124__71(1*__MULT );
    HEAD_95_96 = 0;
    TAIL_95_96 = 0;
        work_GetAvg__509_156__95(1*__MULT );
    HEAD_30_31 = 0;
    TAIL_30_31 = 0;
        work_DataMinusAvg__66_70__30(1*__MULT );
    HEAD_54_55 = 0;
    TAIL_54_55 = 0;
        work_DataMinusAvg__230_102__54(1*__MULT );
    HEAD_78_79 = 0;
    TAIL_78_79 = 0;
        work_DataMinusAvg__394_134__78(1*__MULT );
    HEAD_102_103 = 0;
    TAIL_102_103 = 0;
        work_DataMinusAvg__558_166__102(1*__MULT );
    HEAD_36_37 = 0;
    TAIL_36_37 = 0;
        work_DataMinusAvg__107_78__36(1*__MULT );
    HEAD_60_61 = 0;
    TAIL_60_61 = 0;
        work_DataMinusAvg__271_110__60(1*__MULT );
    HEAD_84_85 = 0;
    TAIL_84_85 = 0;
        work_DataMinusAvg__435_142__84(1*__MULT );
    HEAD_108_109 = 0;
    TAIL_108_109 = 0;
        work_DataMinusAvg__599_174__108(1*__MULT );
    HEAD_42_43 = 0;
    TAIL_42_43 = 0;
        work_DataMinusAvg__148_86__42(1*__MULT );
    HEAD_66_67 = 0;
    TAIL_66_67 = 0;
        work_DataMinusAvg__312_118__66(1*__MULT );
    HEAD_90_91 = 0;
    TAIL_90_91 = 0;
        work_DataMinusAvg__476_150__90(1*__MULT );
    HEAD_114_115 = 0;
    TAIL_114_115 = 0;
        work_DataMinusAvg__640_182__114(1*__MULT );
    HEAD_4_5 = 0;
    TAIL_4_5 = 0;
        work_DataMinusAvg__25_62__4(1*__MULT );
    HEAD_48_49 = 0;
    TAIL_48_49 = 0;
        work_DataMinusAvg__189_94__48(1*__MULT );
    HEAD_72_73 = 0;
    TAIL_72_73 = 0;
        work_DataMinusAvg__353_126__72(1*__MULT );
    HEAD_96_97 = 0;
    TAIL_96_97 = 0;
        work_DataMinusAvg__517_158__96(1*__MULT );
    HEAD_55_56 = 0;
    TAIL_55_56 = 0;
    HEAD_55_58 = 0;
    TAIL_55_58 = 0;
        __splitter_55_work(256*__MULT );
    HEAD_103_104 = 0;
    TAIL_103_104 = 0;
    HEAD_103_106 = 0;
    TAIL_103_106 = 0;
        __splitter_103_work(256*__MULT );
    HEAD_61_62 = 0;
    TAIL_61_62 = 0;
    HEAD_61_64 = 0;
    TAIL_61_64 = 0;
        __splitter_61_work(256*__MULT );
    HEAD_109_110 = 0;
    TAIL_109_110 = 0;
    HEAD_109_112 = 0;
    TAIL_109_112 = 0;
        __splitter_109_work(256*__MULT );
    HEAD_67_68 = 0;
    TAIL_67_68 = 0;
    HEAD_67_70 = 0;
    TAIL_67_70 = 0;
        __splitter_67_work(256*__MULT );
    HEAD_115_116 = 0;
    TAIL_115_116 = 0;
    HEAD_115_118 = 0;
    TAIL_115_118 = 0;
        __splitter_115_work(256*__MULT );
    HEAD_73_74 = 0;
    TAIL_73_74 = 0;
    HEAD_73_76 = 0;
    TAIL_73_76 = 0;
        __splitter_73_work(256*__MULT );
    HEAD_5_6 = 0;
    TAIL_5_6 = 0;
    HEAD_5_28 = 0;
    TAIL_5_28 = 0;
        __splitter_5_work(256*__MULT );
    HEAD_79_80 = 0;
    TAIL_79_80 = 0;
    HEAD_79_82 = 0;
    TAIL_79_82 = 0;
        __splitter_79_work(256*__MULT );
    HEAD_31_32 = 0;
    TAIL_31_32 = 0;
    HEAD_31_34 = 0;
    TAIL_31_34 = 0;
        __splitter_31_work(256*__MULT );
    HEAD_85_86 = 0;
    TAIL_85_86 = 0;
    HEAD_85_88 = 0;
    TAIL_85_88 = 0;
        __splitter_85_work(256*__MULT );
    HEAD_37_38 = 0;
    TAIL_37_38 = 0;
    HEAD_37_40 = 0;
    TAIL_37_40 = 0;
        __splitter_37_work(256*__MULT );
    HEAD_91_92 = 0;
    TAIL_91_92 = 0;
    HEAD_91_94 = 0;
    TAIL_91_94 = 0;
        __splitter_91_work(256*__MULT );
    HEAD_43_44 = 0;
    TAIL_43_44 = 0;
    HEAD_43_46 = 0;
    TAIL_43_46 = 0;
        __splitter_43_work(256*__MULT );
    HEAD_97_98 = 0;
    TAIL_97_98 = 0;
    HEAD_97_100 = 0;
    TAIL_97_100 = 0;
        __splitter_97_work(256*__MULT );
    HEAD_49_50 = 0;
    TAIL_49_50 = 0;
    HEAD_49_52 = 0;
    TAIL_49_52 = 0;
        __splitter_49_work(256*__MULT );
    HEAD_6_7 = 0;
    TAIL_6_7 = 0;
        work_DenomDataSum__31_64__6(1*__MULT );
    HEAD_74_75 = 0;
    TAIL_74_75 = 0;
        work_DenomDataSum__359_128__74(1*__MULT );
    HEAD_28_7 = 0;
    TAIL_28_7 = 0;
        work_PatternSums__40_65__28(1*__MULT );
    HEAD_76_75 = 0;
    TAIL_76_75 = 0;
        work_PatternSums__368_129__76(1*__MULT );
    HEAD_32_33 = 0;
    TAIL_32_33 = 0;
        work_DenomDataSum__72_72__32(1*__MULT );
    HEAD_80_81 = 0;
    TAIL_80_81 = 0;
        work_DenomDataSum__400_136__80(1*__MULT );
    HEAD_34_33 = 0;
    TAIL_34_33 = 0;
        work_PatternSums__81_73__34(1*__MULT );
    HEAD_82_81 = 0;
    TAIL_82_81 = 0;
        work_PatternSums__409_137__82(1*__MULT );
    HEAD_38_39 = 0;
    TAIL_38_39 = 0;
        work_DenomDataSum__113_80__38(1*__MULT );
    HEAD_86_87 = 0;
    TAIL_86_87 = 0;
        work_DenomDataSum__441_144__86(1*__MULT );
    HEAD_40_39 = 0;
    TAIL_40_39 = 0;
        work_PatternSums__122_81__40(1*__MULT );
    HEAD_88_87 = 0;
    TAIL_88_87 = 0;
        work_PatternSums__450_145__88(1*__MULT );
    HEAD_44_45 = 0;
    TAIL_44_45 = 0;
        work_DenomDataSum__154_88__44(1*__MULT );
    HEAD_92_93 = 0;
    TAIL_92_93 = 0;
        work_DenomDataSum__482_152__92(1*__MULT );
    HEAD_46_45 = 0;
    TAIL_46_45 = 0;
        work_PatternSums__163_89__46(1*__MULT );
    HEAD_94_93 = 0;
    TAIL_94_93 = 0;
        work_PatternSums__491_153__94(1*__MULT );
    HEAD_50_51 = 0;
    TAIL_50_51 = 0;
        work_DenomDataSum__195_96__50(1*__MULT );
    HEAD_98_99 = 0;
    TAIL_98_99 = 0;
        work_DenomDataSum__523_160__98(1*__MULT );
    HEAD_52_51 = 0;
    TAIL_52_51 = 0;
        work_PatternSums__204_97__52(1*__MULT );
    HEAD_100_99 = 0;
    TAIL_100_99 = 0;
        work_PatternSums__532_161__100(1*__MULT );
    HEAD_56_57 = 0;
    TAIL_56_57 = 0;
        work_DenomDataSum__236_104__56(1*__MULT );
    HEAD_104_105 = 0;
    TAIL_104_105 = 0;
        work_DenomDataSum__564_168__104(1*__MULT );
    HEAD_58_57 = 0;
    TAIL_58_57 = 0;
        work_PatternSums__245_105__58(1*__MULT );
    HEAD_106_105 = 0;
    TAIL_106_105 = 0;
        work_PatternSums__573_169__106(1*__MULT );
    HEAD_62_63 = 0;
    TAIL_62_63 = 0;
        work_DenomDataSum__277_112__62(1*__MULT );
    HEAD_110_111 = 0;
    TAIL_110_111 = 0;
        work_DenomDataSum__605_176__110(1*__MULT );
    HEAD_64_63 = 0;
    TAIL_64_63 = 0;
        work_PatternSums__286_113__64(1*__MULT );
    HEAD_112_111 = 0;
    TAIL_112_111 = 0;
        work_PatternSums__614_177__112(1*__MULT );
    HEAD_68_69 = 0;
    TAIL_68_69 = 0;
        work_DenomDataSum__318_120__68(1*__MULT );
    HEAD_116_117 = 0;
    TAIL_116_117 = 0;
        work_DenomDataSum__646_184__116(1*__MULT );
    HEAD_70_69 = 0;
    TAIL_70_69 = 0;
        work_PatternSums__327_121__70(1*__MULT );
    HEAD_118_117 = 0;
    TAIL_118_117 = 0;
        work_PatternSums__655_185__118(1*__MULT );
    HEAD_51_8 = 0;
    TAIL_51_8 = 0;
        __joiner_51_work(1*__MULT );
    HEAD_99_8 = 0;
    TAIL_99_8 = 0;
        __joiner_99_work(1*__MULT );
    HEAD_57_8 = 0;
    TAIL_57_8 = 0;
        __joiner_57_work(1*__MULT );
    HEAD_105_8 = 0;
    TAIL_105_8 = 0;
        __joiner_105_work(1*__MULT );
    HEAD_63_8 = 0;
    TAIL_63_8 = 0;
        __joiner_63_work(1*__MULT );
    HEAD_111_8 = 0;
    TAIL_111_8 = 0;
        __joiner_111_work(1*__MULT );
    HEAD_69_8 = 0;
    TAIL_69_8 = 0;
        __joiner_69_work(1*__MULT );
    HEAD_117_8 = 0;
    TAIL_117_8 = 0;
        __joiner_117_work(1*__MULT );
    HEAD_75_8 = 0;
    TAIL_75_8 = 0;
        __joiner_75_work(1*__MULT );
    HEAD_7_8 = 0;
    TAIL_7_8 = 0;
        __joiner_7_work(1*__MULT );
    HEAD_81_8 = 0;
    TAIL_81_8 = 0;
        __joiner_81_work(1*__MULT );
    HEAD_33_8 = 0;
    TAIL_33_8 = 0;
        __joiner_33_work(1*__MULT );
    HEAD_87_8 = 0;
    TAIL_87_8 = 0;
        __joiner_87_work(1*__MULT );
    HEAD_39_8 = 0;
    TAIL_39_8 = 0;
        __joiner_39_work(1*__MULT );
    HEAD_93_8 = 0;
    TAIL_93_8 = 0;
        __joiner_93_work(1*__MULT );
    HEAD_45_8 = 0;
    TAIL_45_8 = 0;
        __joiner_45_work(1*__MULT );
    HEAD_8_9 = 0;
    TAIL_8_9 = 0;
        __joiner_8_work(1*__MULT );
    HEAD_9_10 = 0;
    TAIL_9_10 = 0;
    HEAD_9_13 = 0;
    TAIL_9_13 = 0;
    HEAD_9_14 = 0;
    TAIL_9_14 = 0;
    HEAD_9_15 = 0;
    TAIL_9_15 = 0;
    HEAD_9_16 = 0;
    TAIL_9_16 = 0;
    HEAD_9_17 = 0;
    TAIL_9_17 = 0;
    HEAD_9_18 = 0;
    TAIL_9_18 = 0;
    HEAD_9_19 = 0;
    TAIL_9_19 = 0;
    HEAD_9_20 = 0;
    TAIL_9_20 = 0;
    HEAD_9_21 = 0;
    TAIL_9_21 = 0;
    HEAD_9_22 = 0;
    TAIL_9_22 = 0;
    HEAD_9_23 = 0;
    TAIL_9_23 = 0;
    HEAD_9_24 = 0;
    TAIL_9_24 = 0;
    HEAD_9_25 = 0;
    TAIL_9_25 = 0;
    HEAD_9_26 = 0;
    TAIL_9_26 = 0;
    HEAD_9_27 = 0;
    TAIL_9_27 = 0;
        __splitter_9_work(1*__MULT );
    HEAD_10_11 = 0;
    TAIL_10_11 = 0;
        work_Division__51_66__10(1*__MULT );
    HEAD_16_11 = 0;
    TAIL_16_11 = 0;
        work_Division__215_98__16(1*__MULT );
    HEAD_20_11 = 0;
    TAIL_20_11 = 0;
        work_Division__379_130__20(1*__MULT );
    HEAD_24_11 = 0;
    TAIL_24_11 = 0;
        work_Division__543_162__24(1*__MULT );
    HEAD_13_11 = 0;
    TAIL_13_11 = 0;
        work_Division__92_74__13(1*__MULT );
    HEAD_17_11 = 0;
    TAIL_17_11 = 0;
        work_Division__256_106__17(1*__MULT );
    HEAD_21_11 = 0;
    TAIL_21_11 = 0;
        work_Division__420_138__21(1*__MULT );
    HEAD_25_11 = 0;
    TAIL_25_11 = 0;
        work_Division__584_170__25(1*__MULT );
    HEAD_14_11 = 0;
    TAIL_14_11 = 0;
        work_Division__133_82__14(1*__MULT );
    HEAD_18_11 = 0;
    TAIL_18_11 = 0;
        work_Division__297_114__18(1*__MULT );
    HEAD_22_11 = 0;
    TAIL_22_11 = 0;
        work_Division__461_146__22(1*__MULT );
    HEAD_26_11 = 0;
    TAIL_26_11 = 0;
        work_Division__625_178__26(1*__MULT );
    HEAD_15_11 = 0;
    TAIL_15_11 = 0;
        work_Division__174_90__15(1*__MULT );
    HEAD_19_11 = 0;
    TAIL_19_11 = 0;
        work_Division__338_122__19(1*__MULT );
    HEAD_23_11 = 0;
    TAIL_23_11 = 0;
        work_Division__502_154__23(1*__MULT );
    HEAD_27_11 = 0;
    TAIL_27_11 = 0;
        work_Division__666_186__27(1*__MULT );
    HEAD_11_12 = 0;
    TAIL_11_12 = 0;
        __joiner_11_work(1*__MULT );
        work_SecondCounter__671_187__12(16*__MULT );
}

void iterations(int periods, void* data) {
    samples = (short*)data;
    for(int p = 0; p < periods; p++)
        iteration();
}

int main(int argc, char **argv) {
  read_setup::read_setup_file();
  __max_iteration = read_setup::max_iteration;
  for (int a = 1; a < argc; a++) {
    if (argc > a + 1 && strcmp(argv[a], "-i") == 0) {
      int tmp;
      sscanf(argv[a + 1], "%d", &tmp);
#ifdef VERBOSE
#endif
      __max_iteration = tmp;
    }
    if (strcmp(argv[a], "-t") == 0) {
#ifdef VERBOSE
#endif
      __timer_enabled = 1;
    }
  }
// number of phases: 13


  // ============= Initialization =============
  // ============= Steady State =============
  return 0;
}

// moved or inserted by concat_cluster_threads.pl
#include <message.h>
message *__msg_stack_117;
message *__msg_stack_97;
message *__msg_stack_38;
message *__msg_stack_103;
message *__msg_stack_105;
message *__msg_stack_10;
message *__msg_stack_70;
message *__msg_stack_81;
message *__msg_stack_42;
message *__msg_stack_84;
message *__msg_stack_91;
message *__msg_stack_9;
message *__msg_stack_56;
message *__msg_stack_47;
message *__msg_stack_31;
message *__msg_stack_83;
message *__msg_stack_107;
message *__msg_stack_95;
message *__msg_stack_102;
message *__msg_stack_65;
message *__msg_stack_55;
message *__msg_stack_14;
message *__msg_stack_101;
message *__msg_stack_77;
message *__msg_stack_19;
message *__msg_stack_2;
message *__msg_stack_66;
message *__msg_stack_52;
message *__msg_stack_118;
message *__msg_stack_23;
message *__msg_stack_100;
message *__msg_stack_73;
message *__msg_stack_30;
message *__msg_stack_87;
message *__msg_stack_7;
message *__msg_stack_6;
message *__msg_stack_78;
message *__msg_stack_63;
message *__msg_stack_0;
message *__msg_stack_90;
message *__msg_stack_27;
message *__msg_stack_86;
message *__msg_stack_76;
message *__msg_stack_75;
message *__msg_stack_11;
message *__msg_stack_13;
message *__msg_stack_18;
message *__msg_stack_93;
message *__msg_stack_85;
message *__msg_stack_82;
message *__msg_stack_57;
message *__msg_stack_43;
message *__msg_stack_60;
message *__msg_stack_37;
message *__msg_stack_114;
message *__msg_stack_108;
message *__msg_stack_34;
message *__msg_stack_113;
message *__msg_stack_112;
message *__msg_stack_3;
message *__msg_stack_64;
message *__msg_stack_24;
message *__msg_stack_106;
message *__msg_stack_59;
message *__msg_stack_46;
message *__msg_stack_116;
message *__msg_stack_40;
message *__msg_stack_69;
message *__msg_stack_99;
message *__msg_stack_49;
message *__msg_stack_1;
message *__msg_stack_111;
message *__msg_stack_67;
message *__msg_stack_5;
message *__msg_stack_15;
message *__msg_stack_45;
message *__msg_stack_71;
message *__msg_stack_17;
message *__msg_stack_92;
message *__msg_stack_21;
message *__msg_stack_74;
message *__msg_stack_58;
message *__msg_stack_41;
message *__msg_stack_48;
message *__msg_stack_44;
message *__msg_stack_36;
message *__msg_stack_26;
message *__msg_stack_29;
message *__msg_stack_89;
message *__msg_stack_68;
message *__msg_stack_62;
message *__msg_stack_35;
message *__msg_stack_50;
message *__msg_stack_25;
message *__msg_stack_20;
message *__msg_stack_4;
message *__msg_stack_110;
message *__msg_stack_98;
message *__msg_stack_53;
message *__msg_stack_80;
message *__msg_stack_61;
message *__msg_stack_33;
message *__msg_stack_94;
message *__msg_stack_8;
message *__msg_stack_109;
message *__msg_stack_32;
message *__msg_stack_12;
message *__msg_stack_115;
message *__msg_stack_28;
message *__msg_stack_104;
message *__msg_stack_51;
message *__msg_stack_16;
message *__msg_stack_88;
message *__msg_stack_79;
message *__msg_stack_54;
message *__msg_stack_22;
message *__msg_stack_39;
message *__msg_stack_96;
message *__msg_stack_72;

// end of moved or inserted by concat_cluster_threads.pl

// peek: 0 pop: 0 push 1
// init counts: 255 steady counts: 16

// ClusterFusion isEliminated: false


#include <mysocket.h>
#include <sdep.h>
#include <thread_info.h>
#include <consumer2.h>
#include <consumer2p.h>
#include <producer2.h>
#include "cluster.h"
#include "global.h"

int __number_of_iterations_0;
int __counter_0 = 0;
int __steady_0 = 0;
int __tmp_0 = 0;
int __tmp2_0 = 0;
int *__state_flag_0 = NULL;
thread_info *__thread_0 = NULL;



void save_file_pointer__0(object_write_buffer *buf);
void load_file_pointer__0(object_write_buffer *buf);

inline void check_status__0() {
  check_thread_status(__state_flag_0, __thread_0);
}

void check_status_during_io__0() {
  check_thread_status_during_io(__state_flag_0, __thread_0);
}

void __init_thread_info_0(thread_info *info) {
  __state_flag_0 = info->get_state_flag();
}

thread_info *__get_thread_info_0() {
  if (__thread_0 != NULL) return __thread_0;
  __thread_0 = new thread_info(0, check_status_during_io__0);
  __init_thread_info_0(__thread_0);
  return __thread_0;
}

void __declare_sockets_0() {
  init_instance::add_outgoing(0,1, DATA_SOCKET);
}

void __init_sockets_0(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_0() {
}

void __peek_sockets_0() {
}

 
float short2float__0__0(int sample__3); 
void init_src__5_57__0();
inline void check_status__0();

void work_src__5_57__0(int);



inline void __push_0_1(float data) {
BUFFER_0_1[HEAD_0_1]=data;
HEAD_0_1++;
}



 
float short2float__0__0(int sample__3){
  return ((float)(sample__3));
}
 
void init_src__5_57__0(){
}
void save_file_pointer__0(object_write_buffer *buf) {}
void load_file_pointer__0(object_write_buffer *buf) {}
 
void work_src__5_57__0(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp2__4 = 0.0f;/* float */

      // mark begin: SIRFilter src

      (__tmp2__4 = short2float__0__0(*samples++))/*float*/;
      __push_0_1(__tmp2__4);
      // mark end: SIRFilter src

    }
  }
}

// peek: 256 pop: 1 push 256
// init counts: 0 steady counts: 16

// ClusterFusion isEliminated: false



int __number_of_iterations_1;
int __counter_1 = 0;
int __steady_1 = 0;
int __tmp_1 = 0;
int __tmp2_1 = 0;
int *__state_flag_1 = NULL;
thread_info *__thread_1 = NULL;



int i__6__1 = 0;
void save_peek_buffer__1(object_write_buffer *buf);
void load_peek_buffer__1(object_write_buffer *buf);
void save_file_pointer__1(object_write_buffer *buf);
void load_file_pointer__1(object_write_buffer *buf);

inline void check_status__1() {
  check_thread_status(__state_flag_1, __thread_1);
}

void check_status_during_io__1() {
  check_thread_status_during_io(__state_flag_1, __thread_1);
}

void __init_thread_info_1(thread_info *info) {
  __state_flag_1 = info->get_state_flag();
}

thread_info *__get_thread_info_1() {
  if (__thread_1 != NULL) return __thread_1;
  __thread_1 = new thread_info(1, check_status_during_io__1);
  __init_thread_info_1(__thread_1);
  return __thread_1;
}

void __declare_sockets_1() {
  init_instance::add_incoming(0,1, DATA_SOCKET);
  init_instance::add_outgoing(1,2, DATA_SOCKET);
}

void __init_sockets_1(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_1() {
}

void __peek_sockets_1() {
}

 
void init_Framer__10_58__1();
inline void check_status__1();

void work_Framer__10_58__1(int);


inline float __pop_0_1() {
float res=BUFFER_0_1[TAIL_0_1];
TAIL_0_1++;
return res;
}

inline void __pop_0_1(int n) {
TAIL_0_1+=n;
}

inline float __peek_0_1(int offs) {
return BUFFER_0_1[TAIL_0_1+offs];
}



inline void __push_1_2(float data) {
BUFFER_1_2[HEAD_1_2]=data;
HEAD_1_2++;
}



 
void init_Framer__10_58__1(){
}
void save_file_pointer__1(object_write_buffer *buf) {}
void load_file_pointer__1(object_write_buffer *buf) {}
 
void work_Framer__10_58__1(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp7__9 = 0.0f;/* float */

      // mark begin: SIRFilter Framer

      for (((i__6__1) = 0)/*int*/; ((i__6__1) < 256); ((i__6__1)++)) {{
          __tmp7__9 = BUFFER_0_1[TAIL_0_1+(i__6__1)];
;
          __push_1_2(__tmp7__9);
        }
      }
      __pop_0_1();
      // mark end: SIRFilter Framer

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_10;
int __counter_10 = 0;
int __steady_10 = 0;
int __tmp_10 = 0;
int __tmp2_10 = 0;
int *__state_flag_10 = NULL;
thread_info *__thread_10 = NULL;



float numer__41__10 = 0.0f;
float denom_x__42__10 = 0.0f;
float denom_y__43__10 = 0.0f;
float denom__44__10 = 0.0f;
void save_peek_buffer__10(object_write_buffer *buf);
void load_peek_buffer__10(object_write_buffer *buf);
void save_file_pointer__10(object_write_buffer *buf);
void load_file_pointer__10(object_write_buffer *buf);

inline void check_status__10() {
  check_thread_status(__state_flag_10, __thread_10);
}

void check_status_during_io__10() {
  check_thread_status_during_io(__state_flag_10, __thread_10);
}

void __init_thread_info_10(thread_info *info) {
  __state_flag_10 = info->get_state_flag();
}

thread_info *__get_thread_info_10() {
  if (__thread_10 != NULL) return __thread_10;
  __thread_10 = new thread_info(10, check_status_during_io__10);
  __init_thread_info_10(__thread_10);
  return __thread_10;
}

void __declare_sockets_10() {
  init_instance::add_incoming(9,10, DATA_SOCKET);
  init_instance::add_outgoing(10,11, DATA_SOCKET);
}

void __init_sockets_10(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_10() {
}

void __peek_sockets_10() {
}

 
void init_Division__51_66__10();
inline void check_status__10();

void work_Division__51_66__10(int);


inline float __pop_9_10() {
float res=BUFFER_9_10[TAIL_9_10];
TAIL_9_10++;
return res;
}

inline void __pop_9_10(int n) {
TAIL_9_10+=n;
}

inline float __peek_9_10(int offs) {
return BUFFER_9_10[TAIL_9_10+offs];
}



inline void __push_10_11(float data) {
BUFFER_10_11[HEAD_10_11]=data;
HEAD_10_11++;
}



 
void init_Division__51_66__10(){
}
void save_file_pointer__10(object_write_buffer *buf) {}
void load_file_pointer__10(object_write_buffer *buf) {}
 
void work_Division__51_66__10(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__47 = 0.0f;/* float */
      float __tmp4__48 = 0.0f;/* float */
      double __tmp5__49 = 0.0f;/* double */
      double __tmp6__50 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__47 = ((float)0.0))/*float*/;
      (denom_x__42__10) = BUFFER_9_10[TAIL_9_10]; TAIL_9_10++;
;
      (denom_y__43__10) = BUFFER_9_10[TAIL_9_10]; TAIL_9_10++;
;
      (__tmp6__50 = ((double)(((denom_x__42__10) * (denom_y__43__10)))))/*double*/;
      (__tmp5__49 = sqrtf(__tmp6__50))/*double*/;
      (__tmp4__48 = ((float)(__tmp5__49)))/*float*/;
      ((denom__44__10) = __tmp4__48)/*float*/;
      (numer__41__10) = BUFFER_9_10[TAIL_9_10]; TAIL_9_10++;
;

      if (((denom__44__10) != ((float)0.0))) {(v__47 = ((numer__41__10) / (denom__44__10)))/*float*/;
      } else {}
      __push_10_11(v__47);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_100;
int __counter_100 = 0;
int __steady_100 = 0;
int __tmp_100 = 0;
int __tmp2_100 = 0;
int *__state_flag_100 = NULL;
thread_info *__thread_100 = NULL;



float _TheGlobal__pattern__6____100[256] = {0};
float numerSum__525__100 = 0.0f;
float denomPatternSum__526__100 = 0.0f;
float data__527__100 = 0.0f;
float patternData__528__100 = 0.0f;
int i__529__100 = 0;
void save_peek_buffer__100(object_write_buffer *buf);
void load_peek_buffer__100(object_write_buffer *buf);
void save_file_pointer__100(object_write_buffer *buf);
void load_file_pointer__100(object_write_buffer *buf);

inline void check_status__100() {
  check_thread_status(__state_flag_100, __thread_100);
}

void check_status_during_io__100() {
  check_thread_status_during_io(__state_flag_100, __thread_100);
}

void __init_thread_info_100(thread_info *info) {
  __state_flag_100 = info->get_state_flag();
}

thread_info *__get_thread_info_100() {
  if (__thread_100 != NULL) return __thread_100;
  __thread_100 = new thread_info(100, check_status_during_io__100);
  __init_thread_info_100(__thread_100);
  return __thread_100;
}

void __declare_sockets_100() {
  init_instance::add_incoming(97,100, DATA_SOCKET);
  init_instance::add_outgoing(100,99, DATA_SOCKET);
}

void __init_sockets_100(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_100() {
}

void __peek_sockets_100() {
}

 
void init_PatternSums__532_161__100();
inline void check_status__100();

void work_PatternSums__532_161__100(int);


inline float __pop_97_100() {
float res=BUFFER_97_100[TAIL_97_100];
TAIL_97_100++;
return res;
}

inline void __pop_97_100(int n) {
TAIL_97_100+=n;
}

inline float __peek_97_100(int offs) {
return BUFFER_97_100[TAIL_97_100+offs];
}



inline void __push_100_99(float data) {
BUFFER_100_99[HEAD_100_99]=data;
HEAD_100_99++;
}



 
void init_PatternSums__532_161__100(){
  (((_TheGlobal__pattern__6____100)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____100)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__100(object_write_buffer *buf) {}
void load_file_pointer__100(object_write_buffer *buf) {}
 
void work_PatternSums__532_161__100(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__525__100) = ((float)0.0))/*float*/;
      ((denomPatternSum__526__100) = ((float)0.0))/*float*/;
      for (((i__529__100) = 0)/*int*/; ((i__529__100) < 256); ((i__529__100)++)) {{
          ((patternData__528__100) = (((_TheGlobal__pattern__6____100)[(int)(i__529__100)]) - ((float)1.6013207E-4)))/*float*/;
          (data__527__100) = BUFFER_97_100[TAIL_97_100]; TAIL_97_100++;
;
          ((numerSum__525__100) = ((numerSum__525__100) + ((data__527__100) * (patternData__528__100))))/*float*/;
          ((denomPatternSum__526__100) = ((denomPatternSum__526__100) + ((patternData__528__100) * (patternData__528__100))))/*float*/;
        }
      }
      __push_100_99((denomPatternSum__526__100));
      __push_100_99((numerSum__525__100));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_101;
int __counter_101 = 0;
int __steady_101 = 0;
int __tmp_101 = 0;
int __tmp2_101 = 0;
int *__state_flag_101 = NULL;
thread_info *__thread_101 = NULL;



void save_peek_buffer__101(object_write_buffer *buf);
void load_peek_buffer__101(object_write_buffer *buf);
void save_file_pointer__101(object_write_buffer *buf);
void load_file_pointer__101(object_write_buffer *buf);

inline void check_status__101() {
  check_thread_status(__state_flag_101, __thread_101);
}

void check_status_during_io__101() {
  check_thread_status_during_io(__state_flag_101, __thread_101);
}

void __init_thread_info_101(thread_info *info) {
  __state_flag_101 = info->get_state_flag();
}

thread_info *__get_thread_info_101() {
  if (__thread_101 != NULL) return __thread_101;
  __thread_101 = new thread_info(101, check_status_during_io__101);
  __init_thread_info_101(__thread_101);
  return __thread_101;
}

void __declare_sockets_101() {
  init_instance::add_incoming(2,101, DATA_SOCKET);
  init_instance::add_outgoing(101,102, DATA_SOCKET);
}

void __init_sockets_101(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_101() {
}

void __peek_sockets_101() {
}

 
void init_GetAvg__550_164__101();
inline void check_status__101();

void work_GetAvg__550_164__101(int);


inline float __pop_2_101() {
float res=BUFFER_2_101[TAIL_2_101];
TAIL_2_101++;
return res;
}

inline void __pop_2_101(int n) {
TAIL_2_101+=n;
}

inline float __peek_2_101(int offs) {
return BUFFER_2_101[TAIL_2_101+offs];
}



inline void __push_101_102(float data) {
BUFFER_101_102[HEAD_101_102]=data;
HEAD_101_102++;
}



 
void init_GetAvg__550_164__101(){
}
void save_file_pointer__101(object_write_buffer *buf) {}
void load_file_pointer__101(object_write_buffer *buf) {}
 
void work_GetAvg__550_164__101(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__546 = 0;/* int */
      float Sum__547 = 0.0f;/* float */
      float val__548 = 0.0f;/* float */
      float __tmp8__549 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__547 = ((float)0.0))/*float*/;
      for ((i__546 = 0)/*int*/; (i__546 < 256); (i__546++)) {{
          val__548 = BUFFER_2_101[TAIL_2_101]; TAIL_2_101++;
;
          (Sum__547 = (Sum__547 + val__548))/*float*/;
          __push_101_102(val__548);
        }
      }
      (__tmp8__549 = (Sum__547 / ((float)256.0)))/*float*/;
      __push_101_102(__tmp8__549);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_102;
int __counter_102 = 0;
int __steady_102 = 0;
int __tmp_102 = 0;
int __tmp2_102 = 0;
int *__state_flag_102 = NULL;
thread_info *__thread_102 = NULL;



float avg__551__102 = 0.0f;
int i__552__102 = 0;
void save_peek_buffer__102(object_write_buffer *buf);
void load_peek_buffer__102(object_write_buffer *buf);
void save_file_pointer__102(object_write_buffer *buf);
void load_file_pointer__102(object_write_buffer *buf);

inline void check_status__102() {
  check_thread_status(__state_flag_102, __thread_102);
}

void check_status_during_io__102() {
  check_thread_status_during_io(__state_flag_102, __thread_102);
}

void __init_thread_info_102(thread_info *info) {
  __state_flag_102 = info->get_state_flag();
}

thread_info *__get_thread_info_102() {
  if (__thread_102 != NULL) return __thread_102;
  __thread_102 = new thread_info(102, check_status_during_io__102);
  __init_thread_info_102(__thread_102);
  return __thread_102;
}

void __declare_sockets_102() {
  init_instance::add_incoming(101,102, DATA_SOCKET);
  init_instance::add_outgoing(102,103, DATA_SOCKET);
}

void __init_sockets_102(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_102() {
}

void __peek_sockets_102() {
}

 
void init_DataMinusAvg__558_166__102();
inline void check_status__102();

void work_DataMinusAvg__558_166__102(int);


inline float __pop_101_102() {
float res=BUFFER_101_102[TAIL_101_102];
TAIL_101_102++;
return res;
}

inline void __pop_101_102(int n) {
TAIL_101_102+=n;
}

inline float __peek_101_102(int offs) {
return BUFFER_101_102[TAIL_101_102+offs];
}



inline void __push_102_103(float data) {
BUFFER_102_103[HEAD_102_103]=data;
HEAD_102_103++;
}



 
void init_DataMinusAvg__558_166__102(){
}
void save_file_pointer__102(object_write_buffer *buf) {}
void load_file_pointer__102(object_write_buffer *buf) {}
 
void work_DataMinusAvg__558_166__102(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__555 = 0.0f;/* float */
      float v__556 = 0.0f;/* float */
      float __tmp11__557 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__555 = BUFFER_101_102[TAIL_101_102+256];
;
      ((avg__551__102) = __tmp9__555)/*float*/;
      for (((i__552__102) = 0)/*int*/; ((i__552__102) < 256); ((i__552__102)++)) {{
          v__556 = BUFFER_101_102[TAIL_101_102]; TAIL_101_102++;
;
          (__tmp11__557 = (v__556 - (avg__551__102)))/*float*/;
          __push_102_103(__tmp11__557);
        }
      }
      __pop_101_102();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_103;
int __counter_103 = 0;
int __steady_103 = 0;
int __tmp_103 = 0;
int __tmp2_103 = 0;
int *__state_flag_103 = NULL;
thread_info *__thread_103 = NULL;



inline void check_status__103() {
  check_thread_status(__state_flag_103, __thread_103);
}

void check_status_during_io__103() {
  check_thread_status_during_io(__state_flag_103, __thread_103);
}

void __init_thread_info_103(thread_info *info) {
  __state_flag_103 = info->get_state_flag();
}

thread_info *__get_thread_info_103() {
  if (__thread_103 != NULL) return __thread_103;
  __thread_103 = new thread_info(103, check_status_during_io__103);
  __init_thread_info_103(__thread_103);
  return __thread_103;
}

void __declare_sockets_103() {
  init_instance::add_incoming(102,103, DATA_SOCKET);
  init_instance::add_outgoing(103,104, DATA_SOCKET);
  init_instance::add_outgoing(103,106, DATA_SOCKET);
}

void __init_sockets_103(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_103() {
}

void __peek_sockets_103() {
}

inline float __pop_102_103() {
float res=BUFFER_102_103[TAIL_102_103];
TAIL_102_103++;
return res;
}


inline void __push_103_104(float data) {
BUFFER_103_104[HEAD_103_104]=data;
HEAD_103_104++;
}



inline void __push_103_106(float data) {
BUFFER_103_106[HEAD_103_106]=data;
HEAD_103_106++;
}



void __splitter_103_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_102_103[TAIL_102_103]; TAIL_102_103++;
;
  __push_103_104(tmp);
  __push_103_106(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_104;
int __counter_104 = 0;
int __steady_104 = 0;
int __tmp_104 = 0;
int __tmp2_104 = 0;
int *__state_flag_104 = NULL;
thread_info *__thread_104 = NULL;



float data__559__104 = 0.0f;
int i__560__104 = 0;
void save_peek_buffer__104(object_write_buffer *buf);
void load_peek_buffer__104(object_write_buffer *buf);
void save_file_pointer__104(object_write_buffer *buf);
void load_file_pointer__104(object_write_buffer *buf);

inline void check_status__104() {
  check_thread_status(__state_flag_104, __thread_104);
}

void check_status_during_io__104() {
  check_thread_status_during_io(__state_flag_104, __thread_104);
}

void __init_thread_info_104(thread_info *info) {
  __state_flag_104 = info->get_state_flag();
}

thread_info *__get_thread_info_104() {
  if (__thread_104 != NULL) return __thread_104;
  __thread_104 = new thread_info(104, check_status_during_io__104);
  __init_thread_info_104(__thread_104);
  return __thread_104;
}

void __declare_sockets_104() {
  init_instance::add_incoming(103,104, DATA_SOCKET);
  init_instance::add_outgoing(104,105, DATA_SOCKET);
}

void __init_sockets_104(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_104() {
}

void __peek_sockets_104() {
}

 
void init_DenomDataSum__564_168__104();
inline void check_status__104();

void work_DenomDataSum__564_168__104(int);


inline float __pop_103_104() {
float res=BUFFER_103_104[TAIL_103_104];
TAIL_103_104++;
return res;
}

inline void __pop_103_104(int n) {
TAIL_103_104+=n;
}

inline float __peek_103_104(int offs) {
return BUFFER_103_104[TAIL_103_104+offs];
}



inline void __push_104_105(float data) {
BUFFER_104_105[HEAD_104_105]=data;
HEAD_104_105++;
}



 
void init_DenomDataSum__564_168__104(){
}
void save_file_pointer__104(object_write_buffer *buf) {}
void load_file_pointer__104(object_write_buffer *buf) {}
 
void work_DenomDataSum__564_168__104(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__563 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__563 = ((float)0.0))/*float*/;
      for (((i__560__104) = 0)/*int*/; ((i__560__104) < 256); ((i__560__104)++)) {{
          (data__559__104) = BUFFER_103_104[TAIL_103_104]; TAIL_103_104++;
;
          (sum__563 = (sum__563 + ((data__559__104) * (data__559__104))))/*float*/;
        }
      }
      __push_104_105(sum__563);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_105;
int __counter_105 = 0;
int __steady_105 = 0;
int __tmp_105 = 0;
int __tmp2_105 = 0;
int *__state_flag_105 = NULL;
thread_info *__thread_105 = NULL;



inline void check_status__105() {
  check_thread_status(__state_flag_105, __thread_105);
}

void check_status_during_io__105() {
  check_thread_status_during_io(__state_flag_105, __thread_105);
}

void __init_thread_info_105(thread_info *info) {
  __state_flag_105 = info->get_state_flag();
}

thread_info *__get_thread_info_105() {
  if (__thread_105 != NULL) return __thread_105;
  __thread_105 = new thread_info(105, check_status_during_io__105);
  __init_thread_info_105(__thread_105);
  return __thread_105;
}

void __declare_sockets_105() {
  init_instance::add_incoming(104,105, DATA_SOCKET);
  init_instance::add_incoming(106,105, DATA_SOCKET);
  init_instance::add_outgoing(105,8, DATA_SOCKET);
}

void __init_sockets_105(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_105() {
}

void __peek_sockets_105() {
}


inline void __push_105_8(float data) {
BUFFER_105_8[HEAD_105_8]=data;
HEAD_105_8++;
}


inline float __pop_104_105() {
float res=BUFFER_104_105[TAIL_104_105];
TAIL_104_105++;
return res;
}

inline float __pop_106_105() {
float res=BUFFER_106_105[TAIL_106_105];
TAIL_106_105++;
return res;
}


void __joiner_105_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_105_8(__pop_104_105());
    __push_105_8(__pop_106_105());
    __push_105_8(__pop_106_105());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_106;
int __counter_106 = 0;
int __steady_106 = 0;
int __tmp_106 = 0;
int __tmp2_106 = 0;
int *__state_flag_106 = NULL;
thread_info *__thread_106 = NULL;



float _TheGlobal__pattern__6____106[256] = {0};
float numerSum__566__106 = 0.0f;
float denomPatternSum__567__106 = 0.0f;
float data__568__106 = 0.0f;
float patternData__569__106 = 0.0f;
int i__570__106 = 0;
void save_peek_buffer__106(object_write_buffer *buf);
void load_peek_buffer__106(object_write_buffer *buf);
void save_file_pointer__106(object_write_buffer *buf);
void load_file_pointer__106(object_write_buffer *buf);

inline void check_status__106() {
  check_thread_status(__state_flag_106, __thread_106);
}

void check_status_during_io__106() {
  check_thread_status_during_io(__state_flag_106, __thread_106);
}

void __init_thread_info_106(thread_info *info) {
  __state_flag_106 = info->get_state_flag();
}

thread_info *__get_thread_info_106() {
  if (__thread_106 != NULL) return __thread_106;
  __thread_106 = new thread_info(106, check_status_during_io__106);
  __init_thread_info_106(__thread_106);
  return __thread_106;
}

void __declare_sockets_106() {
  init_instance::add_incoming(103,106, DATA_SOCKET);
  init_instance::add_outgoing(106,105, DATA_SOCKET);
}

void __init_sockets_106(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_106() {
}

void __peek_sockets_106() {
}

 
void init_PatternSums__573_169__106();
inline void check_status__106();

void work_PatternSums__573_169__106(int);


inline float __pop_103_106() {
float res=BUFFER_103_106[TAIL_103_106];
TAIL_103_106++;
return res;
}

inline void __pop_103_106(int n) {
TAIL_103_106+=n;
}

inline float __peek_103_106(int offs) {
return BUFFER_103_106[TAIL_103_106+offs];
}



inline void __push_106_105(float data) {
BUFFER_106_105[HEAD_106_105]=data;
HEAD_106_105++;
}



 
void init_PatternSums__573_169__106(){
  (((_TheGlobal__pattern__6____106)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____106)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__106(object_write_buffer *buf) {}
void load_file_pointer__106(object_write_buffer *buf) {}
 
void work_PatternSums__573_169__106(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__566__106) = ((float)0.0))/*float*/;
      ((denomPatternSum__567__106) = ((float)0.0))/*float*/;
      for (((i__570__106) = 0)/*int*/; ((i__570__106) < 256); ((i__570__106)++)) {{
          ((patternData__569__106) = (((_TheGlobal__pattern__6____106)[(int)(i__570__106)]) - ((float)1.6013207E-4)))/*float*/;
          (data__568__106) = BUFFER_103_106[TAIL_103_106]; TAIL_103_106++;
;
          ((numerSum__566__106) = ((numerSum__566__106) + ((data__568__106) * (patternData__569__106))))/*float*/;
          ((denomPatternSum__567__106) = ((denomPatternSum__567__106) + ((patternData__569__106) * (patternData__569__106))))/*float*/;
        }
      }
      __push_106_105((denomPatternSum__567__106));
      __push_106_105((numerSum__566__106));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_107;
int __counter_107 = 0;
int __steady_107 = 0;
int __tmp_107 = 0;
int __tmp2_107 = 0;
int *__state_flag_107 = NULL;
thread_info *__thread_107 = NULL;



void save_peek_buffer__107(object_write_buffer *buf);
void load_peek_buffer__107(object_write_buffer *buf);
void save_file_pointer__107(object_write_buffer *buf);
void load_file_pointer__107(object_write_buffer *buf);

inline void check_status__107() {
  check_thread_status(__state_flag_107, __thread_107);
}

void check_status_during_io__107() {
  check_thread_status_during_io(__state_flag_107, __thread_107);
}

void __init_thread_info_107(thread_info *info) {
  __state_flag_107 = info->get_state_flag();
}

thread_info *__get_thread_info_107() {
  if (__thread_107 != NULL) return __thread_107;
  __thread_107 = new thread_info(107, check_status_during_io__107);
  __init_thread_info_107(__thread_107);
  return __thread_107;
}

void __declare_sockets_107() {
  init_instance::add_incoming(2,107, DATA_SOCKET);
  init_instance::add_outgoing(107,108, DATA_SOCKET);
}

void __init_sockets_107(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_107() {
}

void __peek_sockets_107() {
}

 
void init_GetAvg__591_172__107();
inline void check_status__107();

void work_GetAvg__591_172__107(int);


inline float __pop_2_107() {
float res=BUFFER_2_107[TAIL_2_107];
TAIL_2_107++;
return res;
}

inline void __pop_2_107(int n) {
TAIL_2_107+=n;
}

inline float __peek_2_107(int offs) {
return BUFFER_2_107[TAIL_2_107+offs];
}



inline void __push_107_108(float data) {
BUFFER_107_108[HEAD_107_108]=data;
HEAD_107_108++;
}



 
void init_GetAvg__591_172__107(){
}
void save_file_pointer__107(object_write_buffer *buf) {}
void load_file_pointer__107(object_write_buffer *buf) {}
 
void work_GetAvg__591_172__107(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__587 = 0;/* int */
      float Sum__588 = 0.0f;/* float */
      float val__589 = 0.0f;/* float */
      float __tmp8__590 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__588 = ((float)0.0))/*float*/;
      for ((i__587 = 0)/*int*/; (i__587 < 256); (i__587++)) {{
          val__589 = BUFFER_2_107[TAIL_2_107]; TAIL_2_107++;
;
          (Sum__588 = (Sum__588 + val__589))/*float*/;
          __push_107_108(val__589);
        }
      }
      (__tmp8__590 = (Sum__588 / ((float)256.0)))/*float*/;
      __push_107_108(__tmp8__590);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_108;
int __counter_108 = 0;
int __steady_108 = 0;
int __tmp_108 = 0;
int __tmp2_108 = 0;
int *__state_flag_108 = NULL;
thread_info *__thread_108 = NULL;



float avg__592__108 = 0.0f;
int i__593__108 = 0;
void save_peek_buffer__108(object_write_buffer *buf);
void load_peek_buffer__108(object_write_buffer *buf);
void save_file_pointer__108(object_write_buffer *buf);
void load_file_pointer__108(object_write_buffer *buf);

inline void check_status__108() {
  check_thread_status(__state_flag_108, __thread_108);
}

void check_status_during_io__108() {
  check_thread_status_during_io(__state_flag_108, __thread_108);
}

void __init_thread_info_108(thread_info *info) {
  __state_flag_108 = info->get_state_flag();
}

thread_info *__get_thread_info_108() {
  if (__thread_108 != NULL) return __thread_108;
  __thread_108 = new thread_info(108, check_status_during_io__108);
  __init_thread_info_108(__thread_108);
  return __thread_108;
}

void __declare_sockets_108() {
  init_instance::add_incoming(107,108, DATA_SOCKET);
  init_instance::add_outgoing(108,109, DATA_SOCKET);
}

void __init_sockets_108(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_108() {
}

void __peek_sockets_108() {
}

 
void init_DataMinusAvg__599_174__108();
inline void check_status__108();

void work_DataMinusAvg__599_174__108(int);


inline float __pop_107_108() {
float res=BUFFER_107_108[TAIL_107_108];
TAIL_107_108++;
return res;
}

inline void __pop_107_108(int n) {
TAIL_107_108+=n;
}

inline float __peek_107_108(int offs) {
return BUFFER_107_108[TAIL_107_108+offs];
}



inline void __push_108_109(float data) {
BUFFER_108_109[HEAD_108_109]=data;
HEAD_108_109++;
}



 
void init_DataMinusAvg__599_174__108(){
}
void save_file_pointer__108(object_write_buffer *buf) {}
void load_file_pointer__108(object_write_buffer *buf) {}
 
void work_DataMinusAvg__599_174__108(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__596 = 0.0f;/* float */
      float v__597 = 0.0f;/* float */
      float __tmp11__598 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__596 = BUFFER_107_108[TAIL_107_108+256];
;
      ((avg__592__108) = __tmp9__596)/*float*/;
      for (((i__593__108) = 0)/*int*/; ((i__593__108) < 256); ((i__593__108)++)) {{
          v__597 = BUFFER_107_108[TAIL_107_108]; TAIL_107_108++;
;
          (__tmp11__598 = (v__597 - (avg__592__108)))/*float*/;
          __push_108_109(__tmp11__598);
        }
      }
      __pop_107_108();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_109;
int __counter_109 = 0;
int __steady_109 = 0;
int __tmp_109 = 0;
int __tmp2_109 = 0;
int *__state_flag_109 = NULL;
thread_info *__thread_109 = NULL;



inline void check_status__109() {
  check_thread_status(__state_flag_109, __thread_109);
}

void check_status_during_io__109() {
  check_thread_status_during_io(__state_flag_109, __thread_109);
}

void __init_thread_info_109(thread_info *info) {
  __state_flag_109 = info->get_state_flag();
}

thread_info *__get_thread_info_109() {
  if (__thread_109 != NULL) return __thread_109;
  __thread_109 = new thread_info(109, check_status_during_io__109);
  __init_thread_info_109(__thread_109);
  return __thread_109;
}

void __declare_sockets_109() {
  init_instance::add_incoming(108,109, DATA_SOCKET);
  init_instance::add_outgoing(109,110, DATA_SOCKET);
  init_instance::add_outgoing(109,112, DATA_SOCKET);
}

void __init_sockets_109(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_109() {
}

void __peek_sockets_109() {
}

inline float __pop_108_109() {
float res=BUFFER_108_109[TAIL_108_109];
TAIL_108_109++;
return res;
}


inline void __push_109_110(float data) {
BUFFER_109_110[HEAD_109_110]=data;
HEAD_109_110++;
}



inline void __push_109_112(float data) {
BUFFER_109_112[HEAD_109_112]=data;
HEAD_109_112++;
}



void __splitter_109_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_108_109[TAIL_108_109]; TAIL_108_109++;
;
  __push_109_110(tmp);
  __push_109_112(tmp);
  }
}


// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_11;
int __counter_11 = 0;
int __steady_11 = 0;
int __tmp_11 = 0;
int __tmp2_11 = 0;
int *__state_flag_11 = NULL;
thread_info *__thread_11 = NULL;



inline void check_status__11() {
  check_thread_status(__state_flag_11, __thread_11);
}

void check_status_during_io__11() {
  check_thread_status_during_io(__state_flag_11, __thread_11);
}

void __init_thread_info_11(thread_info *info) {
  __state_flag_11 = info->get_state_flag();
}

thread_info *__get_thread_info_11() {
  if (__thread_11 != NULL) return __thread_11;
  __thread_11 = new thread_info(11, check_status_during_io__11);
  __init_thread_info_11(__thread_11);
  return __thread_11;
}

void __declare_sockets_11() {
  init_instance::add_incoming(10,11, DATA_SOCKET);
  init_instance::add_incoming(13,11, DATA_SOCKET);
  init_instance::add_incoming(14,11, DATA_SOCKET);
  init_instance::add_incoming(15,11, DATA_SOCKET);
  init_instance::add_incoming(16,11, DATA_SOCKET);
  init_instance::add_incoming(17,11, DATA_SOCKET);
  init_instance::add_incoming(18,11, DATA_SOCKET);
  init_instance::add_incoming(19,11, DATA_SOCKET);
  init_instance::add_incoming(20,11, DATA_SOCKET);
  init_instance::add_incoming(21,11, DATA_SOCKET);
  init_instance::add_incoming(22,11, DATA_SOCKET);
  init_instance::add_incoming(23,11, DATA_SOCKET);
  init_instance::add_incoming(24,11, DATA_SOCKET);
  init_instance::add_incoming(25,11, DATA_SOCKET);
  init_instance::add_incoming(26,11, DATA_SOCKET);
  init_instance::add_incoming(27,11, DATA_SOCKET);
  init_instance::add_outgoing(11,12, DATA_SOCKET);
}

void __init_sockets_11(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_11() {
}

void __peek_sockets_11() {
}


inline void __push_11_12(float data) {
BUFFER_11_12[HEAD_11_12]=data;
HEAD_11_12++;
}


inline float __pop_10_11() {
float res=BUFFER_10_11[TAIL_10_11];
TAIL_10_11++;
return res;
}

inline float __pop_13_11() {
float res=BUFFER_13_11[TAIL_13_11];
TAIL_13_11++;
return res;
}

inline float __pop_14_11() {
float res=BUFFER_14_11[TAIL_14_11];
TAIL_14_11++;
return res;
}

inline float __pop_15_11() {
float res=BUFFER_15_11[TAIL_15_11];
TAIL_15_11++;
return res;
}

inline float __pop_16_11() {
float res=BUFFER_16_11[TAIL_16_11];
TAIL_16_11++;
return res;
}

inline float __pop_17_11() {
float res=BUFFER_17_11[TAIL_17_11];
TAIL_17_11++;
return res;
}

inline float __pop_18_11() {
float res=BUFFER_18_11[TAIL_18_11];
TAIL_18_11++;
return res;
}

inline float __pop_19_11() {
float res=BUFFER_19_11[TAIL_19_11];
TAIL_19_11++;
return res;
}

inline float __pop_20_11() {
float res=BUFFER_20_11[TAIL_20_11];
TAIL_20_11++;
return res;
}

inline float __pop_21_11() {
float res=BUFFER_21_11[TAIL_21_11];
TAIL_21_11++;
return res;
}

inline float __pop_22_11() {
float res=BUFFER_22_11[TAIL_22_11];
TAIL_22_11++;
return res;
}

inline float __pop_23_11() {
float res=BUFFER_23_11[TAIL_23_11];
TAIL_23_11++;
return res;
}

inline float __pop_24_11() {
float res=BUFFER_24_11[TAIL_24_11];
TAIL_24_11++;
return res;
}

inline float __pop_25_11() {
float res=BUFFER_25_11[TAIL_25_11];
TAIL_25_11++;
return res;
}

inline float __pop_26_11() {
float res=BUFFER_26_11[TAIL_26_11];
TAIL_26_11++;
return res;
}

inline float __pop_27_11() {
float res=BUFFER_27_11[TAIL_27_11];
TAIL_27_11++;
return res;
}


void __joiner_11_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_11_12(__pop_10_11());
    __push_11_12(__pop_13_11());
    __push_11_12(__pop_14_11());
    __push_11_12(__pop_15_11());
    __push_11_12(__pop_16_11());
    __push_11_12(__pop_17_11());
    __push_11_12(__pop_18_11());
    __push_11_12(__pop_19_11());
    __push_11_12(__pop_20_11());
    __push_11_12(__pop_21_11());
    __push_11_12(__pop_22_11());
    __push_11_12(__pop_23_11());
    __push_11_12(__pop_24_11());
    __push_11_12(__pop_25_11());
    __push_11_12(__pop_26_11());
    __push_11_12(__pop_27_11());
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_110;
int __counter_110 = 0;
int __steady_110 = 0;
int __tmp_110 = 0;
int __tmp2_110 = 0;
int *__state_flag_110 = NULL;
thread_info *__thread_110 = NULL;



float data__600__110 = 0.0f;
int i__601__110 = 0;
void save_peek_buffer__110(object_write_buffer *buf);
void load_peek_buffer__110(object_write_buffer *buf);
void save_file_pointer__110(object_write_buffer *buf);
void load_file_pointer__110(object_write_buffer *buf);

inline void check_status__110() {
  check_thread_status(__state_flag_110, __thread_110);
}

void check_status_during_io__110() {
  check_thread_status_during_io(__state_flag_110, __thread_110);
}

void __init_thread_info_110(thread_info *info) {
  __state_flag_110 = info->get_state_flag();
}

thread_info *__get_thread_info_110() {
  if (__thread_110 != NULL) return __thread_110;
  __thread_110 = new thread_info(110, check_status_during_io__110);
  __init_thread_info_110(__thread_110);
  return __thread_110;
}

void __declare_sockets_110() {
  init_instance::add_incoming(109,110, DATA_SOCKET);
  init_instance::add_outgoing(110,111, DATA_SOCKET);
}

void __init_sockets_110(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_110() {
}

void __peek_sockets_110() {
}

 
void init_DenomDataSum__605_176__110();
inline void check_status__110();

void work_DenomDataSum__605_176__110(int);


inline float __pop_109_110() {
float res=BUFFER_109_110[TAIL_109_110];
TAIL_109_110++;
return res;
}

inline void __pop_109_110(int n) {
TAIL_109_110+=n;
}

inline float __peek_109_110(int offs) {
return BUFFER_109_110[TAIL_109_110+offs];
}



inline void __push_110_111(float data) {
BUFFER_110_111[HEAD_110_111]=data;
HEAD_110_111++;
}



 
void init_DenomDataSum__605_176__110(){
}
void save_file_pointer__110(object_write_buffer *buf) {}
void load_file_pointer__110(object_write_buffer *buf) {}
 
void work_DenomDataSum__605_176__110(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__604 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__604 = ((float)0.0))/*float*/;
      for (((i__601__110) = 0)/*int*/; ((i__601__110) < 256); ((i__601__110)++)) {{
          (data__600__110) = BUFFER_109_110[TAIL_109_110]; TAIL_109_110++;
;
          (sum__604 = (sum__604 + ((data__600__110) * (data__600__110))))/*float*/;
        }
      }
      __push_110_111(sum__604);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_111;
int __counter_111 = 0;
int __steady_111 = 0;
int __tmp_111 = 0;
int __tmp2_111 = 0;
int *__state_flag_111 = NULL;
thread_info *__thread_111 = NULL;



inline void check_status__111() {
  check_thread_status(__state_flag_111, __thread_111);
}

void check_status_during_io__111() {
  check_thread_status_during_io(__state_flag_111, __thread_111);
}

void __init_thread_info_111(thread_info *info) {
  __state_flag_111 = info->get_state_flag();
}

thread_info *__get_thread_info_111() {
  if (__thread_111 != NULL) return __thread_111;
  __thread_111 = new thread_info(111, check_status_during_io__111);
  __init_thread_info_111(__thread_111);
  return __thread_111;
}

void __declare_sockets_111() {
  init_instance::add_incoming(110,111, DATA_SOCKET);
  init_instance::add_incoming(112,111, DATA_SOCKET);
  init_instance::add_outgoing(111,8, DATA_SOCKET);
}

void __init_sockets_111(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_111() {
}

void __peek_sockets_111() {
}


inline void __push_111_8(float data) {
BUFFER_111_8[HEAD_111_8]=data;
HEAD_111_8++;
}


inline float __pop_110_111() {
float res=BUFFER_110_111[TAIL_110_111];
TAIL_110_111++;
return res;
}

inline float __pop_112_111() {
float res=BUFFER_112_111[TAIL_112_111];
TAIL_112_111++;
return res;
}


void __joiner_111_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_111_8(__pop_110_111());
    __push_111_8(__pop_112_111());
    __push_111_8(__pop_112_111());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_112;
int __counter_112 = 0;
int __steady_112 = 0;
int __tmp_112 = 0;
int __tmp2_112 = 0;
int *__state_flag_112 = NULL;
thread_info *__thread_112 = NULL;



float _TheGlobal__pattern__6____112[256] = {0};
float numerSum__607__112 = 0.0f;
float denomPatternSum__608__112 = 0.0f;
float data__609__112 = 0.0f;
float patternData__610__112 = 0.0f;
int i__611__112 = 0;
void save_peek_buffer__112(object_write_buffer *buf);
void load_peek_buffer__112(object_write_buffer *buf);
void save_file_pointer__112(object_write_buffer *buf);
void load_file_pointer__112(object_write_buffer *buf);

inline void check_status__112() {
  check_thread_status(__state_flag_112, __thread_112);
}

void check_status_during_io__112() {
  check_thread_status_during_io(__state_flag_112, __thread_112);
}

void __init_thread_info_112(thread_info *info) {
  __state_flag_112 = info->get_state_flag();
}

thread_info *__get_thread_info_112() {
  if (__thread_112 != NULL) return __thread_112;
  __thread_112 = new thread_info(112, check_status_during_io__112);
  __init_thread_info_112(__thread_112);
  return __thread_112;
}

void __declare_sockets_112() {
  init_instance::add_incoming(109,112, DATA_SOCKET);
  init_instance::add_outgoing(112,111, DATA_SOCKET);
}

void __init_sockets_112(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_112() {
}

void __peek_sockets_112() {
}

 
void init_PatternSums__614_177__112();
inline void check_status__112();

void work_PatternSums__614_177__112(int);


inline float __pop_109_112() {
float res=BUFFER_109_112[TAIL_109_112];
TAIL_109_112++;
return res;
}

inline void __pop_109_112(int n) {
TAIL_109_112+=n;
}

inline float __peek_109_112(int offs) {
return BUFFER_109_112[TAIL_109_112+offs];
}



inline void __push_112_111(float data) {
BUFFER_112_111[HEAD_112_111]=data;
HEAD_112_111++;
}



 
void init_PatternSums__614_177__112(){
  (((_TheGlobal__pattern__6____112)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____112)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__112(object_write_buffer *buf) {}
void load_file_pointer__112(object_write_buffer *buf) {}
 
void work_PatternSums__614_177__112(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__607__112) = ((float)0.0))/*float*/;
      ((denomPatternSum__608__112) = ((float)0.0))/*float*/;
      for (((i__611__112) = 0)/*int*/; ((i__611__112) < 256); ((i__611__112)++)) {{
          ((patternData__610__112) = (((_TheGlobal__pattern__6____112)[(int)(i__611__112)]) - ((float)1.6013207E-4)))/*float*/;
          (data__609__112) = BUFFER_109_112[TAIL_109_112]; TAIL_109_112++;
;
          ((numerSum__607__112) = ((numerSum__607__112) + ((data__609__112) * (patternData__610__112))))/*float*/;
          ((denomPatternSum__608__112) = ((denomPatternSum__608__112) + ((patternData__610__112) * (patternData__610__112))))/*float*/;
        }
      }
      __push_112_111((denomPatternSum__608__112));
      __push_112_111((numerSum__607__112));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_113;
int __counter_113 = 0;
int __steady_113 = 0;
int __tmp_113 = 0;
int __tmp2_113 = 0;
int *__state_flag_113 = NULL;
thread_info *__thread_113 = NULL;



void save_peek_buffer__113(object_write_buffer *buf);
void load_peek_buffer__113(object_write_buffer *buf);
void save_file_pointer__113(object_write_buffer *buf);
void load_file_pointer__113(object_write_buffer *buf);

inline void check_status__113() {
  check_thread_status(__state_flag_113, __thread_113);
}

void check_status_during_io__113() {
  check_thread_status_during_io(__state_flag_113, __thread_113);
}

void __init_thread_info_113(thread_info *info) {
  __state_flag_113 = info->get_state_flag();
}

thread_info *__get_thread_info_113() {
  if (__thread_113 != NULL) return __thread_113;
  __thread_113 = new thread_info(113, check_status_during_io__113);
  __init_thread_info_113(__thread_113);
  return __thread_113;
}

void __declare_sockets_113() {
  init_instance::add_incoming(2,113, DATA_SOCKET);
  init_instance::add_outgoing(113,114, DATA_SOCKET);
}

void __init_sockets_113(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_113() {
}

void __peek_sockets_113() {
}

 
void init_GetAvg__632_180__113();
inline void check_status__113();

void work_GetAvg__632_180__113(int);


inline float __pop_2_113() {
float res=BUFFER_2_113[TAIL_2_113];
TAIL_2_113++;
return res;
}

inline void __pop_2_113(int n) {
TAIL_2_113+=n;
}

inline float __peek_2_113(int offs) {
return BUFFER_2_113[TAIL_2_113+offs];
}



inline void __push_113_114(float data) {
BUFFER_113_114[HEAD_113_114]=data;
HEAD_113_114++;
}



 
void init_GetAvg__632_180__113(){
}
void save_file_pointer__113(object_write_buffer *buf) {}
void load_file_pointer__113(object_write_buffer *buf) {}
 
void work_GetAvg__632_180__113(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__628 = 0;/* int */
      float Sum__629 = 0.0f;/* float */
      float val__630 = 0.0f;/* float */
      float __tmp8__631 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__629 = ((float)0.0))/*float*/;
      for ((i__628 = 0)/*int*/; (i__628 < 256); (i__628++)) {{
          val__630 = BUFFER_2_113[TAIL_2_113]; TAIL_2_113++;
;
          (Sum__629 = (Sum__629 + val__630))/*float*/;
          __push_113_114(val__630);
        }
      }
      (__tmp8__631 = (Sum__629 / ((float)256.0)))/*float*/;
      __push_113_114(__tmp8__631);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_114;
int __counter_114 = 0;
int __steady_114 = 0;
int __tmp_114 = 0;
int __tmp2_114 = 0;
int *__state_flag_114 = NULL;
thread_info *__thread_114 = NULL;



float avg__633__114 = 0.0f;
int i__634__114 = 0;
void save_peek_buffer__114(object_write_buffer *buf);
void load_peek_buffer__114(object_write_buffer *buf);
void save_file_pointer__114(object_write_buffer *buf);
void load_file_pointer__114(object_write_buffer *buf);

inline void check_status__114() {
  check_thread_status(__state_flag_114, __thread_114);
}

void check_status_during_io__114() {
  check_thread_status_during_io(__state_flag_114, __thread_114);
}

void __init_thread_info_114(thread_info *info) {
  __state_flag_114 = info->get_state_flag();
}

thread_info *__get_thread_info_114() {
  if (__thread_114 != NULL) return __thread_114;
  __thread_114 = new thread_info(114, check_status_during_io__114);
  __init_thread_info_114(__thread_114);
  return __thread_114;
}

void __declare_sockets_114() {
  init_instance::add_incoming(113,114, DATA_SOCKET);
  init_instance::add_outgoing(114,115, DATA_SOCKET);
}

void __init_sockets_114(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_114() {
}

void __peek_sockets_114() {
}

 
void init_DataMinusAvg__640_182__114();
inline void check_status__114();

void work_DataMinusAvg__640_182__114(int);


inline float __pop_113_114() {
float res=BUFFER_113_114[TAIL_113_114];
TAIL_113_114++;
return res;
}

inline void __pop_113_114(int n) {
TAIL_113_114+=n;
}

inline float __peek_113_114(int offs) {
return BUFFER_113_114[TAIL_113_114+offs];
}



inline void __push_114_115(float data) {
BUFFER_114_115[HEAD_114_115]=data;
HEAD_114_115++;
}



 
void init_DataMinusAvg__640_182__114(){
}
void save_file_pointer__114(object_write_buffer *buf) {}
void load_file_pointer__114(object_write_buffer *buf) {}
 
void work_DataMinusAvg__640_182__114(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__637 = 0.0f;/* float */
      float v__638 = 0.0f;/* float */
      float __tmp11__639 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__637 = BUFFER_113_114[TAIL_113_114+256];
;
      ((avg__633__114) = __tmp9__637)/*float*/;
      for (((i__634__114) = 0)/*int*/; ((i__634__114) < 256); ((i__634__114)++)) {{
          v__638 = BUFFER_113_114[TAIL_113_114]; TAIL_113_114++;
;
          (__tmp11__639 = (v__638 - (avg__633__114)))/*float*/;
          __push_114_115(__tmp11__639);
        }
      }
      __pop_113_114();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_115;
int __counter_115 = 0;
int __steady_115 = 0;
int __tmp_115 = 0;
int __tmp2_115 = 0;
int *__state_flag_115 = NULL;
thread_info *__thread_115 = NULL;



inline void check_status__115() {
  check_thread_status(__state_flag_115, __thread_115);
}

void check_status_during_io__115() {
  check_thread_status_during_io(__state_flag_115, __thread_115);
}

void __init_thread_info_115(thread_info *info) {
  __state_flag_115 = info->get_state_flag();
}

thread_info *__get_thread_info_115() {
  if (__thread_115 != NULL) return __thread_115;
  __thread_115 = new thread_info(115, check_status_during_io__115);
  __init_thread_info_115(__thread_115);
  return __thread_115;
}

void __declare_sockets_115() {
  init_instance::add_incoming(114,115, DATA_SOCKET);
  init_instance::add_outgoing(115,116, DATA_SOCKET);
  init_instance::add_outgoing(115,118, DATA_SOCKET);
}

void __init_sockets_115(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_115() {
}

void __peek_sockets_115() {
}

inline float __pop_114_115() {
float res=BUFFER_114_115[TAIL_114_115];
TAIL_114_115++;
return res;
}


inline void __push_115_116(float data) {
BUFFER_115_116[HEAD_115_116]=data;
HEAD_115_116++;
}



inline void __push_115_118(float data) {
BUFFER_115_118[HEAD_115_118]=data;
HEAD_115_118++;
}



void __splitter_115_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_114_115[TAIL_114_115]; TAIL_114_115++;
;
  __push_115_116(tmp);
  __push_115_118(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_116;
int __counter_116 = 0;
int __steady_116 = 0;
int __tmp_116 = 0;
int __tmp2_116 = 0;
int *__state_flag_116 = NULL;
thread_info *__thread_116 = NULL;



float data__641__116 = 0.0f;
int i__642__116 = 0;
void save_peek_buffer__116(object_write_buffer *buf);
void load_peek_buffer__116(object_write_buffer *buf);
void save_file_pointer__116(object_write_buffer *buf);
void load_file_pointer__116(object_write_buffer *buf);

inline void check_status__116() {
  check_thread_status(__state_flag_116, __thread_116);
}

void check_status_during_io__116() {
  check_thread_status_during_io(__state_flag_116, __thread_116);
}

void __init_thread_info_116(thread_info *info) {
  __state_flag_116 = info->get_state_flag();
}

thread_info *__get_thread_info_116() {
  if (__thread_116 != NULL) return __thread_116;
  __thread_116 = new thread_info(116, check_status_during_io__116);
  __init_thread_info_116(__thread_116);
  return __thread_116;
}

void __declare_sockets_116() {
  init_instance::add_incoming(115,116, DATA_SOCKET);
  init_instance::add_outgoing(116,117, DATA_SOCKET);
}

void __init_sockets_116(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_116() {
}

void __peek_sockets_116() {
}

 
void init_DenomDataSum__646_184__116();
inline void check_status__116();

void work_DenomDataSum__646_184__116(int);


inline float __pop_115_116() {
float res=BUFFER_115_116[TAIL_115_116];
TAIL_115_116++;
return res;
}

inline void __pop_115_116(int n) {
TAIL_115_116+=n;
}

inline float __peek_115_116(int offs) {
return BUFFER_115_116[TAIL_115_116+offs];
}



inline void __push_116_117(float data) {
BUFFER_116_117[HEAD_116_117]=data;
HEAD_116_117++;
}



 
void init_DenomDataSum__646_184__116(){
}
void save_file_pointer__116(object_write_buffer *buf) {}
void load_file_pointer__116(object_write_buffer *buf) {}
 
void work_DenomDataSum__646_184__116(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__645 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__645 = ((float)0.0))/*float*/;
      for (((i__642__116) = 0)/*int*/; ((i__642__116) < 256); ((i__642__116)++)) {{
          (data__641__116) = BUFFER_115_116[TAIL_115_116]; TAIL_115_116++;
;
          (sum__645 = (sum__645 + ((data__641__116) * (data__641__116))))/*float*/;
        }
      }
      __push_116_117(sum__645);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_117;
int __counter_117 = 0;
int __steady_117 = 0;
int __tmp_117 = 0;
int __tmp2_117 = 0;
int *__state_flag_117 = NULL;
thread_info *__thread_117 = NULL;



inline void check_status__117() {
  check_thread_status(__state_flag_117, __thread_117);
}

void check_status_during_io__117() {
  check_thread_status_during_io(__state_flag_117, __thread_117);
}

void __init_thread_info_117(thread_info *info) {
  __state_flag_117 = info->get_state_flag();
}

thread_info *__get_thread_info_117() {
  if (__thread_117 != NULL) return __thread_117;
  __thread_117 = new thread_info(117, check_status_during_io__117);
  __init_thread_info_117(__thread_117);
  return __thread_117;
}

void __declare_sockets_117() {
  init_instance::add_incoming(116,117, DATA_SOCKET);
  init_instance::add_incoming(118,117, DATA_SOCKET);
  init_instance::add_outgoing(117,8, DATA_SOCKET);
}

void __init_sockets_117(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_117() {
}

void __peek_sockets_117() {
}


inline void __push_117_8(float data) {
BUFFER_117_8[HEAD_117_8]=data;
HEAD_117_8++;
}


inline float __pop_116_117() {
float res=BUFFER_116_117[TAIL_116_117];
TAIL_116_117++;
return res;
}

inline float __pop_118_117() {
float res=BUFFER_118_117[TAIL_118_117];
TAIL_118_117++;
return res;
}


void __joiner_117_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_117_8(__pop_116_117());
    __push_117_8(__pop_118_117());
    __push_117_8(__pop_118_117());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_118;
int __counter_118 = 0;
int __steady_118 = 0;
int __tmp_118 = 0;
int __tmp2_118 = 0;
int *__state_flag_118 = NULL;
thread_info *__thread_118 = NULL;



float _TheGlobal__pattern__6____118[256] = {0};
float numerSum__648__118 = 0.0f;
float denomPatternSum__649__118 = 0.0f;
float data__650__118 = 0.0f;
float patternData__651__118 = 0.0f;
int i__652__118 = 0;
void save_peek_buffer__118(object_write_buffer *buf);
void load_peek_buffer__118(object_write_buffer *buf);
void save_file_pointer__118(object_write_buffer *buf);
void load_file_pointer__118(object_write_buffer *buf);

inline void check_status__118() {
  check_thread_status(__state_flag_118, __thread_118);
}

void check_status_during_io__118() {
  check_thread_status_during_io(__state_flag_118, __thread_118);
}

void __init_thread_info_118(thread_info *info) {
  __state_flag_118 = info->get_state_flag();
}

thread_info *__get_thread_info_118() {
  if (__thread_118 != NULL) return __thread_118;
  __thread_118 = new thread_info(118, check_status_during_io__118);
  __init_thread_info_118(__thread_118);
  return __thread_118;
}

void __declare_sockets_118() {
  init_instance::add_incoming(115,118, DATA_SOCKET);
  init_instance::add_outgoing(118,117, DATA_SOCKET);
}

void __init_sockets_118(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_118() {
}

void __peek_sockets_118() {
}

 
void init_PatternSums__655_185__118();
inline void check_status__118();

void work_PatternSums__655_185__118(int);


inline float __pop_115_118() {
float res=BUFFER_115_118[TAIL_115_118];
TAIL_115_118++;
return res;
}

inline void __pop_115_118(int n) {
TAIL_115_118+=n;
}

inline float __peek_115_118(int offs) {
return BUFFER_115_118[TAIL_115_118+offs];
}



inline void __push_118_117(float data) {
BUFFER_118_117[HEAD_118_117]=data;
HEAD_118_117++;
}



 
void init_PatternSums__655_185__118(){
  (((_TheGlobal__pattern__6____118)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____118)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__118(object_write_buffer *buf) {}
void load_file_pointer__118(object_write_buffer *buf) {}
 
void work_PatternSums__655_185__118(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__648__118) = ((float)0.0))/*float*/;
      ((denomPatternSum__649__118) = ((float)0.0))/*float*/;
      for (((i__652__118) = 0)/*int*/; ((i__652__118) < 256); ((i__652__118)++)) {{
          ((patternData__651__118) = (((_TheGlobal__pattern__6____118)[(int)(i__652__118)]) - ((float)1.6013207E-4)))/*float*/;
          (data__650__118) = BUFFER_115_118[TAIL_115_118]; TAIL_115_118++;
;
          ((numerSum__648__118) = ((numerSum__648__118) + ((data__650__118) * (patternData__651__118))))/*float*/;
          ((denomPatternSum__649__118) = ((denomPatternSum__649__118) + ((patternData__651__118) * (patternData__651__118))))/*float*/;
        }
      }
      __push_118_117((denomPatternSum__649__118));
      __push_118_117((numerSum__648__118));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 1 pop: 1 push 0
// init counts: 0 steady counts: 16

// ClusterFusion isEliminated: false



int __number_of_iterations_12;
int __counter_12 = 0;
int __steady_12 = 0;
int __tmp_12 = 0;
int __tmp2_12 = 0;
int *__state_flag_12 = NULL;
thread_info *__thread_12 = NULL;



float i__667__12 = 0.0f;
float data__668__12 = 0.0f;
void save_peek_buffer__12(object_write_buffer *buf);
void load_peek_buffer__12(object_write_buffer *buf);
void save_file_pointer__12(object_write_buffer *buf);
void load_file_pointer__12(object_write_buffer *buf);

inline void check_status__12() {
  check_thread_status(__state_flag_12, __thread_12);
}

void check_status_during_io__12() {
  check_thread_status_during_io(__state_flag_12, __thread_12);
}

void __init_thread_info_12(thread_info *info) {
  __state_flag_12 = info->get_state_flag();
}

thread_info *__get_thread_info_12() {
  if (__thread_12 != NULL) return __thread_12;
  __thread_12 = new thread_info(12, check_status_during_io__12);
  __init_thread_info_12(__thread_12);
  return __thread_12;
}

void __declare_sockets_12() {
  init_instance::add_incoming(11,12, DATA_SOCKET);
}

void __init_sockets_12(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_12() {
}

void __peek_sockets_12() {
}

 
void init_SecondCounter__671_187__12();
inline void check_status__12();

void work_SecondCounter__671_187__12(int);


inline float __pop_11_12() {
float res=BUFFER_11_12[TAIL_11_12];
TAIL_11_12++;
return res;
}

inline void __pop_11_12(int n) {
TAIL_11_12+=n;
}

inline float __peek_11_12(int offs) {
return BUFFER_11_12[TAIL_11_12+offs];
}


 
void init_SecondCounter__671_187__12(){
  ((i__667__12) = ((float)0.0))/*float*/;
}
void save_file_pointer__12(object_write_buffer *buf) {}
void load_file_pointer__12(object_write_buffer *buf) {}
 
void work_SecondCounter__671_187__12(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter SecondCounter

      (data__668__12) = BUFFER_11_12[TAIL_11_12]; TAIL_11_12++;
;

      // TIMER_PRINT_CODE: __print_sink__ += (int)((data__668__12)); 

      ((i__667__12)++);
      // mark end: SIRFilter SecondCounter

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_13;
int __counter_13 = 0;
int __steady_13 = 0;
int __tmp_13 = 0;
int __tmp2_13 = 0;
int *__state_flag_13 = NULL;
thread_info *__thread_13 = NULL;



float numer__82__13 = 0.0f;
float denom_x__83__13 = 0.0f;
float denom_y__84__13 = 0.0f;
float denom__85__13 = 0.0f;
void save_peek_buffer__13(object_write_buffer *buf);
void load_peek_buffer__13(object_write_buffer *buf);
void save_file_pointer__13(object_write_buffer *buf);
void load_file_pointer__13(object_write_buffer *buf);

inline void check_status__13() {
  check_thread_status(__state_flag_13, __thread_13);
}

void check_status_during_io__13() {
  check_thread_status_during_io(__state_flag_13, __thread_13);
}

void __init_thread_info_13(thread_info *info) {
  __state_flag_13 = info->get_state_flag();
}

thread_info *__get_thread_info_13() {
  if (__thread_13 != NULL) return __thread_13;
  __thread_13 = new thread_info(13, check_status_during_io__13);
  __init_thread_info_13(__thread_13);
  return __thread_13;
}

void __declare_sockets_13() {
  init_instance::add_incoming(9,13, DATA_SOCKET);
  init_instance::add_outgoing(13,11, DATA_SOCKET);
}

void __init_sockets_13(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_13() {
}

void __peek_sockets_13() {
}

 
void init_Division__92_74__13();
inline void check_status__13();

void work_Division__92_74__13(int);


inline float __pop_9_13() {
float res=BUFFER_9_13[TAIL_9_13];
TAIL_9_13++;
return res;
}

inline void __pop_9_13(int n) {
TAIL_9_13+=n;
}

inline float __peek_9_13(int offs) {
return BUFFER_9_13[TAIL_9_13+offs];
}



inline void __push_13_11(float data) {
BUFFER_13_11[HEAD_13_11]=data;
HEAD_13_11++;
}



 
void init_Division__92_74__13(){
}
void save_file_pointer__13(object_write_buffer *buf) {}
void load_file_pointer__13(object_write_buffer *buf) {}
 
void work_Division__92_74__13(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__88 = 0.0f;/* float */
      float __tmp4__89 = 0.0f;/* float */
      double __tmp5__90 = 0.0f;/* double */
      double __tmp6__91 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__88 = ((float)0.0))/*float*/;
      (denom_x__83__13) = BUFFER_9_13[TAIL_9_13]; TAIL_9_13++;
;
      (denom_y__84__13) = BUFFER_9_13[TAIL_9_13]; TAIL_9_13++;
;
      (__tmp6__91 = ((double)(((denom_x__83__13) * (denom_y__84__13)))))/*double*/;
      (__tmp5__90 = sqrtf(__tmp6__91))/*double*/;
      (__tmp4__89 = ((float)(__tmp5__90)))/*float*/;
      ((denom__85__13) = __tmp4__89)/*float*/;
      (numer__82__13) = BUFFER_9_13[TAIL_9_13]; TAIL_9_13++;
;

      if (((denom__85__13) != ((float)0.0))) {(v__88 = ((numer__82__13) / (denom__85__13)))/*float*/;
      } else {}
      __push_13_11(v__88);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_14;
int __counter_14 = 0;
int __steady_14 = 0;
int __tmp_14 = 0;
int __tmp2_14 = 0;
int *__state_flag_14 = NULL;
thread_info *__thread_14 = NULL;



float numer__123__14 = 0.0f;
float denom_x__124__14 = 0.0f;
float denom_y__125__14 = 0.0f;
float denom__126__14 = 0.0f;
void save_peek_buffer__14(object_write_buffer *buf);
void load_peek_buffer__14(object_write_buffer *buf);
void save_file_pointer__14(object_write_buffer *buf);
void load_file_pointer__14(object_write_buffer *buf);

inline void check_status__14() {
  check_thread_status(__state_flag_14, __thread_14);
}

void check_status_during_io__14() {
  check_thread_status_during_io(__state_flag_14, __thread_14);
}

void __init_thread_info_14(thread_info *info) {
  __state_flag_14 = info->get_state_flag();
}

thread_info *__get_thread_info_14() {
  if (__thread_14 != NULL) return __thread_14;
  __thread_14 = new thread_info(14, check_status_during_io__14);
  __init_thread_info_14(__thread_14);
  return __thread_14;
}

void __declare_sockets_14() {
  init_instance::add_incoming(9,14, DATA_SOCKET);
  init_instance::add_outgoing(14,11, DATA_SOCKET);
}

void __init_sockets_14(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_14() {
}

void __peek_sockets_14() {
}

 
void init_Division__133_82__14();
inline void check_status__14();

void work_Division__133_82__14(int);


inline float __pop_9_14() {
float res=BUFFER_9_14[TAIL_9_14];
TAIL_9_14++;
return res;
}

inline void __pop_9_14(int n) {
TAIL_9_14+=n;
}

inline float __peek_9_14(int offs) {
return BUFFER_9_14[TAIL_9_14+offs];
}



inline void __push_14_11(float data) {
BUFFER_14_11[HEAD_14_11]=data;
HEAD_14_11++;
}



 
void init_Division__133_82__14(){
}
void save_file_pointer__14(object_write_buffer *buf) {}
void load_file_pointer__14(object_write_buffer *buf) {}
 
void work_Division__133_82__14(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__129 = 0.0f;/* float */
      float __tmp4__130 = 0.0f;/* float */
      double __tmp5__131 = 0.0f;/* double */
      double __tmp6__132 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__129 = ((float)0.0))/*float*/;
      (denom_x__124__14) = BUFFER_9_14[TAIL_9_14]; TAIL_9_14++;
;
      (denom_y__125__14) = BUFFER_9_14[TAIL_9_14]; TAIL_9_14++;
;
      (__tmp6__132 = ((double)(((denom_x__124__14) * (denom_y__125__14)))))/*double*/;
      (__tmp5__131 = sqrtf(__tmp6__132))/*double*/;
      (__tmp4__130 = ((float)(__tmp5__131)))/*float*/;
      ((denom__126__14) = __tmp4__130)/*float*/;
      (numer__123__14) = BUFFER_9_14[TAIL_9_14]; TAIL_9_14++;
;

      if (((denom__126__14) != ((float)0.0))) {(v__129 = ((numer__123__14) / (denom__126__14)))/*float*/;
      } else {}
      __push_14_11(v__129);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_15;
int __counter_15 = 0;
int __steady_15 = 0;
int __tmp_15 = 0;
int __tmp2_15 = 0;
int *__state_flag_15 = NULL;
thread_info *__thread_15 = NULL;



float numer__164__15 = 0.0f;
float denom_x__165__15 = 0.0f;
float denom_y__166__15 = 0.0f;
float denom__167__15 = 0.0f;
void save_peek_buffer__15(object_write_buffer *buf);
void load_peek_buffer__15(object_write_buffer *buf);
void save_file_pointer__15(object_write_buffer *buf);
void load_file_pointer__15(object_write_buffer *buf);

inline void check_status__15() {
  check_thread_status(__state_flag_15, __thread_15);
}

void check_status_during_io__15() {
  check_thread_status_during_io(__state_flag_15, __thread_15);
}

void __init_thread_info_15(thread_info *info) {
  __state_flag_15 = info->get_state_flag();
}

thread_info *__get_thread_info_15() {
  if (__thread_15 != NULL) return __thread_15;
  __thread_15 = new thread_info(15, check_status_during_io__15);
  __init_thread_info_15(__thread_15);
  return __thread_15;
}

void __declare_sockets_15() {
  init_instance::add_incoming(9,15, DATA_SOCKET);
  init_instance::add_outgoing(15,11, DATA_SOCKET);
}

void __init_sockets_15(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_15() {
}

void __peek_sockets_15() {
}

 
void init_Division__174_90__15();
inline void check_status__15();

void work_Division__174_90__15(int);


inline float __pop_9_15() {
float res=BUFFER_9_15[TAIL_9_15];
TAIL_9_15++;
return res;
}

inline void __pop_9_15(int n) {
TAIL_9_15+=n;
}

inline float __peek_9_15(int offs) {
return BUFFER_9_15[TAIL_9_15+offs];
}



inline void __push_15_11(float data) {
BUFFER_15_11[HEAD_15_11]=data;
HEAD_15_11++;
}



 
void init_Division__174_90__15(){
}
void save_file_pointer__15(object_write_buffer *buf) {}
void load_file_pointer__15(object_write_buffer *buf) {}
 
void work_Division__174_90__15(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__170 = 0.0f;/* float */
      float __tmp4__171 = 0.0f;/* float */
      double __tmp5__172 = 0.0f;/* double */
      double __tmp6__173 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__170 = ((float)0.0))/*float*/;
      (denom_x__165__15) = BUFFER_9_15[TAIL_9_15]; TAIL_9_15++;
;
      (denom_y__166__15) = BUFFER_9_15[TAIL_9_15]; TAIL_9_15++;
;
      (__tmp6__173 = ((double)(((denom_x__165__15) * (denom_y__166__15)))))/*double*/;
      (__tmp5__172 = sqrtf(__tmp6__173))/*double*/;
      (__tmp4__171 = ((float)(__tmp5__172)))/*float*/;
      ((denom__167__15) = __tmp4__171)/*float*/;
      (numer__164__15) = BUFFER_9_15[TAIL_9_15]; TAIL_9_15++;
;

      if (((denom__167__15) != ((float)0.0))) {(v__170 = ((numer__164__15) / (denom__167__15)))/*float*/;
      } else {}
      __push_15_11(v__170);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_16;
int __counter_16 = 0;
int __steady_16 = 0;
int __tmp_16 = 0;
int __tmp2_16 = 0;
int *__state_flag_16 = NULL;
thread_info *__thread_16 = NULL;



float numer__205__16 = 0.0f;
float denom_x__206__16 = 0.0f;
float denom_y__207__16 = 0.0f;
float denom__208__16 = 0.0f;
void save_peek_buffer__16(object_write_buffer *buf);
void load_peek_buffer__16(object_write_buffer *buf);
void save_file_pointer__16(object_write_buffer *buf);
void load_file_pointer__16(object_write_buffer *buf);

inline void check_status__16() {
  check_thread_status(__state_flag_16, __thread_16);
}

void check_status_during_io__16() {
  check_thread_status_during_io(__state_flag_16, __thread_16);
}

void __init_thread_info_16(thread_info *info) {
  __state_flag_16 = info->get_state_flag();
}

thread_info *__get_thread_info_16() {
  if (__thread_16 != NULL) return __thread_16;
  __thread_16 = new thread_info(16, check_status_during_io__16);
  __init_thread_info_16(__thread_16);
  return __thread_16;
}

void __declare_sockets_16() {
  init_instance::add_incoming(9,16, DATA_SOCKET);
  init_instance::add_outgoing(16,11, DATA_SOCKET);
}

void __init_sockets_16(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_16() {
}

void __peek_sockets_16() {
}

 
void init_Division__215_98__16();
inline void check_status__16();

void work_Division__215_98__16(int);


inline float __pop_9_16() {
float res=BUFFER_9_16[TAIL_9_16];
TAIL_9_16++;
return res;
}

inline void __pop_9_16(int n) {
TAIL_9_16+=n;
}

inline float __peek_9_16(int offs) {
return BUFFER_9_16[TAIL_9_16+offs];
}



inline void __push_16_11(float data) {
BUFFER_16_11[HEAD_16_11]=data;
HEAD_16_11++;
}



 
void init_Division__215_98__16(){
}
void save_file_pointer__16(object_write_buffer *buf) {}
void load_file_pointer__16(object_write_buffer *buf) {}
 
void work_Division__215_98__16(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__211 = 0.0f;/* float */
      float __tmp4__212 = 0.0f;/* float */
      double __tmp5__213 = 0.0f;/* double */
      double __tmp6__214 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__211 = ((float)0.0))/*float*/;
      (denom_x__206__16) = BUFFER_9_16[TAIL_9_16]; TAIL_9_16++;
;
      (denom_y__207__16) = BUFFER_9_16[TAIL_9_16]; TAIL_9_16++;
;
      (__tmp6__214 = ((double)(((denom_x__206__16) * (denom_y__207__16)))))/*double*/;
      (__tmp5__213 = sqrtf(__tmp6__214))/*double*/;
      (__tmp4__212 = ((float)(__tmp5__213)))/*float*/;
      ((denom__208__16) = __tmp4__212)/*float*/;
      (numer__205__16) = BUFFER_9_16[TAIL_9_16]; TAIL_9_16++;
;

      if (((denom__208__16) != ((float)0.0))) {(v__211 = ((numer__205__16) / (denom__208__16)))/*float*/;
      } else {}
      __push_16_11(v__211);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_17;
int __counter_17 = 0;
int __steady_17 = 0;
int __tmp_17 = 0;
int __tmp2_17 = 0;
int *__state_flag_17 = NULL;
thread_info *__thread_17 = NULL;



float numer__246__17 = 0.0f;
float denom_x__247__17 = 0.0f;
float denom_y__248__17 = 0.0f;
float denom__249__17 = 0.0f;
void save_peek_buffer__17(object_write_buffer *buf);
void load_peek_buffer__17(object_write_buffer *buf);
void save_file_pointer__17(object_write_buffer *buf);
void load_file_pointer__17(object_write_buffer *buf);

inline void check_status__17() {
  check_thread_status(__state_flag_17, __thread_17);
}

void check_status_during_io__17() {
  check_thread_status_during_io(__state_flag_17, __thread_17);
}

void __init_thread_info_17(thread_info *info) {
  __state_flag_17 = info->get_state_flag();
}

thread_info *__get_thread_info_17() {
  if (__thread_17 != NULL) return __thread_17;
  __thread_17 = new thread_info(17, check_status_during_io__17);
  __init_thread_info_17(__thread_17);
  return __thread_17;
}

void __declare_sockets_17() {
  init_instance::add_incoming(9,17, DATA_SOCKET);
  init_instance::add_outgoing(17,11, DATA_SOCKET);
}

void __init_sockets_17(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_17() {
}

void __peek_sockets_17() {
}

 
void init_Division__256_106__17();
inline void check_status__17();

void work_Division__256_106__17(int);


inline float __pop_9_17() {
float res=BUFFER_9_17[TAIL_9_17];
TAIL_9_17++;
return res;
}

inline void __pop_9_17(int n) {
TAIL_9_17+=n;
}

inline float __peek_9_17(int offs) {
return BUFFER_9_17[TAIL_9_17+offs];
}



inline void __push_17_11(float data) {
BUFFER_17_11[HEAD_17_11]=data;
HEAD_17_11++;
}



 
void init_Division__256_106__17(){
}
void save_file_pointer__17(object_write_buffer *buf) {}
void load_file_pointer__17(object_write_buffer *buf) {}
 
void work_Division__256_106__17(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__252 = 0.0f;/* float */
      float __tmp4__253 = 0.0f;/* float */
      double __tmp5__254 = 0.0f;/* double */
      double __tmp6__255 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__252 = ((float)0.0))/*float*/;
      (denom_x__247__17) = BUFFER_9_17[TAIL_9_17]; TAIL_9_17++;
;
      (denom_y__248__17) = BUFFER_9_17[TAIL_9_17]; TAIL_9_17++;
;
      (__tmp6__255 = ((double)(((denom_x__247__17) * (denom_y__248__17)))))/*double*/;
      (__tmp5__254 = sqrtf(__tmp6__255))/*double*/;
      (__tmp4__253 = ((float)(__tmp5__254)))/*float*/;
      ((denom__249__17) = __tmp4__253)/*float*/;
      (numer__246__17) = BUFFER_9_17[TAIL_9_17]; TAIL_9_17++;
;

      if (((denom__249__17) != ((float)0.0))) {(v__252 = ((numer__246__17) / (denom__249__17)))/*float*/;
      } else {}
      __push_17_11(v__252);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_18;
int __counter_18 = 0;
int __steady_18 = 0;
int __tmp_18 = 0;
int __tmp2_18 = 0;
int *__state_flag_18 = NULL;
thread_info *__thread_18 = NULL;



float numer__287__18 = 0.0f;
float denom_x__288__18 = 0.0f;
float denom_y__289__18 = 0.0f;
float denom__290__18 = 0.0f;
void save_peek_buffer__18(object_write_buffer *buf);
void load_peek_buffer__18(object_write_buffer *buf);
void save_file_pointer__18(object_write_buffer *buf);
void load_file_pointer__18(object_write_buffer *buf);

inline void check_status__18() {
  check_thread_status(__state_flag_18, __thread_18);
}

void check_status_during_io__18() {
  check_thread_status_during_io(__state_flag_18, __thread_18);
}

void __init_thread_info_18(thread_info *info) {
  __state_flag_18 = info->get_state_flag();
}

thread_info *__get_thread_info_18() {
  if (__thread_18 != NULL) return __thread_18;
  __thread_18 = new thread_info(18, check_status_during_io__18);
  __init_thread_info_18(__thread_18);
  return __thread_18;
}

void __declare_sockets_18() {
  init_instance::add_incoming(9,18, DATA_SOCKET);
  init_instance::add_outgoing(18,11, DATA_SOCKET);
}

void __init_sockets_18(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_18() {
}

void __peek_sockets_18() {
}

 
void init_Division__297_114__18();
inline void check_status__18();

void work_Division__297_114__18(int);


inline float __pop_9_18() {
float res=BUFFER_9_18[TAIL_9_18];
TAIL_9_18++;
return res;
}

inline void __pop_9_18(int n) {
TAIL_9_18+=n;
}

inline float __peek_9_18(int offs) {
return BUFFER_9_18[TAIL_9_18+offs];
}



inline void __push_18_11(float data) {
BUFFER_18_11[HEAD_18_11]=data;
HEAD_18_11++;
}



 
void init_Division__297_114__18(){
}
void save_file_pointer__18(object_write_buffer *buf) {}
void load_file_pointer__18(object_write_buffer *buf) {}
 
void work_Division__297_114__18(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__293 = 0.0f;/* float */
      float __tmp4__294 = 0.0f;/* float */
      double __tmp5__295 = 0.0f;/* double */
      double __tmp6__296 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__293 = ((float)0.0))/*float*/;
      (denom_x__288__18) = BUFFER_9_18[TAIL_9_18]; TAIL_9_18++;
;
      (denom_y__289__18) = BUFFER_9_18[TAIL_9_18]; TAIL_9_18++;
;
      (__tmp6__296 = ((double)(((denom_x__288__18) * (denom_y__289__18)))))/*double*/;
      (__tmp5__295 = sqrtf(__tmp6__296))/*double*/;
      (__tmp4__294 = ((float)(__tmp5__295)))/*float*/;
      ((denom__290__18) = __tmp4__294)/*float*/;
      (numer__287__18) = BUFFER_9_18[TAIL_9_18]; TAIL_9_18++;
;

      if (((denom__290__18) != ((float)0.0))) {(v__293 = ((numer__287__18) / (denom__290__18)))/*float*/;
      } else {}
      __push_18_11(v__293);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_19;
int __counter_19 = 0;
int __steady_19 = 0;
int __tmp_19 = 0;
int __tmp2_19 = 0;
int *__state_flag_19 = NULL;
thread_info *__thread_19 = NULL;



float numer__328__19 = 0.0f;
float denom_x__329__19 = 0.0f;
float denom_y__330__19 = 0.0f;
float denom__331__19 = 0.0f;
void save_peek_buffer__19(object_write_buffer *buf);
void load_peek_buffer__19(object_write_buffer *buf);
void save_file_pointer__19(object_write_buffer *buf);
void load_file_pointer__19(object_write_buffer *buf);

inline void check_status__19() {
  check_thread_status(__state_flag_19, __thread_19);
}

void check_status_during_io__19() {
  check_thread_status_during_io(__state_flag_19, __thread_19);
}

void __init_thread_info_19(thread_info *info) {
  __state_flag_19 = info->get_state_flag();
}

thread_info *__get_thread_info_19() {
  if (__thread_19 != NULL) return __thread_19;
  __thread_19 = new thread_info(19, check_status_during_io__19);
  __init_thread_info_19(__thread_19);
  return __thread_19;
}

void __declare_sockets_19() {
  init_instance::add_incoming(9,19, DATA_SOCKET);
  init_instance::add_outgoing(19,11, DATA_SOCKET);
}

void __init_sockets_19(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_19() {
}

void __peek_sockets_19() {
}

 
void init_Division__338_122__19();
inline void check_status__19();

void work_Division__338_122__19(int);


inline float __pop_9_19() {
float res=BUFFER_9_19[TAIL_9_19];
TAIL_9_19++;
return res;
}

inline void __pop_9_19(int n) {
TAIL_9_19+=n;
}

inline float __peek_9_19(int offs) {
return BUFFER_9_19[TAIL_9_19+offs];
}



inline void __push_19_11(float data) {
BUFFER_19_11[HEAD_19_11]=data;
HEAD_19_11++;
}



 
void init_Division__338_122__19(){
}
void save_file_pointer__19(object_write_buffer *buf) {}
void load_file_pointer__19(object_write_buffer *buf) {}
 
void work_Division__338_122__19(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__334 = 0.0f;/* float */
      float __tmp4__335 = 0.0f;/* float */
      double __tmp5__336 = 0.0f;/* double */
      double __tmp6__337 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__334 = ((float)0.0))/*float*/;
      (denom_x__329__19) = BUFFER_9_19[TAIL_9_19]; TAIL_9_19++;
;
      (denom_y__330__19) = BUFFER_9_19[TAIL_9_19]; TAIL_9_19++;
;
      (__tmp6__337 = ((double)(((denom_x__329__19) * (denom_y__330__19)))))/*double*/;
      (__tmp5__336 = sqrtf(__tmp6__337))/*double*/;
      (__tmp4__335 = ((float)(__tmp5__336)))/*float*/;
      ((denom__331__19) = __tmp4__335)/*float*/;
      (numer__328__19) = BUFFER_9_19[TAIL_9_19]; TAIL_9_19++;
;

      if (((denom__331__19) != ((float)0.0))) {(v__334 = ((numer__328__19) / (denom__331__19)))/*float*/;
      } else {}
      __push_19_11(v__334);
      // mark end: SIRFilter Division

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_2;
int __counter_2 = 0;
int __steady_2 = 0;
int __tmp_2 = 0;
int __tmp2_2 = 0;
int *__state_flag_2 = NULL;
thread_info *__thread_2 = NULL;



inline void check_status__2() {
  check_thread_status(__state_flag_2, __thread_2);
}

void check_status_during_io__2() {
  check_thread_status_during_io(__state_flag_2, __thread_2);
}

void __init_thread_info_2(thread_info *info) {
  __state_flag_2 = info->get_state_flag();
}

thread_info *__get_thread_info_2() {
  if (__thread_2 != NULL) return __thread_2;
  __thread_2 = new thread_info(2, check_status_during_io__2);
  __init_thread_info_2(__thread_2);
  return __thread_2;
}

void __declare_sockets_2() {
  init_instance::add_incoming(1,2, DATA_SOCKET);
  init_instance::add_outgoing(2,3, DATA_SOCKET);
  init_instance::add_outgoing(2,29, DATA_SOCKET);
  init_instance::add_outgoing(2,35, DATA_SOCKET);
  init_instance::add_outgoing(2,41, DATA_SOCKET);
  init_instance::add_outgoing(2,47, DATA_SOCKET);
  init_instance::add_outgoing(2,53, DATA_SOCKET);
  init_instance::add_outgoing(2,59, DATA_SOCKET);
  init_instance::add_outgoing(2,65, DATA_SOCKET);
  init_instance::add_outgoing(2,71, DATA_SOCKET);
  init_instance::add_outgoing(2,77, DATA_SOCKET);
  init_instance::add_outgoing(2,83, DATA_SOCKET);
  init_instance::add_outgoing(2,89, DATA_SOCKET);
  init_instance::add_outgoing(2,95, DATA_SOCKET);
  init_instance::add_outgoing(2,101, DATA_SOCKET);
  init_instance::add_outgoing(2,107, DATA_SOCKET);
  init_instance::add_outgoing(2,113, DATA_SOCKET);
}

void __init_sockets_2(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_2() {
}

void __peek_sockets_2() {
}

inline float __pop_1_2() {
float res=BUFFER_1_2[TAIL_1_2];
TAIL_1_2++;
return res;
}


inline void __push_2_3(float data) {
BUFFER_2_3[HEAD_2_3]=data;
HEAD_2_3++;
}



inline void __push_2_29(float data) {
BUFFER_2_29[HEAD_2_29]=data;
HEAD_2_29++;
}



inline void __push_2_35(float data) {
BUFFER_2_35[HEAD_2_35]=data;
HEAD_2_35++;
}



inline void __push_2_41(float data) {
BUFFER_2_41[HEAD_2_41]=data;
HEAD_2_41++;
}



inline void __push_2_47(float data) {
BUFFER_2_47[HEAD_2_47]=data;
HEAD_2_47++;
}



inline void __push_2_53(float data) {
BUFFER_2_53[HEAD_2_53]=data;
HEAD_2_53++;
}



inline void __push_2_59(float data) {
BUFFER_2_59[HEAD_2_59]=data;
HEAD_2_59++;
}



inline void __push_2_65(float data) {
BUFFER_2_65[HEAD_2_65]=data;
HEAD_2_65++;
}



inline void __push_2_71(float data) {
BUFFER_2_71[HEAD_2_71]=data;
HEAD_2_71++;
}



inline void __push_2_77(float data) {
BUFFER_2_77[HEAD_2_77]=data;
HEAD_2_77++;
}



inline void __push_2_83(float data) {
BUFFER_2_83[HEAD_2_83]=data;
HEAD_2_83++;
}



inline void __push_2_89(float data) {
BUFFER_2_89[HEAD_2_89]=data;
HEAD_2_89++;
}



inline void __push_2_95(float data) {
BUFFER_2_95[HEAD_2_95]=data;
HEAD_2_95++;
}



inline void __push_2_101(float data) {
BUFFER_2_101[HEAD_2_101]=data;
HEAD_2_101++;
}



inline void __push_2_107(float data) {
BUFFER_2_107[HEAD_2_107]=data;
HEAD_2_107++;
}



inline void __push_2_113(float data) {
BUFFER_2_113[HEAD_2_113]=data;
HEAD_2_113++;
}



void __splitter_2_work(int ____n) {
  for (;____n > 0; ____n--) {
  for (int __k = 0; __k < 32; __k++) {
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
__push_2_3(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_29(__pop_1_2());
__push_2_29(__pop_1_2());
__push_2_29(__pop_1_2());
__push_2_29(__pop_1_2());
__push_2_29(__pop_1_2());
__push_2_29(__pop_1_2());
__push_2_29(__pop_1_2());
__push_2_29(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_35(__pop_1_2());
__push_2_35(__pop_1_2());
__push_2_35(__pop_1_2());
__push_2_35(__pop_1_2());
__push_2_35(__pop_1_2());
__push_2_35(__pop_1_2());
__push_2_35(__pop_1_2());
__push_2_35(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_41(__pop_1_2());
__push_2_41(__pop_1_2());
__push_2_41(__pop_1_2());
__push_2_41(__pop_1_2());
__push_2_41(__pop_1_2());
__push_2_41(__pop_1_2());
__push_2_41(__pop_1_2());
__push_2_41(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
__push_2_47(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_53(__pop_1_2());
__push_2_53(__pop_1_2());
__push_2_53(__pop_1_2());
__push_2_53(__pop_1_2());
__push_2_53(__pop_1_2());
__push_2_53(__pop_1_2());
__push_2_53(__pop_1_2());
__push_2_53(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_59(__pop_1_2());
__push_2_59(__pop_1_2());
__push_2_59(__pop_1_2());
__push_2_59(__pop_1_2());
__push_2_59(__pop_1_2());
__push_2_59(__pop_1_2());
__push_2_59(__pop_1_2());
__push_2_59(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_65(__pop_1_2());
__push_2_65(__pop_1_2());
__push_2_65(__pop_1_2());
__push_2_65(__pop_1_2());
__push_2_65(__pop_1_2());
__push_2_65(__pop_1_2());
__push_2_65(__pop_1_2());
__push_2_65(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_71(__pop_1_2());
__push_2_71(__pop_1_2());
__push_2_71(__pop_1_2());
__push_2_71(__pop_1_2());
__push_2_71(__pop_1_2());
__push_2_71(__pop_1_2());
__push_2_71(__pop_1_2());
__push_2_71(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_77(__pop_1_2());
__push_2_77(__pop_1_2());
__push_2_77(__pop_1_2());
__push_2_77(__pop_1_2());
__push_2_77(__pop_1_2());
__push_2_77(__pop_1_2());
__push_2_77(__pop_1_2());
__push_2_77(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_83(__pop_1_2());
__push_2_83(__pop_1_2());
__push_2_83(__pop_1_2());
__push_2_83(__pop_1_2());
__push_2_83(__pop_1_2());
__push_2_83(__pop_1_2());
__push_2_83(__pop_1_2());
__push_2_83(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
__push_2_89(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_95(__pop_1_2());
__push_2_95(__pop_1_2());
__push_2_95(__pop_1_2());
__push_2_95(__pop_1_2());
__push_2_95(__pop_1_2());
__push_2_95(__pop_1_2());
__push_2_95(__pop_1_2());
__push_2_95(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_101(__pop_1_2());
__push_2_101(__pop_1_2());
__push_2_101(__pop_1_2());
__push_2_101(__pop_1_2());
__push_2_101(__pop_1_2());
__push_2_101(__pop_1_2());
__push_2_101(__pop_1_2());
__push_2_101(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_107(__pop_1_2());
__push_2_107(__pop_1_2());
__push_2_107(__pop_1_2());
__push_2_107(__pop_1_2());
__push_2_107(__pop_1_2());
__push_2_107(__pop_1_2());
__push_2_107(__pop_1_2());
__push_2_107(__pop_1_2());
  }
  for (int __k = 0; __k < 32; __k++) {
__push_2_113(__pop_1_2());
__push_2_113(__pop_1_2());
__push_2_113(__pop_1_2());
__push_2_113(__pop_1_2());
__push_2_113(__pop_1_2());
__push_2_113(__pop_1_2());
__push_2_113(__pop_1_2());
__push_2_113(__pop_1_2());
  }
  }
}


// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_20;
int __counter_20 = 0;
int __steady_20 = 0;
int __tmp_20 = 0;
int __tmp2_20 = 0;
int *__state_flag_20 = NULL;
thread_info *__thread_20 = NULL;



float numer__369__20 = 0.0f;
float denom_x__370__20 = 0.0f;
float denom_y__371__20 = 0.0f;
float denom__372__20 = 0.0f;
void save_peek_buffer__20(object_write_buffer *buf);
void load_peek_buffer__20(object_write_buffer *buf);
void save_file_pointer__20(object_write_buffer *buf);
void load_file_pointer__20(object_write_buffer *buf);

inline void check_status__20() {
  check_thread_status(__state_flag_20, __thread_20);
}

void check_status_during_io__20() {
  check_thread_status_during_io(__state_flag_20, __thread_20);
}

void __init_thread_info_20(thread_info *info) {
  __state_flag_20 = info->get_state_flag();
}

thread_info *__get_thread_info_20() {
  if (__thread_20 != NULL) return __thread_20;
  __thread_20 = new thread_info(20, check_status_during_io__20);
  __init_thread_info_20(__thread_20);
  return __thread_20;
}

void __declare_sockets_20() {
  init_instance::add_incoming(9,20, DATA_SOCKET);
  init_instance::add_outgoing(20,11, DATA_SOCKET);
}

void __init_sockets_20(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_20() {
}

void __peek_sockets_20() {
}

 
void init_Division__379_130__20();
inline void check_status__20();

void work_Division__379_130__20(int);


inline float __pop_9_20() {
float res=BUFFER_9_20[TAIL_9_20];
TAIL_9_20++;
return res;
}

inline void __pop_9_20(int n) {
TAIL_9_20+=n;
}

inline float __peek_9_20(int offs) {
return BUFFER_9_20[TAIL_9_20+offs];
}



inline void __push_20_11(float data) {
BUFFER_20_11[HEAD_20_11]=data;
HEAD_20_11++;
}



 
void init_Division__379_130__20(){
}
void save_file_pointer__20(object_write_buffer *buf) {}
void load_file_pointer__20(object_write_buffer *buf) {}
 
void work_Division__379_130__20(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__375 = 0.0f;/* float */
      float __tmp4__376 = 0.0f;/* float */
      double __tmp5__377 = 0.0f;/* double */
      double __tmp6__378 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__375 = ((float)0.0))/*float*/;
      (denom_x__370__20) = BUFFER_9_20[TAIL_9_20]; TAIL_9_20++;
;
      (denom_y__371__20) = BUFFER_9_20[TAIL_9_20]; TAIL_9_20++;
;
      (__tmp6__378 = ((double)(((denom_x__370__20) * (denom_y__371__20)))))/*double*/;
      (__tmp5__377 = sqrtf(__tmp6__378))/*double*/;
      (__tmp4__376 = ((float)(__tmp5__377)))/*float*/;
      ((denom__372__20) = __tmp4__376)/*float*/;
      (numer__369__20) = BUFFER_9_20[TAIL_9_20]; TAIL_9_20++;
;

      if (((denom__372__20) != ((float)0.0))) {(v__375 = ((numer__369__20) / (denom__372__20)))/*float*/;
      } else {}
      __push_20_11(v__375);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_21;
int __counter_21 = 0;
int __steady_21 = 0;
int __tmp_21 = 0;
int __tmp2_21 = 0;
int *__state_flag_21 = NULL;
thread_info *__thread_21 = NULL;



float numer__410__21 = 0.0f;
float denom_x__411__21 = 0.0f;
float denom_y__412__21 = 0.0f;
float denom__413__21 = 0.0f;
void save_peek_buffer__21(object_write_buffer *buf);
void load_peek_buffer__21(object_write_buffer *buf);
void save_file_pointer__21(object_write_buffer *buf);
void load_file_pointer__21(object_write_buffer *buf);

inline void check_status__21() {
  check_thread_status(__state_flag_21, __thread_21);
}

void check_status_during_io__21() {
  check_thread_status_during_io(__state_flag_21, __thread_21);
}

void __init_thread_info_21(thread_info *info) {
  __state_flag_21 = info->get_state_flag();
}

thread_info *__get_thread_info_21() {
  if (__thread_21 != NULL) return __thread_21;
  __thread_21 = new thread_info(21, check_status_during_io__21);
  __init_thread_info_21(__thread_21);
  return __thread_21;
}

void __declare_sockets_21() {
  init_instance::add_incoming(9,21, DATA_SOCKET);
  init_instance::add_outgoing(21,11, DATA_SOCKET);
}

void __init_sockets_21(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_21() {
}

void __peek_sockets_21() {
}

 
void init_Division__420_138__21();
inline void check_status__21();

void work_Division__420_138__21(int);


inline float __pop_9_21() {
float res=BUFFER_9_21[TAIL_9_21];
TAIL_9_21++;
return res;
}

inline void __pop_9_21(int n) {
TAIL_9_21+=n;
}

inline float __peek_9_21(int offs) {
return BUFFER_9_21[TAIL_9_21+offs];
}



inline void __push_21_11(float data) {
BUFFER_21_11[HEAD_21_11]=data;
HEAD_21_11++;
}



 
void init_Division__420_138__21(){
}
void save_file_pointer__21(object_write_buffer *buf) {}
void load_file_pointer__21(object_write_buffer *buf) {}
 
void work_Division__420_138__21(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__416 = 0.0f;/* float */
      float __tmp4__417 = 0.0f;/* float */
      double __tmp5__418 = 0.0f;/* double */
      double __tmp6__419 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__416 = ((float)0.0))/*float*/;
      (denom_x__411__21) = BUFFER_9_21[TAIL_9_21]; TAIL_9_21++;
;
      (denom_y__412__21) = BUFFER_9_21[TAIL_9_21]; TAIL_9_21++;
;
      (__tmp6__419 = ((double)(((denom_x__411__21) * (denom_y__412__21)))))/*double*/;
      (__tmp5__418 = sqrtf(__tmp6__419))/*double*/;
      (__tmp4__417 = ((float)(__tmp5__418)))/*float*/;
      ((denom__413__21) = __tmp4__417)/*float*/;
      (numer__410__21) = BUFFER_9_21[TAIL_9_21]; TAIL_9_21++;
;

      if (((denom__413__21) != ((float)0.0))) {(v__416 = ((numer__410__21) / (denom__413__21)))/*float*/;
      } else {}
      __push_21_11(v__416);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_22;
int __counter_22 = 0;
int __steady_22 = 0;
int __tmp_22 = 0;
int __tmp2_22 = 0;
int *__state_flag_22 = NULL;
thread_info *__thread_22 = NULL;



float numer__451__22 = 0.0f;
float denom_x__452__22 = 0.0f;
float denom_y__453__22 = 0.0f;
float denom__454__22 = 0.0f;
void save_peek_buffer__22(object_write_buffer *buf);
void load_peek_buffer__22(object_write_buffer *buf);
void save_file_pointer__22(object_write_buffer *buf);
void load_file_pointer__22(object_write_buffer *buf);

inline void check_status__22() {
  check_thread_status(__state_flag_22, __thread_22);
}

void check_status_during_io__22() {
  check_thread_status_during_io(__state_flag_22, __thread_22);
}

void __init_thread_info_22(thread_info *info) {
  __state_flag_22 = info->get_state_flag();
}

thread_info *__get_thread_info_22() {
  if (__thread_22 != NULL) return __thread_22;
  __thread_22 = new thread_info(22, check_status_during_io__22);
  __init_thread_info_22(__thread_22);
  return __thread_22;
}

void __declare_sockets_22() {
  init_instance::add_incoming(9,22, DATA_SOCKET);
  init_instance::add_outgoing(22,11, DATA_SOCKET);
}

void __init_sockets_22(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_22() {
}

void __peek_sockets_22() {
}

 
void init_Division__461_146__22();
inline void check_status__22();

void work_Division__461_146__22(int);


inline float __pop_9_22() {
float res=BUFFER_9_22[TAIL_9_22];
TAIL_9_22++;
return res;
}

inline void __pop_9_22(int n) {
TAIL_9_22+=n;
}

inline float __peek_9_22(int offs) {
return BUFFER_9_22[TAIL_9_22+offs];
}



inline void __push_22_11(float data) {
BUFFER_22_11[HEAD_22_11]=data;
HEAD_22_11++;
}



 
void init_Division__461_146__22(){
}
void save_file_pointer__22(object_write_buffer *buf) {}
void load_file_pointer__22(object_write_buffer *buf) {}
 
void work_Division__461_146__22(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__457 = 0.0f;/* float */
      float __tmp4__458 = 0.0f;/* float */
      double __tmp5__459 = 0.0f;/* double */
      double __tmp6__460 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__457 = ((float)0.0))/*float*/;
      (denom_x__452__22) = BUFFER_9_22[TAIL_9_22]; TAIL_9_22++;
;
      (denom_y__453__22) = BUFFER_9_22[TAIL_9_22]; TAIL_9_22++;
;
      (__tmp6__460 = ((double)(((denom_x__452__22) * (denom_y__453__22)))))/*double*/;
      (__tmp5__459 = sqrtf(__tmp6__460))/*double*/;
      (__tmp4__458 = ((float)(__tmp5__459)))/*float*/;
      ((denom__454__22) = __tmp4__458)/*float*/;
      (numer__451__22) = BUFFER_9_22[TAIL_9_22]; TAIL_9_22++;
;

      if (((denom__454__22) != ((float)0.0))) {(v__457 = ((numer__451__22) / (denom__454__22)))/*float*/;
      } else {}
      __push_22_11(v__457);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_23;
int __counter_23 = 0;
int __steady_23 = 0;
int __tmp_23 = 0;
int __tmp2_23 = 0;
int *__state_flag_23 = NULL;
thread_info *__thread_23 = NULL;



float numer__492__23 = 0.0f;
float denom_x__493__23 = 0.0f;
float denom_y__494__23 = 0.0f;
float denom__495__23 = 0.0f;
void save_peek_buffer__23(object_write_buffer *buf);
void load_peek_buffer__23(object_write_buffer *buf);
void save_file_pointer__23(object_write_buffer *buf);
void load_file_pointer__23(object_write_buffer *buf);

inline void check_status__23() {
  check_thread_status(__state_flag_23, __thread_23);
}

void check_status_during_io__23() {
  check_thread_status_during_io(__state_flag_23, __thread_23);
}

void __init_thread_info_23(thread_info *info) {
  __state_flag_23 = info->get_state_flag();
}

thread_info *__get_thread_info_23() {
  if (__thread_23 != NULL) return __thread_23;
  __thread_23 = new thread_info(23, check_status_during_io__23);
  __init_thread_info_23(__thread_23);
  return __thread_23;
}

void __declare_sockets_23() {
  init_instance::add_incoming(9,23, DATA_SOCKET);
  init_instance::add_outgoing(23,11, DATA_SOCKET);
}

void __init_sockets_23(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_23() {
}

void __peek_sockets_23() {
}

 
void init_Division__502_154__23();
inline void check_status__23();

void work_Division__502_154__23(int);


inline float __pop_9_23() {
float res=BUFFER_9_23[TAIL_9_23];
TAIL_9_23++;
return res;
}

inline void __pop_9_23(int n) {
TAIL_9_23+=n;
}

inline float __peek_9_23(int offs) {
return BUFFER_9_23[TAIL_9_23+offs];
}



inline void __push_23_11(float data) {
BUFFER_23_11[HEAD_23_11]=data;
HEAD_23_11++;
}



 
void init_Division__502_154__23(){
}
void save_file_pointer__23(object_write_buffer *buf) {}
void load_file_pointer__23(object_write_buffer *buf) {}
 
void work_Division__502_154__23(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__498 = 0.0f;/* float */
      float __tmp4__499 = 0.0f;/* float */
      double __tmp5__500 = 0.0f;/* double */
      double __tmp6__501 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__498 = ((float)0.0))/*float*/;
      (denom_x__493__23) = BUFFER_9_23[TAIL_9_23]; TAIL_9_23++;
;
      (denom_y__494__23) = BUFFER_9_23[TAIL_9_23]; TAIL_9_23++;
;
      (__tmp6__501 = ((double)(((denom_x__493__23) * (denom_y__494__23)))))/*double*/;
      (__tmp5__500 = sqrtf(__tmp6__501))/*double*/;
      (__tmp4__499 = ((float)(__tmp5__500)))/*float*/;
      ((denom__495__23) = __tmp4__499)/*float*/;
      (numer__492__23) = BUFFER_9_23[TAIL_9_23]; TAIL_9_23++;
;

      if (((denom__495__23) != ((float)0.0))) {(v__498 = ((numer__492__23) / (denom__495__23)))/*float*/;
      } else {}
      __push_23_11(v__498);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_24;
int __counter_24 = 0;
int __steady_24 = 0;
int __tmp_24 = 0;
int __tmp2_24 = 0;
int *__state_flag_24 = NULL;
thread_info *__thread_24 = NULL;



float numer__533__24 = 0.0f;
float denom_x__534__24 = 0.0f;
float denom_y__535__24 = 0.0f;
float denom__536__24 = 0.0f;
void save_peek_buffer__24(object_write_buffer *buf);
void load_peek_buffer__24(object_write_buffer *buf);
void save_file_pointer__24(object_write_buffer *buf);
void load_file_pointer__24(object_write_buffer *buf);

inline void check_status__24() {
  check_thread_status(__state_flag_24, __thread_24);
}

void check_status_during_io__24() {
  check_thread_status_during_io(__state_flag_24, __thread_24);
}

void __init_thread_info_24(thread_info *info) {
  __state_flag_24 = info->get_state_flag();
}

thread_info *__get_thread_info_24() {
  if (__thread_24 != NULL) return __thread_24;
  __thread_24 = new thread_info(24, check_status_during_io__24);
  __init_thread_info_24(__thread_24);
  return __thread_24;
}

void __declare_sockets_24() {
  init_instance::add_incoming(9,24, DATA_SOCKET);
  init_instance::add_outgoing(24,11, DATA_SOCKET);
}

void __init_sockets_24(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_24() {
}

void __peek_sockets_24() {
}

 
void init_Division__543_162__24();
inline void check_status__24();

void work_Division__543_162__24(int);


inline float __pop_9_24() {
float res=BUFFER_9_24[TAIL_9_24];
TAIL_9_24++;
return res;
}

inline void __pop_9_24(int n) {
TAIL_9_24+=n;
}

inline float __peek_9_24(int offs) {
return BUFFER_9_24[TAIL_9_24+offs];
}



inline void __push_24_11(float data) {
BUFFER_24_11[HEAD_24_11]=data;
HEAD_24_11++;
}



 
void init_Division__543_162__24(){
}
void save_file_pointer__24(object_write_buffer *buf) {}
void load_file_pointer__24(object_write_buffer *buf) {}
 
void work_Division__543_162__24(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__539 = 0.0f;/* float */
      float __tmp4__540 = 0.0f;/* float */
      double __tmp5__541 = 0.0f;/* double */
      double __tmp6__542 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__539 = ((float)0.0))/*float*/;
      (denom_x__534__24) = BUFFER_9_24[TAIL_9_24]; TAIL_9_24++;
;
      (denom_y__535__24) = BUFFER_9_24[TAIL_9_24]; TAIL_9_24++;
;
      (__tmp6__542 = ((double)(((denom_x__534__24) * (denom_y__535__24)))))/*double*/;
      (__tmp5__541 = sqrtf(__tmp6__542))/*double*/;
      (__tmp4__540 = ((float)(__tmp5__541)))/*float*/;
      ((denom__536__24) = __tmp4__540)/*float*/;
      (numer__533__24) = BUFFER_9_24[TAIL_9_24]; TAIL_9_24++;
;

      if (((denom__536__24) != ((float)0.0))) {(v__539 = ((numer__533__24) / (denom__536__24)))/*float*/;
      } else {}
      __push_24_11(v__539);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_25;
int __counter_25 = 0;
int __steady_25 = 0;
int __tmp_25 = 0;
int __tmp2_25 = 0;
int *__state_flag_25 = NULL;
thread_info *__thread_25 = NULL;



float numer__574__25 = 0.0f;
float denom_x__575__25 = 0.0f;
float denom_y__576__25 = 0.0f;
float denom__577__25 = 0.0f;
void save_peek_buffer__25(object_write_buffer *buf);
void load_peek_buffer__25(object_write_buffer *buf);
void save_file_pointer__25(object_write_buffer *buf);
void load_file_pointer__25(object_write_buffer *buf);

inline void check_status__25() {
  check_thread_status(__state_flag_25, __thread_25);
}

void check_status_during_io__25() {
  check_thread_status_during_io(__state_flag_25, __thread_25);
}

void __init_thread_info_25(thread_info *info) {
  __state_flag_25 = info->get_state_flag();
}

thread_info *__get_thread_info_25() {
  if (__thread_25 != NULL) return __thread_25;
  __thread_25 = new thread_info(25, check_status_during_io__25);
  __init_thread_info_25(__thread_25);
  return __thread_25;
}

void __declare_sockets_25() {
  init_instance::add_incoming(9,25, DATA_SOCKET);
  init_instance::add_outgoing(25,11, DATA_SOCKET);
}

void __init_sockets_25(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_25() {
}

void __peek_sockets_25() {
}

 
void init_Division__584_170__25();
inline void check_status__25();

void work_Division__584_170__25(int);


inline float __pop_9_25() {
float res=BUFFER_9_25[TAIL_9_25];
TAIL_9_25++;
return res;
}

inline void __pop_9_25(int n) {
TAIL_9_25+=n;
}

inline float __peek_9_25(int offs) {
return BUFFER_9_25[TAIL_9_25+offs];
}



inline void __push_25_11(float data) {
BUFFER_25_11[HEAD_25_11]=data;
HEAD_25_11++;
}



 
void init_Division__584_170__25(){
}
void save_file_pointer__25(object_write_buffer *buf) {}
void load_file_pointer__25(object_write_buffer *buf) {}
 
void work_Division__584_170__25(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__580 = 0.0f;/* float */
      float __tmp4__581 = 0.0f;/* float */
      double __tmp5__582 = 0.0f;/* double */
      double __tmp6__583 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__580 = ((float)0.0))/*float*/;
      (denom_x__575__25) = BUFFER_9_25[TAIL_9_25]; TAIL_9_25++;
;
      (denom_y__576__25) = BUFFER_9_25[TAIL_9_25]; TAIL_9_25++;
;
      (__tmp6__583 = ((double)(((denom_x__575__25) * (denom_y__576__25)))))/*double*/;
      (__tmp5__582 = sqrtf(__tmp6__583))/*double*/;
      (__tmp4__581 = ((float)(__tmp5__582)))/*float*/;
      ((denom__577__25) = __tmp4__581)/*float*/;
      (numer__574__25) = BUFFER_9_25[TAIL_9_25]; TAIL_9_25++;
;

      if (((denom__577__25) != ((float)0.0))) {(v__580 = ((numer__574__25) / (denom__577__25)))/*float*/;
      } else {}
      __push_25_11(v__580);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_26;
int __counter_26 = 0;
int __steady_26 = 0;
int __tmp_26 = 0;
int __tmp2_26 = 0;
int *__state_flag_26 = NULL;
thread_info *__thread_26 = NULL;



float numer__615__26 = 0.0f;
float denom_x__616__26 = 0.0f;
float denom_y__617__26 = 0.0f;
float denom__618__26 = 0.0f;
void save_peek_buffer__26(object_write_buffer *buf);
void load_peek_buffer__26(object_write_buffer *buf);
void save_file_pointer__26(object_write_buffer *buf);
void load_file_pointer__26(object_write_buffer *buf);

inline void check_status__26() {
  check_thread_status(__state_flag_26, __thread_26);
}

void check_status_during_io__26() {
  check_thread_status_during_io(__state_flag_26, __thread_26);
}

void __init_thread_info_26(thread_info *info) {
  __state_flag_26 = info->get_state_flag();
}

thread_info *__get_thread_info_26() {
  if (__thread_26 != NULL) return __thread_26;
  __thread_26 = new thread_info(26, check_status_during_io__26);
  __init_thread_info_26(__thread_26);
  return __thread_26;
}

void __declare_sockets_26() {
  init_instance::add_incoming(9,26, DATA_SOCKET);
  init_instance::add_outgoing(26,11, DATA_SOCKET);
}

void __init_sockets_26(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_26() {
}

void __peek_sockets_26() {
}

 
void init_Division__625_178__26();
inline void check_status__26();

void work_Division__625_178__26(int);


inline float __pop_9_26() {
float res=BUFFER_9_26[TAIL_9_26];
TAIL_9_26++;
return res;
}

inline void __pop_9_26(int n) {
TAIL_9_26+=n;
}

inline float __peek_9_26(int offs) {
return BUFFER_9_26[TAIL_9_26+offs];
}



inline void __push_26_11(float data) {
BUFFER_26_11[HEAD_26_11]=data;
HEAD_26_11++;
}



 
void init_Division__625_178__26(){
}
void save_file_pointer__26(object_write_buffer *buf) {}
void load_file_pointer__26(object_write_buffer *buf) {}
 
void work_Division__625_178__26(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__621 = 0.0f;/* float */
      float __tmp4__622 = 0.0f;/* float */
      double __tmp5__623 = 0.0f;/* double */
      double __tmp6__624 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__621 = ((float)0.0))/*float*/;
      (denom_x__616__26) = BUFFER_9_26[TAIL_9_26]; TAIL_9_26++;
;
      (denom_y__617__26) = BUFFER_9_26[TAIL_9_26]; TAIL_9_26++;
;
      (__tmp6__624 = ((double)(((denom_x__616__26) * (denom_y__617__26)))))/*double*/;
      (__tmp5__623 = sqrtf(__tmp6__624))/*double*/;
      (__tmp4__622 = ((float)(__tmp5__623)))/*float*/;
      ((denom__618__26) = __tmp4__622)/*float*/;
      (numer__615__26) = BUFFER_9_26[TAIL_9_26]; TAIL_9_26++;
;

      if (((denom__618__26) != ((float)0.0))) {(v__621 = ((numer__615__26) / (denom__618__26)))/*float*/;
      } else {}
      __push_26_11(v__621);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 3 pop: 3 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_27;
int __counter_27 = 0;
int __steady_27 = 0;
int __tmp_27 = 0;
int __tmp2_27 = 0;
int *__state_flag_27 = NULL;
thread_info *__thread_27 = NULL;



float numer__656__27 = 0.0f;
float denom_x__657__27 = 0.0f;
float denom_y__658__27 = 0.0f;
float denom__659__27 = 0.0f;
void save_peek_buffer__27(object_write_buffer *buf);
void load_peek_buffer__27(object_write_buffer *buf);
void save_file_pointer__27(object_write_buffer *buf);
void load_file_pointer__27(object_write_buffer *buf);

inline void check_status__27() {
  check_thread_status(__state_flag_27, __thread_27);
}

void check_status_during_io__27() {
  check_thread_status_during_io(__state_flag_27, __thread_27);
}

void __init_thread_info_27(thread_info *info) {
  __state_flag_27 = info->get_state_flag();
}

thread_info *__get_thread_info_27() {
  if (__thread_27 != NULL) return __thread_27;
  __thread_27 = new thread_info(27, check_status_during_io__27);
  __init_thread_info_27(__thread_27);
  return __thread_27;
}

void __declare_sockets_27() {
  init_instance::add_incoming(9,27, DATA_SOCKET);
  init_instance::add_outgoing(27,11, DATA_SOCKET);
}

void __init_sockets_27(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_27() {
}

void __peek_sockets_27() {
}

 
void init_Division__666_186__27();
inline void check_status__27();

void work_Division__666_186__27(int);


inline float __pop_9_27() {
float res=BUFFER_9_27[TAIL_9_27];
TAIL_9_27++;
return res;
}

inline void __pop_9_27(int n) {
TAIL_9_27+=n;
}

inline float __peek_9_27(int offs) {
return BUFFER_9_27[TAIL_9_27+offs];
}



inline void __push_27_11(float data) {
BUFFER_27_11[HEAD_27_11]=data;
HEAD_27_11++;
}



 
void init_Division__666_186__27(){
}
void save_file_pointer__27(object_write_buffer *buf) {}
void load_file_pointer__27(object_write_buffer *buf) {}
 
void work_Division__666_186__27(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float v__662 = 0.0f;/* float */
      float __tmp4__663 = 0.0f;/* float */
      double __tmp5__664 = 0.0f;/* double */
      double __tmp6__665 = 0.0f;/* double */

      // mark begin: SIRFilter Division

      (v__662 = ((float)0.0))/*float*/;
      (denom_x__657__27) = BUFFER_9_27[TAIL_9_27]; TAIL_9_27++;
;
      (denom_y__658__27) = BUFFER_9_27[TAIL_9_27]; TAIL_9_27++;
;
      (__tmp6__665 = ((double)(((denom_x__657__27) * (denom_y__658__27)))))/*double*/;
      (__tmp5__664 = sqrtf(__tmp6__665))/*double*/;
      (__tmp4__663 = ((float)(__tmp5__664)))/*float*/;
      ((denom__659__27) = __tmp4__663)/*float*/;
      (numer__656__27) = BUFFER_9_27[TAIL_9_27]; TAIL_9_27++;
;

      if (((denom__659__27) != ((float)0.0))) {(v__662 = ((numer__656__27) / (denom__659__27)))/*float*/;
      } else {}
      __push_27_11(v__662);
      // mark end: SIRFilter Division

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_28;
int __counter_28 = 0;
int __steady_28 = 0;
int __tmp_28 = 0;
int __tmp2_28 = 0;
int *__state_flag_28 = NULL;
thread_info *__thread_28 = NULL;



float _TheGlobal__pattern__6____28[256] = {0};
float numerSum__33__28 = 0.0f;
float denomPatternSum__34__28 = 0.0f;
float data__35__28 = 0.0f;
float patternData__36__28 = 0.0f;
int i__37__28 = 0;
void save_peek_buffer__28(object_write_buffer *buf);
void load_peek_buffer__28(object_write_buffer *buf);
void save_file_pointer__28(object_write_buffer *buf);
void load_file_pointer__28(object_write_buffer *buf);

inline void check_status__28() {
  check_thread_status(__state_flag_28, __thread_28);
}

void check_status_during_io__28() {
  check_thread_status_during_io(__state_flag_28, __thread_28);
}

void __init_thread_info_28(thread_info *info) {
  __state_flag_28 = info->get_state_flag();
}

thread_info *__get_thread_info_28() {
  if (__thread_28 != NULL) return __thread_28;
  __thread_28 = new thread_info(28, check_status_during_io__28);
  __init_thread_info_28(__thread_28);
  return __thread_28;
}

void __declare_sockets_28() {
  init_instance::add_incoming(5,28, DATA_SOCKET);
  init_instance::add_outgoing(28,7, DATA_SOCKET);
}

void __init_sockets_28(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_28() {
}

void __peek_sockets_28() {
}

 
void init_PatternSums__40_65__28();
inline void check_status__28();

void work_PatternSums__40_65__28(int);


inline float __pop_5_28() {
float res=BUFFER_5_28[TAIL_5_28];
TAIL_5_28++;
return res;
}

inline void __pop_5_28(int n) {
TAIL_5_28+=n;
}

inline float __peek_5_28(int offs) {
return BUFFER_5_28[TAIL_5_28+offs];
}



inline void __push_28_7(float data) {
BUFFER_28_7[HEAD_28_7]=data;
HEAD_28_7++;
}



 
void init_PatternSums__40_65__28(){
  (((_TheGlobal__pattern__6____28)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____28)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__28(object_write_buffer *buf) {}
void load_file_pointer__28(object_write_buffer *buf) {}
 
void work_PatternSums__40_65__28(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__33__28) = ((float)0.0))/*float*/;
      ((denomPatternSum__34__28) = ((float)0.0))/*float*/;
      for (((i__37__28) = 0)/*int*/; ((i__37__28) < 256); ((i__37__28)++)) {{
          ((patternData__36__28) = (((_TheGlobal__pattern__6____28)[(int)(i__37__28)]) - ((float)1.6013207E-4)))/*float*/;
          (data__35__28) = BUFFER_5_28[TAIL_5_28]; TAIL_5_28++;
;
          ((numerSum__33__28) = ((numerSum__33__28) + ((data__35__28) * (patternData__36__28))))/*float*/;
          ((denomPatternSum__34__28) = ((denomPatternSum__34__28) + ((patternData__36__28) * (patternData__36__28))))/*float*/;
        }
      }
      __push_28_7((denomPatternSum__34__28));
      __push_28_7((numerSum__33__28));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_29;
int __counter_29 = 0;
int __steady_29 = 0;
int __tmp_29 = 0;
int __tmp2_29 = 0;
int *__state_flag_29 = NULL;
thread_info *__thread_29 = NULL;



void save_peek_buffer__29(object_write_buffer *buf);
void load_peek_buffer__29(object_write_buffer *buf);
void save_file_pointer__29(object_write_buffer *buf);
void load_file_pointer__29(object_write_buffer *buf);

inline void check_status__29() {
  check_thread_status(__state_flag_29, __thread_29);
}

void check_status_during_io__29() {
  check_thread_status_during_io(__state_flag_29, __thread_29);
}

void __init_thread_info_29(thread_info *info) {
  __state_flag_29 = info->get_state_flag();
}

thread_info *__get_thread_info_29() {
  if (__thread_29 != NULL) return __thread_29;
  __thread_29 = new thread_info(29, check_status_during_io__29);
  __init_thread_info_29(__thread_29);
  return __thread_29;
}

void __declare_sockets_29() {
  init_instance::add_incoming(2,29, DATA_SOCKET);
  init_instance::add_outgoing(29,30, DATA_SOCKET);
}

void __init_sockets_29(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_29() {
}

void __peek_sockets_29() {
}

 
void init_GetAvg__58_68__29();
inline void check_status__29();

void work_GetAvg__58_68__29(int);


inline float __pop_2_29() {
float res=BUFFER_2_29[TAIL_2_29];
TAIL_2_29++;
return res;
}

inline void __pop_2_29(int n) {
TAIL_2_29+=n;
}

inline float __peek_2_29(int offs) {
return BUFFER_2_29[TAIL_2_29+offs];
}



inline void __push_29_30(float data) {
BUFFER_29_30[HEAD_29_30]=data;
HEAD_29_30++;
}



 
void init_GetAvg__58_68__29(){
}
void save_file_pointer__29(object_write_buffer *buf) {}
void load_file_pointer__29(object_write_buffer *buf) {}
 
void work_GetAvg__58_68__29(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__54 = 0;/* int */
      float Sum__55 = 0.0f;/* float */
      float val__56 = 0.0f;/* float */
      float __tmp8__57 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__55 = ((float)0.0))/*float*/;
      for ((i__54 = 0)/*int*/; (i__54 < 256); (i__54++)) {{
          val__56 = BUFFER_2_29[TAIL_2_29]; TAIL_2_29++;
;
          (Sum__55 = (Sum__55 + val__56))/*float*/;
          __push_29_30(val__56);
        }
      }
      (__tmp8__57 = (Sum__55 / ((float)256.0)))/*float*/;
      __push_29_30(__tmp8__57);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_3;
int __counter_3 = 0;
int __steady_3 = 0;
int __tmp_3 = 0;
int __tmp2_3 = 0;
int *__state_flag_3 = NULL;
thread_info *__thread_3 = NULL;



void save_peek_buffer__3(object_write_buffer *buf);
void load_peek_buffer__3(object_write_buffer *buf);
void save_file_pointer__3(object_write_buffer *buf);
void load_file_pointer__3(object_write_buffer *buf);

inline void check_status__3() {
  check_thread_status(__state_flag_3, __thread_3);
}

void check_status_during_io__3() {
  check_thread_status_during_io(__state_flag_3, __thread_3);
}

void __init_thread_info_3(thread_info *info) {
  __state_flag_3 = info->get_state_flag();
}

thread_info *__get_thread_info_3() {
  if (__thread_3 != NULL) return __thread_3;
  __thread_3 = new thread_info(3, check_status_during_io__3);
  __init_thread_info_3(__thread_3);
  return __thread_3;
}

void __declare_sockets_3() {
  init_instance::add_incoming(2,3, DATA_SOCKET);
  init_instance::add_outgoing(3,4, DATA_SOCKET);
}

void __init_sockets_3(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_3() {
}

void __peek_sockets_3() {
}

 
void init_GetAvg__17_60__3();
inline void check_status__3();

void work_GetAvg__17_60__3(int);


inline float __pop_2_3() {
float res=BUFFER_2_3[TAIL_2_3];
TAIL_2_3++;
return res;
}

inline void __pop_2_3(int n) {
TAIL_2_3+=n;
}

inline float __peek_2_3(int offs) {
return BUFFER_2_3[TAIL_2_3+offs];
}



inline void __push_3_4(float data) {
BUFFER_3_4[HEAD_3_4]=data;
HEAD_3_4++;
}



 
void init_GetAvg__17_60__3(){
}
void save_file_pointer__3(object_write_buffer *buf) {}
void load_file_pointer__3(object_write_buffer *buf) {}
 
void work_GetAvg__17_60__3(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__13 = 0;/* int */
      float Sum__14 = 0.0f;/* float */
      float val__15 = 0.0f;/* float */
      float __tmp8__16 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__14 = ((float)0.0))/*float*/;
      for ((i__13 = 0)/*int*/; (i__13 < 256); (i__13++)) {{
          val__15 = BUFFER_2_3[TAIL_2_3]; TAIL_2_3++;
;
          (Sum__14 = (Sum__14 + val__15))/*float*/;
          __push_3_4(val__15);
        }
      }
      (__tmp8__16 = (Sum__14 / ((float)256.0)))/*float*/;
      __push_3_4(__tmp8__16);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_30;
int __counter_30 = 0;
int __steady_30 = 0;
int __tmp_30 = 0;
int __tmp2_30 = 0;
int *__state_flag_30 = NULL;
thread_info *__thread_30 = NULL;



float avg__59__30 = 0.0f;
int i__60__30 = 0;
void save_peek_buffer__30(object_write_buffer *buf);
void load_peek_buffer__30(object_write_buffer *buf);
void save_file_pointer__30(object_write_buffer *buf);
void load_file_pointer__30(object_write_buffer *buf);

inline void check_status__30() {
  check_thread_status(__state_flag_30, __thread_30);
}

void check_status_during_io__30() {
  check_thread_status_during_io(__state_flag_30, __thread_30);
}

void __init_thread_info_30(thread_info *info) {
  __state_flag_30 = info->get_state_flag();
}

thread_info *__get_thread_info_30() {
  if (__thread_30 != NULL) return __thread_30;
  __thread_30 = new thread_info(30, check_status_during_io__30);
  __init_thread_info_30(__thread_30);
  return __thread_30;
}

void __declare_sockets_30() {
  init_instance::add_incoming(29,30, DATA_SOCKET);
  init_instance::add_outgoing(30,31, DATA_SOCKET);
}

void __init_sockets_30(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_30() {
}

void __peek_sockets_30() {
}

 
void init_DataMinusAvg__66_70__30();
inline void check_status__30();

void work_DataMinusAvg__66_70__30(int);


inline float __pop_29_30() {
float res=BUFFER_29_30[TAIL_29_30];
TAIL_29_30++;
return res;
}

inline void __pop_29_30(int n) {
TAIL_29_30+=n;
}

inline float __peek_29_30(int offs) {
return BUFFER_29_30[TAIL_29_30+offs];
}



inline void __push_30_31(float data) {
BUFFER_30_31[HEAD_30_31]=data;
HEAD_30_31++;
}



 
void init_DataMinusAvg__66_70__30(){
}
void save_file_pointer__30(object_write_buffer *buf) {}
void load_file_pointer__30(object_write_buffer *buf) {}
 
void work_DataMinusAvg__66_70__30(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__63 = 0.0f;/* float */
      float v__64 = 0.0f;/* float */
      float __tmp11__65 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__63 = BUFFER_29_30[TAIL_29_30+256];
;
      ((avg__59__30) = __tmp9__63)/*float*/;
      for (((i__60__30) = 0)/*int*/; ((i__60__30) < 256); ((i__60__30)++)) {{
          v__64 = BUFFER_29_30[TAIL_29_30]; TAIL_29_30++;
;
          (__tmp11__65 = (v__64 - (avg__59__30)))/*float*/;
          __push_30_31(__tmp11__65);
        }
      }
      __pop_29_30();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_31;
int __counter_31 = 0;
int __steady_31 = 0;
int __tmp_31 = 0;
int __tmp2_31 = 0;
int *__state_flag_31 = NULL;
thread_info *__thread_31 = NULL;



inline void check_status__31() {
  check_thread_status(__state_flag_31, __thread_31);
}

void check_status_during_io__31() {
  check_thread_status_during_io(__state_flag_31, __thread_31);
}

void __init_thread_info_31(thread_info *info) {
  __state_flag_31 = info->get_state_flag();
}

thread_info *__get_thread_info_31() {
  if (__thread_31 != NULL) return __thread_31;
  __thread_31 = new thread_info(31, check_status_during_io__31);
  __init_thread_info_31(__thread_31);
  return __thread_31;
}

void __declare_sockets_31() {
  init_instance::add_incoming(30,31, DATA_SOCKET);
  init_instance::add_outgoing(31,32, DATA_SOCKET);
  init_instance::add_outgoing(31,34, DATA_SOCKET);
}

void __init_sockets_31(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_31() {
}

void __peek_sockets_31() {
}

inline float __pop_30_31() {
float res=BUFFER_30_31[TAIL_30_31];
TAIL_30_31++;
return res;
}


inline void __push_31_32(float data) {
BUFFER_31_32[HEAD_31_32]=data;
HEAD_31_32++;
}



inline void __push_31_34(float data) {
BUFFER_31_34[HEAD_31_34]=data;
HEAD_31_34++;
}



void __splitter_31_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_30_31[TAIL_30_31]; TAIL_30_31++;
;
  __push_31_32(tmp);
  __push_31_34(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_32;
int __counter_32 = 0;
int __steady_32 = 0;
int __tmp_32 = 0;
int __tmp2_32 = 0;
int *__state_flag_32 = NULL;
thread_info *__thread_32 = NULL;



float data__67__32 = 0.0f;
int i__68__32 = 0;
void save_peek_buffer__32(object_write_buffer *buf);
void load_peek_buffer__32(object_write_buffer *buf);
void save_file_pointer__32(object_write_buffer *buf);
void load_file_pointer__32(object_write_buffer *buf);

inline void check_status__32() {
  check_thread_status(__state_flag_32, __thread_32);
}

void check_status_during_io__32() {
  check_thread_status_during_io(__state_flag_32, __thread_32);
}

void __init_thread_info_32(thread_info *info) {
  __state_flag_32 = info->get_state_flag();
}

thread_info *__get_thread_info_32() {
  if (__thread_32 != NULL) return __thread_32;
  __thread_32 = new thread_info(32, check_status_during_io__32);
  __init_thread_info_32(__thread_32);
  return __thread_32;
}

void __declare_sockets_32() {
  init_instance::add_incoming(31,32, DATA_SOCKET);
  init_instance::add_outgoing(32,33, DATA_SOCKET);
}

void __init_sockets_32(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_32() {
}

void __peek_sockets_32() {
}

 
void init_DenomDataSum__72_72__32();
inline void check_status__32();

void work_DenomDataSum__72_72__32(int);


inline float __pop_31_32() {
float res=BUFFER_31_32[TAIL_31_32];
TAIL_31_32++;
return res;
}

inline void __pop_31_32(int n) {
TAIL_31_32+=n;
}

inline float __peek_31_32(int offs) {
return BUFFER_31_32[TAIL_31_32+offs];
}



inline void __push_32_33(float data) {
BUFFER_32_33[HEAD_32_33]=data;
HEAD_32_33++;
}



 
void init_DenomDataSum__72_72__32(){
}
void save_file_pointer__32(object_write_buffer *buf) {}
void load_file_pointer__32(object_write_buffer *buf) {}
 
void work_DenomDataSum__72_72__32(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__71 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__71 = ((float)0.0))/*float*/;
      for (((i__68__32) = 0)/*int*/; ((i__68__32) < 256); ((i__68__32)++)) {{
          (data__67__32) = BUFFER_31_32[TAIL_31_32]; TAIL_31_32++;
;
          (sum__71 = (sum__71 + ((data__67__32) * (data__67__32))))/*float*/;
        }
      }
      __push_32_33(sum__71);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_33;
int __counter_33 = 0;
int __steady_33 = 0;
int __tmp_33 = 0;
int __tmp2_33 = 0;
int *__state_flag_33 = NULL;
thread_info *__thread_33 = NULL;



inline void check_status__33() {
  check_thread_status(__state_flag_33, __thread_33);
}

void check_status_during_io__33() {
  check_thread_status_during_io(__state_flag_33, __thread_33);
}

void __init_thread_info_33(thread_info *info) {
  __state_flag_33 = info->get_state_flag();
}

thread_info *__get_thread_info_33() {
  if (__thread_33 != NULL) return __thread_33;
  __thread_33 = new thread_info(33, check_status_during_io__33);
  __init_thread_info_33(__thread_33);
  return __thread_33;
}

void __declare_sockets_33() {
  init_instance::add_incoming(32,33, DATA_SOCKET);
  init_instance::add_incoming(34,33, DATA_SOCKET);
  init_instance::add_outgoing(33,8, DATA_SOCKET);
}

void __init_sockets_33(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_33() {
}

void __peek_sockets_33() {
}


inline void __push_33_8(float data) {
BUFFER_33_8[HEAD_33_8]=data;
HEAD_33_8++;
}


inline float __pop_32_33() {
float res=BUFFER_32_33[TAIL_32_33];
TAIL_32_33++;
return res;
}

inline float __pop_34_33() {
float res=BUFFER_34_33[TAIL_34_33];
TAIL_34_33++;
return res;
}


void __joiner_33_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_33_8(__pop_32_33());
    __push_33_8(__pop_34_33());
    __push_33_8(__pop_34_33());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_34;
int __counter_34 = 0;
int __steady_34 = 0;
int __tmp_34 = 0;
int __tmp2_34 = 0;
int *__state_flag_34 = NULL;
thread_info *__thread_34 = NULL;



float _TheGlobal__pattern__6____34[256] = {0};
float numerSum__74__34 = 0.0f;
float denomPatternSum__75__34 = 0.0f;
float data__76__34 = 0.0f;
float patternData__77__34 = 0.0f;
int i__78__34 = 0;
void save_peek_buffer__34(object_write_buffer *buf);
void load_peek_buffer__34(object_write_buffer *buf);
void save_file_pointer__34(object_write_buffer *buf);
void load_file_pointer__34(object_write_buffer *buf);

inline void check_status__34() {
  check_thread_status(__state_flag_34, __thread_34);
}

void check_status_during_io__34() {
  check_thread_status_during_io(__state_flag_34, __thread_34);
}

void __init_thread_info_34(thread_info *info) {
  __state_flag_34 = info->get_state_flag();
}

thread_info *__get_thread_info_34() {
  if (__thread_34 != NULL) return __thread_34;
  __thread_34 = new thread_info(34, check_status_during_io__34);
  __init_thread_info_34(__thread_34);
  return __thread_34;
}

void __declare_sockets_34() {
  init_instance::add_incoming(31,34, DATA_SOCKET);
  init_instance::add_outgoing(34,33, DATA_SOCKET);
}

void __init_sockets_34(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_34() {
}

void __peek_sockets_34() {
}

 
void init_PatternSums__81_73__34();
inline void check_status__34();

void work_PatternSums__81_73__34(int);


inline float __pop_31_34() {
float res=BUFFER_31_34[TAIL_31_34];
TAIL_31_34++;
return res;
}

inline void __pop_31_34(int n) {
TAIL_31_34+=n;
}

inline float __peek_31_34(int offs) {
return BUFFER_31_34[TAIL_31_34+offs];
}



inline void __push_34_33(float data) {
BUFFER_34_33[HEAD_34_33]=data;
HEAD_34_33++;
}



 
void init_PatternSums__81_73__34(){
  (((_TheGlobal__pattern__6____34)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____34)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__34(object_write_buffer *buf) {}
void load_file_pointer__34(object_write_buffer *buf) {}
 
void work_PatternSums__81_73__34(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__74__34) = ((float)0.0))/*float*/;
      ((denomPatternSum__75__34) = ((float)0.0))/*float*/;
      for (((i__78__34) = 0)/*int*/; ((i__78__34) < 256); ((i__78__34)++)) {{
          ((patternData__77__34) = (((_TheGlobal__pattern__6____34)[(int)(i__78__34)]) - ((float)1.6013207E-4)))/*float*/;
          (data__76__34) = BUFFER_31_34[TAIL_31_34]; TAIL_31_34++;
;
          ((numerSum__74__34) = ((numerSum__74__34) + ((data__76__34) * (patternData__77__34))))/*float*/;
          ((denomPatternSum__75__34) = ((denomPatternSum__75__34) + ((patternData__77__34) * (patternData__77__34))))/*float*/;
        }
      }
      __push_34_33((denomPatternSum__75__34));
      __push_34_33((numerSum__74__34));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_35;
int __counter_35 = 0;
int __steady_35 = 0;
int __tmp_35 = 0;
int __tmp2_35 = 0;
int *__state_flag_35 = NULL;
thread_info *__thread_35 = NULL;



void save_peek_buffer__35(object_write_buffer *buf);
void load_peek_buffer__35(object_write_buffer *buf);
void save_file_pointer__35(object_write_buffer *buf);
void load_file_pointer__35(object_write_buffer *buf);

inline void check_status__35() {
  check_thread_status(__state_flag_35, __thread_35);
}

void check_status_during_io__35() {
  check_thread_status_during_io(__state_flag_35, __thread_35);
}

void __init_thread_info_35(thread_info *info) {
  __state_flag_35 = info->get_state_flag();
}

thread_info *__get_thread_info_35() {
  if (__thread_35 != NULL) return __thread_35;
  __thread_35 = new thread_info(35, check_status_during_io__35);
  __init_thread_info_35(__thread_35);
  return __thread_35;
}

void __declare_sockets_35() {
  init_instance::add_incoming(2,35, DATA_SOCKET);
  init_instance::add_outgoing(35,36, DATA_SOCKET);
}

void __init_sockets_35(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_35() {
}

void __peek_sockets_35() {
}

 
void init_GetAvg__99_76__35();
inline void check_status__35();

void work_GetAvg__99_76__35(int);


inline float __pop_2_35() {
float res=BUFFER_2_35[TAIL_2_35];
TAIL_2_35++;
return res;
}

inline void __pop_2_35(int n) {
TAIL_2_35+=n;
}

inline float __peek_2_35(int offs) {
return BUFFER_2_35[TAIL_2_35+offs];
}



inline void __push_35_36(float data) {
BUFFER_35_36[HEAD_35_36]=data;
HEAD_35_36++;
}



 
void init_GetAvg__99_76__35(){
}
void save_file_pointer__35(object_write_buffer *buf) {}
void load_file_pointer__35(object_write_buffer *buf) {}
 
void work_GetAvg__99_76__35(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__95 = 0;/* int */
      float Sum__96 = 0.0f;/* float */
      float val__97 = 0.0f;/* float */
      float __tmp8__98 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__96 = ((float)0.0))/*float*/;
      for ((i__95 = 0)/*int*/; (i__95 < 256); (i__95++)) {{
          val__97 = BUFFER_2_35[TAIL_2_35]; TAIL_2_35++;
;
          (Sum__96 = (Sum__96 + val__97))/*float*/;
          __push_35_36(val__97);
        }
      }
      (__tmp8__98 = (Sum__96 / ((float)256.0)))/*float*/;
      __push_35_36(__tmp8__98);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_36;
int __counter_36 = 0;
int __steady_36 = 0;
int __tmp_36 = 0;
int __tmp2_36 = 0;
int *__state_flag_36 = NULL;
thread_info *__thread_36 = NULL;



float avg__100__36 = 0.0f;
int i__101__36 = 0;
void save_peek_buffer__36(object_write_buffer *buf);
void load_peek_buffer__36(object_write_buffer *buf);
void save_file_pointer__36(object_write_buffer *buf);
void load_file_pointer__36(object_write_buffer *buf);

inline void check_status__36() {
  check_thread_status(__state_flag_36, __thread_36);
}

void check_status_during_io__36() {
  check_thread_status_during_io(__state_flag_36, __thread_36);
}

void __init_thread_info_36(thread_info *info) {
  __state_flag_36 = info->get_state_flag();
}

thread_info *__get_thread_info_36() {
  if (__thread_36 != NULL) return __thread_36;
  __thread_36 = new thread_info(36, check_status_during_io__36);
  __init_thread_info_36(__thread_36);
  return __thread_36;
}

void __declare_sockets_36() {
  init_instance::add_incoming(35,36, DATA_SOCKET);
  init_instance::add_outgoing(36,37, DATA_SOCKET);
}

void __init_sockets_36(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_36() {
}

void __peek_sockets_36() {
}

 
void init_DataMinusAvg__107_78__36();
inline void check_status__36();

void work_DataMinusAvg__107_78__36(int);


inline float __pop_35_36() {
float res=BUFFER_35_36[TAIL_35_36];
TAIL_35_36++;
return res;
}

inline void __pop_35_36(int n) {
TAIL_35_36+=n;
}

inline float __peek_35_36(int offs) {
return BUFFER_35_36[TAIL_35_36+offs];
}



inline void __push_36_37(float data) {
BUFFER_36_37[HEAD_36_37]=data;
HEAD_36_37++;
}



 
void init_DataMinusAvg__107_78__36(){
}
void save_file_pointer__36(object_write_buffer *buf) {}
void load_file_pointer__36(object_write_buffer *buf) {}
 
void work_DataMinusAvg__107_78__36(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__104 = 0.0f;/* float */
      float v__105 = 0.0f;/* float */
      float __tmp11__106 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__104 = BUFFER_35_36[TAIL_35_36+256];
;
      ((avg__100__36) = __tmp9__104)/*float*/;
      for (((i__101__36) = 0)/*int*/; ((i__101__36) < 256); ((i__101__36)++)) {{
          v__105 = BUFFER_35_36[TAIL_35_36]; TAIL_35_36++;
;
          (__tmp11__106 = (v__105 - (avg__100__36)))/*float*/;
          __push_36_37(__tmp11__106);
        }
      }
      __pop_35_36();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_37;
int __counter_37 = 0;
int __steady_37 = 0;
int __tmp_37 = 0;
int __tmp2_37 = 0;
int *__state_flag_37 = NULL;
thread_info *__thread_37 = NULL;



inline void check_status__37() {
  check_thread_status(__state_flag_37, __thread_37);
}

void check_status_during_io__37() {
  check_thread_status_during_io(__state_flag_37, __thread_37);
}

void __init_thread_info_37(thread_info *info) {
  __state_flag_37 = info->get_state_flag();
}

thread_info *__get_thread_info_37() {
  if (__thread_37 != NULL) return __thread_37;
  __thread_37 = new thread_info(37, check_status_during_io__37);
  __init_thread_info_37(__thread_37);
  return __thread_37;
}

void __declare_sockets_37() {
  init_instance::add_incoming(36,37, DATA_SOCKET);
  init_instance::add_outgoing(37,38, DATA_SOCKET);
  init_instance::add_outgoing(37,40, DATA_SOCKET);
}

void __init_sockets_37(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_37() {
}

void __peek_sockets_37() {
}

inline float __pop_36_37() {
float res=BUFFER_36_37[TAIL_36_37];
TAIL_36_37++;
return res;
}


inline void __push_37_38(float data) {
BUFFER_37_38[HEAD_37_38]=data;
HEAD_37_38++;
}



inline void __push_37_40(float data) {
BUFFER_37_40[HEAD_37_40]=data;
HEAD_37_40++;
}



void __splitter_37_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_36_37[TAIL_36_37]; TAIL_36_37++;
;
  __push_37_38(tmp);
  __push_37_40(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_38;
int __counter_38 = 0;
int __steady_38 = 0;
int __tmp_38 = 0;
int __tmp2_38 = 0;
int *__state_flag_38 = NULL;
thread_info *__thread_38 = NULL;



float data__108__38 = 0.0f;
int i__109__38 = 0;
void save_peek_buffer__38(object_write_buffer *buf);
void load_peek_buffer__38(object_write_buffer *buf);
void save_file_pointer__38(object_write_buffer *buf);
void load_file_pointer__38(object_write_buffer *buf);

inline void check_status__38() {
  check_thread_status(__state_flag_38, __thread_38);
}

void check_status_during_io__38() {
  check_thread_status_during_io(__state_flag_38, __thread_38);
}

void __init_thread_info_38(thread_info *info) {
  __state_flag_38 = info->get_state_flag();
}

thread_info *__get_thread_info_38() {
  if (__thread_38 != NULL) return __thread_38;
  __thread_38 = new thread_info(38, check_status_during_io__38);
  __init_thread_info_38(__thread_38);
  return __thread_38;
}

void __declare_sockets_38() {
  init_instance::add_incoming(37,38, DATA_SOCKET);
  init_instance::add_outgoing(38,39, DATA_SOCKET);
}

void __init_sockets_38(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_38() {
}

void __peek_sockets_38() {
}

 
void init_DenomDataSum__113_80__38();
inline void check_status__38();

void work_DenomDataSum__113_80__38(int);


inline float __pop_37_38() {
float res=BUFFER_37_38[TAIL_37_38];
TAIL_37_38++;
return res;
}

inline void __pop_37_38(int n) {
TAIL_37_38+=n;
}

inline float __peek_37_38(int offs) {
return BUFFER_37_38[TAIL_37_38+offs];
}



inline void __push_38_39(float data) {
BUFFER_38_39[HEAD_38_39]=data;
HEAD_38_39++;
}



 
void init_DenomDataSum__113_80__38(){
}
void save_file_pointer__38(object_write_buffer *buf) {}
void load_file_pointer__38(object_write_buffer *buf) {}
 
void work_DenomDataSum__113_80__38(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__112 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__112 = ((float)0.0))/*float*/;
      for (((i__109__38) = 0)/*int*/; ((i__109__38) < 256); ((i__109__38)++)) {{
          (data__108__38) = BUFFER_37_38[TAIL_37_38]; TAIL_37_38++;
;
          (sum__112 = (sum__112 + ((data__108__38) * (data__108__38))))/*float*/;
        }
      }
      __push_38_39(sum__112);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_39;
int __counter_39 = 0;
int __steady_39 = 0;
int __tmp_39 = 0;
int __tmp2_39 = 0;
int *__state_flag_39 = NULL;
thread_info *__thread_39 = NULL;



inline void check_status__39() {
  check_thread_status(__state_flag_39, __thread_39);
}

void check_status_during_io__39() {
  check_thread_status_during_io(__state_flag_39, __thread_39);
}

void __init_thread_info_39(thread_info *info) {
  __state_flag_39 = info->get_state_flag();
}

thread_info *__get_thread_info_39() {
  if (__thread_39 != NULL) return __thread_39;
  __thread_39 = new thread_info(39, check_status_during_io__39);
  __init_thread_info_39(__thread_39);
  return __thread_39;
}

void __declare_sockets_39() {
  init_instance::add_incoming(38,39, DATA_SOCKET);
  init_instance::add_incoming(40,39, DATA_SOCKET);
  init_instance::add_outgoing(39,8, DATA_SOCKET);
}

void __init_sockets_39(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_39() {
}

void __peek_sockets_39() {
}


inline void __push_39_8(float data) {
BUFFER_39_8[HEAD_39_8]=data;
HEAD_39_8++;
}


inline float __pop_38_39() {
float res=BUFFER_38_39[TAIL_38_39];
TAIL_38_39++;
return res;
}

inline float __pop_40_39() {
float res=BUFFER_40_39[TAIL_40_39];
TAIL_40_39++;
return res;
}


void __joiner_39_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_39_8(__pop_38_39());
    __push_39_8(__pop_40_39());
    __push_39_8(__pop_40_39());
  }
}


// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_4;
int __counter_4 = 0;
int __steady_4 = 0;
int __tmp_4 = 0;
int __tmp2_4 = 0;
int *__state_flag_4 = NULL;
thread_info *__thread_4 = NULL;



float avg__18__4 = 0.0f;
int i__19__4 = 0;
void save_peek_buffer__4(object_write_buffer *buf);
void load_peek_buffer__4(object_write_buffer *buf);
void save_file_pointer__4(object_write_buffer *buf);
void load_file_pointer__4(object_write_buffer *buf);

inline void check_status__4() {
  check_thread_status(__state_flag_4, __thread_4);
}

void check_status_during_io__4() {
  check_thread_status_during_io(__state_flag_4, __thread_4);
}

void __init_thread_info_4(thread_info *info) {
  __state_flag_4 = info->get_state_flag();
}

thread_info *__get_thread_info_4() {
  if (__thread_4 != NULL) return __thread_4;
  __thread_4 = new thread_info(4, check_status_during_io__4);
  __init_thread_info_4(__thread_4);
  return __thread_4;
}

void __declare_sockets_4() {
  init_instance::add_incoming(3,4, DATA_SOCKET);
  init_instance::add_outgoing(4,5, DATA_SOCKET);
}

void __init_sockets_4(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_4() {
}

void __peek_sockets_4() {
}

 
void init_DataMinusAvg__25_62__4();
inline void check_status__4();

void work_DataMinusAvg__25_62__4(int);


inline float __pop_3_4() {
float res=BUFFER_3_4[TAIL_3_4];
TAIL_3_4++;
return res;
}

inline void __pop_3_4(int n) {
TAIL_3_4+=n;
}

inline float __peek_3_4(int offs) {
return BUFFER_3_4[TAIL_3_4+offs];
}



inline void __push_4_5(float data) {
BUFFER_4_5[HEAD_4_5]=data;
HEAD_4_5++;
}



 
void init_DataMinusAvg__25_62__4(){
}
void save_file_pointer__4(object_write_buffer *buf) {}
void load_file_pointer__4(object_write_buffer *buf) {}
 
void work_DataMinusAvg__25_62__4(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__22 = 0.0f;/* float */
      float v__23 = 0.0f;/* float */
      float __tmp11__24 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__22 = BUFFER_3_4[TAIL_3_4+256];
;
      ((avg__18__4) = __tmp9__22)/*float*/;
      for (((i__19__4) = 0)/*int*/; ((i__19__4) < 256); ((i__19__4)++)) {{
          v__23 = BUFFER_3_4[TAIL_3_4]; TAIL_3_4++;
;
          (__tmp11__24 = (v__23 - (avg__18__4)))/*float*/;
          __push_4_5(__tmp11__24);
        }
      }
      __pop_3_4();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_40;
int __counter_40 = 0;
int __steady_40 = 0;
int __tmp_40 = 0;
int __tmp2_40 = 0;
int *__state_flag_40 = NULL;
thread_info *__thread_40 = NULL;



float _TheGlobal__pattern__6____40[256] = {0};
float numerSum__115__40 = 0.0f;
float denomPatternSum__116__40 = 0.0f;
float data__117__40 = 0.0f;
float patternData__118__40 = 0.0f;
int i__119__40 = 0;
void save_peek_buffer__40(object_write_buffer *buf);
void load_peek_buffer__40(object_write_buffer *buf);
void save_file_pointer__40(object_write_buffer *buf);
void load_file_pointer__40(object_write_buffer *buf);

inline void check_status__40() {
  check_thread_status(__state_flag_40, __thread_40);
}

void check_status_during_io__40() {
  check_thread_status_during_io(__state_flag_40, __thread_40);
}

void __init_thread_info_40(thread_info *info) {
  __state_flag_40 = info->get_state_flag();
}

thread_info *__get_thread_info_40() {
  if (__thread_40 != NULL) return __thread_40;
  __thread_40 = new thread_info(40, check_status_during_io__40);
  __init_thread_info_40(__thread_40);
  return __thread_40;
}

void __declare_sockets_40() {
  init_instance::add_incoming(37,40, DATA_SOCKET);
  init_instance::add_outgoing(40,39, DATA_SOCKET);
}

void __init_sockets_40(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_40() {
}

void __peek_sockets_40() {
}

 
void init_PatternSums__122_81__40();
inline void check_status__40();

void work_PatternSums__122_81__40(int);


inline float __pop_37_40() {
float res=BUFFER_37_40[TAIL_37_40];
TAIL_37_40++;
return res;
}

inline void __pop_37_40(int n) {
TAIL_37_40+=n;
}

inline float __peek_37_40(int offs) {
return BUFFER_37_40[TAIL_37_40+offs];
}



inline void __push_40_39(float data) {
BUFFER_40_39[HEAD_40_39]=data;
HEAD_40_39++;
}



 
void init_PatternSums__122_81__40(){
  (((_TheGlobal__pattern__6____40)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____40)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__40(object_write_buffer *buf) {}
void load_file_pointer__40(object_write_buffer *buf) {}
 
void work_PatternSums__122_81__40(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__115__40) = ((float)0.0))/*float*/;
      ((denomPatternSum__116__40) = ((float)0.0))/*float*/;
      for (((i__119__40) = 0)/*int*/; ((i__119__40) < 256); ((i__119__40)++)) {{
          ((patternData__118__40) = (((_TheGlobal__pattern__6____40)[(int)(i__119__40)]) - ((float)1.6013207E-4)))/*float*/;
          (data__117__40) = BUFFER_37_40[TAIL_37_40]; TAIL_37_40++;
;
          ((numerSum__115__40) = ((numerSum__115__40) + ((data__117__40) * (patternData__118__40))))/*float*/;
          ((denomPatternSum__116__40) = ((denomPatternSum__116__40) + ((patternData__118__40) * (patternData__118__40))))/*float*/;
        }
      }
      __push_40_39((denomPatternSum__116__40));
      __push_40_39((numerSum__115__40));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_41;
int __counter_41 = 0;
int __steady_41 = 0;
int __tmp_41 = 0;
int __tmp2_41 = 0;
int *__state_flag_41 = NULL;
thread_info *__thread_41 = NULL;



void save_peek_buffer__41(object_write_buffer *buf);
void load_peek_buffer__41(object_write_buffer *buf);
void save_file_pointer__41(object_write_buffer *buf);
void load_file_pointer__41(object_write_buffer *buf);

inline void check_status__41() {
  check_thread_status(__state_flag_41, __thread_41);
}

void check_status_during_io__41() {
  check_thread_status_during_io(__state_flag_41, __thread_41);
}

void __init_thread_info_41(thread_info *info) {
  __state_flag_41 = info->get_state_flag();
}

thread_info *__get_thread_info_41() {
  if (__thread_41 != NULL) return __thread_41;
  __thread_41 = new thread_info(41, check_status_during_io__41);
  __init_thread_info_41(__thread_41);
  return __thread_41;
}

void __declare_sockets_41() {
  init_instance::add_incoming(2,41, DATA_SOCKET);
  init_instance::add_outgoing(41,42, DATA_SOCKET);
}

void __init_sockets_41(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_41() {
}

void __peek_sockets_41() {
}

 
void init_GetAvg__140_84__41();
inline void check_status__41();

void work_GetAvg__140_84__41(int);


inline float __pop_2_41() {
float res=BUFFER_2_41[TAIL_2_41];
TAIL_2_41++;
return res;
}

inline void __pop_2_41(int n) {
TAIL_2_41+=n;
}

inline float __peek_2_41(int offs) {
return BUFFER_2_41[TAIL_2_41+offs];
}



inline void __push_41_42(float data) {
BUFFER_41_42[HEAD_41_42]=data;
HEAD_41_42++;
}



 
void init_GetAvg__140_84__41(){
}
void save_file_pointer__41(object_write_buffer *buf) {}
void load_file_pointer__41(object_write_buffer *buf) {}
 
void work_GetAvg__140_84__41(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__136 = 0;/* int */
      float Sum__137 = 0.0f;/* float */
      float val__138 = 0.0f;/* float */
      float __tmp8__139 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__137 = ((float)0.0))/*float*/;
      for ((i__136 = 0)/*int*/; (i__136 < 256); (i__136++)) {{
          val__138 = BUFFER_2_41[TAIL_2_41]; TAIL_2_41++;
;
          (Sum__137 = (Sum__137 + val__138))/*float*/;
          __push_41_42(val__138);
        }
      }
      (__tmp8__139 = (Sum__137 / ((float)256.0)))/*float*/;
      __push_41_42(__tmp8__139);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_42;
int __counter_42 = 0;
int __steady_42 = 0;
int __tmp_42 = 0;
int __tmp2_42 = 0;
int *__state_flag_42 = NULL;
thread_info *__thread_42 = NULL;



float avg__141__42 = 0.0f;
int i__142__42 = 0;
void save_peek_buffer__42(object_write_buffer *buf);
void load_peek_buffer__42(object_write_buffer *buf);
void save_file_pointer__42(object_write_buffer *buf);
void load_file_pointer__42(object_write_buffer *buf);

inline void check_status__42() {
  check_thread_status(__state_flag_42, __thread_42);
}

void check_status_during_io__42() {
  check_thread_status_during_io(__state_flag_42, __thread_42);
}

void __init_thread_info_42(thread_info *info) {
  __state_flag_42 = info->get_state_flag();
}

thread_info *__get_thread_info_42() {
  if (__thread_42 != NULL) return __thread_42;
  __thread_42 = new thread_info(42, check_status_during_io__42);
  __init_thread_info_42(__thread_42);
  return __thread_42;
}

void __declare_sockets_42() {
  init_instance::add_incoming(41,42, DATA_SOCKET);
  init_instance::add_outgoing(42,43, DATA_SOCKET);
}

void __init_sockets_42(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_42() {
}

void __peek_sockets_42() {
}

 
void init_DataMinusAvg__148_86__42();
inline void check_status__42();

void work_DataMinusAvg__148_86__42(int);


inline float __pop_41_42() {
float res=BUFFER_41_42[TAIL_41_42];
TAIL_41_42++;
return res;
}

inline void __pop_41_42(int n) {
TAIL_41_42+=n;
}

inline float __peek_41_42(int offs) {
return BUFFER_41_42[TAIL_41_42+offs];
}



inline void __push_42_43(float data) {
BUFFER_42_43[HEAD_42_43]=data;
HEAD_42_43++;
}



 
void init_DataMinusAvg__148_86__42(){
}
void save_file_pointer__42(object_write_buffer *buf) {}
void load_file_pointer__42(object_write_buffer *buf) {}
 
void work_DataMinusAvg__148_86__42(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__145 = 0.0f;/* float */
      float v__146 = 0.0f;/* float */
      float __tmp11__147 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__145 = BUFFER_41_42[TAIL_41_42+256];
;
      ((avg__141__42) = __tmp9__145)/*float*/;
      for (((i__142__42) = 0)/*int*/; ((i__142__42) < 256); ((i__142__42)++)) {{
          v__146 = BUFFER_41_42[TAIL_41_42]; TAIL_41_42++;
;
          (__tmp11__147 = (v__146 - (avg__141__42)))/*float*/;
          __push_42_43(__tmp11__147);
        }
      }
      __pop_41_42();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_43;
int __counter_43 = 0;
int __steady_43 = 0;
int __tmp_43 = 0;
int __tmp2_43 = 0;
int *__state_flag_43 = NULL;
thread_info *__thread_43 = NULL;



inline void check_status__43() {
  check_thread_status(__state_flag_43, __thread_43);
}

void check_status_during_io__43() {
  check_thread_status_during_io(__state_flag_43, __thread_43);
}

void __init_thread_info_43(thread_info *info) {
  __state_flag_43 = info->get_state_flag();
}

thread_info *__get_thread_info_43() {
  if (__thread_43 != NULL) return __thread_43;
  __thread_43 = new thread_info(43, check_status_during_io__43);
  __init_thread_info_43(__thread_43);
  return __thread_43;
}

void __declare_sockets_43() {
  init_instance::add_incoming(42,43, DATA_SOCKET);
  init_instance::add_outgoing(43,44, DATA_SOCKET);
  init_instance::add_outgoing(43,46, DATA_SOCKET);
}

void __init_sockets_43(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_43() {
}

void __peek_sockets_43() {
}

inline float __pop_42_43() {
float res=BUFFER_42_43[TAIL_42_43];
TAIL_42_43++;
return res;
}


inline void __push_43_44(float data) {
BUFFER_43_44[HEAD_43_44]=data;
HEAD_43_44++;
}



inline void __push_43_46(float data) {
BUFFER_43_46[HEAD_43_46]=data;
HEAD_43_46++;
}



void __splitter_43_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_42_43[TAIL_42_43]; TAIL_42_43++;
;
  __push_43_44(tmp);
  __push_43_46(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_44;
int __counter_44 = 0;
int __steady_44 = 0;
int __tmp_44 = 0;
int __tmp2_44 = 0;
int *__state_flag_44 = NULL;
thread_info *__thread_44 = NULL;



float data__149__44 = 0.0f;
int i__150__44 = 0;
void save_peek_buffer__44(object_write_buffer *buf);
void load_peek_buffer__44(object_write_buffer *buf);
void save_file_pointer__44(object_write_buffer *buf);
void load_file_pointer__44(object_write_buffer *buf);

inline void check_status__44() {
  check_thread_status(__state_flag_44, __thread_44);
}

void check_status_during_io__44() {
  check_thread_status_during_io(__state_flag_44, __thread_44);
}

void __init_thread_info_44(thread_info *info) {
  __state_flag_44 = info->get_state_flag();
}

thread_info *__get_thread_info_44() {
  if (__thread_44 != NULL) return __thread_44;
  __thread_44 = new thread_info(44, check_status_during_io__44);
  __init_thread_info_44(__thread_44);
  return __thread_44;
}

void __declare_sockets_44() {
  init_instance::add_incoming(43,44, DATA_SOCKET);
  init_instance::add_outgoing(44,45, DATA_SOCKET);
}

void __init_sockets_44(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_44() {
}

void __peek_sockets_44() {
}

 
void init_DenomDataSum__154_88__44();
inline void check_status__44();

void work_DenomDataSum__154_88__44(int);


inline float __pop_43_44() {
float res=BUFFER_43_44[TAIL_43_44];
TAIL_43_44++;
return res;
}

inline void __pop_43_44(int n) {
TAIL_43_44+=n;
}

inline float __peek_43_44(int offs) {
return BUFFER_43_44[TAIL_43_44+offs];
}



inline void __push_44_45(float data) {
BUFFER_44_45[HEAD_44_45]=data;
HEAD_44_45++;
}



 
void init_DenomDataSum__154_88__44(){
}
void save_file_pointer__44(object_write_buffer *buf) {}
void load_file_pointer__44(object_write_buffer *buf) {}
 
void work_DenomDataSum__154_88__44(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__153 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__153 = ((float)0.0))/*float*/;
      for (((i__150__44) = 0)/*int*/; ((i__150__44) < 256); ((i__150__44)++)) {{
          (data__149__44) = BUFFER_43_44[TAIL_43_44]; TAIL_43_44++;
;
          (sum__153 = (sum__153 + ((data__149__44) * (data__149__44))))/*float*/;
        }
      }
      __push_44_45(sum__153);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_45;
int __counter_45 = 0;
int __steady_45 = 0;
int __tmp_45 = 0;
int __tmp2_45 = 0;
int *__state_flag_45 = NULL;
thread_info *__thread_45 = NULL;



inline void check_status__45() {
  check_thread_status(__state_flag_45, __thread_45);
}

void check_status_during_io__45() {
  check_thread_status_during_io(__state_flag_45, __thread_45);
}

void __init_thread_info_45(thread_info *info) {
  __state_flag_45 = info->get_state_flag();
}

thread_info *__get_thread_info_45() {
  if (__thread_45 != NULL) return __thread_45;
  __thread_45 = new thread_info(45, check_status_during_io__45);
  __init_thread_info_45(__thread_45);
  return __thread_45;
}

void __declare_sockets_45() {
  init_instance::add_incoming(44,45, DATA_SOCKET);
  init_instance::add_incoming(46,45, DATA_SOCKET);
  init_instance::add_outgoing(45,8, DATA_SOCKET);
}

void __init_sockets_45(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_45() {
}

void __peek_sockets_45() {
}


inline void __push_45_8(float data) {
BUFFER_45_8[HEAD_45_8]=data;
HEAD_45_8++;
}


inline float __pop_44_45() {
float res=BUFFER_44_45[TAIL_44_45];
TAIL_44_45++;
return res;
}

inline float __pop_46_45() {
float res=BUFFER_46_45[TAIL_46_45];
TAIL_46_45++;
return res;
}


void __joiner_45_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_45_8(__pop_44_45());
    __push_45_8(__pop_46_45());
    __push_45_8(__pop_46_45());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_46;
int __counter_46 = 0;
int __steady_46 = 0;
int __tmp_46 = 0;
int __tmp2_46 = 0;
int *__state_flag_46 = NULL;
thread_info *__thread_46 = NULL;



float _TheGlobal__pattern__6____46[256] = {0};
float numerSum__156__46 = 0.0f;
float denomPatternSum__157__46 = 0.0f;
float data__158__46 = 0.0f;
float patternData__159__46 = 0.0f;
int i__160__46 = 0;
void save_peek_buffer__46(object_write_buffer *buf);
void load_peek_buffer__46(object_write_buffer *buf);
void save_file_pointer__46(object_write_buffer *buf);
void load_file_pointer__46(object_write_buffer *buf);

inline void check_status__46() {
  check_thread_status(__state_flag_46, __thread_46);
}

void check_status_during_io__46() {
  check_thread_status_during_io(__state_flag_46, __thread_46);
}

void __init_thread_info_46(thread_info *info) {
  __state_flag_46 = info->get_state_flag();
}

thread_info *__get_thread_info_46() {
  if (__thread_46 != NULL) return __thread_46;
  __thread_46 = new thread_info(46, check_status_during_io__46);
  __init_thread_info_46(__thread_46);
  return __thread_46;
}

void __declare_sockets_46() {
  init_instance::add_incoming(43,46, DATA_SOCKET);
  init_instance::add_outgoing(46,45, DATA_SOCKET);
}

void __init_sockets_46(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_46() {
}

void __peek_sockets_46() {
}

 
void init_PatternSums__163_89__46();
inline void check_status__46();

void work_PatternSums__163_89__46(int);


inline float __pop_43_46() {
float res=BUFFER_43_46[TAIL_43_46];
TAIL_43_46++;
return res;
}

inline void __pop_43_46(int n) {
TAIL_43_46+=n;
}

inline float __peek_43_46(int offs) {
return BUFFER_43_46[TAIL_43_46+offs];
}



inline void __push_46_45(float data) {
BUFFER_46_45[HEAD_46_45]=data;
HEAD_46_45++;
}



 
void init_PatternSums__163_89__46(){
  (((_TheGlobal__pattern__6____46)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____46)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__46(object_write_buffer *buf) {}
void load_file_pointer__46(object_write_buffer *buf) {}
 
void work_PatternSums__163_89__46(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__156__46) = ((float)0.0))/*float*/;
      ((denomPatternSum__157__46) = ((float)0.0))/*float*/;
      for (((i__160__46) = 0)/*int*/; ((i__160__46) < 256); ((i__160__46)++)) {{
          ((patternData__159__46) = (((_TheGlobal__pattern__6____46)[(int)(i__160__46)]) - ((float)1.6013207E-4)))/*float*/;
          (data__158__46) = BUFFER_43_46[TAIL_43_46]; TAIL_43_46++;
;
          ((numerSum__156__46) = ((numerSum__156__46) + ((data__158__46) * (patternData__159__46))))/*float*/;
          ((denomPatternSum__157__46) = ((denomPatternSum__157__46) + ((patternData__159__46) * (patternData__159__46))))/*float*/;
        }
      }
      __push_46_45((denomPatternSum__157__46));
      __push_46_45((numerSum__156__46));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_47;
int __counter_47 = 0;
int __steady_47 = 0;
int __tmp_47 = 0;
int __tmp2_47 = 0;
int *__state_flag_47 = NULL;
thread_info *__thread_47 = NULL;



void save_peek_buffer__47(object_write_buffer *buf);
void load_peek_buffer__47(object_write_buffer *buf);
void save_file_pointer__47(object_write_buffer *buf);
void load_file_pointer__47(object_write_buffer *buf);

inline void check_status__47() {
  check_thread_status(__state_flag_47, __thread_47);
}

void check_status_during_io__47() {
  check_thread_status_during_io(__state_flag_47, __thread_47);
}

void __init_thread_info_47(thread_info *info) {
  __state_flag_47 = info->get_state_flag();
}

thread_info *__get_thread_info_47() {
  if (__thread_47 != NULL) return __thread_47;
  __thread_47 = new thread_info(47, check_status_during_io__47);
  __init_thread_info_47(__thread_47);
  return __thread_47;
}

void __declare_sockets_47() {
  init_instance::add_incoming(2,47, DATA_SOCKET);
  init_instance::add_outgoing(47,48, DATA_SOCKET);
}

void __init_sockets_47(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_47() {
}

void __peek_sockets_47() {
}

 
void init_GetAvg__181_92__47();
inline void check_status__47();

void work_GetAvg__181_92__47(int);


inline float __pop_2_47() {
float res=BUFFER_2_47[TAIL_2_47];
TAIL_2_47++;
return res;
}

inline void __pop_2_47(int n) {
TAIL_2_47+=n;
}

inline float __peek_2_47(int offs) {
return BUFFER_2_47[TAIL_2_47+offs];
}



inline void __push_47_48(float data) {
BUFFER_47_48[HEAD_47_48]=data;
HEAD_47_48++;
}



 
void init_GetAvg__181_92__47(){
}
void save_file_pointer__47(object_write_buffer *buf) {}
void load_file_pointer__47(object_write_buffer *buf) {}
 
void work_GetAvg__181_92__47(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__177 = 0;/* int */
      float Sum__178 = 0.0f;/* float */
      float val__179 = 0.0f;/* float */
      float __tmp8__180 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__178 = ((float)0.0))/*float*/;
      for ((i__177 = 0)/*int*/; (i__177 < 256); (i__177++)) {{
          val__179 = BUFFER_2_47[TAIL_2_47]; TAIL_2_47++;
;
          (Sum__178 = (Sum__178 + val__179))/*float*/;
          __push_47_48(val__179);
        }
      }
      (__tmp8__180 = (Sum__178 / ((float)256.0)))/*float*/;
      __push_47_48(__tmp8__180);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_48;
int __counter_48 = 0;
int __steady_48 = 0;
int __tmp_48 = 0;
int __tmp2_48 = 0;
int *__state_flag_48 = NULL;
thread_info *__thread_48 = NULL;



float avg__182__48 = 0.0f;
int i__183__48 = 0;
void save_peek_buffer__48(object_write_buffer *buf);
void load_peek_buffer__48(object_write_buffer *buf);
void save_file_pointer__48(object_write_buffer *buf);
void load_file_pointer__48(object_write_buffer *buf);

inline void check_status__48() {
  check_thread_status(__state_flag_48, __thread_48);
}

void check_status_during_io__48() {
  check_thread_status_during_io(__state_flag_48, __thread_48);
}

void __init_thread_info_48(thread_info *info) {
  __state_flag_48 = info->get_state_flag();
}

thread_info *__get_thread_info_48() {
  if (__thread_48 != NULL) return __thread_48;
  __thread_48 = new thread_info(48, check_status_during_io__48);
  __init_thread_info_48(__thread_48);
  return __thread_48;
}

void __declare_sockets_48() {
  init_instance::add_incoming(47,48, DATA_SOCKET);
  init_instance::add_outgoing(48,49, DATA_SOCKET);
}

void __init_sockets_48(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_48() {
}

void __peek_sockets_48() {
}

 
void init_DataMinusAvg__189_94__48();
inline void check_status__48();

void work_DataMinusAvg__189_94__48(int);


inline float __pop_47_48() {
float res=BUFFER_47_48[TAIL_47_48];
TAIL_47_48++;
return res;
}

inline void __pop_47_48(int n) {
TAIL_47_48+=n;
}

inline float __peek_47_48(int offs) {
return BUFFER_47_48[TAIL_47_48+offs];
}



inline void __push_48_49(float data) {
BUFFER_48_49[HEAD_48_49]=data;
HEAD_48_49++;
}



 
void init_DataMinusAvg__189_94__48(){
}
void save_file_pointer__48(object_write_buffer *buf) {}
void load_file_pointer__48(object_write_buffer *buf) {}
 
void work_DataMinusAvg__189_94__48(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__186 = 0.0f;/* float */
      float v__187 = 0.0f;/* float */
      float __tmp11__188 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__186 = BUFFER_47_48[TAIL_47_48+256];
;
      ((avg__182__48) = __tmp9__186)/*float*/;
      for (((i__183__48) = 0)/*int*/; ((i__183__48) < 256); ((i__183__48)++)) {{
          v__187 = BUFFER_47_48[TAIL_47_48]; TAIL_47_48++;
;
          (__tmp11__188 = (v__187 - (avg__182__48)))/*float*/;
          __push_48_49(__tmp11__188);
        }
      }
      __pop_47_48();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_49;
int __counter_49 = 0;
int __steady_49 = 0;
int __tmp_49 = 0;
int __tmp2_49 = 0;
int *__state_flag_49 = NULL;
thread_info *__thread_49 = NULL;



inline void check_status__49() {
  check_thread_status(__state_flag_49, __thread_49);
}

void check_status_during_io__49() {
  check_thread_status_during_io(__state_flag_49, __thread_49);
}

void __init_thread_info_49(thread_info *info) {
  __state_flag_49 = info->get_state_flag();
}

thread_info *__get_thread_info_49() {
  if (__thread_49 != NULL) return __thread_49;
  __thread_49 = new thread_info(49, check_status_during_io__49);
  __init_thread_info_49(__thread_49);
  return __thread_49;
}

void __declare_sockets_49() {
  init_instance::add_incoming(48,49, DATA_SOCKET);
  init_instance::add_outgoing(49,50, DATA_SOCKET);
  init_instance::add_outgoing(49,52, DATA_SOCKET);
}

void __init_sockets_49(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_49() {
}

void __peek_sockets_49() {
}

inline float __pop_48_49() {
float res=BUFFER_48_49[TAIL_48_49];
TAIL_48_49++;
return res;
}


inline void __push_49_50(float data) {
BUFFER_49_50[HEAD_49_50]=data;
HEAD_49_50++;
}



inline void __push_49_52(float data) {
BUFFER_49_52[HEAD_49_52]=data;
HEAD_49_52++;
}



void __splitter_49_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_48_49[TAIL_48_49]; TAIL_48_49++;
;
  __push_49_50(tmp);
  __push_49_52(tmp);
  }
}


// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_5;
int __counter_5 = 0;
int __steady_5 = 0;
int __tmp_5 = 0;
int __tmp2_5 = 0;
int *__state_flag_5 = NULL;
thread_info *__thread_5 = NULL;



inline void check_status__5() {
  check_thread_status(__state_flag_5, __thread_5);
}

void check_status_during_io__5() {
  check_thread_status_during_io(__state_flag_5, __thread_5);
}

void __init_thread_info_5(thread_info *info) {
  __state_flag_5 = info->get_state_flag();
}

thread_info *__get_thread_info_5() {
  if (__thread_5 != NULL) return __thread_5;
  __thread_5 = new thread_info(5, check_status_during_io__5);
  __init_thread_info_5(__thread_5);
  return __thread_5;
}

void __declare_sockets_5() {
  init_instance::add_incoming(4,5, DATA_SOCKET);
  init_instance::add_outgoing(5,6, DATA_SOCKET);
  init_instance::add_outgoing(5,28, DATA_SOCKET);
}

void __init_sockets_5(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_5() {
}

void __peek_sockets_5() {
}

inline float __pop_4_5() {
float res=BUFFER_4_5[TAIL_4_5];
TAIL_4_5++;
return res;
}


inline void __push_5_6(float data) {
BUFFER_5_6[HEAD_5_6]=data;
HEAD_5_6++;
}



inline void __push_5_28(float data) {
BUFFER_5_28[HEAD_5_28]=data;
HEAD_5_28++;
}



void __splitter_5_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_4_5[TAIL_4_5]; TAIL_4_5++;
;
  __push_5_6(tmp);
  __push_5_28(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_50;
int __counter_50 = 0;
int __steady_50 = 0;
int __tmp_50 = 0;
int __tmp2_50 = 0;
int *__state_flag_50 = NULL;
thread_info *__thread_50 = NULL;



float data__190__50 = 0.0f;
int i__191__50 = 0;
void save_peek_buffer__50(object_write_buffer *buf);
void load_peek_buffer__50(object_write_buffer *buf);
void save_file_pointer__50(object_write_buffer *buf);
void load_file_pointer__50(object_write_buffer *buf);

inline void check_status__50() {
  check_thread_status(__state_flag_50, __thread_50);
}

void check_status_during_io__50() {
  check_thread_status_during_io(__state_flag_50, __thread_50);
}

void __init_thread_info_50(thread_info *info) {
  __state_flag_50 = info->get_state_flag();
}

thread_info *__get_thread_info_50() {
  if (__thread_50 != NULL) return __thread_50;
  __thread_50 = new thread_info(50, check_status_during_io__50);
  __init_thread_info_50(__thread_50);
  return __thread_50;
}

void __declare_sockets_50() {
  init_instance::add_incoming(49,50, DATA_SOCKET);
  init_instance::add_outgoing(50,51, DATA_SOCKET);
}

void __init_sockets_50(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_50() {
}

void __peek_sockets_50() {
}

 
void init_DenomDataSum__195_96__50();
inline void check_status__50();

void work_DenomDataSum__195_96__50(int);


inline float __pop_49_50() {
float res=BUFFER_49_50[TAIL_49_50];
TAIL_49_50++;
return res;
}

inline void __pop_49_50(int n) {
TAIL_49_50+=n;
}

inline float __peek_49_50(int offs) {
return BUFFER_49_50[TAIL_49_50+offs];
}



inline void __push_50_51(float data) {
BUFFER_50_51[HEAD_50_51]=data;
HEAD_50_51++;
}



 
void init_DenomDataSum__195_96__50(){
}
void save_file_pointer__50(object_write_buffer *buf) {}
void load_file_pointer__50(object_write_buffer *buf) {}
 
void work_DenomDataSum__195_96__50(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__194 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__194 = ((float)0.0))/*float*/;
      for (((i__191__50) = 0)/*int*/; ((i__191__50) < 256); ((i__191__50)++)) {{
          (data__190__50) = BUFFER_49_50[TAIL_49_50]; TAIL_49_50++;
;
          (sum__194 = (sum__194 + ((data__190__50) * (data__190__50))))/*float*/;
        }
      }
      __push_50_51(sum__194);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_51;
int __counter_51 = 0;
int __steady_51 = 0;
int __tmp_51 = 0;
int __tmp2_51 = 0;
int *__state_flag_51 = NULL;
thread_info *__thread_51 = NULL;



inline void check_status__51() {
  check_thread_status(__state_flag_51, __thread_51);
}

void check_status_during_io__51() {
  check_thread_status_during_io(__state_flag_51, __thread_51);
}

void __init_thread_info_51(thread_info *info) {
  __state_flag_51 = info->get_state_flag();
}

thread_info *__get_thread_info_51() {
  if (__thread_51 != NULL) return __thread_51;
  __thread_51 = new thread_info(51, check_status_during_io__51);
  __init_thread_info_51(__thread_51);
  return __thread_51;
}

void __declare_sockets_51() {
  init_instance::add_incoming(50,51, DATA_SOCKET);
  init_instance::add_incoming(52,51, DATA_SOCKET);
  init_instance::add_outgoing(51,8, DATA_SOCKET);
}

void __init_sockets_51(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_51() {
}

void __peek_sockets_51() {
}


inline void __push_51_8(float data) {
BUFFER_51_8[HEAD_51_8]=data;
HEAD_51_8++;
}


inline float __pop_50_51() {
float res=BUFFER_50_51[TAIL_50_51];
TAIL_50_51++;
return res;
}

inline float __pop_52_51() {
float res=BUFFER_52_51[TAIL_52_51];
TAIL_52_51++;
return res;
}


void __joiner_51_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_51_8(__pop_50_51());
    __push_51_8(__pop_52_51());
    __push_51_8(__pop_52_51());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_52;
int __counter_52 = 0;
int __steady_52 = 0;
int __tmp_52 = 0;
int __tmp2_52 = 0;
int *__state_flag_52 = NULL;
thread_info *__thread_52 = NULL;



float _TheGlobal__pattern__6____52[256] = {0};
float numerSum__197__52 = 0.0f;
float denomPatternSum__198__52 = 0.0f;
float data__199__52 = 0.0f;
float patternData__200__52 = 0.0f;
int i__201__52 = 0;
void save_peek_buffer__52(object_write_buffer *buf);
void load_peek_buffer__52(object_write_buffer *buf);
void save_file_pointer__52(object_write_buffer *buf);
void load_file_pointer__52(object_write_buffer *buf);

inline void check_status__52() {
  check_thread_status(__state_flag_52, __thread_52);
}

void check_status_during_io__52() {
  check_thread_status_during_io(__state_flag_52, __thread_52);
}

void __init_thread_info_52(thread_info *info) {
  __state_flag_52 = info->get_state_flag();
}

thread_info *__get_thread_info_52() {
  if (__thread_52 != NULL) return __thread_52;
  __thread_52 = new thread_info(52, check_status_during_io__52);
  __init_thread_info_52(__thread_52);
  return __thread_52;
}

void __declare_sockets_52() {
  init_instance::add_incoming(49,52, DATA_SOCKET);
  init_instance::add_outgoing(52,51, DATA_SOCKET);
}

void __init_sockets_52(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_52() {
}

void __peek_sockets_52() {
}

 
void init_PatternSums__204_97__52();
inline void check_status__52();

void work_PatternSums__204_97__52(int);


inline float __pop_49_52() {
float res=BUFFER_49_52[TAIL_49_52];
TAIL_49_52++;
return res;
}

inline void __pop_49_52(int n) {
TAIL_49_52+=n;
}

inline float __peek_49_52(int offs) {
return BUFFER_49_52[TAIL_49_52+offs];
}



inline void __push_52_51(float data) {
BUFFER_52_51[HEAD_52_51]=data;
HEAD_52_51++;
}



 
void init_PatternSums__204_97__52(){
  (((_TheGlobal__pattern__6____52)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____52)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__52(object_write_buffer *buf) {}
void load_file_pointer__52(object_write_buffer *buf) {}
 
void work_PatternSums__204_97__52(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__197__52) = ((float)0.0))/*float*/;
      ((denomPatternSum__198__52) = ((float)0.0))/*float*/;
      for (((i__201__52) = 0)/*int*/; ((i__201__52) < 256); ((i__201__52)++)) {{
          ((patternData__200__52) = (((_TheGlobal__pattern__6____52)[(int)(i__201__52)]) - ((float)1.6013207E-4)))/*float*/;
          (data__199__52) = BUFFER_49_52[TAIL_49_52]; TAIL_49_52++;
;
          ((numerSum__197__52) = ((numerSum__197__52) + ((data__199__52) * (patternData__200__52))))/*float*/;
          ((denomPatternSum__198__52) = ((denomPatternSum__198__52) + ((patternData__200__52) * (patternData__200__52))))/*float*/;
        }
      }
      __push_52_51((denomPatternSum__198__52));
      __push_52_51((numerSum__197__52));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_53;
int __counter_53 = 0;
int __steady_53 = 0;
int __tmp_53 = 0;
int __tmp2_53 = 0;
int *__state_flag_53 = NULL;
thread_info *__thread_53 = NULL;



void save_peek_buffer__53(object_write_buffer *buf);
void load_peek_buffer__53(object_write_buffer *buf);
void save_file_pointer__53(object_write_buffer *buf);
void load_file_pointer__53(object_write_buffer *buf);

inline void check_status__53() {
  check_thread_status(__state_flag_53, __thread_53);
}

void check_status_during_io__53() {
  check_thread_status_during_io(__state_flag_53, __thread_53);
}

void __init_thread_info_53(thread_info *info) {
  __state_flag_53 = info->get_state_flag();
}

thread_info *__get_thread_info_53() {
  if (__thread_53 != NULL) return __thread_53;
  __thread_53 = new thread_info(53, check_status_during_io__53);
  __init_thread_info_53(__thread_53);
  return __thread_53;
}

void __declare_sockets_53() {
  init_instance::add_incoming(2,53, DATA_SOCKET);
  init_instance::add_outgoing(53,54, DATA_SOCKET);
}

void __init_sockets_53(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_53() {
}

void __peek_sockets_53() {
}

 
void init_GetAvg__222_100__53();
inline void check_status__53();

void work_GetAvg__222_100__53(int);


inline float __pop_2_53() {
float res=BUFFER_2_53[TAIL_2_53];
TAIL_2_53++;
return res;
}

inline void __pop_2_53(int n) {
TAIL_2_53+=n;
}

inline float __peek_2_53(int offs) {
return BUFFER_2_53[TAIL_2_53+offs];
}



inline void __push_53_54(float data) {
BUFFER_53_54[HEAD_53_54]=data;
HEAD_53_54++;
}



 
void init_GetAvg__222_100__53(){
}
void save_file_pointer__53(object_write_buffer *buf) {}
void load_file_pointer__53(object_write_buffer *buf) {}
 
void work_GetAvg__222_100__53(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__218 = 0;/* int */
      float Sum__219 = 0.0f;/* float */
      float val__220 = 0.0f;/* float */
      float __tmp8__221 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__219 = ((float)0.0))/*float*/;
      for ((i__218 = 0)/*int*/; (i__218 < 256); (i__218++)) {{
          val__220 = BUFFER_2_53[TAIL_2_53]; TAIL_2_53++;
;
          (Sum__219 = (Sum__219 + val__220))/*float*/;
          __push_53_54(val__220);
        }
      }
      (__tmp8__221 = (Sum__219 / ((float)256.0)))/*float*/;
      __push_53_54(__tmp8__221);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_54;
int __counter_54 = 0;
int __steady_54 = 0;
int __tmp_54 = 0;
int __tmp2_54 = 0;
int *__state_flag_54 = NULL;
thread_info *__thread_54 = NULL;



float avg__223__54 = 0.0f;
int i__224__54 = 0;
void save_peek_buffer__54(object_write_buffer *buf);
void load_peek_buffer__54(object_write_buffer *buf);
void save_file_pointer__54(object_write_buffer *buf);
void load_file_pointer__54(object_write_buffer *buf);

inline void check_status__54() {
  check_thread_status(__state_flag_54, __thread_54);
}

void check_status_during_io__54() {
  check_thread_status_during_io(__state_flag_54, __thread_54);
}

void __init_thread_info_54(thread_info *info) {
  __state_flag_54 = info->get_state_flag();
}

thread_info *__get_thread_info_54() {
  if (__thread_54 != NULL) return __thread_54;
  __thread_54 = new thread_info(54, check_status_during_io__54);
  __init_thread_info_54(__thread_54);
  return __thread_54;
}

void __declare_sockets_54() {
  init_instance::add_incoming(53,54, DATA_SOCKET);
  init_instance::add_outgoing(54,55, DATA_SOCKET);
}

void __init_sockets_54(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_54() {
}

void __peek_sockets_54() {
}

 
void init_DataMinusAvg__230_102__54();
inline void check_status__54();

void work_DataMinusAvg__230_102__54(int);


inline float __pop_53_54() {
float res=BUFFER_53_54[TAIL_53_54];
TAIL_53_54++;
return res;
}

inline void __pop_53_54(int n) {
TAIL_53_54+=n;
}

inline float __peek_53_54(int offs) {
return BUFFER_53_54[TAIL_53_54+offs];
}



inline void __push_54_55(float data) {
BUFFER_54_55[HEAD_54_55]=data;
HEAD_54_55++;
}



 
void init_DataMinusAvg__230_102__54(){
}
void save_file_pointer__54(object_write_buffer *buf) {}
void load_file_pointer__54(object_write_buffer *buf) {}
 
void work_DataMinusAvg__230_102__54(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__227 = 0.0f;/* float */
      float v__228 = 0.0f;/* float */
      float __tmp11__229 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__227 = BUFFER_53_54[TAIL_53_54+256];
;
      ((avg__223__54) = __tmp9__227)/*float*/;
      for (((i__224__54) = 0)/*int*/; ((i__224__54) < 256); ((i__224__54)++)) {{
          v__228 = BUFFER_53_54[TAIL_53_54]; TAIL_53_54++;
;
          (__tmp11__229 = (v__228 - (avg__223__54)))/*float*/;
          __push_54_55(__tmp11__229);
        }
      }
      __pop_53_54();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_55;
int __counter_55 = 0;
int __steady_55 = 0;
int __tmp_55 = 0;
int __tmp2_55 = 0;
int *__state_flag_55 = NULL;
thread_info *__thread_55 = NULL;



inline void check_status__55() {
  check_thread_status(__state_flag_55, __thread_55);
}

void check_status_during_io__55() {
  check_thread_status_during_io(__state_flag_55, __thread_55);
}

void __init_thread_info_55(thread_info *info) {
  __state_flag_55 = info->get_state_flag();
}

thread_info *__get_thread_info_55() {
  if (__thread_55 != NULL) return __thread_55;
  __thread_55 = new thread_info(55, check_status_during_io__55);
  __init_thread_info_55(__thread_55);
  return __thread_55;
}

void __declare_sockets_55() {
  init_instance::add_incoming(54,55, DATA_SOCKET);
  init_instance::add_outgoing(55,56, DATA_SOCKET);
  init_instance::add_outgoing(55,58, DATA_SOCKET);
}

void __init_sockets_55(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_55() {
}

void __peek_sockets_55() {
}

inline float __pop_54_55() {
float res=BUFFER_54_55[TAIL_54_55];
TAIL_54_55++;
return res;
}


inline void __push_55_56(float data) {
BUFFER_55_56[HEAD_55_56]=data;
HEAD_55_56++;
}



inline void __push_55_58(float data) {
BUFFER_55_58[HEAD_55_58]=data;
HEAD_55_58++;
}



void __splitter_55_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_54_55[TAIL_54_55]; TAIL_54_55++;
;
  __push_55_56(tmp);
  __push_55_58(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_56;
int __counter_56 = 0;
int __steady_56 = 0;
int __tmp_56 = 0;
int __tmp2_56 = 0;
int *__state_flag_56 = NULL;
thread_info *__thread_56 = NULL;



float data__231__56 = 0.0f;
int i__232__56 = 0;
void save_peek_buffer__56(object_write_buffer *buf);
void load_peek_buffer__56(object_write_buffer *buf);
void save_file_pointer__56(object_write_buffer *buf);
void load_file_pointer__56(object_write_buffer *buf);

inline void check_status__56() {
  check_thread_status(__state_flag_56, __thread_56);
}

void check_status_during_io__56() {
  check_thread_status_during_io(__state_flag_56, __thread_56);
}

void __init_thread_info_56(thread_info *info) {
  __state_flag_56 = info->get_state_flag();
}

thread_info *__get_thread_info_56() {
  if (__thread_56 != NULL) return __thread_56;
  __thread_56 = new thread_info(56, check_status_during_io__56);
  __init_thread_info_56(__thread_56);
  return __thread_56;
}

void __declare_sockets_56() {
  init_instance::add_incoming(55,56, DATA_SOCKET);
  init_instance::add_outgoing(56,57, DATA_SOCKET);
}

void __init_sockets_56(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_56() {
}

void __peek_sockets_56() {
}

 
void init_DenomDataSum__236_104__56();
inline void check_status__56();

void work_DenomDataSum__236_104__56(int);


inline float __pop_55_56() {
float res=BUFFER_55_56[TAIL_55_56];
TAIL_55_56++;
return res;
}

inline void __pop_55_56(int n) {
TAIL_55_56+=n;
}

inline float __peek_55_56(int offs) {
return BUFFER_55_56[TAIL_55_56+offs];
}



inline void __push_56_57(float data) {
BUFFER_56_57[HEAD_56_57]=data;
HEAD_56_57++;
}



 
void init_DenomDataSum__236_104__56(){
}
void save_file_pointer__56(object_write_buffer *buf) {}
void load_file_pointer__56(object_write_buffer *buf) {}
 
void work_DenomDataSum__236_104__56(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__235 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__235 = ((float)0.0))/*float*/;
      for (((i__232__56) = 0)/*int*/; ((i__232__56) < 256); ((i__232__56)++)) {{
          (data__231__56) = BUFFER_55_56[TAIL_55_56]; TAIL_55_56++;
;
          (sum__235 = (sum__235 + ((data__231__56) * (data__231__56))))/*float*/;
        }
      }
      __push_56_57(sum__235);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_57;
int __counter_57 = 0;
int __steady_57 = 0;
int __tmp_57 = 0;
int __tmp2_57 = 0;
int *__state_flag_57 = NULL;
thread_info *__thread_57 = NULL;



inline void check_status__57() {
  check_thread_status(__state_flag_57, __thread_57);
}

void check_status_during_io__57() {
  check_thread_status_during_io(__state_flag_57, __thread_57);
}

void __init_thread_info_57(thread_info *info) {
  __state_flag_57 = info->get_state_flag();
}

thread_info *__get_thread_info_57() {
  if (__thread_57 != NULL) return __thread_57;
  __thread_57 = new thread_info(57, check_status_during_io__57);
  __init_thread_info_57(__thread_57);
  return __thread_57;
}

void __declare_sockets_57() {
  init_instance::add_incoming(56,57, DATA_SOCKET);
  init_instance::add_incoming(58,57, DATA_SOCKET);
  init_instance::add_outgoing(57,8, DATA_SOCKET);
}

void __init_sockets_57(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_57() {
}

void __peek_sockets_57() {
}


inline void __push_57_8(float data) {
BUFFER_57_8[HEAD_57_8]=data;
HEAD_57_8++;
}


inline float __pop_56_57() {
float res=BUFFER_56_57[TAIL_56_57];
TAIL_56_57++;
return res;
}

inline float __pop_58_57() {
float res=BUFFER_58_57[TAIL_58_57];
TAIL_58_57++;
return res;
}


void __joiner_57_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_57_8(__pop_56_57());
    __push_57_8(__pop_58_57());
    __push_57_8(__pop_58_57());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_58;
int __counter_58 = 0;
int __steady_58 = 0;
int __tmp_58 = 0;
int __tmp2_58 = 0;
int *__state_flag_58 = NULL;
thread_info *__thread_58 = NULL;



float _TheGlobal__pattern__6____58[256] = {0};
float numerSum__238__58 = 0.0f;
float denomPatternSum__239__58 = 0.0f;
float data__240__58 = 0.0f;
float patternData__241__58 = 0.0f;
int i__242__58 = 0;
void save_peek_buffer__58(object_write_buffer *buf);
void load_peek_buffer__58(object_write_buffer *buf);
void save_file_pointer__58(object_write_buffer *buf);
void load_file_pointer__58(object_write_buffer *buf);

inline void check_status__58() {
  check_thread_status(__state_flag_58, __thread_58);
}

void check_status_during_io__58() {
  check_thread_status_during_io(__state_flag_58, __thread_58);
}

void __init_thread_info_58(thread_info *info) {
  __state_flag_58 = info->get_state_flag();
}

thread_info *__get_thread_info_58() {
  if (__thread_58 != NULL) return __thread_58;
  __thread_58 = new thread_info(58, check_status_during_io__58);
  __init_thread_info_58(__thread_58);
  return __thread_58;
}

void __declare_sockets_58() {
  init_instance::add_incoming(55,58, DATA_SOCKET);
  init_instance::add_outgoing(58,57, DATA_SOCKET);
}

void __init_sockets_58(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_58() {
}

void __peek_sockets_58() {
}

 
void init_PatternSums__245_105__58();
inline void check_status__58();

void work_PatternSums__245_105__58(int);


inline float __pop_55_58() {
float res=BUFFER_55_58[TAIL_55_58];
TAIL_55_58++;
return res;
}

inline void __pop_55_58(int n) {
TAIL_55_58+=n;
}

inline float __peek_55_58(int offs) {
return BUFFER_55_58[TAIL_55_58+offs];
}



inline void __push_58_57(float data) {
BUFFER_58_57[HEAD_58_57]=data;
HEAD_58_57++;
}



 
void init_PatternSums__245_105__58(){
  (((_TheGlobal__pattern__6____58)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____58)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__58(object_write_buffer *buf) {}
void load_file_pointer__58(object_write_buffer *buf) {}
 
void work_PatternSums__245_105__58(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__238__58) = ((float)0.0))/*float*/;
      ((denomPatternSum__239__58) = ((float)0.0))/*float*/;
      for (((i__242__58) = 0)/*int*/; ((i__242__58) < 256); ((i__242__58)++)) {{
          ((patternData__241__58) = (((_TheGlobal__pattern__6____58)[(int)(i__242__58)]) - ((float)1.6013207E-4)))/*float*/;
          (data__240__58) = BUFFER_55_58[TAIL_55_58]; TAIL_55_58++;
;
          ((numerSum__238__58) = ((numerSum__238__58) + ((data__240__58) * (patternData__241__58))))/*float*/;
          ((denomPatternSum__239__58) = ((denomPatternSum__239__58) + ((patternData__241__58) * (patternData__241__58))))/*float*/;
        }
      }
      __push_58_57((denomPatternSum__239__58));
      __push_58_57((numerSum__238__58));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_59;
int __counter_59 = 0;
int __steady_59 = 0;
int __tmp_59 = 0;
int __tmp2_59 = 0;
int *__state_flag_59 = NULL;
thread_info *__thread_59 = NULL;



void save_peek_buffer__59(object_write_buffer *buf);
void load_peek_buffer__59(object_write_buffer *buf);
void save_file_pointer__59(object_write_buffer *buf);
void load_file_pointer__59(object_write_buffer *buf);

inline void check_status__59() {
  check_thread_status(__state_flag_59, __thread_59);
}

void check_status_during_io__59() {
  check_thread_status_during_io(__state_flag_59, __thread_59);
}

void __init_thread_info_59(thread_info *info) {
  __state_flag_59 = info->get_state_flag();
}

thread_info *__get_thread_info_59() {
  if (__thread_59 != NULL) return __thread_59;
  __thread_59 = new thread_info(59, check_status_during_io__59);
  __init_thread_info_59(__thread_59);
  return __thread_59;
}

void __declare_sockets_59() {
  init_instance::add_incoming(2,59, DATA_SOCKET);
  init_instance::add_outgoing(59,60, DATA_SOCKET);
}

void __init_sockets_59(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_59() {
}

void __peek_sockets_59() {
}

 
void init_GetAvg__263_108__59();
inline void check_status__59();

void work_GetAvg__263_108__59(int);


inline float __pop_2_59() {
float res=BUFFER_2_59[TAIL_2_59];
TAIL_2_59++;
return res;
}

inline void __pop_2_59(int n) {
TAIL_2_59+=n;
}

inline float __peek_2_59(int offs) {
return BUFFER_2_59[TAIL_2_59+offs];
}



inline void __push_59_60(float data) {
BUFFER_59_60[HEAD_59_60]=data;
HEAD_59_60++;
}



 
void init_GetAvg__263_108__59(){
}
void save_file_pointer__59(object_write_buffer *buf) {}
void load_file_pointer__59(object_write_buffer *buf) {}
 
void work_GetAvg__263_108__59(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__259 = 0;/* int */
      float Sum__260 = 0.0f;/* float */
      float val__261 = 0.0f;/* float */
      float __tmp8__262 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__260 = ((float)0.0))/*float*/;
      for ((i__259 = 0)/*int*/; (i__259 < 256); (i__259++)) {{
          val__261 = BUFFER_2_59[TAIL_2_59]; TAIL_2_59++;
;
          (Sum__260 = (Sum__260 + val__261))/*float*/;
          __push_59_60(val__261);
        }
      }
      (__tmp8__262 = (Sum__260 / ((float)256.0)))/*float*/;
      __push_59_60(__tmp8__262);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_6;
int __counter_6 = 0;
int __steady_6 = 0;
int __tmp_6 = 0;
int __tmp2_6 = 0;
int *__state_flag_6 = NULL;
thread_info *__thread_6 = NULL;



float data__26__6 = 0.0f;
int i__27__6 = 0;
void save_peek_buffer__6(object_write_buffer *buf);
void load_peek_buffer__6(object_write_buffer *buf);
void save_file_pointer__6(object_write_buffer *buf);
void load_file_pointer__6(object_write_buffer *buf);

inline void check_status__6() {
  check_thread_status(__state_flag_6, __thread_6);
}

void check_status_during_io__6() {
  check_thread_status_during_io(__state_flag_6, __thread_6);
}

void __init_thread_info_6(thread_info *info) {
  __state_flag_6 = info->get_state_flag();
}

thread_info *__get_thread_info_6() {
  if (__thread_6 != NULL) return __thread_6;
  __thread_6 = new thread_info(6, check_status_during_io__6);
  __init_thread_info_6(__thread_6);
  return __thread_6;
}

void __declare_sockets_6() {
  init_instance::add_incoming(5,6, DATA_SOCKET);
  init_instance::add_outgoing(6,7, DATA_SOCKET);
}

void __init_sockets_6(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_6() {
}

void __peek_sockets_6() {
}

 
void init_DenomDataSum__31_64__6();
inline void check_status__6();

void work_DenomDataSum__31_64__6(int);


inline float __pop_5_6() {
float res=BUFFER_5_6[TAIL_5_6];
TAIL_5_6++;
return res;
}

inline void __pop_5_6(int n) {
TAIL_5_6+=n;
}

inline float __peek_5_6(int offs) {
return BUFFER_5_6[TAIL_5_6+offs];
}



inline void __push_6_7(float data) {
BUFFER_6_7[HEAD_6_7]=data;
HEAD_6_7++;
}



 
void init_DenomDataSum__31_64__6(){
}
void save_file_pointer__6(object_write_buffer *buf) {}
void load_file_pointer__6(object_write_buffer *buf) {}
 
void work_DenomDataSum__31_64__6(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__30 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__30 = ((float)0.0))/*float*/;
      for (((i__27__6) = 0)/*int*/; ((i__27__6) < 256); ((i__27__6)++)) {{
          (data__26__6) = BUFFER_5_6[TAIL_5_6]; TAIL_5_6++;
;
          (sum__30 = (sum__30 + ((data__26__6) * (data__26__6))))/*float*/;
        }
      }
      __push_6_7(sum__30);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_60;
int __counter_60 = 0;
int __steady_60 = 0;
int __tmp_60 = 0;
int __tmp2_60 = 0;
int *__state_flag_60 = NULL;
thread_info *__thread_60 = NULL;



float avg__264__60 = 0.0f;
int i__265__60 = 0;
void save_peek_buffer__60(object_write_buffer *buf);
void load_peek_buffer__60(object_write_buffer *buf);
void save_file_pointer__60(object_write_buffer *buf);
void load_file_pointer__60(object_write_buffer *buf);

inline void check_status__60() {
  check_thread_status(__state_flag_60, __thread_60);
}

void check_status_during_io__60() {
  check_thread_status_during_io(__state_flag_60, __thread_60);
}

void __init_thread_info_60(thread_info *info) {
  __state_flag_60 = info->get_state_flag();
}

thread_info *__get_thread_info_60() {
  if (__thread_60 != NULL) return __thread_60;
  __thread_60 = new thread_info(60, check_status_during_io__60);
  __init_thread_info_60(__thread_60);
  return __thread_60;
}

void __declare_sockets_60() {
  init_instance::add_incoming(59,60, DATA_SOCKET);
  init_instance::add_outgoing(60,61, DATA_SOCKET);
}

void __init_sockets_60(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_60() {
}

void __peek_sockets_60() {
}

 
void init_DataMinusAvg__271_110__60();
inline void check_status__60();

void work_DataMinusAvg__271_110__60(int);


inline float __pop_59_60() {
float res=BUFFER_59_60[TAIL_59_60];
TAIL_59_60++;
return res;
}

inline void __pop_59_60(int n) {
TAIL_59_60+=n;
}

inline float __peek_59_60(int offs) {
return BUFFER_59_60[TAIL_59_60+offs];
}



inline void __push_60_61(float data) {
BUFFER_60_61[HEAD_60_61]=data;
HEAD_60_61++;
}



 
void init_DataMinusAvg__271_110__60(){
}
void save_file_pointer__60(object_write_buffer *buf) {}
void load_file_pointer__60(object_write_buffer *buf) {}
 
void work_DataMinusAvg__271_110__60(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__268 = 0.0f;/* float */
      float v__269 = 0.0f;/* float */
      float __tmp11__270 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__268 = BUFFER_59_60[TAIL_59_60+256];
;
      ((avg__264__60) = __tmp9__268)/*float*/;
      for (((i__265__60) = 0)/*int*/; ((i__265__60) < 256); ((i__265__60)++)) {{
          v__269 = BUFFER_59_60[TAIL_59_60]; TAIL_59_60++;
;
          (__tmp11__270 = (v__269 - (avg__264__60)))/*float*/;
          __push_60_61(__tmp11__270);
        }
      }
      __pop_59_60();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_61;
int __counter_61 = 0;
int __steady_61 = 0;
int __tmp_61 = 0;
int __tmp2_61 = 0;
int *__state_flag_61 = NULL;
thread_info *__thread_61 = NULL;



inline void check_status__61() {
  check_thread_status(__state_flag_61, __thread_61);
}

void check_status_during_io__61() {
  check_thread_status_during_io(__state_flag_61, __thread_61);
}

void __init_thread_info_61(thread_info *info) {
  __state_flag_61 = info->get_state_flag();
}

thread_info *__get_thread_info_61() {
  if (__thread_61 != NULL) return __thread_61;
  __thread_61 = new thread_info(61, check_status_during_io__61);
  __init_thread_info_61(__thread_61);
  return __thread_61;
}

void __declare_sockets_61() {
  init_instance::add_incoming(60,61, DATA_SOCKET);
  init_instance::add_outgoing(61,62, DATA_SOCKET);
  init_instance::add_outgoing(61,64, DATA_SOCKET);
}

void __init_sockets_61(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_61() {
}

void __peek_sockets_61() {
}

inline float __pop_60_61() {
float res=BUFFER_60_61[TAIL_60_61];
TAIL_60_61++;
return res;
}


inline void __push_61_62(float data) {
BUFFER_61_62[HEAD_61_62]=data;
HEAD_61_62++;
}



inline void __push_61_64(float data) {
BUFFER_61_64[HEAD_61_64]=data;
HEAD_61_64++;
}



void __splitter_61_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_60_61[TAIL_60_61]; TAIL_60_61++;
;
  __push_61_62(tmp);
  __push_61_64(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_62;
int __counter_62 = 0;
int __steady_62 = 0;
int __tmp_62 = 0;
int __tmp2_62 = 0;
int *__state_flag_62 = NULL;
thread_info *__thread_62 = NULL;



float data__272__62 = 0.0f;
int i__273__62 = 0;
void save_peek_buffer__62(object_write_buffer *buf);
void load_peek_buffer__62(object_write_buffer *buf);
void save_file_pointer__62(object_write_buffer *buf);
void load_file_pointer__62(object_write_buffer *buf);

inline void check_status__62() {
  check_thread_status(__state_flag_62, __thread_62);
}

void check_status_during_io__62() {
  check_thread_status_during_io(__state_flag_62, __thread_62);
}

void __init_thread_info_62(thread_info *info) {
  __state_flag_62 = info->get_state_flag();
}

thread_info *__get_thread_info_62() {
  if (__thread_62 != NULL) return __thread_62;
  __thread_62 = new thread_info(62, check_status_during_io__62);
  __init_thread_info_62(__thread_62);
  return __thread_62;
}

void __declare_sockets_62() {
  init_instance::add_incoming(61,62, DATA_SOCKET);
  init_instance::add_outgoing(62,63, DATA_SOCKET);
}

void __init_sockets_62(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_62() {
}

void __peek_sockets_62() {
}

 
void init_DenomDataSum__277_112__62();
inline void check_status__62();

void work_DenomDataSum__277_112__62(int);


inline float __pop_61_62() {
float res=BUFFER_61_62[TAIL_61_62];
TAIL_61_62++;
return res;
}

inline void __pop_61_62(int n) {
TAIL_61_62+=n;
}

inline float __peek_61_62(int offs) {
return BUFFER_61_62[TAIL_61_62+offs];
}



inline void __push_62_63(float data) {
BUFFER_62_63[HEAD_62_63]=data;
HEAD_62_63++;
}



 
void init_DenomDataSum__277_112__62(){
}
void save_file_pointer__62(object_write_buffer *buf) {}
void load_file_pointer__62(object_write_buffer *buf) {}
 
void work_DenomDataSum__277_112__62(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__276 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__276 = ((float)0.0))/*float*/;
      for (((i__273__62) = 0)/*int*/; ((i__273__62) < 256); ((i__273__62)++)) {{
          (data__272__62) = BUFFER_61_62[TAIL_61_62]; TAIL_61_62++;
;
          (sum__276 = (sum__276 + ((data__272__62) * (data__272__62))))/*float*/;
        }
      }
      __push_62_63(sum__276);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_63;
int __counter_63 = 0;
int __steady_63 = 0;
int __tmp_63 = 0;
int __tmp2_63 = 0;
int *__state_flag_63 = NULL;
thread_info *__thread_63 = NULL;



inline void check_status__63() {
  check_thread_status(__state_flag_63, __thread_63);
}

void check_status_during_io__63() {
  check_thread_status_during_io(__state_flag_63, __thread_63);
}

void __init_thread_info_63(thread_info *info) {
  __state_flag_63 = info->get_state_flag();
}

thread_info *__get_thread_info_63() {
  if (__thread_63 != NULL) return __thread_63;
  __thread_63 = new thread_info(63, check_status_during_io__63);
  __init_thread_info_63(__thread_63);
  return __thread_63;
}

void __declare_sockets_63() {
  init_instance::add_incoming(62,63, DATA_SOCKET);
  init_instance::add_incoming(64,63, DATA_SOCKET);
  init_instance::add_outgoing(63,8, DATA_SOCKET);
}

void __init_sockets_63(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_63() {
}

void __peek_sockets_63() {
}


inline void __push_63_8(float data) {
BUFFER_63_8[HEAD_63_8]=data;
HEAD_63_8++;
}


inline float __pop_62_63() {
float res=BUFFER_62_63[TAIL_62_63];
TAIL_62_63++;
return res;
}

inline float __pop_64_63() {
float res=BUFFER_64_63[TAIL_64_63];
TAIL_64_63++;
return res;
}


void __joiner_63_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_63_8(__pop_62_63());
    __push_63_8(__pop_64_63());
    __push_63_8(__pop_64_63());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_64;
int __counter_64 = 0;
int __steady_64 = 0;
int __tmp_64 = 0;
int __tmp2_64 = 0;
int *__state_flag_64 = NULL;
thread_info *__thread_64 = NULL;



float _TheGlobal__pattern__6____64[256] = {0};
float numerSum__279__64 = 0.0f;
float denomPatternSum__280__64 = 0.0f;
float data__281__64 = 0.0f;
float patternData__282__64 = 0.0f;
int i__283__64 = 0;
void save_peek_buffer__64(object_write_buffer *buf);
void load_peek_buffer__64(object_write_buffer *buf);
void save_file_pointer__64(object_write_buffer *buf);
void load_file_pointer__64(object_write_buffer *buf);

inline void check_status__64() {
  check_thread_status(__state_flag_64, __thread_64);
}

void check_status_during_io__64() {
  check_thread_status_during_io(__state_flag_64, __thread_64);
}

void __init_thread_info_64(thread_info *info) {
  __state_flag_64 = info->get_state_flag();
}

thread_info *__get_thread_info_64() {
  if (__thread_64 != NULL) return __thread_64;
  __thread_64 = new thread_info(64, check_status_during_io__64);
  __init_thread_info_64(__thread_64);
  return __thread_64;
}

void __declare_sockets_64() {
  init_instance::add_incoming(61,64, DATA_SOCKET);
  init_instance::add_outgoing(64,63, DATA_SOCKET);
}

void __init_sockets_64(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_64() {
}

void __peek_sockets_64() {
}

 
void init_PatternSums__286_113__64();
inline void check_status__64();

void work_PatternSums__286_113__64(int);


inline float __pop_61_64() {
float res=BUFFER_61_64[TAIL_61_64];
TAIL_61_64++;
return res;
}

inline void __pop_61_64(int n) {
TAIL_61_64+=n;
}

inline float __peek_61_64(int offs) {
return BUFFER_61_64[TAIL_61_64+offs];
}



inline void __push_64_63(float data) {
BUFFER_64_63[HEAD_64_63]=data;
HEAD_64_63++;
}



 
void init_PatternSums__286_113__64(){
  (((_TheGlobal__pattern__6____64)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____64)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__64(object_write_buffer *buf) {}
void load_file_pointer__64(object_write_buffer *buf) {}
 
void work_PatternSums__286_113__64(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__279__64) = ((float)0.0))/*float*/;
      ((denomPatternSum__280__64) = ((float)0.0))/*float*/;
      for (((i__283__64) = 0)/*int*/; ((i__283__64) < 256); ((i__283__64)++)) {{
          ((patternData__282__64) = (((_TheGlobal__pattern__6____64)[(int)(i__283__64)]) - ((float)1.6013207E-4)))/*float*/;
          (data__281__64) = BUFFER_61_64[TAIL_61_64]; TAIL_61_64++;
;
          ((numerSum__279__64) = ((numerSum__279__64) + ((data__281__64) * (patternData__282__64))))/*float*/;
          ((denomPatternSum__280__64) = ((denomPatternSum__280__64) + ((patternData__282__64) * (patternData__282__64))))/*float*/;
        }
      }
      __push_64_63((denomPatternSum__280__64));
      __push_64_63((numerSum__279__64));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_65;
int __counter_65 = 0;
int __steady_65 = 0;
int __tmp_65 = 0;
int __tmp2_65 = 0;
int *__state_flag_65 = NULL;
thread_info *__thread_65 = NULL;



void save_peek_buffer__65(object_write_buffer *buf);
void load_peek_buffer__65(object_write_buffer *buf);
void save_file_pointer__65(object_write_buffer *buf);
void load_file_pointer__65(object_write_buffer *buf);

inline void check_status__65() {
  check_thread_status(__state_flag_65, __thread_65);
}

void check_status_during_io__65() {
  check_thread_status_during_io(__state_flag_65, __thread_65);
}

void __init_thread_info_65(thread_info *info) {
  __state_flag_65 = info->get_state_flag();
}

thread_info *__get_thread_info_65() {
  if (__thread_65 != NULL) return __thread_65;
  __thread_65 = new thread_info(65, check_status_during_io__65);
  __init_thread_info_65(__thread_65);
  return __thread_65;
}

void __declare_sockets_65() {
  init_instance::add_incoming(2,65, DATA_SOCKET);
  init_instance::add_outgoing(65,66, DATA_SOCKET);
}

void __init_sockets_65(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_65() {
}

void __peek_sockets_65() {
}

 
void init_GetAvg__304_116__65();
inline void check_status__65();

void work_GetAvg__304_116__65(int);


inline float __pop_2_65() {
float res=BUFFER_2_65[TAIL_2_65];
TAIL_2_65++;
return res;
}

inline void __pop_2_65(int n) {
TAIL_2_65+=n;
}

inline float __peek_2_65(int offs) {
return BUFFER_2_65[TAIL_2_65+offs];
}



inline void __push_65_66(float data) {
BUFFER_65_66[HEAD_65_66]=data;
HEAD_65_66++;
}



 
void init_GetAvg__304_116__65(){
}
void save_file_pointer__65(object_write_buffer *buf) {}
void load_file_pointer__65(object_write_buffer *buf) {}
 
void work_GetAvg__304_116__65(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__300 = 0;/* int */
      float Sum__301 = 0.0f;/* float */
      float val__302 = 0.0f;/* float */
      float __tmp8__303 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__301 = ((float)0.0))/*float*/;
      for ((i__300 = 0)/*int*/; (i__300 < 256); (i__300++)) {{
          val__302 = BUFFER_2_65[TAIL_2_65]; TAIL_2_65++;
;
          (Sum__301 = (Sum__301 + val__302))/*float*/;
          __push_65_66(val__302);
        }
      }
      (__tmp8__303 = (Sum__301 / ((float)256.0)))/*float*/;
      __push_65_66(__tmp8__303);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_66;
int __counter_66 = 0;
int __steady_66 = 0;
int __tmp_66 = 0;
int __tmp2_66 = 0;
int *__state_flag_66 = NULL;
thread_info *__thread_66 = NULL;



float avg__305__66 = 0.0f;
int i__306__66 = 0;
void save_peek_buffer__66(object_write_buffer *buf);
void load_peek_buffer__66(object_write_buffer *buf);
void save_file_pointer__66(object_write_buffer *buf);
void load_file_pointer__66(object_write_buffer *buf);

inline void check_status__66() {
  check_thread_status(__state_flag_66, __thread_66);
}

void check_status_during_io__66() {
  check_thread_status_during_io(__state_flag_66, __thread_66);
}

void __init_thread_info_66(thread_info *info) {
  __state_flag_66 = info->get_state_flag();
}

thread_info *__get_thread_info_66() {
  if (__thread_66 != NULL) return __thread_66;
  __thread_66 = new thread_info(66, check_status_during_io__66);
  __init_thread_info_66(__thread_66);
  return __thread_66;
}

void __declare_sockets_66() {
  init_instance::add_incoming(65,66, DATA_SOCKET);
  init_instance::add_outgoing(66,67, DATA_SOCKET);
}

void __init_sockets_66(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_66() {
}

void __peek_sockets_66() {
}

 
void init_DataMinusAvg__312_118__66();
inline void check_status__66();

void work_DataMinusAvg__312_118__66(int);


inline float __pop_65_66() {
float res=BUFFER_65_66[TAIL_65_66];
TAIL_65_66++;
return res;
}

inline void __pop_65_66(int n) {
TAIL_65_66+=n;
}

inline float __peek_65_66(int offs) {
return BUFFER_65_66[TAIL_65_66+offs];
}



inline void __push_66_67(float data) {
BUFFER_66_67[HEAD_66_67]=data;
HEAD_66_67++;
}



 
void init_DataMinusAvg__312_118__66(){
}
void save_file_pointer__66(object_write_buffer *buf) {}
void load_file_pointer__66(object_write_buffer *buf) {}
 
void work_DataMinusAvg__312_118__66(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__309 = 0.0f;/* float */
      float v__310 = 0.0f;/* float */
      float __tmp11__311 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__309 = BUFFER_65_66[TAIL_65_66+256];
;
      ((avg__305__66) = __tmp9__309)/*float*/;
      for (((i__306__66) = 0)/*int*/; ((i__306__66) < 256); ((i__306__66)++)) {{
          v__310 = BUFFER_65_66[TAIL_65_66]; TAIL_65_66++;
;
          (__tmp11__311 = (v__310 - (avg__305__66)))/*float*/;
          __push_66_67(__tmp11__311);
        }
      }
      __pop_65_66();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_67;
int __counter_67 = 0;
int __steady_67 = 0;
int __tmp_67 = 0;
int __tmp2_67 = 0;
int *__state_flag_67 = NULL;
thread_info *__thread_67 = NULL;



inline void check_status__67() {
  check_thread_status(__state_flag_67, __thread_67);
}

void check_status_during_io__67() {
  check_thread_status_during_io(__state_flag_67, __thread_67);
}

void __init_thread_info_67(thread_info *info) {
  __state_flag_67 = info->get_state_flag();
}

thread_info *__get_thread_info_67() {
  if (__thread_67 != NULL) return __thread_67;
  __thread_67 = new thread_info(67, check_status_during_io__67);
  __init_thread_info_67(__thread_67);
  return __thread_67;
}

void __declare_sockets_67() {
  init_instance::add_incoming(66,67, DATA_SOCKET);
  init_instance::add_outgoing(67,68, DATA_SOCKET);
  init_instance::add_outgoing(67,70, DATA_SOCKET);
}

void __init_sockets_67(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_67() {
}

void __peek_sockets_67() {
}

inline float __pop_66_67() {
float res=BUFFER_66_67[TAIL_66_67];
TAIL_66_67++;
return res;
}


inline void __push_67_68(float data) {
BUFFER_67_68[HEAD_67_68]=data;
HEAD_67_68++;
}



inline void __push_67_70(float data) {
BUFFER_67_70[HEAD_67_70]=data;
HEAD_67_70++;
}



void __splitter_67_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_66_67[TAIL_66_67]; TAIL_66_67++;
;
  __push_67_68(tmp);
  __push_67_70(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_68;
int __counter_68 = 0;
int __steady_68 = 0;
int __tmp_68 = 0;
int __tmp2_68 = 0;
int *__state_flag_68 = NULL;
thread_info *__thread_68 = NULL;



float data__313__68 = 0.0f;
int i__314__68 = 0;
void save_peek_buffer__68(object_write_buffer *buf);
void load_peek_buffer__68(object_write_buffer *buf);
void save_file_pointer__68(object_write_buffer *buf);
void load_file_pointer__68(object_write_buffer *buf);

inline void check_status__68() {
  check_thread_status(__state_flag_68, __thread_68);
}

void check_status_during_io__68() {
  check_thread_status_during_io(__state_flag_68, __thread_68);
}

void __init_thread_info_68(thread_info *info) {
  __state_flag_68 = info->get_state_flag();
}

thread_info *__get_thread_info_68() {
  if (__thread_68 != NULL) return __thread_68;
  __thread_68 = new thread_info(68, check_status_during_io__68);
  __init_thread_info_68(__thread_68);
  return __thread_68;
}

void __declare_sockets_68() {
  init_instance::add_incoming(67,68, DATA_SOCKET);
  init_instance::add_outgoing(68,69, DATA_SOCKET);
}

void __init_sockets_68(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_68() {
}

void __peek_sockets_68() {
}

 
void init_DenomDataSum__318_120__68();
inline void check_status__68();

void work_DenomDataSum__318_120__68(int);


inline float __pop_67_68() {
float res=BUFFER_67_68[TAIL_67_68];
TAIL_67_68++;
return res;
}

inline void __pop_67_68(int n) {
TAIL_67_68+=n;
}

inline float __peek_67_68(int offs) {
return BUFFER_67_68[TAIL_67_68+offs];
}



inline void __push_68_69(float data) {
BUFFER_68_69[HEAD_68_69]=data;
HEAD_68_69++;
}



 
void init_DenomDataSum__318_120__68(){
}
void save_file_pointer__68(object_write_buffer *buf) {}
void load_file_pointer__68(object_write_buffer *buf) {}
 
void work_DenomDataSum__318_120__68(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__317 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__317 = ((float)0.0))/*float*/;
      for (((i__314__68) = 0)/*int*/; ((i__314__68) < 256); ((i__314__68)++)) {{
          (data__313__68) = BUFFER_67_68[TAIL_67_68]; TAIL_67_68++;
;
          (sum__317 = (sum__317 + ((data__313__68) * (data__313__68))))/*float*/;
        }
      }
      __push_68_69(sum__317);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_69;
int __counter_69 = 0;
int __steady_69 = 0;
int __tmp_69 = 0;
int __tmp2_69 = 0;
int *__state_flag_69 = NULL;
thread_info *__thread_69 = NULL;



inline void check_status__69() {
  check_thread_status(__state_flag_69, __thread_69);
}

void check_status_during_io__69() {
  check_thread_status_during_io(__state_flag_69, __thread_69);
}

void __init_thread_info_69(thread_info *info) {
  __state_flag_69 = info->get_state_flag();
}

thread_info *__get_thread_info_69() {
  if (__thread_69 != NULL) return __thread_69;
  __thread_69 = new thread_info(69, check_status_during_io__69);
  __init_thread_info_69(__thread_69);
  return __thread_69;
}

void __declare_sockets_69() {
  init_instance::add_incoming(68,69, DATA_SOCKET);
  init_instance::add_incoming(70,69, DATA_SOCKET);
  init_instance::add_outgoing(69,8, DATA_SOCKET);
}

void __init_sockets_69(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_69() {
}

void __peek_sockets_69() {
}


inline void __push_69_8(float data) {
BUFFER_69_8[HEAD_69_8]=data;
HEAD_69_8++;
}


inline float __pop_68_69() {
float res=BUFFER_68_69[TAIL_68_69];
TAIL_68_69++;
return res;
}

inline float __pop_70_69() {
float res=BUFFER_70_69[TAIL_70_69];
TAIL_70_69++;
return res;
}


void __joiner_69_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_69_8(__pop_68_69());
    __push_69_8(__pop_70_69());
    __push_69_8(__pop_70_69());
  }
}


// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_7;
int __counter_7 = 0;
int __steady_7 = 0;
int __tmp_7 = 0;
int __tmp2_7 = 0;
int *__state_flag_7 = NULL;
thread_info *__thread_7 = NULL;



inline void check_status__7() {
  check_thread_status(__state_flag_7, __thread_7);
}

void check_status_during_io__7() {
  check_thread_status_during_io(__state_flag_7, __thread_7);
}

void __init_thread_info_7(thread_info *info) {
  __state_flag_7 = info->get_state_flag();
}

thread_info *__get_thread_info_7() {
  if (__thread_7 != NULL) return __thread_7;
  __thread_7 = new thread_info(7, check_status_during_io__7);
  __init_thread_info_7(__thread_7);
  return __thread_7;
}

void __declare_sockets_7() {
  init_instance::add_incoming(6,7, DATA_SOCKET);
  init_instance::add_incoming(28,7, DATA_SOCKET);
  init_instance::add_outgoing(7,8, DATA_SOCKET);
}

void __init_sockets_7(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_7() {
}

void __peek_sockets_7() {
}


inline void __push_7_8(float data) {
BUFFER_7_8[HEAD_7_8]=data;
HEAD_7_8++;
}


inline float __pop_6_7() {
float res=BUFFER_6_7[TAIL_6_7];
TAIL_6_7++;
return res;
}

inline float __pop_28_7() {
float res=BUFFER_28_7[TAIL_28_7];
TAIL_28_7++;
return res;
}


void __joiner_7_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_7_8(__pop_6_7());
    __push_7_8(__pop_28_7());
    __push_7_8(__pop_28_7());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_70;
int __counter_70 = 0;
int __steady_70 = 0;
int __tmp_70 = 0;
int __tmp2_70 = 0;
int *__state_flag_70 = NULL;
thread_info *__thread_70 = NULL;



float _TheGlobal__pattern__6____70[256] = {0};
float numerSum__320__70 = 0.0f;
float denomPatternSum__321__70 = 0.0f;
float data__322__70 = 0.0f;
float patternData__323__70 = 0.0f;
int i__324__70 = 0;
void save_peek_buffer__70(object_write_buffer *buf);
void load_peek_buffer__70(object_write_buffer *buf);
void save_file_pointer__70(object_write_buffer *buf);
void load_file_pointer__70(object_write_buffer *buf);

inline void check_status__70() {
  check_thread_status(__state_flag_70, __thread_70);
}

void check_status_during_io__70() {
  check_thread_status_during_io(__state_flag_70, __thread_70);
}

void __init_thread_info_70(thread_info *info) {
  __state_flag_70 = info->get_state_flag();
}

thread_info *__get_thread_info_70() {
  if (__thread_70 != NULL) return __thread_70;
  __thread_70 = new thread_info(70, check_status_during_io__70);
  __init_thread_info_70(__thread_70);
  return __thread_70;
}

void __declare_sockets_70() {
  init_instance::add_incoming(67,70, DATA_SOCKET);
  init_instance::add_outgoing(70,69, DATA_SOCKET);
}

void __init_sockets_70(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_70() {
}

void __peek_sockets_70() {
}

 
void init_PatternSums__327_121__70();
inline void check_status__70();

void work_PatternSums__327_121__70(int);


inline float __pop_67_70() {
float res=BUFFER_67_70[TAIL_67_70];
TAIL_67_70++;
return res;
}

inline void __pop_67_70(int n) {
TAIL_67_70+=n;
}

inline float __peek_67_70(int offs) {
return BUFFER_67_70[TAIL_67_70+offs];
}



inline void __push_70_69(float data) {
BUFFER_70_69[HEAD_70_69]=data;
HEAD_70_69++;
}



 
void init_PatternSums__327_121__70(){
  (((_TheGlobal__pattern__6____70)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____70)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__70(object_write_buffer *buf) {}
void load_file_pointer__70(object_write_buffer *buf) {}
 
void work_PatternSums__327_121__70(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__320__70) = ((float)0.0))/*float*/;
      ((denomPatternSum__321__70) = ((float)0.0))/*float*/;
      for (((i__324__70) = 0)/*int*/; ((i__324__70) < 256); ((i__324__70)++)) {{
          ((patternData__323__70) = (((_TheGlobal__pattern__6____70)[(int)(i__324__70)]) - ((float)1.6013207E-4)))/*float*/;
          (data__322__70) = BUFFER_67_70[TAIL_67_70]; TAIL_67_70++;
;
          ((numerSum__320__70) = ((numerSum__320__70) + ((data__322__70) * (patternData__323__70))))/*float*/;
          ((denomPatternSum__321__70) = ((denomPatternSum__321__70) + ((patternData__323__70) * (patternData__323__70))))/*float*/;
        }
      }
      __push_70_69((denomPatternSum__321__70));
      __push_70_69((numerSum__320__70));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_71;
int __counter_71 = 0;
int __steady_71 = 0;
int __tmp_71 = 0;
int __tmp2_71 = 0;
int *__state_flag_71 = NULL;
thread_info *__thread_71 = NULL;



void save_peek_buffer__71(object_write_buffer *buf);
void load_peek_buffer__71(object_write_buffer *buf);
void save_file_pointer__71(object_write_buffer *buf);
void load_file_pointer__71(object_write_buffer *buf);

inline void check_status__71() {
  check_thread_status(__state_flag_71, __thread_71);
}

void check_status_during_io__71() {
  check_thread_status_during_io(__state_flag_71, __thread_71);
}

void __init_thread_info_71(thread_info *info) {
  __state_flag_71 = info->get_state_flag();
}

thread_info *__get_thread_info_71() {
  if (__thread_71 != NULL) return __thread_71;
  __thread_71 = new thread_info(71, check_status_during_io__71);
  __init_thread_info_71(__thread_71);
  return __thread_71;
}

void __declare_sockets_71() {
  init_instance::add_incoming(2,71, DATA_SOCKET);
  init_instance::add_outgoing(71,72, DATA_SOCKET);
}

void __init_sockets_71(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_71() {
}

void __peek_sockets_71() {
}

 
void init_GetAvg__345_124__71();
inline void check_status__71();

void work_GetAvg__345_124__71(int);


inline float __pop_2_71() {
float res=BUFFER_2_71[TAIL_2_71];
TAIL_2_71++;
return res;
}

inline void __pop_2_71(int n) {
TAIL_2_71+=n;
}

inline float __peek_2_71(int offs) {
return BUFFER_2_71[TAIL_2_71+offs];
}



inline void __push_71_72(float data) {
BUFFER_71_72[HEAD_71_72]=data;
HEAD_71_72++;
}



 
void init_GetAvg__345_124__71(){
}
void save_file_pointer__71(object_write_buffer *buf) {}
void load_file_pointer__71(object_write_buffer *buf) {}
 
void work_GetAvg__345_124__71(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__341 = 0;/* int */
      float Sum__342 = 0.0f;/* float */
      float val__343 = 0.0f;/* float */
      float __tmp8__344 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__342 = ((float)0.0))/*float*/;
      for ((i__341 = 0)/*int*/; (i__341 < 256); (i__341++)) {{
          val__343 = BUFFER_2_71[TAIL_2_71]; TAIL_2_71++;
;
          (Sum__342 = (Sum__342 + val__343))/*float*/;
          __push_71_72(val__343);
        }
      }
      (__tmp8__344 = (Sum__342 / ((float)256.0)))/*float*/;
      __push_71_72(__tmp8__344);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_72;
int __counter_72 = 0;
int __steady_72 = 0;
int __tmp_72 = 0;
int __tmp2_72 = 0;
int *__state_flag_72 = NULL;
thread_info *__thread_72 = NULL;



float avg__346__72 = 0.0f;
int i__347__72 = 0;
void save_peek_buffer__72(object_write_buffer *buf);
void load_peek_buffer__72(object_write_buffer *buf);
void save_file_pointer__72(object_write_buffer *buf);
void load_file_pointer__72(object_write_buffer *buf);

inline void check_status__72() {
  check_thread_status(__state_flag_72, __thread_72);
}

void check_status_during_io__72() {
  check_thread_status_during_io(__state_flag_72, __thread_72);
}

void __init_thread_info_72(thread_info *info) {
  __state_flag_72 = info->get_state_flag();
}

thread_info *__get_thread_info_72() {
  if (__thread_72 != NULL) return __thread_72;
  __thread_72 = new thread_info(72, check_status_during_io__72);
  __init_thread_info_72(__thread_72);
  return __thread_72;
}

void __declare_sockets_72() {
  init_instance::add_incoming(71,72, DATA_SOCKET);
  init_instance::add_outgoing(72,73, DATA_SOCKET);
}

void __init_sockets_72(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_72() {
}

void __peek_sockets_72() {
}

 
void init_DataMinusAvg__353_126__72();
inline void check_status__72();

void work_DataMinusAvg__353_126__72(int);


inline float __pop_71_72() {
float res=BUFFER_71_72[TAIL_71_72];
TAIL_71_72++;
return res;
}

inline void __pop_71_72(int n) {
TAIL_71_72+=n;
}

inline float __peek_71_72(int offs) {
return BUFFER_71_72[TAIL_71_72+offs];
}



inline void __push_72_73(float data) {
BUFFER_72_73[HEAD_72_73]=data;
HEAD_72_73++;
}



 
void init_DataMinusAvg__353_126__72(){
}
void save_file_pointer__72(object_write_buffer *buf) {}
void load_file_pointer__72(object_write_buffer *buf) {}
 
void work_DataMinusAvg__353_126__72(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__350 = 0.0f;/* float */
      float v__351 = 0.0f;/* float */
      float __tmp11__352 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__350 = BUFFER_71_72[TAIL_71_72+256];
;
      ((avg__346__72) = __tmp9__350)/*float*/;
      for (((i__347__72) = 0)/*int*/; ((i__347__72) < 256); ((i__347__72)++)) {{
          v__351 = BUFFER_71_72[TAIL_71_72]; TAIL_71_72++;
;
          (__tmp11__352 = (v__351 - (avg__346__72)))/*float*/;
          __push_72_73(__tmp11__352);
        }
      }
      __pop_71_72();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_73;
int __counter_73 = 0;
int __steady_73 = 0;
int __tmp_73 = 0;
int __tmp2_73 = 0;
int *__state_flag_73 = NULL;
thread_info *__thread_73 = NULL;



inline void check_status__73() {
  check_thread_status(__state_flag_73, __thread_73);
}

void check_status_during_io__73() {
  check_thread_status_during_io(__state_flag_73, __thread_73);
}

void __init_thread_info_73(thread_info *info) {
  __state_flag_73 = info->get_state_flag();
}

thread_info *__get_thread_info_73() {
  if (__thread_73 != NULL) return __thread_73;
  __thread_73 = new thread_info(73, check_status_during_io__73);
  __init_thread_info_73(__thread_73);
  return __thread_73;
}

void __declare_sockets_73() {
  init_instance::add_incoming(72,73, DATA_SOCKET);
  init_instance::add_outgoing(73,74, DATA_SOCKET);
  init_instance::add_outgoing(73,76, DATA_SOCKET);
}

void __init_sockets_73(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_73() {
}

void __peek_sockets_73() {
}

inline float __pop_72_73() {
float res=BUFFER_72_73[TAIL_72_73];
TAIL_72_73++;
return res;
}


inline void __push_73_74(float data) {
BUFFER_73_74[HEAD_73_74]=data;
HEAD_73_74++;
}



inline void __push_73_76(float data) {
BUFFER_73_76[HEAD_73_76]=data;
HEAD_73_76++;
}



void __splitter_73_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_72_73[TAIL_72_73]; TAIL_72_73++;
;
  __push_73_74(tmp);
  __push_73_76(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_74;
int __counter_74 = 0;
int __steady_74 = 0;
int __tmp_74 = 0;
int __tmp2_74 = 0;
int *__state_flag_74 = NULL;
thread_info *__thread_74 = NULL;



float data__354__74 = 0.0f;
int i__355__74 = 0;
void save_peek_buffer__74(object_write_buffer *buf);
void load_peek_buffer__74(object_write_buffer *buf);
void save_file_pointer__74(object_write_buffer *buf);
void load_file_pointer__74(object_write_buffer *buf);

inline void check_status__74() {
  check_thread_status(__state_flag_74, __thread_74);
}

void check_status_during_io__74() {
  check_thread_status_during_io(__state_flag_74, __thread_74);
}

void __init_thread_info_74(thread_info *info) {
  __state_flag_74 = info->get_state_flag();
}

thread_info *__get_thread_info_74() {
  if (__thread_74 != NULL) return __thread_74;
  __thread_74 = new thread_info(74, check_status_during_io__74);
  __init_thread_info_74(__thread_74);
  return __thread_74;
}

void __declare_sockets_74() {
  init_instance::add_incoming(73,74, DATA_SOCKET);
  init_instance::add_outgoing(74,75, DATA_SOCKET);
}

void __init_sockets_74(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_74() {
}

void __peek_sockets_74() {
}

 
void init_DenomDataSum__359_128__74();
inline void check_status__74();

void work_DenomDataSum__359_128__74(int);


inline float __pop_73_74() {
float res=BUFFER_73_74[TAIL_73_74];
TAIL_73_74++;
return res;
}

inline void __pop_73_74(int n) {
TAIL_73_74+=n;
}

inline float __peek_73_74(int offs) {
return BUFFER_73_74[TAIL_73_74+offs];
}



inline void __push_74_75(float data) {
BUFFER_74_75[HEAD_74_75]=data;
HEAD_74_75++;
}



 
void init_DenomDataSum__359_128__74(){
}
void save_file_pointer__74(object_write_buffer *buf) {}
void load_file_pointer__74(object_write_buffer *buf) {}
 
void work_DenomDataSum__359_128__74(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__358 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__358 = ((float)0.0))/*float*/;
      for (((i__355__74) = 0)/*int*/; ((i__355__74) < 256); ((i__355__74)++)) {{
          (data__354__74) = BUFFER_73_74[TAIL_73_74]; TAIL_73_74++;
;
          (sum__358 = (sum__358 + ((data__354__74) * (data__354__74))))/*float*/;
        }
      }
      __push_74_75(sum__358);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_75;
int __counter_75 = 0;
int __steady_75 = 0;
int __tmp_75 = 0;
int __tmp2_75 = 0;
int *__state_flag_75 = NULL;
thread_info *__thread_75 = NULL;



inline void check_status__75() {
  check_thread_status(__state_flag_75, __thread_75);
}

void check_status_during_io__75() {
  check_thread_status_during_io(__state_flag_75, __thread_75);
}

void __init_thread_info_75(thread_info *info) {
  __state_flag_75 = info->get_state_flag();
}

thread_info *__get_thread_info_75() {
  if (__thread_75 != NULL) return __thread_75;
  __thread_75 = new thread_info(75, check_status_during_io__75);
  __init_thread_info_75(__thread_75);
  return __thread_75;
}

void __declare_sockets_75() {
  init_instance::add_incoming(74,75, DATA_SOCKET);
  init_instance::add_incoming(76,75, DATA_SOCKET);
  init_instance::add_outgoing(75,8, DATA_SOCKET);
}

void __init_sockets_75(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_75() {
}

void __peek_sockets_75() {
}


inline void __push_75_8(float data) {
BUFFER_75_8[HEAD_75_8]=data;
HEAD_75_8++;
}


inline float __pop_74_75() {
float res=BUFFER_74_75[TAIL_74_75];
TAIL_74_75++;
return res;
}

inline float __pop_76_75() {
float res=BUFFER_76_75[TAIL_76_75];
TAIL_76_75++;
return res;
}


void __joiner_75_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_75_8(__pop_74_75());
    __push_75_8(__pop_76_75());
    __push_75_8(__pop_76_75());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_76;
int __counter_76 = 0;
int __steady_76 = 0;
int __tmp_76 = 0;
int __tmp2_76 = 0;
int *__state_flag_76 = NULL;
thread_info *__thread_76 = NULL;



float _TheGlobal__pattern__6____76[256] = {0};
float numerSum__361__76 = 0.0f;
float denomPatternSum__362__76 = 0.0f;
float data__363__76 = 0.0f;
float patternData__364__76 = 0.0f;
int i__365__76 = 0;
void save_peek_buffer__76(object_write_buffer *buf);
void load_peek_buffer__76(object_write_buffer *buf);
void save_file_pointer__76(object_write_buffer *buf);
void load_file_pointer__76(object_write_buffer *buf);

inline void check_status__76() {
  check_thread_status(__state_flag_76, __thread_76);
}

void check_status_during_io__76() {
  check_thread_status_during_io(__state_flag_76, __thread_76);
}

void __init_thread_info_76(thread_info *info) {
  __state_flag_76 = info->get_state_flag();
}

thread_info *__get_thread_info_76() {
  if (__thread_76 != NULL) return __thread_76;
  __thread_76 = new thread_info(76, check_status_during_io__76);
  __init_thread_info_76(__thread_76);
  return __thread_76;
}

void __declare_sockets_76() {
  init_instance::add_incoming(73,76, DATA_SOCKET);
  init_instance::add_outgoing(76,75, DATA_SOCKET);
}

void __init_sockets_76(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_76() {
}

void __peek_sockets_76() {
}

 
void init_PatternSums__368_129__76();
inline void check_status__76();

void work_PatternSums__368_129__76(int);


inline float __pop_73_76() {
float res=BUFFER_73_76[TAIL_73_76];
TAIL_73_76++;
return res;
}

inline void __pop_73_76(int n) {
TAIL_73_76+=n;
}

inline float __peek_73_76(int offs) {
return BUFFER_73_76[TAIL_73_76+offs];
}



inline void __push_76_75(float data) {
BUFFER_76_75[HEAD_76_75]=data;
HEAD_76_75++;
}



 
void init_PatternSums__368_129__76(){
  (((_TheGlobal__pattern__6____76)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____76)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__76(object_write_buffer *buf) {}
void load_file_pointer__76(object_write_buffer *buf) {}
 
void work_PatternSums__368_129__76(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__361__76) = ((float)0.0))/*float*/;
      ((denomPatternSum__362__76) = ((float)0.0))/*float*/;
      for (((i__365__76) = 0)/*int*/; ((i__365__76) < 256); ((i__365__76)++)) {{
          ((patternData__364__76) = (((_TheGlobal__pattern__6____76)[(int)(i__365__76)]) - ((float)1.6013207E-4)))/*float*/;
          (data__363__76) = BUFFER_73_76[TAIL_73_76]; TAIL_73_76++;
;
          ((numerSum__361__76) = ((numerSum__361__76) + ((data__363__76) * (patternData__364__76))))/*float*/;
          ((denomPatternSum__362__76) = ((denomPatternSum__362__76) + ((patternData__364__76) * (patternData__364__76))))/*float*/;
        }
      }
      __push_76_75((denomPatternSum__362__76));
      __push_76_75((numerSum__361__76));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_77;
int __counter_77 = 0;
int __steady_77 = 0;
int __tmp_77 = 0;
int __tmp2_77 = 0;
int *__state_flag_77 = NULL;
thread_info *__thread_77 = NULL;



void save_peek_buffer__77(object_write_buffer *buf);
void load_peek_buffer__77(object_write_buffer *buf);
void save_file_pointer__77(object_write_buffer *buf);
void load_file_pointer__77(object_write_buffer *buf);

inline void check_status__77() {
  check_thread_status(__state_flag_77, __thread_77);
}

void check_status_during_io__77() {
  check_thread_status_during_io(__state_flag_77, __thread_77);
}

void __init_thread_info_77(thread_info *info) {
  __state_flag_77 = info->get_state_flag();
}

thread_info *__get_thread_info_77() {
  if (__thread_77 != NULL) return __thread_77;
  __thread_77 = new thread_info(77, check_status_during_io__77);
  __init_thread_info_77(__thread_77);
  return __thread_77;
}

void __declare_sockets_77() {
  init_instance::add_incoming(2,77, DATA_SOCKET);
  init_instance::add_outgoing(77,78, DATA_SOCKET);
}

void __init_sockets_77(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_77() {
}

void __peek_sockets_77() {
}

 
void init_GetAvg__386_132__77();
inline void check_status__77();

void work_GetAvg__386_132__77(int);


inline float __pop_2_77() {
float res=BUFFER_2_77[TAIL_2_77];
TAIL_2_77++;
return res;
}

inline void __pop_2_77(int n) {
TAIL_2_77+=n;
}

inline float __peek_2_77(int offs) {
return BUFFER_2_77[TAIL_2_77+offs];
}



inline void __push_77_78(float data) {
BUFFER_77_78[HEAD_77_78]=data;
HEAD_77_78++;
}



 
void init_GetAvg__386_132__77(){
}
void save_file_pointer__77(object_write_buffer *buf) {}
void load_file_pointer__77(object_write_buffer *buf) {}
 
void work_GetAvg__386_132__77(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__382 = 0;/* int */
      float Sum__383 = 0.0f;/* float */
      float val__384 = 0.0f;/* float */
      float __tmp8__385 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__383 = ((float)0.0))/*float*/;
      for ((i__382 = 0)/*int*/; (i__382 < 256); (i__382++)) {{
          val__384 = BUFFER_2_77[TAIL_2_77]; TAIL_2_77++;
;
          (Sum__383 = (Sum__383 + val__384))/*float*/;
          __push_77_78(val__384);
        }
      }
      (__tmp8__385 = (Sum__383 / ((float)256.0)))/*float*/;
      __push_77_78(__tmp8__385);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_78;
int __counter_78 = 0;
int __steady_78 = 0;
int __tmp_78 = 0;
int __tmp2_78 = 0;
int *__state_flag_78 = NULL;
thread_info *__thread_78 = NULL;



float avg__387__78 = 0.0f;
int i__388__78 = 0;
void save_peek_buffer__78(object_write_buffer *buf);
void load_peek_buffer__78(object_write_buffer *buf);
void save_file_pointer__78(object_write_buffer *buf);
void load_file_pointer__78(object_write_buffer *buf);

inline void check_status__78() {
  check_thread_status(__state_flag_78, __thread_78);
}

void check_status_during_io__78() {
  check_thread_status_during_io(__state_flag_78, __thread_78);
}

void __init_thread_info_78(thread_info *info) {
  __state_flag_78 = info->get_state_flag();
}

thread_info *__get_thread_info_78() {
  if (__thread_78 != NULL) return __thread_78;
  __thread_78 = new thread_info(78, check_status_during_io__78);
  __init_thread_info_78(__thread_78);
  return __thread_78;
}

void __declare_sockets_78() {
  init_instance::add_incoming(77,78, DATA_SOCKET);
  init_instance::add_outgoing(78,79, DATA_SOCKET);
}

void __init_sockets_78(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_78() {
}

void __peek_sockets_78() {
}

 
void init_DataMinusAvg__394_134__78();
inline void check_status__78();

void work_DataMinusAvg__394_134__78(int);


inline float __pop_77_78() {
float res=BUFFER_77_78[TAIL_77_78];
TAIL_77_78++;
return res;
}

inline void __pop_77_78(int n) {
TAIL_77_78+=n;
}

inline float __peek_77_78(int offs) {
return BUFFER_77_78[TAIL_77_78+offs];
}



inline void __push_78_79(float data) {
BUFFER_78_79[HEAD_78_79]=data;
HEAD_78_79++;
}



 
void init_DataMinusAvg__394_134__78(){
}
void save_file_pointer__78(object_write_buffer *buf) {}
void load_file_pointer__78(object_write_buffer *buf) {}
 
void work_DataMinusAvg__394_134__78(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__391 = 0.0f;/* float */
      float v__392 = 0.0f;/* float */
      float __tmp11__393 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__391 = BUFFER_77_78[TAIL_77_78+256];
;
      ((avg__387__78) = __tmp9__391)/*float*/;
      for (((i__388__78) = 0)/*int*/; ((i__388__78) < 256); ((i__388__78)++)) {{
          v__392 = BUFFER_77_78[TAIL_77_78]; TAIL_77_78++;
;
          (__tmp11__393 = (v__392 - (avg__387__78)))/*float*/;
          __push_78_79(__tmp11__393);
        }
      }
      __pop_77_78();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_79;
int __counter_79 = 0;
int __steady_79 = 0;
int __tmp_79 = 0;
int __tmp2_79 = 0;
int *__state_flag_79 = NULL;
thread_info *__thread_79 = NULL;



inline void check_status__79() {
  check_thread_status(__state_flag_79, __thread_79);
}

void check_status_during_io__79() {
  check_thread_status_during_io(__state_flag_79, __thread_79);
}

void __init_thread_info_79(thread_info *info) {
  __state_flag_79 = info->get_state_flag();
}

thread_info *__get_thread_info_79() {
  if (__thread_79 != NULL) return __thread_79;
  __thread_79 = new thread_info(79, check_status_during_io__79);
  __init_thread_info_79(__thread_79);
  return __thread_79;
}

void __declare_sockets_79() {
  init_instance::add_incoming(78,79, DATA_SOCKET);
  init_instance::add_outgoing(79,80, DATA_SOCKET);
  init_instance::add_outgoing(79,82, DATA_SOCKET);
}

void __init_sockets_79(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_79() {
}

void __peek_sockets_79() {
}

inline float __pop_78_79() {
float res=BUFFER_78_79[TAIL_78_79];
TAIL_78_79++;
return res;
}


inline void __push_79_80(float data) {
BUFFER_79_80[HEAD_79_80]=data;
HEAD_79_80++;
}



inline void __push_79_82(float data) {
BUFFER_79_82[HEAD_79_82]=data;
HEAD_79_82++;
}



void __splitter_79_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_78_79[TAIL_78_79]; TAIL_78_79++;
;
  __push_79_80(tmp);
  __push_79_82(tmp);
  }
}


// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_8;
int __counter_8 = 0;
int __steady_8 = 0;
int __tmp_8 = 0;
int __tmp2_8 = 0;
int *__state_flag_8 = NULL;
thread_info *__thread_8 = NULL;



inline void check_status__8() {
  check_thread_status(__state_flag_8, __thread_8);
}

void check_status_during_io__8() {
  check_thread_status_during_io(__state_flag_8, __thread_8);
}

void __init_thread_info_8(thread_info *info) {
  __state_flag_8 = info->get_state_flag();
}

thread_info *__get_thread_info_8() {
  if (__thread_8 != NULL) return __thread_8;
  __thread_8 = new thread_info(8, check_status_during_io__8);
  __init_thread_info_8(__thread_8);
  return __thread_8;
}

void __declare_sockets_8() {
  init_instance::add_incoming(7,8, DATA_SOCKET);
  init_instance::add_incoming(33,8, DATA_SOCKET);
  init_instance::add_incoming(39,8, DATA_SOCKET);
  init_instance::add_incoming(45,8, DATA_SOCKET);
  init_instance::add_incoming(51,8, DATA_SOCKET);
  init_instance::add_incoming(57,8, DATA_SOCKET);
  init_instance::add_incoming(63,8, DATA_SOCKET);
  init_instance::add_incoming(69,8, DATA_SOCKET);
  init_instance::add_incoming(75,8, DATA_SOCKET);
  init_instance::add_incoming(81,8, DATA_SOCKET);
  init_instance::add_incoming(87,8, DATA_SOCKET);
  init_instance::add_incoming(93,8, DATA_SOCKET);
  init_instance::add_incoming(99,8, DATA_SOCKET);
  init_instance::add_incoming(105,8, DATA_SOCKET);
  init_instance::add_incoming(111,8, DATA_SOCKET);
  init_instance::add_incoming(117,8, DATA_SOCKET);
  init_instance::add_outgoing(8,9, DATA_SOCKET);
}

void __init_sockets_8(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_8() {
}

void __peek_sockets_8() {
}


inline void __push_8_9(float data) {
BUFFER_8_9[HEAD_8_9]=data;
HEAD_8_9++;
}


inline float __pop_7_8() {
float res=BUFFER_7_8[TAIL_7_8];
TAIL_7_8++;
return res;
}

inline float __pop_33_8() {
float res=BUFFER_33_8[TAIL_33_8];
TAIL_33_8++;
return res;
}

inline float __pop_39_8() {
float res=BUFFER_39_8[TAIL_39_8];
TAIL_39_8++;
return res;
}

inline float __pop_45_8() {
float res=BUFFER_45_8[TAIL_45_8];
TAIL_45_8++;
return res;
}

inline float __pop_51_8() {
float res=BUFFER_51_8[TAIL_51_8];
TAIL_51_8++;
return res;
}

inline float __pop_57_8() {
float res=BUFFER_57_8[TAIL_57_8];
TAIL_57_8++;
return res;
}

inline float __pop_63_8() {
float res=BUFFER_63_8[TAIL_63_8];
TAIL_63_8++;
return res;
}

inline float __pop_69_8() {
float res=BUFFER_69_8[TAIL_69_8];
TAIL_69_8++;
return res;
}

inline float __pop_75_8() {
float res=BUFFER_75_8[TAIL_75_8];
TAIL_75_8++;
return res;
}

inline float __pop_81_8() {
float res=BUFFER_81_8[TAIL_81_8];
TAIL_81_8++;
return res;
}

inline float __pop_87_8() {
float res=BUFFER_87_8[TAIL_87_8];
TAIL_87_8++;
return res;
}

inline float __pop_93_8() {
float res=BUFFER_93_8[TAIL_93_8];
TAIL_93_8++;
return res;
}

inline float __pop_99_8() {
float res=BUFFER_99_8[TAIL_99_8];
TAIL_99_8++;
return res;
}

inline float __pop_105_8() {
float res=BUFFER_105_8[TAIL_105_8];
TAIL_105_8++;
return res;
}

inline float __pop_111_8() {
float res=BUFFER_111_8[TAIL_111_8];
TAIL_111_8++;
return res;
}

inline float __pop_117_8() {
float res=BUFFER_117_8[TAIL_117_8];
TAIL_117_8++;
return res;
}


void __joiner_8_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_8_9(__pop_7_8());
    __push_8_9(__pop_7_8());
    __push_8_9(__pop_7_8());
    __push_8_9(__pop_33_8());
    __push_8_9(__pop_33_8());
    __push_8_9(__pop_33_8());
    __push_8_9(__pop_39_8());
    __push_8_9(__pop_39_8());
    __push_8_9(__pop_39_8());
    __push_8_9(__pop_45_8());
    __push_8_9(__pop_45_8());
    __push_8_9(__pop_45_8());
    __push_8_9(__pop_51_8());
    __push_8_9(__pop_51_8());
    __push_8_9(__pop_51_8());
    __push_8_9(__pop_57_8());
    __push_8_9(__pop_57_8());
    __push_8_9(__pop_57_8());
    __push_8_9(__pop_63_8());
    __push_8_9(__pop_63_8());
    __push_8_9(__pop_63_8());
    __push_8_9(__pop_69_8());
    __push_8_9(__pop_69_8());
    __push_8_9(__pop_69_8());
    __push_8_9(__pop_75_8());
    __push_8_9(__pop_75_8());
    __push_8_9(__pop_75_8());
    __push_8_9(__pop_81_8());
    __push_8_9(__pop_81_8());
    __push_8_9(__pop_81_8());
    __push_8_9(__pop_87_8());
    __push_8_9(__pop_87_8());
    __push_8_9(__pop_87_8());
    __push_8_9(__pop_93_8());
    __push_8_9(__pop_93_8());
    __push_8_9(__pop_93_8());
    __push_8_9(__pop_99_8());
    __push_8_9(__pop_99_8());
    __push_8_9(__pop_99_8());
    __push_8_9(__pop_105_8());
    __push_8_9(__pop_105_8());
    __push_8_9(__pop_105_8());
    __push_8_9(__pop_111_8());
    __push_8_9(__pop_111_8());
    __push_8_9(__pop_111_8());
    __push_8_9(__pop_117_8());
    __push_8_9(__pop_117_8());
    __push_8_9(__pop_117_8());
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_80;
int __counter_80 = 0;
int __steady_80 = 0;
int __tmp_80 = 0;
int __tmp2_80 = 0;
int *__state_flag_80 = NULL;
thread_info *__thread_80 = NULL;



float data__395__80 = 0.0f;
int i__396__80 = 0;
void save_peek_buffer__80(object_write_buffer *buf);
void load_peek_buffer__80(object_write_buffer *buf);
void save_file_pointer__80(object_write_buffer *buf);
void load_file_pointer__80(object_write_buffer *buf);

inline void check_status__80() {
  check_thread_status(__state_flag_80, __thread_80);
}

void check_status_during_io__80() {
  check_thread_status_during_io(__state_flag_80, __thread_80);
}

void __init_thread_info_80(thread_info *info) {
  __state_flag_80 = info->get_state_flag();
}

thread_info *__get_thread_info_80() {
  if (__thread_80 != NULL) return __thread_80;
  __thread_80 = new thread_info(80, check_status_during_io__80);
  __init_thread_info_80(__thread_80);
  return __thread_80;
}

void __declare_sockets_80() {
  init_instance::add_incoming(79,80, DATA_SOCKET);
  init_instance::add_outgoing(80,81, DATA_SOCKET);
}

void __init_sockets_80(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_80() {
}

void __peek_sockets_80() {
}

 
void init_DenomDataSum__400_136__80();
inline void check_status__80();

void work_DenomDataSum__400_136__80(int);


inline float __pop_79_80() {
float res=BUFFER_79_80[TAIL_79_80];
TAIL_79_80++;
return res;
}

inline void __pop_79_80(int n) {
TAIL_79_80+=n;
}

inline float __peek_79_80(int offs) {
return BUFFER_79_80[TAIL_79_80+offs];
}



inline void __push_80_81(float data) {
BUFFER_80_81[HEAD_80_81]=data;
HEAD_80_81++;
}



 
void init_DenomDataSum__400_136__80(){
}
void save_file_pointer__80(object_write_buffer *buf) {}
void load_file_pointer__80(object_write_buffer *buf) {}
 
void work_DenomDataSum__400_136__80(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__399 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__399 = ((float)0.0))/*float*/;
      for (((i__396__80) = 0)/*int*/; ((i__396__80) < 256); ((i__396__80)++)) {{
          (data__395__80) = BUFFER_79_80[TAIL_79_80]; TAIL_79_80++;
;
          (sum__399 = (sum__399 + ((data__395__80) * (data__395__80))))/*float*/;
        }
      }
      __push_80_81(sum__399);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_81;
int __counter_81 = 0;
int __steady_81 = 0;
int __tmp_81 = 0;
int __tmp2_81 = 0;
int *__state_flag_81 = NULL;
thread_info *__thread_81 = NULL;



inline void check_status__81() {
  check_thread_status(__state_flag_81, __thread_81);
}

void check_status_during_io__81() {
  check_thread_status_during_io(__state_flag_81, __thread_81);
}

void __init_thread_info_81(thread_info *info) {
  __state_flag_81 = info->get_state_flag();
}

thread_info *__get_thread_info_81() {
  if (__thread_81 != NULL) return __thread_81;
  __thread_81 = new thread_info(81, check_status_during_io__81);
  __init_thread_info_81(__thread_81);
  return __thread_81;
}

void __declare_sockets_81() {
  init_instance::add_incoming(80,81, DATA_SOCKET);
  init_instance::add_incoming(82,81, DATA_SOCKET);
  init_instance::add_outgoing(81,8, DATA_SOCKET);
}

void __init_sockets_81(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_81() {
}

void __peek_sockets_81() {
}


inline void __push_81_8(float data) {
BUFFER_81_8[HEAD_81_8]=data;
HEAD_81_8++;
}


inline float __pop_80_81() {
float res=BUFFER_80_81[TAIL_80_81];
TAIL_80_81++;
return res;
}

inline float __pop_82_81() {
float res=BUFFER_82_81[TAIL_82_81];
TAIL_82_81++;
return res;
}


void __joiner_81_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_81_8(__pop_80_81());
    __push_81_8(__pop_82_81());
    __push_81_8(__pop_82_81());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_82;
int __counter_82 = 0;
int __steady_82 = 0;
int __tmp_82 = 0;
int __tmp2_82 = 0;
int *__state_flag_82 = NULL;
thread_info *__thread_82 = NULL;



float _TheGlobal__pattern__6____82[256] = {0};
float numerSum__402__82 = 0.0f;
float denomPatternSum__403__82 = 0.0f;
float data__404__82 = 0.0f;
float patternData__405__82 = 0.0f;
int i__406__82 = 0;
void save_peek_buffer__82(object_write_buffer *buf);
void load_peek_buffer__82(object_write_buffer *buf);
void save_file_pointer__82(object_write_buffer *buf);
void load_file_pointer__82(object_write_buffer *buf);

inline void check_status__82() {
  check_thread_status(__state_flag_82, __thread_82);
}

void check_status_during_io__82() {
  check_thread_status_during_io(__state_flag_82, __thread_82);
}

void __init_thread_info_82(thread_info *info) {
  __state_flag_82 = info->get_state_flag();
}

thread_info *__get_thread_info_82() {
  if (__thread_82 != NULL) return __thread_82;
  __thread_82 = new thread_info(82, check_status_during_io__82);
  __init_thread_info_82(__thread_82);
  return __thread_82;
}

void __declare_sockets_82() {
  init_instance::add_incoming(79,82, DATA_SOCKET);
  init_instance::add_outgoing(82,81, DATA_SOCKET);
}

void __init_sockets_82(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_82() {
}

void __peek_sockets_82() {
}

 
void init_PatternSums__409_137__82();
inline void check_status__82();

void work_PatternSums__409_137__82(int);


inline float __pop_79_82() {
float res=BUFFER_79_82[TAIL_79_82];
TAIL_79_82++;
return res;
}

inline void __pop_79_82(int n) {
TAIL_79_82+=n;
}

inline float __peek_79_82(int offs) {
return BUFFER_79_82[TAIL_79_82+offs];
}



inline void __push_82_81(float data) {
BUFFER_82_81[HEAD_82_81]=data;
HEAD_82_81++;
}



 
void init_PatternSums__409_137__82(){
  (((_TheGlobal__pattern__6____82)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____82)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__82(object_write_buffer *buf) {}
void load_file_pointer__82(object_write_buffer *buf) {}
 
void work_PatternSums__409_137__82(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__402__82) = ((float)0.0))/*float*/;
      ((denomPatternSum__403__82) = ((float)0.0))/*float*/;
      for (((i__406__82) = 0)/*int*/; ((i__406__82) < 256); ((i__406__82)++)) {{
          ((patternData__405__82) = (((_TheGlobal__pattern__6____82)[(int)(i__406__82)]) - ((float)1.6013207E-4)))/*float*/;
          (data__404__82) = BUFFER_79_82[TAIL_79_82]; TAIL_79_82++;
;
          ((numerSum__402__82) = ((numerSum__402__82) + ((data__404__82) * (patternData__405__82))))/*float*/;
          ((denomPatternSum__403__82) = ((denomPatternSum__403__82) + ((patternData__405__82) * (patternData__405__82))))/*float*/;
        }
      }
      __push_82_81((denomPatternSum__403__82));
      __push_82_81((numerSum__402__82));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_83;
int __counter_83 = 0;
int __steady_83 = 0;
int __tmp_83 = 0;
int __tmp2_83 = 0;
int *__state_flag_83 = NULL;
thread_info *__thread_83 = NULL;



void save_peek_buffer__83(object_write_buffer *buf);
void load_peek_buffer__83(object_write_buffer *buf);
void save_file_pointer__83(object_write_buffer *buf);
void load_file_pointer__83(object_write_buffer *buf);

inline void check_status__83() {
  check_thread_status(__state_flag_83, __thread_83);
}

void check_status_during_io__83() {
  check_thread_status_during_io(__state_flag_83, __thread_83);
}

void __init_thread_info_83(thread_info *info) {
  __state_flag_83 = info->get_state_flag();
}

thread_info *__get_thread_info_83() {
  if (__thread_83 != NULL) return __thread_83;
  __thread_83 = new thread_info(83, check_status_during_io__83);
  __init_thread_info_83(__thread_83);
  return __thread_83;
}

void __declare_sockets_83() {
  init_instance::add_incoming(2,83, DATA_SOCKET);
  init_instance::add_outgoing(83,84, DATA_SOCKET);
}

void __init_sockets_83(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_83() {
}

void __peek_sockets_83() {
}

 
void init_GetAvg__427_140__83();
inline void check_status__83();

void work_GetAvg__427_140__83(int);


inline float __pop_2_83() {
float res=BUFFER_2_83[TAIL_2_83];
TAIL_2_83++;
return res;
}

inline void __pop_2_83(int n) {
TAIL_2_83+=n;
}

inline float __peek_2_83(int offs) {
return BUFFER_2_83[TAIL_2_83+offs];
}



inline void __push_83_84(float data) {
BUFFER_83_84[HEAD_83_84]=data;
HEAD_83_84++;
}



 
void init_GetAvg__427_140__83(){
}
void save_file_pointer__83(object_write_buffer *buf) {}
void load_file_pointer__83(object_write_buffer *buf) {}
 
void work_GetAvg__427_140__83(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__423 = 0;/* int */
      float Sum__424 = 0.0f;/* float */
      float val__425 = 0.0f;/* float */
      float __tmp8__426 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__424 = ((float)0.0))/*float*/;
      for ((i__423 = 0)/*int*/; (i__423 < 256); (i__423++)) {{
          val__425 = BUFFER_2_83[TAIL_2_83]; TAIL_2_83++;
;
          (Sum__424 = (Sum__424 + val__425))/*float*/;
          __push_83_84(val__425);
        }
      }
      (__tmp8__426 = (Sum__424 / ((float)256.0)))/*float*/;
      __push_83_84(__tmp8__426);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_84;
int __counter_84 = 0;
int __steady_84 = 0;
int __tmp_84 = 0;
int __tmp2_84 = 0;
int *__state_flag_84 = NULL;
thread_info *__thread_84 = NULL;



float avg__428__84 = 0.0f;
int i__429__84 = 0;
void save_peek_buffer__84(object_write_buffer *buf);
void load_peek_buffer__84(object_write_buffer *buf);
void save_file_pointer__84(object_write_buffer *buf);
void load_file_pointer__84(object_write_buffer *buf);

inline void check_status__84() {
  check_thread_status(__state_flag_84, __thread_84);
}

void check_status_during_io__84() {
  check_thread_status_during_io(__state_flag_84, __thread_84);
}

void __init_thread_info_84(thread_info *info) {
  __state_flag_84 = info->get_state_flag();
}

thread_info *__get_thread_info_84() {
  if (__thread_84 != NULL) return __thread_84;
  __thread_84 = new thread_info(84, check_status_during_io__84);
  __init_thread_info_84(__thread_84);
  return __thread_84;
}

void __declare_sockets_84() {
  init_instance::add_incoming(83,84, DATA_SOCKET);
  init_instance::add_outgoing(84,85, DATA_SOCKET);
}

void __init_sockets_84(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_84() {
}

void __peek_sockets_84() {
}

 
void init_DataMinusAvg__435_142__84();
inline void check_status__84();

void work_DataMinusAvg__435_142__84(int);


inline float __pop_83_84() {
float res=BUFFER_83_84[TAIL_83_84];
TAIL_83_84++;
return res;
}

inline void __pop_83_84(int n) {
TAIL_83_84+=n;
}

inline float __peek_83_84(int offs) {
return BUFFER_83_84[TAIL_83_84+offs];
}



inline void __push_84_85(float data) {
BUFFER_84_85[HEAD_84_85]=data;
HEAD_84_85++;
}



 
void init_DataMinusAvg__435_142__84(){
}
void save_file_pointer__84(object_write_buffer *buf) {}
void load_file_pointer__84(object_write_buffer *buf) {}
 
void work_DataMinusAvg__435_142__84(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__432 = 0.0f;/* float */
      float v__433 = 0.0f;/* float */
      float __tmp11__434 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__432 = BUFFER_83_84[TAIL_83_84+256];
;
      ((avg__428__84) = __tmp9__432)/*float*/;
      for (((i__429__84) = 0)/*int*/; ((i__429__84) < 256); ((i__429__84)++)) {{
          v__433 = BUFFER_83_84[TAIL_83_84]; TAIL_83_84++;
;
          (__tmp11__434 = (v__433 - (avg__428__84)))/*float*/;
          __push_84_85(__tmp11__434);
        }
      }
      __pop_83_84();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_85;
int __counter_85 = 0;
int __steady_85 = 0;
int __tmp_85 = 0;
int __tmp2_85 = 0;
int *__state_flag_85 = NULL;
thread_info *__thread_85 = NULL;



inline void check_status__85() {
  check_thread_status(__state_flag_85, __thread_85);
}

void check_status_during_io__85() {
  check_thread_status_during_io(__state_flag_85, __thread_85);
}

void __init_thread_info_85(thread_info *info) {
  __state_flag_85 = info->get_state_flag();
}

thread_info *__get_thread_info_85() {
  if (__thread_85 != NULL) return __thread_85;
  __thread_85 = new thread_info(85, check_status_during_io__85);
  __init_thread_info_85(__thread_85);
  return __thread_85;
}

void __declare_sockets_85() {
  init_instance::add_incoming(84,85, DATA_SOCKET);
  init_instance::add_outgoing(85,86, DATA_SOCKET);
  init_instance::add_outgoing(85,88, DATA_SOCKET);
}

void __init_sockets_85(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_85() {
}

void __peek_sockets_85() {
}

inline float __pop_84_85() {
float res=BUFFER_84_85[TAIL_84_85];
TAIL_84_85++;
return res;
}


inline void __push_85_86(float data) {
BUFFER_85_86[HEAD_85_86]=data;
HEAD_85_86++;
}



inline void __push_85_88(float data) {
BUFFER_85_88[HEAD_85_88]=data;
HEAD_85_88++;
}



void __splitter_85_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_84_85[TAIL_84_85]; TAIL_84_85++;
;
  __push_85_86(tmp);
  __push_85_88(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_86;
int __counter_86 = 0;
int __steady_86 = 0;
int __tmp_86 = 0;
int __tmp2_86 = 0;
int *__state_flag_86 = NULL;
thread_info *__thread_86 = NULL;



float data__436__86 = 0.0f;
int i__437__86 = 0;
void save_peek_buffer__86(object_write_buffer *buf);
void load_peek_buffer__86(object_write_buffer *buf);
void save_file_pointer__86(object_write_buffer *buf);
void load_file_pointer__86(object_write_buffer *buf);

inline void check_status__86() {
  check_thread_status(__state_flag_86, __thread_86);
}

void check_status_during_io__86() {
  check_thread_status_during_io(__state_flag_86, __thread_86);
}

void __init_thread_info_86(thread_info *info) {
  __state_flag_86 = info->get_state_flag();
}

thread_info *__get_thread_info_86() {
  if (__thread_86 != NULL) return __thread_86;
  __thread_86 = new thread_info(86, check_status_during_io__86);
  __init_thread_info_86(__thread_86);
  return __thread_86;
}

void __declare_sockets_86() {
  init_instance::add_incoming(85,86, DATA_SOCKET);
  init_instance::add_outgoing(86,87, DATA_SOCKET);
}

void __init_sockets_86(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_86() {
}

void __peek_sockets_86() {
}

 
void init_DenomDataSum__441_144__86();
inline void check_status__86();

void work_DenomDataSum__441_144__86(int);


inline float __pop_85_86() {
float res=BUFFER_85_86[TAIL_85_86];
TAIL_85_86++;
return res;
}

inline void __pop_85_86(int n) {
TAIL_85_86+=n;
}

inline float __peek_85_86(int offs) {
return BUFFER_85_86[TAIL_85_86+offs];
}



inline void __push_86_87(float data) {
BUFFER_86_87[HEAD_86_87]=data;
HEAD_86_87++;
}



 
void init_DenomDataSum__441_144__86(){
}
void save_file_pointer__86(object_write_buffer *buf) {}
void load_file_pointer__86(object_write_buffer *buf) {}
 
void work_DenomDataSum__441_144__86(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__440 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__440 = ((float)0.0))/*float*/;
      for (((i__437__86) = 0)/*int*/; ((i__437__86) < 256); ((i__437__86)++)) {{
          (data__436__86) = BUFFER_85_86[TAIL_85_86]; TAIL_85_86++;
;
          (sum__440 = (sum__440 + ((data__436__86) * (data__436__86))))/*float*/;
        }
      }
      __push_86_87(sum__440);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_87;
int __counter_87 = 0;
int __steady_87 = 0;
int __tmp_87 = 0;
int __tmp2_87 = 0;
int *__state_flag_87 = NULL;
thread_info *__thread_87 = NULL;



inline void check_status__87() {
  check_thread_status(__state_flag_87, __thread_87);
}

void check_status_during_io__87() {
  check_thread_status_during_io(__state_flag_87, __thread_87);
}

void __init_thread_info_87(thread_info *info) {
  __state_flag_87 = info->get_state_flag();
}

thread_info *__get_thread_info_87() {
  if (__thread_87 != NULL) return __thread_87;
  __thread_87 = new thread_info(87, check_status_during_io__87);
  __init_thread_info_87(__thread_87);
  return __thread_87;
}

void __declare_sockets_87() {
  init_instance::add_incoming(86,87, DATA_SOCKET);
  init_instance::add_incoming(88,87, DATA_SOCKET);
  init_instance::add_outgoing(87,8, DATA_SOCKET);
}

void __init_sockets_87(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_87() {
}

void __peek_sockets_87() {
}


inline void __push_87_8(float data) {
BUFFER_87_8[HEAD_87_8]=data;
HEAD_87_8++;
}


inline float __pop_86_87() {
float res=BUFFER_86_87[TAIL_86_87];
TAIL_86_87++;
return res;
}

inline float __pop_88_87() {
float res=BUFFER_88_87[TAIL_88_87];
TAIL_88_87++;
return res;
}


void __joiner_87_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_87_8(__pop_86_87());
    __push_87_8(__pop_88_87());
    __push_87_8(__pop_88_87());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_88;
int __counter_88 = 0;
int __steady_88 = 0;
int __tmp_88 = 0;
int __tmp2_88 = 0;
int *__state_flag_88 = NULL;
thread_info *__thread_88 = NULL;



float _TheGlobal__pattern__6____88[256] = {0};
float numerSum__443__88 = 0.0f;
float denomPatternSum__444__88 = 0.0f;
float data__445__88 = 0.0f;
float patternData__446__88 = 0.0f;
int i__447__88 = 0;
void save_peek_buffer__88(object_write_buffer *buf);
void load_peek_buffer__88(object_write_buffer *buf);
void save_file_pointer__88(object_write_buffer *buf);
void load_file_pointer__88(object_write_buffer *buf);

inline void check_status__88() {
  check_thread_status(__state_flag_88, __thread_88);
}

void check_status_during_io__88() {
  check_thread_status_during_io(__state_flag_88, __thread_88);
}

void __init_thread_info_88(thread_info *info) {
  __state_flag_88 = info->get_state_flag();
}

thread_info *__get_thread_info_88() {
  if (__thread_88 != NULL) return __thread_88;
  __thread_88 = new thread_info(88, check_status_during_io__88);
  __init_thread_info_88(__thread_88);
  return __thread_88;
}

void __declare_sockets_88() {
  init_instance::add_incoming(85,88, DATA_SOCKET);
  init_instance::add_outgoing(88,87, DATA_SOCKET);
}

void __init_sockets_88(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_88() {
}

void __peek_sockets_88() {
}

 
void init_PatternSums__450_145__88();
inline void check_status__88();

void work_PatternSums__450_145__88(int);


inline float __pop_85_88() {
float res=BUFFER_85_88[TAIL_85_88];
TAIL_85_88++;
return res;
}

inline void __pop_85_88(int n) {
TAIL_85_88+=n;
}

inline float __peek_85_88(int offs) {
return BUFFER_85_88[TAIL_85_88+offs];
}



inline void __push_88_87(float data) {
BUFFER_88_87[HEAD_88_87]=data;
HEAD_88_87++;
}



 
void init_PatternSums__450_145__88(){
  (((_TheGlobal__pattern__6____88)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____88)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__88(object_write_buffer *buf) {}
void load_file_pointer__88(object_write_buffer *buf) {}
 
void work_PatternSums__450_145__88(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__443__88) = ((float)0.0))/*float*/;
      ((denomPatternSum__444__88) = ((float)0.0))/*float*/;
      for (((i__447__88) = 0)/*int*/; ((i__447__88) < 256); ((i__447__88)++)) {{
          ((patternData__446__88) = (((_TheGlobal__pattern__6____88)[(int)(i__447__88)]) - ((float)1.6013207E-4)))/*float*/;
          (data__445__88) = BUFFER_85_88[TAIL_85_88]; TAIL_85_88++;
;
          ((numerSum__443__88) = ((numerSum__443__88) + ((data__445__88) * (patternData__446__88))))/*float*/;
          ((denomPatternSum__444__88) = ((denomPatternSum__444__88) + ((patternData__446__88) * (patternData__446__88))))/*float*/;
        }
      }
      __push_88_87((denomPatternSum__444__88));
      __push_88_87((numerSum__443__88));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_89;
int __counter_89 = 0;
int __steady_89 = 0;
int __tmp_89 = 0;
int __tmp2_89 = 0;
int *__state_flag_89 = NULL;
thread_info *__thread_89 = NULL;



void save_peek_buffer__89(object_write_buffer *buf);
void load_peek_buffer__89(object_write_buffer *buf);
void save_file_pointer__89(object_write_buffer *buf);
void load_file_pointer__89(object_write_buffer *buf);

inline void check_status__89() {
  check_thread_status(__state_flag_89, __thread_89);
}

void check_status_during_io__89() {
  check_thread_status_during_io(__state_flag_89, __thread_89);
}

void __init_thread_info_89(thread_info *info) {
  __state_flag_89 = info->get_state_flag();
}

thread_info *__get_thread_info_89() {
  if (__thread_89 != NULL) return __thread_89;
  __thread_89 = new thread_info(89, check_status_during_io__89);
  __init_thread_info_89(__thread_89);
  return __thread_89;
}

void __declare_sockets_89() {
  init_instance::add_incoming(2,89, DATA_SOCKET);
  init_instance::add_outgoing(89,90, DATA_SOCKET);
}

void __init_sockets_89(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_89() {
}

void __peek_sockets_89() {
}

 
void init_GetAvg__468_148__89();
inline void check_status__89();

void work_GetAvg__468_148__89(int);


inline float __pop_2_89() {
float res=BUFFER_2_89[TAIL_2_89];
TAIL_2_89++;
return res;
}

inline void __pop_2_89(int n) {
TAIL_2_89+=n;
}

inline float __peek_2_89(int offs) {
return BUFFER_2_89[TAIL_2_89+offs];
}



inline void __push_89_90(float data) {
BUFFER_89_90[HEAD_89_90]=data;
HEAD_89_90++;
}



 
void init_GetAvg__468_148__89(){
}
void save_file_pointer__89(object_write_buffer *buf) {}
void load_file_pointer__89(object_write_buffer *buf) {}
 
void work_GetAvg__468_148__89(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__464 = 0;/* int */
      float Sum__465 = 0.0f;/* float */
      float val__466 = 0.0f;/* float */
      float __tmp8__467 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__465 = ((float)0.0))/*float*/;
      for ((i__464 = 0)/*int*/; (i__464 < 256); (i__464++)) {{
          val__466 = BUFFER_2_89[TAIL_2_89]; TAIL_2_89++;
;
          (Sum__465 = (Sum__465 + val__466))/*float*/;
          __push_89_90(val__466);
        }
      }
      (__tmp8__467 = (Sum__465 / ((float)256.0)))/*float*/;
      __push_89_90(__tmp8__467);
      // mark end: SIRFilter GetAvg

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_9;
int __counter_9 = 0;
int __steady_9 = 0;
int __tmp_9 = 0;
int __tmp2_9 = 0;
int *__state_flag_9 = NULL;
thread_info *__thread_9 = NULL;



inline void check_status__9() {
  check_thread_status(__state_flag_9, __thread_9);
}

void check_status_during_io__9() {
  check_thread_status_during_io(__state_flag_9, __thread_9);
}

void __init_thread_info_9(thread_info *info) {
  __state_flag_9 = info->get_state_flag();
}

thread_info *__get_thread_info_9() {
  if (__thread_9 != NULL) return __thread_9;
  __thread_9 = new thread_info(9, check_status_during_io__9);
  __init_thread_info_9(__thread_9);
  return __thread_9;
}

void __declare_sockets_9() {
  init_instance::add_incoming(8,9, DATA_SOCKET);
  init_instance::add_outgoing(9,10, DATA_SOCKET);
  init_instance::add_outgoing(9,13, DATA_SOCKET);
  init_instance::add_outgoing(9,14, DATA_SOCKET);
  init_instance::add_outgoing(9,15, DATA_SOCKET);
  init_instance::add_outgoing(9,16, DATA_SOCKET);
  init_instance::add_outgoing(9,17, DATA_SOCKET);
  init_instance::add_outgoing(9,18, DATA_SOCKET);
  init_instance::add_outgoing(9,19, DATA_SOCKET);
  init_instance::add_outgoing(9,20, DATA_SOCKET);
  init_instance::add_outgoing(9,21, DATA_SOCKET);
  init_instance::add_outgoing(9,22, DATA_SOCKET);
  init_instance::add_outgoing(9,23, DATA_SOCKET);
  init_instance::add_outgoing(9,24, DATA_SOCKET);
  init_instance::add_outgoing(9,25, DATA_SOCKET);
  init_instance::add_outgoing(9,26, DATA_SOCKET);
  init_instance::add_outgoing(9,27, DATA_SOCKET);
}

void __init_sockets_9(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_9() {
}

void __peek_sockets_9() {
}

inline float __pop_8_9() {
float res=BUFFER_8_9[TAIL_8_9];
TAIL_8_9++;
return res;
}


inline void __push_9_10(float data) {
BUFFER_9_10[HEAD_9_10]=data;
HEAD_9_10++;
}



inline void __push_9_13(float data) {
BUFFER_9_13[HEAD_9_13]=data;
HEAD_9_13++;
}



inline void __push_9_14(float data) {
BUFFER_9_14[HEAD_9_14]=data;
HEAD_9_14++;
}



inline void __push_9_15(float data) {
BUFFER_9_15[HEAD_9_15]=data;
HEAD_9_15++;
}



inline void __push_9_16(float data) {
BUFFER_9_16[HEAD_9_16]=data;
HEAD_9_16++;
}



inline void __push_9_17(float data) {
BUFFER_9_17[HEAD_9_17]=data;
HEAD_9_17++;
}



inline void __push_9_18(float data) {
BUFFER_9_18[HEAD_9_18]=data;
HEAD_9_18++;
}



inline void __push_9_19(float data) {
BUFFER_9_19[HEAD_9_19]=data;
HEAD_9_19++;
}



inline void __push_9_20(float data) {
BUFFER_9_20[HEAD_9_20]=data;
HEAD_9_20++;
}



inline void __push_9_21(float data) {
BUFFER_9_21[HEAD_9_21]=data;
HEAD_9_21++;
}



inline void __push_9_22(float data) {
BUFFER_9_22[HEAD_9_22]=data;
HEAD_9_22++;
}



inline void __push_9_23(float data) {
BUFFER_9_23[HEAD_9_23]=data;
HEAD_9_23++;
}



inline void __push_9_24(float data) {
BUFFER_9_24[HEAD_9_24]=data;
HEAD_9_24++;
}



inline void __push_9_25(float data) {
BUFFER_9_25[HEAD_9_25]=data;
HEAD_9_25++;
}



inline void __push_9_26(float data) {
BUFFER_9_26[HEAD_9_26]=data;
HEAD_9_26++;
}



inline void __push_9_27(float data) {
BUFFER_9_27[HEAD_9_27]=data;
HEAD_9_27++;
}



void __splitter_9_work(int ____n) {
  for (;____n > 0; ____n--) {
__push_9_10(__pop_8_9());
__push_9_10(__pop_8_9());
__push_9_10(__pop_8_9());
__push_9_13(__pop_8_9());
__push_9_13(__pop_8_9());
__push_9_13(__pop_8_9());
__push_9_14(__pop_8_9());
__push_9_14(__pop_8_9());
__push_9_14(__pop_8_9());
__push_9_15(__pop_8_9());
__push_9_15(__pop_8_9());
__push_9_15(__pop_8_9());
__push_9_16(__pop_8_9());
__push_9_16(__pop_8_9());
__push_9_16(__pop_8_9());
__push_9_17(__pop_8_9());
__push_9_17(__pop_8_9());
__push_9_17(__pop_8_9());
__push_9_18(__pop_8_9());
__push_9_18(__pop_8_9());
__push_9_18(__pop_8_9());
__push_9_19(__pop_8_9());
__push_9_19(__pop_8_9());
__push_9_19(__pop_8_9());
__push_9_20(__pop_8_9());
__push_9_20(__pop_8_9());
__push_9_20(__pop_8_9());
__push_9_21(__pop_8_9());
__push_9_21(__pop_8_9());
__push_9_21(__pop_8_9());
__push_9_22(__pop_8_9());
__push_9_22(__pop_8_9());
__push_9_22(__pop_8_9());
__push_9_23(__pop_8_9());
__push_9_23(__pop_8_9());
__push_9_23(__pop_8_9());
__push_9_24(__pop_8_9());
__push_9_24(__pop_8_9());
__push_9_24(__pop_8_9());
__push_9_25(__pop_8_9());
__push_9_25(__pop_8_9());
__push_9_25(__pop_8_9());
__push_9_26(__pop_8_9());
__push_9_26(__pop_8_9());
__push_9_26(__pop_8_9());
__push_9_27(__pop_8_9());
__push_9_27(__pop_8_9());
__push_9_27(__pop_8_9());
  }
}


// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_90;
int __counter_90 = 0;
int __steady_90 = 0;
int __tmp_90 = 0;
int __tmp2_90 = 0;
int *__state_flag_90 = NULL;
thread_info *__thread_90 = NULL;



float avg__469__90 = 0.0f;
int i__470__90 = 0;
void save_peek_buffer__90(object_write_buffer *buf);
void load_peek_buffer__90(object_write_buffer *buf);
void save_file_pointer__90(object_write_buffer *buf);
void load_file_pointer__90(object_write_buffer *buf);

inline void check_status__90() {
  check_thread_status(__state_flag_90, __thread_90);
}

void check_status_during_io__90() {
  check_thread_status_during_io(__state_flag_90, __thread_90);
}

void __init_thread_info_90(thread_info *info) {
  __state_flag_90 = info->get_state_flag();
}

thread_info *__get_thread_info_90() {
  if (__thread_90 != NULL) return __thread_90;
  __thread_90 = new thread_info(90, check_status_during_io__90);
  __init_thread_info_90(__thread_90);
  return __thread_90;
}

void __declare_sockets_90() {
  init_instance::add_incoming(89,90, DATA_SOCKET);
  init_instance::add_outgoing(90,91, DATA_SOCKET);
}

void __init_sockets_90(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_90() {
}

void __peek_sockets_90() {
}

 
void init_DataMinusAvg__476_150__90();
inline void check_status__90();

void work_DataMinusAvg__476_150__90(int);


inline float __pop_89_90() {
float res=BUFFER_89_90[TAIL_89_90];
TAIL_89_90++;
return res;
}

inline void __pop_89_90(int n) {
TAIL_89_90+=n;
}

inline float __peek_89_90(int offs) {
return BUFFER_89_90[TAIL_89_90+offs];
}



inline void __push_90_91(float data) {
BUFFER_90_91[HEAD_90_91]=data;
HEAD_90_91++;
}



 
void init_DataMinusAvg__476_150__90(){
}
void save_file_pointer__90(object_write_buffer *buf) {}
void load_file_pointer__90(object_write_buffer *buf) {}
 
void work_DataMinusAvg__476_150__90(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__473 = 0.0f;/* float */
      float v__474 = 0.0f;/* float */
      float __tmp11__475 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__473 = BUFFER_89_90[TAIL_89_90+256];
;
      ((avg__469__90) = __tmp9__473)/*float*/;
      for (((i__470__90) = 0)/*int*/; ((i__470__90) < 256); ((i__470__90)++)) {{
          v__474 = BUFFER_89_90[TAIL_89_90]; TAIL_89_90++;
;
          (__tmp11__475 = (v__474 - (avg__469__90)))/*float*/;
          __push_90_91(__tmp11__475);
        }
      }
      __pop_89_90();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_91;
int __counter_91 = 0;
int __steady_91 = 0;
int __tmp_91 = 0;
int __tmp2_91 = 0;
int *__state_flag_91 = NULL;
thread_info *__thread_91 = NULL;



inline void check_status__91() {
  check_thread_status(__state_flag_91, __thread_91);
}

void check_status_during_io__91() {
  check_thread_status_during_io(__state_flag_91, __thread_91);
}

void __init_thread_info_91(thread_info *info) {
  __state_flag_91 = info->get_state_flag();
}

thread_info *__get_thread_info_91() {
  if (__thread_91 != NULL) return __thread_91;
  __thread_91 = new thread_info(91, check_status_during_io__91);
  __init_thread_info_91(__thread_91);
  return __thread_91;
}

void __declare_sockets_91() {
  init_instance::add_incoming(90,91, DATA_SOCKET);
  init_instance::add_outgoing(91,92, DATA_SOCKET);
  init_instance::add_outgoing(91,94, DATA_SOCKET);
}

void __init_sockets_91(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_91() {
}

void __peek_sockets_91() {
}

inline float __pop_90_91() {
float res=BUFFER_90_91[TAIL_90_91];
TAIL_90_91++;
return res;
}


inline void __push_91_92(float data) {
BUFFER_91_92[HEAD_91_92]=data;
HEAD_91_92++;
}



inline void __push_91_94(float data) {
BUFFER_91_94[HEAD_91_94]=data;
HEAD_91_94++;
}



void __splitter_91_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_90_91[TAIL_90_91]; TAIL_90_91++;
;
  __push_91_92(tmp);
  __push_91_94(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_92;
int __counter_92 = 0;
int __steady_92 = 0;
int __tmp_92 = 0;
int __tmp2_92 = 0;
int *__state_flag_92 = NULL;
thread_info *__thread_92 = NULL;



float data__477__92 = 0.0f;
int i__478__92 = 0;
void save_peek_buffer__92(object_write_buffer *buf);
void load_peek_buffer__92(object_write_buffer *buf);
void save_file_pointer__92(object_write_buffer *buf);
void load_file_pointer__92(object_write_buffer *buf);

inline void check_status__92() {
  check_thread_status(__state_flag_92, __thread_92);
}

void check_status_during_io__92() {
  check_thread_status_during_io(__state_flag_92, __thread_92);
}

void __init_thread_info_92(thread_info *info) {
  __state_flag_92 = info->get_state_flag();
}

thread_info *__get_thread_info_92() {
  if (__thread_92 != NULL) return __thread_92;
  __thread_92 = new thread_info(92, check_status_during_io__92);
  __init_thread_info_92(__thread_92);
  return __thread_92;
}

void __declare_sockets_92() {
  init_instance::add_incoming(91,92, DATA_SOCKET);
  init_instance::add_outgoing(92,93, DATA_SOCKET);
}

void __init_sockets_92(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_92() {
}

void __peek_sockets_92() {
}

 
void init_DenomDataSum__482_152__92();
inline void check_status__92();

void work_DenomDataSum__482_152__92(int);


inline float __pop_91_92() {
float res=BUFFER_91_92[TAIL_91_92];
TAIL_91_92++;
return res;
}

inline void __pop_91_92(int n) {
TAIL_91_92+=n;
}

inline float __peek_91_92(int offs) {
return BUFFER_91_92[TAIL_91_92+offs];
}



inline void __push_92_93(float data) {
BUFFER_92_93[HEAD_92_93]=data;
HEAD_92_93++;
}



 
void init_DenomDataSum__482_152__92(){
}
void save_file_pointer__92(object_write_buffer *buf) {}
void load_file_pointer__92(object_write_buffer *buf) {}
 
void work_DenomDataSum__482_152__92(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__481 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__481 = ((float)0.0))/*float*/;
      for (((i__478__92) = 0)/*int*/; ((i__478__92) < 256); ((i__478__92)++)) {{
          (data__477__92) = BUFFER_91_92[TAIL_91_92]; TAIL_91_92++;
;
          (sum__481 = (sum__481 + ((data__477__92) * (data__477__92))))/*float*/;
        }
      }
      __push_92_93(sum__481);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_93;
int __counter_93 = 0;
int __steady_93 = 0;
int __tmp_93 = 0;
int __tmp2_93 = 0;
int *__state_flag_93 = NULL;
thread_info *__thread_93 = NULL;



inline void check_status__93() {
  check_thread_status(__state_flag_93, __thread_93);
}

void check_status_during_io__93() {
  check_thread_status_during_io(__state_flag_93, __thread_93);
}

void __init_thread_info_93(thread_info *info) {
  __state_flag_93 = info->get_state_flag();
}

thread_info *__get_thread_info_93() {
  if (__thread_93 != NULL) return __thread_93;
  __thread_93 = new thread_info(93, check_status_during_io__93);
  __init_thread_info_93(__thread_93);
  return __thread_93;
}

void __declare_sockets_93() {
  init_instance::add_incoming(92,93, DATA_SOCKET);
  init_instance::add_incoming(94,93, DATA_SOCKET);
  init_instance::add_outgoing(93,8, DATA_SOCKET);
}

void __init_sockets_93(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_93() {
}

void __peek_sockets_93() {
}


inline void __push_93_8(float data) {
BUFFER_93_8[HEAD_93_8]=data;
HEAD_93_8++;
}


inline float __pop_92_93() {
float res=BUFFER_92_93[TAIL_92_93];
TAIL_92_93++;
return res;
}

inline float __pop_94_93() {
float res=BUFFER_94_93[TAIL_94_93];
TAIL_94_93++;
return res;
}


void __joiner_93_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_93_8(__pop_92_93());
    __push_93_8(__pop_94_93());
    __push_93_8(__pop_94_93());
  }
}


// peek: 256 pop: 256 push 2
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_94;
int __counter_94 = 0;
int __steady_94 = 0;
int __tmp_94 = 0;
int __tmp2_94 = 0;
int *__state_flag_94 = NULL;
thread_info *__thread_94 = NULL;



float _TheGlobal__pattern__6____94[256] = {0};
float numerSum__484__94 = 0.0f;
float denomPatternSum__485__94 = 0.0f;
float data__486__94 = 0.0f;
float patternData__487__94 = 0.0f;
int i__488__94 = 0;
void save_peek_buffer__94(object_write_buffer *buf);
void load_peek_buffer__94(object_write_buffer *buf);
void save_file_pointer__94(object_write_buffer *buf);
void load_file_pointer__94(object_write_buffer *buf);

inline void check_status__94() {
  check_thread_status(__state_flag_94, __thread_94);
}

void check_status_during_io__94() {
  check_thread_status_during_io(__state_flag_94, __thread_94);
}

void __init_thread_info_94(thread_info *info) {
  __state_flag_94 = info->get_state_flag();
}

thread_info *__get_thread_info_94() {
  if (__thread_94 != NULL) return __thread_94;
  __thread_94 = new thread_info(94, check_status_during_io__94);
  __init_thread_info_94(__thread_94);
  return __thread_94;
}

void __declare_sockets_94() {
  init_instance::add_incoming(91,94, DATA_SOCKET);
  init_instance::add_outgoing(94,93, DATA_SOCKET);
}

void __init_sockets_94(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_94() {
}

void __peek_sockets_94() {
}

 
void init_PatternSums__491_153__94();
inline void check_status__94();

void work_PatternSums__491_153__94(int);


inline float __pop_91_94() {
float res=BUFFER_91_94[TAIL_91_94];
TAIL_91_94++;
return res;
}

inline void __pop_91_94(int n) {
TAIL_91_94+=n;
}

inline float __peek_91_94(int offs) {
return BUFFER_91_94[TAIL_91_94+offs];
}



inline void __push_94_93(float data) {
BUFFER_94_93[HEAD_94_93]=data;
HEAD_94_93++;
}



 
void init_PatternSums__491_153__94(){
  (((_TheGlobal__pattern__6____94)[(int)0]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)1]) = ((float)0.81481516))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)2]) = ((float)0.3187751))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)3]) = ((float)-0.3080322))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)4]) = ((float)-0.8193477))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)5]) = ((float)-0.9989324))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)6]) = ((float)-0.7567074))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)7]) = ((float)-0.18134266))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)8]) = ((float)0.4821842))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)9]) = ((float)0.9313967))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)10]) = ((float)0.9453574))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)11]) = ((float)0.50071883))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)12]) = ((float)-0.19809836))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)13]) = ((float)-0.8033218))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)14]) = ((float)-0.9944844))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)15]) = ((float)-0.6541886))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)16]) = ((float)0.049067523))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)17]) = ((float)0.7317405))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)18]) = ((float)0.9995056))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)19]) = ((float)0.6804607))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)20]) = ((float)-0.052131888))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)21]) = ((float)-0.7593349))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)22]) = ((float)-0.99382204))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)23]) = ((float)-0.5893665))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)24]) = ((float)0.20711324))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)25]) = ((float)0.87093675))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)26]) = ((float)0.94128513))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)27]) = ((float)0.3511118))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)28]) = ((float)-0.4955663))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)29]) = ((float)-0.98634))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)30]) = ((float)-0.74454516))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)31]) = ((float)0.0661063))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)32]) = ((float)0.83147013))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)33]) = ((float)0.94403666))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)34]) = ((float)0.29541615))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)35]) = ((float)-0.6007715))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)36]) = ((float)-0.99961877))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)37]) = ((float)-0.5477329))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)38]) = ((float)0.3833934))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)39]) = ((float)0.9811211))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)40]) = ((float)0.6983764))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)41]) = ((float)-0.22676428))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)42]) = ((float)-0.94585705))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)43]) = ((float)-0.7709367))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)44]) = ((float)0.14976725))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)45]) = ((float)0.92857796))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)46]) = ((float)0.78408086))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)47]) = ((float)-0.15753722))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)48]) = ((float)-0.9415442))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)49]) = ((float)-0.7418499))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)50]) = ((float)0.24967095))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)51]) = ((float)0.97574437))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)52]) = ((float)0.63201547))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)53]) = ((float)-0.41939676))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)54]) = ((float)-0.99993384))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)55]) = ((float)-0.43153432))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)56]) = ((float)0.6438307))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)57]) = ((float)0.9599461))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)58]) = ((float)0.12164798))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)59]) = ((float)-0.8686663))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)60]) = ((float)-0.78645533))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)61]) = ((float)0.28458887))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)62]) = ((float)0.99688154))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)63]) = ((float)0.42321593))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)64]) = ((float)-0.7071089))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)65]) = ((float)-0.90192926))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)66]) = ((float)0.11708063))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)67]) = ((float)0.9796089))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)68]) = ((float)0.49022233))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)69]) = ((float)-0.6874558))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)70]) = ((float)-0.89287865))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)71]) = ((float)0.18473229))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)72]) = ((float)0.9963134))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)73]) = ((float)0.34103754))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)74]) = ((float)-0.82502556))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)75]) = ((float)-0.74697125))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)76]) = ((float)0.47410357))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)77]) = ((float)0.9637249))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)78]) = ((float)-0.055967513))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)79]) = ((float)-0.98897874))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)80]) = ((float)-0.33688518))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)81]) = ((float)0.8632985))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)82]) = ((float)0.64792514))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)83]) = ((float)-0.6439816))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)84]) = ((float)-0.856141))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)85]) = ((float)0.38569298))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)86]) = ((float)0.96677727))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)87]) = ((float)-0.13020845))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)88]) = ((float)-0.9999246))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)89]) = ((float)-0.09629034))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)90]) = ((float)0.9809338))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)91]) = ((float)0.2812736))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)92]) = ((float)-0.9340924))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)93]) = ((float)-0.42182478))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)94]) = ((float)0.87938094))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)95]) = ((float)0.5204965))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)96]) = ((float)-0.83147097))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)97]) = ((float)-0.58190566))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)98]) = ((float)0.7999985))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)99]) = ((float)0.6102209))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)100]) = ((float)-0.7902279))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)101]) = ((float)-0.6077933))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)102]) = ((float)0.80366087))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)103]) = ((float)0.5743889))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)104]) = ((float)-0.83822674))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)105]) = ((float)-0.5073362))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)106]) = ((float)0.8879967))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)107]) = ((float)0.4022553))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)108]) = ((float)-0.9425739))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)109]) = ((float)-0.25466993))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)110]) = ((float)0.9864341))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)111]) = ((float)0.062647454))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)112]) = ((float)-0.9987955))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)113]) = ((float)0.16965018))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)114]) = ((float)0.9549129))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)115]) = ((float)-0.42773297))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)116]) = ((float)-0.82975996))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)117]) = ((float)0.68299294))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)118]) = ((float)0.60489017))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)119]) = ((float)-0.8912377))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)120]) = ((float)-0.27851585))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)121]) = ((float)0.99645984))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)122]) = ((float)-0.12317802))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)123]) = ((float)-0.9425091))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)124]) = ((float)0.5376035))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)125]) = ((float)0.69383085))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)126]) = ((float)-0.86743474))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)127]) = ((float)-0.26208684))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)128]) = ((float)1.0))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)129]) = ((float)-0.27134258))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)130]) = ((float)-0.8477118))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)131]) = ((float)0.7533158))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)132]) = ((float)0.40243447))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)133]) = ((float)-0.99489933))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)134]) = ((float)0.21985714))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)135]) = ((float)0.85044634))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)136]) = ((float)-0.78074425))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)137]) = ((float)-0.31786814))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)138]) = ((float)0.99929345))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)139]) = ((float)-0.39698857))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)140]) = ((float)-0.7049301))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)141]) = ((float)0.9239542))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)142]) = ((float)-0.0069145486))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)143]) = ((float)-0.9147529))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)144]) = ((float)0.7409603))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)145]) = ((float)0.30327824))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)146]) = ((float)-0.99160945))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)147]) = ((float)0.54548186))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)148]) = ((float)0.5114514))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)149]) = ((float)-0.99752563))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)150]) = ((float)0.39470577))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)151]) = ((float)0.6282929))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)152]) = ((float)-0.9831045))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)153]) = ((float)0.3153301))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)154]) = ((float)0.67098564))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)155]) = ((float)-0.97772026))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)156]) = ((float)0.31659204))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)157]) = ((float)0.6483606))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)158]) = ((float)-0.98837477))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)159]) = ((float)0.39841086))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)160]) = ((float)0.5555696))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)161]) = ((float)-0.9999721))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)162]) = ((float)0.5511072))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)163]) = ((float)0.37680766))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)164]) = ((float)-0.975025))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)165]) = ((float)0.74723125))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)166]) = ((float)0.09725057))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)167]) = ((float)-0.856826))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)168]) = ((float)0.92851204))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)169]) = ((float)-0.2742812))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)170]) = ((float)-0.5851684))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)171]) = ((float)0.99862945))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)172]) = ((float)-0.6738392))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)173]) = ((float)-0.13436688))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)174]) = ((float)0.84114105))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)175]) = ((float)-0.9600599))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)176]) = ((float)0.4275643))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)177]) = ((float)0.38389412))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)178]) = ((float)-0.9391902))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)179]) = ((float)0.8933098))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)180]) = ((float)-0.29322395))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)181]) = ((float)-0.47931948))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)182]) = ((float)0.96021557))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)183]) = ((float)-0.8798463))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)184]) = ((float)0.30201897))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)185]) = ((float)0.43706828))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)186]) = ((float)-0.932715))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)187]) = ((float)0.9319564))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)188]) = ((float)-0.45238176))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)189]) = ((float)-0.24874286))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)190]) = ((float)0.82065415))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)191]) = ((float)-0.9956433))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)192]) = ((float)0.7071147))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)193]) = ((float)-0.102802284))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)194]) = ((float)-0.5395129))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)195]) = ((float)0.94352657))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)196]) = ((float)-0.9504956))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)197]) = ((float)0.5734672))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)198]) = ((float)0.023747925))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)199]) = ((float)-0.6041352))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)200]) = ((float)0.9533051))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)201]) = ((float)-0.95565057))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)202]) = ((float)0.62547445))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)203]) = ((float)-0.08908227))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)204]) = ((float)-0.46867162))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)205]) = ((float)0.86999434))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)206]) = ((float)-0.9991116))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)207]) = ((float)0.83073795))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)208]) = ((float)-0.42757252))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)209]) = ((float)-0.087127484))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)210]) = ((float)0.5701339))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)211]) = ((float)-0.8985776))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)212]) = ((float)0.9989414))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)213]) = ((float)-0.85941607))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)214]) = ((float)0.52526444))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)215]) = ((float)-0.08138699))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)216]) = ((float)-0.37131795))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)217]) = ((float)0.73978496))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)218]) = ((float)-0.9567044))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)219]) = ((float)0.9905093))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)220]) = ((float)-0.84649616))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)221]) = ((float)0.5608459))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)222]) = ((float)-0.18986793))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)223]) = ((float)-0.20242192))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)224]) = ((float)0.55556715))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)225]) = ((float)-0.82186484))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)226]) = ((float)0.9713141))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)227]) = ((float)-0.9932335))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)228]) = ((float)0.8946046))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)229]) = ((float)-0.69633204))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)230]) = ((float)0.42827713))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)231]) = ((float)-0.12412753))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)232]) = ((float)-0.18300752))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)233]) = ((float)0.46442574))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)234]) = ((float)-0.6978167))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)235]) = ((float)0.8684767))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)236]) = ((float)-0.96927947))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)237]) = ((float)0.9999275))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)238]) = ((float)-0.96560496))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)239]) = ((float)0.87542474))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)240]) = ((float)-0.74096835))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)241]) = ((float)0.57474387))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)242]) = ((float)-0.38909924))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)243]) = ((float)0.19530457))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)244]) = ((float)-0.003064934))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)245]) = ((float)-0.17981124))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)246]) = ((float)0.34771466))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)247]) = ((float)-0.49673003))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)248]) = ((float)0.62482584))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)249]) = ((float)-0.7314653))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)250]) = ((float)0.81714803))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)251]) = ((float)-0.88326454))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)252]) = ((float)0.93186903))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)253]) = ((float)-0.96532875))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)254]) = ((float)0.98618436))/*float*/;
  (((_TheGlobal__pattern__6____94)[(int)255]) = ((float)-0.99692714))/*float*/;
}
void save_file_pointer__94(object_write_buffer *buf) {}
void load_file_pointer__94(object_write_buffer *buf) {}
 
void work_PatternSums__491_153__94(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      // mark begin: SIRFilter PatternSums

      ((numerSum__484__94) = ((float)0.0))/*float*/;
      ((denomPatternSum__485__94) = ((float)0.0))/*float*/;
      for (((i__488__94) = 0)/*int*/; ((i__488__94) < 256); ((i__488__94)++)) {{
          ((patternData__487__94) = (((_TheGlobal__pattern__6____94)[(int)(i__488__94)]) - ((float)1.6013207E-4)))/*float*/;
          (data__486__94) = BUFFER_91_94[TAIL_91_94]; TAIL_91_94++;
;
          ((numerSum__484__94) = ((numerSum__484__94) + ((data__486__94) * (patternData__487__94))))/*float*/;
          ((denomPatternSum__485__94) = ((denomPatternSum__485__94) + ((patternData__487__94) * (patternData__487__94))))/*float*/;
        }
      }
      __push_94_93((denomPatternSum__485__94));
      __push_94_93((numerSum__484__94));
      // mark end: SIRFilter PatternSums

    }
  }
}

// peek: 256 pop: 256 push 257
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_95;
int __counter_95 = 0;
int __steady_95 = 0;
int __tmp_95 = 0;
int __tmp2_95 = 0;
int *__state_flag_95 = NULL;
thread_info *__thread_95 = NULL;



void save_peek_buffer__95(object_write_buffer *buf);
void load_peek_buffer__95(object_write_buffer *buf);
void save_file_pointer__95(object_write_buffer *buf);
void load_file_pointer__95(object_write_buffer *buf);

inline void check_status__95() {
  check_thread_status(__state_flag_95, __thread_95);
}

void check_status_during_io__95() {
  check_thread_status_during_io(__state_flag_95, __thread_95);
}

void __init_thread_info_95(thread_info *info) {
  __state_flag_95 = info->get_state_flag();
}

thread_info *__get_thread_info_95() {
  if (__thread_95 != NULL) return __thread_95;
  __thread_95 = new thread_info(95, check_status_during_io__95);
  __init_thread_info_95(__thread_95);
  return __thread_95;
}

void __declare_sockets_95() {
  init_instance::add_incoming(2,95, DATA_SOCKET);
  init_instance::add_outgoing(95,96, DATA_SOCKET);
}

void __init_sockets_95(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_95() {
}

void __peek_sockets_95() {
}

 
void init_GetAvg__509_156__95();
inline void check_status__95();

void work_GetAvg__509_156__95(int);


inline float __pop_2_95() {
float res=BUFFER_2_95[TAIL_2_95];
TAIL_2_95++;
return res;
}

inline void __pop_2_95(int n) {
TAIL_2_95+=n;
}

inline float __peek_2_95(int offs) {
return BUFFER_2_95[TAIL_2_95+offs];
}



inline void __push_95_96(float data) {
BUFFER_95_96[HEAD_95_96]=data;
HEAD_95_96++;
}



 
void init_GetAvg__509_156__95(){
}
void save_file_pointer__95(object_write_buffer *buf) {}
void load_file_pointer__95(object_write_buffer *buf) {}
 
void work_GetAvg__509_156__95(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      int i__505 = 0;/* int */
      float Sum__506 = 0.0f;/* float */
      float val__507 = 0.0f;/* float */
      float __tmp8__508 = 0.0f;/* float */

      // mark begin: SIRFilter GetAvg

      (Sum__506 = ((float)0.0))/*float*/;
      for ((i__505 = 0)/*int*/; (i__505 < 256); (i__505++)) {{
          val__507 = BUFFER_2_95[TAIL_2_95]; TAIL_2_95++;
;
          (Sum__506 = (Sum__506 + val__507))/*float*/;
          __push_95_96(val__507);
        }
      }
      (__tmp8__508 = (Sum__506 / ((float)256.0)))/*float*/;
      __push_95_96(__tmp8__508);
      // mark end: SIRFilter GetAvg

    }
  }
}

// peek: 257 pop: 257 push 256
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_96;
int __counter_96 = 0;
int __steady_96 = 0;
int __tmp_96 = 0;
int __tmp2_96 = 0;
int *__state_flag_96 = NULL;
thread_info *__thread_96 = NULL;



float avg__510__96 = 0.0f;
int i__511__96 = 0;
void save_peek_buffer__96(object_write_buffer *buf);
void load_peek_buffer__96(object_write_buffer *buf);
void save_file_pointer__96(object_write_buffer *buf);
void load_file_pointer__96(object_write_buffer *buf);

inline void check_status__96() {
  check_thread_status(__state_flag_96, __thread_96);
}

void check_status_during_io__96() {
  check_thread_status_during_io(__state_flag_96, __thread_96);
}

void __init_thread_info_96(thread_info *info) {
  __state_flag_96 = info->get_state_flag();
}

thread_info *__get_thread_info_96() {
  if (__thread_96 != NULL) return __thread_96;
  __thread_96 = new thread_info(96, check_status_during_io__96);
  __init_thread_info_96(__thread_96);
  return __thread_96;
}

void __declare_sockets_96() {
  init_instance::add_incoming(95,96, DATA_SOCKET);
  init_instance::add_outgoing(96,97, DATA_SOCKET);
}

void __init_sockets_96(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_96() {
}

void __peek_sockets_96() {
}

 
void init_DataMinusAvg__517_158__96();
inline void check_status__96();

void work_DataMinusAvg__517_158__96(int);


inline float __pop_95_96() {
float res=BUFFER_95_96[TAIL_95_96];
TAIL_95_96++;
return res;
}

inline void __pop_95_96(int n) {
TAIL_95_96+=n;
}

inline float __peek_95_96(int offs) {
return BUFFER_95_96[TAIL_95_96+offs];
}



inline void __push_96_97(float data) {
BUFFER_96_97[HEAD_96_97]=data;
HEAD_96_97++;
}



 
void init_DataMinusAvg__517_158__96(){
}
void save_file_pointer__96(object_write_buffer *buf) {}
void load_file_pointer__96(object_write_buffer *buf) {}
 
void work_DataMinusAvg__517_158__96(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float __tmp9__514 = 0.0f;/* float */
      float v__515 = 0.0f;/* float */
      float __tmp11__516 = 0.0f;/* float */

      // mark begin: SIRFilter DataMinusAvg

      __tmp9__514 = BUFFER_95_96[TAIL_95_96+256];
;
      ((avg__510__96) = __tmp9__514)/*float*/;
      for (((i__511__96) = 0)/*int*/; ((i__511__96) < 256); ((i__511__96)++)) {{
          v__515 = BUFFER_95_96[TAIL_95_96]; TAIL_95_96++;
;
          (__tmp11__516 = (v__515 - (avg__510__96)))/*float*/;
          __push_96_97(__tmp11__516);
        }
      }
      __pop_95_96();
      // mark end: SIRFilter DataMinusAvg

    }
  }
}

// init counts: 0 steady counts: 256

// ClusterFusion isEliminated: false



int __number_of_iterations_97;
int __counter_97 = 0;
int __steady_97 = 0;
int __tmp_97 = 0;
int __tmp2_97 = 0;
int *__state_flag_97 = NULL;
thread_info *__thread_97 = NULL;



inline void check_status__97() {
  check_thread_status(__state_flag_97, __thread_97);
}

void check_status_during_io__97() {
  check_thread_status_during_io(__state_flag_97, __thread_97);
}

void __init_thread_info_97(thread_info *info) {
  __state_flag_97 = info->get_state_flag();
}

thread_info *__get_thread_info_97() {
  if (__thread_97 != NULL) return __thread_97;
  __thread_97 = new thread_info(97, check_status_during_io__97);
  __init_thread_info_97(__thread_97);
  return __thread_97;
}

void __declare_sockets_97() {
  init_instance::add_incoming(96,97, DATA_SOCKET);
  init_instance::add_outgoing(97,98, DATA_SOCKET);
  init_instance::add_outgoing(97,100, DATA_SOCKET);
}

void __init_sockets_97(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_97() {
}

void __peek_sockets_97() {
}

inline float __pop_96_97() {
float res=BUFFER_96_97[TAIL_96_97];
TAIL_96_97++;
return res;
}


inline void __push_97_98(float data) {
BUFFER_97_98[HEAD_97_98]=data;
HEAD_97_98++;
}



inline void __push_97_100(float data) {
BUFFER_97_100[HEAD_97_100]=data;
HEAD_97_100++;
}



void __splitter_97_work(int ____n) {
  for (;____n > 0; ____n--) {
  float tmp;
  tmp = BUFFER_96_97[TAIL_96_97]; TAIL_96_97++;
;
  __push_97_98(tmp);
  __push_97_100(tmp);
  }
}


// peek: 256 pop: 256 push 1
// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_98;
int __counter_98 = 0;
int __steady_98 = 0;
int __tmp_98 = 0;
int __tmp2_98 = 0;
int *__state_flag_98 = NULL;
thread_info *__thread_98 = NULL;



float data__518__98 = 0.0f;
int i__519__98 = 0;
void save_peek_buffer__98(object_write_buffer *buf);
void load_peek_buffer__98(object_write_buffer *buf);
void save_file_pointer__98(object_write_buffer *buf);
void load_file_pointer__98(object_write_buffer *buf);

inline void check_status__98() {
  check_thread_status(__state_flag_98, __thread_98);
}

void check_status_during_io__98() {
  check_thread_status_during_io(__state_flag_98, __thread_98);
}

void __init_thread_info_98(thread_info *info) {
  __state_flag_98 = info->get_state_flag();
}

thread_info *__get_thread_info_98() {
  if (__thread_98 != NULL) return __thread_98;
  __thread_98 = new thread_info(98, check_status_during_io__98);
  __init_thread_info_98(__thread_98);
  return __thread_98;
}

void __declare_sockets_98() {
  init_instance::add_incoming(97,98, DATA_SOCKET);
  init_instance::add_outgoing(98,99, DATA_SOCKET);
}

void __init_sockets_98(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_98() {
}

void __peek_sockets_98() {
}

 
void init_DenomDataSum__523_160__98();
inline void check_status__98();

void work_DenomDataSum__523_160__98(int);


inline float __pop_97_98() {
float res=BUFFER_97_98[TAIL_97_98];
TAIL_97_98++;
return res;
}

inline void __pop_97_98(int n) {
TAIL_97_98+=n;
}

inline float __peek_97_98(int offs) {
return BUFFER_97_98[TAIL_97_98+offs];
}



inline void __push_98_99(float data) {
BUFFER_98_99[HEAD_98_99]=data;
HEAD_98_99++;
}



 
void init_DenomDataSum__523_160__98(){
}
void save_file_pointer__98(object_write_buffer *buf) {}
void load_file_pointer__98(object_write_buffer *buf) {}
 
void work_DenomDataSum__523_160__98(int ____n){
  for (
  ; (0 < ____n); (____n--)) {{
      float sum__522 = 0.0f;/* float */

      // mark begin: SIRFilter DenomDataSum

      (sum__522 = ((float)0.0))/*float*/;
      for (((i__519__98) = 0)/*int*/; ((i__519__98) < 256); ((i__519__98)++)) {{
          (data__518__98) = BUFFER_97_98[TAIL_97_98]; TAIL_97_98++;
;
          (sum__522 = (sum__522 + ((data__518__98) * (data__518__98))))/*float*/;
        }
      }
      __push_98_99(sum__522);
      // mark end: SIRFilter DenomDataSum

    }
  }
}

// init counts: 0 steady counts: 1

// ClusterFusion isEliminated: false



int __number_of_iterations_99;
int __counter_99 = 0;
int __steady_99 = 0;
int __tmp_99 = 0;
int __tmp2_99 = 0;
int *__state_flag_99 = NULL;
thread_info *__thread_99 = NULL;



inline void check_status__99() {
  check_thread_status(__state_flag_99, __thread_99);
}

void check_status_during_io__99() {
  check_thread_status_during_io(__state_flag_99, __thread_99);
}

void __init_thread_info_99(thread_info *info) {
  __state_flag_99 = info->get_state_flag();
}

thread_info *__get_thread_info_99() {
  if (__thread_99 != NULL) return __thread_99;
  __thread_99 = new thread_info(99, check_status_during_io__99);
  __init_thread_info_99(__thread_99);
  return __thread_99;
}

void __declare_sockets_99() {
  init_instance::add_incoming(98,99, DATA_SOCKET);
  init_instance::add_incoming(100,99, DATA_SOCKET);
  init_instance::add_outgoing(99,8, DATA_SOCKET);
}

void __init_sockets_99(void (*cs_fptr)()) {
  mysocket *sock;

}

void __flush_sockets_99() {
}

void __peek_sockets_99() {
}


inline void __push_99_8(float data) {
BUFFER_99_8[HEAD_99_8]=data;
HEAD_99_8++;
}


inline float __pop_98_99() {
float res=BUFFER_98_99[TAIL_98_99];
TAIL_98_99++;
return res;
}

inline float __pop_100_99() {
float res=BUFFER_100_99[TAIL_100_99];
TAIL_100_99++;
return res;
}


void __joiner_99_work(int ____n) {
  for (;____n > 0; ____n--) {
    __push_99_8(__pop_98_99());
    __push_99_8(__pop_100_99());
    __push_99_8(__pop_100_99());
  }
}


