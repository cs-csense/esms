#ifndef __FUSION_H
#define __FUSION_H

#define max(A,B) (((A)>(B))?(A):(B))
#define pow2ceil(A) ((A<=2)?(2): (((A<=4)?(4): (((A<=8)?(8): (((A<=16)?(16): (((A<=32)?(32): (((A<=64)?(64): (((A<=128)?(128): (((A<=256)?(256):(((A<=1024)?(1024):(((A<=4096)?(4096):(((A<=16384)?(16384):(((A<=65536)?(65536):(((A<=131072)?(131072):(((A<=262144)?(262144):(((A<=524288)?(524288):(((A<=1048576)?(1048576):(((A<=2097152)?(2097152):(((A<=4194304)?(4194304):(((A<=8388608)?(8388608):(((A<=16777216)?(16777216):(((A<=33554432)?(33554432):(((A<=67108864)?(67108864):(((A<=134217728)?(134217728):(((A<=268435456)?(268435456):(((A<=536870912)?(536870912):(1073741824))))))))))))))))))))))))))))))))))))))))))))))))))
#define __MULT 3

//destination peeks: 255 extra items
#define __PEEK_BUF_SIZE_0_1 255
#define __BUF_SIZE_MASK_0_1 (pow2ceil(271*__MULT+255)-1)

#define __BUF_SIZE_MASK_1_2 (pow2ceil(4096*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_3 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_29 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_35 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_41 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_47 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_53 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_59 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_65 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_71 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_77 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_83 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_89 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_95 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_101 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_107 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_2_113 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_3_4 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_4_5 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_5_6 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_5_28 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_6_7 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_7_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_8_9 (pow2ceil(48*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_10 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_13 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_14 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_15 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_16 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_17 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_18 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_19 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_20 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_21 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_22 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_23 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_24 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_25 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_26 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_9_27 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_10_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_11_12 (pow2ceil(16*__MULT+0)-1)

#define __BUF_SIZE_MASK_13_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_14_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_15_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_16_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_17_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_18_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_19_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_20_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_21_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_22_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_23_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_24_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_25_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_26_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_27_11 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_28_7 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_29_30 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_30_31 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_31_32 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_31_34 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_32_33 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_33_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_34_33 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_35_36 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_36_37 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_37_38 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_37_40 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_38_39 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_39_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_40_39 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_41_42 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_42_43 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_43_44 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_43_46 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_44_45 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_45_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_46_45 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_47_48 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_48_49 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_49_50 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_49_52 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_50_51 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_51_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_52_51 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_53_54 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_54_55 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_55_56 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_55_58 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_56_57 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_57_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_58_57 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_59_60 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_60_61 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_61_62 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_61_64 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_62_63 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_63_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_64_63 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_65_66 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_66_67 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_67_68 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_67_70 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_68_69 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_69_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_70_69 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_71_72 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_72_73 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_73_74 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_73_76 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_74_75 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_75_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_76_75 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_77_78 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_78_79 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_79_80 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_79_82 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_80_81 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_81_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_82_81 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_83_84 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_84_85 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_85_86 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_85_88 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_86_87 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_87_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_88_87 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_89_90 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_90_91 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_91_92 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_91_94 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_92_93 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_93_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_94_93 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_95_96 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_96_97 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_97_98 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_97_100 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_98_99 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_99_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_100_99 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_101_102 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_102_103 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_103_104 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_103_106 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_104_105 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_105_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_106_105 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_107_108 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_108_109 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_109_110 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_109_112 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_110_111 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_111_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_112_111 (pow2ceil(2*__MULT+0)-1)

#define __BUF_SIZE_MASK_113_114 (pow2ceil(257*__MULT+0)-1)

#define __BUF_SIZE_MASK_114_115 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_115_116 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_115_118 (pow2ceil(256*__MULT+0)-1)

#define __BUF_SIZE_MASK_116_117 (pow2ceil(1*__MULT+0)-1)

#define __BUF_SIZE_MASK_117_8 (pow2ceil(3*__MULT+0)-1)

#define __BUF_SIZE_MASK_118_117 (pow2ceil(2*__MULT+0)-1)

#endif
