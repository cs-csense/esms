//
// Created by Farley Lai on 7/31/15.
//

#include "gtest/gtest.h"

using namespace testing;

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}