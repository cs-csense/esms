//
// Created by Farley Lai on 7/30/15.
//

#include <gtest/gtest.h>
#include "IntelProfiler.h"
#include "thread.h"

using namespace testing;

static void setUp() {
}

static void tearDown() {
}

TEST(IntelProfilerTest, test_profiling) {
    setUp();
    IntelProfiler<2> profiler;
    if(!profiler.start(4, 500))
        throw "Failed to start profiler\n";
    thread_sleep(4000);
    profiler.stop();
    profiler.save("test_profiling.csv");
    tearDown();
}