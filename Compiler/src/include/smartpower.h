//
// Created by Farley Lai on 8/31/15.
//

#ifndef ESMS_SMARTPOWER_H
#define ESMS_SMARTPOWER_H
#ifdef __cplusplus
extern "C" {
#endif

#include "hidapi.h"
#include "odroid/odroid.h"

#define REQUEST_DATA        0x37
#define REQUEST_STARTSTOP   0x80
#define REQUEST_STATUS      0x81
#define REQUEST_ONOFF       0x82
#define REQUEST_VERSION     0x83

hid_device* smartpower_open();
void smartpower_close(hid_device* device);
int smartpower_sample(hid_device* device, sample_t* sample);
int smartpower_cmd(hid_device* device, unsigned char code, void* data, size_t size);
const char* smartpower_str(unsigned char code);

#ifdef __cplusplus
}
#endif
#endif //ESMS_SMARTPOWER_H
