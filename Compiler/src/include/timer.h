#ifndef __TIMER_H
#define __TIMER_H
#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>
#include <sys/time.h>

long long timestamp();

#ifdef __cplusplus
}
#endif
#endif
