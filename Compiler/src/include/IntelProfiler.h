//
// Created by Farley Lai on 11/17/15.
//

#ifndef ESMS_INTELPROFILER_H
#define ESMS_INTELPROFILER_H

#include "Profiler.h"

typedef struct {
    int cpu;
    int freq;
    struct {
        double watts;
        double joules;
        double mWh;
    } power;
    struct {
        int degrees;
        bool hot;
    } temperature;
} Sample;

ostream& operator<<(ostream& os, const Sample& s);

template<size_t N>
class IntelProfiler: public Profiler<Sample, N> {
public:
    IntelProfiler();
//    bool GetNumNodes(int * nNodes);
//    bool GetNumMsrs(int *nMsr);
//    bool GetMsrName(int iMsr, char *szName);
//    bool GetMsrFunc(int iMsr, int *funcID);
//    bool GetIAFrequency(int iNode, int *freqInMHz);
//    bool GetGTFrequency(int *freq);
//    bool GetTDP(int iNode, double *TDP);
//    bool GetMaxTemperature(int iNode, int *degreeC);
//    bool GetTemperature(int iNode, int *degreeC);
//    bool GetBaseFrequency(int iNode, double *baseFrequency);
//    bool GetPowerData(int iNode, int iMSR, double *result, int *nResult);
//    bool ReadSample();
//
//    bool IsGTAvailable();
//    bool StartLog(char *szFileName);
//    bool StopLog();
//    bool GetSysTime(void *sysTime);
//    bool GetRDTSC(uint64_t *TSC);
//    bool GetTimeInterval(double *offset);
//    std::string GetLastError();

protected:
    bool sample();

private:
    int nodes;
    int msrs;
};

#include <iostream>
#include <fstream>
#include <algorithm>
#include <IntelPowerGadget/EnergyLib.h>
#include "IntelProfiler.h"

ostream& operator<<(ostream& os, const Sample& s) {
    string del(", ");
    os << s.freq << del << s.power.watts << del << s.power.joules << del << s.power.mWh << del;
    os << s.temperature.degrees << del << (s.temperature.hot ? 1 : 0);
    return os;
}

template<size_t N>
IntelProfiler<N>::IntelProfiler() {
    if(!IntelEnergyLibInitialize())
        throw "Failed to initialize Intel Energy Lib";
    if(!GetNumNodes(&nodes))
        throw "Failed to get the number of CPU packages: ";
    if(!GetNumMsrs(&msrs))
        throw "Failed to get the number of CPU MSRs: ";
    cout << nodes << " CPUs, each with " << msrs << " MSRs" << endl;
}

typedef struct {
    int secs;
    int ns;
} SysTime;

template<size_t N>
bool IntelProfiler<N>::sample() {
    bool ret;
    if((ret = ReadSample())) {
        typename Profiler<Sample, N>::Record& r = this->getRecord(this->count());
        long long t;
        GetSysTime(&t);
        //r.timestamp = t.secs * 1000000000LL + t.ns;
        r.timestamp = (t >> 32) * 1000000000LL + (t & 0x00000000FFFFFFFF);
        cout << "Sample timestamp: "  << r.timestamp << ", t: " << t << endl;
        r.timestamp = t;
        int n = N < this->nodes ? N : this->nodes;
        for(int i = 0; i < this->nodes; i++) {
            double base;
            int freq, GT;
            GetBaseFrequency(i, &base);
            GetIAFrequency(i, &freq);
            GetGTFrequency(&GT);
            cout << "Base: " << base << ", IAFrequency: " << freq << ", GT: " << GT << endl;

            Sample& s = r.samples[i];
            s.cpu = i;
            for(int j = 0; j < msrs; j++) {
                int func;
                int nData;
                double data[3];
                char name[1024];
                GetMsrName(j, name);
                GetMsrFunc(j, &func);
                GetPowerData(i, j, data, &nData);
                switch(func) {
//                    case MSR_FUNC_FREQ:
//                        cout << name <<": MSR_FUNC_FREQ: "<< endl;
//                        s.freq = (int)data[0];
//                        break;
                    case MSR_FUNC_POWER:
                        cout << name << ": MSR_FUNC_POWER" << endl;
                        s.power.watts = data[0];
                        s.power.joules = data[1];
                        s.power.mWh = data[2];
                        break;
                    case MSR_FUNC_TEMP:
                        cout << name << ": MSR_FUNC_TEMP" << endl;
                        s.temperature.degrees = data[0];
                        s.temperature.hot = data[1] > 0;
                        break;
                    case MSR_FUNC_LIMIT:
                        cout << name << ": MSR_FUNC_LIMIT: " << nData << ": " << data[0] << endl;
                        break;
                    default:
                        cout << name << ": MSR_FUNC_UNKNOWN" << endl;
                        break;
                }
                cout  << "CPU[" << s.cpu << "] MSR[" << j << "] " << " (freq, watts, joules, mWh, deg, hot) = (" << s << ")" << endl;
            }
        }
    } else
        throw "Failed to read power sample";
    return ret;
}


#endif //ESMS_INTELPROFILER_H
