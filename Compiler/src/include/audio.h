#ifndef __AUDIO_H
#define __AUDIO_H
#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    AUDIO_SRC_DEFAULT,
    AUDIO_SRC_MIC,
    AUDIO_SRC_VOICE_RECOGNITION,
} audio_input_t;

typedef enum {
    SAMPLE_RATE_8K = 8000,
    SAMPLE_RATE_16K = 16000,
    SAMPLE_RATE_44K = 44100,
} audio_sample_rate_t;

typedef enum {
    CHANNEL_IN_MONO = 1,
    CHANNEL_IN_STEREO = 2,
} audio_channel_t;

typedef enum {
    ENCODING_PCM_S8_LE,
    ENCODING_PCM_S16_LE,
    ENCODING_PCM_FLOAT_LE,
} audio_format_t;

typedef struct {
    size_t size;
    void *addr;
    long long timestamp;
} audio_buffer_t;

size_t audio_min_buffer_size(audio_sample_rate_t rate, audio_channel_t channels, audio_format_t fmt);
void* audio_source_open(audio_input_t device, audio_sample_rate_t rate, audio_channel_t channels, audio_format_t fmt, size_t size);
int audio_source_start(void* src);
int audio_source_stop(void* src);
int audio_source_close(void* src);
audio_buffer_t* audio_source_read(void* src);

#ifdef __cplusplus
}
#endif
#endif
