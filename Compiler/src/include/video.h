//
// Created by Farley Lai on 7/30/15.
//

#ifndef ESMS_VIDEO_H
#define ESMS_VIDEO_H
#ifdef __cplusplus
extern "C" {
#endif

#include <linux/videodev2.h>

typedef enum {
    LENS_FACING_FRONT,
    LENS_FACING_BACK,
} lens_facing_t;

typedef enum {
    HD_720P,
    VGA_480P,
    QGVA_240P,
} video_size_t;

typedef enum {
    NV16,
    NV21,
    MJPEG,
    YUV_420_888,
    YUYV_422_888,
} video_format_t;

typedef struct {
    int index;
    size_t size;
    void *addr;
    long long timestamp;
} video_buffer_t;

void* video_source_open(lens_facing_t lens, video_size_t resolution, video_format_t fmt, int fps);
int video_source_start(void* src);
int video_source_stop(void* src);
int video_source_close(void* src);
int video_source_release(void* src, video_buffer_t *buffer);
video_buffer_t* video_source_read(void *handle);

#ifdef __cplusplus
}
#endif
#endif //ESMS_VIDEO_H
