#ifndef __SHELL_H
#define __SHELL_H
#ifdef __cplusplus
extern "C" {
#endif

#include<stdlib.h>

int shell(const char *cmd, char buf[], size_t size);
int shell_run(const char *cmd);
int shell_atoi(const char *cmd);

#ifdef __cplusplus
}
#endif
#endif
