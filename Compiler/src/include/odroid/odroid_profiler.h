#ifndef __ODROID_PROFILER_H
#define __ODROID_PROFILER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include "odroid/odroid.h"

/**
 * Start the profiler for duration seconds and the sampling interval in ms.
 *
 * @param duration the total sampling duration in seconds
 * @param interval the sampling interval in ms.
 * @cpu the CPU to run the profiler.
 * @return the profiler handle
 */
void* odroid_profiler_start(int duration, int interval, int cpu);

/**
 * Stop the profiler.
 *
 * @param self the profiler
 */
int odroid_profiler_stop(void* self);

/**
 * Save the profile data as csv.
 *
 * @param self the profiler
 * @filename the csv filename
 */
int odroid_profiler_save(void* self, const char* filename);
int odroid_profiler_samples(void* self);
long long odroid_profiler_duration(void* self);
void odroid_profiler_energy(void* self, energy_t energies[], energy_t* energy);

/**
 * Print the toString profile data.
 *
 * @param self the profiler
 * @param trace true to print some sample values.
 */
void odroid_profiler_info(void* self, bool trace);

#ifdef __cplusplus
}
#endif
#endif
