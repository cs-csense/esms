#ifndef __ODROID_H
#define __ODROID_H

#ifdef __cplusplus
extern "C" {
#endif

#define GPUFREQ_NODE    "/sys/devices/11800000.mali/clock"
#define TEMP_NODE       "/sys/devices/10060000.tmu/temp"

#define INA231_IOCGREG      _IOR('i', 1, ina231_iocreg_t *)
#define INA231_IOCSSTATUS   _IOW('i', 2, ina231_iocreg_t *)
#define INA231_IOCGSTATUS   _IOR('i', 3, ina231_iocreg_t *)

#define DEV_SENSOR_ARM  "/dev/sensor_arm"
#define DEV_SENSOR_KFC  "/dev/sensor_kfc"
#define DEV_SENSOR_MEM  "/dev/sensor_mem"
#define DEV_SENSOR_G3D  "/dev/sensor_g3d"

#define NPROC 8

enum {
    SENSOR_ARM = 0,
    SENSOR_KFC,
    SENSOR_MEM,
    SENSOR_G3D,
    SENSOR_MAX
};

typedef struct {
    char name[20];
    unsigned int enable;
    unsigned int cur_uV;
    unsigned int cur_uA;
    unsigned int cur_uW;
} ina231_iocreg_t;

typedef struct {
    int  fd;
    ina231_iocreg_t data;
} sensor_t;

typedef struct {
    long long timestamp;
    int user[NPROC], sys[NPROC], idle[NPROC];
    int armuV, armuA, armuW;
    int kfcuV, kfcuA, kfcuW;
    int memuV, memuA, memuW;
    int g3duV, g3duA, g3duW;
    double sysV, sysA, sysW, sysWh;
} sample_t;

typedef struct {
    double ms;
    double util[NPROC];
    double armmWh, kfcmWh, memmWh, g3dmWh, sysWh;
} energy_t;

int odroid_open();
int odroid_close();
void odroid_sample(sample_t* sample, const int nproc);
void odroid_energy(sample_t* s1, sample_t* s2, energy_t* energy);
void odroid_sample_info(sample_t* sample);
void odroid_energy_info(energy_t* energy);

#ifdef __cplusplus
}
#endif
#endif
