#ifndef __THREAD_H
#define __THREAD_H
#ifdef __cplusplus
extern "C" {
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef __USE_GNU
#define __USE_GNU
#endif
#ifndef __USE_XOPEN2K8
#define __USE_XOPEN2K8
#endif
#include <sched.h>
#include <pthread.h>

#ifdef __ANDROID__
typedef pid_t thread_t;
#else
typedef pthread_t thread_t;
#endif

thread_t thread_self();
int thread_sleep(int ms);
int thread_create(thread_t* thrd, void*(*start)(void *), void* arg);
int thread_join(thread_t thrd);
void thread_exit(void* ret);

#ifndef __APPLE__
// FIXME OS X does not have cpu_set_t
int thread_getaffinity(thread_t thrd, size_t sz, cpu_set_t* cpuset);
int thread_setaffinity(thread_t thrd, size_t sz, cpu_set_t* cpuset);
#endif

#ifdef __cplusplus
}
#endif
#endif
