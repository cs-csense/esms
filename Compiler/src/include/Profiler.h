//
// Created by Farley Lai on 11/17/15.
//

#ifndef ESMS_PROFILER_H
#define ESMS_PROFILER_H

#include <string>
#include <vector>
#include <thread>
#include <iostream>
#include <fstream>
#include "thread.h"
#include "Profiler.h"


using namespace std;
using namespace std::chrono;

template<typename S, size_t N>
class Profiler {
public:
    typedef struct {
        long long timestamp;
        S samples[N];
    } Record;

    Profiler() {
        cpu = progress = total = duration = interval = 0;
        profiling = false;
        records = NULL;
        profiler = NULL;
    }
    template<typename... strings>
    Profiler<S, N>& fields(string& f, strings&... fs);
    Profiler<S, N>& field(string& f);
    bool start(int duration, int interval, int cpu = 0);
    bool stop();
    bool save(const string &filename);
    int count();
    Record& getRecord(int r) { return records[r]; }
    S& getSample(int r, int s) { return records[r].samples[s]; }
    long long getSampleTime(int s);
    long long getSampleDuration(int s1, int s2);

protected:
    virtual bool sample() = 0;
    virtual bool startProfiling() { return true; };
    virtual bool stopProfiling() { return true; };

private:
    int cpu;
    int progress;
    int total;
    int duration;
    int interval;
    bool profiling;
    thread* profiler;
    Record* records;
    vector<string> header;

    static void profile(Profiler<S, N>* profiler) {
#ifndef __APPLE__
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        CPU_SET(cpu, &cpuset);
        if(thread_setaffinity(thread_self(), sizeof(cpu_set_t), &cpuset)) {
            fprintf(stderr, "[Profiler] Failed to set affinity to CPU[%d]: (%d) %s\n", profiler.cpu, ret, strerror(ret));
            return;
        }
#endif
        steady_clock timer;
        for(profiler->progress = 0; profiler->progress < profiler->total && profiler->profiling; profiler->progress++) {
            time_point<steady_clock> before = timer.now();
            profiler->sample();
            time_point<steady_clock> after = timer.now();
            long long ns = (after - before).count();
            cout << ns << "ns taken to sample" << ", sleep for " << (profiler->interval - ns / 1000000) << "ms" <<endl;
            thread_sleep(profiler->interval - ns / 1000000);
        }
        if(profiler->progress < profiler->total) {
            profiler->sample();
            profiler->progress++;
        }
    }

};

template<typename S, size_t N>
Profiler<S, N>& Profiler<S, N>::field(string& f) {
    header.push_back(f);
    return *this;
}

template<typename S, size_t N>
template<typename... strings>
Profiler<S, N>& Profiler<S, N>::fields(string& f, strings&... fs) {
    field(f);
    return fields(fs...);
}

template<typename S, size_t N>
bool Profiler<S, N>::start(int duration, int interval, int cpu) {
    if(profiler == NULL && startProfiling()) {
        if(records != NULL) delete records;
        this->duration = duration;
        this->interval = interval;
        this->cpu = cpu;
        progress = 0;
        total = duration * 1000 / interval;
        profiling = true;
        records = new Record[total];
        profiler = new thread(profile, this);
    }
    return profiler != NULL;
}

template<typename S, size_t N>
bool Profiler<S, N>::stop() {
    if(profiler) {
        profiling = false;
        profiler->join();
        delete profiler;
        profiler = NULL;
        return stopProfiling();
    }
    return false;
}

template<typename S, size_t N>
bool Profiler<S, N>::save(const string& filename) {
    ofstream fout(filename, ios::out);
    if(!header.empty()) {
        vector<string>::iterator it = header.begin();
        for(; it != header.end() - 1; it++)
            fout << *it << ", ";
        fout << *it << endl;
    }
    for(int i = 0; i < progress; i++) {
        for(int j = 0; j < N; j++)
            fout << records[i].timestamp << ", " << records[i].samples[j] << endl;
    }
    fout.close();
}

template<typename S, size_t N>
int Profiler<S, N>::count() {
    return progress;
}

template<typename S, size_t N>
long long Profiler<S, N>::getSampleTime(int s) {
    return s < progress ? records[s].timestamp : -1;
}

template<typename S, size_t N>
long long Profiler<S, N>::getSampleDuration(int s1, int s2) {
    return abs(records[s2].timestamp - records[s1].timestamp);
}

#endif //ESMS_PROFILER_H
