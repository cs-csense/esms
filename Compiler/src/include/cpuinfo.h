#ifndef __CPUFREQ_H
#define __CPUFREQ_H
#ifdef __cplusplus
extern "C" {
#endif

#define GOVERNOR_PERFORMANCE    "performance"
#define GOVERNOR_POWERSAVE      "powersave"
#define GOVERNOR_USERSPACE      "userspace"
#define GOVERNOR_ONDEMAND       "ondemand"
#define GOVERNOR_INTERACTIVE    "interactive"
#define GOVERNOR_CONSERVATIVE   "conservative"

typedef struct {
    int min;    // KHz
    int max;    // KHz
    int freq;   // KHz
    char governor[16];
} cpufreq_t;

typedef struct {
    int user;
    int nice;
    int sys;
    int idle;
    int iowait;
    int irq;
    int softirq;
} cpustat_t;

int nproc();
int cpufreq_online_get(int cpu);
int cpufreq_online_set(int cpu, int online);
int cpufreq_get(int cpu, cpufreq_t *info);
int cpufreq_set(int cpu, cpufreq_t *info);
void cpufreq();

int cpustat_get(cpustat_t stat[]);
int cpustat_user(cpustat_t *stat);
int cpustat_sys(cpustat_t *stat);
int cpustat_idle(cpustat_t *stat);
float cpustat_util(cpustat_t *prev, cpustat_t *cur);

#ifdef __cplusplus
}
#endif
#endif
