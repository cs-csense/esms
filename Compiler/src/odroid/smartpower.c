#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "thread.h"
#include "smartpower.h"

const char* smartpower_str(unsigned char code) {
    switch(code) {
        case REQUEST_DATA:      return "REQUEST_DATA";
        case REQUEST_STARTSTOP: return "REQUEST_STARTSTOP";
        case REQUEST_STATUS:    return "REQUEST_STATUS";
        case REQUEST_ONOFF:     return "REQUEST_ONOFF";
        case REQUEST_VERSION:   return "REQUEST_VERSION";
        default:
            return NULL;
    }
}

int smartpower_cmd(hid_device* device, unsigned char code, void* data, size_t size) {
    unsigned char cmd[65] = {0x00,};
    cmd[1] = code;
    if (hid_write(device, cmd, sizeof(cmd)) == -1) {
        fprintf(stderr, "[SmartPower] Failed to write %s\n", smartpower_str(code));
        return -1;
    }

    switch(code) {
        case REQUEST_STARTSTOP:
        case REQUEST_ONOFF:
            break;
        default: {}

            if (hid_read(device, (unsigned char*)data, size) == -1 || ((unsigned char*)data)[0] != code) {
                fprintf(stderr, "[SmartPower] Failed to read %s\n", smartpower_str(code));
                return -1;
            }
    }
    return 0;
}

int smartpower_sample(hid_device* device, sample_t* sample) {
    unsigned char buf[65] = {0};
    if(smartpower_cmd(device, REQUEST_DATA, buf, sizeof(buf)) == -1) {
        fprintf(stderr, "[SmartPower] Failed to read power sample\n");
        return -1;
    }

    char voltage[6] = {0};
    char amp[6] = {0};
    char watt[7] = {0};
    char wh[8] = {0};
    strncpy(voltage, (char*)&buf[2], 5);
    strncpy(amp, (char*)&buf[10], 5);
    strncpy(watt, (char*)&buf[17], 6);
    strncpy(wh,  (char*)&buf[24], 7);
    voltage[sizeof(voltage) - 1] = '\0';
    amp[sizeof(amp) - 1] = '\0';
    watt[sizeof(watt) - 1] = '\0';
    wh[sizeof(wh) - 1] = '\0';
    sample->sysV = atof(voltage);
    sample->sysA = atof(amp);
    sample->sysW = atof(watt);
    sample->sysWh = atof(wh);
//    fprintf(stderr, "(%s)%.3fV, (%s)%.3fA, (%s)%.3fW, (%s)%.3fwh\n", voltage, atof(voltage), amp, atof(amp), watt, atof(watt), wh, atof(wh));
//    fprintf(stderr, "[SmartPower] %.3fV, %.3fA, %.3fW, %.3fwh\n", sample->sysV, sample->sysA, sample->sysW, sample->sysWh);
    return 0;
}

hid_device* smartpower_open() {
    hid_device* device = hid_open(0x04d8, 0x003f, NULL);
    if(device) {
        unsigned char buf[65];
        if(smartpower_cmd(device, REQUEST_VERSION, buf, sizeof(buf)) == -1) {
            smartpower_close(device);
            fprintf(stderr, "[SmartPower] Failed to read version\n");
            return NULL;
        }

        bool start = false;
        bool on = false;
        if(smartpower_cmd(device, REQUEST_STATUS, buf, sizeof(buf)) == -1) {
            hid_close(device);
            fprintf(stderr, "[SmartPower] Failed to read status\n");
            return NULL;
        }
        start = (buf[1] == 0x01);
        on = (buf[2] == 0x01);
        fprintf(stderr, "[SmartPower] %s, %s\n", on ? "on" : "off", start ? "started" : "stopped");

        if(!on && smartpower_cmd(device, REQUEST_ONOFF, buf, sizeof(buf)) == -1) {
            hid_close(device);
            fprintf(stderr, "[SmartPower] Failed to turn on power\n");
            return NULL;
        }

        if(!start && smartpower_cmd(device, REQUEST_STARTSTOP, buf, sizeof(buf)) == -1) {
            hid_close(device);
            fprintf(stderr, "[SmartPower] Failed to start measuring\n");
            return NULL;
        }
    }
    return device;
}

void smartpower_close(hid_device* device) {
    if(device) {
        unsigned char buf[65];
        if(smartpower_cmd(device, REQUEST_STATUS, buf, sizeof(buf)) >= 0) {
            bool start = (buf[1] == 0x01);
            bool on = (buf[2] == 0x01);
            fprintf(stderr, "[SmartPower] %s, %s on close\n", on ? "on" : "off", start ? "started" : "stopped");
            if (start && smartpower_cmd(device, REQUEST_STARTSTOP, buf, sizeof(buf)) >= 0)
                fprintf(stderr, "[SmartPower] Stop measuring on close\n");
            else
                fprintf(stderr, "[SmartPower] Failed to stop measuring on close\n");
        } else
            fprintf(stderr, "[SmartPower] Failed to read status on close\n");
    }
    hid_close(device);
}