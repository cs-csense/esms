#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "timer.h"
#include "shell.h"
#include "thread.h"
#include "cpuinfo.h"
#include "odroid/odroid.h"
#include "odroid/odroid_profiler.h"

static sensor_t SENSOR[SENSOR_MAX] = {0};

static const char* DEV_SENSOR[] = {
    DEV_SENSOR_ARM,
    DEV_SENSOR_KFC,
    DEV_SENSOR_MEM,
    DEV_SENSOR_G3D,
};

static int sensor_open(const char *node, sensor_t *sensor) {
    if ((sensor->fd = open(node, O_RDWR)) < 0)
        perror("Open Fail:");
    return sensor->fd;
}

static int sensor_close(sensor_t *sensor) {
    if (sensor->fd > 0) 
        return close(sensor->fd);
    sensor->fd;
}

static int sensor_enable(sensor_t *sensor, unsigned int enable) {
    int ret = 0;
    if (sensor->fd > 0) {
        sensor->data.enable = enable ? 1 : 0;
        if ((ret = ioctl(sensor->fd, INA231_IOCSSTATUS, &sensor->data)) < 0)
            perror("IOCTL Error:");
        else
            thread_sleep(300);
    }
    return ret;
}

static int sensor_read_status(sensor_t *sensor) {
    int ret = 0;
    if (sensor->fd > 0) {
        if ((ret = ioctl(sensor->fd, INA231_IOCGSTATUS, &sensor->data)) < 0)
            perror("IOCTL Error");
    }
    return ret;
}

static int sensor_read(sensor_t *sensor) {
    int ret = 0;
    if (sensor->fd > 0) {
        if ((ret = ioctl(sensor->fd, INA231_IOCGREG, &sensor->data)) < 0)
            perror("IOCTL Error");
    }
    return ret;
}

int odroid_open() {
    for(int i = 0; i < SENSOR_MAX; i++) {
        if (sensor_open(DEV_SENSOR[i], &SENSOR[i]) < 0)
            return -1;
    }

    for(int i = 0; i < SENSOR_MAX; i++) {
        if (sensor_read_status(&SENSOR[i]))
            return -1;
    }

    for(int i = 0; i < SENSOR_MAX; i++) {
        if (!SENSOR[i].data.enable)
            sensor_enable(&SENSOR[i], 1);
    }
    return 0;
}

int odroid_close() {
    for(int i = 0; i < SENSOR_MAX; i++) {
        if (SENSOR[i].data.enable)
            sensor_enable(&SENSOR[i], 0);
        sensor_close(&SENSOR[i]);
    }
    return 0;
}

void odroid_sample(sample_t* sample, const int nproc) {
    sensor_t sensor[SENSOR_MAX];
    memcpy(sensor, SENSOR, sizeof(SENSOR));
    for(int i = 0; i < SENSOR_MAX; i++)
        sensor_read(&sensor[i]);

    sample->timestamp = timestamp();
    if(nproc > 0) {
        cpustat_t stat[nproc];
        cpustat_get(stat);
        for(int i = 0; i < nproc; i++) {
            sample->user[i] = cpustat_user(&stat[i]);
            sample->sys[i] = cpustat_sys(&stat[i]);
            sample->idle[i] = cpustat_idle(&stat[i]);
        }
    }
    sample->armuV = sensor[SENSOR_ARM].data.cur_uV;
    sample->armuA = sensor[SENSOR_ARM].data.cur_uA;
    sample->armuW = sensor[SENSOR_ARM].data.cur_uW;
    sample->kfcuV = sensor[SENSOR_KFC].data.cur_uV;
    sample->kfcuA = sensor[SENSOR_KFC].data.cur_uA;
    sample->kfcuW = sensor[SENSOR_KFC].data.cur_uW;
    sample->memuV = sensor[SENSOR_MEM].data.cur_uV;
    sample->memuA = sensor[SENSOR_MEM].data.cur_uA;
    sample->memuW = sensor[SENSOR_MEM].data.cur_uW;
    sample->g3duV = sensor[SENSOR_G3D].data.cur_uV;
    sample->g3duA = sensor[SENSOR_G3D].data.cur_uA;
    sample->g3duW = sensor[SENSOR_G3D].data.cur_uW;
}

void odroid_energy(sample_t* s1, sample_t* s2, energy_t* energy) {
    int us = (int)(s2->timestamp - s1->timestamp);
    energy->ms = us / 1000.0;
    for(int i = 0; i < NPROC; i++) {
        int user    = s2->user[i] - s1->user[i];
        int sys     = s2->sys[i] - s1->sys[i];
        int idle    = s2->idle[i] - s1->idle[i];
        double busy  = user + sys;
        double total = busy + idle;
        energy->util[i] = busy / total;
    }
    energy->armmWh = (s1->armuW + s2->armuW) / 2.0 * us / 360000000000.0;
    energy->kfcmWh = (s1->kfcuW + s2->kfcuW) / 2.0 * us / 360000000000.0;
    energy->memmWh = (s1->memuW + s2->memuW) / 2.0 * us / 360000000000.0;
    energy->g3dmWh = (s1->g3duW + s2->memuW) / 2.0 * us / 360000000000.0;
}

void odroid_sample_info(sample_t* sample) {
    for(int i = 0; i < NPROC; i++) 
        printf("CPU[%d] user: %d, sys: %d, idle: %d\n", i, sample->user[i], sample->sys[i], sample->idle[i]);
    printf("arm: %duV, %duA, %duW\n", sample->armuV, sample->armuA, sample->armuW);
    printf("kfc: %duV, %duA, %duW\n", sample->kfcuV, sample->kfcuA, sample->kfcuW);
    printf("mem: %duV, %duA, %duW\n", sample->memuV, sample->memuA, sample->memuW);
    printf("g3d: %duV, %duA, %duW\n", sample->g3duV, sample->g3duA, sample->g3duW);
    printf("sys: %.3fV, %.3fA, %.3fW, %.3fWh\n", sample->sysV, sample->sysA, sample->sysW, sample->sysWh);
}

void odroid_energy_info(energy_t* energy) {
    printf("CPU utils: ");
    for(int i = 0; i < NPROC; i++) {
        printf("%.2f%%", cpufreq_online_get(i) ? energy->util[i] * 100 : 0);
        printf(i == NPROC - 1 ? "\n" : ", ");
    }
    printf("arm: %.3fmWh, kfc: %.3fmWh, mem: %.3fmWh, g3d: %.3fmWh\n", energy->armmWh, energy->kfcmWh, energy->memmWh, energy->g3dmWh);
}

int odroid_gpufreq_get() {
    FILE* fp = fopen(GPUFREQ_NODE, "r");
    if (fp == NULL) {
        perror("Failed to open GPU clock");
        return 0;
    }

    char line[4];
    void* ret = fgets(line, sizeof(line), fp);
    fclose(fp);
    return atoi(line);
}

int odroid_temps(int temp[]) {
    FILE* fp = fopen(TEMP_NODE, "r");
    if(fp == NULL) {
        perror("Failed to open temperature node");
        return -1; 
    }

    int i = 0;
    char buf[24];
    while(fgets(buf, sizeof(buf), fp))
        temp[i++] = atoi(&buf[9]);

    fclose(fp);
    return 0;
}

void odroid_gpuinfo() {
    int freq = odroid_gpufreq_get();
    printf("GPU runs at %dMHz\n", freq);
}

void odroid_tempinfo() {
    int temp[5];
    odroid_temps(temp);
    for(int i = 0; i < sizeof(temp) / sizeof(int); i++) {
        switch(i) {
            case 0:
            case 1:
            case 2:
            case 3:
                printf("CPU[%d] temperature at %dC\n", i, temp[i] / 1000);
                break;
            case 4:
                printf("GPU temperature at %dC\n", temp[i] / 1000);
                break;
        }
    }
}
