#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "cpuinfo.h"
#include "thread.h"
#include "timer.h"
#include "smartpower.h"
#include "odroid/odroid.h"

typedef struct {
    hid_device *device;
    thread_t tid;
    bool running;
    int interval;   // ms
    int total;      // total samples
    int count;      // sampled count
    sample_t* samples;
} odroid_t;

static void* run(void* data) {
    odroid_t* self = (odroid_t*)data;
    if((self->device = smartpower_open())== NULL)
        fprintf(stderr, "Failed to open Smart Power");

    int total = self->total;
    int count = self->count;
    int n = nproc();
    for(; count < total && self->running; count++) {
        long long before = timestamp();
        odroid_sample(&self->samples[count], n);
        if(self->device)
            smartpower_sample(self->device, &self->samples[count]);
        thread_sleep(self->interval - (timestamp() - before) / 1000000);
    }
    if(count < total && !self->running) {
        odroid_sample(&self->samples[count++], n);
        smartpower_sample(self->device, &self->samples[count]);
    }
    self->count = count;

    if(self->device)
        smartpower_close(self->device);
    return (void*)0;
}

void* odroid_profiler_start(int duration, int interval, int cpu) {
    if(duration <= 0 || interval <= 0) {
        fprintf(stderr, "Duration and interval must be greater than zero.");
        return NULL;
    }

    // Allocate samples
    odroid_t* self = (odroid_t*)calloc(1, sizeof(odroid_t));

    self->interval = interval;
    self->total = duration * 1000 / interval + 1;
    self->count = 0;
    self->samples = (sample_t*)calloc(self->total, sizeof(sample_t));
    self->running = true;

    // Start pthread
    if(thread_create(&self->tid, run, self)) {
        perror("Failed to create the sampling thread");
        free(self);
        return NULL;
    }

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(cpu, &cpuset);
    int ret = thread_setaffinity(self->tid, sizeof(cpu_set_t), &cpuset);
    if(ret) {
        fprintf(stderr, "[odroid_profiler_start] Failed to set affinity to CPU[%d]: (%d) %s\n", cpu, ret, strerror(ret));
        self->running = false;
        pthread_join(self->tid, NULL);
        free(self);
        return NULL;
    }

    return self;
}

int odroid_profiler_stop(void* data) {
    odroid_t* self = (odroid_t*)data;
    self->running = false;
    int ret = pthread_join(self->tid, NULL);
    free(self);
    return ret;
}

int odroid_profiler_save(void* data, const char* filename) {
    odroid_t *self = (odroid_t *) data;
    FILE *fp = fopen(filename, "w");
    fprintf(fp, "timestamp, ");
    for (int i = 0; i < NPROC; i++)
        fprintf(fp, "user%d, sys%d, idle%d, ", i, i, i);
    fprintf(fp, "armuV, armuA, armuW, kfcuV, kfcuA, kfcuW, memuV, memuA, memuW, sysV, sysA, sysW, sysWh\n");
    for(int i = 0; i < self->count; i++) {
        sample_t* sample = &self->samples[i];
        fprintf(fp, "%lld, ", sample->timestamp);
        for (int n = 0; n < NPROC; n++)
            fprintf(fp, "%d, %d, %d, ", sample->user[n], sample->sys[n], sample->idle[n]);
        fprintf(fp, "%d, %d, %d, ", sample->armuV, sample->armuA, sample->armuW);
        fprintf(fp, "%d, %d, %d, ", sample->kfcuV, sample->kfcuA, sample->kfcuW);
        fprintf(fp, "%d, %d, %d, ", sample->memuV, sample->memuA, sample->memuW);
        fprintf(fp, "%d, %d, %d, ", sample->g3duV, sample->g3duA, sample->g3duW);
        fprintf(fp, "%.3f, %.3f, %.3f, %.3f\n", sample->sysV, sample->sysA, sample->sysW, sample->sysWh);
    }
    fclose(fp);
}

int odroid_profiler_samples(void* data) {
    odroid_t* self = (odroid_t*)data;
    return self->count;
}

long long odroid_profiler_duration(void* data) {
    odroid_t* self = (odroid_t*)data;
    return self->samples[self->count-1].timestamp - self->samples[0].timestamp; 
}

void odroid_profiler_energy(void* data, energy_t energies[], energy_t* energy) {
    odroid_t* self = (odroid_t*)data;
    if(self->count > 1) {
        odroid_energy(&self->samples[0], &self->samples[self->count-1], energy);
        energy->armmWh = 0;
        energy->kfcmWh = 0;
        energy->memmWh = 0;
        energy->g3dmWh = 0;
        for(int i = 0; i < self->count - 1; i++) {
            odroid_energy(&self->samples[i], &self->samples[i+1], &energies[i]);
            energy->armmWh += energies[i].armmWh;
            energy->kfcmWh += energies[i].kfcmWh;
            energy->memmWh += energies[i].memmWh;
            energy->g3dmWh += energies[i].g3dmWh;
        }
    }
}

void odroid_profiler_info(void* data, bool trace) {
    odroid_t* self = (odroid_t*)data;
    double duration = odroid_profiler_duration(self) / 1000000.0;
    printf("ODROID profiler: %s\n", self->running ? "running" : "stopped"); 
    printf("%d/%d samples profiled at interval %dms for %.2fs\n", self->count, self->total, self->interval, duration);
    if(trace) {
        for(int i = 0; i < self->count; i++) {
            for(int i = 0; i < 10; i++) {
                printf("=========== Sample[%d] at %lld ==========\n", i, self->samples[i].timestamp); 
                odroid_sample_info(&self->samples[i]);
            }
        }
    }

    if(self->count > 1) {
        energy_t energy;
        energy_t energies[self->count-1];
        odroid_profiler_energy(self, energies, &energy);
        if(trace) {
            for(int i = 0; i < self->count - 1; i++) {
                printf("=========== Energy[%d] for %.3fms ==========\n", i, energies[i].ms); 
                odroid_energy_info(&energies[i]);
            }
        }

        printf("========== Total CPU Utilizations and Energy Consumptions ==========\n");
        printf("CPU utils: ");
        for(int i = 0; i < NPROC; i++) {
            printf("%.2f%%", cpufreq_online_get(i) ? energy.util[i] * 100 : 0);
            printf(i == NPROC - 1 ? "\n" : ", ");
        }
        printf("arm %.2fmWh, kfc %.2fmWh, mem %.2fmWh, g3d %.2fmWh, sys %.3fWh\n", energy.armmWh, energy.kfcmWh, energy.memmWh, energy.g3dmWh, self->samples[self->count-1].sysWh);
    }
}
