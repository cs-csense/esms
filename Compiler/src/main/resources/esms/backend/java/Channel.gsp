/**
 * binding = [
 *      pkg: 'esms.backend.java'
 * ]
 */

${pkg ? "package $pkg;" : ''}

import java.nio.ByteBuffer;
import esms.util.Log;
public abstract class Channel {
    private class State {
        public void inputBarrier(int invocations) throws InterruptedException {}
        public void outputBarrier(int invocations) throws InterruptedException {
            // In case the input channel of the source component is be partitioned
            if(remaining() < invocations * PUSH) {
                if (PEEK > POP) flip();
                else clear();
            }
        }
        public void inputRelease() throws InterruptedException {}
        public void outputRelease() throws InterruptedException {}
    }

    private class SpinState extends State {
        private volatile int size, rem;

        @Override
        public void inputBarrier(int invocations) throws InterruptedException {
            int consumption = (invocations - 1) * POP + PEEK;
            while ((size = size()) < consumption && !Thread.currentThread().interrupted()) inputContentions++;
            if(size < consumption) throw new InterruptedException();
        }

        @Override
        public void outputBarrier(int invocations) throws InterruptedException {
            if((rem = remaining()) < invocations * PUSH) {
                while ((size = size()) > PEEK - POP && !Thread.currentThread().interrupted()) outputContentions++;
                if(size > PEEK - POP) throw new InterruptedException();
                if (PEEK > POP) flip();
                else clear();
            }
        }
    }

    private class BlockingState extends State {
        private volatile int size, rem;

        @Override
        public void inputBarrier(int invocations) throws InterruptedException {
            int consumption = (invocations - 1) * POP + PEEK;
            synchronized (instance) {
                while ((size = size()) < consumption && !Thread.currentThread().interrupted()) {
                    inputContentions++;
                    instance.wait();
                }
                if(size < consumption) throw new InterruptedException();
            }
        }

        @Override
        public void outputBarrier(int invocations) throws InterruptedException {
            if((rem = remaining()) < invocations * PUSH) {
                synchronized (instance) {
                    while ((size = size()) > PEEK - POP && !Thread.currentThread().interrupted()) {
                        outputContentions++;
                        instance.wait();
                    };
                    if(size > PEEK - POP) throw new InterruptedException();
                }
                if (PEEK > POP) flip();
                else clear();
            }
        }

        @Override
        public void inputRelease() throws InterruptedException {
            synchronized (instance) {
                instance.notify();
            }
        }

        @Override
        public void outputRelease() throws InterruptedException {
            synchronized (instance) {
                instance.notify();
            }
        }
    }


    private final Channel instance;
    private final int ID, CAPACITY, PUSH, PEEK, POP;
    private final State state;
    private final String TAG;
    private int inputContentions, outputContentions;
    private boolean partitioned;
    public Channel(int id, int capacity, int push, int peek, int pop, boolean partitioned) {
        instance = this;
        ID = id;
        TAG = "channel" + id;
        CAPACITY = capacity;
        PUSH = push;
        PEEK = peek;
        POP = pop;
        if(this.partitioned = partitioned) {
//            state = new SpinState();
            state = new BlockingState();
        } else
            state = new State();
    }

    public abstract int head();
    public abstract int tail();
    public abstract ByteBuffer headBuffer();
    public abstract ByteBuffer tailBuffer();
    public abstract Channel clear();
    public abstract Channel flip();
    public abstract int sizeInBytes();
    public int size() { return head() - tail(); }
    public int remaining() { return CAPACITY - head(); }
    public int capacity() { return CAPACITY; }
    public boolean isEmpty() { return size() == 0; }
    public void inputBarrier(int invocations) throws InterruptedException { state.inputBarrier(invocations); }
    public void outputBarrier(int invocations) throws InterruptedException { state.outputBarrier(invocations); }
    public void inputRelease() throws InterruptedException { state.inputRelease(); }
    public void outputRelease() throws InterruptedException { state.outputRelease(); }
    public void report() {
        Log.i(TAG, "Input contentions: " + inputContentions);
        Log.i(TAG, "Output contentions: " + inputContentions);
    }
}

