${pkg ? "package $pkg;" : ''}

import java.io.*;
import esms.util.Log;
//public class Scheduler extends Thread {
//    private String name;
//    public Scheduler(String name) {
//        this.name = name;
//    }
//
//    protected void work() throws InterruptedException {}
//
//    @Override
//    public void run() {
//        try {
//            work();
//            System.out.printf("%s is done\\n", name);
//        } catch (InterruptedException e) {
//            System.err.printf("%s is interrupted to stop\\n", name);
//        }
//    }
//}

public class Scheduler extends Thread {
//    private RUsage mUsage;
//    private ByteBuffer mRecords;
    private static int ID;
    private final int mId;
    public Scheduler(String name) {
        super(name + (mId = ID++));
//        mRecords = ByteBuffer.allocateDirect(1024 * 1024);
//        mUsage = new RUsage();
    }

    protected void initialization() throws InterruptedException {}
    protected void steady() throws InterruptedException {}
    public void work() throws InterruptedException {
        try {
            initialization();
            steady();
        } catch(InterruptedException e) {
            Log.w(getName(), "interrupted to stop");
        }
    }

    protected void profile(int cid, int eid) {
//        if(!mRecords.hasRemaining()) {
//            ByteBuffer prev = mRecords;
//            mRecords = ByteBuffer.allocateDirect(2 * prev.capacity());
//            prev.flip();
//            mRecords.put(prev);
//        }
//        mUsage.getusage();
//        mRecords.putInt(cid);
//        mRecords.putInt(eid);
//        mRecords.putDouble(mUsage.real());
//        mRecords.putDouble(mUsage.user());
//        mRecords.putDouble(mUsage.sys());
    }

    public void save(String path) throws IOException {
//        PrintWriter out = new PrintWriter(new File(path, getName() + ".trace"));
//        out.println("Component, Event, real, user, sys");
//        while(mRecords.hasRemaining()) {
//            out.printf("%d, %d, %f, %f, %f\\n", mRecords.getInt(), mRecords.getInt(), mRecords.getDouble(), mRecords.getDouble(), mRecords.getDouble());
//        }
//        out.close();
    }

    @Override
    public void run() {
        try {
            profile(-1, 0);
            work();
            profile(-1, 1);
            Log.i(getName(), "%s is done\\n", getName());
        } catch (InterruptedException e) {
            profile(-1, 2);
            Log.w(getName(), "%s is interrupted to stop\\n", getName());
        }
    }
}
