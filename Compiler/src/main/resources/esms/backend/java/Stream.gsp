${pkg ? "package $pkg;" : ''}

import esms.api.Loggable;

public class Stream extends Loggable {
    private final int mId;
    private final String TAG;
    public Stream(int id) {
        mId = id;
        TAG = getClass().getSimpleName() + id;
        init();
    }

    private void init() {}
    public String tag() { return TAG; }
    public void work(int invocations) throws InterruptedException{
        barrier(invocations);
        for(; invocations-- > 0;)
            doWork();
        release();
    }
    protected void doWork() {}
    public void close() {}
    protected void barrier(int invocations) throws InterruptedException {}
    protected void release() throws InterruptedException {}
}
