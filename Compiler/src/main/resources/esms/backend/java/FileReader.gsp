/**
 * binding = [
 *      package: 'esms.backend.java'
 *      type: 'int' | 'float'
 * ]
 */

${pkg ? "package $pkg;" : ''}

import java.io.*;
import esms.util.Log;
public class FileReader extends Filter {
    private Channel _in;
    private ${type.capitalize()}Channel _out;
    private String filename;
    private RandomAccessFile file;
    public FileReader(int id, Channel _in, Channel out, String filename) {
        super(id);
        this._in = _in;
        this._out = (${type.capitalize()}Channel)out;
        this.filename = filename;
        init();
    }

    private void init() {
        try {
            filename = getClass().getResource(File.separator + filename).getFile();
            file = new RandomAccessFile(filename, "r");
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    int endianFlip (int x) {
        int x0, x1, x2, x3;
        x0 = (x >> 24) & 0xff;
        x1 = (x >> 16) & 0xff;
        x2 = (x >> 8) & 0xff;
        x3 = (x >> 0) & 0xff;
        return (x0 | (x1 << 8) | (x2 << 16) | (x3 << 24));
    }

    short endianFlip (short x) {
        int x0, x1, x2, x3;
        x0 = (x >> 8) & 0xff;
        x1 = (x >> 0) & 0xff;
        return (short)(x0 | (x1 << 8));
    }

    $type read() {
        while(true) {
            try {
                return ${type == 'int' ? 'file.readInt' : 'Float.intBitsToFloat(endianFlip(file.readInt()))'};
            } catch(EOFException e) {
                try {
                    file.seek(0);
                } catch(IOException ex) {
                    throw new RuntimeException(ex);
                }
                continue;
            } catch(IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void close(){
        try {
            file.close();
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doWork() {
        _out.push(read());
    }

    @Override
    protected void barrier(int invocations) throws InterruptedException {
        _out.outputBarrier(invocations);
    }

    @Override
    protected void release() throws InterruptedException {
        _out.outputRelease();
    }
}
