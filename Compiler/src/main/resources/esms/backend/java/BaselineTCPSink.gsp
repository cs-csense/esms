/**
 * binding = [
 *      package: 'esms.backend.java'
 *      type: 'int' | 'float'
 * ]
 */

${pkg ? "package $pkg;" : ''}

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import esms.runtime.Program;
public class BaselineTCPSink extends Filter {
    private ${type.capitalize()}Channel _in;
    private Channel _out;
    private ByteBuffer server;
    private String host;
    private int port;
    private int size;
    public BaselineTCPSink(int id, Channel _in, Channel _out, String host, int port, int count) {
        super(id);
        this._in = (${type.capitalize()}Channel)_in;
        this._out = _out;
        this.host = Program.getRemote();
        if(this.host == null || this.host.length() == 0)
            this.host = host;
        this.port = port;
        this.size = count * ${ type == 'char' ? 1 : 4 };
        init();
    }

    private void init() {
        server = ByteBuffer.allocate(size);
//        try {
//            server = SocketChannel.open(new InetSocketAddress(host, port));
//        } catch(IOException e) {
//            throw new RuntimeException(e);
//        }
    }

    public void close(){
        server.clear();
//        try {
//            server.close();
//        } catch(IOException e) {
//            throw new RuntimeException(e);
//        }
    }

    protected void doWork() {
        ByteBuffer buffer = _in.tailBuffer();
        int limit = buffer.limit();
        buffer.limit(buffer.position() + size);
        server.put(buffer);
        buffer.limit(limit);
        server.clear();
//        try {
//            ByteBuffer buffer = _in.tailBuffer();
//            int limit = buffer.limit();
//            buffer.limit(buffer.position() + size);
//            server.write(buffer);
//            buffer.limit(limit);
//        } catch(IOException e) {
//            throw new RuntimeException(e);
//        }
    }

    @Override
    protected void barrier(int invocations) throws InterruptedException {
        if(_in != null) _in.inputBarrier(invocations);
        if(_out != null) _out.outputBarrier(invocations);
    }

    @Override
    protected void release() throws InterruptedException {
        if(_in != null) _in.inputRelease();
        if(_out != null) _out.outputRelease();
    }
}
