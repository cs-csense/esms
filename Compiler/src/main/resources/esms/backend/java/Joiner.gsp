${pkg ? "package $pkg;" : ''}

public class Joiner extends Stream {
    private interface State {
        public void doWork();
    }

    private final Channel[] ins;
    private final Channel out;
    private final State state;
    public Joiner(int id, final IntChannel[] ins, final IntChannel out, final int[] weights) {
        super(id);
        this.ins = ins;
        this.out = out;
        if(weights.length == 1) {
            state = new State() {
                int weight = weights[0];
                public void doWork() {
                    for(IntChannel _in: ins) {
                        for(int i = 0; i < weight; i++)
                            out.push(_in.pop());
                    }
                }
            };
        } else {
            state = new State() {
                public void doWork() {
                    for(int i = 0; i < ins.length; i++) {
                        IntChannel _in = ins[i];
                        int weight = weights[i];
                        for(int w = 0; w < weight; w++)
                            out.push(_in.pop());
                    }
                }
            };
        }
    }

    public Joiner(int id, final FloatChannel[] ins, final FloatChannel out, final int[] weights) {
        super(id);
        this.ins = ins;
        this.out = out;
        if(weights.length == 1) {
            state = new State() {
                int weight = weights[0];
                public void doWork() {
                    for(FloatChannel _in: ins) {
                        for(int i = 0; i < weight; i++)
                            out.push(_in.pop());
                    }
                }
            };
        } else {
            state = new State() {
                public void doWork() {
                    for(int i = 0; i < ins.length; i++) {
                        FloatChannel _in = ins[i];
                        int weight = weights[i];
                        for(int w = 0; w < weight; w++)
                            out.push(_in.pop());
                    }
                }
            };
        }
    }

    @Override
    protected void doWork() {
        state.doWork();
    }

    @Override
    protected void barrier(int invocations) throws InterruptedException {
        for(Channel _in: ins)
            _in.inputBarrier(invocations);
        out.outputBarrier(invocations);
    }

    @Override
    protected void release() throws InterruptedException {
        for(Channel _in: ins)
            _in.inputRelease();
        out.outputRelease();
    }
}
