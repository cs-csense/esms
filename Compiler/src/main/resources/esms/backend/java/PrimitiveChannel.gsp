/**
 * binding = [
 *      package: 'esms.backend.java'
 *      type: 'int' | 'float'
 * ]
 */

${pkg ? "package $pkg;" : ''}

import java.nio.ByteBuffer;

public class ${type.capitalize()}Channel extends Channel {
    private final ByteBuffer head;
    private final ByteBuffer tail;
    private final int bytes;
    private final int shift;
    public ${type.capitalize()}Channel(int id, int capacity, int push, int peek, int pop, boolean partitioned) {
        super(id, capacity, push, peek, pop, partitioned);
        head = ByteBuffer.allocateDirect(4 * capacity);
        tail = head.duplicate();
        bytes = ${ type == 'char' ? 1 : 4 };
        shift = bytes / 2;
    }

    public $type peek(int offset) {
        return tail.get${type.capitalize()}(tail.position() + (offset << shift));
    }

    public $type pop() {
        return tail.get${type.capitalize()}();
    }

    public void push($type val) {
        head.put${type.capitalize()}(val);
    }

    @Override
    public int head() {
        return head.position() >> shift;
    }

    @Override
    public int tail() { return tail.position() >> shift; }

    @Override
    public ByteBuffer headBuffer() {
        return head;
    }

    @Override
    public ByteBuffer tailBuffer() {
        return tail;
    }

    @Override
    public int sizeInBytes() {
        return head.position() - tail.position();
    }

    @Override
    public ${type.capitalize()}Channel clear() {
        head.clear();
        tail.clear();
        return this;
    }

    @Override
    public ${type.capitalize()}Channel flip() {
        head.limit(head.position());
        head.position(tail.position());
        head.compact();
        tail.position(0);
        return this;
    }
}
