${pkg ? "package $pkg;" : ''}

public class Splitter extends Stream {
    private interface State {
        public void doWork();
    }

    private final Channel _in;
    private final Channel[] _outs;
    private final State state;
    public Splitter(int id, final IntChannel _in, final IntChannel[] _outs, final int[] weights) {
        super(id);
        this._in = _in;
        this._outs = _outs;
        if(weights == null || weights.length == 0) {
            state = new State() {
                public void doWork() {
                    int v = _in.pop();
                    for(IntChannel out: _outs)
                        out.push(v);
                }
            };
        } else if(weights.length == 1) {
            state = new State() {
                int weight = weights[0];
                public void doWork() {
                    for(IntChannel out: _outs) {
                        for(int i = 0; i < weight; i++)
                            out.push(_in.pop());
                    }
                }
            };
        } else {
            state = new State() {
                public void doWork() {
                    for(int i = 0; i < _outs.length; i++) {
                        IntChannel out = _outs[i];
                        int weight = weights[i];
                        for(int w = 0; w < weight; w++)
                            out.push(_in.pop());
                    }
                }
            };
        }
    }

    public Splitter(int id, final FloatChannel _in, final FloatChannel[] _outs, final int[] weights) {
        super(id);
        this._in = _in;
        this._outs = _outs;
        if(weights == null || weights.length == 0) {
            state = new State() {
                public void doWork() {
                    float v = _in.pop();
                    for(FloatChannel out: _outs)
                        out.push(v);
                }
            };
        } else if(weights.length == 1) {
            state = new State() {
                int weight = weights[0];
                public void doWork() {
                    for(FloatChannel out: _outs) {
                        for(int i = 0; i < weight; i++)
                            out.push(_in.pop());
                    }
                }
            };
        } else {
            state = new State() {
                public void doWork() {
                    for(int i = 0; i < _outs.length; i++) {
                        FloatChannel out = _outs[i];
                        int weight = weights[i];
                        for(int w = 0; w < weight; w++)
                            out.push(_in.pop());
                    }
                }
            };
        }
    }

    @Override
    protected void doWork() {
        state.doWork();
    }

    @Override
    protected void barrier(int invocations) throws InterruptedException {
        _in.inputBarrier(invocations);
        for(Channel out: _outs)
            out.outputBarrier(invocations);
    }

    @Override
    protected void release() throws InterruptedException {
        _in.inputRelease();
        for(Channel out: _outs)
            out.outputRelease();
    }
}
