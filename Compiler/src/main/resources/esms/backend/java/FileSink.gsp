/**
 * binding = [
 *      package: 'esms.backend.java'
 *      type: 'int' | 'float'
 * ]
 */

${pkg ? "package $pkg;" : ''}

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import esms.runtime.Program;
public class FileSink extends Filter {
    private ${type.capitalize()}Channel _in;
    private Channel _out;
    private String filename;
    private FileChannel file;
    private int size;
    public FileSink(int id, Channel _in, Channel _out, String filename, int count) {
        super(id);
        this._in = (${type.capitalize()}Channel)_in;
        this._out = _out;
        this.filename = Program.getTracePath() + File.separator + filename;
        this.size = count * ${ type == 'char' ? 1 : 4 };
        init();
    }

    private void init() {
        try {
            file = new RandomAccessFile(filename, "rw").getChannel();
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void close(){
        try {
            file.close();
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected void doWork() {
        try {
            ByteBuffer buffer = _in.tailBuffer();
            int limit = buffer.limit();
            buffer.limit(buffer.position() + size);
            file.write(buffer);
            buffer.limit(limit);
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void barrier(int invocations) throws InterruptedException {
        _in.inputBarrier(invocations);
    }

    @Override
    protected void release() throws InterruptedException {
        _in.inputRelease();
    }
}
