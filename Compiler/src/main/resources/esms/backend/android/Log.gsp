${pkg ? "package $pkg;" : ''}

${imports ? "$imports;" : ''}
public class Log {
    public enum Severity {
        ASSERT(7) {
            @Override
            public String toString() { return "A/"; }
        }, ERROR(6) {
            @Override
            public String toString() { return "E/"; }
        }, WARN(5) {
            @Override
            public String toString() { return "W/"; }
        }, INFO(4) {
            @Override
            public String toString() { return "I/"; }
        }, DEBUG(3) {
            @Override
            public String toString() { return "D/"; }
        }, VERBOSE(2) {
            @Override
            public String toString() { return "V/"; }
        };

        private final int priority;
        Severity(int priority) {
            this.priority = priority;
        }

        public int priority() {
            return priority;
        }
    }
    private static Severity level = Severity.INFO;

    public static Severity level() { return level; }

    public static Severity level(Severity severity) { return level = severity; }

    public static boolean isLoggable(String tag, Severity severity) { return severity.compareTo(level) >= 0; }

    private static void printf(Severity severity, String tag, String fmt, Object... args) {
        if(fmt.charAt(fmt.length() - 1) == '\\n') fmt = fmt.substring(0, fmt.length() - 1);
        android.util.Log.println(severity.priority(), tag, String.format(fmt, args));
    }

    private static void println(Severity severity, String tag, String line) {
        android.util.Log.println(severity.priority(), tag, line);
    }

    public static void e(String tag, Object line) {
        println(Severity.ERROR, tag, line.toString());
    }

    public static void e(String tag, String fmt, Object... args) {
        printf(Severity.ERROR, tag, fmt, args);
    }

    public static void w(String tag, Object line) {
        if (isLoggable(tag, Severity.WARN))
            println(Severity.WARN, tag, line.toString());
    }

    public static void w(String tag, String fmt, Object... args) {
        if (isLoggable(tag, Severity.WARN))
            printf(Severity.WARN, tag, fmt, args);
    }

    public static void i(String tag, Object line) {
        if (isLoggable(tag, Severity.INFO))
            println(Severity.INFO, tag, line == null ? "" : line.toString());
    }

    public static void i(String tag, String fmt, Object... args) {
        if (isLoggable(tag, Severity.INFO))
            printf(Severity.INFO, tag, fmt, args);
    }

    public static void d(String tag, Object line) {
        if (isLoggable(tag, Severity.DEBUG))
            println(Severity.DEBUG, tag, line.toString());
    }

    public static void d(String tag, String fmt, Object... args) {
        if (isLoggable(tag, Severity.DEBUG))
            printf(Severity.DEBUG, tag, fmt, args);
    }

    public static void v(String tag, Object line) {
        if (isLoggable(tag, Severity.VERBOSE))
            println(Severity.VERBOSE, tag, line.toString());
    }

    public static void v(String tag, String fmt, Object... args) {
        if (isLoggable(tag, Severity.VERBOSE))
            printf(Severity.VERBOSE, tag, fmt, args);
    }
}
