package esms.runtime;

import esms.profiling.Profiler
import esms.util.Log;
public class Scheduler extends Thread {
    private static int ID;
    private int mId;
    private int mPeriods;
    private int mIterations;
    private final Profiler mProfiler;
    public Scheduler(String name, int periods, int iterations, Profiler profiler) {
        super(name + (mId = ID++));
        mPeriods = periods;
        mIterations = iterations;
        mProfiler = profiler;
    }

    protected void initialization() throws InterruptedException {}
    protected void steadyPeriod() throws InterruptedException {}
    protected void remainingIteration() throws InterruptedException {}
    private final void steady(int periods) throws InterruptedException {
        for(int i = 0; i < periods; i++)
            steadyPeriod();
    }
    private final void remaining(int iterations) throws InterruptedException {
        for(int i = 0; i < iterations; i++)
            remainingIteration();
    }

    protected void profile(int cid, int eid) {
        if(mProfiler != null) mProfiler.profile(cid, eid);
    }

    @Override
    public void run() {
        try {
            profile(-1, 0);
            initialization();
            profile(-1, 1);
            steady(mPeriods);
            profile(-1, 2);
            remaining(mIterations);
            profile(-1, 3);
            Log.i("%s is done\\n", getName());
        } catch(InterruptedException e) {
            profile(-1, 3);
            Log.w(getName(), "interrupted to stop");
        }

        try {
            mProfiler.save(getName() + ".trace");
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }
}