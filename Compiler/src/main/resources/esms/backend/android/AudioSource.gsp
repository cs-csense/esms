${pkg ? "package $pkg;" : ''}

import android.os.Build.VERSION;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import esms.util.Log;
public class AudioSource extends Filter {
    public static boolean PCM_FLOAT_SUPPORT = VERSION.SDK_INT >= 23;
    public enum SRC {
        MIC,
        VOICE_RECOGNITION;
        public int getValue() {
            switch(this) {
                case MIC: return MediaRecorder.AudioSource.MIC;
                default:
                    return MediaRecorder.AudioSource.VOICE_RECOGNITION;
            }
        }
    }

    public enum Rate {
        SAMPLE_RATE_8K,
        SAMPLE_RATE_16K,
        SAMPLE_RATE_44K;
        public int getValue() {
            switch(this) {
                case SAMPLE_RATE_8K: return 8000;
                case SAMPLE_RATE_44K: return 44100;
                default:
                    return 16000;
            }
        }
    }

    public enum ChannelConfig {
        MONO, STEREO;
        public int getValue() {
            switch(this) {
                case STEREO: return AudioFormat.CHANNEL_IN_STEREO;
                default: return AudioFormat.CHANNEL_IN_MONO;
            }
        }
    }

    public enum Encoding {
        PCM_16BIT,
        PCM_FLOAT;
        public int getValue() {
            switch(this) {
                case PCM_16BIT: return AudioFormat.ENCODING_PCM_16BIT;
                default: return AudioFormat.ENCODING_PCM_FLOAT;
            }
        }
    }

    private Channel _in;
    private ${type.capitalize()}Channel _out;
    private int size;

    private AudioRecord mRecorder;
    private SRC mSource;
    private Rate mRate;
    private ChannelConfig mChannelConfig;
    private Encoding mEncoding;
    private ByteBuffer mBuffer;

    public AudioSource(int id, Channel _in, Channel _out, int rate, int count) {
        super(id);
        mSource = SRC.VOICE_RECOGNITION;
        mRate = rate == 16000 ? Rate.SAMPLE_RATE_16K : rate == 44100 ? Rate.SAMPLE_RATE_44K : Rate.SAMPLE_RATE_8K;
        mChannelConfig = ChannelConfig.MONO;
        mEncoding = Encoding.PCM_FLOAT;
        this._out = (${type.capitalize()}Channel)_out;
        this.size = count * ${ type == 'char' ? 1 : type == 'short' ? 2 : 4 };
        if(!open())
            throw new RuntimeException("Failed to open AudioRecord");
    }

    private boolean start() {
        mRecorder.startRecording();
        return true;
    }

    private boolean stop() {
        mRecorder.stop();
        return true;
    }

    private boolean open() {
        if(PCM_FLOAT_SUPPORT) Log.i(tag(), "PCM_FLOAT is supported");
        if(!PCM_FLOAT_SUPPORT && mEncoding == Encoding.PCM_FLOAT) {
            // enforce PCM_16BIT before M if PCM_FLOAT
            mRecorder = new AudioRecord(mSource.getValue(), mRate.getValue(), mChannelConfig.getValue(), Encoding.PCM_16BIT.getValue(), size / 2);
            mBuffer = ByteBuffer.allocateDirect(size / 2);
            mBuffer.order(ByteOrder.nativeOrder());
            Log.i(tag(), "set internal buffer to native order: " + ByteOrder.nativeOrder());
        } else {
            mRecorder = new AudioRecord(mSource.getValue(), mRate.getValue(), mChannelConfig.getValue(), mEncoding.getValue(), size);
            _out.headBuffer().order(ByteOrder.nativeOrder());
            _out.tailBuffer().order(ByteOrder.nativeOrder());
            Log.i(tag(), "set output channel to native order: " + ByteOrder.nativeOrder());
        }
        return mRecorder != null ? start() : false;
    }

    public void close() {
        stop();
        mRecorder.release();
        mRecorder = null;
    }

    @Override
    protected void doWork() {
        if(!PCM_FLOAT_SUPPORT && mEncoding == Encoding.PCM_FLOAT) {
            int bytes = mRecorder.read(mBuffer, mBuffer.capacity());
            if(bytes < mBuffer.capacity())
                throw new RuntimeException(String.format("Failed to read enough bytes %d < %d\\n", bytes, mBuffer.capacity()));
            while (mBuffer.hasRemaining())
                _out.push((float)mBuffer.getShort() / Short.MAX_VALUE);
            mBuffer.clear();
        } else {
            ByteBuffer buffer = _out.headBuffer();
            int bytes = mRecorder.read(buffer, size);
            if(bytes < size)
                throw new RuntimeException(String.format("Failed to read enough bytes %d < %d\\n", bytes, size));
            buffer.position(buffer.position() + size);
        }
    }

    @Override
    protected void barrier(int invocations) throws InterruptedException {
        _out.outputBarrier(invocations);
    }

    @Override
    protected void release() throws InterruptedException {
        _out.outputRelease();
    }
}
