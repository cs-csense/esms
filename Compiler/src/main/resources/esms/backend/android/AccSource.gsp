${pkg ? "package $pkg;" : ''}

import java.nio.ByteBuffer;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import esms.runtime.Environment;
import esms.util.Log;

public class AccSource extends Filter implements SensorEventListener {
    private ${type.capitalize()}Channel _out;
    private SensorManager _sm;
    private int _size;
    private ByteBuffer _buffer;

    public AccSource(int id, Channel _in, Channel _out, int count) {
        super(id);
        this._out = (${type.capitalize()}Channel)_out;
        _size = 3 * count * ${ type == 'char' ? 1 : type == 'short' ? 2 : 4 }; // X, Y, Z
        _buffer = ByteBuffer.allocate(_size);
        Context ctx = Environment.get("Context");
        _sm = (SensorManager)ctx.getSystemService(Context.SENSOR_SERVICE);
        _sm.registerListener(this, _sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void close() {
        _sm.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int pos = 0;
        synchronized (_buffer) {
            while(pos < event.values.length) {
                //Log.i(tag(), "onSensorChanged():0 buffer pos=%d, remaining=%d, samples left=%d\\n", _buffer.position(), _buffer.remaining(), event.values.length - pos);
                while (!_buffer.hasRemaining()) {
                    _buffer.notifyAll();
                    try {
                        _buffer.wait();
                    } catch(InterruptedException e) {}
                };

                //Log.i(tag(), "onSensorChanged():1 buffer pos=%d, remaining=%d, samples left=%d\\n", _buffer.position(), _buffer.remaining(), event.values.length - pos);
                int count = Math.min(_buffer.remaining() / 4, event.values.length - pos);
                for (int i = 0; i < count; i++)
                    _buffer.putFloat(event.values[pos++]);
                //Log.i(tag(), "onSensorChanged():2 buffer pos=%d, remaining=%d, samples left=%d\\n", _buffer.position(), _buffer.remaining(), event.values.length - pos);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    @Override
    protected void doWork() {
        synchronized(_buffer) {
            //Log.i(tag(), "doWork():0 buffer pos=%d, remaining=%d\\n", _buffer.position(), _buffer.remaining());
            while(_buffer.hasRemaining()) {
                try {
                    _buffer.wait();
                } catch(InterruptedException e) {}
            };
            //Log.i(tag(), "doWork():1 buffer pos=%d, remaining=%d\\n", _buffer.position(), _buffer.remaining());
            _buffer.flip();
            while(_buffer.hasRemaining())_out.push(_buffer.getFloat());
            _buffer.clear();
            _buffer.notifyAll();
            //Log.i(tag(), "doWork():2 buffer pos=%d, remaining=%d\\n", _buffer.position(), _buffer.remaining());
        }
    }

    @Override
    protected void barrier(int invocations) throws InterruptedException {
        _out.outputBarrier(invocations);
    }

    @Override
    protected void release() throws InterruptedException {
        _out.outputRelease();
    }
}
