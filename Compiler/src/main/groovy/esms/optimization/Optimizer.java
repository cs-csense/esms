package esms.optimization;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import esms.api.Channel;
import esms.api.Component;
import esms.api.Configuration;
import esms.core.Operation;
import esms.api.SFG;
import esms.core.Superframe;
import esms.core.Window;
import esms.streamit.Filter;
import esms.types.Strategy;
import esms.util.Formatter;
import esms.util.Log;

/**
 * Created by farleylai on 2/23/15.
 */
public abstract class Optimizer {
    private final String TAG = getClass().getSimpleName();
    private static Strategy OPT = Strategy.IN_PLACE;
    private static int MAX_WINDOW_GROUPS = 32;
    public static Strategy opt() { return OPT; }
    public static Strategy opt(Strategy strategy) { return OPT = strategy; }
    public static int maxWindowGroups() { return MAX_WINDOW_GROUPS; }
    public static int maxWindowGroups(int groups) { return MAX_WINDOW_GROUPS = groups; }
    public abstract Optimizer rebuild(SFG sfg);
    protected abstract Optimizer append(SFG sfg, Channel out, Channel in, int invocation, int size, int sampleSize);
    protected abstract Optimizer create(SFG sfg, Channel out, Channel in, int invocation, int size, int sampleSize);
    protected abstract Optimizer insert(SFG sfg, Channel out, Channel in, int invocation, int pos, int size, int sampleSize);
    protected abstract Optimizer update(SFG sfg, Channel out, Channel in, int invocation, int pos, int size);
    protected abstract Optimizer pass(SFG sfg, Channel out, Channel in, int invocation, int pos, int size);
    protected abstract Optimizer window(SFG sfg, Channel ch, int invocations);
//    protected abstract Optimizer window(SFG sfg, Component c);
    protected Optimizer matchWindowGroups(SFG sfg) {
        Log.i(TAG, Formatter.title("Matching Window Groups"));
        // TODO matching MIMO in general
        // match window getLargerSourceWindowGroups
        for(Component c: sfg.components()) {
            if(sfg.isProductive(c)) {
                Channel in = sfg.getInputChannel(c, 0);
                Channel out = sfg.getOutputChannel(c, 0);
                Channel.State sin = sfg.getState(in);
                Channel.State sout = sfg.getState(out);
                List<List<Window>> sinkGrps = sin == null ? null : sin.getSinkWindowGroups();
                List<List<Window>> srcGrps = sout == null ? null : sout.getSourceWindowGroups();
                // Reduce code complexity by indrect addressing
                if(sfg.inDegrees(c) > 0 && sinkGrps.size() > MAX_WINDOW_GROUPS) {
                    Log.d(TAG, "Indirect addressing sink windows of %s for too many window groups %d > %d\n", c, sinkGrps.size(), MAX_WINDOW_GROUPS);
                    sinkGrps = sin.getSinkIndirectWindowGroups(c);
//                    for(List<Window> g: sinkGrps) {
//                        for(Window sink: g) {
//                            Log.i("Indirect sink widnow: \n%s\n", sink);
//                        }
//                    }
                } else if(!((Filter)c).isSafe()) {
                    Log.d("Indirect addressing sink windows of %s for unsafe stream operatons\n", c);
                    sinkGrps = sin.getSinkIndirectWindowGroups(c);
                }

                if(sfg.outDegrees(c) > 0 && srcGrps.size() > MAX_WINDOW_GROUPS) {
                    Log.d(TAG, "Indirect addressing source windows of %s for too many window groups %d > %d\n", c, srcGrps.size(), MAX_WINDOW_GROUPS);
                    srcGrps = sout.getSourceIndirectWindowGroups(c);
                }

                if(!sfg.isSource(c) && !sfg.isSink(c)) {
                    Log.d("Matching window groups of %s\n", c);
                    List<List<Window>> newSinkGrps = new ArrayList<>();
                    List<List<Window>> newSrcGrps = new ArrayList<>();
//                    Log.d("%s initial sink window groups:\n%s\n", c, sinkGrps);
//                    Log.d("%s initial src window groups:\n%s\n", c, srcGrps);
                    int i = 0, j = 0;
                    while(i < sinkGrps.size() && j < srcGrps.size()) {
                        List<Window> sinkGrp = sinkGrps.get(i);
                        List<Window> srcGrp = srcGrps.get(j);
                        if(sinkGrp.size() < srcGrp.size()) {
                            List<Window> group = new ArrayList<>(srcGrp.subList(0, sinkGrp.size()));
                            newSinkGrps.add(sinkGrp);
                            newSrcGrps.add(group);
                            srcGrp.removeAll(group);
                            i++;
                        } else if(sinkGrp.size() > srcGrp.size()) {
                            List<Window> group = new ArrayList<>(sinkGrp.subList(0, srcGrp.size()));
                            newSrcGrps.add(srcGrp);
                            newSinkGrps.add(group);
                            sinkGrp.removeAll(group);
                            j++;
                        } else {
                            newSinkGrps.add(sinkGrp);
                            newSrcGrps.add(srcGrp);
                            i++;
                            j++;
                        }
                    }
                    sinkGrps.clear();
                    srcGrps.clear();
                    sinkGrps.addAll(newSinkGrps);
                    srcGrps.addAll(newSrcGrps);
//                    Log.d("%s grouped sink windows:\n%s\n", c, sinkGrps);
//                    Log.d("%s grouped src windows:\n%s\n", c, srcGrps);
                }
            }
        }
        return this;
    }

    public final Optimizer join(SFG sfg, Component c) {
//        Superframe sf = sfg.getSuperframe(sfg.getInputChannel(c, 0));
        Superframe sf = new Superframe();
        Set<Superframe> removes = new HashSet<>();

        // FIXME Superframe offset and limit do not cover the last output source layout
        // channels are added in schedule order

        for(Channel in: sfg.getInputChannels(c)) {
            Superframe f = sfg.getSuperframe(in);
            if (!removes.contains(f)) {
                int offset = sfg.offset(f);
                if(offset > 0) {
                    Log.d("Superframe of %s has offset %d at %s\n", sfg.toString(in), offset, c);
                    System.exit(-1);
                }
                sfg.shift(f, sfg.limit(sf) - offset);
                sf.add(f);
                removes.add(f);
            }
        }

        // push forward the superframe while removing any input channels from future
        for(int i = 0; i < sfg.inDegrees(c); i++)
//            sf.push(sfg.getOutputChannel(c, 0), sfg.getInputChannel(c, i), false);
            sf.push(sfg.getOutputChannel(c, 0), sfg.getInputChannel(c, i));//false);
        sfg.removeAll(removes);
        sfg.add(sf);
        return this;
    }

    /**
     * Invoke a scheduled component with a specified memory sizeInBytes decision.
     *
     * @param c          the component to invoke
     * @param invocation the order of invocation
     * @param cfg        true to sizeInBytes a new superframe to write or false to write in place.
     * @return
     */
    public final Optimizer invoke(SFG sfg, Component c, int invocation, Configuration cfg) {
        if (sfg.isSink(c)) {
            // push the superframe to the last channel
            sfg.push(null, sfg.getInputChannel(c, 0), false);
            return this;
        }

        for(Channel out: sfg.getOutputChannels(c)) {
            if (invocation == 0 && out != null) {
                Channel in = sfg.getInputChannel(c, 0);
                sfg.getState(out).resetPushIndex();
                if (sfg.isSource(c) || cfg.decision(c, out)) {
                    // allocate a superframe
                    sfg.push(out, in, true);
                } else if (sfg.isFilter(c)) {
                    // pass the input superframe as future if the following filter is not scheduled next
                    // TODO non-linear order => upstream splitters and future
                    sfg.push(out, in, false);
                } else if (sfg.isSplitter(c)) {
                    // pass the input superframe as future
                    sfg.push(out, in, false);
                } else if (sfg.isJoiner(c)) {
                    // merge input superframes, StreamIt may not need to join superframes
                    join(sfg, c);
                } else
                    throw new RuntimeException("Unsupported non-StreamIt component " + c);
            }
            apply(sfg, c, out, sfg.getOperations(c, out), invocation);
        }

        if(sfg.isFilter(c) && !sfg.isSource(c)) {
            Channel in = sfg.getInputChannel(c, 0);
            Channel out = sfg.getOutputChannel(c, 0);
            int n = sfg.getState(in).PEEK - sfg.getState(out).PUSH;
//            Log.i("releasing %d samples not passed from %s for invocation %d\n", n, sfg.toString(in), invocation);
            release(sfg, c, in, invocation, n);
        }
        return this;
    }

    public Optimizer apply(SFG sfg, Component c, Channel out, List<Operation> operations, int invocation) {
        Channel in = sfg.getInputChannel(c, 0);
        if (out != null) {
            for (Operation operation : operations) {
                Log.d(TAG, "%s applies %s %s\n", c, operation, String.format("to %s", sfg.toString(out)));
                switch (operation.op()) {
                    case APPEND: {
                        append(sfg, out, in, invocation, operation.size(), operation.sampleSize());
                        break;
                    }
                    case CREATE: {
                        create(sfg, out, in, invocation, operation.size(), operation.sampleSize());
                        break;
                    }
                    case INSERT: {
                        insert(sfg, out, in, invocation, operation.pos(), operation.size(), operation.sampleSize());
                        break;
                    }
                    case UPDATE: {
                        update(sfg, out, in, invocation, operation.pos(), operation.size());
                        break;
                    }
                    case PASS: {
                        pass(sfg, out, sfg.getInputChannel(c, operation.src().get()), invocation, operation.pos(), operation.size());
                        break;
                    }
                }
            }
        }
        return this;
    }

    protected Optimizer release(SFG sfg, Component c, Channel in, int invocation, int n) { return this; }
}
