package esms.optimization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import esms.api.Channel;
import esms.api.Component;
import esms.core.Range;
import esms.api.SFG;
import esms.math.mlp.Program;
import esms.streamit.Splitter;
import esms.util.Formatter;
import esms.util.Log;

/**
 * Live Sample Management
 * 1. name -> refCount
 * 2. alive -> refCount > 0
 *
 * Created by farleylai on 2/23/15.
 */
public class ILPOptimizer extends Optimizer {
    private static class Sample implements Comparable<Sample> {
        private Component mOriginator;
        private Component mWriter;
        private int mSeq;
        private int mRef;
        private int mSize;

        public Sample(Sample s) {
            mOriginator = s.mOriginator;
            mWriter = s.mWriter;
            mSeq = s.mSeq;
            mRef = s.mRef;
            mSize = s.mSize;
        }

        public Sample(Component c, int seq, int size) {
            mOriginator = mWriter = c;
            mSeq = seq;
            mRef = 1;
            mSize = size;
        }

        public Sample(Component c, int seq) {
            this(c, seq, 4);
        }

        public Sample writer(Component c) {
            mWriter = c;
            return this;
        }

        public Sample seq(int seq) {
            mSeq = seq;
            return this;
        }

        public Component originator() {
            return mOriginator;
        }

        public Component writer() {
            return mWriter;
        }

        public String name() {
            return String.format("%s_%04d", mOriginator.name(), mSeq);
        }

        public boolean isAlive() {
            return mRef > 0;
        }

        public int seq() {
            return mSeq;
        }

        public int size() {
            return mSize;
        }

        public int refCount() {
            return mRef;
        }

        public Sample ref() {
            mRef++;
            return this;
        }

        public Sample deRef() {
            if(mRef-- == 0)
                throw new RuntimeException("Failed to decrement ref count of live sample " + name());
            return this;
        }

        @Override
        public boolean equals(Object s) {
            //return s instanceof LiveSample && compareTo((LiveSample)s) == 0;
            return s == this;
        }

        @Override
        public int compareTo(Sample o) {
            int ret = mOriginator.name().compareTo(o.mOriginator.name());
            return ret == 0 ? Integer.compare(mSeq, o.mSeq) : ret;
        }

        @Override
        public String toString() {
            return name();
        }
    }

    private class Live {
        private Map<Component, Integer> mAllocation;
        private Set<Sample> mSamples;

        public Live() {
            mAllocation = new HashMap<>();
            mSamples = new HashSet<>();
        }

        public Set<Sample> samples() {
            return Collections.unmodifiableSet(mSamples);
        }

        public String[] vars() {
            String[] vars = new String[mSamples.size()];
            int i = 0;
            for(Sample s: mLive.samples())
                vars[i++] = s.name();
            return vars;
        }

        public int size() {
            return mSamples.size();
        }

        public Sample add(Component c) {
            int i = mAllocation.getOrDefault(c, 0);
            Sample s = new Sample(c, i++);
            mAllocation.put(c, i);
            // New sample constraints
            int S = 0;
            for(Sample sample: mSamples) {
                if(sample.isAlive()) {
                    mILP.NE(Program.diff(s.name(), sample.name()), 0);
                    S++;
                }
            }
            mSamples.add(s);
            MaxS = Math.max(MaxS, S);
            return s;
        }

//        public Live add(String prefix, int n) {
//            int i = mAllocation.getOrDefault(prefix, 0);
//            while(i < n)
//                mSamples.add(new LiveSample(prefix, i++));
//            mAllocation.put(prefix, i);
//            return this;
//        }
    }

    private class State {
        private List<Sample> mSourceSamples;
        private List<Sample> mSinkSamples;
        private List<Sample> mRemainder;

        public State() {
            mSourceSamples = new ArrayList<>();
            mSinkSamples = new ArrayList<>();
        }

        public List<Sample> sourceSamples() {
            return Collections.unmodifiableList(mSourceSamples);
        }

        public List<Sample> sinkSamples() {
            return Collections.unmodifiableList(mSinkSamples);
        }

        public List<Sample> remaining() {
            return Collections.unmodifiableList(mRemainder);
        }

        public boolean hasRemaining() {
            return mRemainder != null;
        }

        public State addSourceSample(Sample s) {
            mSourceSamples.add(s);
            return this;
        }

        public State addSinkSample(Sample s, boolean overlapped) {
            // increment reference if already added due to peeking
            if(mSinkSamples.contains(s) && overlapped) s.ref();
            mSinkSamples.add(s);
            return this;
        }

        public State addRemainingSamples(int m, int n) {
            // add remaining source samples if any
            int total = mSourceSamples.size();
            mRemainder = new ArrayList<>();
            mRemainder.addAll(mSourceSamples.subList(total - m, total));
            // in case of overlapping
            for(int i = 0; i < n; i++)
                mRemainder.get(i).ref();
            // add range increment constraints
            Sample prev = mSinkSamples.get(mSinkSamples.size() - 1);
            for(Sample sample: mRemainder) {
                addCI(prev, sample);
                prev = sample;
            }
            return this;
        }
    }

    private class States {
        private Map<SFG, Map<Channel, State>> mStates;
        public States() {
            mStates = new HashMap<>();
        }

        public State get(SFG sfg, Channel ch) {
            Map<Channel, State> state = mStates.getOrDefault(sfg, new HashMap<Channel, State>());
            State s = state.getOrDefault(ch, new State());
            state.put(ch, s);
            mStates.put(sfg, state);
            return s;
        }
    }

    private States mStates;
    private Live mLive;
    private Program mILP;
    private Set<String> mCI;
    private Set<String> mCR;
    private Set<Sample> mJudge;
    private int MaxS;

    public ILPOptimizer() {
        mStates = new States();
        mLive = new Live();
        mILP = new Program();
        mCI = new HashSet<>();
        mCR = new HashSet<>();
        mJudge = new HashSet<>();
    }

    private boolean distinct(Sample... samples) {
        mJudge.clear();
        for(Sample s: samples)
            mJudge.add(s);
        return mJudge.size() == samples.length;
    }

    private Optimizer addCI(Sample prev, Sample sample) {
        if(!prev.equals(sample)) {
            String I = "I" + prev + sample;
            mILP.abs(I, Program.diff(sample.name(), prev.name()));
            mILP.GE(I, 1);
            mCI.add(I);
        }
        return this;
    }

    private Optimizer addCWR(Sample s0, Sample s1, Sample s2) {
        if(distinct(s0, s1, s2)) {
            String R = "WR" + s0 + s1 + s2;
            mILP.abs(R, Program.summate(Program.diff(s2.name(), ("2" + s1)), s0.name()));
            mCR.add(R);
        }
        return this;
    }

    private Optimizer addCIWR(Sample w0s0, Sample w0s1, Sample w1s0, Sample w1s1) {
        if(!w0s0.equals(w0s1) || !w1s0.equals(w1s1)) {
            String R = "IWR" + w0s0 + w0s1 + w1s0 + w1s1;
            mILP.abs(R, Program.summate(Program.diff(w1s1.name(), w1s0.name(), w0s1.name()), w0s0.name()));
            mCR.add(R);
        }
        return this;
    }

    private Optimizer addCFIFO(Sample s0, Sample s1) {
        mILP.LE(Program.diff(s0.name(), s1.name()), 0);
        return this;
    }

    private int offset(Sample s) {
        return mILP.get(s.name()).intValue();
    }

    @Override
    public Optimizer window(SFG sfg, SFG from, Channel ch) {
        Channel.State cs = sfg.getState(ch);
        State s = mStates.get(sfg, ch);
        if(from != null) {
            // add remaining samples from INIT if any
            Channel.State fcs = from.getState(ch);
            State fs = mStates.get(from, ch);
            if (fs.hasRemaining()) {
                for (Sample sample: fs.remaining()) {
                    sample = new Sample(sample);
                    sample.writer(ch.sink());
                    sample.seq(-sample.seq());
                    s.addSinkSample(sample.ref(), false);
                }
            }
        }

        // add source samples to sink layout
        List<Sample> sourceSamples = s.sourceSamples();
        int m = sourceSamples.size();
        int i = 0;
        int windows = 0;
        boolean peeking = cs.PEEK > cs.POP;
        Sample prev = null;
        while(m >= cs.PEEK) {
            for(int p = 0; p < cs.PEEK; p++) {
                Sample sample = sourceSamples.get(i + p);
                s.addSinkSample(sample, peeking && i >= cs.POP && p < cs.POP);
                // add range increment constraint >= 1
                if(prev != null)
                    addCI(prev, sample);
                prev = sample;
            }
            i += cs.POP;
            m -= cs.POP;
            windows++;
        }

        // mark remaining samples if any
        if(cs.PEEK > cs.POP)
            s.addRemainingSamples(m, cs.PEEK - cs.POP);

        // add window regularity constraints
        List<Sample> samples = s.sinkSamples();
        for(int w = 0; w < windows; w++) {
            for (i = 0; i < cs.PEEK - 2; i++) {
                // intra window regularity
                Sample s0 = samples.get(w * cs.POP + i);
                Sample s1 = samples.get(w * cs.POP + i + 1);
                Sample s2 = samples.get(w * cs.POP + i + 2);
                addCWR(s0, s1, s2);
            }

            if(w > 0) {
                // inter window regularity
                for(i = 0; i < cs.PEEK - 1; i++) {
                    Sample w0s0 = samples.get((w-1) * cs.POP + i);
                    Sample w0s1 = samples.get((w-1) * cs.POP + i + 1);
                    Sample w1s0 = samples.get(w * cs.POP + i);
                    Sample w1s1 = samples.get(w * cs.POP + i + 1);
                    addCIWR(w0s0, w0s1, w1s0, w1s1);
                }
            }
        }
        return this;
    }

    @Override
    public Optimizer rebuild(SFG sfg) {
        Log.i(Formatter.title("Solving ILP Layout"));
        // ILP objective
        String S = "S";
        String L = "L";
        String CC = "CC";
        // S: Allocation Size
        mILP.max(S, mLive.vars());
        // faster to reach a feasible solution
//        mILP.LE(S, 3 * MaxS / 2);
        // L: Range Increments
        String[] vars = new String[mCI.size()];
        mCI.toArray(vars);
        mILP.sum(L, vars);
        // CC: Window Regularity
        vars = new String[mCR.size()];
        mCR.toArray(vars);
        mILP.sum(CC, vars);
        mILP.minimize(Program.summate(S, 3 + L, 2 + CC));

        // solve ILP to derive layout
        mILP.solve();
        Log.i(mILP);
        if(!mILP.solved())
            throw new RuntimeException("No feasible solution found");

        // rebuild layouts in ranges and windows
        Log.i(Formatter.title("Rebuilding ILP Layout"));
        for(Channel ch: sfg.channels()) {
            Channel.State s = sfg.getState(ch);
            State state = mStates.get(sfg, ch);
            // rebuild source layout
            List<Sample> samples = state.sourceSamples();
            for(int w = 0; w < samples.size() / s.PUSH; w++) {
                // rebuild a source window layout
                int pos = w * s.PUSH;
                int increment = 1;
                Sample prev = null;
                List<Sample> range = new ArrayList<>();
                for(int i = 0; i < s.PUSH; i++) {
                    Sample sample = samples.get(pos + i);
                    if(range.isEmpty()) {
                        range.add(sample);
                        increment = 1;
                    } else if(range.size() == 1 && sample.originator() == prev.originator()){
                        range.add(sample);
                        increment = offset(sample) - offset(prev);
                    } else {
                        // at least two samples in the range
                        if(offset(sample) - offset(prev) == increment && sample.originator() == prev.originator())
                            range.add(sample);
                        else {
                            // add a range
                            Sample head = range.get(0);
                            Range r = new Range(head.originator(), head.writer(), head.seq(), offset(head), range.size(), head.size(), increment);
                            s.getSourceWindow(w, true).add(r);
                            // start over
                            range.clear();
                            range.add(sample);
                            increment = 1;
                        }
                    }
                    prev = sample;
                }
                // add the last range of samples
                if(!range.isEmpty()) {
                    Sample head = range.get(0);
                    Range r = new Range(head.originator(), head.writer(), head.seq(), offset(head), range.size(), head.size(), increment);
                    s.getSourceWindow(w, true).add(r);
                }
            }

            // rebuild sink layout
            samples = state.sinkSamples();
            int windows = samples.size() / s.POP;
            for(int w = 0; w < windows; w++) {
                // rebuild a sink window layout
                int pos = w * s.POP;
                int increment = 1;
                Sample prev = null;
                List<Sample> range = new ArrayList<>();
                for(int i = 0; i < s.PEEK; i++) {
                    Sample sample = samples.get(pos + i);
                    if(range.isEmpty()) {
                        range.add(sample);
                        increment = 1;
                    } else if(range.size() == 1){
                        range.add(sample);
                        increment = offset(sample) - offset(prev);
                    } else {
                        // at least two samples in the range
                        if(offset(sample) - offset(prev) == increment)
                            range.add(sample);
                        else {
                            // add a range
                            Sample head = range.get(0);
                            Range r = new Range(head.originator(), head.writer(), head.seq(), offset(head), range.size(), head.size(), increment);
                            s.getSinkWindow(w, true).add(r);
                            // start over
                            range.clear();
                            range.add(sample);
                            increment = 1;
                        }
                    }
                    prev = sample;
                }
                // add the last range of samples
                if(!range.isEmpty()) {
                    Sample head = range.get(0);
                    Range r = new Range(head.originator(), head.writer(), head.seq(), offset(head), range.size(), head.size(), increment);
                    s.getSinkWindow(w, true).add(r);
                }
            }
        }

        matchWindowGroups(sfg);
        return this;
    }

    @Override
    protected Optimizer append(SFG sfg, Channel out, Channel in, int invocation, int size, int sampleSize) {
        return null;
    }

    @Override
    protected Optimizer release(SFG sfg, Component c, Channel in, int invocation, int n) {
        // dereference remaining samples
        Channel.State cs = sfg.getState(in);
        State sink = mStates.get(sfg, in);
        List<Sample> sinkSamples = sink.sinkSamples();
        int pos = invocation * cs.POP + cs.PEEK - n;
        for(int i = 0; i < n; i++)
            sinkSamples.get(pos + i).deRef();
        return this;
    }


    @Override
    protected Optimizer create(SFG sfg, Channel out, Channel in, int invocation, int size, int sampleSize) {
        if(in == null) {
            // source filter repeatedly inserts new live samples
            State src = mStates.get(sfg, out);
            for (int i = 0; i < size; i++)
                src.addSourceSample(mLive.add(out.src()));
            if(invocation == 0 && mLive.size() > 1) {
                // add source FIFO order constraint in general
                List<Sample> samples = src.sourceSamples();
                for(int i = 0; i < samples.size() - 1; i++) {
                    Sample s0 = samples.get(i);
                    Sample s1 = samples.get(i+1);
                    addCFIFO(s0, s1);
                }
            }
        } else {
            // repeatedly free and insert new live samples
            Channel.State cs = sfg.getState(in);
            State sink = mStates.get(sfg, in);
            State src = mStates.get(sfg, out);
            List<Sample> sinkSamples = sink.sinkSamples();
            int max = Math.min(cs.PEEK, size);
            for(int i = 0; i < max; i++) {
                Sample s = sinkSamples.get(invocation * cs.POP + i);
                s.deRef();
                src.addSourceSample(mLive.add(out.src()));
            }
            if(size > cs.PEEK) {
                for(int i = 0; i < size - cs.PEEK; i++)
                    src.addSourceSample(mLive.add(out.src()));
            }
        }
        return this;
    }

    @Override
    protected Optimizer insert(SFG sfg, Channel out, Channel in, int invocation, int pos, int size, int sampleSize) {
        // insert new live samples
        State src = mStates.get(sfg, out);
        for(int i = 0; i < size; i++)
            src.addSourceSample(mLive.add(out.src()));
        return this;
    }

    @Override
    protected Optimizer update(SFG sfg, Channel out, Channel in, int invocation, int pos, int size) {
        // free a live sample and insert a new live sample
        Channel.State cs = sfg.getState(in);
        State sink = mStates.get(sfg, in);
        State src = mStates.get(sfg, out);
        List<Sample> sinkSamples = sink.sinkSamples();
        for(int i = 0; i < size; i++) {
            Sample s = sinkSamples.get(invocation * cs.POP + pos + i);
            s.deRef();
            src.addSourceSample(mLive.add(out.src()));
        }
        return this;
    }

    @Override
    protected Optimizer pass(SFG sfg, Channel out, Channel in, int invocation, int pos, int size) {
        // add w/o dereference
        Channel.State cs = sfg.getState(in);
        State sink = mStates.get(sfg, in);
        State src = mStates.get(sfg, out);
        List<Sample> sinkSamples = sink.sinkSamples();

        // increment reference count if duplicate
//        Log.i("PASS: from %s to %s for invocation %d, pos %d, size %d\n", sfg.toString(in), sfg.toString(out), invocation, pos, size);
        List<Channel> outputs = sfg.getOutputChannels(out.src());
        boolean isSplitter = sfg.isSplitter(out.src());
        boolean duplicate = isSplitter && ((Splitter)out.src()).isDuplicate() && outputs.indexOf(out) > 0;
        for(int i = 0; i < size; i++) {
            Sample s = sinkSamples.get(invocation * cs.POP + pos + i);
            if(duplicate) s.ref();
            src.addSourceSample(s);
        }
        return this;
    }
}
