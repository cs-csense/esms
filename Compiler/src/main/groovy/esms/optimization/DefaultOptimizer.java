package esms.optimization;

import java.util.List;

import esms.api.Channel;
import esms.api.Component;
import esms.api.SFG;
import esms.core.Operation;

/**
 * Created by farleylai on 2/23/15.
 */
public class DefaultOptimizer extends Optimizer {
    /**
     * Reuild by aligning source window ranges along the superframe route with sink windows.
     *
     * @return
     */
    @Override
    public Optimizer rebuild(SFG sfg) {
        return this;
    }

    @Override
    protected Optimizer window(SFG sfg, Channel channel, int invocations) {
        return this;
    }

    @Override
    public Optimizer apply(SFG sfg, Component c, Channel out, List<Operation> operations, int invocation) {
        return this;
    }

    @Override
    protected Optimizer append(SFG sfg, Channel out, Channel in, int invocation, int size, int sampleSize) {
        return this;
    }

    /**
     * Push non-overlapped windows.
     *
     * @param out        the output channel
     * @param in         the input channel to base
     * @param invocation the order of invocation
     * @param size       the window sizeInBytes to create
     * @return the SFG executed
     */
    protected Optimizer create(SFG sfg, Channel out, Channel in, int invocation, int size, int sampleSize) {
        return this;
    }

    /**
     * Insert contiguously for reinterpreted filters if any.
     *
     * @param out
     * @param in
     * @param invocation
     * @param pos Sink window position.
     * @param size
     * @param sampleSize
     * @return
     */
    protected Optimizer insert(SFG sfg, Channel out, Channel in, int invocation, int pos, int size, int sampleSize) {
        return this;
    }

    /**
     * Identify ranges that can be directly updated and those that require rearrangement due to
     * RAW and WAR conflicts.
     *
     * @param out
     * @param in
     * @param invocation
     * @param pos
     * @param size
     * @return
     */
    protected Optimizer update(SFG sfg, Channel out, Channel in, int invocation, int pos, int size) {
        return this;
    }

    /**
     * Pass input sink window ranges to output source windows by splitter or joiner.
     *
     * @param out
     * @param in
     * @param invocation
     * @param pos
     * @param size
     * @return
     */
    protected Optimizer pass(SFG sfg, Channel out, Channel in, int invocation, int pos, int size) {
        return this;
    }
}
