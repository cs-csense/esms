package esms.optimization;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import esms.api.Channel;
import esms.api.Component;
import esms.api.SFG;
import esms.core.OP;
import esms.core.Range;
import esms.core.Superframe;
import esms.core.Window;
import esms.streamit.Filter;
import esms.util.Log;

/**
 * Created by farleylai on 2/23/15.
 */
public class ESMSOptimizer extends Optimizer {
    private final String TAG = getClass().getSimpleName();
    /**
     * Reuild by aligning source window ranges along the superframe route with sink windows.
     *
     * @return
     */
    @Override
    public Optimizer rebuild(SFG sfg) {
        for(Superframe sf: sfg.superframes()) {
            for(Channel ch: sf.route()) {
                if(!sfg.isProductive(ch.src())) continue;
//                Log.d("Rebuilding %s\n", sfg.toString(ch));
                Channel.State s = sfg.getState(ch);
                Window layout = sfg.layout(ch, true);
//                Log.d("Sink Layout\n%s\n", layout);
                List<Window> ws = new ArrayList<>(s.getSourceWindows());
                s.clearSourceWindows();
                for(Window w: ws) {
//                Log.d("BEFORE source window:\n%s\n", w);
                    Window src = new Window();
                    for(Range r: w) {
                        while (!r.isEmpty()) {
                            int displacement = layout.align(r);
                            if (displacement == -1)
                                throw new RuntimeException("Cannot align source range: " + r);
                            r.shift(displacement);
                            Range range = layout.search(r.offset()).get(0);
                            Range intersection = range.intersect(r);
//                        Log.d("layout range: %s, r: %s, intersection: %s\n", range, r, intersection);
                            src.add(intersection);
                            if (!r.reverse())
//                            r.index(r.index() + intersection.size());
                                r.index(r.index() + (r.index() >= 0 ? 1 : -1) * intersection.size());
                            r.offset(intersection.limit());
                            r.size(r.size() - intersection.size());
                        }
                    }
//                    Log.d("AFTER source window:\n%s\n", src.linearize(true));
                    s.push(src);
                }
            }
        }
        matchWindowGroups(sfg);
        return this;
    }

//    /**
//     * FIXME This allows non-SSAS but ESMS requires SSAS to ensure correct remainder save/load
//     * Windowing the output channels after invocation.
//     * Assume one single superframe whose route is determined by the schedule.
//     *
//     * FIXME Orgiginal ESMS assumption
//     * Windowing the input channels before invocation.
//     * The input channels are remove from the superframe route and added back after invocation.
//     *
//     * @param to
//     * @param from
//     * @param c
//     * @return
//     */
//    @Override
//    public Optimizer window(SFG sfg, Component c) {
//        for(Channel ch: sfg.getOutputChannels(c))
////        for(Channel ch: to.getInputChannels(c))
//            window(sfg, ch);
//        return this;
//    }

    /**
     * Windowing by schedule instead of consuming as mush as possible.
     *
     * @param sfg
     * @param channel
     * @return
     */
    @Override
    protected Optimizer window(SFG sfg, Channel channel, int invocations) {
        sfg.getSuperframe(channel).push(channel);
        Channel.State s = sfg.getState(channel);
        Window sink;
        if (s.hasRemaining()) {
            // load the remainder as is and its dependencies to future sink windows
            load(sfg, channel);
            sink = s.getSinkWindow(0);
        } else {
            // no remainder or preloaded
            sink = s.getSinkWindows().isEmpty() ? new Window() : s.getSinkWindow(0);
            if(!s.getSinkWindows().isEmpty()) {
                // shift source layout for preloaded remainder
                s.shift(s.displacement(), true);
            }
        }

        List<Window> srcWindows = s.getSourceWindows();
        List<Window> sinkWindows = s.getSinkWindows();
        sinkWindows.clear();
        Log.d("Before windowing %s, \n", sfg.toString(channel));
        for (int i = 0; i < srcWindows.size(); i++) {
            Window src = srcWindows.get(i);
            if(i == 0 && sinkWindows.isEmpty() && sink.size() >= s.PEEK) {
                // FIXME the remainder can be larger than peek size
                src = sink;
                sink = new Window();
                i--;
                Log.d("src from loaded remainder:\n%s\n", src);
            } else
                Log.d(TAG, "src[%d]:\n%s\n", i, src);
            int pos = 0;
            while (pos < src.size()) {
                if (sinkWindows.size() == invocations || src.size() - pos < s.PEEK - sink.size()) {
                    // append source windows to the remainder sink even if more than necessary
                    sink.addAll(src.slice(pos, src.size() - pos));
                    pos = src.size();
                } else {
                    int advance = s.PEEK - sink.size();
                    Window slice = src.slice(pos, advance);
                    sink.addAll(slice);
                    sinkWindows.add(sink.merge());
                    Log.d(TAG, "sink[%d] added:\n%s\n", sinkWindows.size()-1, sinkWindows.get(sinkWindows.size()-1));
                    sink = sink.slice(s.POP, sink.size() - s.POP);
                    pos += advance;
                }
            }
        }

        // The schedule may cause the remainder to be as large as the peek size or more
        Log.d("%d sink windows added\n", sinkWindows.size());
        if (sink.size() > 0) {
            // save remainder to the last sink window too
            s.save(sink.writer(channel.sink()));
            Log.d("remainder added: \n%s", s.remainder());
        }
        return this;
    }


    /**
     * Precondition: no remainder pre-loaded of the channel
     * Postcondition: remainders of dependent channels are loaded and serve as future
     * Remainder shift offset is determined by shared remainders.
     *
     * @param channel
     * @return
     */
    private Optimizer load(SFG sfg, Channel channel) {
        // collect originators in the remainder
        Set<Component> originators = new HashSet<>();
        sfg.getState(channel).remainder().forEach(r -> originators.add(r.originator()));

        // find other channels with shared remainders of dependent originators
        List<Channel> others = new ArrayList<>();
        for(Channel ch: sfg.channels()) {
            if(ch != channel && sfg.getState(ch).hasRemaining()) {
                if(sfg.getState(ch).remainder().stream().anyMatch(r -> originators.contains(r.originator()))) {
                    sfg.getState(ch).remainder().forEach(r -> originators.add(r.originator()));
                    others.add(ch);
                }
            }
        }

        // compute shared remainder displacement
        Window remainders = new Window();
        remainders.addAll(sfg.getState(channel).remainder());
        others.forEach(ch -> remainders.addAll(sfg.getState(ch).remainder()));
        final int remainderDisplacement = remainders.offset();
        final int srcLayoutDisplacement = remainders.size(false);
        sfg.getState(channel).displacement(srcLayoutDisplacement);
        sfg.getState(channel).load(-remainderDisplacement);
        Log.d("Load remainder to %s with remainder displacement %d\n", sfg.toString(channel), remainderDisplacement);

        // reserve upstream sink layouts for the loaded remainder
        sfg.getSuperframe(channel).route()
                .forEach(ch -> sfg.getState(ch).shift(srcLayoutDisplacement));
        // reserve current source layout for the loaded remainder
        sfg.getState(channel).shift(srcLayoutDisplacement, true);

        // pre-load all the other channels with shared remainders as future
        others.forEach(ch -> {
            sfg.getState(ch).load(-remainderDisplacement);
            sfg.getSuperframe(channel).push(ch, null);
            if(sfg.getState(ch).getSourceWindows().isEmpty()) {
                // shared downstream or splitjons
                if(sfg.isSuccessor(channel.sink(), ch.sink())) {
                    sfg.getState(ch).displacement(0);
                    Log.d("Preload downstream remainder in %s without displacement\n", sfg.toString(channel));
                } else {
                    sfg.getState(ch).displacement(srcLayoutDisplacement);
                    Log.d("Preload splitjoin remainder in %s with later source layout displacement of %d\n", sfg.toString(ch), srcLayoutDisplacement);
                }
            } else {
                // shared splitjoins and shift existing source layouts
                sfg.getState(ch).shift(srcLayoutDisplacement, true);
                Log.d("Preload splitjoin %s with immediate source layout displacement of %d\n", sfg.toString(ch), srcLayoutDisplacement);
            }
        });
        return this;
    }

    /**
     * Reserve space for insertions.
     *
     * @param out
     * @param offset
     * @param size
     * @return
     */
    private Optimizer reserve(SFG sfg, Channel out, Channel in, int invocation, int offset, int size) {
        Superframe inSF = sfg.getSuperframe(in);
        Window src = sfg.getSourceWindow(out, invocation);
        if (sfg.isAllocated(out, in)) {
            sfg.getState(out).getSourceWindows().stream().filter(w -> w != src).forEach(w -> w.reserve(offset, size));
        } else {
//            Log.d("reserve(): SF route:");
//            for(Channel ch: inSF.route())
//                Log.d("%s->%s, ", ch.src(), ch.sink());
//            Log.d("\nreserve(): SF future:");
//            for(Channel ch: inSF.future())
//                Log.d("%s->%s, ", ch.src(), ch.sink());
//            Log.d("\n");
            sfg.getState(out).getSourceWindows().stream().filter(w -> w != src).forEach(w -> w.reserve(offset, size));
            // reserve upstream sink windows
            inSF.route().stream()
                    .filter(ch -> ch != out)
                    .map(ch -> sfg.getState(ch).getSinkWindows())
                    .forEach(ws -> ws.stream().forEach(w -> w.reserve(offset, size)));
            // reserve future source windows
            inSF.future().stream()
                    .filter(ch -> ch != out)
                    .map(ch -> sfg.getState(ch).getSourceWindows())
                    .forEach(ws -> ws.forEach(w -> w.reserve(offset, size)));
            // reserve future sink windows for pre-loaded remainders
            inSF.future().stream()
                    .filter(ch -> ch != out)
                    .map(ch -> sfg.getState(ch).getSinkWindows())
                    .forEach(ws -> ws.forEach(w -> w.reserve(offset, size)));
         }
        return this;
    }

    @Override
    protected Optimizer append(SFG sfg, Channel out, Channel in, int invocation, int size, int sampleSize) {
        Channel.State sin = sfg.getState(in);
        Channel.State sout = sfg.getState(out);
        Window src = sout.getSourceWindow(invocation, true);
        int limit = sfg.limit(sfg.getSuperframe(in));
//        Log.d("Before %s[%d] appends %s samples at %d,\n", out.src(), invocation, size, limit);
        switch(Optimizer.opt()) {
            case APPEND_ALWAYS:
                src.add(out.src(), sout.nextPushIndex(size), limit, size, sampleSize);
                break;
            case APPEND_ON_CONFLICT:
                boolean conflicted = sin.POP < size || ((Filter)out.src()).isConflited();
                if (!conflicted) {
                    List<Window> references = sfg.references(out, in, invocation, OP.UPDATE);
                    Window sink = sin.getSinkWindow(invocation);
                    for (int pos = 0; pos < size && !conflicted; pos++)
                        conflicted = sfg.conflicts(references, sink.offsetAt(pos));
                }
                if (conflicted) {
                    src.add(out.src(), sout.nextPushIndex(size), limit, size, sampleSize);
                    Log.d("%s append on conflict\n", out.src());
                } else {
                    update(sfg, out, in, invocation, 0, size);
                    Log.d("%s update w/o conflict\n", out.src());
                }
        }
//        Log.d("src[%d] added:\n%s\n", invocation, src);
        return this;
    }

    /**
     * Push non-overlapped windows.
     *
     * @param out        the output channel
     * @param in         the input channel to base
     * @param invocation the order of invocation
     * @param size       the window sizeInBytes to create
     * @return the SFG executed
     */
    protected Optimizer create(SFG sfg, Channel out, Channel in, int invocation, int size, int sampleSize) {
        Component c = out.src();
        Channel.State sin = sfg.getState(in);
        Channel.State sout = sfg.getState(out);
        Window sink = sin == null ? null : sin.getSinkWindow(invocation);
        Window src = sout.getSourceWindow(invocation, true);
        if (sfg.isAllocated(out, in)) {
            // new suprframe sizeInBytes => output non-overlapping windows
            src.add(c, sout.nextPushIndex(size), invocation * sout.PUSH, size, sampleSize);
        } else {
            // in-place create on input superframe in view of sink windows and future references
            if(sink == null) {
                src.add(c, sout.nextPushIndex(size), invocation * sout.PUSH, size, sampleSize);
            } else {
                if (size <= sin.POP) {
                    // compactly append to the previous source window if possible
                    if (invocation == 0) {
                        update(sfg, out, in, invocation, 0, size);
                    } else {
                        // utilize headroom if sink/src are linear
                        Window prevSource = sout.getSourceWindow(invocation - 1);
                        if(size < sin.POP && sin.isSinkLinear() && prevSource.isLinear()) {
                            int begin = prevSource.limit();
                            List<Window> references = sfg.references(out, in, invocation, OP.CREATE);
                            int i;
                            boolean conflicted = false;
                            for (i = 0; i < size && !conflicted; i++) {
                                if (sfg.conflicts(references, begin + i))
                                    conflicted = true;
                            }
//                            Log.d("conflicted: %s, i: %d, size: %d\n", conflicted, i, size);
                            if (!conflicted && i == size) {
                                src.add(c, sout.nextPushIndex(size), begin + i - 1, size, sampleSize);
                                Log.d("creates %s in headroom\n", src.get(src.ranges().size() - 1));
                                return this;
                            }
                        }
                        update(sfg, out, in, invocation, 0, size);
                    }
                } else {
                    // overwrite the source window and reserve room for more
                    update(sfg, out, in, invocation, 0, sin.POP);
                    insert(sfg, out, in, invocation, sin.POP, size - sin.POP, sampleSize);
                }
            }
        }
        return this;
    }

    /**
     * Insert contiguously for reinterpreted filters if any.
     *
     * @param out
     * @param in
     * @param invocation
     * @param pos Sink window position.
     * @param size
     * @param sampleSize
     * @return
     */
    protected Optimizer insert(SFG sfg, Channel out, Channel in, int invocation, int pos, int size, int sampleSize) {
        Channel.State sin = sfg.getState(in);
        Channel.State sout = sfg.getState(out);
        Window sink = sin.getSinkWindow(invocation);
        Window src = sout.getSourceWindow(invocation, true);
        int offset = sink.offsetAt(pos);
        if(((Filter)out.src()).isReinterpreted()) {
            // Reinterpreted INSERTs are supposed to be contiguous
            offset = invocation == 0 ? sink.offset() : sout.getSourceWindow(invocation-1).limit();
            Log.d(TAG, "%s realizes reinterpreted INSERT to range(%d, %d) while expected offset=%d\n", out.src(), offset, size, sink.offsetAt(pos));
        }

        if(offset < 0) {
            offset = sink.limit();
//            Log.d("%s insert by appending to sink at limit %d\n%s\n", in.sink(), offset, sink);
        }
        reserve(sfg, out, in, invocation, offset, size);
        src.reserve(offset, size);
        src.add(out.src(), sout.nextPushIndex(size), offset, size, sampleSize);
        return this;
    }

    /**
     * Update the layouts depending on the conflicted range in the invoked window.
     * if not conflicted, (pos, size) must be contiguous to update.
     *
     * @param out
     * @param in
     * @param invocation
     * @param pos Sink window position
     * @param size       a consecutive range length to update
     * @param conflicted true if the range sizeInBytes is conflicted or false otherwise.
     * @return
     */
    private Optimizer update(SFG sfg, Channel out, Channel in, int invocation, int pos, int size, boolean conflicted) {
        Channel.State sin = sfg.getState(in);
        Channel.State sout = sfg.getState(out);
        Window sink = sin.getSinkWindow(invocation);
        Window src = sout.getSourceWindow(invocation, true);
        if (conflicted)
            return insert(sfg, out, in, invocation, pos, size, sink.rangeAt(pos).sampleSize());
        Window updated = sink.slice(pos, size).originator(out.src()).writer(out.src());
        updated.forEach(r -> r.index(sout.nextPushIndex(r.size())));
        src.addAll(updated);
        return this;
    }

    /**
     * Identify ranges that can be directly updated and those that require rearrangement due to
     * RAW and WAR conflicts.
     *
     * @param out
     * @param in
     * @param invocation
     * @param pos
     * @param size
     * @return
     */
    protected Optimizer update(SFG sfg, Channel out, Channel in, int invocation, int pos, int size) {
        Channel.State sin = sfg.getState(in);
        Window sink = sin.getSinkWindow(invocation);
        while (size > 0) {
            // locate the subrange to update in the current window
            Range r = sink.rangeAt(pos);
            int offset = sink.offsetAt(pos);
            int sz = r.reverse() ? offset - r.offset() : r.limit() - offset;
            sz = Math.min(size, sz);
//            Log.i("max range sizeInBytes to update: %d\n", sz);

            // R/W conflict length
            int length = 0;
            List<Window> references = sfg.references(out, in, invocation, OP.UPDATE);
            while (length < sz && sfg.conflicts(references, sink.offsetAt(pos + length)))
                length++;
            if (length > 0) {
//                Log.i("update(out, in, %d, %d, %d, true)\n", invocation, pos, length);
                update(sfg, out, in, invocation, pos, length, true);
            } else {
                // update the output source window directly
                while (length < sz && !sfg.conflicts(references, sink.offsetAt(pos + length)))
                    length++;
                if (length > 0) {
//                    Log.i("update(out, in, %d, %d, %d, false)\n", invocation, pos, length);
                    update(sfg, out, in, invocation, pos, length, false);
                }
            }
            pos += length;
            size -= length;
        }
        return this;
    }

    /**
     * Pass input sink window ranges to output source windows by splitter or joiner.
     *
     * @param out
     * @param in
     * @param invocation
     * @param pos
     * @param size
     * @return
     */
    protected Optimizer pass(SFG sfg, Channel out, Channel in, int invocation, int pos, int size) {
        Channel.State sin = sfg.getState(in);
        Channel.State sout = sfg.getState(out);
        Window sink = sin.getSinkWindow(invocation);
        Window src = sout.getSourceWindow(invocation, true);
//        Log.d("%s pass from sink window(pos: %d, size: %d): \n%s", out.src(), pos, size, sink);
//        Log.d("%s pass to src window: \n%s", out.src(), src);
        src.addAll(sink.slice(pos, size));
        return this;
    }
}
