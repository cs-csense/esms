package esms.streamit

import esms.util.Log;
import streamit.frontend.nodes.ExprArray;
import streamit.frontend.nodes.ExprConstFloat;
import streamit.frontend.nodes.ExprConstInt;
import streamit.frontend.nodes.ExprPeek;
import streamit.frontend.nodes.ExprPop;
import streamit.frontend.nodes.ExprTernary;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.FEReplacer;
import streamit.frontend.nodes.FieldDecl;
import streamit.frontend.nodes.FuncWork;
import streamit.frontend.nodes.Function;
import streamit.frontend.nodes.StmtBody;
import streamit.frontend.nodes.StmtBreak;
import streamit.frontend.nodes.StmtContinue;
import streamit.frontend.nodes.StmtExpr;
import streamit.frontend.nodes.StmtPush;
import streamit.frontend.nodes.StreamSpec;
import streamit.frontend.nodes.StreamType;
import streamit.frontend.nodes.Type;
import streamit.frontend.nodes.TypeArray;

/**
 * Created by farleylai on 9/21/14.
 */
public class StreamItSimulator extends StreamItEvaluator {
    private static final String TAG = StreamItSimulation.class.getSimpleName();
    private StreamItSimulation mSimulation;

    public StreamItSimulator(StreamItEvaluator parent) {
        super(parent);
        mSimulation = new StreamItSimulation(this);
    }

    public StreamItSimulator simulate(Filter s) {
        if (s.basename().equals("Identity")) {
            mSimulation.pass(s.getInput(0), 0, 1);
        } else if(s.basename().equals("FileReader")) {
            mSimulation.push();
        } else if(s.basename().equals("FileWriter")) {
            mSimulation.pop();
        } else {
            StreamSpec spec = s.spec();
            if (spec.getInitFunc() != null) {
                Log.i(TAG, "========== Simulating %s.init() ===========\n", s);
                spec.getInitFunc().accept(this);
            }

            Log.i(TAG, "========== Simulating %s.work() ===========\n", s);
            spec.getWorkFunc().accept(this);
            if(s.isInputDependent() || !s.isSafe()) {
                Log.w(TAG, "%s is input dependent %s\n", s, s.isSafe() ? "but safe" : "and unsafe");
            }
            if(spec.isStateful()) {
                // check if there are stateful conflicts
                FEReplacer detector = new FEReplacer() {
                    @Override
                    public Expression visitExprPeek(ExprPeek exp) {
                        if(mSimulation.isConflicted(exp))
                            mSimulation.setConflictStateful(exp);
                        return exp;
                    }

                    @Override
                    public Expression visitExprPop(ExprPop exp) {
                        if(mSimulation.isConflicted(exp)) {
                            mSimulation.setConflictStateful(exp);
                        }
                        return exp;
                    }
                };

                spec.getVars().each { fields ->
                    for (int i = 0; i < fields.getNumFields(); i++) {
                        Type type = fields.getType(i);
                        String name = fields.getName(i);
                        if (type instanceof TypeArray) {
                            TypeArray arrayT = (TypeArray) type;
                            int[] dims = getArrayDims(arrayT);
                            int total = dims.inject(1) { ret, it -> ret * it };
                            for (int index = 0; index < total; index++) {
                                ExprArray array = getExprArray(arrayT, name, index);
                                getArrayElement(array).accept(detector);
                            }
                        } else {
                            Expression value = getVar(name);
                            Log.d(TAG, "check stateful field: %s=%s\n", name, value);
                            value.accept(detector);
                        }
                    }
                };
            }
        }
//        Log.i("========== %s Operation Simulation ===========\n", s.name());
//        mSimulation.operations().each { operation -> Log.i("%s: %s\n", operation, mSimulation.isConflicted(operation) ? "conflicted" : "conflictless") }
        s.submit(mSimulation);
        return this;
    }

    @Override
    public ExprPeek visitExprPeek(ExprPeek exp) {
//        Log.i("visitExprPeek(): " + exp);
        if(!mSimulation.contains(exp)) {
            Expression offset = doExpression(exp.getExpr());
            exp = new ExprPeek(exp.getContext(), offset);
            if (isEvaluable(offset))
                mSimulation.peek(exp, toInt(exp.getExpr()));
            else
                mSimulation.peek(exp);
        }
        return exp;
    }

    @Override
    public ExprPop visitExprPop(ExprPop exp) {
//        Log.i("visitExprPop(): " + exp);
        if(!mSimulation.contains(exp)) {
            exp = new ExprPop(exp.getContext());
            mSimulation.pop(exp);
        }
        return exp;
    }

    @Override
    public Object visitStmtPush(StmtPush stmt) {
//        Log.i("visitStmtPush(): " + stmt);
        Expression arg = doExpression(stmt.getValue());
        stmt = new StmtPush(stmt.getContext(), arg);
        if(arg instanceof ExprConstInt) {
            // push a constant
            mSimulation.push();
        } else if(arg instanceof ExprConstFloat) {
            // push a constant
            mSimulation.push();
        } else if(arg instanceof ExprPop) {
            // push a popped value
            mSimulation.push(stmt, (ExprPop)arg);
        } else if(arg instanceof ExprPeek) {
            // push a peeked value
            mSimulation.push(stmt, (ExprPeek)arg);
        } else {
            // push a compound expression involving peek/pop
            List<Expression> exps = decompose(stmt.getValue());
            boolean conflicted = exps.any { mSimulation.isConflicted(it) }
//            Log.d("push a compound expression %s with %speek()/pop()\n", exps, conflicted ? "conflicted " : "");
            mSimulation.push(stmt, conflicted);
        }
        return stmt;
    }

    @Override
    public Object visitFieldDecl(FieldDecl field) {
//        Log.i("visitFieldDecl()");
        return super.visitFieldDecl(field);
    }

    @Override
    public Object visitFunction(Function func) {
//        Log.i("entering Function() cls=%d\n", func.getCls());
        return super.visitFunction(func);
    }

    @Override
    public Object visitFuncWork(FuncWork func) {
//        Log.i("visitFuncWork()");
        return super.visitFuncWork(func);
    }

    @Override
    public Object visitStreamType(StreamType type) {
//        Log.i("visitStreamType()");
        return super.visitStreamType(type);
    }

    @Override
    public Object visitStmtExpr(StmtExpr stmt) {
//        Log.i("visitStmtExpr()");
        return super.visitStmtExpr(stmt);
    }

    @Override
    public Object visitStmtBody(StmtBody stmt) {
//        Log.i("visitStmtBody()");
        return super.visitStmtBody(stmt);
    }

    @Override
    public Object visitStmtBreak(StmtBreak stmt) {
//        Log.i("visitStmtBreak()");
        return super.visitStmtBreak(stmt);
    }

    @Override
    public Object visitStmtContinue(StmtContinue stmt) {
//        Log.i("visitStmtContinue()");
        return super.visitStmtContinue(stmt);
    }

    @Override
    public Object visitExprTernary(ExprTernary exp) {
//        Log.i("visitExprTernary()");
        return super.visitExprTernary(exp);
    }
}
