package esms.streamit

import esms.util.Log
import streamit.frontend.nodes.ExprArray;
import streamit.frontend.nodes.ExprBinary;
import streamit.frontend.nodes.ExprComplex;
import streamit.frontend.nodes.ExprComposite;
import streamit.frontend.nodes.ExprConstChar;
import streamit.frontend.nodes.ExprConstFloat;
import streamit.frontend.nodes.ExprConstInt;
import streamit.frontend.nodes.ExprConstStr;
import streamit.frontend.nodes.ExprFunCall;
import streamit.frontend.nodes.ExprPeek;
import streamit.frontend.nodes.ExprPop;
import streamit.frontend.nodes.ExprUnary;
import streamit.frontend.nodes.ExprVar;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.StmtAssign;
import streamit.frontend.nodes.Type;
import streamit.frontend.nodes.TypePrimitive;

/**
 * Created by farleylai on 9/21/14.
 */
public class ExpressionDecomposer extends StreamItEvaluator {
    private List<Expression> mExpressions;
    private int mDelimiter;

    public ExpressionDecomposer(StreamItEvaluator parent) {
        super(parent);
        mDelimiter = -1;
        mExpressions = new ArrayList<>();
    }

    public ExpressionDecomposer(StreamItEvaluator parent, int op) {
        this(parent);
        mDelimiter = op;
    }

    public List<Expression> expressions() {
        return mExpressions;
    }

    @Override
    public StmtAssign visitStmtAssign(StmtAssign stmt) {
        throw new RuntimeException("Unexpected assignment statement");
    }



    @Override
    public ExprBinary visitExprBinary(ExprBinary exp) {
        //Log.i("ExpressionDecomposer.visitExprBinary(): " + exp);
        if(mDelimiter < 0) return (ExprBinary)super.visitExprBinary(exp);
        if(exp.getOp() == mDelimiter) {
            Expression lhs = exp.getLeft();
            Expression rhs = exp.getRight();
            Type type = getType(lhs);
            boolean leftString = type instanceof TypePrimitive && ((TypePrimitive)type).getType() == TypePrimitive.TYPE_STRING;
            type = getType(rhs);
            boolean rightString = type instanceof TypePrimitive && ((TypePrimitive)type).getType() == TypePrimitive.TYPE_STRING;
            if(leftString || rightString) {
                // delimiter
                lhs = doExpression(lhs);
                rhs = doExpression(rhs);
                exp = new ExprBinary(exp.getContext(), exp.getOp(), lhs, rhs);
            } else {
                // binary op
                mExpressions.add(exp);
            }
        } else {
            // binary op
            mExpressions.add(exp);
        }
        return exp;
    }

    /* FIXME Visit the enclosing variable */
    @Override
    public ExprUnary visitExprUnary(ExprUnary exp) {
        mExpressions.add(exp);
        return exp;
    }

    /* FIXME Decompose the value of the variable */
    @Override
    public ExprVar visitExprVar(ExprVar var) {
        mExpressions.add(var);
        return var;
    }

    /* FIXME Decompose the value of the array element */
    @Override
    public ExprArray visitExprArray(ExprArray array) {
        mExpressions.add(array);
        return array;
    }

    @Override
    public ExprPeek visitExprPeek(ExprPeek exp) {
        mExpressions.add(exp);
        return exp;
    }

    @Override
    public ExprPop visitExprPop(ExprPop exp) {
        mExpressions.add(exp);
        return exp;
    }

    @Override
    public ExprConstStr visitExprConstStr(ExprConstStr exp) {
        mExpressions.add(exp);
        return exp;
    }

    @Override
    public ExprConstChar visitExprConstChar(ExprConstChar exp) {
        mExpressions.add(exp);
        return exp;
    }

    @Override
    public ExprConstInt visitExprConstInt(ExprConstInt exp) {
        mExpressions.add(exp);
        return exp;
    }

    @Override
    public ExprConstFloat visitExprConstFloat(ExprConstFloat exp) {
        mExpressions.add(exp);
        return exp;
    }

    @Override
    public ExprComposite visitExprComposite(ExprComposite exp) {
        mExpressions.add(exp);
        return exp;
    }

    @Override
    public ExprComplex visitExprComplex(ExprComplex exp) {
        mExpressions.add(exp);
        return exp;
    }

    // push()/peek()/pop() are converted to functiona calls.
    @Override
    public ExprFunCall visitExprFunCall(ExprFunCall call) {
        mExpressions.add(call);
        return call;
    }
}
