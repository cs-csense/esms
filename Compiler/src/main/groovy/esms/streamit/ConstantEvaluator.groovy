package esms.streamit

import esms.util.Log
import streamit.frontend.nodes.ExprArray;
import streamit.frontend.nodes.ExprBinary;
import streamit.frontend.nodes.ExprConstBoolean;
import streamit.frontend.nodes.ExprConstChar;
import streamit.frontend.nodes.ExprConstFloat;
import streamit.frontend.nodes.ExprConstInt;
import streamit.frontend.nodes.ExprConstStr;
import streamit.frontend.nodes.ExprFunCall;
import streamit.frontend.nodes.ExprPeek;
import streamit.frontend.nodes.ExprPop;
import streamit.frontend.nodes.ExprTernary;
import streamit.frontend.nodes.ExprTypeCast;
import streamit.frontend.nodes.ExprUnary;
import streamit.frontend.nodes.ExprVar;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.FEReplacer;

/**
 * Created by farleylai on 9/19/14.
 */
public class ConstantEvaluator extends FEReplacer {
    private StreamItEvaluator mEvaluator;

    public ConstantEvaluator(StreamItEvaluator evaluator) {
        mEvaluator = evaluator;
    }

    @Override
    public Boolean visitExprUnary(ExprUnary exp) {
        return (Boolean)exp.getExpr().accept(this);
    }

    @Override
    public Boolean visitExprBinary(ExprBinary exp) {
        def exps = [exp]
        boolean ret = true
        while(ret && !exps.isEmpty()) {
            Expression e = exps.pop()
            if(e instanceof ExprBinary) {
                exps << e.getLeft() << e.getRight()
            } else {
                ret = ret && e.accept(this)
            }
        }
        return ret

        // FIXME StackOverflowError
        //return (Boolean)exp.getLeft().accept(this) && (Boolean)exp.getRight().accept(this);
    }

    @Override
    public Boolean visitExprTernary(ExprTernary exp) {
//        Log.i("ConstantEvaluator.visitExprTernary(): %s\n", exp);
        if((Boolean) exp.getA().accept(this))
            return mEvaluator.toBoolean(exp.getA()) ? (Boolean)exp.getB().accept(this) : (Boolean)exp.getC().accept(this);
        return false;
    }

    /**
     * Check the symbol table if the current value is evaluable.
     *
     * @param exp
     * @return
     */
    public Boolean visitExprVar(ExprVar exp) {
//        Log.i("ConstantEvaluator.visitExprVar() %s\n", exp);
        return mEvaluator.isEvaluable(exp);
    }

    /**
     * Check the symbol table if the array expression is evaluable.
     * An array expression is evaluable only if
     * 1.) the array is evaluable
     * 2.) the array expression is evaluable
     * 3.) the array element is evaluable
     *
     * @param exp
     * @return
     */
    public Boolean visitExprArray(ExprArray exp) {
        return mEvaluator.isEvaluable(exp);
    }

    public Boolean visitExprPeek(ExprPeek exp) {
        return Boolean.FALSE;
    }

    public Boolean visitExprPop(ExprPop exp) {
        return Boolean.FALSE;
    }

    public Boolean visitExprConstStr(ExprConstStr exp) {
        return Boolean.TRUE;
    }

    public Boolean visitExprConstChar(ExprConstChar exp) {
        return Boolean.TRUE;
    }

    public Boolean visitExprConstBoolean(ExprConstBoolean exp) {
        return Boolean.TRUE;
    }

    public Boolean visitExprConstInt(ExprConstInt exp) {
        return Boolean.TRUE;
    }

    public Boolean visitExprConstFloat(ExprConstFloat exp) {
        return Boolean.TRUE;
    }

    public Boolean visitExprFunCall(ExprFunCall call) {
        Expression ret = (Expression) call.accept(mEvaluator);
        return ret instanceof ExprFunCall ? Boolean.FALSE : (Boolean)ret.accept(this);
    }

    public Boolean visitExprTypeCast(ExprTypeCast cast) {
        return (Boolean)cast.getExpr().accept(this);
    }
}
