package esms.streamit

import streamit.frontend.nodes.Program;
import streamit.frontend.nodes.SCSimple;
import streamit.frontend.nodes.StreamSpec;
import streamit.frontend.nodes.TypePrimitive;

/**
 * Created by farleylai on 11/8/14.
 */
public class FileReader extends Filter {
    protected static StreamSpec spec(SCSimple sc) {
        try {
            String name;
            Program prog;
            TypePrimitive type = (TypePrimitive) sc.getTypes().get(0);
            switch(type.getType()) {
                case TypePrimitive.TYPE_INT:
                    name = "Int" + sc.getName();
                    prog = StreamItAdapter.parse(StreamItAdapter.lib(name), false);
                    break;
                case TypePrimitive.TYPE_FLOAT:
                    name = "Float" + sc.getName();
                    prog = StreamItAdapter.parse(StreamItAdapter.lib(name), false);
                    break;
                default:
                    throw new RuntimeException("Unsupported FileReader<" + type + ">");
            }
            return prog.getStreams().find { it.getName().equals(name) }
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    public FileReader(Stream parent, SCSimple sc) {
        super(parent, sc, spec(sc));
        set(1, 0, 0);
    }
}
