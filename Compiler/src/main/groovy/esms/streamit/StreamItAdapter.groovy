package esms.streamit

import antlr.RecognitionException
import antlr.TokenStreamException
import esms.api.Channel
import esms.api.Component
import esms.api.SFG
import esms.util.Log
import streamit.frontend.ToAST
import streamit.frontend.nodes.*
import streamit.frontend.passes.SemanticChecker
/**
 * Created by farleylai on 9/1/14.
 */
public class StreamItAdapter extends FEReplacer {
    public final static String LIBRARY = "../StreamIt/src/main/streamit";
    private static final String TAG = StreamItAdapter.class.getSimpleName();

    public static String lib(String stream) {
        return LIBRARY + File.separator + stream + ".str";
    }

    public static Program parse(String filename, boolean check) throws RecognitionException, TokenStreamException, IOException {
        return parse(Collections.singletonList(filename), check);
    }

    public static Program parse(List<String> filenames) throws RecognitionException, TokenStreamException, IOException {
        return parse(filenames, true);
    }

    public static Program parse(List<String> filenames, boolean check) throws RecognitionException, TokenStreamException, IOException {
        Program prog = ToAST.parseFiles(filenames);
//        prog = (Program) prog.accept(new StmtSendToHelper());
//        prog = (Program) prog.accept(new RenameBitVars());
        if (check && !SemanticChecker.check(prog))
            throw new RuntimeException("Semantic checks failed");
//        prog = (Program) prog.accept(new AssignLoopTypes());
//        if (prog == null)
//            throw new RuntimeException("Failed to assign loop types");
        return (Program)prog.accept(new MakeBodiesBlocks());
    }


    private Program mProgram;
    private Map<Stream, StreamItEvaluator> mInstances;
    private Stack<Stream> mTrack;
    private SFG mSFG;
    private Stream mGlobal, mTop;

    public StreamItAdapter(String filename) throws Exception {
        this([filename] as String[]);
    }

    public StreamItAdapter(List<File> files) throws Exception {
        String[] filenames = new String[files.size()];
        for(int i = 0; i < files.size(); i++)
            filenames[i] = files.get(i).getAbsolutePath();
        mProgram = parse(Arrays.asList(filenames));
        mInstances = new HashMap<>();
        mTrack = new Stack<>();
    }

    public StreamItAdapter(String... filenames) throws Exception {
        mProgram = parse(Arrays.asList(filenames));
        mInstances = new HashMap<>();
        mTrack = new Stack<>();
    }

    /**
     * Return the evaluator corresponding to the stream.
     *
     * @param s
     * @return
     */
    public StreamItEvaluator evaluator(Stream s) {
        if(s == null)
            return null;

        if(!mInstances.containsKey(s)) {
            if(s == mGlobal) {
                StreamItEvaluator evaluator = new StreamItEvaluator(mProgram, s);
                evaluator.registerStreamFields(s.spec());
                mInstances.put(s, evaluator);
            } else {
                if (mTrack.isEmpty())
                    mInstances.put(s, new StreamItEvaluator(mProgram, s, evaluator(mGlobal)));
                else {
                    StreamItEvaluator parent = mInstances.get(mTrack.peek());
                    mInstances.put(s, new StreamItEvaluator(mProgram, s, parent));
                }
            }
        }
        return mInstances.get(s);
    }

    /**
     * Return the evaluator corresponding to the parent stream which is assumed present.
     *
     * @return
     */
    public StreamItEvaluator evaluator() {
        return mInstances.get(mTrack.peek());
    }

    public Stream stream() { return mTrack.peek(); }

    public Stream global() { return mGlobal; }

    public Stream top() { return mTop; }

    public static boolean isTopLevelSpec(StreamSpec spec) {
        StreamType st = spec.getStreamType();
        return spec.getType() != StreamSpec.STREAM_GLOBAL &&
                        st != null &&
                        st.getIn() instanceof TypePrimitive &&
                        ((TypePrimitive) st.getIn()).getType() == TypePrimitive.TYPE_VOID &&
                        st.getOut() instanceof TypePrimitive &&
                        ((TypePrimitive) st.getOut()).getType() == TypePrimitive.TYPE_VOID;
    }

    public static boolean isGlobal(StreamSpec spec) {
        return spec.getType() == StreamSpec.STREAM_GLOBAL;
    }

    private StreamSpec getStreamSpec(String name) {
        return mProgram.getStreams().find { it.getName().equals(name) };
    }

    private StreamSpec getGlobalStreamSpec() {
        return mProgram.getStreams().find { isGlobal(it) };
    }

    private StreamSpec getTopLevelStreamSpec() {
        return mProgram.getStreams().find { isTopLevelSpec(it) }
    }

    public Program program() {
        return mProgram;
    }

    public SFG read() throws IOException {
        if (mSFG != null)
            return mSFG;

        mSFG = new SFG();
        // parse the global stream if any
        StreamSpec global = getGlobalStreamSpec()
        if(global) global.accept(evaluator(mGlobal = new Filter(null, global)))
        if(mGlobal == null)
            evaluator(mGlobal = new Filter(null, new StreamSpec(null, StreamSpec.STREAM_GLOBAL, new StreamType(null, TypePrimitive.voidtype, TypePrimitive.voidtype), "global", Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST)));

        // parse the top level pipeline
        mTop = (Stream) getTopLevelStreamSpec().accept(this);
        if (!mTrack.isEmpty()) {
            Log.i(mTrack);
            throw new RuntimeException("Unpopped AST nodes: " + mTrack.size());
        }

        if (mTop == null)
            throw new RuntimeException("Found empty StreamIt streams");

        Log.i(TAG, "========== Constructing CSense SFG from StreamIt streams ==========");
        Stack<Stream> stack = new Stack<>();
        stack.push(mTop);
        while (!stack.isEmpty()) {
            Stream src = stack.pop();
            if (src.first() != src) {
                // pipeline || splitjoin
                stack.push(src.first());
                continue;
            }

            Log.d(TAG, "traversing stream: %s with %d output(s)\n", src, src.outDegrees());
            List<Stream> outputs = new ArrayList<>();
            src.getOutputs().collect { it.first() }.findAll { !mSFG.contains(src.name(), it.name()) }.each { sink ->
                int push = src.getPushRate(sink);
                int peek = sink.getPeekRate(src);
                int pop = sink.getPopRate(src);
                Channel ch = mSFG.link(src, sink, push, peek, pop);
                outputs.add(sink);
                Log.d(TAG, "SFG links %s\n", mSFG.toString(ch));
            }
            Collections.reverse(outputs);
            outputs.each { stack.push(it) }
        }

        // interpretating component operations
        for(Component c: mSFG.components()) {
            Stream s = (Stream)c;
            if (s.isFilter()) {
                // FIXME Skip simulating stream operations to save time
                //StreamItSimulator simulator = new StreamItSimulator(evaluator(s));
                //simulator.simulate((Filter)s);
                //mSFG.annotate(s, mSFG.getOutputChannel(s, 0), ((Filter)s).interpret(mSFG));
            } else if (s.isSplitter()) {
                mSFG.annotateSplitter(s, ((Splitter) s).weights());
            } else if (s.isJoiner()) {
                mSFG.annotateJoiner(s, ((Joiner) s).weights());
            }
        }
        return mSFG;
    }

    @Override
    public Object visitProgram(Program prog) {
//        Log.i("visitProgram()");
        return super.visitProgram(prog);
    }

    /**
     * Only the top level pipeline specs are visited.
     * Filter are only visited to get rates and simulated later.
     *
     * @param spec
     * @return
     */
    @Override
    public Stream visitStreamSpec(StreamSpec spec) {
        //Log.i("visitStreamSpec(): name=%s of %s in top level\n", spec.getName(), spec.getTypeString());
        Stream s;
        switch (spec.getType()) {
            case StreamSpec.STREAM_PIPELINE:
                s = new Pipeline(null, spec); break;
            default:
                throw new RuntimeException("Unexpected top level stream: " + spec.getTypeString());
        }
        evaluator(s).registerStreamFields(spec);
        mTrack.push(s);
        super.visitStreamSpec(spec);
        return mTrack.pop();
    }

    /**
     * FIXME Max rates must be constant.
     * Captures dynamic rates.
     *
     * @param s
     * @return
     */
    private Filter set(Filter s) {
        FuncWork work = s.spec().getWorkFunc();
        Expression push = work.getPushRate();
        Expression peek = work.getPeekRate();
        Expression pop  = work.getPopRate();
        ExprConstInt ZERO = new ExprConstInt(0);
        int pushV = 0;
        int peekV = 0;
        int popV = 0;
        boolean pushDynamic = true;
        boolean peekDynamic = true;
        boolean popDynamic = true;
        if(push instanceof ExprDynamicToken) {
            pushV = -1;
            throw new RuntimeException("Dynamic push rate are not supported yet");
        } else if(push instanceof ExprRange) {
            ExprRange range = (ExprRange)push;
            pushV = evaluator(s).toInt(range.max);
        } else {
            pushV = evaluator(s).toInt(push == null ? ZERO : push);
            pushDynamic = false;
        }

        if(peek instanceof ExprDynamicToken) {
            peekV = -1;
            throw new RuntimeException("Dynamic peek rate are not supported yet");
        } else if(peek instanceof ExprRange) {
            ExprRange range = (ExprRange)peek;
            peekV = evaluator(s).toInt(range.max);
        } else {
            peekV = evaluator(s).toInt(peek == null ? ZERO : peek);
            peekDynamic = false;
        }

        if(pop instanceof ExprDynamicToken) {
            popV = -1;
            throw new RuntimeException("Dynamic pop rate are not supported yet");
        } else if(pop instanceof ExprRange) {
            ExprRange range = (ExprRange)pop;
            popV = evaluator(s).toInt(range.max);
        } else {
            popV = evaluator(s).toInt(pop == null ? ZERO : pop);
            popDynamic = false;
        }

        s.set(pushV, peekV, popV);
        s.set(pushDynamic, peekDynamic, popDynamic);
        return s;
    }

    /**
     * Visit spec with parent evaluator.
     *
     * @param creator
     * @return
     */
    @Override
    public Object visitSCAnon(SCAnon creator) {
//        Log.i("visitSCAnon(): %s, %s, 0x%X\n", creator.spec.name, creator.getSpec().getTypeString(), creator.spec.hashCode());
        StreamSpec spec = creator.getSpec();
        Stream s;
        switch (spec.getType()) {
            case StreamSpec.STREAM_PIPELINE:
                s = new Pipeline(mTrack.peek(), spec);
                evaluator(s).registerStreamFields(spec);
                mTrack.peek().add(s);
                mTrack.push(s);
                super.visitStreamSpec(spec);
                mTrack.pop();
                break;
            case StreamSpec.STREAM_SPLITJOIN:
                s = new SplitJoin(mTrack.peek(), spec);
                evaluator(s).registerStreamFields(spec);
                mTrack.peek().add(s);
                mTrack.push(s);
                mSFG.add(s.first());
                super.visitStreamSpec(spec);
                mSFG.add(s.last());
                mTrack.pop();
                break;
            case StreamSpec.STREAM_FILTER:
                // constant propogation
//                Log.i("Constant propagation for anon filter: ${spec.getTypeString()}")
                def hash = creator.spec.hashCode()
                StreamSpec ret = (StreamSpec) spec.accept(new ConstantPropagator(evaluator(mTrack.peek())));
                if(ret.is(spec)) {
                    // same
                    spec.name = "Filter${hash}"
                } else {
                    spec = ret;
                    spec.name = "Filter${spec.hashCode()}"
                }
                s = new Filter(mTrack.peek(), spec);
                evaluator(s).registerStreamFields(spec);
                set(s);
                mTrack.peek().add(s);
                mSFG.add(s);
                break;
            default:
                throw new RuntimeException("Unexpected anonymous stream type: " + spec.getTypeString());
        }

//        Log.i("exitSCAnon(): %s\n", creator.getSpec().getTypeString());
        return creator;
    }


    @Override
    public Object visitSCSimple(SCSimple sc) {
//        Log.i("visitSCSimple(): %s\n", sc.getName());
        StreamSpec spec = getStreamSpec(sc.getName());
        Stream s;
        if(spec == null) {
            // built-in filters
            s = Filter.newInstance(mTrack.peek(), sc)
            s.set(evaluator(s))
            spec = s.spec()
        } else {
            switch (spec.getType()) {
                case StreamSpec.STREAM_PIPELINE:
                    s = new Pipeline(mTrack.peek(), sc, spec);
                    break;
                case StreamSpec.STREAM_SPLITJOIN:
                    s = new SplitJoin(mTrack.peek(), sc, spec);
                    break;
                case StreamSpec.STREAM_FILTER:
                    s = new Filter(mTrack.peek(), sc, spec);
                    break;
                default:
                    throw new RuntimeException("Unsupported stream type: " + spec.getTypeString());
            }
        }

        evaluator(s).registerStreamParams(spec.getParams(), sc.getParams());
        evaluator(s).registerStreamFields(spec);
        mTrack.peek().add(s);
        if(s.isPipeline()) {
            mTrack.push(s);
            super.visitStreamSpec(spec);
            mTrack.pop();
        } else if(s.isSplitJoin()) {
            mTrack.push(s);
            mSFG.add(s.first());
            super.visitStreamSpec(spec);
            mSFG.add(s.last());
            mTrack.pop();
        } else if(s.isFilter()) {
            if(!((Filter)s).isBuiltIn())
                set((Filter)s);
            mSFG.add(s);
        }
//        Log.i("exitSCSimple(): %s\n", sc.getName());
        return sc;
    }

    @Override
    public Object visitStmtVarDecl(StmtVarDecl stmt) {
//        Log.i("visitStmtVarDecl(): " + stmt);
        StreamItEvaluator evaluator = evaluator(mTrack.peek());
        for(int i = 0; i < stmt.getNumVars(); i++) {
            Type type = stmt.getType(i);
            if (type instanceof TypeArray) {
                TypeArray ta = (TypeArray) type;
                type = new TypeArray(ta.getBase(), (Expression) ta.getLength().accept(this));
            }
            String name = stmt.getName(i);
            Expression init = stmt.getInit(i);
            init = init == null ? evaluator.initialize(type) : evaluator.eval(stmt.getInit(i));
            evaluator.registerVar(name, type, init, SymbolTable.KIND_LOCAL);
        }
        return stmt;
    }

    @Override
    public Object visitSJDuplicate(SJDuplicate sj) {
//        Log.i("visitSJDuplicate()");
        mTrack.peek().add();
        return super.visitSJDuplicate(sj);
    }

    @Override
    public Object visitSJRoundRobin(SJRoundRobin sj) {
//        Log.i("visitSJRoundRobin(%s)\n", sj.getWeight());
        mTrack.peek().add(evaluator().toInt(sj.getWeight()));
        return super.visitSJRoundRobin(sj);
    }

    @Override
    public Object visitSJWeightedRR(SJWeightedRR sj) {
//        Log.i("visitSJWeightedRR(%s)\n", sj.getWeights());
        int[] weights = sj.getWeights().collect { evaluator().toInt(it) } as int[]
        mTrack.peek().add(weights);
        return super.visitSJWeightedRR(sj);
    }

    @Override
    public Object visitStmtAdd(StmtAdd stmt) {
//        Log.i("visiting StmtAdd()");
        return super.visitStmtAdd(stmt);
    }

    /**
     * Only visit assignments in init().
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
//        Log.i("visiting visitStmtAssign(): " + stmt);
        return stmt.accept(evaluator());
    }

    @Override
    public Expression visitExprUnary(ExprUnary exp) {
        exp.accept(evaluator());
        return exp;
    }

    /**
     * Only visit assignments in init().
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtFor(StmtFor stmt) {
//        Log.i("visitStmtFor()");
        stmt.getInit().accept(evaluator());
        Expression cond = stmt.getCond();
        int i = 0;
        while(evaluator().toBoolean(cond)) {
//            Log.i("For loop iteration %d\n", i++);
            stmt.getBody().accept(this);
//            stmt.getBody().accept(evaluator());
            stmt.getIncr().accept(evaluator());
        }
        return stmt;
    }

    /**
     * No push/peek/pop are expected in stream construction.
     * Conditional statements must be evaluable.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtIfThen(StmtIfThen stmt) {
        if(evaluator().toBoolean(stmt.getCond())) {
            stmt.getCons().accept(this);
        } else if(stmt.getAlt() != null)
            stmt.getAlt().accept(this);
        return stmt;
    }
}
