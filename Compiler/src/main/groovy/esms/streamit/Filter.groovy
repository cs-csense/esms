package esms.streamit

import esms.api.SFG;
import esms.api.Scheduler
import esms.core.OP;
import esms.core.Operation;
import esms.types.Strategy;
import esms.optimization.Optimizer;
import esms.util.Log
import streamit.frontend.nodes.ExprBinary
import streamit.frontend.nodes.ExprConstInt
import streamit.frontend.nodes.ExprConstant
import streamit.frontend.nodes.ExprPop
import streamit.frontend.nodes.Expression
import streamit.frontend.nodes.FieldDecl
import streamit.frontend.nodes.FuncWork
import streamit.frontend.nodes.Function
import streamit.frontend.nodes.Parameter;
import streamit.frontend.nodes.SCSimple
import streamit.frontend.nodes.Statement
import streamit.frontend.nodes.StmtBlock
import streamit.frontend.nodes.StmtBody
import streamit.frontend.nodes.StmtEmpty
import streamit.frontend.nodes.StmtPush;
import streamit.frontend.nodes.StreamSpec
import streamit.frontend.nodes.StreamType;
import streamit.frontend.nodes.Type;
import streamit.frontend.nodes.TypePrimitive;

/**
* Created by farleylai on 9/16/14.
*/
public class Filter extends Stream {
    protected final String TAG = name();
    static Function EMPTY_INIT = Function.newInit(null, new StmtBlock(null, []))

    static FuncWork newWork(int push, int peek, int pop, Statement body = new StmtBlock(null, [])) {
        return newWork(new ExprConstInt(push), new ExprConstInt(peek), new ExprConstInt(pop), body)
    }
    static FuncWork newWork(Expression push, Expression peek, Expression pop, Statement body = new StmtBlock(null, [])) {
        return new FuncWork(null, Function.FUNC_WORK, 'work', body, peek, pop, push)
    }

    static StreamSpec newStreamSpec(Type typeIn, Type typeOut, String name, int push, int peek, int pop, List<Parameter> params = [], List<FieldDecl> fields = [], Function init = EMPTY_INIT, FuncWork work = newWork(push, peek, pop), boolean stateful = false) {
        return newStreamSpec(typeIn, typeOut, name, new ExprConstInt(push), new ExprConstInt(peek), new ExprConstInt(pop), params, fields, init, work, stateful)
    }
    static StreamSpec newStreamSpec(Type typeIn, Type typeOut, String name, Expression push, Expression peek, Expression pop, List<Parameter> params = [], List<FieldDecl> fields = [], Function init = EMPTY_INIT, FuncWork work = newWork(push, peek, pop), boolean stateful = false) {
        def funcs = [init, work]
        switch(name) {
            case 'FileSink':
            case 'TCPSink':
            case 'BaselineTCPSink':
            case 'AudioSource':
            case 'AccSource':
                funcs << new Function(null, Function.FUNC_NATIVE, 'close', TypePrimitive.voidtype, [], new StmtBlock(null, []), null, null, null);
        }
        return new StreamSpec(null, StreamSpec.STREAM_FILTER, new StreamType(null, typeIn,  typeOut), name, params, fields, funcs, stateful);
    }

    static Filter newInstance(Stream parent, SCSimple sc) {
        switch(sc.getName()) {
            case 'Identity':
                Type type = sc.getTypes().isEmpty() ? null : (Type)sc.getTypes().get(0);
                StmtBlock body = new StmtBlock(sc.getContext(), [new StmtPush(sc.getContext(), new ExprPop(sc.getContext()))]);
                return new Filter(parent, sc, newStreamSpec(type, type, sc.getName(), 1, 1, 1, [], [], EMPTY_INIT, newWork(1, 1, 1, body))).set(1, 1, 1)
            case 'FileReader':
                def params = [new Parameter(TypePrimitive.stringtype, 'filename')] as List<Parameter>
                return new Filter(parent, sc, newStreamSpec(TypePrimitive.voidtype, sc.getTypes()[0], sc.getName(), new ExprConstInt(1), new ExprConstInt(0), new ExprConstInt(0), params))
            case 'FileWriter':
            case 'FileSink':
                def params = [new Parameter(TypePrimitive.stringtype, 'filename'), new Parameter(TypePrimitive.inttype, 'count')] as List<Parameter>
                return new Filter(parent, sc, newStreamSpec(sc.getTypes()[0], TypePrimitive.voidtype, sc.getName(), new ExprConstInt(0), sc.getParams()[1], sc.getParams()[1], params))
            case 'TCPSink':
            case 'BaselineTCPSink':
                def params = [new Parameter(TypePrimitive.stringtype, 'host'),
                              new Parameter(TypePrimitive.inttype, 'port'),
                              new Parameter(TypePrimitive.inttype, 'count')] as List<Parameter>
                return new Filter(parent, sc, newStreamSpec(sc.getTypes()[0], TypePrimitive.voidtype, sc.getName(), new ExprConstInt(0), sc.getParams()[2], sc.getParams()[2], params))
            case 'AudioSource':
                def params = [new Parameter(TypePrimitive.inttype, 'rate'),
                              new Parameter(TypePrimitive.inttype, 'count')] as List<Parameter>
                return new Filter(parent, sc, newStreamSpec(TypePrimitive.voidtype, sc.getTypes()[0], sc.getName(), sc.getParams()[1], new ExprConstInt(0), new ExprConstInt(0), params))
            case 'AccSource':
                def params = [new Parameter(TypePrimitive.inttype, 'count')] as List<Parameter>
                Expression push = sc.getParams()[0];
                push = new ExprBinary(push.getContext(), ExprBinary.BINOP_MUL, push, new ExprConstInt(3));
                return new Filter(parent, sc, newStreamSpec(TypePrimitive.voidtype, sc.getTypes()[0], sc.getName(), push, new ExprConstInt(0), new ExprConstInt(0), params))
            default:
                throw new RuntimeException("Unsupported named stream " + sc.getName());

        }
    }


    protected StreamItSimulation mSimulation;
    protected boolean mProductive;
    protected boolean mReordering;
    protected boolean mReinterpreted;
    protected boolean mConflited;
    protected int mPush;
    protected int mPeek;
    protected int mPop;
    protected boolean mPushDynamic;
    protected boolean mPeekDynamic;
    protected boolean mPopDynamic;

    public Filter(Stream parent, SCSimple sc, StreamSpec spec) {
        super(parent, sc, spec);
    }

    public Filter(Stream parent, StreamSpec spec) {
        super(parent, spec);
    }

    public List<Operation> operations() { return mSimulation.operations(); }

    public Filter productive() {
        mProductive = true;
        return this;
    }

    public Filter reinterpret() {
        mReinterpreted = true;
        return this;
    }

    /**
     * TODO Support TypeStruct
     * Return output sample size for CREATE and INSERT operations.
     *
     * @return
     */
    public int sampleSize() {
        TypePrimitive type = isSink() ? (TypePrimitive)spec().getStreamType().getIn() : (TypePrimitive)spec().getStreamType().getOut();
//        type = (type.getType() == TypePrimitive.TYPE_VOID) ? (TypePrimitive)spec().getStreamType().getOut() : type;
        switch(type.getType()) {
            case TypePrimitive.TYPE_CHAR: return 1;
            case TypePrimitive.TYPE_INT: return 4;
            case TypePrimitive.TYPE_FLOAT: return 4;
            case TypePrimitive.TYPE_DOUBLE: return 8;
        }
        throw new RuntimeException("Unknown sample size of type: " + type);
    }

    /**
     * Interprets primitive stream operations in CREATE/INSERT/UPDATE/PASS.
     * INSERT: position in source window.
     * UPDATE | PASS: position in sink window.
     *
     * @param sfg
     * @return
     */
    public List<Operation> interpret(SFG sfg) {
        List<Operation> operations = mSimulation.operations();
        Log.i(TAG, "========== Raw Operations of %s ===========\n", name());
        operations.each { operation ->
            Log.d(TAG, "%s: %s\n", operation, mSimulation.isConflicted(operation) ? "conflicted" : "conflictless");
        }
        if(basename().equals("Identity"))
            return operations;
        operations = operations.findAll { it.op() == OP.PUSH }
        List<Operation> interpretations = new ArrayList<>();
        if(!isSafe()) {
            // unsafe: non-deterministic stream operations, e.g. MergeSort ==> indirect peek/pop and insert to push
            interpretations.add(Operation.insert(0, mPush, sampleSize()));
            mProductive = mReinterpreted = true;
            mReordering = false;
            Log.i(TAG, "Reinterpreting unsafe %s as %s\n", this, interpretations.get(0));
        } else {
            // input independent or safe with input dependent variable assignments, e.g. MP3Decoder ==> update to push
            boolean isCreate = operations.every { it.getInt(1) < 0 && !mSimulation.isConflicted(it, it.pos())}
            if (isCreate) {
                // CREATE if all pushes are conflictless and new
//                Log.d("Interpreting raw push/peek/pop as CREATE...");
                if (!operations.isEmpty()) {
                    int beginning = operations.get(0).getInt(0);
                    int end = operations.get(operations.size() - 1).getInt(0);
                    interpretations.add(Operation.create(end - beginning + 1, sampleSize()));
                }
            } else {
                // INSERT: conflicts with later PUSH or PASS
                // UPDATE: push to conflictless position (already popped or peeked and not passed later)
                // PASS: push by passing values intact
//                Log.i("Interpreting raw push/peek/pop as INSERT | UPDATE | PASS...");
                int passes = 0;
                int updates = 0;
                int pos = 0;
                for (int i = 0; i < operations.size(); i++) {
                    Operation operation = operations.get(i);
                    int val = operation.getInt(1);
                    if (val >= 0) {
                        if (isInputDependent() && isSafe()) {
                            // force UPDATE
                            interpretations.add(Operation.update(val, 1));
                            updates++;
                        } else {
                            // PASS
                            interpretations.add(Operation.pass(sfg.getInputChannel(this, 0).src(), val, 1));
                            passes++;
                        }
                        pos = val + 1;
                    } else {
//                    Log.i("isConflicted(%s, %d): %s\n", operation, pos, mSimulation.isConflicted(operation, pos));
                        boolean conflicted = mSimulation.isConflicted(operation, pos);
                        boolean outside = (updates + passes) > mPop || pos >= mPop;
                        if (conflicted || outside) {
                            // INSERT: conflicted with a later push or beyond the pop range
                            if (conflicted)
                                Log.d("%s is conflicted with a later push\n", operation);
                            if (outside) Log.d("%s out of pop range\n", operation);
                            Operation prev = interpretations.isEmpty() ? null : interpretations.get(interpretations.size() - 1);
                            if (prev != null && prev.op() == OP.INSERT)
                                interpretations.add(Operation.insert(prev.pos() + 1, 1, sampleSize()));
                            else
                                interpretations.add(Operation.insert(pos, 1, sampleSize()));
                        } else {
                            // UPDATE: conflictless
                            interpretations.add(Operation.update(pos, 1));
                            updates++;
                            pos++;
                        }
                    }
                }

//            Log.i("========== Not Merged Annotated Operations of %s ===========\n", name());
//            interpretations.forEach(Log::i);
                // merge consecutive operations of the same op
                List<Operation> merged = new ArrayList<>();
                Operation prev = null;
                pos = -1;
                int size = 0;
                for (Operation operation : interpretations) {
                    if (prev == null) {
                        pos = operation.pos();
                        size = operation.size();
                    } else {
                        if (prev.op() == operation.op() && operation.pos() == pos + size) {
                            // consecutive
                            size += operation.size();
                        } else {
                            // new operation merge
                            switch (prev.op()) {
                                case OP.PASS:
                                    merged.add(Operation.pass(prev.src().get(), pos, size));
                                    break;
                                case OP.UPDATE:
                                    merged.add(Operation.update(pos, size));
                                    break;
                                case OP.INSERT:
                                    merged.add(Operation.insert(pos, size, prev.sampleSize(), false));
                                    break;
                            }
                            pos = operation.pos();
                            size = operation.size();
                        }
                    }
                    prev = operation;
                }
                if (size > 0) {
                    switch (prev.op()) {
                        case OP.PASS:
                            merged.add(new Operation(prev.op(), prev.src().get(), pos, size));
                            break;
                        case OP.UPDATE:
                            merged.add(new Operation(prev.op(), pos, size));
                            break;
                        case OP.INSERT:
                            merged.add(new Operation(prev.op(), pos, size, prev.sampleSize()));
                            break;
                    }
                }
                interpretations = merged;
            }

            // currently productive or reordering
            mProductive = interpretations.any { it.op() != OP.PASS }
            List<Operation> passes = interpretations.findAll { it.op() == OP.PASS }
            if (!passes.isEmpty()) {
                int pos = -1;
                for (Operation pass : passes) {
                    if (pass.pos() >= pos)
                        pos = pass.pos();
                    else {
                        mReordering = true;
                        Log.i(TAG, "%s is a reordering filter\n", name());
                        break;
                    }
                }
            }

            // previously productive or reordering
            boolean prevReordering = false;
            Filter prev = null;
            if (!sfg.isSource(this) && !sfg.isSink(this) && getInput(0).isFilter()) {
                prev = (Filter) getInput(0);
                prevReordering = prev.isReordering();
            }

            // FIXME in-place reordering to save space is possible
            // Previous reordering filter is reinterpreted as INSERT to force contiguous layout
            // if(prevReordering) {
            if (prevReordering && mProductive) {
                operations = new ArrayList<>();
                operations.add(Operation.insert(0, prev.mPush, sampleSize()));
                sfg.annotate(prev, sfg.getOutputChannel(prev, 0), operations);
                prev.mProductive = prev.mReinterpreted = true;
                prev.mReordering = false;
                Log.i(TAG, "Reinterpreting previous reordering %s as %s\n", prev, operations.get(0));
            }
            mProductive = isSink() ? true : mProductive;
        }

        // Reinterpreted INSERT due to unsafe stream operations
        // Only apply optimizations on safe filters
        if(mProductive && !mReinterpreted && !isSource() && !isSink()) {
            switch (Optimizer.opt()) {
                case Strategy.INSERT_HEAD:
                    interpretations.clear();
                    interpretations.add(Operation.insert(0, mPush, sampleSize()));
                    Log.i("Interpreting %s as %s for %s\n", name(), interpretations.get(0), Optimizer.opt());
                    break;
                case Strategy.APPEND_ALWAYS:
                case Strategy.APPEND_ON_CONFLICT:
                    mConflited = interpretations.any { it.op() == OP.INSERT }
                    interpretations.clear();
                    interpretations.add(Operation.append(mPush, sampleSize()));
                    Log.i("Interpreting %s as %s for %s\n", name(), interpretations.get(0), Optimizer.opt());
                    break;
                case Strategy.IN_PLACE:
                default:
                    break
            }
        }

        Log.i(TAG, "========== Interpreted Operations of %s ===========\n", name());
        interpretations.each { Log.i(TAG, it) }
        return interpretations;
    }

    public Filter submit(StreamItSimulation simulation) {
        mSimulation = simulation.submit();
        return this;
    }

    public Filter set(StreamItEvaluator evaluator) {
        Expression push = spec().getWorkFunc().getPushRate();
        Expression peek = spec().getWorkFunc().getPeekRate();
        Expression pop = spec().getWorkFunc().getPopRate();
        mPush = push ? evaluator.toInt(push) : 0;
        mPeek = peek ? evaluator.toInt(peek) : 0;
        mPop = pop ? evaluator.toInt(pop) : 0;
        return this
    }

    public Filter set(int push, int peek, int pop) {
        mPush = push;
        if(Math.min(peek, pop) == 0)
            mPeek = mPop = Math.max(peek, pop);
        else {
            mPeek = peek;
            mPop = pop;
        }
//        Log.i("set(%d, %d, %d)->(%d, %d, %d)\n", push, peek, pop, mPush, mPeek, mPop);
        return this;
    }

    public Filter set(boolean pushDynamic, boolean peekDynamic, boolean popDynamic) {
        mPushDynamic = pushDynamic;
        mPeekDynamic = peekDynamic;
        mPopDynamic = popDynamic;
        return this;
    }

    public boolean isInputRateDynamic() {
        return mPeekDynamic || mPopDynamic;
    }

    public boolean isOutputRateDynamic() {
        return mPushDynamic;
    }

    public boolean hasRemaining() { return mPeek > mPop; }

    public Type getInputType() { return getInputType(null); }
    public Type getOutputType() { return getOutputType(null); }

    public boolean isConflited() {
        return mConflited;
    }

    @Override
    public boolean isFilter() { return true; }

    @Override
    public boolean isProductive() {
        return mProductive;
    }

    @Override
    public boolean isReordering() {
        return mReordering;
    }

    @Override
    public boolean isReinterpreted() {
        return mReinterpreted;
    }

    @Override
    public Type getInputType(Stream s) {
        return spec().getStreamType().getIn();
    }

    @Override
    public Type getOutputType(Stream s) {
        return spec().getStreamType().getOut();
    }

    @Override
    public int getPushRate(Stream s = getOutput(0)) { return mPush; }

    @Override
    public int getPeekRate(Stream s = getInput(0)) { return mPeek; }

    @Override
    public int getPopRate(Stream s = getInput(0))  { return mPop; }

    @Override
    Filter accept(Scheduler scheduler) {
        scheduler.visitFilter(this);
        return this;
    }
}
