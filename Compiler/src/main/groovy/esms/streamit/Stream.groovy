package esms.streamit

import esms.api.Scheduler
import esms.api.Component;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.SCSimple;
import streamit.frontend.nodes.StreamSpec;
import streamit.frontend.nodes.Type;
import streamit.frontend.nodes.TypePrimitive;

/**
 * A stream represents a pipeline, splitjoin, filter, splitter or joiner.
 */
public class Stream extends Component {
    private static Map<String, Integer> mInstances = new HashMap<>();
    private Stream mParent;
    private StreamSpec mSpec;       // anonymous
    private SCSimple mSC;           // named with params
    private List<Stream> mInputs;
    private List<Stream> mOutputs;

    private int mIdx;             // instance
    private String mName;
    // input dependent variable assignments ==> MP3Decoder: input depedent but safe ==>insert to push
    private boolean mInputDependent;
    // non-deterministic stream operations ==> MergeSort: input dependent and unsafe ==> indirect peek/pop and insert to push
    private boolean mSafe;

    public Stream(Stream parent, String basename) {
        super(basename);
        mParent = parent;
        mIdx = mInstances.getOrDefault(basename, 0);
        mInputs = new ArrayList<>();
        mOutputs = new ArrayList<>();
        mInputDependent = false;
        mSafe = true;
    }

    /**
     * Construct a named stream with its stream spec.
     *
     * @param sc
     * @param spec
     */
    public Stream(Stream parent, SCSimple sc, StreamSpec spec) {
        this(parent, (String)(sc == null ? (spec.getName() == null ? spec.getTypeString() : spec.getName()) : sc.getName()));
        mSC = sc;
        mSpec = spec;
//        println("Stream: ${basename()}, ${spec.getName()}, ${spec.getTypeString()}, ${(String)(sc == null ? (spec.getName() == null ? spec.getTypeString() : spec.getName()) : sc.getName())}")
        mInstances.put(basename(), mIdx + 1);
    }

    /**
     * Construct an anonymous stream with its spec.
     *
     * @param spec
     */
    public Stream(Stream parent, StreamSpec spec) {
        this(parent, null, spec);
    }

    public final int id() {
        return mIdx;
    }

    public final StreamSpec spec() {
        return mSpec;
    }

    public final List<Expression> args() {
        List<Expression> expressions = Collections.emptyList();
        return mSC == null ? expressions : mSC.getParams();
    }

    public String basename() {
        return mSC == null ? (mSpec.getName() == null ? mSpec.getTypeString() : mSpec.getName()) : mSC.getName();
    }

    public String name() {
        return mName == null ? mName = basename() + mIdx : mName;
    }

    Stream addInput(Stream s) {
        if(!mInputs.contains(s))
            mInputs.add(s);
        return this;
    }

    Stream addOutput(Stream s) {
        if(!mOutputs.contains(s))
            mOutputs.add(s);
        return this;
    }

    public final Stream link(Stream s) {
        addOutput(s);
        s.addInput(this);
        return this;
    }

    public final Stream unlink(Stream s) {
        mOutputs.remove(s);
        s.mInputs.remove(this);
        return this;
    }

    public boolean isLinked(Stream s) {
        Stream sink = s.first()
        return getOutputs().any { it.first() == sink }
    }

    boolean isPeeking() {
        Stream s = first()
        return s.getPeekRate() > s.getPopRate()
    }

    public boolean isSource() { return getInputs().isEmpty(); }

    public boolean isSink() { return getOutputs().isEmpty(); }

    public boolean isInputDependent() { return mInputDependent; }

    public boolean isSafe() { return mSafe; }

    public Stream setInputDependent() { mInputDependent = true; return this; }

    public Stream unsafe() {
        mInputDependent = true;
        mSafe = false;
        return this;
    }

    public Stream parent() { return mParent; }
    public Stream parent(Stream s) {
        mParent = s
        return this
    }

    public final List<Stream> getInputs() {
        return mInputs.isEmpty() && mParent != null ? mParent.getInputs() : mInputs;
    }

    public final List<Stream> getOutputs() {
        return mOutputs.isEmpty() && mParent != null ? mParent.getOutputs() : mOutputs;
    }

    public final Stream getInput(int idx) {
        return inDegrees() == 0 ? null : getInputs().get(idx).last();
    }

    public final Stream getOutput(int idx) {
        return outDegrees() == 0 ? null : getOutputs().get(idx).first();
    }

    public final int inDegrees() {
        return getInputs().size();
    }

    public final int outDegrees() {
        return getOutputs().size();
    }

    public final int inputIndexOf(Stream s) {
        int idx = getInputs().indexOf(s);
        while(idx == -1 && s.parent() != null)
            idx = getInputs().indexOf(s = s.parent());
        return idx;
    }

    public final int outputIndexOf(Stream s) {
        int idx = getOutputs().indexOf(s);
        while(idx == -1 && s.parent() != null)
            idx = getOutputs().indexOf(s = s.parent());
        return idx;
    }

    public Stream first() { return this; }
    public Stream last() { return this; }

    public int getPushRate(Stream s = getOutput(0)) { return last().getPushRate(s); }
    public int getPeekRate(Stream s = getInput(0)) { return first().getPeekRate(s); }
    public int getPopRate(Stream s = getInput(0))  { return first().getPopRate(s); }

    public Type getInputType(Stream s)  { return TypePrimitive.voidtype; }
    public Type getOutputType(Stream s) { return TypePrimitive.voidtype; }
    public Stream add(Stream s)         { return this; }
    public Stream add(int... weights)   { return this; }

    public boolean isBuiltIn() {
        return basename().equals("Identity") || basename().equals("FileReader") || basename().equals("FileWriter");
    }
    public boolean isPipeline()      { return false; }
    public boolean isSplitJoin()     { return false; }
    public boolean isFilter()        { return false; }
    public boolean isSplitter()      { return false; }
    public boolean isJoiner()        { return false; }
    public boolean isProductive()    { return false; }
    public boolean isReordering()    { return false; }
    public boolean isReinterpreted() { return false; }

    boolean isInputRateDynamic() {
        return false;
    }

    boolean isOutputRateDynamic() {
        return false
    }

    def inject(v, Closure closure) {
        closure(v, this)
    }
    def reverseInject(v, Closure closure) {
        closure(v, this)
    }

    def reverseEach(Closure closure) {
        closure(this)
    }

    def each(Closure closure) {
        closure(this)
    }

    def eachWithIndex(Closure closure) {
        closure(this, 0)
    }

    def head() {
        this
    }

    def tail() {
        this
    }

    def get(int i) {
        i == 0 ? this : null
    }

    def size() {
        1
    }

    // Phased scheduling
    Stream accept(Scheduler scheduler) { return this; }

}
