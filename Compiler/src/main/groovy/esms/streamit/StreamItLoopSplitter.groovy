package esms.streamit

import esms.api.Channel;
import esms.core.Range;
import esms.api.SFG;
import esms.core.Window
import esms.util.Log
import streamit.frontend.nodes.ExprBinary;
import streamit.frontend.nodes.ExprConstInt;
import streamit.frontend.nodes.ExprPeek;
import streamit.frontend.nodes.ExprPop;
import streamit.frontend.nodes.ExprUnary;
import streamit.frontend.nodes.ExprVar;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.FEReplacer;
import streamit.frontend.nodes.Statement;
import streamit.frontend.nodes.StmtAssign;
import streamit.frontend.nodes.StmtBlock;
import streamit.frontend.nodes.StmtBreak;
import streamit.frontend.nodes.StmtContinue;
import streamit.frontend.nodes.StmtFor;
import streamit.frontend.nodes.StmtIfThen;
import streamit.frontend.nodes.StmtPush;
import streamit.frontend.nodes.StmtVarDecl;
import streamit.frontend.nodes.Type;

/**
 * Accumulate the number of push/peek/pop in a code segment.
 *
 * Created by farleylai on 10/3/14.
 */
public class StreamItLoopSplitter extends FEReplacer {
    private StreamItAdapter mStreamIt;
    private Window mSource;
    private Window mSink;
    private StreamItEvaluator mEvaluator;
    private int mPush;
    private int mPop;

    public StreamItLoopSplitter(StreamItAdapter streamit, SFG sfg, Filter c, int g, boolean same) {
        mStreamIt = streamit;
        mEvaluator = new StreamItEvaluator(mStreamIt.program(), c, mStreamIt.evaluator(c));
        mPush = mPop = 0;
        Channel ch = sfg.getInputChannel(c, 0);
        Channel.State s = ch == null ? null : sfg.getState(ch);
        mSink = s == null ? null : s.getSinkWindow(g, 0).linearize(same);
        ch = sfg.getOutputChannel(c, 0);
        s = ch == null ? null : sfg.getState(ch);
        mSource = s == null ? null : s.getSourceWindow(g, 0).linearize(true);

        // visit init function if any to update fields
        if(c.spec().getInitFunc() != null)
            c.spec().getInitFunc().accept(mEvaluator);
    }

    @Override
    public Object visitExprUnary(ExprUnary exp) {
        super.visitExprUnary(exp);
        exp.accept(mEvaluator);
        return exp;
    }

    @Override
    public Object visitExprBinary(ExprBinary exp) {
        super.visitExprBinary(exp);
        exp.accept(mEvaluator);
        return exp;
    }

    @Override
    public Object visitExprPeek(ExprPeek peek) {
        peek = (ExprPeek) super.visitExprPeek(peek);
        int offset = mEvaluator.toInt(peek.getExpr());
        int pos = mPop + offset;
        Range range = mSink.rangeAt(pos);
        if(mPeeks != null && (mPeeks.isEmpty() || !mPeeks.get(mPeeks.size() - 1).equals(range))) {
            mPeeks.add(range);
//            Log.i("%s range: %s\n", peek, range);
//            if(mPeeks.sizeInBytes() > 1) {
//                Log.i("peek range0: " + mPeeks.get(0));
//                Log.i("peek range1: " + mPeeks.get(1));
//            }
        }
        return peek;
    }

    @Override
    public Object visitExprPop(ExprPop pop) {
//        Log.d("[StreamLoopSplitter.visitStmtAssign()] $pop: ${mSink.rangeAt(mPop)} at $mPop, mPops: $mPops");
        Range range = mSink.rangeAt(mPop++);
        if(mPops != null && (mPops.isEmpty() || !mPops.get(mPops.size() - 1).equals(range)))
            mPops.add(range);
        return pop;
    }

    @Override
    public Object visitStmtPush(StmtPush stmt) {
        stmt = (StmtPush) super.visitStmtPush(stmt);
        Range range = mSource.rangeAt(mPush++);
        if(mPushes != null && (mPushes.isEmpty() || !mPushes.get(mPushes.size() - 1).equals(range)))
            mPushes.add(range);
        return stmt;
    }

    @Override
    public Object visitStmtVarDecl(StmtVarDecl stmt) {
        super.visitStmtVarDecl(stmt);
        stmt.accept(mEvaluator);
        return stmt;
    }

    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
//        Log.d("StreamLoopSplitter.visitStmtAssign(): " + stmt);
        super.visitStmtAssign(stmt);
        stmt.accept(mEvaluator);
        return stmt;
    }

    @Override
    public Object visitStmtIfThen(StmtIfThen stmt) {
//        Log.i("visitStmtIfThen() ");
        Expression cond = stmt.getCond();
        Statement cons = stmt.getCons();
        Statement alt = stmt.getAlt();
        if (!mEvaluator.isEvaluable(stmt.getCond())) {
            cond = doExpression(cond);
            stmt = new StmtIfThen(stmt.getContext(), cond, cons, alt);
            StreamItCounter counter = new StreamItCounter(mEvaluator);
            cons.accept(counter);
            int countCons = counter.pushes() + counter.pops() + counter.peeks();
            int countAlt = 0;
            if(alt != null) {
                alt.accept(counter);
                countAlt = counter.pushes() + counter.pops() + counter.peeks() - countCons;
            }
            if(countCons + countAlt > 0)
                throw new RuntimeException(String.format("Input dependent order of push/pop are not supported yet: cons(%d) != alt(%d)\n", countCons, countAlt));
        } else if (mEvaluator.toBoolean(stmt.getCond())) {
            cons.accept(this);
        } else if(stmt.getAlt() != null) {
            alt.accept(this);
        }

        return cond == stmt.getCond() && cons == stmt.getCons() && alt == stmt.getAlt() ? stmt : new StmtIfThen(stmt.getContext(), cond, cons, alt);
    }

    private boolean mContinue;
    private boolean mBreak;
    @Override
    public Object visitStmtContinue(StmtContinue ret) {
        mContinue = true;
        return ret;
    }

    @Override
    public Object visitStmtBreak(StmtBreak ret) {
        mBreak = true;
        return ret;
    }

    private List<Range> mPushes;
    private List<Range> mPops;
    private List<Range> mPeeks;

    private StreamItLoopSplitter splitForIteration(Statement init, Statement body, int i) {
//        Log.i("splitForIteration(): init: %s, i=%d\n", init, i);
        List<Statement> stmts = new ArrayList<>();
        if(init instanceof StmtVarDecl) {
            Type type = ((StmtVarDecl) init).getType(0);
            String name = ((StmtVarDecl) init).getName(0);
            StmtVarDecl counter = new StmtVarDecl(init.getContext(), type, name, new ExprConstInt(null, i));
            stmts.add(counter);
        } else if(init instanceof StmtAssign) {
            String name = ((ExprVar)((StmtAssign) init).getLHS()).getName();
            stmts.add(new StmtAssign(null, new ExprVar(null, name), new ExprConstInt(null, i)));
        }
        if(body instanceof StmtBlock)
            stmts.addAll(((StmtBlock) body).getStmts());
        else
            stmts.add(body);
        addStatement(new StmtBlock(null, stmts));
        return this;
    }

    private StreamItLoopSplitter splitFor(Statement init, ExprBinary cond, Statement incr, Statement body, int from, int i) {
//        Log.i("splitFor(): init: %s, from=%d to i=%d\n", init, from, i);
        if(i - from == 1)
            splitForIteration(init, body, from);
        else {
            if(init instanceof StmtVarDecl) {
                Type type = ((StmtVarDecl) init).getType(0);
                String name = ((StmtVarDecl) init).getName(0);
                StmtVarDecl counter = new StmtVarDecl(init.getContext(), type, name, new ExprConstInt(null, from));
                addStatement(new StmtFor(null, counter, new ExprBinary(null, cond.getOp(), cond.getLeft(), new ExprConstInt(null, i)), incr, body));
            } else if(init instanceof StmtAssign) {
                String name = ((ExprVar)((StmtAssign) init).getLHS()).getName();
                StmtAssign counter = new StmtAssign(null, new ExprVar(null, name), new ExprConstInt(null, from));
                addStatement(new StmtFor(null, counter, new ExprBinary(null, cond.getOp(), cond.getLeft(), new ExprConstInt(null, i)), incr, body));
            }
        }
        return this;
    }

    /**
     * A loop iteration is split out if either of the following conditions holds.
     * 1.) pushes across different ranges
     * 2.) pops across different ranges
     *
     * A loop is split if either of the following holds for two subsequent iterations
     * 1.) pushes correspond to different ranges across iterations
     * 2.) pops correspond to different ranges across iterations
     * 3.) peeks correspond to different lists of ranges
     *
     * Only for loops in filter work() are supported so far.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtFor(StmtFor stmt) {
//        Log.d("[StreamItLoopSplitter.visitStmtFor()] for(%s; %s; %s)\n", stmt.getInit(), stmt.getCond(), stmt.getIncr());
        // check if no needq to split
        StreamItCounter counter = new StreamItCounter(mEvaluator);
        stmt.accept(counter);
//        Log.d("[StreamItLoopSplitter.visitStmtFor()] pushes=%d, pops=%d, peeks=%d\n", counter.pushes(), counter.pops(), counter.peeks());
        if(counter.pushes() + counter.pops() + counter.peeks() == 0)
            return stmt;

        mEvaluator.push();
        Statement init = (Statement) stmt.getInit().accept(mEvaluator);
        String count = init instanceof StmtVarDecl ? ((StmtVarDecl) init).getName(0) : ((ExprVar)((StmtAssign) init).getLHS()).getName();
        ExprBinary cond = (ExprBinary)doExpression(stmt.getCond());
        Statement body = stmt.getBody();
        Statement incr = stmt.getIncr();
        List<Range> oldPushes = mPushes;
        List<Range> oldPeeks = mPeeks;
        List<Range> oldPops = mPops;
        mPushes = new ArrayList<>();
        mPops = new ArrayList<>();
        mPeeks = new ArrayList<>();
        List<Range> prevPushes = null;
        List<Range> prevPops = null;
        List<Range> prevPeeks = null;
        int from = mEvaluator.toInt(mEvaluator.getVar(count));
        int i = from;
//        Log.i("[StreamItLoopSplitter.visitStmtFor()] counter: %d, %s: %s\n", i, cond, mEvaluator.toBoolean(cond));
        while(mEvaluator.toBoolean(cond)) {
            if(from == -1)
                from = i;
            body = (Statement) stmt.getBody().accept(this);
            incr.accept(this);
//            Log.d("prev pushes: %s, pops: %s, peeks: %s\n", prevPushes, prevPops, prevPeeks);
//            Log.d("iter pushes: %s, pops: %s, peeks: %s\n", mPushes, mPops, mPeeks);
//            Log.d("push? ${mPushes.equals(prevPushes)}, pop? ${mPops.equals(prevPops)}, peek? ${mPeeks.equals(prevPeeks)}")
            if(!mPushes.isEmpty() || !mPops.isEmpty() || !mPeeks.isEmpty()) {
                // access to push/peek/pop
                if(mPushes.size() > 1 || mPops.size() > 1) {
                    // access to multiple push/pop ranges => split loop (from, i) and iteration i
                    if(i - from > 0)
                        splitFor(init, cond, incr, body, from, i);
                    splitForIteration(init, body, i);
                    // start from scratch
                    from = -1;
                } else if(prevPushes == null && prevPops == null && prevPeeks == null) {
                } else if(!mPushes.is(prevPushes) || !mPops.is(prevPops) || !mPeeks.is(prevPeeks)){
                    // access ranges differ from previous iteration => split loop from < i and start a new loop from i
//                    Log.i("prev pushes: %s, pops: %s, peeks: %s\n", prevPushes, prevPops, prevPeeks);
//                    Log.i("iter pushes: %s, pops: %s, peeks: %s\n", mPushes, mPops, mPeeks);
                    splitFor(init, cond, incr, body, from, i);
                    from = i;
                }
                prevPushes = new ArrayList(mPushes);
                prevPops = new ArrayList(mPops);
                prevPeeks = new ArrayList(mPeeks);
                if(oldPushes != null) oldPushes.addAll(mPushes);
                if(oldPeeks != null) oldPeeks.addAll(mPeeks);
                if(oldPops != null) oldPops.addAll(mPops);
                mPushes.clear();
                mPops.clear();
                mPeeks.clear();
            }
            i = mEvaluator.toInt(mEvaluator.getVar(count));
//            Log.d("[StreamItLoopSplitter.visitStmtFor()] counter: %d, %s: %s\n", i, cond, mEvaluator.toBoolean(cond));
        }
        if(from > -1)
            splitFor(init, cond, incr, body, from, i);
        mPushes = oldPushes;
        mPeeks = oldPeeks;
        mPops = oldPops;
        mEvaluator.pop();
        return null;
    }
}
