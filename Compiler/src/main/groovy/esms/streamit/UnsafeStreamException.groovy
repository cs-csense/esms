package esms.streamit;

/**
 * Created by farleylai on 3/20/15.
 */
public class UnsafeStreamException extends Exception {
    public UnsafeStreamException(String msg) {
        super(msg);
    }
}
