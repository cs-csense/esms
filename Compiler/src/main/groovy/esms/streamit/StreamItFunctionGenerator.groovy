package esms.streamit

import esms.api.SFG
import esms.backend.Encoder
import streamit.frontend.nodes.ExprFunCall
import streamit.frontend.nodes.ExprPeek
import streamit.frontend.nodes.ExprPop
import streamit.frontend.nodes.FEReplacer
import streamit.frontend.nodes.StmtExpr
import streamit.frontend.nodes.StmtPush

class StreamItFunctionGenerator extends FEReplacer {
    SFG mSFG
    Stream mStream
    Encoder mEncoder

    StreamItFunctionGenerator(Encoder encoder, SFG sfg, Stream s) {
        mSFG = sfg
        mStream = s
        mEncoder = encoder
    }

    @Override
    public Object visitExprPeek(ExprPeek peek) {
        peek = (ExprPeek) super.visitExprPeek(peek);
        return new ExprFunCall(peek.getContext(), mEncoder.encodePeek(mSFG.indexOf(mSFG.getInputChannel(mStream, 0))), peek.getExpr());

    }

    @Override
    public Object visitExprPop(ExprPop pop) {
        return new ExprFunCall(pop.getContext(), mEncoder.encodePop(mSFG.indexOf(mSFG.getInputChannel(mStream, 0))));
    }

    @Override
    public Object visitStmtPush(StmtPush stmt) {
        stmt = (StmtPush) super.visitStmtPush(stmt);
        return new StmtExpr(stmt.getContext(), new ExprFunCall(stmt.getContext(), mEncoder.encodePush(mSFG.indexOf(mSFG.getOutputChannel(mStream, 0))), stmt.getValue()));
    }
}