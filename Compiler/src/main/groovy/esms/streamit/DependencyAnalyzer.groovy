package esms.streamit;

import java.util.HashSet;
import java.util.Set;

import esms.util.Log;
import streamit.frontend.nodes.ExprArray;
import streamit.frontend.nodes.ExprUnary;
import streamit.frontend.nodes.ExprVar;
import streamit.frontend.nodes.FEReplacer;
import streamit.frontend.nodes.Statement;
import streamit.frontend.nodes.StmtAssign;

/**
 * Accumulate the number of push/peek/pop in a code segment.
 *
 * Created by farleylai on 10/3/14.
 */
public class DependencyAnalyzer extends FEReplacer {
    private static String TAG = getClass().simpleName;
    private class DependencyDetector extends FEReplacer {
        private boolean mDependent = false;
        public boolean isDependent() { return mDependent; }
        @Override
        public Object visitExprVar(ExprVar var) {
            if(mDepVars.contains(var))
                mDependent = true;
            return var;
        }
    }

    private Set<ExprVar> mDepVars;

    public DependencyAnalyzer() {
        mDepVars = new HashSet<>();
    }

    public boolean analyze(Statement statement) {
        DependencyDetector detector = new DependencyDetector();
        statement.accept(detector);
        if(detector.isDependent()) {
            Log.d(TAG, "Detects input dependency on variables");
            Log.d(TAG, mDepVars);
            return true;
        }
        return false;
    }

    @Override
    public Object visitExprUnary(ExprUnary exp) {
        super.visitExprUnary(exp);
        switch (exp.getOp()) {
            case ExprUnary.UNOP_POSTDEC:
            case ExprUnary.UNOP_POSTINC:
            case ExprUnary.UNOP_PREDEC:
            case ExprUnary.UNOP_PREINC:
                mDepVars.add((ExprVar) exp.getExpr());
        }
        return exp;
    }

    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
        super.visitStmtAssign(stmt);
        if(stmt.getLHS() instanceof ExprArray) {
            ((ExprArray) stmt.getLHS()).getComponent();
            mDepVars.add(((ExprArray) stmt.getLHS()).getComponent());
        } else
            mDepVars.add((ExprVar) stmt.getLHS());
        return stmt;
    }
}
