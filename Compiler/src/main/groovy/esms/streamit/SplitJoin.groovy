package esms.streamit

import esms.api.Scheduler;
import esms.scheduling.PhasedScheduler;
import streamit.frontend.nodes.SCSimple;
import streamit.frontend.nodes.StreamSpec;
import streamit.frontend.nodes.Type;

/**
* Created by farleylai on 9/16/14.
*/
public class SplitJoin extends Stream {
    private Splitter mSplitter;
    private Joiner mJoiner;
    private boolean mSplitterAdded;

    public SplitJoin(Stream parent, SCSimple sc, StreamSpec spec) {
        super(parent, sc, spec);
        mSplitter = new Splitter(this);
        mJoiner = new Joiner(this);
    }

    public SplitJoin(Stream parent, StreamSpec spec) {
        this(parent, null, spec);
    }

    @Override
    public boolean isSplitJoin() {
        return true;
    }

    /**
     * The inpput type of a splitjoin is determined by the output type of the input stream.
     *
     * @param s
     * @return
     */
    @Override
    public Type getInputType(Stream s) {
        return s.last().getOutputType(this);
    }

    /**
     * The output type of a splitjoin is determined from its sole input.
     *
     * @param s
     * @return
     */
    @Override
    public Type getOutputType(Stream s) {
        return getInputType(getInput(0));
    }

    @Override
    public Splitter first() {
        return mSplitter;
    }

    @Override
    public Joiner last() {
        return mJoiner;
    }

    @Override
    public SplitJoin add(Stream s) {
        if(s == mSplitter || s == mJoiner)
            throw new RuntimeException("splitter or joiner must not be added");
        mSplitter.link(s);
        return this;
    }

    @Override
    public SplitJoin add(int... weights) {
        if(mSplitterAdded) {
            mJoiner.add(weights);
            mSplitter.getOutputs().each { it.link(mJoiner) }
        } else {
            mSplitter.add(weights);
            mSplitterAdded = true;
        }
        return this;
    }


    def size() {
        return mSplitter.getOutputs().size()
    }

    def head() {
        return mSplitter
    }

    def tail() {
        return mJoiner
    }

    def get(int i) {
        return mSplitter.getOutputs()[i]
    }

    def inject(int v, int i, Closure closure) {
        v = closure(v, mSplitter)
        v = closure(v, get(i))
        return closure(v, mJoiner)
    }

    def reverseInject(int v, int i, Closure closure) {
        v = closure(v, mJoiner)
        v = closure(v, get(i))
        return closure(v, mSplitter)
    }

    public each(Closure closure) {
        mSplitter.getOutputs().each {
            closure(it)
        }
    }

    public eachWithIndex(Closure closure) {
        mSplitter.getOutputs().eachWithIndex { Stream s, int i ->
            closure(s, i)
        }
    }

    def reverseEach(Closure closure) {
        mSplitter.getOutputs().reverseEach {
            closure(it)
        }
    }

    @Override
    SplitJoin accept(Scheduler scheduler) {
        scheduler.visitSplitJoin(this);
        return this;
    }
}
