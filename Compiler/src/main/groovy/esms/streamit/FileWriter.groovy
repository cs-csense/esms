package esms.streamit

import streamit.frontend.nodes.ExprConstStr;
import streamit.frontend.nodes.ExprFunCall;
import streamit.frontend.nodes.ExprVar;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.Program;
import streamit.frontend.nodes.SCSimple;
import streamit.frontend.nodes.StreamSpec;
import streamit.frontend.nodes.Type;
import streamit.frontend.nodes.TypePrimitive;

/**
 * Created by farleylai on 11/8/14.
 */
public class FileWriter extends Filter {
    protected static ExprFunCall fopen(ExprVar filename, String mode) {
        List<Object> args = new ArrayList<>();
        args.add(filename);
        args.add(new ExprConstStr(null, mode));
        return new ExprFunCall(null, "fopen", args);
    }

    protected static ExprFunCall fclose(ExprVar file) {
        List<Object> args = new ArrayList<>();
        args.add(file);
        return new ExprFunCall(null, "fclose", args);
    }

    protected static ExprFunCall fread(ExprVar buf, Expression size, Expression count, ExprVar file) {
        List<Object> args = new ArrayList<>();
        args.add(buf);
        args.add(size);
        args.add(count);
        args.add(file);
        return new ExprFunCall(null, "fread", args);
    }

    protected static ExprFunCall frewind(ExprVar file) {
        List<Object> args = new ArrayList<>();
        args.add(file);
        return new ExprFunCall(null, "frewind", args);
    }

    protected static ExprFunCall feof(ExprVar file) {
        List<Object> args = new ArrayList<>();
        args.add(file);
        return new ExprFunCall(null, "feof", args);
    }

    protected static ExprFunCall fprintf(ExprVar file, String fmt, Expression... params) {
        List<Object> args = new ArrayList<>();
        args.add(file);
        args.add(new ExprConstStr(null, fmt));
        args.addAll(Arrays.asList(params));
        return new ExprFunCall(null, "fprintf", args);
    }

    protected static ExprFunCall sizeof(Type type) {
        List<Object> args = new ArrayList<>();
        args.add(type);
        return new ExprFunCall(null, "sizeof", args);
    }

    protected static StreamSpec spec(SCSimple sc) {
        try {
            String name;
            Program prog;
            TypePrimitive type = (TypePrimitive) sc.getTypes().get(0);
            switch(type.getType()) {
                case TypePrimitive.TYPE_INT:
                    name = "Int" + sc.getName();
                    prog = StreamItAdapter.parse(StreamItAdapter.lib(name), false); break;
                case TypePrimitive.TYPE_FLOAT:
                    name = "Float" + sc.getName();
                    prog = StreamItAdapter.parse(StreamItAdapter.lib(name), false); break;
                default:
                    throw new RuntimeException("Unsupported FileWriter<" + type + ">");
            }
            return prog.getStreams().find { it.getName().equals(name) }
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    public FileWriter(Stream parent, SCSimple sc) {
        super(parent, sc, spec(sc));
        set(1, 0, 0);
    }
}
