package esms.streamit

import esms.util.Log;
import streamit.frontend.nodes.ExprArray;
import streamit.frontend.nodes.ExprArrayInit;
import streamit.frontend.nodes.ExprBinary;
import streamit.frontend.nodes.ExprConstBoolean;
import streamit.frontend.nodes.ExprConstChar;
import streamit.frontend.nodes.ExprConstFloat;
import streamit.frontend.nodes.ExprConstInt;
import streamit.frontend.nodes.ExprConstStr;
import streamit.frontend.nodes.ExprFunCall;
import streamit.frontend.nodes.ExprPeek;
import streamit.frontend.nodes.ExprPop;
import streamit.frontend.nodes.ExprTernary;
import streamit.frontend.nodes.ExprTypeCast;
import streamit.frontend.nodes.ExprUnary;
import streamit.frontend.nodes.ExprVar;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.FEReplacer;
import streamit.frontend.nodes.Function;
import streamit.frontend.nodes.GetExprType;
import streamit.frontend.nodes.Parameter;
import streamit.frontend.nodes.Program
import streamit.frontend.nodes.SCAnon;
import streamit.frontend.nodes.Statement;
import streamit.frontend.nodes.StmtAssign;
import streamit.frontend.nodes.StmtBlock;
import streamit.frontend.nodes.StmtFor;
import streamit.frontend.nodes.StmtIfThen;
import streamit.frontend.nodes.StmtReturn;
import streamit.frontend.nodes.StmtVarDecl;
import streamit.frontend.nodes.StreamSpec;
import streamit.frontend.nodes.SymbolTable;
import streamit.frontend.nodes.Type;
import streamit.frontend.nodes.TypeArray;
import streamit.frontend.nodes.TypeHelper;
import streamit.frontend.nodes.TypePrimitive;
import streamit.frontend.nodes.TypeStruct;
import streamit.frontend.nodes.TypeStructRef;

/**
 * Maintain the symbol table to be up to date.
 * 1.) push/pop for local evaluations.
 * 2.) values evaluated are simplified.
 * 3.) functions are evaluated as return values.
 * 4.) peek()/pop() are not evaluable.
 *
 * Created by farleylai on 9/16/14.
 */
public class StreamItEvaluator extends FEReplacer {
    private static String TAG = getClass().simpleName
    /**
     * Recursively copy array initializer.
     *
     * @param array
     * @return
     */
    public static ExprArrayInit copy(ExprArrayInit array) {
        List<Expression> elements = new ArrayList<>(array.getElements().size());
        for(Expression exp: array.getElements()) {
            if(exp instanceof ExprArrayInit)
                elements.add(copy((ExprArrayInit)exp));
            else
                elements.add(exp);
        }
        return new ExprArrayInit(null, elements);
    }

    private Program mProgram;
    private Stream mStream;
    private StreamItSymbolTable mSymTable;
    private ConstantEvaluator mConstantEvaluator;
    private DependencyAnalyzer mDependencyAnalyzer;

    public StreamItEvaluator(StreamItEvaluator parent) {
        this(parent.mProgram, parent.mStream, parent.mSymTable);
    }

    public StreamItEvaluator(Program prog, Stream s) {
        this(prog, s, (StreamItSymbolTable)null);
    }

    public StreamItEvaluator(Program prog, Stream s, StreamItEvaluator parent) {
        this(prog, s, parent == null ? null : parent.mSymTable);
    }

    public StreamItEvaluator(Program prog, Stream s, StreamItSymbolTable table) {
        mProgram = prog;
        mStream = s;
        mSymTable = new StreamItSymbolTable(table);
        mConstantEvaluator = new ConstantEvaluator(this);
        mDependencyAnalyzer = new DependencyAnalyzer();
    }

    public StreamItEvaluator push() {
        mSymTable = new StreamItSymbolTable(mSymTable);
        return this;
    }

    public StreamItEvaluator pop() {
        mSymTable = mSymTable.getParent();
        return this;
    }

    public Stream stream() {
        return mStream;
    }

    public List<Expression> decompose(Expression exp) {
        return decompose(exp, -1);
    }

    public List<Expression> decompose(Expression exp, int op) {
        ExpressionDecomposer decomposer = new ExpressionDecomposer(this, op);
        exp.accept(decomposer);
        return decomposer.expressions();
    }

    /**
     * Get expression types including peak or pop.
     *
     * @param exp   the expression to get type of
     * @return
     */
    public Type getType(Expression exp) {
        if(exp instanceof ExprFunCall) {
            ExprFunCall call = (ExprFunCall) exp;
            if(call.getName().indexOf("push") >= 0)
                return mStream.spec().getStreamType().getOut();
            else if(call.getName().indexOf("peek") >= 0 || call.getName().indexOf("pop") >= 0) {
                return mStream.spec().getStreamType().getIn();
            }
        }
        Map<String, TypeStruct> structs = mProgram.getStructs().collectEntries { [it.getName(), it] }
        Map<String, TypeHelper> helpers = mProgram.getHelpers().collectEntries { [it.getName(), it] }
//        Log.i("get %s type in stream %s\n", exp, s);
        return (Type)exp.accept(new GetExprType(mSymTable, mStream.spec().getStreamType(), structs, helpers));
    }

    /**
     * Return default initial expressions for primitive and array types.
     *
     * @param type
     * @return
     */
    public Expression initialize(Type type) {
        if(type instanceof TypeArray) {
            TypeArray t = (TypeArray)type;
            TypePrimitive tt = (TypePrimitive)t.getComponent();
            int[] dims = new int[t.getDims()];
            for(int i = 0; i < dims.length; i++) {
                dims[i] = toInt(t.getLength());
                if(i < dims.length - 1)
                    t = (TypeArray)t.getBase();
            }

            ExprArrayInit array = null;
            Expression initializer = initialize(tt);
            for(int i = 0; i < dims.length; i++) {
                int length = dims[dims.length - i -1];
                List<Expression> elements = new ArrayList<>(length);
                if(i == 0) {
                    for(int j = 0; j < length; j++)
                        elements.add(initializer);
                } else {
                    for(int j = 0; j < length; j++)
                        elements.add(copy(array));
                }
                array = new ExprArrayInit(null, elements);
            }
//            Log.i("initialized array %s of dims %s\n", array, Arrays.toString(dims));
            return array;
        } else if(type instanceof TypePrimitive) {
            switch(((TypePrimitive)type).getType()) {
                case TypePrimitive.TYPE_DOUBLE:
                case TypePrimitive.TYPE_FLOAT:
                    return new ExprConstFloat(0.0);
                case TypePrimitive.TYPE_INT:
                    return new ExprConstInt(0);
                case TypePrimitive.TYPE_BOOLEAN:
                    return new ExprConstBoolean(false);
            }
        } else if(type instanceof TypeStructRef) {
            if(((TypeStructRef)type).getName().equals("FP"))
                return new ExprVar(null, "NULL");
        }
        throw new RuntimeException("Unsupported initializer type " + type);
    }

    public int[] getArrayDims(TypeArray array) {
        int dims = array.getDims();
        int[] lengths = new int[dims];
        for(int d = 0; d < dims; d++) {
            lengths[d] = toInt(array.getLength());
            if(d < dims - 1)
                array = (TypeArray)array.getBase();
        }
        return lengths;
    }

    public ExprArray getExprArray(TypeArray arrayT, String name, int index) {
        int[] dims = getArrayDims(arrayT);
//        Log.i("arrayT: %s of dims: %s, name: %s, index: %d\n", arrayT, Arrays.toString(dims), name, index);
        // convert idx to offsets
        int[] offsets = new int[dims.length];
        int size = 0;
        for(int d = 0; d < dims.length; d++) {
            int dim = dims.length - 1 - d;
            int length = dims[dim];
            if(d == 0) {
                offsets[dim] = index % length;
                size = length;
            } else {
                offsets[dim] = index / size;
                size = length * size;
            }
        }

        // in a reverse order of TypeArray
        ExprArray array = null;
        for(int d = 0; d < offsets.length; d++) {
            if(d == 0)
                array = new ExprArray(null, new ExprVar(null, name), new ExprConstInt(offsets[d]));
            else
                array = new ExprArray(null, array, new ExprConstInt(offsets[d]));
        }
        return array;
    }

    public int kind(ExprVar var) {
        return mSymTable.lookupKind(var.getName());
    }

    /**
     * Determine if the expression can be evaluated as a constant value.
     *
     * @param exp
     * @return
     */
    public boolean isEvaluable(Expression exp) {
//        Log.d("StreamItEvaluator.isEvaluable(): " + exp);
        if(exp instanceof ExprVar) {
            return mSymTable.isEvaluable((ExprVar) exp, this);
        } else if(exp instanceof ExprArray) {
            return mSymTable.isEvaluable((ExprArray) exp, this);
        } else if(exp instanceof ExprArrayInit) {
            // array address to native functions
            return false;
        }
        return (Boolean)exp.accept(mConstantEvaluator);
    }

    public boolean isString(Expression exp) {
        Type type = getType(exp);
        return type instanceof TypePrimitive && ((TypePrimitive)type).getType() == TypePrimitive.TYPE_STRING;
    }

    public boolean isRegistered(String var) {
        return mSymTable.hasVar(var);
    }

    public boolean isRegistered(ExprVar var) {
        return mSymTable.hasVar(var.getName());
    }

    public StreamItEvaluator registerFuncParams(List<Parameter> params, List<Expression> args) {
        List<Expression> arguments = new ArrayList<>();
        args.each { arguments.add(eval(it)) }
        for(int i = 0; i < params.size(); i++) {
            Parameter param = params.get(i);
            mSymTable.registerVar(param.getName(), param.getType(), arguments.get(i), SymbolTable.KIND_FUNC_PARAM);
//            Log.i("registered func param: %s of type: %s, initializer: %s\n", param.getName(), param.getType(), toString(arguments.get(i)));
        }
        return this;
    }

    /**
     * Evaluate and register stream parameters.
     * Arguments are assumed to be evaluable.
     *
     * @param params
     * @param args
     * @return
     */
    public StreamItEvaluator registerStreamParams(List<Parameter> params, List<Expression> args) {
        List<Expression> arguments = new ArrayList<>();
        args.each { arguments.add(eval(it)) }
        for(int i = 0; i < params.size(); i++) {
            Parameter param = params.get(i);
            mSymTable.registerVar(param.getName(), param.getType(), arguments.get(i), SymbolTable.KIND_STREAM_PARAM);
//            Log.i("registered ${mStream.name()} param: %s of type: %s, initializer: %s\n", param.getName(), param.getType(), toString(arguments.get(i)));
        }
        return this;
    }

    /**
     * Evaluate and register stream fields.
     * Field initializers are assumed to be evaluable.
     *
     * @param s
     * @return
     */
    public StreamItEvaluator registerStreamFields(StreamSpec s) {
        //Log.i("registerStreamFields(): ${s.getName()}: ${s.getTypeString()}")
        s.getVars().each { var ->
            //Log.i("field names: %s, inits: %s\n", var.getNames(), var.getInits());
            for(int i = 0; i < var.getNumFields(); i++) {
                Type type = var.getType(i);
                String name = var.getName(i);
                Expression init = var.getInit(i);
                init = init == null ? initialize(type) : eval(var.getInit(i));
                mSymTable.registerVar(name, type, init, s.getType() == StreamSpec.STREAM_GLOBAL ? SymbolTable.KIND_GLOBAL : SymbolTable.KIND_FIELD);
                //Log.i("registered field: %s of type: %s, init: %s\n", name, toString(type), toString(eval(init)));
            }
        }
        if(s.getInitFunc() != null) {
            push();
            s.getInitFunc().accept(this);
            pop();
        }
        return this;
    }

    public StreamItEvaluator registerVar(String name, Type type, Object origin, int kind) {
        if(type instanceof TypePrimitive) {
            switch(((TypePrimitive)type).getType()) {
                case TypePrimitive.TYPE_FLOAT:
                case TypePrimitive.TYPE_DOUBLE:
                    if(origin instanceof ExprConstInt)
                        origin = new ExprConstFloat(Float.valueOf(origin.toString()));
                default: break
            }
        }
        mSymTable.registerVar(name, type, origin, kind);
//        Log.i("registered var: %s, type: %s, init: %s, kind: %d\n", name, type, origin, kind);
        return this;
    }

    /**
     * Get the value expression of the variable.
     *
     * @param var
     * @return the value expression which can be constant or a non-constant compound expression
     */
    public Expression getVar(ExprVar var) {
//        return mSymTable.hasVar(var.getName())
//                ? (Expression)mSymTable.lookupOrigin(var.getName())
//                : var;
        return (Expression)mSymTable.lookupOrigin(var.getName());
    }

    public Expression getVar(String var) {
        return (Expression)mSymTable.lookupOrigin(var);
    }

    public StreamItEvaluator setVar(ExprVar var, Expression value) {
        setVar(var.getName(), value);
        return this;
    }

    /**
     * Save value expression of the variable in the symbol table.
     *
     * @param name
     * @param value a constant expression or a compound expression with non-constant or conditional expressions
     * @return
     */
    public StreamItEvaluator setVar(String name, Expression value) {
        Type type = mSymTable.lookupVar(name);
        int kind = mSymTable.lookupKind(name);
        registerVar(name, type, value, kind);
        return this;
    }

    /**
     * Get array element expression from stored array initializer.
     * The value returned is evaluable only if
     * 1.) the array is evaluable
     * 2.) the array expression is evaluable
     * 3.) the value is evaluable
     * Otherwise, the value returned should not be viewed as evaluable.
     *
     * @param array
     * @return A array element which can be an array as ExprArrayInit.
     */
    public Expression getArrayElement(ExprArray array) {
        ExprArray a = array;
        ExprVar var = array.getComponent();
        ExprArrayInit init = (ExprArrayInit)getVar(var);
        int dims = ((TypeArray)getType(var)).getDims();
//        Log.i("getArrayElement(): array %s of type: %s\n", array, toString(getType(array.getComponent())));

        int exprDims = 0;
        Expression comp = a;
        while (comp instanceof ExprArray) {
            comp = ((ExprArray) comp).getBase();
            exprDims++;
        }

        if(exprDims < dims)
            Log.d(TAG, "getArrayElement(): %s: %s returns an array\n", array, toString(getType(array.getComponent())));

            // get offsets
        int[] offsets = new int[exprDims];
        for(int d = exprDims - 1; d >= 0; d--) {
            if(!isEvaluable(array.getOffset())) {
                //Log.d(TAG, "Inevaluable array offsets: " + array.getOffset());
                return a;
            }
            offsets[d] = toInt(array.getOffset());
            if(d > 0)
                array = (ExprArray) array.getBase();
        }

//        Log.i("getArrayElement(): offsets: %s, init: %s\n", Arrays.toString(offsets), init);
        for(int d = 0; d < exprDims; d++) {
            if(init.getElements().size() == offsets[d]) {
                Log.e(TAG, "%s::getArrayElement(): array %s of type: %s\n", mStream, array, toString(getType(array.getComponent())));
                Log.e(TAG, "%s::getArrayElement(): offsets: %s, init: %s\n", mStream, Arrays.toString(offsets), init);
            }

            if(d < exprDims - 1)
                init = (ExprArrayInit) init.getElements().get(offsets[d]);
            else
                return init.getElements().get(offsets[d]);
        }
        throw new RuntimeException("Invalid array access expression " + array);
    }

    /**
     * Set array element only if the array expression is evaluable.
     * If the array expression is not evaluable, the whole array
     * is marked as not evaluable.
     *
     * @param array
     * @param value
     * @return
     */
    public StreamItEvaluator setArrayElement(ExprArray array, Expression value) {
//        Log.i(TAG, "setArrayElement(): %s=%s, type: %s\n", array, value, toString((TypeArray)mSymTable.lookupVar(array.getComponent())));
        ExprVar var = array.getComponent();
        ExprArrayInit init = (ExprArrayInit)getVar(var);
        int dims = ((TypeArray)getType(var)).getDims();

        // get offsets from outermost dimensions
        int[] offsets = new int[dims];
        for(int d = dims - 1; d >= 0; d--) {
            if(!isEvaluable(array.getOffset())) {
                Log.w(TAG, "Inevaluable array offsets cause array to be not evaluable");
                return this;
            }
            offsets[d] = toInt(array.getOffset());
//            Log.i(TAG, "setArrayElement(): type: %s, offset[%d]=%d\n", toString((TypeArray)mSymTable.lookupVar(array.getComponent())), d, offsets[d]);
            if(d > 0)
                array = (ExprArray) array.getBase();
        }
//        Log.i(TAG, "setArrayElement(): offsets: %s, init: %s\n", Arrays.toString(offsets), init);
        for(int d = 0; d < dims; d++) {
            if(d < dims - 1)
                init = (ExprArrayInit) init.getElements().get(offsets[d]);
            else {
                init.getElements().set(offsets[d], value);
                return this;
            }
        }
        throw new RuntimeException("Invalid array access expression " + array);
    }

    public boolean toBoolean(Expression exp) {
        return toDouble(exp) == 0 ? false : true;
    }

    public int toInt(Expression exp) {
        exp = doExpression(exp);
        if(exp instanceof  ExprConstBoolean)
            return ((ExprConstBoolean) exp).getVal() ? 1 : 0;
        else if(exp instanceof ExprConstChar)
            return ((ExprConstChar) exp).getVal();
        else if(exp instanceof ExprConstInt)
            return ((ExprConstInt) exp).getVal();
        else if(exp instanceof ExprConstFloat)
            return (int)((ExprConstFloat) exp).getVal();
        throw new RuntimeException("Unsupported expression to int: " + exp);
    }

    public double toDouble(Expression exp) {
        exp = doExpression(exp);
        if(exp instanceof  ExprConstBoolean)
            return ((ExprConstBoolean) exp).getVal() ? 1 : 0;
        else if(exp instanceof ExprConstChar)
            return ((ExprConstChar) exp).getVal();
        else if(exp instanceof ExprConstInt)
            return ((ExprConstInt) exp).getVal();
        else if(exp instanceof ExprConstFloat)
            return ((ExprConstFloat) exp).getVal();
        throw new RuntimeException("Unsupported expression to double: " + exp);
    }

    public String toString(Expression exp) {
        if(exp instanceof  ExprConstBoolean)
            return String.valueOf(((ExprConstBoolean) exp).getVal());
        else if(exp instanceof ExprConstChar)
            return String.valueOf(((ExprConstChar) exp).getVal());
        else if(exp instanceof ExprConstFloat)
            return String.valueOf(((ExprConstFloat) exp).getVal());
        else if(exp instanceof ExprConstInt)
            return String.valueOf(((ExprConstInt) exp).getVal());
        else if(exp instanceof ExprConstStr)
            return String.valueOf(((ExprConstStr) exp).getVal());
        else if(exp instanceof ExprVar) {
            return ((ExprVar) exp).getName();
        } else if(exp instanceof ExprArray) {
            return exp.toString();
        } else if(exp instanceof ExprArrayInit) {
            return exp.toString();
        } else if(exp instanceof ExprPeek || exp instanceof ExprPop) {
            return exp.toString();
        }
        return exp.toString();
    }

    public String toString(Type type) {
        if(type instanceof TypeArray) {
            TypeArray array = (TypeArray) type;
            TypePrimitive tt = (TypePrimitive)array.getComponent();
            int[] dims = new int[array.getDims()];
            for(int i = 0; i < dims.length; i++) {
                dims[i] = toInt(array.getLength());
                if(i < dims.length - 1)
                    array = (TypeArray)array.getBase();
            }
            String offsets = String.join("", dims.collect { "[$it]"});
//            Log.d(TAG, "toString(Type): type: %s, dims: %s, %s\n", type, Arrays.toString(dims), tt + offsets);
            return "$tt$offsets";
        }
        return type.toString();
    }

    @Override
    public Object visitSCAnon(SCAnon creator) {
        // FIXME StreamItEvaluator should not visit anonymous streams diretly
        //Log.i(TAG, "StreamItEvaluator.visitSCAnon(): skips anonymous streams")
        return creator
    }

    /**
     * A simplified evaluable or compound expression is returned.
     *
     * @param exp
     * @return
     */
    public Expression eval(Expression exp) {
        return doExpression(exp);
    }

    /**
     * Assume the variable expression is rvalue.
     *
     * @param var
     * @return
     */
    @Override
    public Object visitExprVar(ExprVar var) {
        return mSymTable.hasVar(var.getName()) ? getVar(var) : var;
    }

    /**
     *  Assume the variable expression is rvalue.
     *
     * @param array
     * @return An ExprArrayInit may be returned.
     */
    @Override
    public Object visitExprArray(ExprArray array) {
//        Log.d(TAG, "StreamItEvaluator.visitExprArray(): " + array);
        return getArrayElement(array);
    }

    @Override
    public Object visitExprTypeCast(ExprTypeCast exp) {
//        Log.d(TAG, "StreamItEvaluator.visitExprTypeCast()");
        Expression value = doExpression(exp.getExpr());
        if (!isEvaluable(value))
            return exp.getExpr() == value ? exp : new ExprTypeCast(exp.getContext(), exp.getType(), value);
        TypePrimitive type = (TypePrimitive) exp.getType();
        switch (type.getType()) {
            case TypePrimitive.TYPE_DOUBLE:
            case TypePrimitive.TYPE_FLOAT:
                return new ExprConstFloat(exp.getContext(), toDouble(value));
            case TypePrimitive.TYPE_INT:
            case TypePrimitive.TYPE_BIT:
                return new ExprConstInt(exp.getContext(), toInt(value));
        }
        throw new RuntimeException("Unsupported type cast " + exp);
    }

    @Override
    public Expression visitExprUnary(ExprUnary exp) {
//        Log.d(TAG, "StreamItEvaluator.visitExprUnary():" + exp);
        if(exp.getExpr() instanceof ExprVar) {
            ExprVar var = (ExprVar)exp.getExpr();
            Expression value = getVar(var);
            if(isEvaluable(value)) {
//                Log.d(TAG, "StreamItEvaluator.visitExprUnary(): not evaluable value: " + value);
                switch (exp.getOp()) {
                    case ExprUnary.UNOP_PREDEC:
                        value = new ExprConstInt(toInt(value) - 1);
                        setVar(var, value);
//                        Log.d(TAG, "--%s: %d\n", var.getName(), toInt(value));
                        return value;
                    case ExprUnary.UNOP_PREINC:
                        value = new ExprConstInt(toInt(value) + 1);
                        setVar(var, value);
//                        Log.d(TAG, "++%s: %d\n", var.getName(), toInt(value));
                        return value;
                    case ExprUnary.UNOP_POSTDEC:
                        setVar(var, new ExprConstInt(toInt(value) - 1));
//                        Log.d(TAG, "%s--: %d->%d\n", var.getName(), toInt(value), toInt(value) - 1);
                        return value;
                    case ExprUnary.UNOP_POSTINC:
                        setVar(var, new ExprConstInt(toInt(value) + 1));
//                        Log.d(TAG, "%s++: %d->%d\n", var.getName(), toInt(value), toInt(value) + 1);
                        return value;
                    case ExprUnary.UNOP_NEG:
                        if (value instanceof ExprConstInt)
                            return new ExprConstInt(-((ExprConstInt) value).getVal());
                        else if (value instanceof ExprConstFloat)
                            return new ExprConstFloat(-((ExprConstFloat) value).getVal());
                        else
                            throw new RuntimeException("Unexpected value: " +  value)
                    case ExprUnary.UNOP_NOT:
                        return new ExprConstBoolean(!toBoolean(value));
                    case ExprUnary.UNOP_COMPLEMENT:
                        return new ExprConstInt(~toInt(value) as int);
                    default:
                        throw new RuntimeException("Unknown unary OP: " + exp.getOp());
                }
            } else {
                // in case of infinite recursion
                return new ExprUnary(exp.getContext(), exp.getOp(), value);
            }
        } else if(exp.getExpr() instanceof ExprArray) {
            ExprArray array = (ExprArray) exp.getExpr();
            Expression value = getArrayElement(array);
            if (isEvaluable(value)) {
                switch (exp.getOp()) {
                    case ExprUnary.UNOP_PREDEC:
                        value = new ExprConstInt(toInt(value) - 1);
                        setArrayElement(array, value);
//                        Log.d(TAG, "--%s: %d\n", array, toInt(value));
                        return value;
                    case ExprUnary.UNOP_PREINC:
                        value = new ExprConstInt(toInt(value) + 1);
                        setArrayElement(array, value);
//                        Log.d(TAG, "++%s: %d\n", array, toInt(value));
                        return value;
                    case ExprUnary.UNOP_POSTDEC:
                        setArrayElement(array, new ExprConstInt(toInt(value) - 1));
//                        Log.d(TAG, "%s--: %d->%d\n", array, toInt(value), toInt(value) - 1);
                        return value;
                    case ExprUnary.UNOP_POSTINC:
                        setArrayElement(array, new ExprConstInt(toInt(value) - 1));
//                        Log.d(TAG, "%s++: %d->%d\n", array, toInt(value), toInt(value) + 1);
                        return value;
                    case ExprUnary.UNOP_NEG:
                        if (value instanceof ExprConstInt)
                            return new ExprConstInt(-((ExprConstInt) value).getVal());
                        else if (value instanceof ExprConstFloat)
                            return new ExprConstFloat(-((ExprConstFloat) value).getVal());
                        throw new RuntimeException("Unexpected value: " +  value)
                    case ExprUnary.UNOP_NOT:
                        return new ExprConstBoolean(!toBoolean(value));
                    case ExprUnary.UNOP_COMPLEMENT:
                        return new ExprConstInt(~toInt(value) as int);
                    default:
                        throw new RuntimeException("Unknown unary OP: " + exp.getOp());
                }
            } else {
                // in case of infinite recursion
                return new ExprUnary(exp.getContext(), exp.getOp(), value);
            }
        } else if(exp.getExpr() instanceof ExprConstBoolean) {
            switch(exp.getOp()) {
                case ExprUnary.UNOP_NOT:
                    return new ExprConstBoolean(!toBoolean(exp.getExpr()));
                default:
                    throw new RuntimeException("Unknown unary OP: " + exp.getOp());
            }
        } else if(exp.getExpr() instanceof ExprConstChar) {
            switch(exp.getOp()) {
                case ExprUnary.UNOP_NEG:
                    return new ExprConstChar(exp.getContext(), (char)-((ExprConstChar) exp.getExpr()).getVal());
                case ExprUnary.UNOP_NOT:
                    return new ExprConstBoolean(!toBoolean(exp.getExpr()));
                case ExprUnary.UNOP_COMPLEMENT:
                    return new ExprConstChar(exp.getContext(), (char)~((ExprConstChar)exp.getExpr()).getVal());
                default:
                    throw new RuntimeException("Unknown unary OP: " + exp.getOp());
            }
        } else if(exp.getExpr() instanceof ExprConstInt) {
            switch(exp.getOp()) {
                case ExprUnary.UNOP_NEG:
                    return new ExprConstInt(-((ExprConstInt) exp.getExpr()).getVal());
                case ExprUnary.UNOP_NOT:
                    return new ExprConstBoolean(!toBoolean(exp.getExpr()));
                case ExprUnary.UNOP_COMPLEMENT:
                    return new ExprConstInt(~((ExprConstInt)exp.getExpr()).getVal());
                default:
                    throw new RuntimeException("Unknown unary OP: " + exp.getOp());
            }
        } else if(exp.getExpr() instanceof ExprConstFloat) {
            switch(exp.getOp()) {
                case ExprUnary.UNOP_NEG:
                    return new ExprConstFloat(-((ExprConstFloat) exp.getExpr()).getVal());
                case ExprUnary.UNOP_NOT:
                    return new ExprConstBoolean(!toBoolean(exp.getExpr()));
                default:
                    throw new RuntimeException("Unknown unary OP: " + exp.getOp());
            }
        } else {
            Expression expr = doExpression(exp.getExpr());
            if(expr != exp.getExpr()) {
                ExprUnary unary = new ExprUnary(exp.getContext(), exp.getOp(), expr);
                return isEvaluable(expr) ? visitExprUnary(unary) : unary;
            }
        }
        return exp;
    }

    /**
     * Return a terminal expression.
     * @param exp
     * @return
     */
    @Override
    public Object visitExprBinary(ExprBinary exp) {
        //Log.i(TAG, "StreamItEvaluator.visitExprBinary(): %s\n", exp);
        Expression lhs = exp.getLeft();
        Expression rhs = exp.getRight();
        boolean evaluable = isEvaluable(lhs) && isEvaluable(rhs);
        lhs = doExpression(lhs);
        rhs = doExpression(rhs);
        //Log.i(TAG, "StreamItEvaluator.visitExprBinary(): %s of type: %s %s\n", exp, getType(exp), evaluable ? "evaluable" : "inevaluable");
        if(evaluable && !isString(lhs) && !isString(rhs)) {
            float valR = (float)toDouble(rhs);
            float valL = (float)toDouble(lhs);
            float val = 0;
            switch(exp.getOp()) {
                case ExprBinary.BINOP_ADD:
                    val = valL + valR;
//                    Log.d(TAG, "%.3f + %.3f = %.3f\n", valL, valR, val);
                    break;
                case ExprBinary.BINOP_SUB:
                    val = valL - valR;
//                    Log.d(TAG, "%.3f - %.3f = %.3f\n", valL, valR, val);
                    break;
                case ExprBinary.BINOP_MUL:
                    val = valL * valR;
//                    Log.d(TAG, "%.3f * %.3f = %.3f\n", valL, valR, val);
                    break;
                case ExprBinary.BINOP_DIV:
                    val = valL / valR;
//                    Log.d(TAG, "%.3f / %.3f = %.3f\n", valL, valR, val);
                    break;
                case ExprBinary.BINOP_MOD:
                    return new ExprConstInt((int)valL % (int)valR);
                case ExprBinary.BINOP_AND:
                    return new ExprConstBoolean(valL + valR == 2);
                case ExprBinary.BINOP_OR:
                    return new ExprConstBoolean(valL + valR > 0);
                case ExprBinary.BINOP_EQ:
                    return new ExprConstBoolean(valL == valR);
                case ExprBinary.BINOP_NEQ:
                    return new ExprConstBoolean(valL != valR);
                case ExprBinary.BINOP_LT:
                    return new ExprConstBoolean(valL < valR);
                case ExprBinary.BINOP_LE:
                    return new ExprConstBoolean(valL <= valR);
                case ExprBinary.BINOP_GT:
                    return new ExprConstBoolean(valL > valR);
                case ExprBinary.BINOP_GE:
                    return new ExprConstBoolean(valL >= valR);
                case ExprBinary.BINOP_BAND:
                    return new ExprConstInt((int)valL & (int)valR);
                case ExprBinary.BINOP_BOR:
                    return new ExprConstInt((int)valL | (int)valR);
                case ExprBinary.BINOP_BXOR:
                    return new ExprConstInt((int)valL ^ (int)valR);
                case ExprBinary.BINOP_LSHIFT:
                    return new ExprConstInt((int)valL << (int)valR);
                case ExprBinary.BINOP_RSHIFT:
                    return new ExprConstInt((int)valL >> (int)valR);
                default:
                    throw new UnsupportedOperationException("Unsupported binary op: " + exp.getOp());
            }
            return new ExprTypeCast(exp.getContext(), getType(exp), new ExprConstFloat(exp.getContext(), val)).accept(this);
        } else {
            return lhs == exp.getLeft() && rhs == exp.getRight() ? exp : new ExprBinary(exp.getContext(), exp.getOp(), lhs, rhs);
        }
    }

    @Override
    public Object visitExprTernary(ExprTernary exp) {
        //Log.i(TAG, "StreamItEvaluator.visitExprTernary(): %s ? %s : %s\n", exp.getA(), exp.getB(), exp.getC());
        if(isEvaluable(exp.getA())) {
            boolean cond = toBoolean(exp.getA());
            return cond ? exp.getB().accept(this) : exp.getC().accept(this);
        } else {
            exp.accept(mDependencyAnalyzer);
            StreamItCounter counter = new StreamItCounter(this);
            exp.getB().accept(counter);
            exp.getC().accept(counter);
            if(counter.pushes() + counter.pops() > 0) {
                mStream.unsafe();
                return new RuntimeException("Non-deterministic ternary operator is not supported yet");
            } else {
                // in case of infinite recursion
                return new ExprTernary(exp.getContext(), exp.getOp(), doExpression(exp.getA()), exp.getB(), exp.getC());
            }
        }
    }

    @Override
    public Expression visitExprFunCall(ExprFunCall call) {
//        Log.i(TAG, "StreamItEvaluator.visitExprFunCall(): " + call);
        call = (ExprFunCall) super.visitExprFunCall(call);
//        Log.i(TAG, "StreamItEvaluator.visitExprFunCall(): replaced " + call);
        List<Expression> args = call.getParams().collect { it };
        Function func = mStream.spec().getFuncNamed(call.getName());
        if(func == null) {
            // built-in or native functions
//            Log.i(TAG, "StreamItEvaluator.visitExprFunCall(): check if args are evaluable");
            if(!args.stream().every { isEvaluable(it) }) return call;
//            Log.i(TAG, "StreamItEvaluator.visitExprFunCall(): all args are evaluable");
            String name = call.getName();
            if(name.startsWith("sin")) {
                double val = Math.sin((float)toDouble(args.get(0)));
//                Log.d(TAG, "StreamItEvaluator.visitExprFunCall(): evaluated: " + val);
                return new ExprConstFloat(val);
            } else if(name.startsWith("cos")) {
                double val = Math.cos((float)toDouble(args.get(0)));
//                Log.d(TAG, "StreamItEvaluator.visitExprFunCall(): evaluated: " + val);
                return new ExprConstFloat(val);
            } else if(name.startsWith("atan")) {
                double val = Math.atan((float)toDouble(args.get(0)));
//                Log.d(TAG, "StreamItEvaluator.visitExprFunCall(): evaluated: " + val);
                return new ExprConstFloat(val);
            } else if(name.startsWith("abs")) {
                double val = Math.abs((float)toDouble(args.get(0)));
                return new ExprTypeCast(null, getType(args.get(0)), new ExprConstFloat(val));
            } else if(name.startsWith("exp")) {
                double val = Math.exp((float)toDouble(args.get(0)));
                return new ExprTypeCast(null, getType(args.get(0)), new ExprConstFloat(val));
            } else if(name.startsWith("log")) {
                double val = Math.log((float)toDouble(args.get(0)));
                return new ExprTypeCast(null, getType(args.get(0)), new ExprConstFloat(val));
            } else if(name.startsWith("sqrt")) {
                double val = Math.sqrt((float)toDouble(args.get(0)));
                return new ExprTypeCast(null, getType(args.get(0)), new ExprConstFloat(val));
            } else
                return call;
        }

        push();
//        Log.d(TAG, "calling " + call);
        registerFuncParams(func.getParams(), args);
//        Log.d(TAG, "register function parameters");
        func = (Function)func.accept(this);
//        Log.d(TAG, "return from " + call);
        pop();
        if(func.getReturnType() == TypePrimitive.voidtype)
            return call;
        else {
            List<Statement> stmts = ((StmtBlock) func.getBody()).getStmts();
            Statement stmt = stmts.get(stmts.size() - 1);
            if(stmt instanceof StmtReturn)
                return ((StmtReturn)stmt).getValue();
            else {
                switch(((TypePrimitive)func.getReturnType()).getType()) {
                    // fake results
                    case TypePrimitive.TYPE_INT: return new ExprConstInt(0);
                    case TypePrimitive.TYPE_FLOAT: return new ExprConstFloat(0.0f);
                    default:
                        throw new RuntimeException("Return type " + func.getReturnType() + " is not supported yet");
                }
            }
        }
    }

    @Override
    public Object visitStmtVarDecl(StmtVarDecl stmt) {
//        Log.d(TAG, "StreamItEvaluator.visitStmtVarDecl(): %s by %s\n", stmt, mStream);
        List<Type> types = new ArrayList<>();
        List<Expression> inits = new ArrayList<>();
        boolean changed = false;
        for (int i = 0; i < stmt.getNumVars(); i++) {
            Type type = stmt.getType(i);
            Expression init = stmt.getInit(i);
            init = init == null ? initialize(type) : eval(init);
            registerVar(stmt.getName(i), type, init, SymbolTable.KIND_LOCAL);
            types.add(type);
            inits.add(init);
            if (type != stmt.getType(i) || init != stmt.getInit(i))
                changed = true;
//            Log.d(TAG, "StreamItEvaluator.visitStmtVarDecl(): %s is registered by %s(0x%X)\n", stmt.getName(i), mStream, hashCode());
        }

        return changed ? new StmtVarDecl(stmt.getContext(), types, stmt.getNames(), inits) : stmt;
    }

    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
//        Log.i(TAG, "StreamItEvaluator.visitStmtAssign(): %s\n", stmt);
        Expression lhs = stmt.getLHS();
        Expression rhs = stmt.getRHS();
        switch(stmt.getOp()) {
            case 0:                     // "=";
                rhs = doExpression(rhs);
                if(lhs instanceof ExprVar) {
                    setVar((ExprVar) lhs, rhs);
                } else if(lhs instanceof ExprArray) {
                    setArrayElement((ExprArray) lhs, rhs);
                } else
                    throw new RuntimeException("Unsupported LValue " + stmt.getLHS());
//              Log.i(TAG, "set %s=%s\n", toString(lhs), toString(rhs));
                return lhs == stmt.getLHS() && rhs == stmt.getRHS() ? stmt : new StmtAssign(stmt.getContext(), lhs, rhs, stmt.getOp());
            case ExprBinary.BINOP_ADD:  // "+=";
                stmt = new StmtAssign(stmt.getContext(), lhs, new ExprBinary(null, ExprBinary.BINOP_ADD, lhs, rhs), 0);
                return visitStmtAssign(stmt);
            case ExprBinary.BINOP_SUB:  // "-=";
                stmt = new StmtAssign(stmt.getContext(), lhs, new ExprBinary(null, ExprBinary.BINOP_SUB, lhs, rhs), 0);
                return visitStmtAssign(stmt);
            case ExprBinary.BINOP_MUL:  // "*=";
                stmt = new StmtAssign(stmt.getContext(), lhs, new ExprBinary(null, ExprBinary.BINOP_MUL, lhs, rhs), 0);
                return visitStmtAssign(stmt);
            case ExprBinary.BINOP_DIV:  // "/=";
                stmt = new StmtAssign(stmt.getContext(), lhs, new ExprBinary(null, ExprBinary.BINOP_DIV, lhs, rhs), 0);
                return visitStmtAssign(stmt);
            default:
                throw new RuntimeException("Unknown OP " + stmt.getOp());
        }
    }

    /**
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtIfThen(StmtIfThen stmt) {
        if(mDependencyAnalyzer.analyze(stmt))
            mStream.setInputDependent();

        Expression cond = stmt.getCond();
        Statement cons = stmt.getCons();
        Statement alt = stmt.getAlt();
        if (!isEvaluable(stmt.getCond())) {
            stmt.accept(mDependencyAnalyzer);
            StreamItCounter counter = new StreamItCounter(this);
            stmt.getCons().accept(counter);
            int countCons = counter.pushes() + counter.pops() + counter.peeks();
            int countAlt = 0;
            if (stmt.getAlt() != null) {
                stmt.getAlt().accept(counter);
                countAlt = counter.pushes() + counter.pops() + counter.peeks() - countCons;
            }
            if (countCons + countAlt > 0) {
//                Log.w(TAG, "%s is input dependent and unsafe for cons(%d) != alt(%d) => indirect peek/pop and reinterpreted as insert to push\n", mStream, countCons, countAlt);
                mStream.unsafe();
                return stmt;
//                throw new UnsafeStreamException(String.format("%s is input dependent and unsafe for cons(%d) + alt(%d) > 0=> indirect peek/pop and reinterpreted as insert to push", mStream, countCons, countAlt));
            }
        } else if (toBoolean(stmt.getCond())) {
            cons = (Statement) stmt.getCons().accept(this);
        } else if(stmt.getAlt() != null){
            alt = (Statement) stmt.getAlt().accept(this);
        }

        return cond == stmt.getCond() && cons == stmt.getCons() && alt == stmt.getAlt() ? stmt : new StmtIfThen(stmt.getContext(), cond, cons, alt);
    }

    /**
     * Only deterministic loops are supported yet.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtFor(StmtFor stmt) {
//        Log.d(TAG, "StreamItEvaluator.visitStmtFor(): for(%s; %s; %s)\n", stmt.getInit(), stmt.getCond(), stmt.getIncr());
        if(mDependencyAnalyzer.analyze(stmt))
            mStream.setInputDependent();

        Statement init = stmt.getInit();
        if(init != null)
            init.accept(this);

        Expression cond = stmt.getCond();
        Statement body = stmt.getBody();
        Statement incr = stmt.getIncr();
        if(!isEvaluable(cond)) {
            stmt.accept(mDependencyAnalyzer);
            StreamItCounter counter = new StreamItCounter(this);
            stmt.accept(counter);
            if (counter.pushes() + counter.pops() + counter.peeks() > 0) {
                mStream.unsafe();
                throw new RuntimeException("Non-deterministic loops are not supported yet");
            }
        }

        int i = 0;
        while(toBoolean(cond) && mStream.isSafe()) { // && !mStream.isInputDependent()) {
//            Log.d(TAG, "iteration: " + i++);
            cond.accept(this);
            body.accept(this);
            incr.accept(this);
        }

        return stmt;
    }
}
