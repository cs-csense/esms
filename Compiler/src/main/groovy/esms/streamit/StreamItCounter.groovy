package esms.streamit

import streamit.frontend.nodes.ExprPeek;
import streamit.frontend.nodes.ExprPop;
import streamit.frontend.nodes.ExprUnary;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.FEReplacer;
import streamit.frontend.nodes.Statement;
import streamit.frontend.nodes.StmtAssign;
import streamit.frontend.nodes.StmtFor;
import streamit.frontend.nodes.StmtIfThen;
import streamit.frontend.nodes.StmtPush;
import streamit.frontend.nodes.StmtVarDecl;

/**
 * Accumulate the number of push/peek/pop in a code segment.
 *
 * Created by farleylai on 10/3/14.
 */
public class StreamItCounter extends FEReplacer {
    private StreamItEvaluator mEvaluator;
    private int mPush;
    private int mPeek;
    private int mPop;

    public StreamItCounter(StreamItEvaluator evaluator) {
        mEvaluator = new StreamItEvaluator(evaluator);
    }

    public int pushes() {
        return mPush;
    }

    public int peeks() { return mPeek; }

    public int pops() {
        return mPop;
    }

    @Override
    public Object visitExprUnary(ExprUnary exp) {
        super.visitExprUnary(exp);
        exp.accept(mEvaluator);
        return exp;
    }

    @Override
    public Object visitStmtVarDecl(StmtVarDecl stmt) {
        super.visitStmtVarDecl(stmt);
        stmt.accept(mEvaluator);
        return stmt;
    }

    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
        super.visitStmtAssign(stmt);
        stmt.accept(mEvaluator);
        return stmt;
    }

    @Override
    public Object visitStmtPush(StmtPush stmt) {
        mPush++;
        return super.visitStmtPush(stmt);
    }

    @Override
    public Object visitExprPeek(ExprPeek peek) {
        mPeek++;
        return super.visitExprPeek(peek);
    }

    @Override
    public Object visitExprPop(ExprPop pop) {
        mPop++;
        return super.visitExprPop(pop);
    }

    /**
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtIfThen(StmtIfThen stmt) {
        if (!mEvaluator.isEvaluable(stmt.getCond())) {
            StreamItCounter counter = new StreamItCounter(mEvaluator);
            stmt.getCons().accept(counter);
            if (stmt.getAlt() != null)
                stmt.getAlt().accept(counter);
            if (counter.pushes() + counter.pops() > 0)
                throw new RuntimeException("Non-deterministic conditionals are not supported yet");
        } else if (mEvaluator.toBoolean(stmt.getCond())) {
            stmt.getCons().accept(this);
        } else if(stmt.getAlt() != null){
            stmt.getAlt().accept(this);
        }

        return stmt;
    }

    /**
     * Only deterministic loops are supported yet.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtFor(StmtFor stmt) {
//        Log.i("[StreamItCounter.visitStmtFor()] for(%s; %s; %s)\n", stmt.getInit(), stmt.getCond(), stmt.getIncr());
        Statement init = stmt.getInit();
        if(init != null) {
            init.accept(this);
            init.accept(mEvaluator);
        }

        Expression cond = stmt.getCond();
        Statement body = stmt.getBody();
        Statement incr = stmt.getIncr();
        if(!mEvaluator.isEvaluable(cond)) {
            StreamItCounter counter = new StreamItCounter(mEvaluator);
            body.accept(counter);
            incr.accept(counter);
            if (counter.pushes() + counter.pops() > 0)
                throw new RuntimeException("Non-deterministic loops are not supported yet");
        }

        int i = 0;
//        Log.i("[StreamItCounter.visitStmtFor()] counter: %d, %s: %s\n", i, cond, mEvaluator.toBoolean(cond));
        while(mEvaluator.toBoolean(cond)) {
//            Log.i("[%d] pushes: %d, pops: %d, peeks: %d\n", i, mPush, mPop, mPeek);
            cond.accept(this);
            body.accept(this);
            incr.accept(this);
            i++;
        }

        return stmt;
    }
}
