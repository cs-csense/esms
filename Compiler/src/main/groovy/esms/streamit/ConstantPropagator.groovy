package esms.streamit

import esms.util.Log
import streamit.frontend.nodes.ExprArray;
import streamit.frontend.nodes.ExprUnary;
import streamit.frontend.nodes.ExprVar;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.FEReplacer;
import streamit.frontend.nodes.StmtAssign;
import streamit.frontend.nodes.StmtVarDecl;

/**
 * Created by farleylai on 11/25/14.
 */
public class ConstantPropagator extends FEReplacer {
    private StreamItEvaluator mEvaluator;
    private Set<ExprVar> mExclusion;

    public ConstantPropagator(StreamItEvaluator evaluator) {
        mEvaluator = new StreamItEvaluator(evaluator);
        mExclusion = new HashSet<>();
    }

    private boolean isPropagatable(ExprVar var) {
        return !mExclusion.contains(var) && mEvaluator.isRegistered(var);
    }

    private boolean isPropagatable(ExprArray array) {
        return isPropagatable(array.getComponent());
    }

    /**
     * Assume the variable expression is rvalue.
     *
     * @param var
     * @return
     */
    @Override
    public Object visitExprVar(ExprVar var) {
//        Log.d("ConstantPropagator.visitExprVar(): %s propogatable? %s, evaluable? %s\n", var, isPropagatable(var), mEvaluator.isEvaluable(var));
        return isPropagatable(var) ? mEvaluator.visitExprVar(var) : var;
    }

    /**
     *  Assume the variable expression is rvalue.
     *
     * @param array
     * @return
     */
    @Override
    public Object visitExprArray(ExprArray array) {
//        Log.i("StreamItEvaluator.visitExprArray(): " + array);
        return isPropagatable(array) ? mEvaluator.visitExprArray(array) : array;
    }

    @Override
    public Expression visitExprUnary(ExprUnary exp) {
        if(exp.getExpr() instanceof ExprVar) {
            ExprVar var = (ExprVar) exp.getExpr();
            return isPropagatable(var) ? mEvaluator.visitExprUnary(exp) : exp;
        } else if(exp.getExpr() instanceof ExprArray) {
            ExprArray array = (ExprArray) exp.getExpr();
            return isPropagatable(array.getComponent()) ? mEvaluator.visitExprUnary(exp) : exp;
        } else {
            Expression expr = (Expression) exp.getExpr().accept(this);
            return expr == exp.getExpr() ? exp : new ExprUnary(exp.getContext(), exp.getOp(), expr);
        }
    }

    @Override
    public Object visitStmtVarDecl(StmtVarDecl stmt) {
//        Log.d("ConstantPropagator.visitStmtVarDecl(): " + stmt);
        for (int i = 0; i < stmt.getNumVars(); i++)
            mExclusion.add(new ExprVar(null, stmt.getName(i)));
        // FIXME lhs is excluded from propagation but rhs is still necessary.
        return super.visitStmtVarDecl(stmt)
    }

    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
        //Log.i("ConstantPropagator.visitStmtAssign(): $stmt");
        Expression rhs = doExpression(stmt.getRHS());
        return rhs == stmt.getRHS() ? stmt : new StmtAssign(stmt.getContext(), stmt.getLHS(), rhs, stmt.getOp());
    }
}
