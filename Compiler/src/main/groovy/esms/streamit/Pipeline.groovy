package esms.streamit

import esms.api.Scheduler
import streamit.frontend.nodes.SCSimple;
import streamit.frontend.nodes.StreamSpec;
import streamit.frontend.nodes.Type;

/**
* Created by farleylai on 9/16/14.
*/
public class Pipeline extends Stream {
    private List<Stream> mStreams = new ArrayList<>();

    public Pipeline(Stream parent, SCSimple sc, StreamSpec spec) {
        super(parent, sc, spec);
    }

    public Pipeline(Stream parent, StreamSpec spec) {
        super(parent, spec);
    }

    @Override
    public boolean isPipeline() {
        return true;
    }

    @Override
    public Type getInputType(Stream s) {
        return first().getInputType(s);
    }

    @Override
    public Type getOutputType(Stream s) {
        return last().getOutputType(s);
    }

    @Override
    public Stream first() {
        return mStreams.isEmpty() ? this : mStreams.get(0).first();
    }

    @Override
    public Stream last() {
        return mStreams.isEmpty() ? this : mStreams.get(mStreams.size() - 1).last();
    }

    public Pipeline add(Stream s) {
        if(!mStreams.isEmpty())
            mStreams.get(mStreams.size() - 1).link(s);
        mStreams.add(s);
        s.parent(this)
        return this;
    }

    def removeRange(int from, int to) {
        mStreams.removeRange(from, to)
    }

    def clear() {
        mStreams.clear()
    }

    def addAll(streams) {
        streams.each { add(it) }
    }

    def size() {
        return mStreams.size()
    }

    def head() {
        mStreams[0]
    }

    def tail() {
        mStreams[-1]
    }

    def get(int i) {
        return mStreams[i]
    }

    def inject(v, Closure closure) {
        mStreams.inject(v) { ret, Stream s ->
            closure(ret, s)
        }
    }

    def reverseInject(v, Closure closure) {
        mStreams.reverse().inject(v) { ret, s ->
            closure(ret, s)
        }
    }

    def each(Closure closure) {
        mStreams.each {
            closure(it)
        }
    }

    def eachWithIndex(Closure closure) {
        mStreams.eachWithIndex{ Stream s, int i ->
            closure(s, i)
        }
    }

    def reverseEach(Closure closure) {
        mStreams.reverseEach {
            closure(it)
        }
    }

    @Override
    Pipeline accept(Scheduler scheduler) {
        scheduler.visitPipeline(this);
        return this;
    }

    @Override
    public String toString() {
        return "[${mStreams.collect { it.toString() }.join(",")}]"
    }

}
