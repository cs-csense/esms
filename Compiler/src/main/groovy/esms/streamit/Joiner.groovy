package esms.streamit

import esms.api.Scheduler
import streamit.frontend.nodes.StreamSpec;
import streamit.frontend.nodes.Type;

/**
* Created by farleylai on 9/16/14.
*/
public class Joiner extends Stream {
    public static final String BASENAME = "join";
    private int[] mWeights;

    public Joiner(Stream parent, int... weights) {
        super(parent, new StreamSpec(null, StreamSpec.STREAM_SPLITJOIN, null, BASENAME, null, null, null));
    }

    public final int[] weights() { return mWeights; }

    /**
     * The input type from stream s is determined by the output type of s.
     *
     * @param s
     * @return
     */
    @Override
    public Type getInputType(Stream s) {
        return s.getOutputType(this);
    }

    /**
     * The output type is determined by its input type.
     *
     * @param s
     * @return
     */
    @Override
    public Type getOutputType(Stream s) {
        return getInput(0).getOutputType(this);
    }

    @Override
    public boolean isJoiner() { return true; }

    @Override
    public Joiner add(int... weights) {
        mWeights = weights;
        return this;
    }

    @Override
    public int getPushRate(Stream s) {
        if(mWeights.length == 1) // RR
            return mWeights[0] * getInputs().size();
        else // weighted RR
            return Arrays.stream(mWeights).sum();
    }

    @Override
    public int getPeekRate(Stream s) { return getPopRate(s); }

    @Override
    public int getPopRate(Stream s) {
        if (mWeights.length == 1) // RR
            return mWeights[0];
        else {// weighted RR
            return mWeights[inputIndexOf(s)];
        }
    }

    @Override
    Joiner accept(Scheduler scheduler) {
        scheduler.visitJoiner(this);
        return this;
    }
}
