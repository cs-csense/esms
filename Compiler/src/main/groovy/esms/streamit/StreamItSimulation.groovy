package esms.streamit

import esms.api.Component;
import esms.core.OP;
import esms.core.Operation
import esms.util.Log;
import streamit.frontend.nodes.ExprPeek;
import streamit.frontend.nodes.ExprPop;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.FENode;
import streamit.frontend.nodes.StmtPush;

/**
* Created by farleylai on 9/21/14.
*/
public class StreamItSimulation {
    // PUSH: pushIdx, popIdx, conflicted
    // PEEK: popIdx (pos + offset), conflicted
    // POP:  popIdx, conflicted
    private StreamItSimulator mSimulator;
    private List<Operation> mOperaitons;
    private Map<Operation, FENode> mOperationTable;
    private Map<FENode, Operation> mFENodeTable;
    private Set<Expression> mStateful;
    private Set<FENode> mConflicts;
    private int mPushIndex;
    private int mPopIndex;

    public StreamItSimulation(StreamItSimulator simulator) {
        mSimulator = simulator;
        mOperaitons = new ArrayList<>();
        mOperationTable = new HashMap<>();
        mStateful = new HashSet<>();
        mConflicts = new HashSet<>();
        mPushIndex = mPopIndex = 0;
    }

    private StreamItSimulation add(Operation operation, FENode exp, boolean conflicted) {
        mOperaitons.add(operation);
        mOperationTable.put(operation, exp);
        mFENodeTable = new HashMap<>();
        if(conflicted) {
            mConflicts.add(exp);
        }
//        Log.d("added %s %s => %s (0x%X)\n", conflicted ? "conflicted" : "conflictless", operation, exp, exp.hashCode());
        return this;
    }

    public int indexOf(Operation operation) { return mOperaitons.indexOf(operation); }

    public FENode get(Operation operation) {
        return mOperationTable.get(operation);
    }

    public Operation get(FENode exp) {
        return mOperationTable.find { k, v -> v.is(exp) }.getKey()
    }

    public StreamItSimulation setConflictStateful(Expression exp) {
        mStateful.add(exp);
        return this;
    }

    public boolean isConflicted(int pos) {
        // if the pop/peek position conflicts with an earlier push
        return mOperaitons.findAll { it.op() == OP.PUSH }.any { it.getInt(0) == pos && it.getInt(1) != pos }
    }

    /**
     * Checks if the position to push/peek/pop is conflicted.
     * A push may conflict with a later push through coflicted peek/pop.
     * A peek/pop may conflict with an earlier push.
     *
     * @param push the push operation
     * @param pos the position to push or pop
     * @return
     */
    public boolean isConflicted(Operation push, int pos) {
        // if conflicted with a later push or a stateful peek/pop
        int idx = indexOf(push);
        boolean statefulConflict = mOperaitons.findAll {
            it.op() == OP.POP || it.op() == OP.PEEK
        }.findAll { it.pos() == pos }.any {
            isConflicted(it) && mStateful.contains(mOperationTable.get(it))
        }
//        Log.d("isStatefulConflicted(%d, %s): %s\n", pos, push, statefulConflict);
        if(statefulConflict) return true;
        else return mOperaitons.findAll {
            it.op() == OP.PUSH && it.pos() > push.pos()
        }.any { operation ->
            // for PUSH happened after
            StmtPush stmt = (StmtPush)mOperationTable.get(operation);
            if(stmt == null) return false;
            boolean conflicted = false;
            List<Expression> exps = mSimulator.decompose(stmt.getValue());
            if(exps.size() > 1) {
                // No PASS => check if conflicted
                conflicted = exps.findAll { isConflicted(it) }.collect { mFENodeTable.get(it) }.any { it?.pos() == pos }
            } else {
                // PASS => conflicted if any
                Expression exp = stmt.getValue();
                Operation op = mFENodeTable.get(exp);
                conflicted = op != null && op.pos() == pos;
            }
            return conflicted;
        }
    }

    public boolean isConflicted(FENode exp) {
        return mConflicts.contains(exp);
    }

    /**
     * Push is conflicted if the value to push is composed of conflicted peek/pop value(s).
     * Peek/Pop is conflicted if an earlier pushed value is written to its position.
     * However, a conflictless push may conflict with the peek/pop at the push position.
     *
     * @param operation
     * @return
     */
    public boolean isConflicted(Operation operation) {
        return mConflicts.contains(mOperationTable.get(operation));
    }

    /**
     * FIXME StreamIt makes ExprPop and ExprPeek to be equal while Groovy overloaded == to call equals()
     * The intention is to differentiate different objects.
     *
     * @param exp
     * @return
     */
    public boolean contains(Expression exp) {
        return mOperationTable.any { it.getValue().is(exp) };
    }

    /**
     * For annotation of Identity.
     *
     * @param src
     * @param pos
     * @param size
     * @return
     */
    public StreamItSimulation pass(Component src, int pos, int size) {
        mOperaitons.add(Operation.pass(src, pos, size));
        return this;
    }

    public StreamItSimulation push() {
        mOperaitons.add(Operation.push(mPushIndex++, -1));
        return this;
    }

    public StreamItSimulation pop() {
        mOperaitons.add(Operation.pop(mPopIndex++));

        return this;
    }

    /**
     * Push a compound expression which may be conflicted.
     *
     * @param stmt
     * @param conflicted
     * @return
     */
    public StreamItSimulation push(StmtPush stmt, boolean conflicted) {
        add(Operation.push(mPushIndex++, -1), stmt, conflicted);
        return this;
    }

    public StreamItSimulation push(StmtPush stmt, ExprPeek peek) {
        Operation operation = get(peek);
        int idx = operation.getInt(0);
        int offset = operation.getInt(1);
        return add(Operation.push(mPushIndex++, offset < 0 ? -1 : idx + offset), stmt, isConflicted(peek));
    }

    public StreamItSimulation push(StmtPush stmt, ExprPop pop) {
        Operation operation = get(pop);
        return add(Operation.push(mPushIndex++, operation.getInt(0)), stmt, isConflicted(pop));
    }

    public StreamItSimulation peek(ExprPeek exp) {
        return peek(exp, -1);
    }

    /**
     * peek() is conflicted if pop index + offset < push index && pushed value differs
     *
     * @param exp
     * @param offset >= 0
     * @return
     */
    public StreamItSimulation peek(ExprPeek exp, int offset) {
        boolean conflicted = mPopIndex + offset < mPushIndex && isConflicted(mPopIndex + offset);
        return add(Operation.peek(mPopIndex, offset), exp, conflicted);
    }

    /**
     * pop() is conflicted if pop index < push index && pushed value differs
     *
     * @param exp
     * @return
     */
    public StreamItSimulation pop(ExprPop exp) {
        boolean conflicted = mPopIndex < mPushIndex && isConflicted(mPopIndex);
        return add(Operation.pop(mPopIndex++), exp, conflicted);
    }

    public List<Operation> operations() { return mOperaitons; }

    public StreamItSimulation submit() {
        mOperationTable.each { k, v -> mFENodeTable.put(v, k) };
        return this;
    }
}
