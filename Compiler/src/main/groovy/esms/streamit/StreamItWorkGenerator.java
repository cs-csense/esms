package esms.streamit;

import esms.api.Channel;
import esms.core.Range;
import esms.api.SFG;
import esms.core.Window;
import esms.backend.c.ESMSCodeGenerator;
import streamit.frontend.nodes.ExprBinary;
import streamit.frontend.nodes.ExprConstInt;
import streamit.frontend.nodes.ExprFunCall;
import streamit.frontend.nodes.ExprPeek;
import streamit.frontend.nodes.ExprPop;
import streamit.frontend.nodes.ExprTernary;
import streamit.frontend.nodes.ExprUnary;
import streamit.frontend.nodes.ExprVar;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.FEReplacer;
import streamit.frontend.nodes.FuncWork;
import streamit.frontend.nodes.Statement;
import streamit.frontend.nodes.StmtAssign;
import streamit.frontend.nodes.StmtExpr;
import streamit.frontend.nodes.StmtFor;
import streamit.frontend.nodes.StmtIfThen;
import streamit.frontend.nodes.StmtPush;
import streamit.frontend.nodes.StmtVarDecl;
import streamit.frontend.nodes.StmtWhile;

/**
 * Realize push()/peek()/pop() in terms of derived layouts.
 *
 * Created by farleylai on 10/27/14.
 */
public class StreamItWorkGenerator extends FEReplacer {
    private StreamItEvaluator mEvaluator;
    private Channel mInput;
    private Channel mOutput;
    private Window mSource;
    private Window mSink;
    private boolean mInit;
    private boolean mPushIndirect;
    private boolean mPopIndirect;
    private int mPush;
    private int mPop;

    public StreamItWorkGenerator(SFG sfg, int g, boolean same, StreamItEvaluator evaluator, boolean init) {
        mEvaluator = new StreamItEvaluator(evaluator);
        mInput = sfg.getInputChannel(evaluator.stream(), 0);
        Channel.State s = mInput == null ? null : sfg.getState(mInput);
        mPopIndirect = s == null ? false : s.isPopIndirect();
        mSink = s == null ? null : s.getSinkWindow(g, 0).linearize(same);
        mOutput = sfg.getOutputChannel(evaluator.stream(), 0);
        s = mOutput == null ? null : sfg.getState(mOutput);
        mSource = s == null ? null : s.getSourceWindow(g, 0).linearize(true);
        mInit = init;
        mPushIndirect = s == null ? false : s.isPushIndirect();
    }

    @Override
    public FuncWork visitFuncWork(FuncWork func) {
        mPush = mPop = 0;
        Statement newBody = (Statement) func.getBody().accept(this);
        return newBody != func.getBody()
                ? new FuncWork(func.getContext(), func.getCls(), func.getName(), newBody,
                               func.getPeekRate(), func.getPopRate(), func.getPushRate())
                : func;
    }

    /**
     * FIXME The offset must be evaluable.
     * FIXME Range incr are supposed the same.
     * TODO Optimize for contiguous ranges of different writers.
     * Encode peek() to access the corresponding range.
     *
     * @param peek
     * @return
     */
    @Override
    public Object visitExprPeek(ExprPeek peek) {
        peek.accept(mEvaluator);
        peek = (ExprPeek) super.visitExprPeek(peek);
        if(!mEvaluator.isEvaluable(peek.getExpr()))
            throw new RuntimeException("Non-deterministic peek() is not supported yet.");
        Expression arg = peek.getExpr();
        if(mPopIndirect) {
            ExprFunCall call = new ExprFunCall(peek.getContext(),
                    ESMSCodeGenerator.encodeARPeekPopIndirect(),
                    ESMSCodeGenerator.encodeARTailIndex(mEvaluator.stream(), mInit, arg));
//            Log.d("%s: %s -> %s for indirect addressing\n", mEvaluator.stream(), peek, call);
            return call;
        }

        int offset = mEvaluator.toInt(peek.getExpr());
        int popR = mSink.indexOf(mSink.rangeAt(mPop));
        int pos = mPop + offset;
        Range range = mSink.rangeAt(pos);
        int r = mSink.indexOf(range);
//        Log.d("visitExprPeek(): %s, pos %d(%d+%d), range: %s\n%s", peek, pos, mPop, offset, range, mSink);
        if (mSink.get(popR) != range) {
            // Transition from given offset to target range offset
            int newOffset = (mSink.offsetAt(pos) - range.offsetAt(0)) / range.incr();
//            Log.d("%s: %s, offset=%d, newOffset = offsetAt(pos:%d:%d) - range.offsetAt(0:%d) = %d\n", mEvaluator.stream(), peek, offset, pos, mSink.offsetAt(pos), range.offsetAt(0), newOffset);
            int transition = newOffset - offset;
            arg = new ExprBinary(null, ExprBinary.BINOP_ADD, peek.getExpr(), new ExprConstInt(transition));
            // add pop range index displacement in case of inconsistent offsets between INIT and Steady
            Expression distance0 = new ExprBinary(null, ExprBinary.BINOP_SUB, new ExprConstInt(range.offsetAt(0)), new ExprConstInt(mSink.offsetAt(mPop)));
            Expression distanceNow = new ExprBinary(null, ExprBinary.BINOP_SUB,
                    new ExprVar(null, ESMSCodeGenerator.encodeARTail(mSink.indexOf(range))),
                    new ExprVar(null, ESMSCodeGenerator.encodeARTail(popR)));
            Expression displacement = new ExprBinary(null, ExprBinary.BINOP_SUB, distance0, distanceNow);
            displacement = new ExprTernary(peek.getContext(), ExprTernary.TEROP_COND, new ExprBinary(peek.getContext(), ExprBinary.BINOP_LE, displacement, new ExprConstInt(0)), new ExprUnary(peek.getContext(), ExprUnary.UNOP_NEG, displacement), displacement);
            if(mSink.get(popR).incr() != 1)
                displacement = new ExprBinary(null, ExprBinary.BINOP_DIV, displacement, new ExprVar(null, ESMSCodeGenerator.encodeARTailIncr(popR)));
            arg = new ExprBinary(null, ExprBinary.BINOP_ADD, arg, displacement);
        }
        return new ExprFunCall(peek.getContext(), ESMSCodeGenerator.encodeARPeek(r), arg);
    }

    @Override
    public Object visitExprPop(ExprPop pop) {
        if(mPopIndirect) {
            ExprFunCall call = new ExprFunCall(pop.getContext(),
                    ESMSCodeGenerator.encodeARPeekPopIndirect(),
                    ESMSCodeGenerator.encodeARTailIndexIncr(mEvaluator.stream(), mInit));
//            Log.d("%s: %s -> %s for indirect addressing\n", mEvaluator.stream(), pop, call);
            return call;
        }

        Range range = mSink.rangeAt(mPop++);
        return new ExprFunCall(pop.getContext(), ESMSCodeGenerator.encodeARPop(mSink.indexOf(range), range.incr()));
    }

    @Override
    public Object visitStmtPush(StmtPush stmt) {
        stmt.accept(mEvaluator);
        stmt = (StmtPush) super.visitStmtPush(stmt);
        if(mPushIndirect) {
            ExprFunCall call = new ExprFunCall(stmt.getContext(),
                    ESMSCodeGenerator.encodeARPushIndirect(),
                    ESMSCodeGenerator.encodeARHeadIndexIncr(mEvaluator.stream(), mInit),
                    stmt.getValue());
//            Log.d("%s: %s -> %s for indirect addressing\n", mEvaluator.stream(), stmt, call);
            return new StmtExpr(stmt.getContext(), call);
        }

        // Reinterpreted INSERT or normal ranges due to other optimizations
        Range range = mEvaluator.stream().isReinterpreted() ? mSource.rangeAt(0) : mSource.rangeAt(mPush++);
        int r = mSource.indexOf(range);
        if(mEvaluator.stream().equals(range.writer()))
            return new StmtExpr(stmt.getContext(), new ExprFunCall(stmt.getContext(), ESMSCodeGenerator.encodeARPush(r, range.incr()), stmt.getValue()));
        else {
            // Value arguments may have side effects and cannot be skipped
            return new StmtExpr(stmt.getContext(), new ExprFunCall(stmt.getContext(), ESMSCodeGenerator.encodeARPush(r), stmt.getValue()));
        }
    }

    @Override
    public Object visitStmtVarDecl(StmtVarDecl stmt) {
//        Log.d("StreamItWorkerGenerator.visitStmtVarDecl(): " + stmt);
        stmt.accept(mEvaluator);
        return super.visitStmtVarDecl(stmt);
    }

    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
//        Log.d("StreamWorkGenerator.visitStmtAssign(): " + stmt);
        stmt.accept(mEvaluator);
        return super.visitStmtAssign(stmt);
    }

    /**
     * TODO Remove unvisited statements.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtIfThen(StmtIfThen stmt) {
        Expression cond = stmt.getCond();
        Statement cons = stmt.getCons();
        Statement alt = stmt.getAlt();

        if(!mEvaluator.stream().isSafe()) {
            cond = (Expression) cond.accept(this);
            cons = (Statement) cons.accept(this);
            if (alt != null)
                alt = (Statement) alt.accept(this);
            return cond == stmt.getCond() && cons == stmt.getCons() && alt == stmt.getAlt()
                    ? stmt
                    : new StmtIfThen(stmt.getContext(), cond, cons, alt);
        } else {
            if (!mEvaluator.isEvaluable(cond)) {
                esms.streamit.StreamItCounter counter = new esms.streamit.StreamItCounter(mEvaluator);
                cons.accept(counter);
                if (alt != null)
                    alt.accept(counter);
                if (counter.pushes() + counter.pops() > 0)
                    return new RuntimeException("Non-deterministic conditionals are not supported yet");
                // check if we can replace the conditionals
                cond = (Expression) cond.accept(this);
                return cond == stmt.getCond()
                        ? stmt
                        : new StmtIfThen(stmt.getContext(), cond, cons, alt);
            } else {
                if (mEvaluator.toBoolean(cond)) {
                    return cons = (Statement) cons.accept(this);
                } else if (alt != null) {
                    return alt = (Statement) alt.accept(this);
                }
            }
            return cond == stmt.getCond() && cons == stmt.getCons() && alt == stmt.getAlt()
                    ? stmt
                    : new StmtIfThen(stmt.getContext(), cond, cons, alt);
        }
    }

    @Override
    public Object visitStmtFor(StmtFor stmt) {
//        Log.d("StreamItWorkGenerator.visitStmtFor(): for(%s; %s; %s)\n", stmt.getInit(), stmt.getCond(), stmt.getIncr());
        Statement init = stmt.getInit();
        if (init != null)
            init = (Statement) init.accept(this);

        Expression cond = stmt.getCond();
        Statement body = stmt.getBody();
        Statement incr = stmt.getIncr();
        if(!mEvaluator.stream().isSafe()) {
            body = (Statement) body.accept(this);
            return cond == stmt.getCond() && body == stmt.getBody() && incr == stmt.getIncr()
                    ? stmt
                    : new StmtFor(stmt.getContext(), init, cond, incr, body);
        } else {
            if (!mEvaluator.isEvaluable(cond)) {
                esms.streamit.StreamItCounter counter = new esms.streamit.StreamItCounter(mEvaluator);
                stmt.accept(counter);
                if (counter.pushes() + counter.pops() > 0)
                    return new RuntimeException("Non-deterministic loops are not supported yet");
            }

            esms.streamit.StreamItCounter counter = new esms.streamit.StreamItCounter(mEvaluator);
            stmt.accept(counter);
            int push = mPush;
            int pop = mPop;
            body = (Statement) body.accept(this);
            mPush = push + counter.pushes();
            mPop = pop + counter.pops();
            return cond == stmt.getCond() && body == stmt.getBody() && incr == stmt.getIncr()
                    ? stmt
                    : new StmtFor(stmt.getContext(), init, cond, incr, body);
        }
    }

    @Override
    public Object visitStmtWhile(StmtWhile stmt) {
//        Log.i("StreamitWorkGenerator.visitStmtWhile(): while(%s)\n", stmt.getCond());
        Expression cond = stmt.getCond();
        Statement body = stmt.getBody();

        if (!mEvaluator.isEvaluable(cond)) {
            esms.streamit.StreamItCounter counter = new esms.streamit.StreamItCounter(mEvaluator);
            stmt.accept(counter);
            if (counter.pushes() + counter.pops() > 0)
                return new RuntimeException("Non-deterministic loops are not supported yet");
        }

        esms.streamit.StreamItCounter counter = new esms.streamit.StreamItCounter(mEvaluator);
        stmt.accept(counter);
        int push = mPush;
        int pop = mPop;
        body = (Statement) body.accept(this);
        mPush = push + counter.pushes();
        mPop = pop + counter.pops();
        return cond == stmt.getCond() &&  body == stmt.getBody()
                ? stmt
                : new StmtWhile(stmt.getContext(), cond, body);
    }
}
