package esms.streamit

import streamit.frontend.nodes.ExprArray;
import streamit.frontend.nodes.ExprConstInt;
import streamit.frontend.nodes.ExprVar;
import streamit.frontend.nodes.Expression;
import streamit.frontend.nodes.SymbolTable;
import streamit.frontend.nodes.TypePrimitive;

/**
 * Anything saved in a symbol table must be simplified expressions.
 * Created by farleylai on 9/19/14.
 */
public class StreamItSymbolTable extends SymbolTable {
    private Set<ExprVar> mInevaluables;

    public StreamItSymbolTable(StreamItSymbolTable parent) {
        super(parent);
        mInevaluables = new HashSet<>();
        if(parent == null) {
            registerVar("NULL", TypePrimitive.inttype, new ExprConstInt(0), SymbolTable.KIND_GLOBAL);
//            registerVar("SEEK_SET", TypePrimitive.inttype, new ExprConstInt(0), SymbolTable.KIND_GLOBAL);
//            registerVar("SEEK_END", TypePrimitive.inttype, new ExprConstInt(0), SymbolTable.KIND_GLOBAL);
        }
    }

    public boolean isEvaluable(ExprVar var, StreamItEvaluator eval) {
//        Log.d("StreamItSymbolTable.isEvaluable(): %s: %s and %s\n", var,
//                hasVar(var.getName()) ? "registered" : "unregistered",
//                mInevaluables.contains(var) ? "inevaluable" : "check value");
        if(hasVar(var.getName()) && !mInevaluables.contains(var)) {
            Expression origin = (Expression) lookupOrigin(var.getName());
            if(eval.isEvaluable(origin)) {
//                Log.d("StreamItSymbolTable.isEvaluable(): %s(0x%X)=%s is evaluable\n", var, var.hashCode(), origin);
                return true;
            }
            mInevaluables.add(var);
        }
//        Log.d("StreamItSymbolTable.isEvaluable(): %s(0x%X) not evaluable\n", var, var.hashCode());
        return false;
    }

    public boolean isEvaluable(ExprArray array, StreamItEvaluator eval) {
//        Log.i("StreamItSymbolTable.isEvaluable(): %s" + array);
        Expression val = eval.getArrayElement(array);
        return val == array ? false : eval.isEvaluable(val);
    }

    @Override
    public StreamItSymbolTable getParent() {
        return (StreamItSymbolTable) super.getParent();
    }
}
