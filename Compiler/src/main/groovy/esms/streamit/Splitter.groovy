package esms.streamit

import esms.api.Scheduler
import esms.scheduling.PhasedScheduler
import streamit.frontend.nodes.StreamSpec;
import streamit.frontend.nodes.Type;

/**
* Created by farleylai on 9/16/14.
*/
public class Splitter extends Stream {
    public static final String BASENAME = "split";
    private int[] mWeights;

    public Splitter(Stream parent) {
        super(parent, null, new StreamSpec(null, StreamSpec.STREAM_SPLITJOIN, null, BASENAME, null, null, null));
    }

    public final int[] weights() { return mWeights; }

    /**
     * The input type is reasoned from the input to the parent splitjoin.
     * @param s
     * @return
     */
    @Override
    public Type getInputType(Stream s) {
        return parent().getInputType(s);
    }

    /**
     * The output type should be the same as its input type.
     *
     * @param s
     * @return
     */
    @Override
    public Type getOutputType(Stream s) {
        return getInputType(getInput(0));
    }

    @Override
    public boolean isSplitter() { return true; }

    @Override
    public Splitter add(int... weights) {
        mWeights = weights;
        return this;
    }

    public boolean isDuplicate() {
        return mWeights.length == 0;
    }

    @Override
    public int getPushRate(Stream s = null) {
        switch(mWeights.length) {
            case 0: // duplicate
                // FIXME conform to StreamIt
//                Log.i("DUP %s push rate to %s: %d\n", this, s, getInput(0).getPushRate(this));
//                return getInput(0).getPushRate(this);
                return 1;
            case 1: // round robin
//                Log.i("RR %s push rate to %s: %d\n", this, s, mWeights[0]);
                return mWeights[0];
            default:
//                Log.i("Weighted RR %s push rate to %s: %d\n", this, s, mWeights[outputIndexOf(s)]);
                return mWeights[outputIndexOf(s)];
        }
    }

    @Override
    public int getPeekRate(Stream s = null) { return getPopRate(s); }

    @Override
    public int getPopRate(Stream s = null)  {
        switch(mWeights.length) {
            case 0: // duplicate
                // FIXME conform to StreamIt
//              return getInput(0).getPushRate(this);
                return 1;
            case 1: // round robin
                return mWeights[0] * getOutputs().size();
            default: // weighted round robin
                return Arrays.stream(mWeights).sum();
        }
    }

    @Override
    Splitter accept(Scheduler scheduler) {
        scheduler.visitSplitter(this);
        return this;
    }
}
