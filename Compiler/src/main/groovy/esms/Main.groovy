package esms

import esms.api.Profile
import esms.toolkits.ESMSToolkit
import esms.toolkits.StreamItToolkit4j
import esms.util.Log
import esms.util.StageTimer

public class Main {
    public static void main(String[] args) {
        println "\$> esms ${args.join(' ')}"
        def cli = new CliBuilder(usage:'esms.Main [options] benchmark')
        cli.p(required:true, args:1, longOpt:'platform', argName:'target', 'Target platform: osx | linux | android')
        cli.b(required:true, args: 1, longOpt:'backend', argName:'language', 'Code generation backend')
        cli.s(longOpt:'scaling', args:1, argName:'factor', 'Execution scaling')
        cli.i(longOpt:'iterations', args:1, argName:'number', 'Number of iterations to execute')
        cli.o(longOpt:'optimizer', args:1, argName:'default | esms', 'Stream optimization')
        cli.k(longOpt:'pkg', args:1, 'Java package if java backend is selected')
        cli._(required:true, longOpt:'srcDir', args:1, 'benchmark source directory')
        cli._(required:true, longOpt:'outputDir', args:1, 'output directory for generated benchmark code')
        cli._(longOpt:'partitioning', args:1, 'Threading domain partitioning')
        cli._(longOpt:'printf', 'Whether to print output or not')
        def options = cli.parse(args)
        if(!options) {
            println cli.usage
            System.exit(-1)
        }

        options.arguments().each {
            Profile profile = new Profile()
            profile.benchmark = it
            cli.options.options.findAll { options[it.longOpt] }.each {
                profile."$it.longOpt" = options[it.longOpt]
            }
            profile.iterations = profile.iterations.toInteger()
            profile.scaling = profile.scaling.toInteger()
            profile.partitioning = profile.partitioning.toBoolean()

//            println profile
            if(profile.backend == 'streamit') {
//                StreamItToolkit4j tk = new StreamItToolkit4j(profile)
            } else {
                Log.level(Log.Severity.INFO)
                ESMSToolkit tk = new ESMSToolkit(profile)
                StageTimer timer = tk.build(true)
                println tk
                println timer
            }
        }
    }
}