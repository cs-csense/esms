package esms.core;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by farleylai on 4/21/14.
 */
public class Operation {
    private final OP mOP;
    private final List<Object> mArgs;

    public Operation(OP op, Object... args) {
        mOP = op;
        mArgs = Arrays.asList(args);
    }

    public static Operation push(int pushIdx, int popIdx) {
        return new Operation(OP.PUSH, pushIdx, popIdx);
    }

    public static Operation peek(int pos, int offset) {
        return new Operation(OP.PEEK, pos, offset);
    }

    public static Operation pop(int pos) {
        return new Operation(OP.POP, pos);
    }


    public static Operation create(int size, int sampleSize) {
        return create(size, sampleSize, false);
    }

    public static Operation append(int size, int sampleSize) {
        return new Operation(OP.APPEND, size, sampleSize, false);
    }

    public static Operation create(int size, int sampleSize, boolean reverse) {
        return new Operation(OP.CREATE, size, sampleSize, reverse);
    }

    public static Operation insert(int pos, int size, int sampleSize) {
        return insert(pos, size, sampleSize, false);
    }

    public static Operation insert(int pos, int size, int sampleSize, boolean reverse) {
        return new Operation(OP.INSERT, pos, size, sampleSize, reverse);
    }

    public static Operation update(int pos, int size) {
        return update(pos, size, false);
    }

    public static Operation update(int pos, int size, boolean reverse) {
        return new Operation(OP.UPDATE, pos, size, reverse);
    }

    public static Operation pass(esms.api.Component src, int pos, int size) {
        return new Operation(OP.PASS, src, pos, size);
    }

    public OP op() {
        return mOP;
    }

    public Optional<esms.api.Component> src() {
        switch (mOP) {
            case PASS:
                return Optional.of((esms.api.Component) arg(0).get());
            default:
                return Optional.empty();
        }
    }

    public int pos() {
        switch (mOP) {
            case PEEK:
                return getInt(0) + getInt(1);
            case PUSH:
            case POP:
            case INSERT:
            case UPDATE:
                return getInt(0);
            case PASS:
                return getInt(1);
        }
        return -1;
    }

    public int size() {
        switch (mOP) {
            case APPEND:
            case CREATE:
                return getInt(0);
            case INSERT:
            case UPDATE:
                return getInt(1);
            case PASS:
                return getInt(2);
        }
        return -1;
    }

    public int sampleSize() {
        switch (mOP) {
            case APPEND:
            case CREATE:
                return getInt(1);
            case INSERT:
                return getInt(2);
            default:
        }
        return -1;
    }

    public boolean reverse() {
        switch (mOP) {
            case CREATE:
                return getBoolean(2);
            case INSERT:
                return getBoolean(3);
            case UPDATE:
                return getBoolean(2);
        }
        return false;
    }

    public Optional<Object> arg(int idx) {
        return idx < 0 || idx >= mArgs.size() ? Optional.empty() : Optional.of(mArgs.get(idx));
    }

    public boolean getBoolean(int idx) {
        return (Boolean) arg(idx).orElse(Boolean.FALSE);
    }

    public int getInt(int idx) {
        return (Integer) arg(idx).orElse(-1);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("(").append(mOP);
        mArgs.forEach(arg -> builder.append(",").append(arg));
        return builder.append(")").toString();
    }
}
