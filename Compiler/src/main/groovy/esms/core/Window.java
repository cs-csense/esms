package esms.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import esms.util.Log;

/**
 * Logical sequential access to a physical layout by positions
 * position -> <writerAt, originatorAt, indexAt, offsetAt>
 */
public class Window implements List<Range> {
    public static final Window EMPTY = new Window();
    private static String TAG = Window.class.getSimpleName();

    private List<Range> mRanges;

    public Window() {
        mRanges = new LinkedList<>();
    }

    public Window(List<? extends Range> ranges) {
        this();
        addAll(ranges);
    }

    public Window(Window w, boolean deep) {
        this();
        if (deep)
            w.forEach(r -> add(new Range(r)));
        else
            addAll(w);
    }

    public Window(Window w) {
        this(w, false);
    }

    public Window originator(esms.api.Component originator) {
        forEach(r -> r.originator(originator));
        return this;
    }

    public Window writer(esms.api.Component writer) {
        forEach(r -> r.writer(writer));
        return this;
    }

    public List<Range> ranges() {
        return mRanges;
    }

    public boolean isPeekLinear() {
        int offset = rangeAt(0).offset();
        for (Range r : this) {
            if (offset != r.offset() || r.reverse())
                return false;
            offset = r.limit();
        }
        return true;
    }

    public boolean isPushLinear(esms.api.Component c) {
        Window w = new Window(stream().filter(r -> r.writer() == c).collect(Collectors.toList()));
        int offset = w.offsetAt(0);
        for (Range r : w) {
            if (r.offset() != offset || r.reverse())
                return false;
            offset = r.limit();
        }
        return true;
    }

    public boolean isAccessible(int pos) {
        return offsetAt(pos) > -1;
    }

    public boolean isAccessible(int pos, int size) {
        for (int i = pos; i < size; i++)
            if (offsetAt(pos) <= -1) return false;
        return true;
    }

    public boolean isLinear() {
        int offset = offset();
        for(Range r: mRanges) {
            if(r.offset() != offset)
                return false;
            offset = r.limit();
        }
        return true;
    }

    /**
     * Returns if this window is compatible with the other.
     * True if both of the ranges align with each other with
     * the same increment.
     *
     * @param w
     * @return
     */
    public boolean isAlignable(Window w) {
        int displacement = w.offset() - offset();
        if(mRanges.size() == w.ranges().size()) {
            for(int r = 0; r < mRanges.size(); r++) {
                Range r1 = get(r);
                Range r2 = w.get(r);
                if(r1.size() != r2.size() || r1.offset() + displacement != r2.offset())
                    return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return mRanges.isEmpty();
    }

    /**
     * Test if all the ranges are compatible with each other.
     *
     * @return
     */
    public boolean isValid() {
        List<Range> ranges = new ArrayList<>(mRanges);
        Collections.sort(ranges);
        boolean valid = true;
        for (int i = 1; i < ranges.size() && valid; i++) {
            Range prev = ranges.get(i - 1);
            Range range = ranges.get(i);
            valid = prev.isCompatible(range);
            if (!valid)
                Log.i(TAG, "%s is not compatible with %s\n", prev, range);
        }
        return valid;
    }

    public Range get(int i) {
        return mRanges.get(i);
    }

    @Override
    public Range set(int index, Range r) {
        return mRanges.set(index, r);
    }

    public boolean add(esms.api.Component c, int index, int offset, int size) {
        return add(new Range(c, index, offset, size));
    }

    public boolean add(esms.api.Component c, int index, int offset, int size, int sampleSize) {
        return add(new Range(c, index, offset, size, sampleSize));
    }

    public boolean add(esms.api.Component c, int index, int offset, int size, int sampleSize, int incr) {
        return add(new Range(c, c, index, offset, size, sampleSize, incr));
    }

    @Override
    public boolean add(Range r) {
        return r.size() > 0 ? mRanges.add(r) : false;
    }

    @Override
    public void add(int i, Range r) {
        if (r.size() > 0)
            mRanges.add(i, r);
    }

    @Override
    public boolean addAll(Collection<? extends Range> c) {
        mRanges.addAll(c);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Range> c) {
        return mRanges.addAll(index, c);
    }

    @Override
    public Range remove(int index) {
        return mRanges.remove(index);
    }

    @Override
    public boolean remove(Object r) {
        for(Iterator<Range> itr = mRanges.iterator(); itr.hasNext();) {
            if(itr.next() == r) {
                itr.remove();
                break;
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return mRanges.removeAll(c);
    }

    public boolean contains(esms.api.Component c, int index, int size) {
        return mRanges.stream().filter(r -> {
            int idx0 = r.indexAt(0);
            int idxE = r.indexAt(r.size() - 1);
            return r.writer() == c && (idx0 < index && idxE >= index || idx0 >= index && idx0 < index + size);
        }).mapToInt(r -> r.indexAt(r.size() - 1) - r.indexAt(0)).sum() >= size;
    }

    public boolean contains(esms.api.Component c, int index, int offset, int size) {
        return contains(c, index, offset, size, false);
    }

    public boolean contains(esms.api.Component c, int index, int offset, int size, boolean reverse) {
        Range r = layout().search(offset).stream().findAny().orElse(null);
        if (r != null) {
            if (reverse && index == r.indexAt(limit() - offset - 1) && offset - r.offset() + 1 >= size) {
                return true;
            } else if (index == r.indexAt(offset - r.offset()) && r.limit() - offset >= size)
                return true;
        }
        return false;
    }

    public boolean hasOriginator(esms.api.Component c) {
        return stream().anyMatch(r -> r.originator() == c);
    }

    @Override
    public boolean contains(Object o) {
        return o instanceof Range ? layout().stream().anyMatch(r -> r.contains((Range) o)) : false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        Window layout = layout();
        return c.stream().allMatch(r -> layout.contains(r));
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return mRanges.retainAll(c);
    }

    @Override
    public int indexOf(Object o) {
        for(int i = 0; i < mRanges.size(); i++)
            if(get(i) == o) return i;
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return mRanges.lastIndexOf(o);
    }

    @Override
    public ListIterator<Range> listIterator() {
        return mRanges.listIterator();
    }

    @Override
    public ListIterator<Range> listIterator(int index) {
        return mRanges.listIterator(index);
    }

    @Override
    public List<Range> subList(int fromIndex, int toIndex) {
        return mRanges.subList(fromIndex, toIndex);
    }

    @Override
    public void clear() {
        mRanges.clear();
    }

    @Override
    public Object[] toArray() {
        return mRanges.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return mRanges.toArray(a);
    }

    @Override
    public Iterator<Range> iterator() {
        return mRanges.iterator();
    }

    @Override
    public Stream<Range> stream() {
        return mRanges.stream();
    }

    // TODO Assume all the ranges are of the same sample size
    public int offset() {
        return isEmpty() ? 0 : stream().mapToInt(r -> r.offset()).min().getAsInt();
    }

    public int limit() {
        return isEmpty() ? 0 : stream().mapToInt(r -> r.limit()).max().getAsInt();
    }

    public int offsetInBytes() {
        return isEmpty() ? 0 : stream().mapToInt(r -> r.offset() * r.sampleSize()).min().getAsInt();
    }

    public int limitInByte() {
        return isEmpty() ? 0 : stream().mapToInt(r -> r.limit() * r.sampleSize()).max().getAsInt();
    }

    public int length() { return mRanges.size(); }

    public int size() {
        return size(true);
    }

    /**
     * Returns the number of samples in the ranges.
     *
     * @return sizeInBytes of the physical layout range in samples
     */
    public int size(boolean logical) {
        return logical ? stream().mapToInt(r -> r.size()).sum() : limit() - offset();
    }

    /**
     * Count the number of bytes written to the physical layout by component c.
     * The layout should be counted in the output channel of component c in case of redundancy.
     *
     * @param c the component producing the output samples in the layout
     * @return the number of bytes written by component c in the layout
     */
    public int count(esms.api.Component c, boolean original) {
        // count reesms.er local shift
        return stream()
                .filter(r -> r.writer() == c)
                .filter(r -> original ? r.originator() == c : r.originator() != c)
                .mapToInt(r -> (r.index() < 0 ? 2 : 1) * r.size() * r.sampleSize())
                .sum();
    }

    public int count() {
        // count reesms.er local shift
        return stream()
                .mapToInt(r -> (r.index() < 0 ? 2 : 1) * r.size() * r.sampleSize())
                .sum();
    }

    public esms.api.Component writerAt(int pos) {
        Range range = rangeAt(pos);
        return range == null ? null : range.writer();
    }

    public esms.api.Component originatorAt(int pos) {
        Range range = rangeAt(pos);
        return range == null ? null : range.originator();
    }

    public Range rangeAt(int pos) {
        int current = 0;
        for (Range range : this) {
            if (pos >= current && pos < current + range.size())
                return range;
            current += range.size();
        }
        return null;
    }

    public int indexAt(int pos) {
        int current = 0;
        for (Range range : this) {
            if (pos >= current + range.size()) {
                current += range.size();
                continue;
            }
            return range.indexAt(pos - current);
        }
        return -1;
    }

    public int offsetAt(int pos) {
        int current = 0;
        for (Range range : this) {
            if (pos >= current + range.size()) {
                current += range.size();
                continue;
            }
            return range.offsetAt(pos - current);
        }
        return -1;
    }

    public int offsetAt(esms.api.Component c, int index) {
        Range range = mRanges.stream()
                .filter(r -> r.writer() == c && r.indexAt(0) <= index && r.indexAt(r.size() - 1) >= index)
                .findAny().orElse(null);
        if (range != null) {
            for (int pos = 0; pos < range.size(); pos++) {
                if (range.indexAt(pos) == index)
                    return range.offsetAt(pos);
            }
        }
        return -1;
    }

    /**
     * Count the number ranges referencing the window layout offset.
     *
     * @param offset the layout offset.
     * @return
     */
    public int refCount(int offset) {
        return search(offset).size();
    }

    public Collection<Range> tail(int offset) {
        return tail(offset, true);
    }

    public Collection<Range> tail(int offset, boolean inclusive) {
        List<Range> ranges = new LinkedList<>();
        List<Range> sorted = new LinkedList<>(mRanges);
        Collections.sort(sorted);
        for (Range range : sorted) {
            if (range.offset() > offset || (inclusive && range.offset() == offset))
                ranges.add(range);
        }
        return ranges;
    }

    public List<Range> search(int offset) {
        List<Range> ranges = new ArrayList<>();
        List<Range> sorted = new ArrayList<>(mRanges);
        Collections.sort(sorted);
        for (Range range : sorted) {
            if (range.contains(offset))
                ranges.add(range);
            if (range.offset() > offset)
                break;
        }
        return ranges;
    }

    public List<Range> search(int offset, int size) {
        List<Range> ranges = new ArrayList<>();
        List<Range> sorted = new ArrayList<>(mRanges);
        Collections.sort(sorted);
        for (Range range : sorted) {
            if (range.contains(offset, size))
                ranges.add(range);
            if (range.offset() >= offset + size)
                break;
        }
        return ranges;
    }

    /**
     * Align the range to locate its offset in the layout.
     *
     * @param range the input range to locate
     * @return the offset in the layout
     */
    public int align(Range range) {
//        Log.i("align range %s with window \n%s", range, this);
        Optional<Range> ret = layout().stream().filter(r -> r.isAlignable(range)).findFirst();
        if (ret.isPresent()) {
            Range r = ret.get();
//            Log.i("aligned with %s\n", r);
            if (r.reverse()) {
                for (int pos = 0; pos < r.size(); pos++) {
                    if (r.indexAt(pos) == range.indexAt(range.size() - 1))
                        return r.offsetAt(pos) - range.offset();
                }
            } else {
                for (int pos = 0; pos < r.size(); pos++) {
                    if (r.indexAt(pos) == range.index())
                        return r.offsetAt(pos) - range.offset();
                }
            }
        }
        return -1;
    }

    public Window shift(int offset) {
        forEach(r -> r.shift(offset));
        return this;
    }

    /**
     * Remove holes between non-overlapped ranges.
     *
     * @return the compacted layout
     */
    public Window compact() {
        if (!isEmpty()) {
            List<Range> ranges = new LinkedList<>(mRanges);
            Collections.sort(ranges);
            int offset = 0;
            for (Range r : ranges) {
                if (r.offset() > offset) {
                    for (Range range : tail(offset, false))
                        range.shift(offset - r.offset());
                }
                offset = r.limit();
            }
        }
        return this;
    }

    private Window replace(Range r, List<Range> ranges) {
        int idx = indexOf(r);
        remove(r);
        addAll(idx, ranges);
        return this;
    }

    public Window reserve(int offset, int size) {
        tail(offset).forEach(r -> r.shift(size));
        search(offset).forEach(r -> replace(r, r.reserve(offset, size)));
        return this;
    }

    public Window insert(esms.api.Component c, int index, int pos, int size, int sampleSize) {
        return insert(c, index, pos, size, sampleSize);
    }

    /**
     * Insert only when the range at the position is present. Reserve otherwise.
     *
     * @param c
     * @param index
     * @param pos
     * @param size
     * @param incr
     * @return
     */
    public Window insert(esms.api.Component c, int index, int pos, int size, int sampleSize, int incr) {
        int offset = offsetAt(pos);
        if (offset < 0)
            throw new RuntimeException("No range at pos " + pos);

        List<Range> ranges = new ArrayList<>();
        Range range = rangeAt(pos);
        forEach(r -> {
            if (r == range) {
                ranges.addAll(r.insert(offset, new Range(c, c, index, offset, size, sampleSize, incr)));
            } else
                ranges.addAll(r.reserve(offset, size));
        });
        mRanges = ranges;
        return this;
    }

    /**
     * TODO Potential conflicts
     *
     * @param pos
     * @param size
     * @param range
     * @return
     */
    public Window update(int pos, int size, Range range) {
        int to = -1;
        while (size > 0) {
            Range r = rangeAt(pos);
            int offset = offsetAt(pos);
            int sz = r.reverse() ? Math.min(offset - r.offset() + 1, size) : Math.min(r.limit() - offset, size);
            List<Range> ranges = r.delete(offset, sz);
            if(to == -1) {
                if(offset == r.offset())
                   to = indexOf(r);
                else
                   to = indexOf(r) + 1;
            }

            replace(r, ranges);
            size -= sz;
        }

        if(to < mRanges.size())
            add(to, range);
        else
            add(range);
        return this;
    }

    public Window update(esms.api.Component c, int index, int pos, int size, boolean conflicted) {
        while (size > 0) {
            Range r = rangeAt(pos);
            int offset = offsetAt(pos);
            int sz = r.reverse() ? Math.min(offset - r.offset() + 1, size) : Math.min(r.limit() - offset, size);
            if (conflicted) {
                insert(c, index, pos, sz, r.sampleSize());
                delete(pos + sz, sz);
            } else {
                List<Range> ranges = r.update(offset, new Range(c, index, offset, sz, r.sampleSize()));
                replace(r, ranges);
            }
            index += sz;
            pos += sz;
            size -= sz;
        }
        return this;
    }

    public Window delete(int pos, int size) {
        while (size > 0) {
            Range r = rangeAt(pos);
            int offset = offsetAt(pos);
            int sz = r.reverse() ? Math.min(offset - r.offset() + 1, size) : Math.min(r.limit() - offset, size);
            replace(r, r.delete(offset, sz));
            size -= sz;
        }
        return this;
    }

    public Window slice(int pos, int size) {
        if (size == 0) return new Window();
        List<Range> ranges = new LinkedList<>();
        int current = 0;
        int limit = pos + size;
        for (Range range : mRanges) {
            boolean top = current >= pos && current < limit;
            boolean bottom = current + range.size() > pos && current + range.size() <= limit;
            if (!top && bottom) {
                int advance = pos - current;
                Range r = new Range(range);
                r.index(r.indexAt(advance));
                r.size(range.size() - advance);
                r.shift(advance);
                ranges.add(r);
            } else if (top && bottom)
                ranges.add(new Range(range));
            else if (top && !bottom)
                ranges.add(new Range(range).size(limit - current));
            else if (current <= pos && current + range.size() >= limit) {
                int advance = pos - current;
                Range r = new Range(range);
                r.index(r.indexAt(advance));
                r.size(size);
                r.shift(advance);
                ranges.add(r);
            }
            current += range.size();
        }
        return new Window(ranges);
    }

    public Window slice(int pos) {
        return slice(pos, size() - pos);
    }

    /**
     * Join adjacent ranges.
     * Peek/Pop operations do not care the writers.
     * Push operations the writers to skip writes.
     *
     * @return
     */
    public Window linearize(boolean sameWriter) {
        Window window = new Window();
        Range prev = null;
        boolean indirect = false;
        for (Range r: mRanges) {
            indirect = r.indirect();
            if (prev == null) {
                prev = r;
            } else {
                boolean linearizable = sameWriter ? prev.writer().equals(r.writer()) : true;
                int incr = r.offsetAt(0) - prev.offsetAt(prev.size() - 1);
                if (prev.size() == 1 && r.size() > 1 && r.incr() != incr)
                    linearizable = false;
                else if (prev.size() > 1 && r.size() == 1 && prev.incr() != incr)
                    linearizable = false;
                else if (prev.size() > 1 && r.size() > 1 && (prev.incr() != r.incr() || !prev.isAdjacent(r)))
                    linearizable = false;

                if (linearizable && prev.sampleSize() == r.sampleSize()) {
                    if (incr > 0)
                        prev = new Range(prev.writer(), prev.index(), prev.offset(), prev.size() + r.size(), prev.sampleSize(), incr);
                    else
                        prev = new Range(prev.writer(), prev.index(), r.offset(), prev.size() + r.size(), prev.sampleSize(), incr);
                } else {
                    window.add(prev.indirect(indirect));
                    prev = r;
                }
            }
        }

        if(prev != null)
            window.add(prev.indirect(indirect));
        return window;
    }

    /**
     * Join adjacent ranges if both originator and writer agree, and indexes are consecutive with incr 1.
     *
     * @return
     */
    public Window merge() {
        Window window = new Window();
        Range prev = null;
        for(Range r: mRanges) {
            if(prev == null)
                prev = r;
            else {
                boolean original = prev.originator() == r.originator() && prev.writer() == r.writer();
                boolean indexed = prev.index() + (prev.index() < 0 ? - prev.size() : prev.size()) == r.index();
                if(prev.isAdjacent(r) && original && indexed && prev.sampleSize() == r.sampleSize()) {
                    prev = prev.join(r);
                } else {
                    window.add(prev);
                    prev = r;
                }
            }
        }
        if(prev != null)
            window.add(prev);
        return window;
    }

    /**
     * Join adjacent ranges if possible while removing overlaps
     *
     * @return the reduced layout
     */
    public Window join() {
        LinkedList<Range> ranges = new LinkedList<>();
        for (int i = 0; i < mRanges.size(); i++) {
            Range r = mRanges.get(i);
            if (i == 0)
                ranges.add(r);
            else {
                Range prev = ranges.get(ranges.size() - 1);
                if (prev.isJoinable(r)) {
                    ranges.remove(prev);
                    prev = prev.join(r);
                    ranges.add(prev.join(prev));
                } else
                    ranges.add(r);
            }
        }
        mRanges = ranges;
        return this;
    }

    /**
     * Returns a window of non-overlapped ranges representing the physical layout.
     * For read, ranges should consistent with each other.
     * For write, later ranges overwrite.
     *
     * @return the sorted list of non-overlapped ranges
     */
    public Window layout() {
        final List<Range> ranges = new ArrayList<>();
        for (Range r : mRanges) {
            if (ranges.isEmpty())
                ranges.add(r);
            else {
                List<Range> overlaps = ranges.stream().filter(range -> range.overlaps(r)).collect(Collectors.toList());
                Range range = new Range(r);
                if (overlaps.isEmpty()) {
                    ranges.add(range);
                    continue;
                }
                for (Range overlapped : overlaps) {
                    int index = ranges.indexOf(overlapped);
                    ranges.remove(index);
                    if (overlapped.offset() >= range.offset()) {
                        if (overlapped.limit() > range.limit()) {
                            overlapped = overlapped.subrange(range.limit() - overlapped.offset());
                            ranges.add(index, overlapped);
                        }
                    } else {
                        ranges.add(index, overlapped.subrange(0, range.offset() - overlapped.offset()));
                        if (overlapped.limit() > range.limit()) {
                            overlapped = range.subrange(range.limit() - overlapped.offset());
                            ranges.add(index + 1, overlapped);
                        }
                    }
                }
                ranges.add(range);
            }
        }
        Collections.sort(ranges);
        return new Window(ranges).join();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        forEach(r -> builder.append("\t").append(r).append("\n"));
//        forEach(r -> builder.append("\t").append(String.format("%s, %X", r, r.hashCode())).append("\n"));
        return builder.toString();
    }
}
