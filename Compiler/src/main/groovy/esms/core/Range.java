package esms.core;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import esms.util.Log;

/**
 * Offset for absolute addressing is always increasing.
 * Index and position for logical addressing may be reverse.
 * Copy on Write operations: reserve, insert, update, delete, join, subrange, slice
 * In-place modifications: shift, resize
 */
public class Range implements Comparable<Range> {
    private esms.api.Component mWriter;
    private esms.api.Component mOriginator;
    private boolean mIndirect;
    private int mSampleSize;
    private int mIndex;
    private int mOffset;
    private int mIncr;
    private int mSize;

    public Range(esms.api.Component c, int index, int offset, int size) {
        this(c, index, offset, size, 1);
    }

    public Range(esms.api.Component c, int index, int offset, int size, int sampleSize) {
        this(c, index, offset, size, sampleSize, 1);
    }

    public Range(esms.api.Component c, int index, int offset, int size, int sampleSize, int incr) {
        this(c, c, index, offset, size, sampleSize, incr);
    }

    public Range(esms.api.Component writer, esms.api.Component originator, int index, int offset, int size) {
        this(writer, originator, index, offset, size, 4, 1);
    }

    public Range(esms.api.Component writer, esms.api.Component originator, int index, int offset, int size, int sampleSize, int incr) {
        writer(writer);
        originator(originator);
        sampleSize(sampleSize);
        index(index);
        offset(offset);
        incr(incr);
        size(size);
    }

    public Range(esms.api.Component c, int pos, int size) {
        this(c, c, pos, size);
    }

    /**
     * Returns an indirect addressing range.
     *
     * @param writer
     * @param originator
     * @param size
     */
    public Range(esms.api.Component writer, esms.api.Component originator, int pos, int size) {
        this(writer, originator, pos, pos, size);
        mIndirect = true;
    }

    public Range(Range range) {
        writer(range.writer());
        originator(range.originator());
        sampleSize(range.sampleSize());
        index(range.index());
        offset(range.offset());
        incr(range.incr());
        size(range.size());
    }

    public Range subrange(int offset) {
        return subrange(offset, false);
    }

    public Range subrange(int offset, boolean absolute) {
        if (absolute)
            offset -= mOffset;
        return subrange(offset, mSize - offset, false);
    }

    public Range subrange(int offset, int size) {
        return subrange(offset, size, false);
    }

    public Range subrange(int offset, int size, boolean absolute) {
        Range range = new Range(this);
        if (absolute)
            offset -= mOffset;
        if (size > mSize - offset)
            size = mSize - offset;
        range.index(reverse() ? range.indexAt(mSize - offset - size) : range.index() + (range.index() < 0 ? -offset : offset));
        range.offset(range.offset() + offset);
        range.size(size);
        return range;
    }

    public Range slice(int pos) {
        return slice(pos, mSize - pos);
    }

    public Range slice(int pos, int size) {
        Range range = new Range(this);
        if (size > mSize - pos)
            size = mSize - pos;

        range.index(indexAt(pos));
        range.offset(reverse() ? range.offset() + mSize - 1 - pos - (size - 1) : range.offsetAt(pos));
//        range.offset(offsetAt(pos));
        range.size(size);
        return range;
    }

    public esms.api.Component originator() {
        return mOriginator;
    }

    public esms.api.Component writer() {
        return mWriter;
    }

    public int index() {
        return mIndex;
    }

    public int offset() {
        return mOffset;
    }

    public int incr() { return mIncr; }

    public int size() { return mSize; }

    public int sampleSize() {
        return mSampleSize;
    }

    public int offsetInBytes() {
        return mOffset * mSampleSize;
    }

    public int sizeInBytes() {
        return mSize * mSampleSize;
    }

    public boolean reverse() { return mIncr < 0; }

    public boolean indirect() { return mIndirect; }

    public Range originator(esms.api.Component originator) {
        mOriginator = originator;
        return this;
    }

    public Range writer(esms.api.Component writer) {
        mWriter = writer;
        return this;
    }

    public Range component(esms.api.Component c) {
        mWriter = mOriginator = c;
        return this;
    }

    public Range indirect(boolean indirect) {
        mIndirect = indirect;
        return this;
    }

    public Range index(int index) {
        mIndex = index;
        return this;
    }

    public Range offset(int offset) {
        mOffset = offset;
        return this;
    }

    /**
     * Zero increment is possible for repeating samples.
     *
     * @param incr
     * @return
     */
    public Range incr(int incr) {
        mIncr = incr;
        return this;
    }

    public Range size(int size) {
        mSize = size;
        return this;
    }

    public Range sampleSize(int ss) {
        mSampleSize = ss;
        return this;
    }

    // Offset of the next sample to the last in FIFO order
    public int limit() {
        return mOffset + Math.abs(mSize * mIncr);
    }

    // Displacement in samples
    public int displacement() {
        return limit() - mOffset;
    }

    public Range shift(int offset) {
        mOffset += offset;
        return this;
    }

    public int indexAt(int pos) {
        if (pos < 0 || pos >= mSize) return -1;
        if (mIndex >= 0)
            return mIndex + pos;
        else
            return -(-mIndex + pos);
    }

    public int offsetAt(int pos) {
        if (pos < 0 || pos >= mSize) return -1;
        if (mIncr == 0)
            return mOffset;
        else if(mIncr > 0)
            return mOffset + pos * mIncr;
        else
            return mOffset + Math.abs(mSize * mIncr) + (pos + 1) * mIncr;
    }


    public boolean isEmpty() {
        return mSize <= 0;
    }

    public boolean isLocal() {
        return index() < 0;
    }

    public boolean contains(int offset) {
        return offset >= mOffset && offset < limit();
    }

    public boolean contains(int offset, int size) {
        return offset >= mOffset && offset + size < limit();
    }

    public boolean contains(Range r) {
        return contains(r.offset(), r.size()) && isCompatible(r);
    }

    public boolean overlaps(Range r) {
        return contains(r.offset()) || r.contains(mOffset);
    }

    public boolean isAdjacent(Range r) {
        return mIncr > 0 ? limit() == r.offset() : offset() == r.limit();
    }

    /**
     * Return true if both ranges overlap and have no conflicts with each other.
     * Otherwise, both ranges are compatible.
     *
     * @param range
     * @return
     */
    public boolean isCompatible(Range range) {
        Range container = null;
        Range contained = null;
        if (contains(range.offset())) {
            container = this;
            contained = range;
        } else if (range.contains(offset())) {
            container = range;
            contained = this;
        }

        if (container == null) return true;
        //if (!writer().equals(range.writer())) return false;
        if (!originator().equals(range.originator())) return false;
        if (reverse() != range.reverse()) return false;
        if (isLocal() != range.isLocal()) return false;
        if (mSampleSize != range.sampleSize()) return false;
        int pos = contained.offset() - container.offset();
        if (pos < 0) pos = -pos;
        return container.indexAt(pos) == contained.index();
    }

    /**
     * Join a range if compatible ,and overlapping or adjacent.
     *
     * @param r
     * @return
     */
    public boolean isJoinable(Range r) {
        if (contains(r.offset()) && isCompatible(r))
            return true;
        else if (isAdjacent(r)) {
            boolean ret = mWriter == r.writer() && mOriginator == r.originator();
            boolean indexed = mIndex + (mIndex < 0 ? - mSize : mSize) == r.index();
            if (r.reverse())
                ret = ret && (r.index() + r.size() == mIndex);
            else
                ret = ret && indexed;
            return ret;
        }
        return false;
    }

    /**
     * Return if input range can be shifted to align the indexes.
     *
     * @param r the input range
     * @return the displacement to shift
     */
    public boolean isAlignable(Range r) {
        boolean indexed = mIndex < 0
                ? r.index() <= mIndex && r.index() > mIndex - mSize
                : r.index() >= mIndex && r.index() < mIndex + mSize;
        return mOriginator == r.originator() //&& mWriter == r.writer()
                && reverse() == r.reverse() && mSampleSize == r.sampleSize()
                && indexed;
    }

    /**
     * Return the displacement to align an input range
     *
     * @param r the input range
     * @return the displacement
     */
    public int align(Range r) {
        return isAlignable(r) ? offsetAt(r.index() - mIndex) - mOffset : -1;
    }


    public Range join(Range range) {
        Range r = null;
        if (contains(range.offset()) || limit() == range.offset()) {
            r = new Range(this).size(range.limit() - offset());
            if (reverse())
                r.index(range.index());
        } else if (range.contains(offset()) || range.limit() == offset()) {
            r = new Range(this).size(limit() - range.offset());
            r.offset(range.offset());
            if (!reverse())
                r.index(range.index());
        }
        return r;
    }

    public Range intersect(Range r) {
        if (isCompatible(r)) {
            if (contains(r.offset())) {
                return new Range(r).index(reverse() ? mIndex : r.index()).size(limit() < r.limit() ? limit() - r.offset() : r.size());
            } else if (r.contains(mOffset)) {
                return new Range(this).index(reverse() ? r.index() : mIndex).size(limit() > r.limit() ? r.limit() - mOffset : mSize);
            }
        }
        return null;
    }

    /**
     * Reserve space of sizeInBytes at the specified absolute offset.
     *
     * @param offset
     * @param size
     * @return
     */
    public List<Range> reserve(int offset, int size) {
        List<Range> ranges = new LinkedList<>();
        if (offset <= mOffset) {
            ranges.add(new Range(this).shift(size));
        } else if (offset >= limit()) {
            ranges.add(new Range(this));
        } else if (offset > mOffset && offset < limit()) {
            Range head = new Range(this).subrange(0, offset - mOffset);
            Range tail = new Range(this).subrange(head.size()).shift(size);
            if (reverse()) {
                ranges.add(tail);
                ranges.add(head);
            } else {
                ranges.add(head);
                ranges.add(tail);
            }
        }
        return ranges;
    }

    /**
     * Insert a range at the specified absolute offset.
     *
     * @param offset
     * @param r
     * @return
     */
    public List<Range> insert(int offset, Range r) {
        List<Range> ranges;
        if (offset < mOffset || offset >= limit()) {
            ranges = new LinkedList<>();
            ranges.add(new Range(this));
        } else {
            ranges = reserve(offset, r.size());
            ranges.add(r);
            Collections.sort(ranges);
        }
        return ranges;
    }

    public List<Range> update(int offset, Range r) {
        List<Range> ranges = delete(offset, r.size());
        ranges.add(r);
        Collections.sort(ranges);
        return ranges;
    }

    public List<Range> delete(int offset, int size) {
        List<Range> ranges = new LinkedList<>();
        if (offset <= mOffset && offset + size < limit())
            ranges.add(new Range(this).subrange(offset + size - mOffset));
        else if (offset > mOffset && offset + size >= limit())
            ranges.add(new Range(this).subrange(0, offset - mOffset));
        else {
            Range head = new Range(this).subrange(0, offset - mOffset);
            Range tail = new Range(this).subrange(head.size() + size);
            if (reverse()) {
                if (!tail.isEmpty()) ranges.add(tail);
                if (!head.isEmpty()) ranges.add(head);
            } else {
                if (!head.isEmpty()) ranges.add(head);
                if (!tail.isEmpty()) ranges.add(tail);
            }
        }
        return ranges;
    }

    @Override
    public boolean equals(Object that) {
//        Log.i("%s.equals(%s)\n", this, that);
        Range range = (Range) that;
        if (!writer().equals(range.writer())) return false;
        if (!originator().equals(range.originator())) return false;
        if (index() != range.index() || offset() != range.offset()) return false;
        if (size() != range.size()) return false;
        if (reverse() != range.reverse()) return false;
        if (sampleSize() != range.sampleSize()) return false;
        if (incr() != range.incr()) return false;
        return true;
    }

    /**
     * FIXME Groovy uses compareTo to check if list items are equal, argh!
     *
     * @param range
     * @return
     */
    @Override
    public int compareTo(Range range) {
//        Log.i("%s.compareTo(%s)\n", this, range);
        int ret = Integer.compare(offset(), range.offset());
//        if(ret == 0) return Integer.compare(size(), range.size());
//        else return ret;
        return ret;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("<");
        if (!writer().equals(originator())) builder.append(originator()).append(",");
        builder.append(writer());
        if(mIndirect)
            builder.append(",indrect");
        else {
            builder.append(",").append(index());
            builder.append(",").append(offset());
        }
        builder.append(",").append(size());
        if(incr() != 1) builder.append(",(").append(incr()).append(")");
        if(sampleSize() != 4) builder.append(",").append(sampleSize());
        builder.append(">");
        return builder.toString();
    }
}
