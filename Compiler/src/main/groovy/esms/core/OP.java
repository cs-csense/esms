package esms.core;

/**
 * Created by farleylai on 6/8/14.
 */
public enum OP {
    PUSH,   // (PUSH, pos, expression)
    PEEK,   // (PEEK, pos, offset)
    POP,    // (POP, pos)

    APPEND, // (APPEND, size))
    CREATE, // (CREATE, size, reverse)
    INSERT, // (INSERT, pos, size)
    UPDATE, // (UPDATE, pos, size)
    PASS,   // (PASS, IN, pos, size)
//    DELETE, // (DELETE, pos, sizeInBytes)

//    MAP;     // (MAP, pos, sizeInBytes, sz)

//    @Override
//    public String toString() {
//        switch (this) {
//            case PUSH:
//                return "PUSH";
//            case PEEK:
//                return "PEEK";
//            case POP:
//                return "POP";
//            case APPEND:
//                return "APPEND";
//            case CREATE:
//                return "CREATE";
//            case INSERT:
//                return "INSERT";
//            case UPDATE:
//                return "UPDATE";
//            case DELETE:
//                return "DELETE";
//            case PASS:
//                return "PASS";
//            case MAP:
//                return "MAP";
//        }
//        return "";
//    }
}
