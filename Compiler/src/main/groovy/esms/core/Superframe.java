package esms.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import esms.api.Channel;
import esms.api.Component;

/**
 * Created by farleylai on 5/19/14.
 */
public class Superframe implements Iterable<Channel> {
    private List<Channel> mRoute;
    private List<Channel> mFuture;

    public Superframe() {
        mFuture = new ArrayList<>();
        mRoute = new ArrayList<>();
    }

    public Superframe(Superframe sf) {
        this();
        mRoute.addAll(sf.mRoute);
        mFuture.addAll(sf.mFuture);
    }

    public boolean hasSink(Component c) {
        return mRoute.stream().anyMatch(ch -> ch.isSink(c));
    }

    public boolean contains(Channel ch) {
        return mRoute.contains(ch) || mFuture.contains(ch);
    }

    public int indexOf(Channel ch) {
        int index = mRoute.indexOf(ch);
        if (index == -1) return mFuture.indexOf(ch);
        return index;
    }

    public boolean isEmpty() {
        return mRoute.isEmpty() && mFuture.isEmpty();
    }

    public List<Channel> route() {
        return mRoute;
    }

    public List<Channel> future() {
        return mFuture;
    }

    public Superframe add(Superframe sf) {
        mRoute.addAll(sf.route());
        mFuture.addAll(sf.future());
        return this;
    }

    public Superframe remove(Channel channel) {
        mRoute.remove(channel);
        mFuture.remove(channel);
        List<Channel> channels = mRoute.stream().filter(ch -> ch.isSource(channel.sink())).collect(Collectors.toList());
        channels.addAll(mFuture.stream().filter(ch -> ch.isSource(channel.sink())).collect(Collectors.toList()));
        channels.forEach(ch -> remove(ch));
        return this;
    }

    public Superframe clear() {
        mRoute.clear();
        mFuture.clear();
        return this;
    }

    /**
     * Expand the layout to cover the output channel.
     * Route: windowed sink to reserve
     * Future: source windows and pre-loaded sinks
     *
     * After windowing and before applying operations,
     * 1. filters: may cause pre-loading remainders
     *  in -> route
     *  out:
     *      next to schedule -> future before windowing
     *      upstream splitters -> future in case of pre-loaded remainders
     * 2. splitters:
     *  in -> route
     *  out:
     *      next to schedule -> future before windowing
     *      upstream splitters -> future in case of pre-loaded remainders
     * 3 joiners:
     *  in -> route
     *  out:
     *      next to schedule -> future before windowing
     *      upstream splitters -> future in case of pre-loaded remainders
     *
     * @param out the output channel
     * @return the expanded layout
     */
    public Superframe push(Channel out, Channel in) {
        // move in to route
        if(in != null) {
            if (mFuture.contains(in)) mFuture.remove(in);
            if (mRoute.contains(in)) mRoute.remove(in);
            mRoute.add(in);
        }

        if(out != null) {
            if (mFuture.contains(out)) mFuture.remove(out);
            if (mRoute.contains(out)) mRoute.remove(out);
            mFuture.add(out);
        }
        return this;
    }

    public Superframe push(Channel out) {
        return push(out, null);
    }

    @Override
    public Iterator<Channel> iterator() {
        return mRoute.iterator();
    }
}
