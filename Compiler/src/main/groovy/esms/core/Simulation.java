package esms.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import esms.api.*;
import esms.util.Log;

/**
 * Simulation results as configurations for each schedule.
 * Configurations are compared in terms of its total allocation size.
 *
 * Created by farleylai on 6/26/14.
 */
public class Simulation {
    private Map<Schedule, List<esms.api.Configuration>> mEvaluations;

    public Simulation() {
        mEvaluations = new TreeMap<>();
    }

    public Set<Schedule> schedules() {
        return mEvaluations.keySet();
    }

    public List<esms.api.Configuration> configurations(Schedule schedule) {
        return mEvaluations.get(schedule);
    }

    public Simulation put(Schedule schedule, List<esms.api.Configuration> cfgs) {
        mEvaluations.put(schedule, cfgs);
        return this;
    }

    /**
     * Returns a sorted list of configurations in ascending order.
     *
     * @return
     */
    public List<esms.api.Configuration> evaluate() {
        List<esms.api.Configuration> configs = new ArrayList<>();
        for(List<esms.api.Configuration> cfgs: mEvaluations.values())
            configs.addAll(cfgs);
        Collections.sort(configs);
        return configs;
    }

    /**
     * Collect information of the schedule and its configurations.
     *
     * @param schedule
     * @return
     */
    public String info(Schedule schedule) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("========== %s ==========\n", schedule));
        List<esms.api.Configuration> cfgs = mEvaluations.get(schedule);
        int i = 0;
        for (esms.api.Configuration cfg : cfgs) {
            builder.append(String.format("========== CFG [%d/%d] ===========\n", ++i, cfgs.size()));
            builder.append(cfg.info());
        }
        return builder.toString();
    }

    /**
     * Collect information about the schedules, configurations and evaluations.
     *
     * @return
     */
    public String info() {
        if(!Log.isLoggable(Log.Severity.DEBUG)) return "";
        StringBuilder builder = new StringBuilder();
        for(Schedule schedule: mEvaluations.keySet())
            builder.append(info(schedule));
        builder.append("========== Evaluations ==========\n");
        List<esms.api.Configuration> cfgs = evaluate();
        for(esms.api.Configuration cfg: cfgs) {
            builder.append(String.format("%s, %s at cost (%d, %d) vs StreamIt (%d, %d)\n",
                            cfg.schedule(), cfg, cfg.count(), cfg.allocation(), cfg.countStreamIt(), cfg.allocationStreamIt()));
        }

        return builder.toString();
    }

    public Simulation save(String filename, boolean verbose) throws IOException {
        for (List<esms.api.Configuration> cfgs : mEvaluations.values()) {
            for (esms.api.Configuration cfg : cfgs)
                cfg.save(filename, verbose);
        }
        return this;
    }
}
