package esms.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import esms.backend.java.JavaCodeGenerator;
import esms.data.Evaluation;
import esms.data.Performance;
import esms.toolkits.ESMSToolkit;
import esms.toolkits.StreamItToolkit4j;
import esms.toolkits.Toolkit;

import esms.types.Strategy;
import esms.types.Benchmark;
import static esms.benchmark.BenchmarkCFG.buildESMSPath;
import static esms.benchmark.BenchmarkCFG.buildMSA2Path;
import static esms.benchmark.BenchmarkCFG.buildStreamItPath;

/**
 * Created by farleylai on 11/4/14.
 */
public class DesktopBenchmarkBuilder {
    private static final String TAG = DesktopBenchmarkBuilder.class.getSimpleName();
    private Benchmark mBenchmark;
    private int mIterations;
    private int mScaling = 1;
    private boolean mPrintf = false;
    private List<Toolkit> mToolkits;
    private Evaluation mEvaluation;

//    private static String rewrite(String filename, boolean printf) {
//        File str = new File(filename);
//        try {
//            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(str)));
//            StringBuilder builder = new StringBuilder();
//            String ANNOTATION = "//BENCHMARK";
//            String line;
//            while((line = reader.readLine()) != null) {
//                if (!printf) {
//                    if (line.indexOf("//") == -1 && line.indexOf("println(") > -1)
//                        line = ANNOTATION + line;
//                } else {
//                    int pos = line.indexOf(ANNOTATION);
//                    if (pos > -1)
//                        line = line.substring(pos + ANNOTATION.length());
//                }
//                builder.append(line).append("\n");
//            }
//            RandomAccessFile writer = new RandomAccessFile(str, "rw");
//            writer.setLength(0);
//            writer.write(builder.toString().getBytes(Charset.forName("ISO-8859-1")));
//            reader.close();
//            writer.close();
//        } catch(Exception e) {
//            throw new RuntimeException(e);
//        }
//        return filename;
//    }

    public DesktopBenchmarkBuilder(Benchmark benchmark, int iterations, int scaling, boolean printf) {
        mBenchmark = benchmark;
        mIterations = iterations;
        mScaling = scaling;
        mPrintf = printf;
        mToolkits = new ArrayList<>();
        mEvaluation = new Evaluation();
//        for(String src: ESMS.src())
//            rewrite(src, printf);
    }

    public Toolkit addESMSToolkit4MSA2(String pkg) {
        String build = buildMSA2Path(mBenchmark, mScaling, pkg);
        ESMSToolkit tk = new ESMSToolkit(build, mBenchmark.name(), mIterations, mScaling, pkg);
        tk.profile.putAt("android", true);
        mToolkits.add(tk);
        return tk;
    }

    /**
     * FIXME Reject to add scaling ESMS toolkit currently.
     *
     * @param strategy
     * @param groups
     * @param plus
     * @param params
     * @return
     */
    public Toolkit addESMSToolkit(Strategy strategy, int groups, boolean plus, int... params) {
        if(mScaling > 1)
            return null;

        String build = buildESMSPath(mBenchmark, mScaling, strategy, groups, params);
        Toolkit tk = new ESMSToolkit(build, mBenchmark.name(), mIterations + (plus ? 1 : 0), mScaling, mPrintf, strategy, groups);
        mToolkits.add(tk);
        return tk;
    }

    public Toolkit addStreamItToolkit(int... params) {
        String build = buildStreamItPath(mBenchmark, mScaling, params);
        Toolkit tk = new StreamItToolkit4j(build, mBenchmark.name(), mIterations, mScaling, mPrintf);
        mToolkits.add(tk);
        return tk;
    }

    public DesktopBenchmarkBuilder res(String filename) {
        for(Toolkit tk: mToolkits)
            tk.res(filename);
        return this;
    }

    public DesktopBenchmarkBuilder build(boolean clean) throws Exception {
        for(Toolkit tk: mToolkits)
            mEvaluation.put(tk, tk.build(clean));
        return this;
    }

    public DesktopBenchmarkBuilder run() throws Exception {
//        strcPerf.put("writes", ESMS.countStreamIt(), Measure.UNIT.bytes);
//        mEvaluation.put(ESMS, censePerf);
        for(Toolkit tk: mToolkits) {
            Performance perf = tk.run();
            mEvaluation.put(tk, perf);
        }
        return this;
    }

    public List<Toolkit> getToolkits() {
        return mToolkits;
    }

    public List<Toolkit> getToolkits(String name) {
        List<Toolkit> toolkits = new ArrayList<>();
        for (Toolkit tk : mToolkits) {
            if (tk.name().equals(name))
                toolkits.add(tk);
        }
        return toolkits;
    }

    public boolean verify(int lines) {
        for(int i = 0; i < mToolkits.size() - 1; i++) {
            Toolkit tk1 = mToolkits.get(i);
            Toolkit tk2 = mToolkits.get(i+1);
            String cmd1 = Shell.pwd(tk1.executable().getName());
            String cmd2 = Shell.pwd(tk2.executable().getName());
            Log.i("Verifying %s(%s) and %s(%s)...\n", tk1.name(), tk1.summary(), tk2.name(), tk2.summary(), mScaling > 1 ? "w/ scaling factor " + mScaling : "");
            if(tk1 instanceof ESMSToolkit && ((ESMSToolkit) tk1).profile.getAt("backend") instanceof JavaCodeGenerator)
                cmd1 = "java " + tk1.executable().getName();
            if(!Shell.verify(tk1.pwd(), cmd1, tk2.pwd(), cmd2, lines))
                return false;
        }
        return true;
    }

    public DesktopBenchmarkBuilder cachegrind() throws Exception {
        for(Toolkit tk: mToolkits)
            tk.cachegrind();
        return this;
    }

    public DesktopBenchmarkBuilder cachegrind(int size, int associativity, int line) {
        for(Toolkit tk: mToolkits)
            tk.cachegrind(size, associativity, line);
        return this;
    }

    public DesktopBenchmarkBuilder report() {
        for(Toolkit tk: mToolkits) {
            Log.i(TAG, tk);
            Log.title("%s Compilation", tk.name());
            Log.i(TAG, mEvaluation.buildTimer(tk));
            Log.title("%s Performance", tk.target());
            Log.i(TAG, mEvaluation.performance(tk));
        }
//        Log.title("Improvements");
//        Log.i(mImprovements);
        return this;
    }

    public DesktopBenchmarkBuilder save() throws IOException {
        StringBuilder builder = new StringBuilder();
        for(Toolkit tk: mToolkits) {
            builder.setLength(0);
            builder.append(tk).append("\n");
            tk.save("toolkit.log", builder.toString());
            builder.setLength(0);
            builder.append(Formatter.title("%s Compilation", tk.name())).append("\n");
            builder.append(mEvaluation.buildTimer(tk)).append("\n");
            tk.save("compiler.log", builder.toString());
            builder.setLength(0);
            builder.append(Formatter.title("%s %s Performance", tk.name(), tk.target())).append("\n");
            builder.append(mEvaluation.performance(tk)).append("\n");
            tk.save("performance.log", builder.toString());
        }

//        builder.setLength(0);
//        builder.append(Formatter.title("%s Performance Improvements", ESMS.name())).append("\n");
//        builder.append(mImprovements).append("\n");
//        ESMS.save("improvements.log", builder.toString());
        return this;
    }
}
