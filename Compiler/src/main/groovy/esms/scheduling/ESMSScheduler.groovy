package esms.scheduling

import esms.api.SFG
import esms.api.Channel
import esms.api.Component
import esms.math.Matrix
import esms.util.Log
import groovy.time.TimeCategory

class ESMSScheduler {
    private SFG mSFG;
    private List<ESMSSchedule> mPASSchedules;
    private List<ESMSSchedule> mSASchedules;
    private List<ESMSSchedule> mSSASchedules;

    ESMSScheduler(SFG sfg) {
        mSFG = sfg;
        mPASSchedules = new ArrayList<>();
        mSASchedules = new ArrayList<>();
        mSSASchedules = new ArrayList<>();
    }

    /**
     * Simulate a toplogical ordering to derive a PASS
     * Execution trace: component X iterations
     * Channel buffer trace: channels X iterations
     *
     * @param ordering A topological ordering
     * @return
     */
    ESMSSchedule simulatePASS(List<Component> ordering) {
        Matrix tmPop = mSFG.getTopologyMatrixPop();
        if (tmPop.rank() == mSFG.nodes())
            throw new RuntimeException("No PASS exists for rank==" + mSFG.nodes() + ", singular values: \n" + tmPop.svd(true));

        List<Component> order = new ArrayList<>(ordering);
        Collections.reverse(order);
        boolean reached = false;
        int maxIters = 512 * 4 * 4;
        Matrix channelBufferTrace = new Matrix(mSFG.edges(), maxIters + 1);
        Matrix execTrace = new Matrix(mSFG.nodes(), maxIters);
        Component first = null;
        Component downstream = null;
        ESMSSchedule PASS = null;
        for (int iter = 0; !reached && iter < maxIters; iter++) {
            boolean deadlocked = true;
            Component dependent = null;
            for (Component c : order) {
                // check channel src availability
                boolean available = true;
                boolean source = true;
                int invocations = 0;
                for (int i = 0; i < mSFG.edges(); i++) {
                    Channel channel = mSFG.getChannel(i);
                    Channel.State sin = mSFG.getState(channel);
                    if (channel.sink() == c) {
                        source = false;
                        if (channelBufferTrace.getInt(i, iter) < sin.PEEK) {
                            if (dependent == null || c.equals(dependent)) dependent = channel.src();
                            available = false;
                            break;
                        } else {
                            int d = (channelBufferTrace.getInt(i, iter) - sin.PEEK) / sin.POP + 1;
                            if (invocations == 0) invocations = d;
                            else invocations = Math.min(d, invocations);
                        }
                    }
                }

                if (source) available = (dependent == null || c == dependent);
                if (!available) continue;
                if (c == dependent) dependent = null;

                // simulatePASS the execution of the scheduled writerAt
                if (first == null) first = c;
                invocations = source ? 1 : invocations;
                execTrace.set(mSFG.indexOf(c), iter, invocations);
                Matrix v = execTrace.column(iter);
                Matrix bp = channelBufferTrace.column(iter);
                Matrix bd = tmPop.mult(v);
                Matrix b = bp.plus(bd);
                channelBufferTrace.set(0, iter + 1, b);
                downstream = c;
                deadlocked = false;
                break;
            }
            if (deadlocked) throw new RuntimeException("No PASS exists without delays!");

            // check if steady state is reached
            Matrix bi = channelBufferTrace.column(iter);
            for (int j = 0; j < iter; j++) {
                Matrix bj = channelBufferTrace.column(j);
                if (bj.equals(bi) && downstream.equals(first)) {
                    // found a Super PASS ending with a complete phase
                    execTrace = execTrace.extract(0, Matrix.END, 0, iter - 1);
                    channelBufferTrace = channelBufferTrace.extract(0, Matrix.END, 0, iter);
                    PASS = new ESMSSchedule(mSFG, ordering, execTrace, channelBufferTrace, j);
//                    mPIPE = Schedule.makePIPE(mPASS, this);
//                    mMLS = Schedule.makeMLS(mPASS, this);
//                    mSuperframeMLS = Schedule.makeSuperframeMLS(mMLS, this);
//                    mSAS = Schedule.makeSAS(mPASS, this);
//                    mSASchedules = Schedule.makeSuperframeSAS(mSAS, this);
                    reached = true;
                    break;
                }
            }
            if (!reached && iter == maxIters - 1) {
                Log.d(channelBufferTrace.transpose());
                throw new RuntimeException("PASS not reached yet within iterations!");
            }
        }
        return PASS;
    }

    /**
     * Expand a merged execution trace into an SAS trace
     *
     * @param merged the merged execution trace to expand
     * @param order the scheduling order
     * @return
     */
    Matrix expandTrace(Matrix merged, List<Component> order) {
        int c = 0;
        Matrix trace = new Matrix(mSFG.nodes(), (int) merged.gt(0).sum());
        for (int i = 0; i < merged.cols(); i++) {
            for (Component op : order) {
                int idx = mSFG.indexOf(op);
                if (merged.getInt(idx, i) == 0) continue;
                trace.set(idx, c++, merged.getInt(idx, i));
            }
        }
        return trace;
    }

    /**
     * Simulate the execution trace to derive the channel buffer trace.
     *
     * @param execTrace the execution trace
     * @param order the component simulation order
     * @return the channel buffer usage trace
     */
    Matrix simulateChannelBufferTrace(Matrix execTrace, List<Component> order) {
        Matrix channelBufferTrace = new Matrix(mSFG.edges(), 1 + (int) execTrace.gt(0).sum());
        Matrix TM = mSFG.getTopologyMatrixPop();
        int i = 0;
        for (int t = 0; t < execTrace.cols(); t++) {
            for (Component op : order) {
                int idx = mSFG.indexOf(op);
                if (execTrace.get(idx, t) == 0) continue;
                Matrix d = new Matrix(mSFG.nodes(), 1);
                d.set(idx, 0, execTrace.get(idx, t));
                Matrix diff = TM.mult(d);
                channelBufferTrace.set(0, i + 1, channelBufferTrace.column(i).plus(diff));
                i++;
            }
        }

        return channelBufferTrace.extract(0, Matrix.END, 0, channelBufferTrace.cols() - 1);
    }

    /**
     * Make a StreamIt SAS
     *
     * @param pass
     * @return
     */
    ESMSSchedule makeSAS(ESMSSchedule pass) {
        List<Component> order = pass.order();
        Matrix initExecTrace = pass.initExecTrace();
        if (initExecTrace != null)
            initExecTrace = expandTrace(initExecTrace.sum(false), order);

        Matrix steadyExecTrace = expandTrace(pass.steadyExecTrace().sum(false), order);
        Matrix execTrace = initExecTrace == null ? steadyExecTrace : initExecTrace.combine(0, Matrix.END, steadyExecTrace);
        return new ESMSSchedule(mSFG, order, execTrace,
                simulateChannelBufferTrace(execTrace, order),
                initExecTrace == null ? 0 : initExecTrace.cols())
                .setName("SAS");
    }

    /**
     * Make a superframe SAS which makes sure one single superframe allocation is used up in each iteration.
     *
     * @param sas
     * @param factor
     * @return
     */
    ESMSSchedule makeSuperframeSAS(ESMSSchedule sas, int factor) {
        List<Component> order = sas.order();
        int last = mSFG.indexOf(order.get(order.size() - 1));
        Matrix initExecTrace = sas.initExecTrace();
        Matrix steadyExecTrace = sas.steadyExecTrace().sum(false);
        if (initExecTrace != null) {
            initExecTrace = initExecTrace.sum(false);
            if (initExecTrace.getInt(last, 0) == 0) {
                // merge with the steady run trace
                initExecTrace = initExecTrace.plus(steadyExecTrace);
            }
            initExecTrace = expandTrace(initExecTrace, order);
        }

        steadyExecTrace = expandTrace(steadyExecTrace, order);
        if(factor > 1) {
            Log.i("Apply execution scaling factor %d\n", factor);
            for (int i = 0; i < steadyExecTrace.cols(); i++) {
                for (int j = 0; j < steadyExecTrace.rows(); j++)
                    steadyExecTrace.set(j, i, steadyExecTrace.getInt(j, i) * factor);
            }
        }
        Matrix execTrace = initExecTrace == null ? steadyExecTrace : initExecTrace.combine(0, Matrix.END, steadyExecTrace);
        return new ESMSSchedule(mSFG, order, execTrace, simulateChannelBufferTrace(execTrace, order), initExecTrace == null ? 0 : initExecTrace.cols()).setName("SSAS");
    }

    /**
     * Enuemerate possible superframe SASs w.r.t. topological orderings.
     *
     * @param factor execution scaling factor
     * @param all every possible topological ordering or just the program ordering
     * @return
     */
    public ESMSScheduler makeSchedules(int factor, boolean all) {
        def t0 = new Date()
        Date t1, t2, t3, t4
        mPASSchedules.clear();
        mSASchedules.clear();
        mSSASchedules.clear();
        for (List<Component> s : mSFG.topsort(all)) {
            t1 = new Date()
            ESMSSchedule PASS = simulatePASS(s);
            t2 = new Date()
            ESMSSchedule SAS = makeSAS(PASS);
            t3 = new Date()
            ESMSSchedule SSAS = makeSuperframeSAS(SAS, factor);
            t4 = new Date()
            mPASSchedules.add(PASS);
            mSASchedules.add(SAS);
            mSSASchedules.add(SSAS);

        }
        def t5 = new Date()
        println("makeSchedules: ${TimeCategory.minus(t5, t0)}")
        println("PASS: ${TimeCategory.minus(t2, t1)}")
        println("SAS: ${TimeCategory.minus(t3, t2)}")
        println("SSAS: ${TimeCategory.minus(t4, t3)}")
        return this;
    }

    public List<ESMSSchedule> getSASchedules() {
        return mSASchedules;
    }

    public List<ESMSSchedule> getSSASchedules() {
        return mSSASchedules;
    }
}