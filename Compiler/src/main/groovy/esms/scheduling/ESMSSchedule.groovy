package esms.scheduling

import esms.api.Component;
import esms.api.SFG;
import esms.math.Matrix

/**
 * Created by farleylai on 2/16/14.
 */
public class ESMSSchedule implements Comparable<ESMSSchedule> {
    private SFG mSFG;
    private List<Component> mOrder;
    private String mName;
    private Matrix mExecTrace;
    private Matrix mChannelBufferTrace;
    private int mInitLength;
    def partitions = [:]
    public ESMSSchedule(SFG sfg, List<Component> order, Matrix execTrace, Matrix channelBufferTrace, int init) {
        mSFG = sfg;
        mOrder = order;
        mName = "PASS";
        mInitLength = init;
        mExecTrace = execTrace;
        mChannelBufferTrace = channelBufferTrace;
        partitions[0] = order
    }

    public ESMSSchedule(SFG sfg, List<Component> order, Matrix execTrace, Matrix channelBufferTrace, int init, partitions) {
        mSFG = sfg;
        mOrder = order;
        mName = "PASS";
        mInitLength = init;
        mExecTrace = execTrace;
        mChannelBufferTrace = channelBufferTrace;
        partitions = partitions
    }

    public List<Component> order() {
        return mOrder;
    }

    public String getName() {
        return mName;
    }

    public ESMSSchedule setName(String name) {
        mName = name;
        return this;
    }

    public int initPeriod() {
        return mInitLength;
    }

    public int steadyPeriod() {
        return mExecTrace.cols() - mInitLength;
    }

    public Matrix initExecTrace() {
        if (mInitLength == 0) return null;
        return mExecTrace.extract(0, Matrix.END, 0, mInitLength - 1);
    }

    public Matrix steadyExecTrace() {
        return mExecTrace.extract(0, Matrix.END, mInitLength, Matrix.END);
    }

    public int initInvocations(Component c, int order) {
        return mExecTrace.getInt(mSFG.indexOf(c), order);
    }

    public int steadyInvocations(Component c, int order) {
        return mExecTrace.getInt(mSFG.indexOf(c), mInitLength + order);
    }

    /**
     * TODO A component may appear several times in general.
     * First order in the initialization period.
     */
    public int initOrder(Component c) {
        for (int i = 0; i < initPeriod(); i++)
            if (initInvocations(c, i) > 0) return i;
        return -1;
    }

    /**
     * TODO A component may appear several times in general.
     * First order in the steady state period.
     */
    public int steadyOrder(Component c) {
        for (int i = 0; i < steadyPeriod(); i++)
            if (steadyInvocations(c, i) > 0) return i;
        return -1;
    }

    /**
     * Get component by order in initialization.
     *
     * @param order the scheduling order in initialization.
     * @return the component to schedule in the initialization order.
     */
    public Component initComponent(int order) {
        for (int i = 0; i < mExecTrace.rows(); i++) {
            Component c = mSFG.getComponent(i);
            if (initInvocations(c, order) > 0)
                return c;
        }
        return null;
    }

    /**
     * Get component by order in steady state.
     *
     * @param order the scheduling order in steady state.
     * @return the component to schedule in the steady state order.
     */
    public Component steadyComponent(int order) {
        for (int i = 0; i < mExecTrace.rows(); i++) {
            Component c = mSFG.getComponent(i);
            if (steadyInvocations(c, order) > 0)
                return c;
        }

        return null;
    }

    public int getChannelBufferSize(int ch) {
        return mChannelBufferTrace.row(ch).maxAbs();
    }

    public Matrix initChannelBufferTrace() {
        if (mInitLength == 0) return null;
        return mChannelBufferTrace.extract(0, Matrix.END, 0, mInitLength - 1);
    }

    public Matrix steadyChannelBufferTrace() {
        return mChannelBufferTrace.extract(0, Matrix.END, mInitLength, Matrix.END);
    }

    public String execTrace() {
        StringBuilder builder = new StringBuilder();
        int period = mExecTrace.cols() - mInitLength;
        builder.append(String.format("%s Execution Trace (%d:%d)\n", mName, mInitLength, period));
        Matrix init = initExecTrace();
        if (init == null)
            builder.append("No initialization period\n");
        else {
            builder.append("Initialization Execution Trace\n");
            builder.append(init.transpose());
        }
        builder.append("Steady Execution Trace\n");
        builder.append(steadyExecTrace().transpose());
        return builder.toString();
    }

    public String channelBufferTrace() {
        StringBuilder builder = new StringBuilder();
        int period = mExecTrace.cols() - mInitLength;
        builder.append(String.format("%s Channel Buffer Trace (%d:%d)\n", mName, mInitLength, period));
        builder.append(mChannelBufferTrace.transpose());
        return builder.toString();
    }

    public String trace() {
        StringBuilder builder = new StringBuilder();
        builder.append(execTrace());
        builder.append(channelBufferTrace());
        return builder.toString();
    }

    @Override
    public int compareTo(ESMSSchedule that) {
        return toString().compareTo(that.toString());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder().append("[").append(mName).append("] ");
        for (int i = 0; i < steadyPeriod(); i++)
            builder.append(steadyComponent(i).name()).append(i < steadyPeriod() - 1 ? "," : "");
        return builder.toString();
    }
}
