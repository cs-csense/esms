package esms.scheduling

import esms.api.Scheduler
import esms.api.Schedule
import esms.streamit.Filter
import esms.streamit.Joiner
import esms.streamit.Pipeline
import esms.streamit.SplitJoin
import esms.streamit.Splitter
import esms.streamit.Stream
import streamit.frontend.nodes.StreamSpec
import streamit.frontend.nodes.StreamType

class PhasedScheduler implements Scheduler {
    Stream stream
    Schedule schedule
    Map<Stream, PhasedSchedule> schedules

    PhasedScheduler(Stream s) {
        stream = s
        schedules = [:] as Map<Stream, PhasedSchedule>
        stream.accept(this)
    }

    int getInitPushRate(Stream s) {
        if(s.isFilter() || s.isSplitter() || s.isJoiner()) return 0
        Stream tail = s.tail()
        int initPush = getInitPushRate(tail)
        int steadyPush = getSteadyPushRate(tail)
        return initPush + steadyPush * schedules[s].initInvocations(tail)
    }

    int getInitPopRate(Stream s) {
        if(s.isFilter() || s.isSplitter() || s.isJoiner()) return 0
        Stream head = s.head()
        int initPop = getInitPopRate(head)
        int steadyPop = getSteadyPopRate(head)
        return initPop + steadyPop * schedules[s].initInvocations(head)
    }

    int getInitPeekRate(Stream s) {
        if(s.isFilter() || s.isSplitter() || s.isJoiner()) return 0
        Stream head = s.head()
        int initPeek = getInitPeekRate(head)
        int steadyPop = getSteadyPopRate(head)
        int steadyPeek = getSteadyPeekRate(head)
        int invocations = schedules[s].initInvocations(head)
        return initPeek + (invocations > 0 ? steadyPop * invocations + (steadyPeek - steadyPop) : 0)
    }

    int getSteadyPushRate(Stream s, Stream output = null) {
        if(s.isSplitter()) return s.getPushRate(output)
        if(s.isFilter() || s.isJoiner()) return s.getPushRate()
        Stream tail = s.tail()
        return getSteadyPushRate(tail) * schedules[s].steadyInvocations(tail)
    }

    int getSteadyPopRate(Stream s, Stream input = null) {
        if(s.isJoiner()) return s.getPopRate(input)
        if(s.isFilter() || s.isSplitter()) return s.getPopRate()
        Stream head = s.head()
        return getSteadyPopRate(head) * schedules[s].steadyInvocations(head)
    }

    int getSteadyPeekRate(Stream s, Stream input = null) {
        if(s.isJoiner()) return s.getPeekRate(input)
        if(s.isFilter() || s.isSplitter()) return s.getPeekRate()
        Stream head = s.head()
        int steadyPop = getSteadyPopRate(head)
        int steadyPeek = getSteadyPeekRate(head)
        return steadyPop * schedules[s].steadyInvocations(head) + (steadyPeek - steadyPop)
    }

    PhasedSchedule reduce(PhasedSchedule result, Stream s) {
        if(s.isFilter() || s.isSplitter() || s.isJoiner()) {
            PhasedSchedule schedule = schedules[s.parent()]
            result.addInit(s, schedule.initInvocations(s))
            result.addSteady(s, schedule.steadyInvocations(s))
        } else {
            schedules[s].each { Stream child, int initInvocations, int steadyInvocations ->
                if(child.isPipeline() || child.isSplitJoin()) {
//                    println("child ${s.name()}, init: $initInvocations, steady: $steadyInvocations")
                    PhasedSchedule schedule = schedules[child]
                    schedule.each { Stream it, int itsInitInvocations, int itsSteadyInvocations ->
//                        println("grandchild ${it.name()}, init: $itsInitInvocations, steady: $itsSteadyInvocations")
                        schedule.setInit(it, itsInitInvocations + initInvocations * itsSteadyInvocations)
                        schedule.setSteady(it, steadyInvocations * itsSteadyInvocations)
//                        println("revised schedule: ${schedule.info()}")
                    }
                }
                reduce(result, child)
            }
        }
        return result
    }

    @Override
    int getPushRate(int phase, Stream s) {
        switch(phase) {
            case Schedule.INIT: return getInitPushRate(s)
            case Schedule.STEADY: return getSteadyPushRate(s)
            default:
                throw new RuntimeException("Unsupported phase $phase")
        }
    }

    @Override
    int getPeekRate(int phase, Stream s) {
        switch(phase) {
            case Schedule.INIT: return getInitPeekRate(s)
            case Schedule.STEADY: return getSteadyPeekRate(s)
            default:
                throw new RuntimeException("Unsupported phase $phase")
        }
    }

    @Override
    int getPopRate(int phase, Stream s) {
        switch(phase) {
            case Schedule.INIT: return getInitPopRate(s)
            case Schedule.STEADY: return getSteadyPopRate(s)
            default:
                throw new RuntimeException("Unsupported phase $phase")
        }
    }

    @Override
    Schedule schedule(Stream s) {
        return schedules[s]
    }

    @Override
    Schedule schedule() {
        if(schedule) return schedule
        else
            return schedule = reduce(new PhasedSchedule(this, stream), stream)
    }

    @Override
    Schedule visitPipeline(Pipeline pipeline) {
        //println("visiting ${pipeline.name()}...")

        // Restructure the pipeline if there is any non-first peeking stream
        def pos = []
        pipeline.eachWithIndex { Stream s, int i ->
            if(s.isPeeking()) pos << i
        }
        //println("peeking pos: $pos")
        if(pos.size() > 0 && (pos.size() > 1 || pos[0] != 0)) {
            def pipelines = []
            pos.eachWithIndex { int start, int idx ->
                int end = pos[idx+1] ? pos[idx+1] : pipeline.size()
                StreamSpec spec = new StreamSpec(
                        pipeline.spec().getContext(),
                        StreamSpec.STREAM_PIPELINE,
                        new StreamType(pipeline.spec().getContext(),
                                pipeline.get(start).spec().getStreamType().getIn(),
                                pipeline.get(end - 1).spec().getStreamType().getOut()),
                        "Pipeline", Collections.emptyList(), Collections.emptyList(), Collections.emptyList()
                )
                Pipeline p = new Pipeline(pipeline, null, spec)
                for(int i = start; i < end; i++)
                   p.add(pipeline.get(i))
                Stream prev = start == 0 ? null : pipelines.isEmpty() ? pipeline.get(start - 1) : pipelines[-1]
                if(prev) {
                    prev.last().unlink(p.first())
                } else {
                    prev = pipeline.getInputs()[0]
                    prev?.unlink(pipeline)
                }
                pipelines << p
//                println("adding pipeline: $p")
            }
            if(pos[0] == 0) pipeline.clear()
            else {
                pipeline.removeRange(pos[0], pipeline.size())
                //println("pipeline removed range: $pipeline")
            }
            pipeline.addAll(pipelines)
//            println("adding pipelines: $pipelines")
//            println("Restructured pipeline: $pipeline")
        }

        PhasedSchedule schedule = new PhasedSchedule(this, pipeline)
        // Compute child stream schedules
        pipeline.each {
            it.accept(this)
        }

        // Compute the init schedule if there is any peeking stream
        def init = [0] * pipeline.size()

        // Drain the additional production
        def drain = { int production, int from ->
            for(int idx = from; production > 0 && idx < pipeline.size(); idx++) {
                Stream s = pipeline.get(idx)
                //println("[drain] ${s.name()}=${pipeline.name()}[$idx]: production=$production, steadyPeek=${getSteadyPeekRate(s)}, steadyPop=${getSteadyPopRate(s)}")
                int steadyPop = getSteadyPopRate(s)
                int invocations = production / steadyPop
                init[idx] += invocations
                production = invocations * getSteadyPushRate(s)
            }
        }

        int i = -1
        int needed = 0
        pipeline.reverseEach { Stream s ->
            int initPush = getInitPushRate(s)
            int initPeek = getInitPeekRate(s)
            int steadyPush = getSteadyPushRate(s)
            int steadyPeek = getSteadyPeekRate(s)
            int steadyPop = getSteadyPopRate(s)
//            println("${s.name()}: initPush=$initPush, initPeek=$initPeek, steadyPush=$steadyPush, steadyPeek=$steadyPeek, steadyPop=$steadyPop")
            int invocations = needed == 0 ? 0 : (needed - initPush + steadyPush - 1) / steadyPush
            if (invocations < 0) invocations = 0

            int production = initPush + invocations * steadyPush
//            println("invocations=$invocations, production=$production, needed=$needed")
            if (production > needed)
                drain(production - needed, pipeline.size() + i + 1)

            // steadyPeek - steadyPop <= needed <= initPeek + invocations * steadyPop
            needed = Math.max(initPeek + invocations * steadyPop, steadyPeek - steadyPop)
            init[i--] = invocations
        }
        pipeline.eachWithIndex { s, idx ->
            schedule.addInit(s, init[idx])
        }

        // Compute the steady schedule
        def steady = []
        pipeline.inject(1) { BigInteger push, Stream s ->
            steady << push
            push * getSteadyPushRate(s)
        }
//        println("${pipeline.name()} steady push: $steady")

        i = -1
        pipeline.reverseInject(1) { BigInteger pop, Stream s ->
            steady[i--] *= pop
            // FIXME allows dummies at the end
            int rate = getSteadyPopRate(s)
            rate == 0 ? 1 : pop * getSteadyPopRate(s)
        }
        def gcd = steady.inject(steady[0]) { BigInteger ret, v ->
            ret.gcd(v)
        }
//        println("${pipeline.name()} steady pop before gcd($gcd): $steady")
        pipeline.eachWithIndex { s, idx ->
            schedule.addSteady(s, steady[idx] = steady[idx].intdiv(gcd))
        }
//        println("${pipeline.name()} steady pop after gcd: $steady")
        schedules[pipeline] = schedule
        //println("visited ${pipeline.name()}...")
//        println(schedule.info())
        return schedule
    }

    @Override
    Schedule visitSplitJoin(SplitJoin splitjoin) {
        //println("visiting ${splitjoin.name()}...")
        PhasedSchedule schedule = new PhasedSchedule(this, splitjoin)
        // Compute child stream schedules
        splitjoin.head().accept(this)
        splitjoin.each {
            it.accept(this)
        }
        splitjoin.tail().accept(this)

        // Compute the init schedule
        def init = [[0, 0, 0]]
        (splitjoin.size() - 1).times {
            init << init[0].clone()
        }
        int invocations = 0
        splitjoin.eachWithIndex { Stream s, int i ->
            int needed = Math.max(getInitPeekRate(s), getSteadyPeekRate(s) - getSteadyPopRate(s))
            int push = splitjoin.head().getPushRate(s)
            invocations = Math.max(invocations, (needed + push - 1).intdiv(push))
        }
        splitjoin.eachWithIndex { Stream s, int i ->
            int needed = Math.max(getInitPeekRate(s), getSteadyPeekRate(s) - getSteadyPopRate(s))
            int production = splitjoin.head().getPushRate(s) * invocations
            init[i][0] = invocations
            init[i][1] = (production - needed).intdiv(getSteadyPopRate(s))
            // FIXME allows dummy join
            init[i][2] = splitjoin.tail().getPopRate(s) == 0 ? 0 : (getInitPushRate(s) + init[i][1] * getSteadyPushRate(s)).intdiv(splitjoin.tail().getPopRate(s))
        }
        schedule.addInit(splitjoin.head(), init[0][0])
        init.eachWithIndex { branch, i ->
            schedule.addInit(splitjoin.get(i), init[i][1])
        }
        schedule.addInit(splitjoin.tail(), init[0][-1])

        // Compute the steady schedule
        def steady = []
        (0..<splitjoin.size()).each {
            def branch = []
            splitjoin.inject(1, it) { BigInteger push, s ->
                //println("branch[$it]: ${s.name()} with push rate=$push")
                branch << push
                push * getSteadyPushRate(s, s.isSplitter() ? splitjoin.get(it) : null)
            }
            steady << branch
        }
//        println("${splitjoin.name()} steady push: $steady")

        (0..<steady.size()).each {
            int i = -1
            splitjoin.reverseInject(1, it) { BigInteger pop, s ->
                //println("branch[$it]: ${s.name()} with pop rate=$pop")
                steady[it][i--] *= pop
                // FIXME in case of 0 steady pop rate for dummies
                int rate = getSteadyPopRate(s, s.isJoiner() ? splitjoin.get(it) : null)
                rate == 0 ? 1 : pop * rate
            }
        }
//        println("${splitjoin.name()} steady pop before gcd: $steady")

        def gcds = []
        steady.each {
            def gcd = it.inject(it[0]) { BigInteger ret, v ->
                ret.gcd(v)
            }
            gcds << gcd
            it.eachWithIndex { v, i ->
                it[i] = v.intdiv(gcd)
            }
        }
//        println("${splitjoin.name()} steady pop after gcds($gcds): $steady")
        // Sanity check if splitter push rates and joiner pop rates are consistent
        boolean consistent = steady.every { it[0] == steady[0][0] && it[-1] == steady[0][-1] }
        if(!consistent) {
            throw new RuntimeException("Inconsistent steady push/pop rates for splitjoin ${splitjoin.name()}: \n$steady")
        }

        schedule.addSteady(splitjoin.head(), steady[0][0])
        steady.eachWithIndex { branch, m ->
            branch.eachWithIndex { s, n ->
                steady[m][n] = steady[m][n]
            }
            schedule.addSteady(splitjoin.get(m), steady[m][1])
        }
        schedule.addSteady(splitjoin.tail(), steady[0][-1])

        schedules[splitjoin] = schedule
        //println("visited ${splitjoin.name()}...")
        //println(schedule.info())
        return schedule
    }

    @Override
    Schedule visitSplitter(Splitter s) {
        return schedules[s.parent()]
    }

    @Override
    Schedule visitJoiner(Joiner s) {
        return schedules[s.parent()]
    }

    @Override
    Schedule visitFilter(Filter s) {
        return schedules[s.parent()]
    }
}