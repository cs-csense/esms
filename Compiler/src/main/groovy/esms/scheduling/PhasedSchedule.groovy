package esms.scheduling

import esms.api.Channel
import esms.api.Schedule
import esms.api.Scheduler
import esms.streamit.Stream
import streamit.frontend.nodes.Type
import streamit.frontend.nodes.TypePrimitive

import javax.lang.model.type.PrimitiveType

/**
 * Created by farleylai on 2/16/14.
 */
public class PhasedSchedule implements Schedule, Comparable<Schedule> {
    Scheduler scheduler
    Stream stream;
    def phaseStreams = [:].withDefault { [] }
    def phaseInvocations = [:].withDefault { [:].withDefault { 0 } }

    PhasedSchedule(Scheduler scheduler, Stream s) {
        this.scheduler = scheduler
        stream = s
    }

    @Override
    Scheduler scheduler() {
        return scheduler
    }

    @Override
    Stream stream() {
        return stream
    }

    @Override
    boolean hasPhase(int phase) {
        return phaseStreams[phase].inject(0) { total, s ->
            total + invocations(phase, s)
        } > 0
    }

    def add(int phase, Stream s, int invocations) {
        if(phaseStreams[phase].contains(s))
            throw new RuntimeException("$s already added to phase $phase")

        phaseStreams[phase] << s
        phaseInvocations[phase][s] = invocations
    }
    def addInit(Stream s, int invocations) { add(0, s, invocations) }
    def addSteady(Stream s, int invocations) { add(1, s, invocations) }

    def set(int phase, Stream s, int invocations) {
        phaseInvocations[phase][s] = invocations
    }
    def setInit(Stream s, int invocations) { set(0, s, invocations) }
    def setSteady(Stream s, int invocations) { set(1, s, invocations) }

    Stream[] streams(int phase) { return phaseStreams[phase] as Stream[] }
    Stream[] initStreams() { return streams(0) }
    Stream[] steadyStreams() { return streams(1) }

    @Override
    int invocations(int phase, int idx) { return phaseInvocations[phase][phaseStreams[idx]] }
    @Override
    int invocations(int phase, Stream s) { return phaseInvocations[phase][s] }
    int initInvocations(Stream s) { return invocations(0, s) }
    int steadyInvocations(Stream s) { return invocations(1, s) }

    @Override
    int bufferSize(Channel ch) {
        return bufferSize(ch.src(), ch.sink())
    }

    @Override
    int bufferSize(Stream src, Stream sink) {
        if(!src.isLinked(sink)) return 0
        src = src.last()
        sink = sink.first()
        //println("${src.name()}->${sink.name()}")
        int size = initInvocations(src) * src.getPushRate(sink)
        return Math.max(size, size - initInvocations(sink) * sink.getPopRate(src) + steadyInvocations(src) * src.getPushRate(sink))
    }

    @Override
    int bufferSizeInBytes(Stream src, Stream sink) {
        int bytes
        switch(src.getOutputType(sink)) {
            case TypePrimitive.voidtype:
                bytes = 0; break
            case TypePrimitive.chartype:
                bytes = 1; break
            case TypePrimitive.inttype:
            case TypePrimitive.floattype:
                bytes = 4; break
            default:
                throw new RuntimeException("Unsupported type " + src.getOutputType(sink))
        }
        return bufferSize(src, sink) * bytes
    }

    @Override
    int bufferSizeInBytes(Channel ch) {
        return bufferSizeInBytes(ch.src(), ch.sink())
    }

    @Override
    int sizeInBytes() {
        int bytes = 0
        steadyStreams().findAll { it.isFilter() || it.isSplitter() || it.isJoiner() }.each {
            Stream src = it
            it.getOutputs().each {
                Stream sink = it.first()
                bytes += bufferSizeInBytes(src, sink)
            }
        }
        return bytes
    }

    @Override
    def each(Closure closure) {
        steadyStreams().each {
            closure(it, initInvocations(it), steadyInvocations(it))
        }
    }

    @Override
    def each(int phase, Closure closure) {
        streams(phase).each {
            closure(it, invocations(phase, it))
        }
    }

    @Override
    Schedule scale(int factor) {
        phaseInvocations[STEADY] = phaseInvocations[STEADY].collectEntries { s, i -> [s, i * factor] }
        return this
    }

    @Override
    int compareTo(Schedule o) {
        return Integer.compare(sizeInBytes(), o.sizeInBytes())
    }

    public String info(boolean withBuffer = false ) {
        StringBuilder builder = new StringBuilder()
        builder << "========== ${stream.name()} Schedule ==========\n"
        (0..1).each { int phase ->
            def init = "(${scheduler.getPeekRate(0, stream)}, ${scheduler.getPopRate(0, stream)}, ${scheduler.getPushRate(0, stream)})"
            def steady = "(${scheduler.getPeekRate(1, stream)}, ${scheduler.getPopRate(1, stream)}, ${scheduler.getPushRate(1, stream)})"
            def rates = phase == 0 ? init : steady
            builder << (phase == 0 ? "INIT: " : "Steady: ") << rates << " ["
            builder << streams(phase).collect {
                "${it.name()}:${invocations(phase, it)}"
            }.join(", ")
            builder << "]\n"
        }

        if(withBuffer) {
            // Per channel buffer size
            builder << "\n"
            def channels = []
            steadyStreams().findAll { it.isFilter() || it.isSplitter() || it.isJoiner() }.each {
                Stream src = it
                it.getOutputs().each {
                    Stream sink = it.first()
                    int size = bufferSize(src, sink)
                    channels << "${src.name()}->${sink.name()}: $size"
                }
            }
            builder << "Channel buffer allocations:\n" << channels.join("\n")
        }
        builder << "===== End of ${stream.name()} Schedule ====="
        return builder.toString()
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder().append("[SAS] ");
        builder << steadyStreams().join(",")
        return builder.toString();
    }
}
