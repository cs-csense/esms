package esms.toolkits;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Scanner;

import esms.util.CommandLine;
import esms.util.Log;
import esms.data.Measure;
import esms.data.Performance;
import esms.usage.RUsage;
import esms.util.SSH;
import esms.util.Shell;
import esms.util.StageTimer;

/**
 * Created by farleylai on 12/13/14.
 */
public class StreamItToolkit4j extends Toolkit {
    private static final String OUTPUT = "streamit";
    private static final String SRC = "../StreamIt/src";
    private static final String STR = Shell.path("src/test/streamit");
    private static final String CLUSTER_INC = Shell.path(SRC, "cluster/headers");
    private static final String CLUSTER_LIB = "../StreamIt/build/binaries/clusterStaticLibrary/osx_x86_64";
    private static final String CONCAT_CLUSTER_THREADS = Shell.path(SRC, "main/misc/concat_cluster_threads_cpp.pl");
    private static final String TOJAVA = "streamit.frontend.ToJava";
    private static final String KJC = "at.dms.kjc.Main";
    private int mIterations;
    private int mScaling;
    private boolean mPrintf;


    //StreamIt/cacheopt/FFT3/128
    //StreamIt/linearpartition/FFT3/128
    public StreamItToolkit4j(String build, String target, int iterations, int scaling, boolean printf) {
        super("StreamIt", STR + File.separator + target, build, target);
        String[] filenames = srcDir().list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith("str");
            }
        });
        src(target("str"));
        for(String filename: filenames) {
            if (!filename.startsWith(target()))
                src(filename);
        }
        mIterations = iterations;
        mScaling = scaling;
        mPrintf = printf;
    }

    public StreamItToolkit4j(String build, String target, int iterations, boolean printf) {
        this(build, target, iterations, 1, printf);
    }

    public StreamItToolkit4j(String build, String target, boolean printf) {
        this(build, target, 20160000, 1, printf);
    }

    private StreamItToolkit4j tojava() {
        // java -ea -Xmx1700M  streamit.frontend.ToJava --output FMRadio.java   FMRadio.str
        CommandLine cmd = new CommandLine("java");
        cmd.arg("-cp", "\"" + System.getProperty("java.class.path") + "\"", "-ea", "-Xmx1700M", TOJAVA, "--output", target("java"));
        cmd.arg(src());
        if(Shell.run(pwd(), cmd) != Shell.EXIT_SUCCESS)
            fail("Failed to run " + cmd);
        return this;
    }

    private StreamItToolkit4j kjc() {
        // java -ea -Xmx1700M  at.dms.kjc.Main --streamit --standalone --cluster 1 --havefftw FMRadio.java
        CommandLine cmd = new CommandLine("java");
        cmd.arg("-cp", "\"" + System.getProperty("java.class.path") + "\"", "-ea", "-Xmx1700M", KJC, "--streamit", "--i", String.valueOf(mIterations), "--standalone", "--cluster", "1");
        if(mScaling > 1)
            cmd.arg("--cacheopt");
        cmd.arg(target("java"));
        if(Shell.run(pwd(), cmd) != Shell.EXIT_SUCCESS) {
            fail("Failed to run " + cmd);
        }
        return this;
    }

    private StreamItToolkit4j make() throws Exception {
        // c++ -O3 -I/Users/farleylai/Documents/dev/git/StreamIt/master/library/cluster -c -o combined_threads.o combined_threads.cpp
        // c++ -O3 -o a.out combined_threads.o -L/Users/farleylai/Documents/dev/git/StreamIt/master/library/cluster -lpthread -lcluster -lstdc++
        CommandLine cmd = new CommandLine(Shell.path(CONCAT_CLUSTER_THREADS), " fusion.cpp", " thread*.cpp");
        if(Shell.run(pwd(), cmd) != Shell.EXIT_SUCCESS)
            fail("Failed to run " + cmd);
        if (Shell.run(pwd(), new CommandLine("grep -v -e timer.h -e proc_timer -e start -e stop -e output combined_threads.cpp"), pwd("tmp")) != Shell.EXIT_SUCCESS)
            fail("Failed to filter out StreamIt source combined_threads.cpp");
        if (Shell.run(pwd(), new CommandLine("mv tmp combined_threads.cpp")) != Shell.EXIT_SUCCESS)
            fail("Failed to mv back to StreamIt source combined_threads.cpp");
        ToolChain cpp = new ToolChain("c++", pwd().getPath(), pwd().getPath(), OUTPUT);
        cpp.printf(mPrintf);
        cpp.cflag("-O3");
        cpp.include(CLUSTER_INC);
        cpp.link("cluster", CLUSTER_LIB);
        cpp.link("pthread");
        cpp.link("stdc++");
        cpp.src("combined_threads.cpp");
        cpp.build();
        if(Shell.run(pwd(), new CommandLine("strip", OUTPUT)) != Shell.EXIT_SUCCESS)
            fail("Failed to strip " + OUTPUT);
        if(Shell.run(pwd(), new CommandLine("dot", "-Tpdf", "-o", "stream-graph.pdf", "stream-graph.dot")) != Shell.EXIT_SUCCESS)
            fail("Failed to generate SFG from dot");
        return this;
    }

    private long pow2ceil(int A) {
        return ((A<=2)?(2): (((A<=4)?(4): (((A<=8)?(8): (((A<=16)?(16): (((A<=32)?(32): (((A<=64)?(64): (((A<=128)?(128): (((A<=256)?(256):(((A<=1024)?(1024):(((A<=4096)?(4096):(((A<=16384)?(16384):(((A<=65536)?(65536):(((A<=131072)?(131072):(((A<=262144)?(262144):(((A<=524288)?(524288):(((A<=1048576)?(1048576):(((A<=2097152)?(2097152):(((A<=4194304)?(4194304):(((A<=8388608)?(8388608):(((A<=16777216)?(16777216):(((A<=33554432)?(33554432):(((A<=67108864)?(67108864):(((A<=134217728)?(134217728):(((A<=268435456)?(268435456):(((A<=536870912)?(536870912):(1073741824))))))))))))))))))))))))))))))))))))))))))))))))));
    }

    private long readSources() {
        String[] sources = new String[] {
                "combined_threads.cpp",
                "fusion.h",
                "structs.h"
        };

        long sum = 0;
        for(String src: sources)
            sum += pwd(src).length();
        return sum;
    }

    private long readBufferAllocations() throws IOException {
        long size = 0;
        RandomAccessFile fusion = new RandomAccessFile(pwd("fusion.h"), "r");
        String line;
        while((line = fusion.readLine()) != null) {
            if(!line.startsWith("#define __BUF_SIZE")) continue;
            int pow2ceil = line.indexOf("pow2ceil");
            int plus = line.indexOf("+");
            int minus = line.indexOf("-");
            int mult = line.indexOf("__MULT");
            if(pow2ceil < 0 || plus < 0 || minus < 0) continue;
            int sz = mult < 0
                    ? Integer.parseInt(line.substring(pow2ceil + "pow2ceil(".length(), plus))
                    : Integer.parseInt(line.substring(pow2ceil + "pow2ceil(".length(), mult-1)) * mScaling;
            sz += Integer.parseInt(line.substring(plus + 1, minus - 1));
            size += pow2ceil(sz);
            if(mScaling > 1)
                Log.d("Counted scaled buffer size: %d\n", size);
        }
        fusion.close();
        return size * 4;
    }

    @Override
    public StreamItToolkit4j export(File dir) {
        if(!dir.exists()) dir.mkdirs();
        String path = dir.getAbsolutePath();
        String filename = "combined_threads" + (mScaling > 1 ? "-scaling" : "") + ".cpp";
        Shell.cp(pwd("cluster.h").getAbsolutePath(), path);
        Shell.cp(pwd("fusion.h").getAbsolutePath(), path);
        Shell.cp(pwd("structs.h").getAbsolutePath(), path);
        Shell.cp(pwd("global.h").getAbsolutePath(), path);
        Shell.cp(pwd("combined_threads.cpp").getAbsolutePath(), Shell.path(dir, filename));
        return this;
    }

    @Override
    public StreamItToolkit4j export(SSH ssh, File dir) {
        ssh.run(Shell.cmd("mkdir", "-p", dir.getPath()));
        String path = dir.getPath();
        String filename = "combined_threads" + (mScaling > 1 ? "-scaling" : "") + ".cpp";
        ssh.scp(pwd("cluster-setup.txt").getAbsolutePath(), path);
        ssh.scp(pwd("cluster.h").getAbsolutePath(), path);
        ssh.scp(pwd("fusion.h").getAbsolutePath(), path);
        ssh.scp(pwd("structs.h").getAbsolutePath(), path);
        ssh.scp(pwd("global.h").getAbsolutePath(), path);
        ssh.scp(pwd("combined_threads.cpp").getAbsolutePath(), new File(dir, filename).getPath());
        return this;
    }

    @Override
    public File executable() { return pwd(OUTPUT); }

    @Override
    protected StageTimer doBuild() throws Exception {
        StageTimer timer = new StageTimer("StreamIt Compilation");
        timer.log("tojava");
        tojava();
        timer.log();
        timer.log("kjc");
        kjc();
        timer.log();
        timer.log("toolchain");
        make();
        timer.log();
        for(String src: src())
            Shell.ln(src, pwd().getAbsolutePath());
        return timer;
    }

    @Override
    public Performance run() throws IOException {
        RUsage rusage = Shell.time(pwd(), Shell.pwd(OUTPUT));
        int writes = -1;
        long allocation = readBufferAllocations();
        long source = readSources();
        long program = new File(pwd(), OUTPUT).length();
        Performance performance = new Performance();
        performance.put(rusage);
        performance.put("writes", writes, Measure.UNIT.bytes);
        performance.put("allocation", allocation, Measure.UNIT.bytes);
        performance.put("source", source, Measure.UNIT.bytes);
        performance.put("program", program, Measure.UNIT.bytes);

        // size of __TEXT and __DATA segments
        String prog = executable().getName();
        CommandLine size = new CommandLine("size", Shell.pwd(prog));
        List<String> lines = Shell.read(pwd(), size);
        Scanner sc = new Scanner(lines.get(1));
        performance.put("__TEXT", sc.nextLong(), Measure.UNIT.bytes);
        performance.put("__DATA", (Shell.os() == Shell.OS.MacOS) ? sc.nextLong() : (sc.nextLong() + sc.nextLong()), Measure.UNIT.bytes);
        return performance;
    }

    @Override
    public String summary() {
        return String.format("%d %d", mIterations, mScaling);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString());
        builder.append("Iterations: ").append(mIterations).append("\n");
        builder.append("Scaling: ").append(mScaling).append("\n");
        builder.append("toString: ").append(summary());
        return builder.toString();
    }
}
