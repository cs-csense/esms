package esms.toolkits

import esms.api.Schedule
import esms.api.Scheduler
import esms.api.Profile
import esms.api.Configuration
import esms.api.SFG
import esms.backend.c.ESMSCodeGenerator
import esms.backend.java.JavaCodeGenerator
import esms.core.Simulation
import esms.api.Simulator
import esms.data.Measure
import esms.data.Performance
import esms.optimization.DefaultOptimizer
import esms.optimization.ESMSOptimizer
import esms.optimization.Optimizer
import esms.scheduling.PhasedScheduler
import esms.streamit.Stream
import esms.streamit.StreamItAdapter
import esms.types.Strategy
import esms.usage.RUsage
import esms.util.*
/**
 * Created by farleylai on 12/13/14.
 */
public class ESMSToolkit extends Toolkit {
    private static final String TAG = ESMSToolkit.class.simpleName
    private static final String OUTPUT = "esms"
    private Configuration mConfig
    public Profile profile

    public ESMSToolkit(String build, String benchmark, int iterations, int scaling, String pkg) {
        this(build, benchmark, iterations, scaling, false, Strategy.APPEND_ON_CONFLICT, 32);
        profile.pkg = pkg;
    }

    public ESMSToolkit(String build, String benchmark, int iterations, int scaling, boolean printf, Strategy strategy, int groups) {
        super("ESMS", "src/test/streamit/" + benchmark, build, benchmark);
        String[] filenames = srcDir().list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
//                return name.endsWith("str");
                return true
            }
        });
        src(target("str"));
        for(String filename: filenames) {
            if (filename.endsWith('str')) {
                if (!filename.startsWith(target()))
                    src(filename);
            } else
                res(filename);
        }

        println res()

        profile = new Profile()
        profile.iterations = iterations
        profile.scaling = scaling
        profile.printf = printf
        profile.esms.strategy = strategy
        profile.esms.maxWindowGroups = groups
    }

    public ESMSToolkit(Profile profile) {
        super('ESMS', profile.srcDir, profile.outputDir, profile.benchmark)
        String[] filenames = srcDir().list()
        src(target("str"));
        for(String filename: filenames) {
            if (filename.endsWith('str')) {
                if (!filename.startsWith(target()))
                    src(filename);
            } else
                res(filename);
        }
        this.profile = profile
    }

    public ESMSToolkit(String build, String target, int iterations, boolean printf) {
        this(build, target, iterations, 1, printf, Strategy.IN_PLACE, 32);
    }

    public int countStreamIt() {
        return mConfig == null ? -1 : mConfig.countStreamIt();
    }

    public int allocationStreamIt() {
        return mConfig == null ? -1 : mConfig.allocationStreamIt();
    }

    public Strategy strategy() {
        return profile.strategy
    }

    @Override
    public ESMSToolkit export(File dir) {
        if(!dir.exists()) dir.mkdirs();
        String filename = target() + (profile.scaling > 1 ? "-scaling" : "") + ".cpp";
        Shell.cp(pwd(target("c")).getAbsolutePath(), Shell.path(dir, filename));
        return this;
    }

    @Override
    public ESMSToolkit export(SSH ssh, File dir) {
        String filename = target() + (profile.scaling > 1 ? "-scaling" : "") + ".cpp";
        ssh.run(Shell.cmd("mkdir", "-p", dir.getPath()));
        ssh.scp(pwd(target("c")).getAbsolutePath(), new File(dir, filename).getPath());
        return this;
    }

    @Override
    public File executable() {
        return pwd(OUTPUT);
    }

    @Override
    protected StageTimer doBuild() throws Exception {
        // Construct SFG from StreamIt programs
        StageTimer timer = new StageTimer("ESMS Compilation");
        timer.log("SFG construction from StreamIt program");
        StreamItAdapter streamit = new StreamItAdapter(src());
        SFG sfg = streamit.read();
        timer.log()

        // Save SFG
        sfg.save(Shell.path(pwd(), target()), true);
        Log.d(TAG, sfg);

        // Static schedule derivation
        timer.log("Static schedule derivation");
        Scheduler scheduler = new PhasedScheduler(streamit.top())
        Schedule schedule = scheduler.schedule()
        println(schedule.info())
        timer.log()

        // Partitioning by hand, indexed from 0
        timer.log("SFG Partitioning");
        if(target() == 'ProducerConsumer') {
            profile.partitions = [
                    0: [sfg.getComponent(0)],
                    1: [sfg.getComponent(1)]
            ]
        //} else if(target() == 'Crowd2' || target() == 'SpeakerIdentifier2' || target() == 'Batching') {
        } else if(profile.partitioning) {
            profile.partitions = [:].withDefault { [] }
            schedule.each(Schedule.STEADY) { Stream s, int invocations ->
                if(invocations == 0) return
                if(s.isSource()) profile.partitions[0] << s
                else {
                    // Partition on dynamic input
                    def partitions = []
                    boolean inputDynamic = s.isInputRateDynamic()
                    s.getInputs().each {
                        if(inputDynamic) return
                        Stream prev = it.last()
                        inputDynamic = prev.isOutputRateDynamic()
                        profile.partitions.each { k, v ->
                            if(prev in v && !(k in partitions)) partitions << k
                        }
                    }
                    if(inputDynamic || partitions.size() > 1 || s.basename().endsWith('TCPSink') || s.basename() == 'FileSink') {
                        // add to a new partition
                        profile.partitions[profile.partitions.keySet().max() + 1] << s
                    } else {
                        // append to the prev partition
                        profile.partitions[partitions[0]] << s
                    }
                }
            }
        } else {
            profile.partitions = [
                    0: sfg.components()
            ]
        }
        sfg.setPartitions(profile.partitions);
        Log.i(TAG, "SFG partitions: ${profile.partitions}")
        timer.log()

        // Optimization
        timer.log("Optimizing by simulation");
        profile.optimizer = new DefaultOptimizer();
//        profile.optimizer = new ESMSOptimizer()
        Optimizer.opt(profile.esms.strategy);
        Optimizer.maxWindowGroups(profile.esms.maxWindowGroups);
        Simulator simulator = new Simulator(profile)
        Simulation simulation = simulator.simulate(sfg, schedule)
        timer.log();

        // save final layout or not
        simulation.save(Shell.path(pwd(), target()), false);
        Log.i(TAG, simulation.info());

        // Code generation
        timer.log("Code generation");
        profile.build = pwd()
        profile.target = target()
        if(profile.optimizer instanceof DefaultOptimizer) {
//            profile.backend = new DefaultCodeGenerator(streamit, simulation.evaluate().get(0));
            profile.backend = new JavaCodeGenerator(streamit, simulation.evaluate().get(0))
            if(profile.android) profile.pkg = 'esms.msa2'
        } else if(profile.optimizer instanceof ESMSOptimizer)
            profile.backend = new ESMSCodeGenerator(streamit, simulation.evaluate().get(0));
        profile.backend.compile(profile);
        timer.log();

        // Backend toolchain compilation
        timer.log("Toolchain");
        if(profile.backend instanceof JavaCodeGenerator) {
            // FIXME delegate java build to gradle tasks
//            if(profile.platform  != 'android') {
//                JavaToolChain javac = new JavaToolChain(pwd().getPath(), pwd().getPath(), OUTPUT);
//                javac.printf(profile.printf as boolean)
//                javac.src(pwd("${profile.pkg.replace('.', File.separator)}").absolutePath, '*.java')
//                javac.build()
//            }
            timer.log();
        } else {
            ToolChain cc = new ToolChain("cc", pwd().getPath(), pwd().getPath(), OUTPUT);
            cc.printf(profile.printf);
            cc.cflag("-O3").cflag("-std=gnu99").cflag("-pthread");
            cc.link("m");
            cc.src(target("c"));
            cc.build();
            timer.log();

            // Strip
            if (Shell.run(pwd(), new CommandLine("strip", OUTPUT)) != Shell.EXIT_SUCCESS)
                fail("Failed to strip csense");
        }

        // DOT
        if(Shell.run(pwd(), new CommandLine("dot -Tpdf", "-o", target("pdf"), target("dot"))) != Shell.EXIT_SUCCESS)
            fail("Failed to generate SFG from dot");

        mConfig = simulation.evaluate().get(0);
        for(String source: src())
            Shell.ln(source, pwd().getAbsolutePath());
        return timer;
    }

    @Override
    public Performance run() throws IOException {
        String cmd = profile.backend instanceof JavaCodeGenerator ? "java " + OUTPUT : Shell.pwd(OUTPUT);
        RUsage rusage = Shell.time(pwd(), cmd);
        long writes = mConfig.count();
        long allocation = mConfig.allocation();
        long source = new File(pwd(), target("c")).length();
        long program = new File(pwd(), OUTPUT).length();
        Performance performance = new Performance();
        performance.put(rusage);
        performance.put("writes", writes, Measure.UNIT.bytes);
        performance.put("allocation", allocation, Measure.UNIT.bytes);
        performance.put("source", source, Measure.UNIT.bytes);
        performance.put("program", program, Measure.UNIT.bytes);

        // size of __TEXT and __DATA segments
        String prog = executable().getName();
        CommandLine size = new CommandLine("size", Shell.pwd(prog));
        List<String> lines = Shell.read(pwd(), size);
        Scanner sc = new Scanner(lines.get(1));
        performance.put("__TEXT", sc.nextLong(), Measure.UNIT.bytes);
        performance.put("__DATA", (Shell.os() == Shell.OS.MacOS) ? sc.nextLong() : (sc.nextLong() + sc.nextLong()), Measure.UNIT.bytes);
        return performance;
    }

    @Override
    public String summary() {
        return String.format("%d %d %d %s", profile.iterations, profile.scaling, profile.esms.maxWindowGroups, profile.esms.strategy);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString());
        builder.append("Iterations: ").append(profile.iterations).append("\n");
        builder.append("Scaling: ").append(profile.scaling).append("\n");
        builder.append("Optimizaer: ").append((profile.optimizer.getClass().getSimpleName())).append("\n")
        if(profile.optimizer instanceof ESMSOptimizer) {
            builder.append("Strategy: ").append(profile.esms.strategy).append("\n");
            builder.append("Max Window Groups: ").append(profile.esms.maxWindowGroups).append("\n");
        }
        builder.append("Backend: ").append((profile.backend.getClass().getSimpleName())).append("\n")
        builder.append("toString: ").append(summary());
        return builder.toString();
    }
}
