package esms.api;

import java.util.ArrayList;
import java.util.List;

import esms.core.*;

/**
 * Created by farleylai on 2/15/14.
 */
public class Channel {
    private final Component mSrc;
    private final Component mSink;
    public Channel(Component src, Component sink) {
        mSrc = src;
        mSink = sink;
    }

    public Component src() {
        return mSrc;
    }

    public Component sink() {
        return mSink;
    }

    public boolean isSource(Component src) {
        return mSrc == src;
    }

    public boolean isSink(Component sink) {
        return mSink == sink;
    }

    public boolean isIncident(Component c) {
        return mSrc == c || mSink == c;
    }

    public static class State {
        public int PUSH;
        public int PEEK;
        public int POP;
        private int mPushIndex;
        private int mDisplacement;
        private boolean mPushLinear;
        private boolean mPeekLinear;

        // direct addressing
        private Window mRemainder;
        private List<Window> mSrcWindows;
        private List<Window> mSinkWindows;
        private List<List<Window>> mSrcWindowGroups;
        private List<List<Window>> mSinkWindowGroups;

        // indirect addressing
        private boolean mPushIndirect;
        private boolean mPopIndirect;

        public State() {
            mSrcWindows = new ArrayList<>();
            mSinkWindows = new ArrayList<>();
            mSrcWindowGroups = new ArrayList<>();
            mSinkWindowGroups = new ArrayList<>();
        }

        public State(State s) {
            this();
            set(s.PUSH, s.PEEK, s.POP);
            mPushLinear = s.mPushLinear;
            mPeekLinear = s.mPeekLinear;
            mRemainder = s.mRemainder == null ? null : new Window(s.mRemainder, true);
            s.getSourceWindows().forEach(w -> mSrcWindows.add(new Window(w, true)));
            s.getSinkWindows().forEach(w -> mSinkWindows.add(new Window(w, true)));
        }

        public boolean hasRemaining() {
            return mRemainder != null;
        }

        public Window remainder() {
            return mRemainder;
        }

        public int displacement() { return mDisplacement; }

        public State displacement(int displacement) {
            mDisplacement = displacement;
            return this;
        }

        /**
         * Load the saved remainder to the 1st sink windows at the shifted position of shared layout.
         *
         * @param displacement the shifted position of shared layout
         * @return
         */
        public Window load(int displacement) {
            if(!mSinkWindows.isEmpty())
                throw new RuntimeException("failed to load remainder to non-empty sink windows");
            mRemainder.stream().filter(r -> r.index() > 0).forEach(r -> r.index(-r.index()));
            mRemainder.shift(displacement);
            mSinkWindows.add(mRemainder);
            mRemainder = null;
            return mSinkWindows.get(0);
        }

        public State save(Window remainder) {
            mSinkWindows.add(mRemainder = remainder.merge());
            return this;
        }

        public boolean isSinkLinear() {
            int prev = -1;
            int offset = -1;
            boolean linear = true;
            for(int i = 0; i < sinkInvocations() && linear; i++) {
                Window sink = getSinkWindow(i);
                linear = sink.isLinear();
                if(i > 0) {
                    offset = sink.offset();
                    linear = sink.isLinear() && offset == prev + POP;
                }
                prev = sink.offset();
            }
            return linear;
        }

        public boolean isPushIndirect() {
            return mPushIndirect;
        }
        public boolean isPopIndirect() {
            return mPopIndirect;
        }

        public boolean isSourceGrouped() {
            return !mSrcWindowGroups.isEmpty();
        }

        public boolean isSinkGrouped() {
            return !mSinkWindowGroups.isEmpty();
        }

        private static List<List<Window>> getWindowGroups(List<Window> windows) {
            List<List<Window>> groups = new ArrayList<>();
            if(windows.isEmpty())
                return groups;
            else if(windows.size() == 1)
                groups.add(new ArrayList<>(windows));
            else {
                int prevOffset = -1;
                Window prev = windows.get(0);
                List<Window> group = new ArrayList<>();
                group.add(prev);
                groups.add(group);
                for(int i = 1; i < windows.size(); i++) {
                    boolean regular = true;
                    Window cur = windows.get(i);
                    int offset = cur.offset() - prev.offset();
                    if(prevOffset == -1) prevOffset = offset;
                    if(offset == prevOffset) {
                        // check if range offset can be regularly updated
                        int size = cur.size();
                        for(int pos = 0; pos < size; pos++) {
                            if(cur.offsetAt(pos) != prev.offsetAt(pos) + offset) {
                                regular = false;
                                break;
                            }
                        }
                    } else
                        regular = false;

                    // new group if not regular
                    if(regular) {
                        prevOffset = offset;
                    } else {
                        groups.add(new ArrayList<Window>());
                        prevOffset = -1;
                    }
                    groups.get(groups.size() - 1).add(cur);
                    prev = cur;
                }
            }
            return groups;
        }

        public List<List<Window>> getSourceWindowGroups() {
            if(mSrcWindowGroups.isEmpty() && !mSrcWindows.isEmpty())
                mSrcWindowGroups.addAll(getWindowGroups(mSrcWindows));
            return mSrcWindowGroups;
        }

        public List<List<Window>> getSinkWindowGroups() {
            if(mSinkWindowGroups.isEmpty() && !mSinkWindows.isEmpty())
                mSinkWindowGroups.addAll(getWindowGroups(hasRemaining() ? mSinkWindows.subList(0, mSinkWindows.size() - 1) : mSinkWindows));
            return mSinkWindowGroups;
        }

        public List<List<Window>> getSourceIndirectWindowGroups(Component c) {
            if(!mPushIndirect) {
                mPushIndirect = true;
                List<Window> group = new ArrayList<>(mSrcWindows.size());
                for (int i = 0; i < mSrcWindows.size(); i++) {
                    Window w = new Window();
                    w.add(new Range(c, i * PUSH, PUSH));
                    group.add(w);
                }
                mSrcWindowGroups.clear();
                mSrcWindowGroups.add(group);
            }
            return mSrcWindowGroups;
        }

        public List<List<Window>> getSinkIndirectWindowGroups(Component c) {
            if(!mPopIndirect) {
                mPopIndirect = true;
                int size = hasRemaining() ? mSinkWindows.size() - 1 : mSinkWindows.size();
                List<Window> group = new ArrayList<>(size);
                for (int i = 0; i < size; i++) {
                    Window w = new Window();
                    w.add(new Range(c, i * POP, PEEK));
                    group.add(w);
                }
                mSinkWindowGroups.clear();
                mSinkWindowGroups.add(group);
            }
            return mSinkWindowGroups;
        }

        public Window getSourceWindow(int g, int i) {
            if(mSrcWindowGroups.isEmpty()) return null;
            List<Window> group = g < mSrcWindowGroups.size() ? mSrcWindowGroups.get(g) : null;
            return group != null && i < group.size() ? group.get(i) : null;
        }

        public Window getSinkWindow(int g, int i) {
            if(mSinkWindowGroups.isEmpty()) return null;
            List<Window> group = 0 <= g && g < mSinkWindowGroups.size() ? mSinkWindowGroups.get(g) : null;
            return 0 <= i && i < group.size() ? group.get(i) : null;
        }

        public List<Window> getSourceWindows() {
            return mSrcWindows;
        }

        public List<Window> getSinkWindows() {
            return mSinkWindows;
        }

        public Window getSourceWindow(int i) {
            return getSourceWindow(i, false);
        }

        public Window getSourceWindow(int i, boolean allocated) {
            if(i < mSrcWindows.size())
                return mSrcWindows.get(i);
            else if(allocated) {
                Window src = new Window();
                push(src);
                return src;
            }
            return null;
        }

        public Window getSinkWindow(int i) {
            return i < mSinkWindows.size() ? mSinkWindows.get(i) : null;
        }

        public Window getSinkWindow(int i, boolean allocated) {
            if(i < mSinkWindows.size())
                return mSinkWindows.get(i);
            else if(allocated) {
                Window sink = new Window();
                mSinkWindows.add(sink);
                return sink;
            }
            return null;
        }

        public State set(int push, int peek, int pop) {
            PUSH = push;
            PEEK = peek;
            POP = pop;
            return this;
        }

        public State setPushLinear() {
            mPushLinear = true;
            return this;
        }

        public State setPeekLinear() {
            mPeekLinear = true;
            return this;
        }

        /**
         * Remainder is not cleared to pass from INIT to STEADY.
         *
         * @return
         */
        public State clear() {
            mSrcWindows.clear();
            mSinkWindows.clear();
            return this;
        }

        public State clearSourceWindows() {
            mSrcWindows.clear();
            return this;
        }

        public State clearRemainder() {
            mRemainder = null;
            return this;
        }

        public State resetPushIndex() {
            return resetPushIndex(0);
        }

        public State resetPushIndex(int index) {
            mPushIndex = index;
            return this;
        }

        public State push(Window w) {
            mSrcWindows.add(w);
            return this;
        }

        public int sourceInvocations() {
            return mSrcWindows.size();
        }

        public int sinkInvocations() {
            // after windowing, the remainder is appended to the last sink window
            return Math.max(0, mSinkWindows.size() + (hasRemaining() ? -1 : 0));
        }

        public int nextPushIndex(int advance) {
            int index = mPushIndex;
            mPushIndex += advance;
            return index;
        }

        public boolean isEmpty() {
            return isEmpty(false);
        }

        public boolean isEmpty(boolean src) {
            return src ? mSrcWindows.isEmpty() : mSinkWindows.isEmpty();
        }

        public int offset() {
            return offset(false);
        }

        public int offset(boolean src) {
            return src
                    ? mSrcWindows.stream().mapToInt(w -> w.offset()).min().orElse(0)
                    : mSinkWindows.stream().mapToInt(w -> w.offset()).min().orElse(0);
        }

        public int limit() {
            return limit(false);
        }

        /**
         * Sink limit includes remainder.
         *
         * @param src
         * @return
         */
        public int limit(boolean src) {
            return src
                    ? mSrcWindows.stream().mapToInt(w -> w.limit()).max().orElse(0)
                    : mSinkWindows.stream().mapToInt(w -> w.limit()).max().orElse(0);
        }

        public State shift(int offset, boolean source) {
            if(source) mSrcWindows.forEach(w -> w.shift(offset));
            else mSinkWindows.forEach(w -> w.shift(offset));
            return this;
        }

        public State shift(int offset) {
            mSrcWindows.forEach(w -> w.shift(offset));
            mSinkWindows.forEach(w -> w.shift(offset));
            return this;
        }

        public Window layout() {
            return layout(false);
        }

        public Window layout(boolean all) {
            Window window = new Window();
            if(all)
                mSinkWindows.forEach(w -> window.addAll(w));
            else
                mSinkWindows.stream().filter(w -> w != mRemainder).forEach(w -> window.addAll(w));
            return window.layout();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append(PUSH).append(mPushIndirect ? "/I" : "").append(",");
            builder.append(PEEK).append(mPopIndirect ? "/I" : "").append(",");
            builder.append(POP);
            return builder.toString();
        }
    }
}
