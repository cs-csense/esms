package esms.api;

import java.util.HashMap;
import java.util.Map;

public class Component {
    private String mName;

    public Component(String name) {
        mName = name;
    }

    public String name() {
        return mName;
    }

    @Override
    public boolean equals(Object c) {
        if (c == null || !(c instanceof Component)) return false;
        return ((Component) c).name().equals(name());
    }

    @Override
    public String toString() {
        return name();
    }

    public static class State {
        private Map<Channel, Boolean> mDetections;
        private Map<Channel, Boolean> mDecisions;

        public State() {
            mDetections = new HashMap<>();
            mDecisions = new HashMap<>();
        }

        public State(State s) {
            this();
            set(s);
        }

        public State set(State state) {
            mDetections.putAll(state.mDetections);
            mDecisions.putAll(state.mDecisions);
            return this;
        }

        public boolean isDetected(Channel out) {
            return mDetections.getOrDefault(out, Boolean.FALSE);
        }

        public boolean decision(Channel out) {
            return mDecisions.getOrDefault(out, Boolean.FALSE);
        }

        public State setDetected(Channel out) {
            mDetections.put(out, Boolean.TRUE);
            return this;
        }

        public State decide(Channel out, boolean decision) {
            mDecisions.put(out, decision ? Boolean.TRUE : Boolean.FALSE);
            return this;
        }
    }
}
