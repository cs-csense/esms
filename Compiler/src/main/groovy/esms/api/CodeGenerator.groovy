package esms.api

interface CodeGenerator {
    CodeGenerator include();
    CodeGenerator header();
    CodeGenerator main();
    CodeGenerator compile(Profile profile) throws IOException;
}