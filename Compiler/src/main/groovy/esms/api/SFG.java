package esms.api;

import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import esms.core.OP;
import esms.core.Operation;
import esms.core.Range;
import esms.core.Superframe;
import esms.core.Window;
import esms.math.Matrix;
import esms.streamit.Stream;
import esms.util.Formatter;
import esms.util.Log;

/**
 * Created by farleylai on 2/15/14.
 */
public class SFG {
    private List<Superframe> mSuperframes;
    private List<Component> mComponents;
    private List<Channel> mChannels;
    private Map<Component, Component.State> mComponentStates;
    private Map<Channel, Channel.State> mChannelStates;
    private Map<Component, Map<Channel, List<Operation>>> mComponentOperations;
    private Map<Integer, List<Component>> mPartitions;

    public SFG() {
        mComponents = new ArrayList<>();
        mChannels = new ArrayList<>();
        mSuperframes = new ArrayList<>();
        mComponentStates = new HashMap<>();
        mChannelStates = new HashMap<>();
        mComponentOperations = new HashMap<>();
        mPartitions = new TreeMap<>();
    }

    /**
     * Copy constructor that only duplicates states.
     *
     * @param sfg
     */
    public SFG(SFG sfg) {
        this();
        mComponents.addAll(sfg.mComponents);
        mChannels.addAll(sfg.mChannels);
        mComponentOperations.putAll(sfg.mComponentOperations);
        mPartitions.putAll(sfg.mPartitions);
        sfg.mSuperframes.forEach(sf -> mSuperframes.add(new Superframe(sf)));
        sfg.mComponentStates.forEach((c, s) -> mComponentStates.put(c, new Component.State(s)));
        sfg.mChannelStates.forEach((c, s) -> mChannelStates.put(c, new Channel.State(s)));
    }

    public int nodes() {
        return mComponents.size();
    }

    public int edges() {
        return mChannels.size();
    }

    public int inDegrees(String c) {
        return inDegrees(getComponent(c));
    }

    public int inDegrees(Component c) {
        int degrees = 0;
        for(Channel ch: mChannels) {
            if(ch.isSink(c))
                degrees++;
        }
        return degrees;
    }

    public int outDegrees(String c) {
        return outDegrees(getComponent(c));
    }

    public int outDegrees(Component c) {
        int degrees = 0;
        for(Channel ch: mChannels) {
            if(ch.isSource(c))
                degrees++;
        }
        return degrees;
    }

    public boolean contains(String c) {
        return mComponents.contains(getComponent(c));
    }

    public boolean contains(Component c) {
        return mComponents.contains(c);
    }

    public boolean contains(Channel ch) {
        return mChannels.contains(ch);
    }

    public boolean contains(Superframe sf) {
        return mSuperframes.contains(sf);
    }

    public boolean contains(String src, String sink) {
        return contains(getChannel(src, sink));
    }

    public boolean contains(Component src, Component sink) { return contains(getChannel(src, sink)); }

    public boolean isProductive(Component c) {
        return isSink(c) || !mComponentOperations.get(c).values().stream()
                .flatMap(operations -> operations.stream())
                .allMatch(operation -> operation.op() == OP.PASS);
//        return isFilter(c) && ((Filter)c).isProductive();
    }

    public SFG setPartitions(Map<Integer, List<Component>> partitions) {
        mPartitions = partitions;
        return this;
    }

    public Map<Integer, List<Component>> getPartitions() {
        return mPartitions;
    }

    public int getPartition(Component c) {
        for(Map.Entry<Integer, List<Component>> entry: mPartitions.entrySet()) {
            if(entry.getValue().contains(c)) return entry.getKey();
        }
        return -1;
    }

    public boolean isPartitioned() {
        return mPartitions.size() > 1;
    }

    public boolean isPartitioned(Channel channel) {
        return channel == null ? false : getPartition(channel.src()) != getPartition(channel.sink());
    }

    public boolean isFilter(Component c) {
//        return  ((Stream)c).isFilter();
        return inDegrees(c) <= 1 && outDegrees(c) <= 1;
    }

    public boolean isSplitter(Component c) {
        // Single output splitter is viewed as a pass filter
        return ((Stream)c).isSplitter();
//        return inDegrees(c) == 1 && outDegrees(c) > 1 && !isProductive(c);
    }

    public boolean isJoiner(Component c) {
        // Single input joiner is viewed as a pass filter
        return ((Stream)c).isJoiner();
//        return inDegrees(c) > 1 && outDegrees(c) == 1 && !isProductive(c);
    }

    public boolean isSource(Component c) {
        return inDegrees(c) == 0;
    }

    public boolean isSink(Component c) {
        return outDegrees(c) == 0;
    }

    public List<Superframe> superframes() {
        return Collections.unmodifiableList(mSuperframes);
    }

    public List<Component> components() {
        return Collections.unmodifiableList(mComponents);
    }

    public List<Channel> channels() {
        return Collections.unmodifiableList(mChannels);
    }

    public boolean isStreamIt() {
        for (Component c : mComponents) {
            if (!isFilter(c) && !isSplitter(c) && !isJoiner(c))
                return false;
        }
        return true;
    }

    public Component add(Component c) {
        if(!contains(c))
            mComponents.add(c);
        return c;
    }

    public Component add(String name) {
        Component c = getComponent(name);
       if(c != null) {
            c = new Component(name);
            mComponents.add(c);
        }
        return c;
    }

    public SFG add(Superframe sf) {
        if(!contains(sf))
            mSuperframes.add(sf);
        return this;
    }

    public SFG remove(Component c) {
        mComponents.remove(c);
        mComponentStates.remove(c);
        List<Channel> channels = mChannels.stream().filter(ch -> ch.isIncident(c)).collect(Collectors.toList());
        channels.forEach(ch -> {
            mChannels.remove(ch);
            mChannelStates.remove(ch);
            mSuperframes.stream().filter(sf -> sf.contains(ch)).forEach(sf -> sf.remove(ch));
        });
        mComponentOperations.remove(c);
        return this;
    }

    public SFG removeAll(Collection<Superframe> c) {
        mSuperframes.removeAll(c);
        return this;
    }

    public Component getComponent(int idx) {
        return mComponents.get(idx);
    }

    public Component getComponent(String name) {
        Optional<Component> ret = mComponents.stream().filter(c -> c.name().equals(name)).findAny();
        return ret.isPresent() ? ret.get() : null;
    }

    public Channel getChannel(int idx) {
        return mChannels.get(idx);
    }

    public Channel getChannel(String src, String sink) {
        return getChannel(getComponent(src), getComponent(sink));
    }

    public Channel getChannel(Component src, Component sink) {
        Optional<Channel> channel = mChannels.stream().filter(ch -> ch.src() == src && ch.sink() == sink).findFirst();
        return channel.isPresent() ? channel.get() : null;
    }

    public Channel getInputChannel(Component c, Component from) {
        return mChannels.stream().filter(ch -> ch.isSink(c) && ch.isSource(from)).findFirst().orElse(null);
    }

    public List<Channel> getInputChannels(Component c) {
        return mChannels.stream().filter(ch -> ch.isSink(c)).collect(Collectors.toList());
    }

    public Channel getInputChannel(Component c, int idx) {
        List<Channel> inputs = mChannels.stream().filter(ch -> ch.isSink(c)).collect(Collectors.toList());
        return idx >= 0 && idx < inputs.size() ? inputs.get(idx) : null;
    }

    public Channel getOutputChannel(Component c, int idx) {
        List<Channel> outputs = mChannels.stream().filter(ch -> ch.isSource(c)).collect(Collectors.toList());
        return idx >= 0 && idx < outputs.size() ? outputs.get(idx) : null;
    }

    public List<Channel> getOutputChannels(Component c) {
        return mChannels.stream().filter(ch -> ch.isSource(c)).collect(Collectors.toList());
    }

    public Superframe getSuperframe(Channel ch) {
        Optional<Superframe> ret = mSuperframes.stream().filter(sf -> sf.contains(ch)).findFirst();
        return ret.isPresent() ? ret.get() : null;
    }

    public Component.State getState(Component c) {
        if (c == null) return null;
        Component.State s = mComponentStates.getOrDefault(c, new Component.State());
        mComponentStates.put(c, s);
        return s;
    }

    public Channel.State getState(Channel ch) {
        if (ch == null) return null;
        Channel.State s = mChannelStates.getOrDefault(ch, new Channel.State());
        mChannelStates.put(ch, s);
        return s;
    }

    public int indexOf(Component c) {
        return mComponents.indexOf(c);
    }

    public int indexOf(Channel c) {
        return mChannels.indexOf(c);
    }


    public Channel link(String src, String sink) {
        return link(src, sink, 0, 0, 0);
    }

    public Channel link(Component src, Component sink, int push, int peek, int pop) {
        Channel channel = getChannel(src, sink);
        if (!contains(channel)) {
            channel = new Channel(add(src), add(sink));
            mChannels.add(channel);
        }
        if(Math.min(peek, pop) == 0)
            peek = pop = Math.max(peek, pop);
        set(channel, push, peek, pop);
        return channel;
    }

    public Channel link(String src, String sink, int push, int peek, int pop) {
        Channel channel = getChannel(src, sink);
        if (!contains(channel)) {
            channel = new Channel(add(src), add(sink));
            mChannels.add(channel);
        }
        if(peek != pop && Math.min(peek, pop) == 0)
            peek = pop = Math.max(peek, pop);
        set(channel, push, peek, pop);
        return channel;
    }

    public SFG set(Channel channel, int push, int peek, int pop) {
        if (contains(channel)) {
            Channel.State s = mChannelStates.getOrDefault(channel, new Channel.State());
            s.set(push, peek, pop);
            mChannelStates.put(channel, s);
        }
        return this;
    }

    /**
     * Clear channel buffers while retaining remainders.
     *
     * @return
     */
    public SFG clearChannels() {
        mChannels.stream().map(ch -> getState(ch)).forEach(s -> s.clear());
        return this;
    }

    public SFG clearSuperframes() {
        mSuperframes.clear();
        return this;
    }

    // Layout methods
    public Window layout(Channel ch) {
        return layout(ch, false);
    }

    public Window layout(Channel ch, boolean all) {
        return getState(ch).layout(all);
    }

    /**
     * Math.min(route sinks, future sources)
     * @param sf
     * @return
     */
    public int offset(Superframe sf) {
         if(sf.isEmpty()) return 0;
         else {
            int offset = Integer.MAX_VALUE;
            for (Channel ch : sf.route())
                offset = Math.min(getState(ch).offset(), offset);
            for (Channel ch : sf.future()) {
                offset = Math.min(getState(ch).offset(), offset);
                offset = Math.min(getState(ch).offset(true), offset);
            }
            return offset;
        }
//                : sf.route().stream().mapToInt(ch -> layout(ch).offset()).min().getAsInt();
    }

    public int offsetInBytes(Superframe sf) {
        return sf.isEmpty()
                ? 0
                : sf.route().stream().mapToInt(ch -> layout(ch).offsetInBytes()).min().getAsInt();
    }

    /**
     * Math.max(route sinks, future sources)
     * @param sf
     * @return
     */
    public int limit(Superframe sf) {
        if(sf.isEmpty()) return 0;
        else {
            int limit = Integer.MIN_VALUE;
            for (Channel ch : sf.route())
                limit = Math.max(getState(ch).limit(), limit);
            for (Channel ch : sf.future()) {
                limit = Math.max(getState(ch).limit(), limit);
                limit = Math.max(getState(ch).limit(true), limit);
            }
            return limit;
        }
    }

    public int limitInBytes(Superframe sf) {
        return sf.isEmpty()
                ? 0
                : sf.route().stream()
                .mapToInt(ch -> 4 * Math.max(getState(ch).limit(true), getState(ch).limit()))
                .max().getAsInt();
    }

    public int size(Superframe sf) {
        return sf.isEmpty()
                ? 0
                : limit(sf) - offset(sf);
    }

    public int sizeInBytes(Superframe sf) {
        return sf.isEmpty()
                ? 0
                : limitInBytes(sf) - offsetInBytes(sf);
    }

    public int limit() {
        return mSuperframes.isEmpty()
                ? 0
                : mSuperframes.stream().mapToInt(sf -> limit(sf)).max().getAsInt();
    }

    public int size() {
        return mSuperframes.stream().mapToInt(sf -> size(sf)).sum();
    }


    public int sizeInBytes() {
        return mSuperframes.stream().mapToInt(sf -> sizeInBytes(sf)).sum();
    }

    public int remainderInBytes() {
//        return mChannelStates.values().stream()
//                .filter(s -> s.hasRemaining())
//                .mapToInt(s -> s.load(false).layout().count())
//                .sum();
        Map<Component, Window> remainders = new HashMap<>();
        List<Component> originators = new ArrayList<>();
        superframes().stream()
                .flatMap(sf -> sf.route().stream())
                .filter(ch -> getState(ch).hasRemaining())
                .forEach(ch -> {
                    Channel.State s = getState(ch);
                    s.remainder().forEach(r -> {
                        if (!originators.contains(r.originator())) originators.add(r.originator());
                        Window remainder = remainders.getOrDefault(r.originator(), new Window());
                        remainder.add(r);
                        remainders.put(r.originator(), remainder);
                    });
                });
        return originators.stream().mapToInt(originator -> {
            // union of remainders layouts by originators in a reverse order
            Window remainder = remainders.get(originator);
            Collections.reverse(remainder.ranges());
            remainder = remainder.layout();
            remainders.put(originator, remainder);
            return remainder.stream().mapToInt(r -> r.sampleSize() * r.size()).sum();
        }).sum();
    }

    /**
     * Returns the lower bound on buffer allocations by StreamIt. Though the schedule differs
     * a bit, StreamIt usually allocates more with pow2ceil().
     *
     * INIT: PUSH * invocations
     * STEADY: PUSH * invocations + INIT remainder
     *
     * @return
     */
    public int sizeStreamItInBytes(boolean included) {
//        return mSuperframes.stream()
//                .flatMap(sf -> sf.route().stream())
//                .mapToInt(ch -> layout(ch).count()).sum();
        return mChannelStates.values().stream().mapToInt(s -> {
            int sampleSize = s.getSourceWindow(0).rangeAt(0).sampleSize();
            int remainderBytes = 0;
            if (included && s.hasRemaining())
                remainderBytes = s.remainder().count();
            return s.PUSH * s.sourceInvocations() * sampleSize + remainderBytes;
        }).sum();
    }

    public int count(Superframe sf, Component c) {
        int writes = sf.route().stream().mapToInt(ch -> {
            int count = layout(ch).count(c, true);
//            Log.i("count %d writes in sf through %s by %s\n", count, toString(ch), c);
            return count;
        }).max().getAsInt();
        int copies = sf.route().stream().mapToInt(ch -> {
            int count = layout(ch).count(c, false);
//            Log.i("count %d copies in sf through %s by %s\n", count, toString(ch), c);
            return count;
        }).max().getAsInt();
        return writes + copies;
    }

    public int count(Superframe sf) {
        return mComponents.stream().mapToInt(c -> count(sf, c)).sum();
    }

    public int count() {
        return mSuperframes.stream().mapToInt(sf -> count(sf)).sum();
    }

    /**
     * Returns the lower bound on the bytes written by StreamIt, though the schedule differs
     * a bit.
     *
     * INIT: PUSH * invocations
     * STEADY: PUSH * invocations + INIT remainder copy-shift
     *
     * @return
     */
    public int countStreamIt(boolean included) {
        return mChannelStates.values().stream().mapToInt(s -> {
            int sampleSize = s.getSourceWindow(0).rangeAt(0).sampleSize();
            int remainderBytes = 0;
            if(included && s.hasRemaining())
                remainderBytes = s.remainder().count();
            return s.PUSH * s.sourceInvocations() * sampleSize + remainderBytes;
        }).sum();
    }

    public SFG shift(Superframe sf, int offset) {
        sf.route().stream().map(ch -> getState(ch)).forEach(s -> s.shift(offset));
        return this;
    }

    public boolean isTopsorted(List<Component> order) {
        SFG sfg = new SFG(this);
        for (Component c : order) {
            if (sfg.inDegrees(c) > 0)
                return false;
            sfg.remove(c);
        }
        return true;
    }

    private Set<List<Component>> topsort(List<Component> prefix) {
        SFG sfg = new SFG(this);
        Set<List<Component>> sorted = new TreeSet<>((order1, order2) -> order1.toString().compareTo(order2.toString()));
        prefix.forEach(c -> sfg.remove(c));
        if(sfg.components().isEmpty()) {
            sorted.add(prefix);
            return sorted;
        }
        PriorityQueue<Component> components = new PriorityQueue<>(sfg.components().size(),
                (c1, c2) -> {
                    int ret = Long.compare(sfg.inDegrees(c1), sfg.inDegrees(c2));
                    if (ret == 0) {
                        try {
                            int id1 = Integer.parseInt(c1.name());
                            int id2 = Integer.parseInt(c2.name());
                            return Integer.compare(id1, id2);
                        } catch (NumberFormatException e) {
                            return c1.name().compareTo(c2.name());
                        }
                    } else
                        return ret;
                }
        );
        components.addAll(sfg.components());

        List<Component> tie = new ArrayList<>();
        while (!components.isEmpty()) {
            boolean action = false;
            Component head = components.poll();
            if (tie.isEmpty() || sfg.inDegrees(head) == sfg.inDegrees(tie.get(tie.size() - 1))) {
                tie.add(head);
                if (components.isEmpty())
                    action = true;
            } else
                action = true;

            if (action) {
                Set<List<Component>> branches = new HashSet<>(tie.size());
                tie.forEach(c -> {
                    List<Component> branch = new ArrayList<>(prefix);
                    branch.add(c);
                    branches.add(branch);
                });
                branches.forEach(branch -> sorted.addAll(topsort(branch)));
                break;
            }
        }
        return sorted;
    }

    /**
     * Enumerate all the possible topologically sorted schedules.
     * TODO Equivalent sorted schedules should be eliminated in ahead.
     *
     * @param all Enumerate all possible orders or not
     * @return a set of lists of topological orders
     */
    public Set<List<Component>> topsort(boolean all) {
        Set<List<Component>> sorted;
//        Log.i("already topsorted: %s\n", isTopsorted(mComponents));
        if(!all && isTopsorted(mComponents)) {
            sorted = new TreeSet<>((s1, s2) -> s1.toString().compareTo(s2.toString()));
            sorted.add(mComponents);
        } else {
            sorted = topsort(new ArrayList<>());
            if (!all) {
                // just enumerate the first
                int i = 0;
                for (Iterator<List<Component>> itr = sorted.iterator(); itr.hasNext(); ) {
                    itr.next();
                    if (i++ > 0) itr.remove();
                }
            }
        }
        return sorted;
    }

    public Set<List<Component>> topsort() {
        return topsort(false);
    }

    public Matrix getTopologyMatrixPop() {
        Matrix tm = new Matrix(mChannels.size(), mComponents.size());
        for (int i = 0; i < mChannels.size(); i++) {
            Channel channel = mChannels.get(i);
            int src = indexOf(channel.src());
            int sink = indexOf(channel.sink());
            tm.set(i, src, getState(channel).PUSH);
            tm.set(i, sink, -getState(channel).POP);
        }
        return tm;
    }

    public Matrix getTopologyMatrixPeek() {
        Matrix tm = new Matrix(mChannels.size(), mComponents.size());
        for (int i = 0; i < mChannels.size(); i++) {
            Channel channel = mChannels.get(i);
            int src = indexOf(channel.src());
            int sink = indexOf(channel.sink());
            tm.set(i, src, getState(channel).PUSH);
            tm.set(i, sink, -getState(channel).PEEK);
        }
        return tm;
    }

    // component invocation
    public List<Operation> getOperations(Component c) {
        return getOperations(c, null);
    }

    public List<Operation> getOperations(Component c, Channel out) {
        Map<Channel, List<Operation>> map = mComponentOperations.getOrDefault(c, new HashMap<>());
        mComponentOperations.put(c, map);
        List<Operation> operations = map.getOrDefault(out, new ArrayList<>());
        map.put(out, operations);
        return operations;
    }

    public SFG annotate(String c, Channel out, Operation operation) {
        return annotate(getComponent(c), out, operation);
    }

    public SFG annotate(Component c, Channel out, Operation operation) {
        Map<Channel, List<Operation>> map = mComponentOperations.getOrDefault(c, new HashMap<>());
        List<Operation> operations = map.getOrDefault(out, new ArrayList<>());
        operations.add(operation);
        map.put(out, operations);
        mComponentOperations.put(c, map);
        return this;
    }

    public SFG annotate(Component c, Channel out, List<Operation> operations) {
        Map<Channel, List<Operation>> map = mComponentOperations.getOrDefault(c, new HashMap<>());
        List<Operation> operationTable = map.getOrDefault(out, new ArrayList<>());
        operationTable.clear();
        operationTable.addAll(operations);
        map.put(out, operationTable);
        mComponentOperations.put(c, map);
        return this;
    }

    /**
     * Assume intput pop sizeInBytes is equal to sum of its output weights in one invocation.
     * @param c
     * @param weights
     * @return
     */
    public SFG annotateSplitter(Component c, int...weights) {
        Channel in = getInputChannel(c, 0);
        Channel.State s = getState(in);
        Component src = in.src();
        int degrees = outDegrees(c);
        int pos = 0;
        switch(weights.length) {
            case 0:
                // duplicate
                for (int i = 0; i < degrees; i++)
                    annotate(c, getOutputChannel(c, i), Operation.pass(src, 0, s.POP));
                break;
            case 1:
                // round robin
                int w = weights[0];
                for (int i = 0; i < degrees; pos += w, i++)
                    annotate(c, getOutputChannel(c, i), Operation.pass(src, pos, w));
                break;
            default:
                // weighted round robin
                for (int i = 0; i < degrees; pos += weights[i++])
                    annotate(c, getOutputChannel(c, i), Operation.pass(src, pos, weights[i]));
        }
        return this;
    }

    /**
     * Assume input pop sizeInBytes is equal to its weight in one invocation.
     * @param c
     * @param weights
     * @return
     */
    public SFG annotateJoiner(Component c, int...weights) {
        Channel out = getOutputChannel(c, 0);
        Channel.State s = getState(out);
        Component sink = out.sink();
        int degrees = inDegrees(c);
        if(weights.length == 1) {
            // round robin
            int w = weights[0];
            for (int i = 0; i < degrees; i++)
                annotate(c, out, Operation.pass(getInputChannel(c, i).src(), 0, w));
        } else {
            // weighted round robin
            for (int i = 0; i < degrees; i++)
                annotate(c, out, Operation.pass(getInputChannel(c, i).src(), 0, weights[i]));
        }
        return this;
    }

    public SFG annotate(String c, OP op, Object... args) {
        return annotate(getComponent(c), op, args);
    }

    public SFG annotate(Component c, OP op, Object... args) {
        return annotate(c, null, op, args);
    }

    public SFG annotate(String c, Channel out, OP op, Object... args) {
        return annotate(getComponent(c), out, op, args);
    }

    public SFG annotate(Component c, Channel out, OP op, Object... args) {
        Map<Channel, List<Operation>> map = mComponentOperations.getOrDefault(c, new HashMap<>());
        List<Operation> operations = map.getOrDefault(out, new ArrayList<>());
        operations.add(new Operation(op, args));
        map.put(out, operations);
        mComponentOperations.put(c, map);
        return this;
    }

    // Channel state access
    public Window getSourceWindow(Channel ch, int invocation) {
        Channel.State s = getState(ch);
        return s == null ? null : s.getSourceWindow(invocation);
    }

    public Window getSinkWindow(Channel ch, int invocation) {
        Channel.State s = getState(ch);
        return s == null ? null : s.getSinkWindow(invocation);
    }

    // Superframe access
    public boolean isAllocated(Channel out, Channel in) {
        return getSuperframe(out) != getSuperframe(in);
    }

    /**
     * Push to a superframe to an output channel.
     *
     * @param out       the output channel
     * @param in        the input channel
     * @param allocated whether the superframe is allocated or reused
     * @return
     */
    public SFG push(Channel out, Channel in, boolean allocated) {
        if (allocated)
            mSuperframes.add(new Superframe().push(out));
        else
            getSuperframe(in).push(out, in);
        return this;
    }

    public SFG push(Channel out, Window w) {
        if (getSuperframe(out) == null)
            throw new RuntimeException("no superframe allocated for channel " + toString(out));
        getState(out).push(w);
        return this;
    }

    /**
     * Collect R/W references before invocation from
     * input sink windows and future source windows.
     *
     * @param in
     * @return
     */
    private List<Window> references(Channel in) {
        Channel.State sin = getState(in);
        List<Window> references = new ArrayList<>();
        // input sink windows
        Window ws = new Window();
        sin.getSinkWindows().forEach(w -> ws.addAll(w));
        references.add(ws);
        // future source windows not in current output
        getSuperframe(in).future().stream()
                .filter(ch -> ch.src() != in.sink())
                .forEach(ch -> {
                    Window future = new Window();
                    getState(ch).getSourceWindows().forEach(window -> future.addAll(window));
                    references.add(future);
                });
        return references;
    }

    /**
     * Collect references for INSER/UPDATE/DELETE
     * Write references for current and earlier invocations.
     * Read references for later and future invocations.
     *
     * @param out
     * @param in
     * @return
     */
    public List<Window> references(Channel out, Channel in, int invocation, OP op) {
        Channel.State sin = getState(in);
        Channel.State sout = getState(out);
        List<Window> references = new ArrayList<>();
        if (op == OP.CREATE && isAllocated(out, in)) {
            // no conflicts with the newly allocated window for creation
            return references;
        } else {
            // all output source windows
            Window srcs = new Window();
            sout.getSourceWindows().forEach(w -> srcs.addAll(w));
            references.add(srcs);
            // input sink windows from invocation
            Window sinks = new Window();
            sin.getSinkWindows().subList(invocation, sin.getSinkWindows().size()).forEach(w -> sinks.addAll(w));
            references.add(sinks);
            // future source/sink windows
            if (!isAllocated(out, in)) {
                getSuperframe(in).future().stream()
                        .filter(ch -> ch != out)
                        .forEach(ch -> {
                            Window future = new Window();
                            getState(ch).getSourceWindows().forEach(window -> future.addAll(window));
                            getState(ch).getSinkWindows().forEach(window -> future.addAll(window));
                            references.add(future);
                        });
            }
            return references;
        }
    }

    /**
     * Check if the specified offset has a R/W conflict.
     *
     * @param references
     * @param offset
     * @return
     */
    public boolean conflicts(List<Window> references, int offset) {
        if (references.isEmpty())
            return false;

        // ref count is at least one at the offset since offset comes from the window position
        boolean conflicted = references.get(0).refCount(offset) > 0;// with source windows
        if(!conflicted)
            conflicted = references.get(1).refCount(offset) > 1;
        if (!conflicted)
            conflicted = references.subList(2, references.size()).stream().mapToInt(w -> w.refCount(offset)).sum() > 0;
        return conflicted;
    }

    /**
     * TODO Capture conflicts through default in-place invocations since the update positions may change due to insertions.
     * Output source windows are not pushed yet. Check input sink windows instead.
     *
     * @param c
     * @param out
     * @return
     */
    public boolean conflicts(Component c, Channel out) {
        Channel in = getInputChannel(c, 0);
        Channel.State sin = getState(in);
        boolean conflicted = false;
        for (int i = 0; i < sin.sinkInvocations() && !conflicted; i++) {
            List<Operation> operations = getOperations(c, out);
            for (Operation operation : operations) {
                List<Window> references = references(in);
                switch (operation.op()) {
                    case CREATE: {
                        if (references.isEmpty())
                            return false;
                        Window w = sin.getSinkWindow(i);
                        if (operation.size() > w.size())
                            return true;
                        for (int pos = 0; pos < operation.size() && !conflicted; pos++)
                            conflicted = conflicts(references, w.offsetAt(pos));
                        break;
                    }
                    case UPDATE: {
//                        Log.i("check operation %s on %s for conflicts\n", operation, toString(out));
                        Window w = sin.getSinkWindow(i);
                        int limit = operation.pos() + operation.size();
//                        Log.i("test input sink window[%d] for conflicts\n%s", i, w);
                        for (int pos = operation.pos(); pos < limit && !conflicted; pos++) {
                            conflicted = conflicts(references, w.offsetAt(pos));
//                            Log.i("w[%d] %s conflicted\n", pos, conflicted ? "is" : "is not");
                        }
                        break;
                    }
                }
            }
        }
        return conflicted;
    }

//    public SFG validate(Component c, int invocations, Configuration cfg) {
//        // 1. check if output source windows are consistent with operations
//        // 1.1 CREATE: offset and sizeInBytes
//        // 1.2 INSERT/UPDATE: corresponding ranges between input sink windows and outptu source windows
//        // 1.3 DELETE: unavailable in output source windows.
//        Map<Channel, List<Operation>> map = mComponentOperations.getOrDefault(c, new HashMap<>());
//        map.forEach((out, operations) -> {
//            Channel.State sout = getState(out).resetPushIndex();
//            for (int invocation = 0; invocation < invocations; invocation++) {
//                if (isFilter(c)) {
//                    Channel in = getInputChannel(c, 0);
//                    Window src = getSourceWindow(out, invocation);
//                    if (!src.isValid())
//                        throw new RuntimeException("Invalid output source window:\n" + src);
//                    for (Operation operation : operations) {
//                        int size = operation.size();
//                        switch (operation.op()) {
//                            case CREATE: {
//                                if (isAllocated(out, in)) {
//                                    int offset = invocation * sout.PUSH;
//                                    if (src.offset() != offset)
//                                        throw new RuntimeException("Output source window is not created at expected offset by " + c);
//                                    if (src.limit() - src.offset() != size)
//                                        throw new RuntimeException("Output source window is not created with expected sizeInBytes by " + c);
//                                } else {
//                                    int index = sout.nextPushIndex(operation.size());
//                                    for (int pos = 0; pos < operation.size(); pos++, index++) {
//                                        if (src.indexAt(pos) != index)
//                                            throw new RuntimeException("Output source window is not created with expected index by " + c);
//                                    }
//                                }
//                            }
//                            case INSERT:
//                            case UPDATE: {
//                                int index = sout.nextPushIndex(size);
//                                Window layout = src.layout();
//                                if (layout.contains(c, index, operation.size())) {
//                                    List<Window> references = references(out, in, invocation, OP.UPDATE);
//                                    for (int i = 0; i < operation.size(); i++) {
//                                        int offset = layout.offsetAt(c, index + i);
//                                        if (conflicts(references, offset))
//                                            throw new RuntimeException("Operation " + operation + " by " + c + " has conflicts at offset " + offset);
//                                    }
//                                } else
//                                    throw new RuntimeException("Operation " + operation + " by " + c + " is not performed as expected");
//                            }
//                        }
//                    }
//                } else if (isSplitter(c)) {
//
//                } else if (isJoiner(c)) {
//
//                }
//
//            }
//        });
//
//        // 2. check if sink windows can be derived from source windows
//        return this;
//    }

    // SFG traversal
    public boolean isPredecessor(String c1, String c2) {
        return isPredecessor(getComponent(c1), getComponent(c2));
    }

    public boolean isPredecessor(Component c1, Component c2) {
        return contains(c2, c1);
    }

    public boolean isPredecessor(String c1, String c2, boolean direct) {
        return isPredecessor(getComponent(c1), getComponent(c2), direct);
    }

    public boolean isPredecessor(Component c1, Component c2, boolean direct) {
        for (Channel ch : mChannels) {
            if (!ch.isSink(c1)) continue;
            if (ch.isSource(c2)) return true;
            if (!direct && isPredecessor(ch.src(), c2, direct)) return true;
        }
        return false;
    }

    public boolean isSuccessor(String c1, String c2) {
        return isSuccessor(getComponent(c1), getComponent(c2));
    }

    public boolean isSuccessor(Component c1, Component c2) {
        return contains(c1, c2);
    }

    public boolean isSuccessor(String c1, String c2, boolean direct) {
        return isSuccessor(getComponent(c1), getComponent(c2), direct);
    }

    public boolean isSuccessor(Component c1, Component c2, boolean direct) {
        for (Channel ch : mChannels) {
            if (!ch.isSource(c1)) continue;
            if (ch.isSink(c2)) return true;
            if (!direct && isSuccessor(ch.sink(), c2, direct)) return true;
        }
        return false;
    }

    public SFG saveTextLayout(String filename, String title, boolean verbose) throws IOException {
        PrintWriter printer = new PrintWriter(filename + ".log");
        List<Channel> route = new ArrayList<>();
        List<Channel> future = new ArrayList<>();
        mSuperframes.forEach(sf -> route.addAll(sf.route()));
        mSuperframes.forEach(sf -> future.addAll(sf.future()));

        printer.println(Formatter.title(title));
        printer.println(Formatter.title(new File(filename).getName()));
        Window layout = new Window();
        // layout = route sinks + future soruces
        printer.println(Formatter.title("MEM Layout"));
        layout.clear();
        boolean invoked = false;
        for (Channel ch: route) {
            Channel.State s = getState(ch);
            for(Window sink: s.getSinkWindows().subList(0, s.sinkInvocations()))
                layout.addAll(sink);
            if(ch == route.get(route.size() - 1) && s.sinkInvocations() == 0) {
                invoked = true;

            }
        }

        if(invoked) {
            for (Channel ch: future) {
                Channel.State s = getState(ch);
                for(Window src: s.getSourceWindows())
                    layout.addAll(src);
            }
            Channel.State s = getState(route.get(route.size() - 1));
            for(Window src: s.getSourceWindows())
                layout.addAll(src);
        } else {
            for (Channel ch: future) {
                Channel.State s = getState(ch);
                for(Window src: s.getSourceWindows())
                    layout.addAll(src);
            }
        }

        int limit = layout.limit();
        for(int offset = 0; offset < limit; offset++) {
            List<String> samples = new ArrayList<>();
            List<Range> ranges = new ArrayList<>();
            for (Range range : layout) {
                if (range.contains(offset))
                    ranges.add(range);
            }
            for (Range r : ranges) {
                int pos = offset - r.offset();
                int index = r.indexAt(pos);
                String sample = r.writer().name() + "_" + index;
                if(samples.isEmpty() || !sample.equals(samples.get(samples.size() - 1)))
                    samples.add(sample);
            }
            printer.printf("MEM[%d]: ", offset);
            if(samples.isEmpty())
                printer.println();
            else {
                for (int i = 0; i < samples.size(); i++)
                    printer.printf(i < samples.size() - 1 ? "%s, " : "%s\n", samples.get(i));
            }
        }

        printer.println();
        printer.print("Route: ");
        for(Channel ch: route)
            printer.printf("%s->%s, ", ch.src(), ch.sink());
        printer.println();
        printer.print("Future: ");
        for(Channel ch: future)
            printer.printf("%s->%s, ", ch.src(), ch.sink());
        printer.close();
        return this;
    }

    public SFG saveLayout(String filename, String title, boolean verbose) throws IOException {
        List<Channel> channels = new ArrayList<>();
        mSuperframes.forEach(sf -> {
            channels.addAll(sf.route());
            channels.addAll(sf.future());
        });
        int nameLength = mComponents.stream().mapToInt(c -> c.name().length()).max().getAsInt();
        int max = mChannels.stream()
                .map(ch -> getState(ch))
                .mapToInt(s -> Math.max(s.sourceInvocations(), s.sinkInvocations()))
                .max().getAsInt();
        int xSpacing = Math.max(max * 4, nameLength * 15);
        xSpacing = Math.max(xSpacing, 48);
        int ySpacing = 20;
        int padding = 5;
        int cols = 1 + channels.size() * 2;
        int rows = 1 + size();
        int width = 10 + xSpacing * cols - xSpacing / 2;
        int height = 10 + ySpacing * rows;

        FileOutputStream outputStream = new FileOutputStream(filename + ".eps");
        EpsGraphics g = new EpsGraphics(title, outputStream, 0, 0, width, height, ColorMode.COLOR_RGB);
        // draw layout table grid
        g.setColor(Color.black);
        g.setStroke(new BasicStroke(.5f));
        for (int r = 0; r < rows + 1; r++) {
            int x = padding;
            int y = padding + r * ySpacing;
            g.drawLine(x, y, width - x, y);
            if (r == 1) {
                // draw heading
                for (int c = 0; c < channels.size(); c++) {
                    Channel ch = channels.get(c);
                    String channel = String.format("%s->%s", ch.src(), ch.sink());
                    int chX = padding + (2 * c + 1) * xSpacing;
                    int chY = y;
                    int offset = 5;
                    g.drawString(channel, chX + offset, chY - offset);
                }
            } else if (r > 1) {
                // draw offset
                int offset = 5;
                g.drawString(String.format("%2d", r - 2), x, y - offset);
            }
        }
        for (int c = 1; c < cols; c++) {
            int x = padding + c * xSpacing - xSpacing / 2;
            int y = padding;
            g.drawLine(x, y + (c % 2 == 0 ? ySpacing : 0), x, height - y);
        }

        // draw layouts
        g.setStroke(new BasicStroke(1.0f));
        for (int c = 0; c < channels.size(); c++) {
            Channel ch = channels.get(c);
            Channel.State s = getState(ch);
            // draw source layouts
            g.setColor(Color.blue);
            int invocations = s.sourceInvocations();
            int spacing = xSpacing / (invocations + 1);
            int x = padding + (2 * c + 1) * xSpacing - xSpacing / 2 + spacing;
            for (int i = 0; i < invocations; i++) {
                Window src = s.getSourceWindow(i);
                int srcX = x + spacing * i;
                src.layout().forEach(r -> {
                    int length = r.limit() - r.offset();
                    for (int l = 0; l < length; l++) {
                        int srcY1 = (1 + r.offset() + l) * ySpacing + padding;
                        int srcY2 = srcY1 + ySpacing;
                        g.drawLine(srcX, srcY1, srcX, srcY2);
                        g.drawString(r.originator().toString(), srcX, srcY2);
                    }
                });
            }

            // draw sink layouts
            g.setColor(Color.red);
            invocations = s.sinkInvocations();
            spacing = xSpacing / (invocations + 1);
            x = padding + (2 * c + 1) * xSpacing + xSpacing / 2 + spacing;
            for (int i = 0; i < invocations; i++) {
                Window sink = s.getSinkWindow(i);
                int sinkX = x + spacing * i;
                sink.layout().forEach(r -> {
                    int length = r.limit() - r.offset();
                    for (int l = 0; l < length; l++) {
                        int sinkY1 = (1 + r.offset() + l) * ySpacing + padding;
                        int sinkY2 = sinkY1 + ySpacing;
                        g.drawLine(sinkX, sinkY1, sinkX, sinkY2);
                        g.drawString(r.originator().toString(), sinkX, sinkY2);
                    }
                });
            }
        }

        // save to file
        g.flush();
        g.close();
        return this;
    }

    public SFG save(String filename) throws IOException {
        return save(filename, false);
    }

    public SFG save(String filename, boolean verbose) throws IOException {
        StringBuilder builder = new StringBuilder();
//        digraph finite_state_machine {
//            rankdir=LR;
//            nodes="8,5"
//            node [shape = doublecircle]; LR_0 LR_3 LR_4 LR_8;
//            node [shape = circle];
//            LR_0 -> LR_2 [ name = "SS(B)" ];
//            LR_0 -> LR_1 [ name = "SS(S)" ];
//            LR_1 -> LR_3 [ name = "S($end)" ];
//            LR_2 -> LR_6 [ name = "SS(b)" ];
//            LR_2 -> LR_5 [ name = "SS(a)" ];
//            LR_2 -> LR_4 [ name = "S(A)" ];
//            LR_5 -> LR_7 [ name = "S(b)" ];
//            LR_5 -> LR_5 [ name = "S(a)" ];
//            LR_6 -> LR_6 [ name = "S(b)" ];
//            LR_6 -> LR_5 [ name = "S(a)" ];
//            LR_7 -> LR_8 [ name = "S(b)" ];
//            LR_7 -> LR_5 [ name = "S(a)" ];
//            LR_8 -> LR_6 [ name = "S(b)" ];
//            LR_8 -> LR_5 [ name = "S(a)" ];
//        }

        builder.append("digraph SFG {\n");
        builder.append("rankdir=LR;\n");
        builder.append("node [shape=circle];\n");
        mChannels.stream().forEach(channel -> {
            builder.append(channel.src()).append("->").append(channel.sink());
            builder.append("[arrowhead=vee");
            if (verbose) {
                builder.append(",label=\"");
                builder.append(getState(channel).PUSH).append(",");
                builder.append(getState(channel).PEEK).append(",");
                builder.append(getState(channel).POP);
                builder.append("\"");
            }
            builder.append("];\n");
        });
        builder.append("}");

        RandomAccessFile dot = new RandomAccessFile(filename + ".dot", "rw");
        dot.write(builder.toString().getBytes());
        dot.setLength(dot.getFilePointer());
        dot.close();
        return this;
    }

    public String info(Component c) {
        StringBuilder builder = new StringBuilder();
        builder.append("Component ").append(c.name()).append(" with ");
        Map<Channel, List<Operation>> map = mComponentOperations.getOrDefault(c, new HashMap<>());
        if (map.isEmpty())
            builder.append("0 operations");
        else
            map.forEach((out, operations) -> {
                if (out == null)
                    builder.append(operations.isEmpty() ? 0 : operations.size()).append(" sink operations\n");
                else
                    builder.append("\n\t").append(operations.size()).append(" operations on channel ").append(out).append("\n");
                operations.forEach(operation -> builder.append("\t").append(operation).append("\n"));
            });
        return builder.toString();
    }

    public String info(Channel ch) {
        Channel.State s = mChannelStates.get(ch);
        StringBuilder builder = new StringBuilder().append(toString(ch)).append("\n");
        int invocations = s.sourceInvocations();
        if(s.isSourceGrouped()) {
            for (int i = 0, g = 0; g < s.getSourceWindowGroups().size(); g++) {
                List<Window> group = s.getSourceWindowGroups().get(g);
                int idx = 0;
                for (Window src : group) {
                    builder.append(String.format("========== Source Window[%d/%d:g%d/%d/%d] ==========\n", ++i, invocations, g, ++idx, group.size()));
                    builder.append(src).append("\n");
                }
            }
        } else {
            for (int i = 0; i < invocations; i++) {
                Window src = s.getSourceWindow(i);
                builder.append(String.format("========== Source Window[%d/%d] ==========\n", i + 1, invocations));
                builder.append(src).append("\n");
            }
        }

        invocations = s.sinkInvocations();
        if(s.isSinkGrouped()) {
            for (int i = 0, g = 0; g < s.getSinkWindowGroups().size(); g++) {
                List<Window> group = s.getSinkWindowGroups().get(g);
                int idx = 0;
                for (Window sink : group) {
                    builder.append(String.format("========== Sink Window[%d/%d:g%d/%d/%d] ==========\n", ++i, invocations, g, ++idx, group.size()));
                    builder.append(sink).append("\n");
                }
            }
        } else {
            for (int i = 0; i < invocations; i++) {
                Window sink = s.getSinkWindow(i);
                builder.append(String.format("========== Sink Window[%d/%d] ==========\n", i + 1, invocations));
                builder.append(sink).append("\n");
            }
        }

        if(s.hasRemaining())
            builder.append("========== Remainder ==========\n").append(s.remainder()).append("\n");
        builder.append("========== Layout ==========\n").append(s.layout());
        return builder.toString();
    }

    public String toString(Channel ch) {
        Channel.State s = mChannelStates.get(ch);
        String line = String.format("Channel %s -> %s (%s)", ch.src(), ch.sink(), s);
        int idx = mSuperframes.indexOf(getSuperframe(ch));
        if (idx >= 0)
            line = String.format("%s, SF[%d]", line, idx);
        return line;
    }

    public String toString(Superframe sf) {
        StringBuilder builder = new StringBuilder();
        builder.append("Superframe by Channel:\n");
        for (int i = sf.route().size() - 1; i >= 0; i--) {
            Channel ch = sf.route().get(i);
            builder.append("\t").append(ch).append("\n");
            layout(sf.route().get(i)).forEach(r -> builder.append("\t\t").append(r).append("\n"));
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("========== SFG Info ==========\n");
        builder.append("Components: ").append(mComponents).append("\n");
        Set<List<Component>> sortings = topsort(false);
        builder.append("Sorted components: ").append(sortings.size()).append(sortings).append("\n");
        mChannels.forEach(ch -> builder.append(toString(ch)).append("\n"));
        if(Log.isLoggable(Log.Severity.DEBUG)) {
            builder.append("Push/Peek TM:\n").append(getTopologyMatrixPeek().toString());
            builder.append("Push/Pop TM:\n").append(getTopologyMatrixPop().toString());
        }
        return builder.toString();
    }
}
