package esms.api

import esms.streamit.Stream

interface Schedule {
    static final int INIT = 0
    static final int STEADY = 1
    Scheduler scheduler()
    Stream stream()
    Schedule scale(int factor)
    boolean hasPhase(int phase)
    int invocations(int phase, int idx)
    int invocations(int phase, Stream s)
    int bufferSize(Channel ch)
    int bufferSize(Stream src, Stream sink)
    int bufferSizeInBytes(Channel ch)
    int bufferSizeInBytes(Stream src, Stream sink)
    int sizeInBytes()
    def each(Closure closure)
    def each(int phase, Closure closure)
}