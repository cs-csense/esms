package esms.api;

class Coder {
    private StringBuilder mBuilder = new StringBuilder();
    private int mIndent;
    private boolean mIndentOn;

    public Coder(boolean indent = true) {
        mIndentOn = indent;
    }

    Coder clear() {
        mBuilder.setLength(0);
        return this
    }

    Coder write(String path, String pkg, String filename) {
        File target = new File(path, pkg.replace('.', File.separator))
        new File(target, filename).write(mBuilder.toString())
        return this
    }

    Coder write(String filename) {
        new File(filename).write(mBuilder.toString());
        return this;
    }

    Coder code(String line) {
        return code(line, true);
    }

    Coder code(String line, boolean indentation) {
        if(indentation) indent();
        mBuilder.append(line);
        return this;
    }

    Coder code(String fmt, Object... args) {
        if(mIndentOn) indent();
        mBuilder.append(String.format(fmt, args));
        return this;
    }

    Coder comment(String line) {
        return code("// " + line).newline();
    }

    Coder comment(String fmt, Object ...args) {
        return code("// " + fmt, args).newline();
    }

    Coder EOL() {
        mBuilder.append(";\n");
        return this;
    }

    Coder end() {
        mBuilder.append(";");
        return this;
    }

    Coder newline() {
        mBuilder.append("\n");
        return this;
    }

    Coder indent(boolean on) {
        mIndentOn = on;
        return this;
    }

    Coder indent(int i) {
        mIndent = i;
        return this;
    }

    Coder indent() {
        for(int i = 0; i < mIndent; i++)
            mBuilder.append("    ");
        return this;
    }

    Coder blockStart(boolean indentation = false) {
        return code("{", indentation).newline().indent(++mIndent);
    }

    Coder blockEnd(int newlines) {
        indent(--mIndent).code("}");
        for(int i = 0; i < newlines; i++)
            newline();
        return this;
    }
    Coder blockEnd() {
        return blockEnd(2);
    }

    Coder codeFor(String counter, int to, int decr) {
        if(decr == -1)
            code("for(; $counter > $to; --$counter) ");
        else
            code("for(; $counter > $to; $counter -= $decr) ");
    }

    Coder codeFor(String counter, int from, int to, int incr) {
        if(incr == 1)
            code("for(int %s = %d; %s < %d; ++%s) ", counter, from, counter, to, counter);
        else
            code("for(int %s = %d; %s < %d; %c += %d) ", counter, from, counter, to, counter, incr);
        return this;
    }

    Coder codeFor(String counter, int from, String to, int incr) {
        if(incr == 1)
            code("for(int %s = %d; %s < %s; ++%s) ", counter, from, counter, to, counter);
        else
            code("for(int %s = %d; %s < %s; %s += %d) ", counter, from, counter, to, counter, incr);
        return this;
    }

    Coder codeTemplate(binding = [:], String filename, boolean android = false) {}
}