package esms.api

import esms.core.Superframe
import esms.core.Window
import esms.optimization.Optimizer;
import esms.util.Log;

/**
 * The configuration holds information across optimization phases.
 *
 * Created by farleylai on 6/24/14.
 */
public class Configuration implements Comparable<Configuration> {
    private static final String TAG = 'CFG'
    public static boolean DEBUG = false;
    public static File BUILD;

    // Static settings
    private Schedule schedule;
    private Profile profile

    int phase = Schedule.STEADY
    Map<Integer, SFG> phaseSFG = [:]

    public static void debug(boolean debug, File build) {
        DEBUG = debug;
        BUILD = build == null ? new File("./") : build;
    }

    Configuration(SFG sfg, Schedule schedule, Profile profile) {
        phase = schedule.hasPhase(Schedule.INIT) ? Schedule.INIT : Schedule.STEADY
        phaseSFG[phase] = new SFG(sfg)
        this.schedule = schedule
        this.profile = profile
    }

    public Configuration(Configuration cfg) {
        cfg.each { phase, sfg ->
            phaseSFG[phase] = new SFG(sfg)
        }
        schedule = cfg.schedule;
        profile = cfg.profile
    }

    SFG sfg() {
        phaseSFG[phase]
    }

    SFG sfg(int phase) {
        return phaseSFG[phase]
    }
    SFG initSFG() { return phaseSFG[Schedule.INIT] }
    SFG steadySFG() { return phaseSFG[Schedule.STEADY] }

    List<Channel> route(int phase) {
        SFG sfg = sfg(phase)
        def route =  []
        schedule.each(phase) { Component c, int invocations ->
           if(invocations > 0) {
               route.addAll(sfg.getOutputChannels(c))
           }
        }
        return route
    }
    List<Channel> initRoute() { return route(Schedule.INIT) }
    List<Channel> steadyRoute() { return route(Schedule.STEADY) }

    Schedule schedule() {
        return schedule;
    }

    Optimizer optimizer() {
        return profile.optimizer
    }

    Component.State state(Component c) {
        return phaseSFG[phase].getState(c)
    }

    Channel.State state(Channel ch) {
        return phaseSFG[phase].getState(ch)
    }

    Configuration transition(int phase) {
        SFG sfg = new SFG(sfg()).clearChannels().clearSuperframes()
        this.phase = phase
        phaseSFG[phase] = sfg
        return this
    }

    public boolean hasInit() {
        return schedule.hasPhase(Schedule.INIT);
    }

    public boolean isSteady() {
        return phase == Schedule.STEADY
    }

    public boolean decision(Component c, Channel out) {
        return state(c).decision(out);
    }

    public Configuration window(Component c) {
        SFG sfg = sfg()
        for(Channel ch: sfg.getOutputChannels(c))
            profile.optimizer.window(sfg, ch, schedule.invocations(phase, ch.sink()));
        return this;
    }

    public Configuration rebuild() {
        if (hasInit()) {
            Log.i(TAG, "CFG %s rebuilds INIT superframe source windows\n", this);
            profile.optimizer.rebuild(sfg(Schedule.INIT));
        }
        Log.i(TAG, "CFG %s rebuilds STEADY superframe source windows\n", this);
        profile.optimizer.rebuild(sfg(Schedule.STEADY));
        return this;
    }

    /**
     * Keep track of channels that have been considered whether conflicted or not
     *
     * @param out
     * @param decision
     * @return
     */
    public Configuration decide(Channel out, boolean decision) {
        state(out.src()).decide(out, decision);
        return this;
    }

    /**
     * Enumerate possible schedule orderings due to conflicts
     *
     * @param conflicts read/write conflicts on output channels given the current configuration
     * @return A list of next configurations to explore
     */
    public List<Configuration> branch(List<Channel> conflicts) {
        List<Configuration> cfgs = new ArrayList<>();
        cfgs.add(this);
        int size = (int) Math.pow(2, conflicts.size());
        for (int i = 1; i < size; i++) {
            BitSet combination = BitSet.valueOf([i] as long[]);
            Configuration cfg = new Configuration(this);
            for (int pos = 0; pos < conflicts.size(); pos++) {
                if (combination.get(pos))
                    cfg.decide(conflicts.get(pos), true);
            }
            cfgs.add(cfg);
        }
        return cfgs;
    }

    /**
     * Future work maybe or deprecated.
     *
     * Explore an alternative layout possibility due to read/write conflicts in the StreamIt model.
     * Only filters with CREATE or UPDATE are concerned since the copy and allocations have to be
     * made immediately. Conflicts may be detected only in steady states. If a conflict is detected
     * in initialization, it remains in steady states.
     *
     * @param branching whether to branch for different conflict schedule orderings
     * @return true if no conflict and false otherwise.
     */
    public List<Configuration> detect(Component c, boolean branching) {
        SFG sfg = sfg()
        List<Channel> conflicts = new ArrayList<>();

        // detect read/write conflict in the StreamIt model
        // 1. splitter and joiner won't cause r/w conflicts since they have only PASSes.
        // 2. normal filters are assume to be SISO.
        if (sfg.inDegrees(c) == sfg.outDegrees(c)) {
            // 3. check if there are r/w conflicts
            // 3.1 either write to a read only range
            // 3.2 or write to sliding window overlaps
            Channel out = sfg.getOutputChannel(c, 0);
            if (!state(c).isDetected(out)) {
                if (sfg.conflicts(c, out)) {
                    conflicts.add(out);
                    Log.d(TAG, "Detected R/W conflicts with operations on %s\n", sfg.toString(out));
                } else {
//                    Log.d(TAG, "No R/W conflicts with operations on %s\n", sfg.toString(out));
                }
            }
            state(c).setDetected(out);
        }

        if (!branching)
            conflicts.clear();
        return branch(conflicts);
    }

    public Configuration invoke(Component c, int invocation) {
        optimizer().invoke(sfg(), c, invocation, this)
        return this;
    }

    public int countStreamItInit() {
        return hasInit() ? sfg(Schedule.INIT).countStreamIt(true) : 0;
    }

    public int countStreamItSteady() {
        return sfg(Schedule.STEADY).countStreamIt(false);
    }

    // FIXME: CSense schedule may include a steady state for StreamIt.
    public int countStreamIt() {
        return countStreamItInit() + countStreamItSteady();
    }

    public int allocationStreamItInit() { return hasInit() ? sfg(Schedule.INIT).sizeStreamItInBytes(false) : 0; }

    public int allocationStreamItSteady() { return sfg(Schedule.STEADY).sizeStreamItInBytes(true); }

    public int allocationStreamIt() {
        int initBytes = allocationStreamItInit();
        int steadyBytes = allocationStreamItSteady();
        return Math.max(initBytes, steadyBytes);
    }

    public int evaluateStreamIt() {
        return countStreamIt() + allocationStreamIt();
    }

    public int countInit() {
        return hasInit() ? sfg(Schedule.INIT).count() : 0;
    }

    public int countSteady() {
        return sfg(Schedule.STEADY).count();
    }

    public int count() {
        return countInit() + countSteady();
    }

    public int allocationInit() {
        int bytes = hasInit() ? sfg(Schedule.INIT).remainderInBytes() : 0;
        //Log.d(TAG, "remainder bytes: " + bytes);
        return hasInit() ? sfg(Schedule.INIT).sizeInBytes() + bytes : 0;
    }

    public int allocationSteady() {
        return sfg(Schedule.STEADY).sizeInBytes();
    }

    public int allocation() {
        return Math.max(allocationInit(), allocationSteady());
    }

    public List<Superframe> steadySuperframes() {
        return sfg(Schedule.STEADY).superframes();
    }


    /**
     * Check if different versions of work() are necessary for INIT and Steady
     * given matched window groups from sink to source.
     *
     * Window groups are alignable if
     * 1. there are only steady states
     * 2. IN
     *          2.1 init and steady addressing differs  => false
     *          2.2 both addressing are indirect        => false if the indexes differ and true otherwise
     *          2.3 both addressing are direct          => true if only one group exists or both windows aligns with each other
     *
     * 3. OUT
     *          3.1 init and steady addressing differs  => false
     *          3.2 both addressing are indirect        => false if the actual mapping differ and true otherwise
     *          3.3 both addressing are direct          => true if only one group exists or both windows aligns with each other
     *
     * Two window groups alight if group size == number of windows is the same && the window ranges align.
     */
    public boolean isWindowGroupAlignable(Component c, int g, boolean same) {
        if(!hasInit()) return true;
        SFG sfg = hasInit() ? sfg(Schedule.INIT) : sfg(Schedule.STEADY);
        Channel input = sfg.getInputChannel(c, 0);
        Channel output = sfg.getOutputChannel(c, 0);
        Channel.State sinInit = input == null ? null : initSFG().getState(input);
        Channel.State sinSteady = input == null ? null : steadySFG().getState(input);
        Channel.State soutInit = output == null ? null : initSFG().getState(output);
        Channel.State soutSteady = output == null ? null : steadySFG().getState(output);
        boolean alignable = true;
        if(input != null) {
            if(sinInit.isPopIndirect() != sinSteady.isPopIndirect()) return false;
            else if(sinInit.isPopIndirect()) {
                // FIXME indirect indexes typically differ
                return false;
            } else {
                Window sinkInit = getSinkWindow0(input, g, true);
                Window sinkSteady = getSinkWindow0(input, g, false);
                alignable = sinkInit == null || sinkSteady == null ? true
                        : sinkInit.linearize(same).isAlignable(sinkSteady.linearize(same));
//            if(!alignable) {
//                Log.i(TAG, "sinkInit: \n%s\n", sinkInit);
//                Log.i(TAG, "sinkSteady: \n%s\n", sinkSteady);
//            }
            }
        }
        if(alignable && output != null) {
            if(soutInit.isPushIndirect() != soutSteady.isPushIndirect()) return false;
            else if(soutInit.isPushIndirect()) {
                // FIXME indirect mapping typically differ
                return false;
            } else {
                Window srcInit = getSourceWindow0(output, g, true);
                Window srcSteady = getSourceWindow0(output, g, false);
                alignable = srcInit == null || srcSteady == null ? true
                        : srcInit.linearize(true).isAlignable(srcSteady.linearize(true));
//            if(!alignable) {
//                Log.i(TAG, "srcInit: \n%s\n", srcInit);
//                Log.i(TAG, "srcSteady: \n%s\n", srcSteady);
//            }
            }
        }
        return alignable;
    }

    public SFG getSFG(Component c, int g) {
        SFG sfg = hasInit() ? initSFG() : steadySFG();
        Channel input = sfg.getInputChannel(c, 0);
        Channel output = sfg.getOutputChannel(c, 0);
        if(input != null) {
            Window sinkInit = getSinkWindow0(input, g, true);
            return sinkInit == null ? steadySFG() : initSFG();
        }
        Window srcInit = getSourceWindow0(output, g, true);
        return srcInit == null ? steadySFG() : initSFG();
    }

    public Window getSourceWindow0(Channel ch, int g, boolean init) {
        if(init) {
            if(hasInit()) {
                List<List<Window>> groups = initSFG().getState(ch).getSourceWindowGroups();
                return 0 <= g && g < groups.size() ? groups.get(g).get(0) : null;
            }
            return null;
        }
        List<List<Window>> groups = steadySFG().getState(ch).getSourceWindowGroups();
        return 0 <= g && g < groups.size() ? groups.get(g).get(0) : null;
    }

    public Window getSinkWindow0(Channel ch, int g, boolean init) {
        if(init) {
            if(hasInit()) {
                List<List<Window>> groups = initSFG().getState(ch).getSinkWindowGroups();
                return 0 <= g && g < groups.size() ? groups.get(g).get(0) : null;
            }
            return null;
        }
        List<List<Window>> groups = steadySFG().getState(ch).getSinkWindowGroups();
        return 0 <= g && g < groups.size() ? groups.get(g).get(0) : null;
    }

    public int getMaxWindowGroups(Component c) {
        SFG sfg = hasInit() ? initSFG() : steadySFG();
        Channel input = sfg.getInputChannel(c, 0);
        Channel output = sfg.getOutputChannel(c, 0);
        return Math.max(getMaxSinkWindowGroups(input), getMaxSourceWindowGroups(output));
    }

    public int getMaxSourceWindowGroups(Channel ch) {
        if(ch == null) return 0;
        int max = hasInit() ? initSFG().getState(ch).getSourceWindowGroups().size() : 0;
        return Math.max(max, steadySFG().getState(ch).getSourceWindowGroups().size());
    }

    public int getMaxSinkWindowGroups(Channel ch) {
        if(ch == null) return 0;
        int max = hasInit() ? initSFG().getState(ch).getSinkWindowGroups().size() : 0;
        return Math.max(max, steadySFG().getState(ch).getSinkWindowGroups().size());
    }

    public int getMaxSourceWindowRanges(Channel ch, int g) {
        Window init = getSourceWindow0(ch, g, true);
        Window steady = getSourceWindow0(ch, g, false);
        int max = init == null ? 0 : init.linearize(true).length();
        return Math.max(max, steady == null ? 0 : steady.linearize(true).length());
    }

    public int getMaxSinkWindowRanges(Channel ch, int g, boolean same) {
        Window init = getSinkWindow0(ch, g, true);
        Window steady = getSinkWindow0(ch, g, false);
        int max = init == null ? 0 : init.linearize(same).length();
        return Math.max(max, steady == null ? 0 : steady.linearize(same).length());
    }

    public int sizeInBytes(Superframe sf) {
        int sizeInit = hasInit() ? initSFG().limitInBytes(sf) : 0;
        int sizeSteady = steadySFG().limitInBytes(sf);
        return Math.max(sizeInit, sizeSteady);
    }

    public int evaluate() {
        return count() + allocation();
    }

    public String infoInitChannels() {
        StringBuilder builder = new StringBuilder();
        SFG sfg = initSFG()
        schedule.each(Schedule.INIT) { Component c, int invocations ->
            if(invocations == 0) return
            sfg.getOutputChannels(c).each { ch ->
                if(Log.level() == Log.Severity.DEBUG)
                    builder.append(sfg.info(ch)).append("\n");
                else {
                    if (sfg.isProductive(c))
                        builder.append(sfg.info(ch)).append("\n");
                    else {
                        Channel.State sout = sfg.getState(ch);
                        if (sout.hasRemaining())
                            builder.append(sfg.toString(ch))
                                    .append("\n========== Remainder ==========\n")
                                    .append(sout.remainder()).append("\n");
                    }
                }
            }
        }
        return builder.toString();
    }

    public String infoSteadyChannels() {
        StringBuilder builder = new StringBuilder();
        SFG sfg = steadySFG()
        schedule.each(Schedule.STEADY) { Component c, int invocations ->
            sfg.getOutputChannels(c).each { ch ->
                if(Log.level() == Log.Severity.DEBUG)
                    builder.append(sfg.info(ch)).append("\n");
                else {
                    if (sfg.isProductive(c))
                        builder.append(sfg.info(ch)).append("\n");
                    else {
                        Channel.State sout = sfg.getState(ch);
                        if (sout.hasRemaining())
                            builder.append(sfg.toString(ch))
                                    .append("\n========== Remainder ==========\n")
                                    .append(sout.remainder()).append("\n");
                    }
                }
            }
        }
        return builder.toString();
    }

    /**
     * Collect information of channel layouts.
     *
     * @return
     */
    public String info() {
        StringBuilder builder = new StringBuilder();
        if (hasInit()) {
            builder.append(String.format("===== Final [INIT: %s] cost: (w:%d, a:%d) =====\n", this, countInit(), allocationInit()));
            builder.append(infoInitChannels());
        }
        builder.append(String.format("===== Final [STEADY: %s] cost: (w:%d, a:%d) =====\n", this, countSteady(), allocationSteady()));
        builder.append(infoSteadyChannels());
        return builder.toString();
    }

    /**
     * TODO Time-consuming to generate the information.
     * @return
     */
    @Override
    public String toString() {
        return "";
//        StringBuilder builder = new StringBuilder("|");
//        for (int i = 0; i < schedule.steadyPeriod(); i++) {
//            Component c = schedule.steadyComponent(i);
//            SFG sfg = initSFG == null ? steadySFG : initSFG;
//            sfg.channels().stream().filter(ch -> ch.isSource(c)).forEach(ch -> {
//                builder.append(getState(c).decision(ch) ? "1" : "0").append("|");
//            });
//        }
//        return builder.toString();
    }

    public Configuration save(String filename, boolean verbose) throws IOException {
        if(verbose) {
            if (hasInit())
                initSFG().saveLayout(String.format("%s-init", filename), schedule.toString(), verbose);
            steadySFG().saveLayout(String.format("%s-steady", filename), schedule.toString(), verbose);
        }
        return this;
    }

    public Configuration debug(String filename, boolean verbose) {
        if(DEBUG) {
            try {
                if (!isSteady()) {
//                    initSFG().saveLayout(String.format("%s/%s-init", Configuration.BUILD, filename), schedule.toString(), verbose);
                    initSFG().saveTextLayout(String.format("%s/%s-init", Configuration.BUILD, filename), schedule.toString(), verbose);
                } else {
//                    steadySFG().saveLayout(String.format("%s/%s-steady", Configuration.BUILD, filename), schedule.toString(), verbose);
                    steadySFG().saveTextLayout(String.format("%s/%s-steady", Configuration.BUILD, filename), schedule.toString(), verbose);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return this;
    }

    @Override
    public int compareTo(Configuration cfg) {
        return Integer.compare(evaluate(), cfg.evaluate());
    }
}