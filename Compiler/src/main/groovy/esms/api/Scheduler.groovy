package esms.api

import esms.streamit.Filter
import esms.streamit.Joiner
import esms.streamit.Pipeline
import esms.streamit.SplitJoin
import esms.streamit.Splitter
import esms.streamit.Stream

interface Scheduler {
    Schedule visitPipeline(Pipeline s)
    Schedule visitSplitJoin(SplitJoin s)
    Schedule visitSplitter(Splitter s)
    Schedule visitJoiner(Joiner s)
    Schedule visitFilter(Filter s)

    Schedule schedule()
    Schedule schedule(Stream s)
    int getPushRate(int phase, Stream s)
    int getPeekRate(int phase, Stream s)
    int getPopRate(int phase, Stream s)
}