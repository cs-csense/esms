package esms.api

import esms.optimization.DefaultOptimizer
import esms.types.Strategy
import groovy.transform.Canonical

@Canonical
public class Profile {
    def config = [
            printf: false,
            scaling: 1,
            iterations: 1000,
            partitioning: false,
            partitions: [:],
            optimizer: new DefaultOptimizer(),
            esms: [
                    strategy: Strategy.APPEND_ON_CONFLICT,
                    maxWindowGroups: 32,
            ],
            cacheOpt: [
            ],
            linearStateSpace: [
            ]
    ];

    public Profile() {

    }

    def getAt(b) {
        return config[b]
    }

    def putAt(key, value) {
        config[key] = value
    }

    def propertyMissing(String property) {
        config[property]
    }

    def propertyMissing(String property, value) {
        config[property] = value
    }
}