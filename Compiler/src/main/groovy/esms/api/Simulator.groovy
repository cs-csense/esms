package esms.api

import esms.core.Simulation
import esms.optimization.ESMSOptimizer
import esms.scheduling.ESMSSchedule
import esms.scheduling.ESMSScheduler
import esms.scheduling.PhasedSchedule
import esms.streamit.Stream
import esms.util.Log

/**
 * Created by farleylai on 2/14/14.
 */
public class Simulator {
    static final String TAG = Simulator.class.simpleName
    Profile profile;

    public Simulator() {
       this(new Profile())
    }

    public Simulator(Profile profile) {
        this.profile = profile;
    }

    Simulation simulate(SFG sfg, Schedule schedule) {
        return simulate(sfg, schedule, 1)
    }

    Simulation simulate(SFG sfg, Schedule schedule, int factor) {
        // FIXME ESMS requires SSAS for initialization
        if(schedule.hasPhase(Schedule.INIT) && schedule.invocations(Schedule.INIT, -1) == 0 && profile.optimizer instanceof ESMSOptimizer) {
            PhasedSchedule ssas = (PhasedSchedule) schedule
            schedule.each { Stream s, int initInvocations, int steadyInvocatios ->
                ssas.setInit(s, initInvocations + steadyInvocatios)
            }
        }

        schedule.scale(factor)
        Configuration cfg = new Configuration(sfg, schedule, profile)
        if(cfg.hasInit()) {
            Log.i(TAG, "Simulating INIT schedule...")
            schedule.each(Schedule.INIT) { Stream s, int invocations ->
                if(invocations == 0) return
                invocations.times {
                    cfg.invoke(s, it)
                }
                cfg.window(s)
            }
            cfg.transition(Schedule.STEADY)
        }
        Log.i(TAG, "Simulating STEADY schedule...")
        schedule.each(Schedule.STEADY) { Stream s, int invocations ->
            invocations.times {
                cfg.invoke(s, it)
            }
            cfg.window(s)
        }
        cfg.rebuild()
        List<Configuration> configs = new ArrayList<>()
        configs.add(cfg)
        Simulation simulation = new Simulation()
        simulation.put(schedule, configs)
        return simulation
    }


    public Simulation simulate(SFG sfg) {
        return simulate(sfg, 1);
    }

    @Deprecated
    public Simulation simulate(SFG sfg, int factor) {
        return simulate(sfg, factor, false, false);
    }

    @Deprecated
    public Simulation simulate(SFG sfg, int factor, boolean all) {
        return simulate(sfg, factor, all, false);
    }

    /**
     * Simulate superframe SASs.
     *
     * @param sfg the src SFG that enumerates all possible SASs to simulatePASS.
     * @param factor execution scaling factor
     * @param all explore all possible topological orderings
     * @param branching
     * @return Simulation results of all the possible schedule configurations.
     */
    @Deprecated
    public Simulation simulate(SFG sfg, int factor, boolean all, boolean branching) {
        if (!sfg.isStreamIt())
            throw new RuntimeException("input SFG contains invalid StreamIt construct(s)");

        sfg.setPartitions(profile.partitions);
        Simulation simulation = new Simulation();
        ESMSScheduler scheduler = new ESMSScheduler(sfg).makeSchedules(factor, all);
        List<ESMSSchedule> schedules = profile.optimizer instanceof ESMSOptimizer ? scheduler.getSSASchedules() : scheduler.getSASchedules()

        // Enumerate possible configurations to explore for each schedule
        // So far, only explore the default configuration
        for (ESMSSchedule schedule : schedules) {
            Log.i(TAG, "Simulating %s .....\n", schedule);
            List<Configuration> cfgs = new ArrayList<>();
            cfgs.add(new Configuration(sfg, schedule, profile.optimizer));
            cfgs = simulate(schedule, cfgs, branching);
            simulation.put(schedule, cfgs);
        }
        return simulation;
    }

    /**
     * Simulate the schedule order while applying the optimization.
     *
     * @param schedule the schedule to simulate
     * @param cfgs  the list of configurations to explore
     * @param branching whether to explore possible branching configurations
     * @return
     */
    @Deprecated
    List<Configuration> simulate(ESMSSchedule schedule, List<Configuration> cfgs, boolean branching) {
        int i = 0;
        boolean done = false;
        List<Configuration> configs = new ArrayList<>();
        while (!done) {
            done = true;
            for (Configuration cfg : cfgs) {
                final Component c;
                final int invocations;
                final String stage;
                int order = -1;
                if (cfg.hasNextInitComponent()) {
                    order = cfg.order();
                    c = cfg.nextInitComponent();
                    invocations = schedule.initInvocations(c, order);
                    stage = "INIT";
                    done = false;
                } else if (cfg.hasNextSteadyComponent()) {
                    order = cfg.order();
                    c = cfg.nextSteadyComponent();
                    invocations = schedule.steadyInvocations(c, order);
                    stage = "STEADY";
                    done = false;
                } else {
                    configs.add(cfg);
                    continue;
                }

                // Detect if there are branch configurations
                // For each branch configuration, invoke the scheduled component
                List<Configuration> branches = cfg.window(c).detect(c, branching);
                String before = String.format("%02d-before-%s", i, c);
                String after = String.format("%02d-invoked-%s", i, c);
                for(Configuration branch: branches) {
                    cfg.debug(before, true);
                    for (int invocation = 0; invocation < invocations; invocation++) {
                        Log.d(TAG, "========== [%s] %s invoked(%d), %s ==========\n", stage, c, invocation, branch);
                        branch.invoke(c, invocation);
                    }
                    cfg.debug(after, true);
                }
//                Log.i("%d configuration branches invoked\n", branches.size());
                configs.addAll(branches);
            }
            i++;
            cfgs.clear();
            cfgs.addAll(configs);
            configs.clear();
//            Log.i("%d configurations to simiulate\n", cfgs.size());
        }

        cfgs.each {
            it.rebuild();
        }
        return cfgs;
    }
}
