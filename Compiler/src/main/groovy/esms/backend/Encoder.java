package esms.backend;

import java.util.List;

import esms.streamit.Stream;
import esms.streamit.StreamItEvaluator;
import streamit.frontend.nodes.Function;
import streamit.frontend.nodes.Parameter;

/**
 * Created by farleylai on 11/20/15.
 */
public interface Encoder {
    String encodeChannel(int i);
    String encodeReset(int i);
    String encodeShift(int i);
    String encodeHead(int i);
    String encodeTail(int i);
    String encodePush(int i);
    String encodePeek(int i);
    String encodePop(int i);
    String encodeParams(StreamItEvaluator evaluator, List<Parameter> params);
    String encodeField(Stream s, String field);
    String encodeField(Stream s, String field, boolean global);
    String encodeInit(Stream s);
    String encodeWork(Stream s);
    String encodeBarrier(Stream s);
    String encodeFunction(Stream s, Function func);
    String encodePartitionRunner(int i);
}
