package esms.backend.c

import esms.api.Channel
import esms.api.Component
import esms.api.Configuration
import esms.api.Profile
import esms.api.SFG
import esms.api.Schedule
import esms.core.*
import esms.streamit.*
import esms.util.Log
import esms.util.Shell
import streamit.frontend.nodes.*

import java.io.FileWriter
/**
 * Stream operation naming: op_src_sink_w_r()
 * op: push/peek/pop
 * type: byte/int/float
 * src/sink: component name
 * window: src/sink window index
 * range: window range index
 * => pushInt_source0_FIR0_w0_r3(int value);
 *
 * Superframe head/tail naming:
 * head_src_sink_w_r
 * tail)_src_sink_w_r
 *
 * Created by farleylai on 9/22/14.
 */
public class ESMSCodeGenerator extends FEReplacer implements esms.api.CodeGenerator {
    public static final boolean SINK_LINEARIZE_SAME = false;

    public static final String encodeSchedule(boolean init) { return init ? "init" : "steady"; }

    public static Expression encodeARHeadIndexIncr(Component c, boolean init) {
        // %s[%s++]
        ExprUnary index = new ExprUnary(null, ExprUnary.UNOP_POSTINC, new ExprVar(null, encodeARHeadIndirect()));
        return new ExprArray(null, new ExprVar(null, encodeARHeadIndexes(c, init)), index);
    }

    public static Expression encodeARTailIndex(Component c, boolean init, Expression offset) {
        // %s[%s+%s]
        ExprBinary index = new ExprBinary(null, ExprBinary.BINOP_ADD, new ExprVar(null, encodeARTailIndirect()), offset);
        return new ExprArray(null, new ExprVar(null, encodeARTailIndexes(c, init)), index);
    }

    public static Expression encodeARTailIndexIncr(Component c, boolean init) {
        // %s[++s]
        ExprUnary index = new ExprUnary(null, ExprUnary.UNOP_POSTINC, new ExprVar(null, encodeARTailIndirect()));
        return new ExprArray(null, new ExprVar(null, encodeARTailIndexes(c, init)), index);
    }

    // encode AR peek and pop in indirect addressing mode
    public static String encodeARHeadIndexes(Component c, boolean init) { return String.format("%s_head_AR_indexes_%s", c, encodeSchedule(init)); }
    public static String encodeARTailIndexes(Component c, boolean init) { return String.format("%s_tail_AR_indexes_%s", c, encodeSchedule(init)); }
    public static String encodeARHeadIndirect() { return String.format("head_AR"); }
    public static String encodeARTailIndirect() { return String.format("tail_AR");    }
    public static String encodeARPushIndirect() {
        return String.format("push_AR_indirect");
    }
    public static String encodeARPeekPopIndirect() { return String.format("peek_pop_AR_indirect"); }

    // encode AR push/peek/pop in direct addressing mode indexed by ranges
    public static String encodeARHead(int r) { return String.format("head_AR%d", r); }
    public static String encodeARTail(int r) {
        return String.format("tail_AR%d", r);
    }
    public static String encodeARHeadIncr(int r) { return String.format("head_incr_AR%d", r); }
    public static String encodeARTailIncr(int r) {
        return String.format("tail_incr_AR%d", r);
    }
    public static String encodeARPush(int r) { return String.format("push_AR%d_void", r); }
    public static String encodeARPush(int r, int incr) {
        switch(incr) {
            case  0: return String.format("push_AR%d_zero", r);
            case  1: return String.format("push_AR%d_incr", r);
            case -1: return String.format("push_AR%d_decr", r);
            default:
                return String.format("push_AR%d", r);
        }
    }

    public static String encodeARPeek(int r) {
        return String.format("peek_AR%d", r);
    }

    public static String encodeARPop(int r, int incr) {
        switch(incr) {
            case  0: return String.format("pop_AR%d_zero", r);
            case  1: return String.format("pop_AR%d_incr", r);
            case -1: return String.format("pop_AR%d_decr", r);
            default:
                return String.format("pop_AR%d", r);
        }
    }

    private static String encodeARGroupReset(Filter s, int g, boolean init) {
        return String.format("resetAR_%s_%s_g%d", s, init ? "init" : "steady", g);
    }

    private static String encodeARIntraReset(Filter s, int g, boolean init) {
        return String.format("resetAR_%s_%s_g%di", s, init ? "init" : "steady", g);
    }

    private String encodeParams(List<Parameter> params) {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < params.size(); i++) {
            Parameter param = params.get(i);
            Type type = param.getType();
            if(type instanceof TypeArray) {
                TypeArray t = (TypeArray) type;
                String array = String.format("%s %s", t.getComponent(), param.getName());
                int dims = t.getDims();
                for(int d = 0; d < dims; d++) {
                    array += String.format("[%d]", evaluator().toInt(t.getLength()));
                    if(d + 1 < dims)
                        t = (TypeArray) t.getBase();
                }
                builder.append(array);
            } else
                builder.append(param.getType().toString()).append(" ").append(param.getName());
            if(i + 1 < params.size())
                builder.append(", ");
        }
        return builder.toString();
    }

    public String encodeField(Filter s, String field) {
        return s == mStreamIt.global() ? field : String.format("%s_%s", s.name(), field);
    }

    private static String encodeInit(Stream s) {
        return String.format("%s_init", s);
    }

    private static String encodeWork(Filter s, int g) {
        return String.format("%s_work%d", s, g);
    }

    private static String encodeWork(Filter s, int g, boolean init) {
        return String.format("%s_work%d%s", s, g, init ? 'i' : 's');
    }

    private static String encodeFunction(Filter s, Function func) { return String.format("%s_%s", s.name(), func.getName()); }

    private static String encodeRemainder(Filter s) {
        return String.format("%s_remainder", s);
    }

    private static String encodeSave(Filter s, boolean init) {
        return String.format("%s_save_%s", s.name(), init ? "init" : "steady");
    }

    private static String encodeLoad(Filter s, boolean init) {
        return String.format("%s_load_%s", s.name(), init ? "init" : "steady");
    }

    private String encodeSuperframe(int i) { return String.format("SF%d", i); }
    private String encodeSuperframe(Channel ch) {
        List<Superframe> superframes = mConfig.steadySuperframes();
        Superframe sf = mConfig.steadySuperframes().find { it.contains(ch) }
        return String.format("%s", encodeSuperframe(superframes.indexOf(sf)));
    }

    // code to call push/peek/pop/reset stream operations
    private ESMSCodeGenerator codeARGroupReset(Filter s, int g, boolean init) {
        code("%s()", encodeARGroupReset(s, g, init));
        return this;
    }

    private ESMSCodeGenerator codeARIntraReset(Filter s, int g, boolean init) {
        code("%s()", encodeARIntraReset(s, g, init));
        return this;
    }

    private ESMSCodeGenerator defTypes() {
        code("typedef bool boolean").EOL();
        code("typedef FILE* FP").EOL();
        newline();
        return this;
    }

    // define superframe access indexes
    private ESMSCodeGenerator defSuperframeIndexes(SFG sfg, boolean init) {
        for(Channel ch: sfg.channels()) {
            Channel.State s = sfg.getState(ch);
            if(s.isPushIndirect()) {
                // FIXME cheat the evaluator as a global indexe array
                TypeArray array = new TypeArray(TypePrimitive.inttype, new ExprConstInt(20 * s.PUSH));
                global().registerVar(encodeARHeadIndexes(ch.src(), init), array, global().initialize(array), SymbolTable.KIND_GLOBAL);
                code("static const short %s[%d] = { ", encodeARHeadIndexes(ch.src(), init), s.sourceInvocations() * s.PUSH);
                for(Window src: s.getSourceWindows()) {
                    for(Range r: src) {
                        for(int pos = 0; pos < r.size(); pos++)
                            code("%d, ", r.offsetAt(pos));
                    }
                }
                code(" }").EOL().newline();
            }
            if(s.isPopIndirect()) {
                // FIXME cheat the evaluator as a global indexe array
                TypeArray array = new TypeArray(TypePrimitive.inttype, new ExprConstInt(20 * s.PEEK));
                global().registerVar(encodeARTailIndexes(ch.sink(), init), array, global().initialize(array), SymbolTable.KIND_GLOBAL);
                code("static const short %s[%d] = { ", encodeARTailIndexes(ch.sink(), init), s.sinkInvocations() * s.POP + s.PEEK - s.POP);
                for(Window sink: s.getSinkWindows()) {
                    boolean last = sink == s.getSinkWindows().get(s.getSinkWindows().size() - (s.hasRemaining() ? 2 : 1));
                    int size = last ? s.PEEK : s.POP;
                    while(size > 0) {
                        for(int r = 0; r < sink.length() && size > 0; r++) {
                            Range range = sink.get(r);
                            for(int p = 0; p < range.size() && size > 0; p++) {
                                code("%d, ", range.offsetAt(p));
                                size--
                            }
                        }
                    }
                    if(last) break;
                }
                code(" }").EOL().newline();
            }
        }
        return this;
    }

    // define superframes
    private ESMSCodeGenerator defSuperframes() {
        List<Superframe> superframes = mConfig.steadySuperframes();
        for(int i = 0; i < superframes.size(); i++) {
            Superframe sf = superframes.get(i);
            code("static char %s[%d]", encodeSuperframe(i), mConfig.sizeInBytes(sf)).EOL();
            newline();
        }
        return this;
    }

    // define stream functions
    private ESMSCodeGenerator defARPush() {
        // 1. find max number of linearized push window ranges for all channels
        int max = 0;
        Type type = null;
        SFG sfg = mConfig.steadySFG();
        for(Channel ch: mConfig.steadySFG().channels()) {
            if(sfg.isProductive(ch.src()) && sfg.isFilter(ch.src())) {
                Filter filter = (Filter) ch.src();
                type = type == null ? filter.getOutputType() : type;
                int groups = mConfig.getMaxSourceWindowGroups(ch);
                for (int g = 0; g < groups; g++) {
                    int m = mConfig.getMaxSourceWindowRanges(ch, g);
                    if (m > max)
                        max = m;
                }
            }
        }

        // 2. define head, incr and push for each AR
        for(int r = 0; r < max; r++) {
            String head = encodeARHead(r);
            String incr = encodeARHeadIncr(r);
            code("int %s = 0", head).EOL();
            code("int %s = 1", incr).EOL();
            // define push_ARx_void()
            code("static inline void %s(%s value) ", encodeARPush(r), type).blockStart().blockEnd();
            // define push_ARx_zero()
            code("static inline void %s(%s value) ", encodeARPush(r, 0), type).blockStart()
                    .code("((%s*)%s)[%s] = value", type, encodeSuperframe(0), head).EOL()
                    .blockEnd();
            // define push_ARx_incr()
            code("static inline void %s(%s value) ", encodeARPush(r, 1), type).blockStart()
                    .code("((%s*)%s)[%s++] = value", type, encodeSuperframe(0), head).EOL()
                    .blockEnd();
            // define push_ARx_decr()
            code("static inline void %s(%s value) ", encodeARPush(r, -1), type).blockStart()
                    .code("((%s*)%s)[%s--] = value", type, encodeSuperframe(0), head).EOL()
                    .blockEnd();
            // define push_ARx()
            code("static inline void %s(%s value) ", encodeARPush(r, 2), type).blockStart()
                    .code("((%s*)%s)[%s] = value", type, encodeSuperframe(0), head).EOL()
                    .code("%s += %s", head, incr).EOL()
                    .blockEnd();
        }

        // 3. define push_AR_indirect()
        global().registerVar(encodeARHeadIndirect(), TypePrimitive.inttype, new ExprConstInt(0), SymbolTable.KIND_GLOBAL);
        code("int %s = 0", encodeARHeadIndirect()).EOL();
        code("static inline void %s(int index, %s value) ", encodeARPushIndirect(), type).blockStart()
                .code("((%s*)%s)[index] = value", type, encodeSuperframe(0)).EOL()
                .blockEnd();
        return this;
    }

    private ESMSCodeGenerator defARPeekPop() {
        // 1. find max number of linearized peek/pop window ranges for all channels
        int max = 0;
        Type type = null;
        SFG sfg = mConfig.steadySFG();
        for(Channel ch: sfg.channels()) {
            if(sfg.isProductive(ch.sink()) && sfg.isFilter(ch.sink())) {
                Filter s = (Filter) ch.sink();
                type = type == null ? s.getInputType() : type;
                int groups = mConfig.getMaxSinkWindowGroups(ch);
                for (int g = 0; g < groups; g++) {
                    int m = mConfig.getMaxSinkWindowRanges(ch, g, SINK_LINEARIZE_SAME);
                    if (m > max)
                        max = m;
                }
            }
        }

        // 2. define tail, incr and peek/pop for each AR
        for(int r = 0; r < max; r++) {
            String tail = encodeARTail(r);
            String incr = encodeARTailIncr(r);
            String peek = encodeARPeek(r);
            code("int %s = 0", tail).EOL();
            code("int %s = 1", incr).EOL();
            code("static inline %s %s(int offset) ", type, peek).blockStart()
                    .code("return ((%s*)%s)[%s + offset * %s]", type, encodeSuperframe(0), tail, incr).EOL()
                    .blockEnd();

            // define pop_ARx_zero()
            String pop = encodeARPop(r, 0);
            code("static inline %s %s() ", type, pop).blockStart()
                    .code("return ((%s*)%s)[%s]", type, encodeSuperframe(0), tail).EOL()
                    .blockEnd();
            // define pop_ARx_incr()
            pop = encodeARPop(r, 1);
            code("static inline %s %s() ", type, pop).blockStart()
                    .code("return ((%s*)%s)[%s++]", type, encodeSuperframe(0), tail).EOL()
                    .blockEnd();
            // define pop_ARx_decr()
            pop = encodeARPop(r, -1);
            code("static inline %s %s() ", type, pop).blockStart()
                    .code("return ((%s*)%s)[%s--]", type, encodeSuperframe(0), tail).EOL()
                    .blockEnd();
            // define pop_ARx()
            pop = encodeARPop(r, 2);
            code("static inline %s %s() ", type, pop).blockStart()
                    .code("%s value = ((%s*)%s)[%s]", type, type, encodeSuperframe(0), tail).EOL()
                    .code("%s += %s", tail, incr).EOL()
                    .code("return value").EOL()
                    .blockEnd();
        }

        // 3. define tail indirect and peek_AR_indirect() as well as pop_AR_indirect()
        global().registerVar(encodeARTailIndirect(), TypePrimitive.inttype, new ExprConstInt(0), SymbolTable.KIND_GLOBAL);
        code("int %s = 0", encodeARTailIndirect()).EOL();
        code("static inline %s %s(int index) ", type, encodeARPeekPopIndirect()).blockStart()
                .code("return ((%s*)%s)[index]", type, encodeSuperframe(0)).EOL()
                .blockEnd();
        return this;
    }

    private ESMSCodeGenerator defARReset(Filter s, boolean init) {
        // reset for
        //      inter init window groups
        //      intra init window group
        //      inter steady window groups
        //      intra steady window group
        SFG sfg = init ? mConfig.initSFG() : mConfig.steadySFG();
        Channel input = sfg.getInputChannel(s, 0);
        Channel output = sfg.getOutputChannel(s, 0);
        Channel.State sin = input == null ? null : sfg.getState(input);
        Channel.State sout = (output == null ? null : sfg.getState(output))
        List<List<Window>> sinkGrps = (sin == null ? null : sin.getSinkWindowGroups())
        List<List<Window>> srcGrps = (sout == null ? null : sout.getSourceWindowGroups())
        int groups = (sinkGrps == null ? srcGrps.size() : sinkGrps.size())
        for(int g = 0; g < groups; g++) {
            // absolute inter-group reset
            code("static inline void %s() ", encodeARGroupReset(s, g, init)).blockStart();
            if(input != null) {
                Type type = s.getInputType();
                // indirect AR pop indexes once if any
                if(g == 0 && sin.isPopIndirect()) {
                    code("%s = 0", encodeARTailIndirect()).EOL();
                } else if(!sin.isPopIndirect()){
                    // direct AR ranges
                    String sf = encodeSuperframe(input);
                    List<Window> sinks = sinkGrps.get(g);
                    int r = 0;
                    for (Range range : sinks.get(0).linearize(SINK_LINEARIZE_SAME)) {
                        code("%s = %d", encodeARTail(r), range.offsetAt(0)).EOL();
                        code("%s = %d", encodeARTailIncr(r), range.incr()).EOL();
                        r++;
                    }
                }
            }
            if(output != null) {
                Type type = s.getOutputType();
                // indirect AR push indexes once if any
                if(g == 0 && sout.isPushIndirect()) {
                    code("%s = 0", encodeARHeadIndirect()).EOL();
                } else if(!sout.isPushIndirect()){
                    String sf = encodeSuperframe(output);
                    List<Window> srcs = srcGrps.get(g);
                    int r = 0;
                    for (Range range : srcs.get(0).linearize(true)) {
                        code("%s = %d", encodeARHead(r), range.offsetAt(0)).EOL();
                        code("%s = %d", encodeARHeadIncr(r), range.incr()).EOL();
                        r++;
                    }
                }
            }
            blockEnd();

            // incremental intra-group reset
            code("static inline void %s() ", encodeARIntraReset(s, g, init)).blockStart();
            if(input != null && !sin.isPopIndirect()) {
                List<Window> sinks = sinkGrps.get(g);
                int offset = sinks.size() == 1 ? 0 : (sinks.get(1).offsetAt(0) - sinks.get(0).offsetAt(0));
                int pos = 0;
                int r = 0;
                Window sink = sinks.get(0).linearize(SINK_LINEARIZE_SAME);
                for (Range range: sink) {
                    pos += range.size();
                    if(pos <= sin.POP) {
                        // pure pop range
                        if(offset - range.displacement() != 0)
                            code("%s += %d", encodeARTail(r), offset - range.displacement()).EOL();
                    } else {
                        if(sink.rangeAt(sin.POP - 1) == range) {
                            // partial pop range
                            int displacement = sink.offsetAt(sin.POP - 1) + range.incr() - range.offsetAt(0);
                            if(offset - displacement != 0)
                                code("%s += %d", encodeARTail(r), offset - displacement).EOL();
                        } else {
                            // pure peek range
                            if(offset == 0)
                                code("%s += %d", encodeARTail(r), offset).EOL();
                        }
                    }
                    r++;
                }
            }
            if(output != null && !sout.isPushIndirect()) {
                List<Window> srcs = srcGrps.get(g);
//                int offset = srcs.size() == 1 ? 0 : (srcs.get(1).offset() - srcs.get(0).offset());
                int offset = srcs.size() == 1 ? 0 : (srcs.get(1).offsetAt(0) - srcs.get(0).offsetAt(0));
                int r = 0;
                for (Range range: srcs.get(0).linearize(true)) {
                    if(offset - range.displacement() != 0)
                        code("%s += %d", encodeARHead(r), offset - range.displacement()).EOL();
                    r++;
                }
            }
            blockEnd();
        }

        return this;
    }

    private ESMSCodeGenerator defArray(TypeArray t, String name, StreamItEvaluator evaluator) {
        code("%s %s", t.getComponent(), name);
        int dims = t.getDims();
        for(int i = 0; i < dims; i++) {
            code("[%d]", evaluator.toInt(t.getLength()));
            if(i + 1 < dims)
                t = (TypeArray) t.getBase();
        }
        EOL();
        return this;
    }

    private ESMSCodeGenerator defField(Filter s, Type type, String name, Expression init) {
        StreamItEvaluator evaluator = mStreamIt.evaluator(s);
        if(type instanceof TypeArray) {
            defArray((TypeArray)type, encodeField(s, name), evaluator);
        } else {
            if(init == null)
                code("%s %s", type, encodeField(s, name)).EOL();
            else
                code("%s %s = %s", type, encodeField(s, name), evaluator.eval(init)).EOL();
        }
        return this;
    }

    /**
     * Define sharable remainders by originators.
     * save: remainder by originators -> remainder layout -> compact storage layout
     * load: compact storage layout -> remainder layout -> remainder by originators -> superframe sink0 and more
     *
     * @return
     */
    private ESMSCodeGenerator defRemainders(boolean init) {
        SFG sfg = init ? mConfig.initSFG() : mConfig.steadySFG();
        def route = init ? mConfig.initRoute() : mConfig.steadyRoute()
        Map<Component, Window> remainders = new HashMap<>();
        List<Component> originators = new ArrayList<>();

        // remainders by originators
        route.findAll {
            sfg.getState(it).hasRemaining()
        }.each { Channel ch ->
            Channel.State s = sfg.getState(ch);
            // FIXME Incorrect on execution scaling
            s.remainder().each { r ->
                if(!originators.contains(r.originator())) originators.add(r.originator());
                Window remainder = remainders.getOrDefault(r.originator(), new Window());
                remainder.add(r);
                remainders.put(r.originator(), remainder);
            }
        }

        // remainder layouts and compact storage size
        originators.each { originator ->
            // union of remainders layouts by originators in a reverse order
            Window remainder = remainders.get(originator);
            Collections.reverse(remainder.ranges());
            remainder = remainder.layout();
            remainders.put(originator, remainder);
            int size = remainder.collect { r -> r.sampleSize() * r.size() }.sum();
            if(init)
                code("char %s[%d]", encodeRemainder((Filter) originator), size).EOL();
        }

        // in the schedule order
        route.findAll { sfg.getState(it).hasRemaining() }.each { Channel ch ->
            // save remainder layouts compactly in initialization and steady states
            Channel.State s = sfg.getState(ch);
            if(ch.sink().name() == "join0") {
                Log.i(sfg.info(ch))
            }
            code("static inline void %s() ", encodeSave((Filter)ch.sink(), init)).blockStart();
            remainders.each { originator, remainder ->
                int offset = 0;
                for(Range r: remainder) {
                    if(r.writer() == ch.sink()) {
                        code("memcpy(%s + %d, %s + %d, %d)",
                                encodeRemainder((Filter) originator), offset * r.sampleSize(),
                                encodeSuperframe(ch), r.offsetInBytes(),
                                r.sizeInBytes()).EOL();
                    }
                    offset += r.size();
                }
            }
            blockEnd();

            if (!init) {
                // load remainder by originators to sink0 and more only in steady states
                // FIXME remainder can span more than one sink
                code("static inline void %s() ", encodeLoad((Filter)ch.sink(), init)).blockStart();
                Window sink = s.getSinkWindow(0);
                int pos = 0;
                for(Range from: s.remainder()) {
                    // skip if pre-loaded by prior filters
                    Window remainder = remainders.get(from.originator());
                    if(remainder.every { it.writer() != ch.sink() }) {
                        pos += from.size()
                        continue
                    }

                    // find offset in compact storage
                    int offset = 0;
                    for(Range r: remainder) {
                        if(r.limit() <= from.offset())
                            offset += r.size();
                        else {
                            offset += from.offset() - r.offset();
                            break;
                        }
                    }

                    int limit = pos + from.size();
                    int size = from.size();
                    Log.d("load remainder range from: $from, pos: $pos, limit: $limit, offset: $offset");
                    while(pos < limit && size > 0) {
                        int start = sink.offsetAt(pos);
                        if(start == -1) {
                            // remainder.size() > sink.size()
                            Log.d("start: $start at $pos");
                            start = sink.offsetAt(0) + (s.remainder().offsetAt(pos) - s.remainder().offsetAt(0));
                            code("memcpy(%s + %d, %s + %d, %d)",
                                    encodeSuperframe(ch), start * from.sampleSize(),
                                    encodeRemainder((Filter) from.originator()), offset * from.sampleSize(),
                                    size * from.sampleSize()).EOL();
                            pos += size;
                            break;
                        };
                        int sz = 1;
                        while(sink.offsetAt(pos + sz) == start + sz && sz < size)
                            sz++;
                        code("memcpy(%s + %d, %s + %d, %d)",
                                encodeSuperframe(ch), start * from.sampleSize(),
                                encodeRemainder((Filter) from.originator()), offset * from.sampleSize(),
                                sz * from.sampleSize()).EOL();
                        pos += sz;
                        offset += sz;
                        size -= sz;
                    }
                }
                blockEnd();
            }
        }
        return this;
    }

    private ESMSCodeGenerator defFilter(Filter s) {
        if(mConfig.steadySFG().isProductive(s)) {
            mStream = s;
            s.spec().accept(this);
        }
        return this;
    }

    private ESMSCodeGenerator defSplitter(Splitter s) {
        return this;
    }

    private ESMSCodeGenerator defJoiner(Joiner s) {
        return this;
    }

    private StreamItAdapter mStreamIt;
    private Configuration mConfig;
    private StringBuilder mBuilder;
    private int mIndent;
    private boolean mIndentOn;

    private Stream mStream;
    private int mIterations;
    private int mScaling;

    public ESMSCodeGenerator(StreamItAdapter streamit, Configuration cfg) {
        mStreamIt = streamit;
        mConfig = cfg;
        mIndentOn = true;
    }

    public StreamItEvaluator global() {
        return mStreamIt.evaluator(mStreamIt.global());
    }

    public StreamItEvaluator evaluator() {
        return mStreamIt.evaluator(mStream);
    }

    public ESMSCodeGenerator compile(Profile profile) throws IOException {
        // FIXME incompatible with StreamIt
        mIterations = (int)profile.getAt("iterations") + (mConfig.hasInit() ? -1 : 0);
        mScaling = (int)profile.getAt("scaling");
        Log.i("========== StreamIt to C/C++ Code Generation ==========");
        mBuilder = new StringBuilder();
        header();
        main();
        FileWriter out = new FileWriter(Shell.path(profile["build"], profile["target"]) + ".c", false);
        out.write(mBuilder.toString());
        out.close();
        return this;
    }

    private ESMSCodeGenerator code(String line) {
        return code(line, true);
    }

    private ESMSCodeGenerator code(String line, boolean indent) {
        if(indent) this.indent();
        mBuilder.append(line);
        return this;
    }

    private ESMSCodeGenerator code(String fmt, Object... args) {
        if(mIndentOn) indent();
        mBuilder.append(String.format(fmt, args));
        return this;
    }

    private ESMSCodeGenerator comment(String line) {
        return code("// " + line).newline();
    }

    private ESMSCodeGenerator comment(String fmt, Object ...args) {
        return code("// " + fmt, args).newline();
    }

    private ESMSCodeGenerator EOL() {
        mBuilder.append(";\n");
        return this;
    }

    private ESMSCodeGenerator end() {
        mBuilder.append(";");
        return this;
    }

    private ESMSCodeGenerator newline() {
        mBuilder.append("\n");
        return this;
    }

    private ESMSCodeGenerator indent(boolean on) {
        mIndentOn = on;
        return this;
    }

    private ESMSCodeGenerator indent(int i) {
        mIndent = i;
        return this;
    }

    private ESMSCodeGenerator indent() {
        for(int i = 0; i < mIndent; i++)
            mBuilder.append("\t");
        return this;
    }

    private ESMSCodeGenerator blockStart() {
        return code("{", false).newline().indent(++mIndent);
    }

    private ESMSCodeGenerator blockEnd() {
        return indent(--mIndent).code("}").newline().newline();
    }

    @Override
    public ESMSCodeGenerator include() {
        code("#include<stdio.h>").newline();
        code("#include<stdlib.h>").newline();
        code("#include<stdbool.h>").newline();
        code("#include<string.h>").newline();
        code("#include<math.h>").newline();
        code("#include<time.h>").newline();
        code("#ifdef __MACH__").newline();
        code("#include <sys/resource.h>").newline();
        code("#endif").newline();
        return newline();
    }

    @Override
    public ESMSCodeGenerator header() {
        include();
        defTypes();
        defSuperframes();
        if(mConfig.hasInit())
            defSuperframeIndexes(mConfig.initSFG(), true);
        defSuperframeIndexes(mConfig.steadySFG(), false);
        defARPush();
        defARPeekPop();
        if(mConfig.hasInit()) {
            mConfig.initSFG().components().findAll { mConfig.initSFG().isProductive(it) }.each {
                defARReset((Filter)it, true)
            }
        }
        mConfig.steadySFG().components().findAll { mConfig.steadySFG().isProductive(it) }.each {
            defARReset((Filter)it, false)
        }
        if(mConfig.hasInit())
            defRemainders(true);
        defRemainders(false);
        return newline();
    }

    @Override
    public ESMSCodeGenerator main() {
        mStreamIt.program().accept(this);
        return this;
    }

    @Override
    public Object visitProgram(Program prog) {
        // visit non top-level filters to generate init and work functions
        if((mStream = mStreamIt.global()) != null)
            mStream.spec().accept(this);
        mConfig.steadySFG().components().each { Stream s ->
            if (s.isFilter()) {
                defFilter((Filter) s);
            } else if (s.isSplitter()) {
                defSplitter((Splitter) s);
            } else if (s.isJoiner()) {
                defJoiner((Joiner) s);
            }
        }
        // call init functions with arguments
        prog.getStreams().find { mStreamIt.isTopLevelSpec(it) }?.accept(this)
        return prog;
    }

    private ESMSCodeGenerator codeFor(String counter, int from, int to, int incr) {
        if(incr == 1)
            code("for(int %s = %d; %s < %d; ++%s) ", counter, from, counter, to, counter);
        else
            code("for(int %s = %d; %s < %d; %s += %d) ", counter, from, counter, to, counter, incr);
        return this;
    }

    private ESMSCodeGenerator codeFor(String counter, int from, String to, int incr) {
        if(incr == 1)
            code("for(int %s = %d; %s < %s; ++%s) ", counter, from, counter, to, counter);
        else
            code("for(int %s = %d; %s < %s; %s += %d) ", counter, from, counter, to, counter, incr);
        return this;
    }

    private boolean isProductive(Stream s) {
        return mConfig.steadySFG().isProductive(s);
    }

    private ESMSCodeGenerator codeFilterInit() {
        SFG sfg = mConfig.hasInit() ? mConfig.initSFG() : mConfig.steadySFG();
        if(mStreamIt.global() != null && mStreamIt.global().spec().getInitFunc() != null)
            code("%s()", encodeInit(mStreamIt.global())).EOL();
        sfg.components().findAll { Stream s -> isProductive(s) && s.spec() && s.spec().getInitFunc() }.each {
            code("%s()", encodeInit(it)).EOL()
        }
        return this;
    }

    private ESMSCodeGenerator codeFilterClose() {
        SFG sfg = mConfig.hasInit() ? mConfig.initSFG() : mConfig.steadySFG();
        sfg.components().findAll { Stream s -> isProductive(s) && s.isFilter() && s.isBuiltIn() && s.spec().getFuncNamed("close") }.each {
            code("%s()", encodeFunction(it, it.spec().getFuncNamed("close"))).EOL()
        }
        return this;
    }

    private ESMSCodeGenerator codeInitSchedule() {
        mConfig.schedule().each(Schedule.INIT) { Stream c, int invocations ->
            if(invocations == 0 || c.isSplitter() || c.isJoiner()) return
            Filter s = (Filter)c
            if(s.isProductive()) {
                if(invocations == 1) {
                    codeARGroupReset(s, 0, true).EOL();
                    if (mConfig.isWindowGroupAlignable(c, 0, SINK_LINEARIZE_SAME))
                        code("%s()", encodeWork(s, 0)).EOL();
                    else
                        code("%s()", encodeWork(s, 0, true)).EOL();
                } else {
                    SFG sfg = mConfig.initSFG();
                    Channel input = sfg.getInputChannel(s, 0);
                    Channel output = sfg.getOutputChannel(s, 0);
                    List<List<Window>> groups = (output == null ? sfg.getState(input).getSinkWindowGroups() : sfg.getState(output).getSourceWindowGroups())
                    for (int g = 0; g < groups.size(); g++) {
                        codeARGroupReset(s, g, true).EOL();
                        int times = groups.get(g).size();
                        if (mConfig.isWindowGroupAlignable(c, g, SINK_LINEARIZE_SAME)) {
                            if (times == 1)
                                code("%s()", encodeWork(s, g)).EOL();
                            else {
                                codeFor("i", 0, groups.get(g).size(), 1).blockStart()
                                        .code("%s()", encodeWork(s, g)).EOL()
                                        .codeARIntraReset(s, g, true).EOL()
                                        .blockEnd();
                            }
                        } else {
                            if (times == 1)
                                code("%s()", encodeWork(s, g, true)).EOL();
                            else {
                                codeFor("i", 0, groups.get(g).size(), 1).blockStart()
                                        .code("%s()", encodeWork(s, g, true)).EOL()
                                        .codeARIntraReset(s, g, true).EOL()
                                        .blockEnd();
                            }
                        }
                    }
                }
            }
            if(s.hasRemaining())
                code("%s()", encodeSave(s, true)).EOL();
        }
        return this;
    }

    private ESMSCodeGenerator codeSteadyState(int periods) {
        codeFor("p", 0, periods, 1).blockStart();
        mConfig.schedule().each(Schedule.STEADY) { Stream c, int invocations ->
            if(c.isSplitter() || c.isJoiner()) return
            Filter s = (Filter)c;
            if (s.hasRemaining())
                code("%s()", encodeLoad(s, false)).EOL();
            if(isProductive(c)) {
                if (invocations == 1) {
                    codeARGroupReset(s, 0, false).EOL();
                    if (mConfig.isWindowGroupAlignable(c, 0, SINK_LINEARIZE_SAME))
                        code("%s()", encodeWork(s, 0)).EOL();
                    else
                        code("%s()", encodeWork(s, 0, false)).EOL();
                } else {
                    SFG sfg = mConfig.steadySFG();
                    Channel input = sfg.getInputChannel(s, 0);
                    Channel output = sfg.getOutputChannel(s, 0);
                    List<List<Window>> groups = (output == null ? sfg.getState(input).getSinkWindowGroups() : sfg.getState(output).getSourceWindowGroups())
                    for (int g = 0; g < groups.size(); g++) {
                        codeARGroupReset(s, g, false).EOL();
                        int times = groups.get(g).size();
                        if (mConfig.isWindowGroupAlignable(c, g, SINK_LINEARIZE_SAME)) {
                            if (times == 1)
                                code("%s()", encodeWork(s, g)).EOL();
                            else {
                                codeFor("i", 0, groups.get(g).size(), 1).blockStart()
                                        .code("%s()", encodeWork(s, g)).EOL()
                                        .codeARIntraReset(s, g, false).EOL()
                                        .blockEnd();
                            }
                        } else {
                            if (times == 1)
                                code("%s()", encodeWork(s, g, false)).EOL();
                            else {
                                codeFor("i", 0, groups.get(g).size(), 1).blockStart()
                                        .code("%s()", encodeWork(s, g, false)).EOL()
                                        .codeARIntraReset(s, g, false).EOL()
                                        .blockEnd();
                            }
                        }
                    }
                }
            }
            if(s.hasRemaining())
                code("%s()", encodeSave(s, false)).EOL();
        }
        blockEnd();
        return this;
    }

    private ESMSCodeGenerator codeRemainingSteadyState(int iterations) {
        mConfig.schedule().each(Schedule.STEADY) { Stream c, int invocations ->
            if(c.isSplitter() || c.isJoiner()) return
            Filter s = (Filter)c;
            if(s.hasRemaining())
                code("%s()", encodeLoad(s, false)).EOL();
            if(!isProductive(c)) return
            invocations = iterations * invocations / mScaling;
            if (iterations == 1) {
                codeARGroupReset(s, 0, false).EOL();
                if (mConfig.isWindowGroupAlignable(c, 0, SINK_LINEARIZE_SAME))
                    code("%s()", encodeWork(s, 0)).EOL();
                else
                    code("%s()", encodeWork(s, 0, false)).EOL();
            } else {
                SFG sfg = mConfig.steadySFG();
                Channel input = sfg.getInputChannel(s, 0);
                Channel output = sfg.getOutputChannel(s, 0);
                List<List<Window>> groups = (output == null ? sfg.getState(input).getSinkWindowGroups() : sfg.getState(output).getSourceWindowGroups())
                for (int g = 0; g < groups.size() && invocations > 0; g++) {
                    codeARGroupReset(s, g, false).EOL();
                    int times = invocations > groups.get(g).size() ? groups.get(g).size() : invocations;
                    if (mConfig.isWindowGroupAlignable(c, g, SINK_LINEARIZE_SAME)) {
                        if (times == 1)
                            code("%s()", encodeWork(s, g)).EOL();
                        else
                            codeFor("i", 0, times, 1).blockStart()
                                    .code("%s()", encodeWork(s, g)).EOL()
                                    .codeARIntraReset(s, g, false).EOL()
                                    .blockEnd();
                    } else {
                        if (times == 1)
                            code("%s()", encodeWork(s, g, false)).EOL();
                        else
                            codeFor("i", 0, times, 1).blockStart()
                                    .code("%s()", encodeWork(s, g, false)).EOL()
                                    .codeARIntraReset(s, g, false).EOL()
                                    .blockEnd();
                    }
                }
            }
        }
        return this;
    }

    private boolean mTimed;
    private ESMSCodeGenerator codeTime() {
        if(!mTimed) {
            // time start
            code("#ifdef __MACH__").newline();
            code("struct rusage rr").EOL();
            code("getrusage(RUSAGE_SELF, &rr)").EOL();
            code("long ut = rr.ru_utime.tv_sec * 1000000 + rr.ru_utime.tv_usec").EOL();
            code("long st = rr.ru_stime.tv_sec * 1000000 + rr.ru_stime.tv_usec").EOL();
            code("#else").newline();
            code("long nanos").EOL();
            code("struct timespec tt").EOL();
            code("clock_gettime(CLOCK_THREAD_CPUTIME_ID, &tt)").EOL();
            code("nanos = tt.tv_sec * 1000000000 + tt.tv_nsec").EOL();
            code("#endif").newline();
        } else {
            // time end
            code("#ifdef __MACH__").newline();
            code("getrusage(RUSAGE_SELF, &rr)").EOL();
            code("long rss = rr.ru_maxrss").EOL();
            code("ut = rr.ru_utime.tv_sec * 1000000 + rr.ru_utime.tv_usec - ut").EOL();
            code("st = rr.ru_stime.tv_sec * 1000000 + rr.ru_stime.tv_usec - st").EOL();
            //code("printf(\"utime: %.3fs, stime: %.3fs, max rss: %ldB\\n\", ut / 1000000.0, st / 1000000.0, rss)").EOL();
            code("#else").newline();
            code("clock_gettime(CLOCK_THREAD_CPUTIME_ID, &tt)").EOL();
            code("nanos = tt.tv_sec * 1000000000 + tt.tv_nsec - nanos").EOL();
            //code("printf(\"Thread time elapsed: %fs\\n\", nanos / 1000000000.0)").EOL();
            code("#endif").newline();

        }
        mTimed = !mTimed;
        return this;
    }

    public ESMSCodeGenerator codeSetup() {
        code("int iterations = " + mIterations).EOL();
        codeFor("i", 1, "argc", 1).blockStart()
                .code("if(argc > i + 1 && strcmp(argv[i], \"-i\") == 0)").blockStart()
                .code("iterations = " + mIterations).EOL()
                .code("sscanf(argv[i + 1], \"%d\", &iterations)").EOL()
                .blockEnd()
                .blockEnd();
        code("int periods = iterations / "+ mScaling).EOL();
        code("iterations %= " + mScaling);
        return this;
    }

    @Override
    public Object visitStreamSpec(final StreamSpec spec) {
        if(mStreamIt.isTopLevelSpec(spec)) {
            int periods = mIterations / mScaling;
            int iterations = mIterations % mScaling;
            code("int main(int argc, const char* argv[]) ")
                    .blockStart()
//                    .codeMain()
//                    .codeTime()
                    .comment("========== Filter Initialization ==========")
                    .codeFilterInit()
                    .comment("========== Initialization Period ==========")
                    .codeInitSchedule()
                    .comment("========== Steady State %s Scaling ==========", mScaling > 1 ? "w/" : "w/o")
                    .codeSteadyState(periods);
            if(iterations > 0)
                comment("========== Remaining Steady State ==========", iterations)
                    .codeRemainingSteadyState(iterations);
            comment("========== Filter Finalization ==========")
                    .codeFilterClose();
//            codeTime();
            blockEnd();
//            code("int main() ")
//                    .blockStart()
//                    .code("run()")
//                    .EOL().blockEnd();
        } else {
            // filter definitions with func reordering
            spec.getVars().each { it.accept(this) }
            spec.getFuncs().findAll { it.getCls() != Function.FUNC_WORK && it.getCls() != Function.FUNC_PREWORK }.each {
                it.accept(this)
            }
            spec.getFuncs().findAll { it.getCls() == Function.FUNC_PREWORK || it.getCls() == Function.FUNC_WORK }.each {
                if(it == spec.getWorkFunc())
                    it.accept(this)
                else {
                    // prework
                    // it.accept(this)
                }
            }
        }
        return spec;
    }

    @Override
    public Object visitFieldDecl(FieldDecl field) {
        for(int i = 0; i < field.getNumFields(); i++)
            defField((Filter)mStream, field.getType(i), field.getName(i), field.getInit(i));
        return field;
    }

    @Override
    public Object visitFunction(Function func) {
        switch(func.getCls()) {
            case Function.FUNC_NATIVE: return func;
            case Function.FUNC_INIT:
//                Log.d("StreamItCodeGenerator.visitFunction(): %s.init()\n", mStream);
                code("static inline %s %s(%s) ", func.getReturnType().toString(), encodeInit((Filter) mStream), encodeParams(func.getParams()));
                func.getBody().accept(this);
                break;
            default:
                for(Object p: func.getParams()) {
                    Parameter param = (Parameter)p;
                    //evaluator().push();
                    evaluator().registerVar(param.getName(), param.getType(), evaluator().initialize(param.getType()), StreamItSymbolTable.KIND_FUNC_PARAM);
                }

                code("%s %s(%s) ", func.getReturnType().toString(), encodeFunction((Filter) mStream, func), encodeParams(func.getParams()));
                func.getBody().accept(this);
                //evaluator().pop();
        }
        return func;
    }

    @Override
    public Object visitFuncWork(FuncWork func) {
//        Log.i("visitFuncWork(): %s_work()\n", mStream);
        int groups = mConfig.getMaxWindowGroups(mStream);
        for(int g = 0; g < groups; g++) {
            boolean alignable = mConfig.isWindowGroupAlignable(mStream, g, SINK_LINEARIZE_SAME);
            if(alignable) {
                SFG sfg = mConfig.getSFG(mStream, g);
                FuncWork work = func;
                if(mStream.isSafe()) {
                    work = (FuncWork) func.accept(new StreamItLoopSplitter(mStreamIt, sfg, (Filter) mStream, g, SINK_LINEARIZE_SAME));
                    Log.d("%s: loop splitting of windows group[%d] .... Done\n", mStream, g);
                }
                work = (FuncWork) work.accept(new StreamItWorkGenerator(sfg, g, SINK_LINEARIZE_SAME, evaluator(), false));
                Log.d("%s: stream operations mapped to layout of windows group[%d] .... Done\n", mStream, g);
                code("static inline %s %s(%s) ", func.getReturnType().toString(), encodeWork((Filter) mStream, g), encodeParams(func.getParams()));
                work.getBody().accept(this);

            } else {
                SFG sfg = mConfig.initSFG();
                Channel input = sfg.getInputChannel(mStream, 0);
                Channel output = sfg.getOutputChannel(mStream, 0);
                Channel.State sin = input == null ? null : sfg.getState(input);
                Channel.State sout = output == null ? null : sfg.getState(output);
                boolean existing = sin == null || g < sin.getSinkWindowGroups().size();
                existing = existing && (sout == null || g < sout.getSourceWindowGroups().size());
                if(existing) {
                    // work() for initialization
                    FuncWork workInit = func;
                    if(mStream.isSafe()) {
                        workInit = (FuncWork) func.accept(new StreamItLoopSplitter(mStreamIt, mConfig.initSFG(), (Filter) mStream, g, SINK_LINEARIZE_SAME));
                        Log.d("%s init: loop splitting of windows group[%d] .... Done\n", mStream, g);
                    }
                    workInit = (FuncWork) workInit.accept(new StreamItWorkGenerator(mConfig.initSFG(), g, SINK_LINEARIZE_SAME, evaluator(), true));
                    Log.d("%s init: stream operations mapped to layout of windows group[%d] .... Done\n", mStream, g);
                    code("static inline %s %s(%s) ", func.getReturnType().toString(), encodeWork((Filter) mStream, g, true), encodeParams(func.getParams()));
                    workInit.getBody().accept(this);
                }

                sfg = mConfig.steadySFG();
                input = sfg.getInputChannel(mStream, 0);
                output = sfg.getOutputChannel(mStream, 0);
                sin = input == null ? null : sfg.getState(input);
                sout = output == null ? null : sfg.getState(output);
                existing = sin == null || g < sin.getSinkWindowGroups().size();
                existing = existing && (sout == null || g < sout.getSourceWindowGroups().size());
                if(existing) {
                    // work() for steady states
                    FuncWork workSteady = func;
                    if(mStream.isSafe()) {
                        workSteady = (FuncWork) func.accept(new StreamItLoopSplitter(mStreamIt, mConfig.steadySFG(), (Filter) mStream, g, SINK_LINEARIZE_SAME));
                        Log.d("%s steady: loop splitting of windows group[%d] .... Done\n", mStream, g);
                    }
                    workSteady = (FuncWork) workSteady.accept(new StreamItWorkGenerator(mConfig.steadySFG(), g, SINK_LINEARIZE_SAME, evaluator(), false));
                    Log.d("%s steady: stream operations mapped to layout of windows group[%d] .... Done\n", mStream, g);
                    code("static inline %s %s(%s) ", func.getReturnType().toString(), encodeWork((Filter) mStream, g, false), encodeParams(func.getParams()));
                    workSteady.getBody().accept(this);
                }
            }
        }
        return func;
    }

    @Override
    public Object visitExprVar(ExprVar var) {
//        Log.i("StreamItCodeGenerator.visitExprVar(): " + var);
        if(evaluator().isRegistered(var)) {
            switch (evaluator().kind(var)) {
                case SymbolTable.KIND_STREAM_PARAM:
//                    Log.i("StreamItCodeGenerator.visitExprVar(): KIND_STREAM_PARAM " + var);
                    return evaluator().getVar(var);
                case SymbolTable.KIND_FIELD:
//                    Log.i("StreamItCodeGenerator.visitExprVar(): KIND_FIELD " + var);
                    return new ExprVar(var.getContext(), encodeField((Filter) mStream, var.getName()));
            }
        }
        return var;
    }

    public static String typeC(Type type) {
        if(type instanceof TypePrimitive) {
            TypePrimitive t = (TypePrimitive) type;
            switch (t.getType()) {
                case TypePrimitive.TYPE_STRING:
                    return "%s";
                case TypePrimitive.TYPE_CHAR:
                    return "%c";
                case TypePrimitive.TYPE_BIT:
                case TypePrimitive.TYPE_INT:
                    return "%d";
                case TypePrimitive.TYPE_FLOAT:
                case TypePrimitive.TYPE_DOUBLE:
                    return "%.20f";
//                    return "%.16f";
            }
        } else if(type instanceof TypeArray)
            return typeC(((TypeArray)type).getComponent());
        throw new RuntimeException("Unsupported type: " + type);

    }

    @Override
    public Object visitExprTernary(ExprTernary exp) {
        exp = (ExprTernary) super.visitExprTernary(exp);
        return new ExprTernary(exp.getContext(), exp.getOp() , exp.getA(), exp.getB(), exp.getC()) {
            @Override
            public String toString() {
                return "${getA()} ? ${getB()} : ${getC()}"
            }
        };
    }

    @Override
    public Object visitExprFunCall(ExprFunCall call) {
//        Log.d("StreamItCodeGenerator.visitExprFunCall(): %s(%s)\n", call.getName(), call.getParams());
        String name = call.getName();
        if(name.equals("println")) {
            name = "printf";
            List<Expression> expressions = evaluator().decompose((Expression) call.getParams().get(0), ExprBinary.BINOP_ADD);
            List<Object> args = new ArrayList<>();
            args.add(new ExprConstStr(null, "\"" + expressions.collect { e -> typeC(evaluator().getType(e)) }.join("") + "\\n\""));
            args.addAll(expressions);
            call = new ExprFunCall(call.getContext(), name, args);
        } else if(name.equals("sin") || name.equals("cos") || name.equals("atan") || name.equals("exp") || name.equals("log") || name.equals("sqrt") || name.equals("pow") || name.equals("abs")) {
            // FIXME StreamIt only supports float
//            Log.d("StreamItCodeGenerator.visitExprFunCall(): stream %s, %s.evaluator has 'pos': %s\n", mStream, evaluator().stream(), evaluator().isRegistered("pos"));
//            TypePrimitive type = (TypePrimitive)evaluator().getType((Expression)call.getParams().get(0));
            String fix = "f";
//            switch(type.getType()) {
//                case TypePrimitive.TYPE_FLOAT:
//                case TypePrimitive.TYPE_DOUBLE:
//                    fix = "f";
//                    break;
//            }
            name = name.equals("abs") ? fix + name : name + fix;
            call = new ExprFunCall(call.getContext(), name, call.getParams());
        } else {
            if(!call.getName().startsWith("push") && !call.getName().startsWith("peek") && !call.getName().startsWith("pop")) {
                for (Function func: mStream.spec().getFuncs()) {
                    if (func.getName() != null && func.getName().equals(call.getName())) {
                        call = new ExprFunCall(call.getContext(), encodeFunction((Filter) mStream, func), call.getParams());
                        break;
                    }
                }
            }
        }
        return super.visitExprFunCall(call);
    }

    /**
     * Allow evaluators to register variables so as to determine printf format string.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtVarDecl(StmtVarDecl stmt) {
//        Log.i("StreamItCodeGenerator.visitStmtVarDecl(): %s in stream %s\n", stmt, mStream);
        stmt.accept(evaluator());
        stmt = (StmtVarDecl) super.visitStmtVarDecl(stmt);
        for(int i = 0; i < stmt.getNumVars(); i++) {
            if(i > 0) code("; ", false);
            Type type = stmt.getType(i);
            if(type instanceof TypeArray) {
                // FEReplacer forgot to replace TypeArray!!!
                TypeArray ta = (TypeArray) type;
                type = new TypeArray(ta.getBase(), (Expression) ta.getLength().accept(this));
                String base = ((TypeArray) type).getComponent().toString();
                String offsets = type.toString().substring(base.length());
                code(base + " " + stmt.getName(i) + offsets, false);
            } else {
                code("$type ${stmt.getName(i)}", false);
            }

            if(stmt.getInit(i) != null)
                code(" = " + stmt.getInit(i), false);
        }
        return stmt;
    }

    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
//        Log.i("StreamItCodeGenerator.visitStmtAssign(): " +  stmt);
        stmt = (StmtAssign) super.visitStmtAssign(stmt);
        code(stmt.toString(), false);
        return stmt;
    }

    @Override
    public Object visitStmtReturn(StmtReturn ret) {
        code("return " + ret.getValue().accept(this), false);
        return ret;
    }

    @Override
    public Object visitStmtContinue(StmtContinue ret) {
        code(ret.toString(), false);
        return ret;
    }

    @Override
    public Object visitStmtBreak(StmtBreak ret) {
        code(ret.toString(), false);
        return ret;
    }

    @Override
    public Object visitStmtExpr(StmtExpr stmt) {
        stmt = (StmtExpr) super.visitStmtExpr(stmt);
        code(stmt.toString(), false);
        return stmt;
    }

    @Override
    public Object visitStmtBlock(StmtBlock block) {
        blockStart();
        block.getStmts().each { stmt ->
            indent();
            stmt.accept(this);
            if(stmt instanceof StmtFor) {
            } else if(stmt instanceof StmtIfThen) {
            } else if(stmt instanceof StmtBlock) {
            } else {
                EOL();
            }
        }
        blockEnd();
        return block;
    }

    @Override
    public Object visitStmtIfThen(StmtIfThen stmt) {
        code("if(", false);
        Expression newCond = (Expression) stmt.getCond().accept(this);
        code(newCond.toString(), false).code(") ", false);

        Statement newCons = stmt.getCons();
        if(newCons == null) EOL();
        else newCons = (Statement) stmt.getCons().accept(this);
        if(!(newCons instanceof StmtBlock)) EOL();

        Statement newAlt = stmt.getAlt();
        if(newAlt != null) {
            code("else ");
            newAlt = (Statement) newAlt.accept(this);
            if(!(newAlt instanceof StmtBlock)) EOL();
        }

        return stmt;
    }

    /**
     * Code the for loop while visiting.
     * Always evaluate each statement to replace stream params if any.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtFor(StmtFor stmt) {
        code("for(", false);
        Statement init = stmt.getInit();
        if(init != null)
            init.accept(this);
        end();

        ExprBinary cond = (ExprBinary) stmt.getCond();
        code(" " + cond.accept(this).toString(), false).end().code(" ", false);
        Statement incr = stmt.getIncr();
        incr.accept(this);
        code(") ", false);

        Statement body = stmt.getBody();
        body.accept(this);
        if(!(body instanceof StmtBlock)) EOL();
        return stmt;
    }

    /**
     * Code the while loop while visiting.
     * Always evaluate each statement to replace stream params if any.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtWhile(StmtWhile stmt) {
        code("while(" + stmt.getCond().accept(this).toString() + ") ", false);
        Statement body = stmt.getBody();
        body.accept(this);
        if(!(body instanceof StmtBlock)) EOL();
        return stmt;
    }
}
