package esms.backend.c

import esms.backend.Encoder
import esms.streamit.Stream
import esms.streamit.StreamItEvaluator
import streamit.frontend.nodes.Function
import streamit.frontend.nodes.Parameter
import streamit.frontend.nodes.Type
import streamit.frontend.nodes.TypeArray

class StreamItEncoder implements Encoder {
    @Override
    String encodeChannel(int i) {
        return String.format("channel%d", i);
    }

    @Override
    String encodeReset(int i) {
        return String.format("reset%d", i);
    }

    @Override
    String encodeShift(int i) {
        return String.format("shift%d", i);
    }

    @Override
    String encodeHead(int i) {
        return String.format("head%d", i);
    }

    @Override
    String encodeTail(int i) {
        return String.format("tail%d", i);
    }

    @Override
    String encodePush(int i) {
        return String.format("push%d", i);
    }

    @Override
    String encodePeek(int i) {
        return String.format("peek%d", i);
    }

    @Override
    String encodePop(int i) {
        return String.format("pop%d", i);
    }

    @Override
    String encodeParams(StreamItEvaluator evaluator, List<Parameter> params) {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < params.size(); i++) {
            Parameter param = params.get(i);
            Type type = param.getType();
            if(type instanceof TypeArray) {
                TypeArray t = (TypeArray) type;
                String array = String.format("%s %s", t.getComponent(), param.getName());
                int dims = t.getDims();
                for(int d = 0; d < dims; d++) {
                    array += String.format("[%d]", evaluator.toInt(t.getLength()));
                    if(d + 1 < dims)
                        t = (TypeArray) t.getBase();
                }
                builder.append(array);
            } else
                builder.append(param.getType().toString()).append(" ").append(param.getName());
            if(i + 1 < params.size())
                builder.append(", ");
        }
        return builder.toString();
    }

    @Override
    String encodeField(Stream s, String field) {
        return encodeField(s, field, false)
    }

    @Override
    String encodeField(Stream s, String field, boolean global) {
        return global ? field : String.format("%s_%s", s.name(), field);
    }

    @Override
    String encodeInit(Stream s) {
        return String.format("%s_init", s);
    }

    @Override
    String encodeWork(Stream s) {
        return String.format("%s_work", s);
    }

    @Override
    String encodeBarrier(Stream s) {
        return String.format("%s_barrier", s);
    }

    @Override
    String encodeFunction(Stream s, Function func) { return String.format("%s_%s", s.name(), func.getName()); }

    @Override
    String encodePartitionRunner(int i) {
        return String.format("run$i")
    }
}