package esms.backend.c

import esms.api.Coder
import esms.api.Profile
import esms.api.Channel
import esms.api.Component
import esms.api.Configuration
import esms.api.SFG
import esms.api.Schedule
import esms.api.CodeGenerator
import esms.backend.Encoder
import esms.streamit.Filter
import esms.streamit.Joiner
import esms.streamit.Splitter
import esms.streamit.Stream
import esms.streamit.StreamItFunctionGenerator
import esms.streamit.StreamItAdapter
import esms.streamit.StreamItEvaluator
import esms.streamit.StreamItSymbolTable
import esms.util.Log
import esms.util.Shell
import streamit.frontend.nodes.ExprBinary
import streamit.frontend.nodes.ExprConstStr
import streamit.frontend.nodes.ExprFunCall
import streamit.frontend.nodes.ExprTernary
import streamit.frontend.nodes.ExprVar
import streamit.frontend.nodes.Expression
import streamit.frontend.nodes.FEReplacer
import streamit.frontend.nodes.FieldDecl
import streamit.frontend.nodes.FuncWork
import streamit.frontend.nodes.Function
import streamit.frontend.nodes.Parameter
import streamit.frontend.nodes.Program
import streamit.frontend.nodes.Statement
import streamit.frontend.nodes.StmtAssign
import streamit.frontend.nodes.StmtBlock
import streamit.frontend.nodes.StmtBreak
import streamit.frontend.nodes.StmtContinue
import streamit.frontend.nodes.StmtExpr
import streamit.frontend.nodes.StmtFor
import streamit.frontend.nodes.StmtIfThen
import streamit.frontend.nodes.StmtReturn
import streamit.frontend.nodes.StmtVarDecl
import streamit.frontend.nodes.StmtWhile
import streamit.frontend.nodes.StreamSpec
import streamit.frontend.nodes.SymbolTable
import streamit.frontend.nodes.Type
import streamit.frontend.nodes.TypeArray
import streamit.frontend.nodes.TypePrimitive

public class DefaultCodeGenerator extends FEReplacer implements CodeGenerator {
    class StreamItCoder extends Coder {
        StreamItCoder() {
            super(true)
        }

        StreamItCoder codeFilterInit() {
            SFG sfg = mConfig.hasInit() ? mConfig.initSFG() : mConfig.steadySFG()
            if (mStreamIt.global() != null && mStreamIt.global().spec().getInitFunc() != null)
                code("%s()", encodeInit(mStreamIt.global())).EOL()

            sfg.components().findAll { ((Stream)it).isFilter() && ((Filter)it).spec().getInitFunc() }.each {
                code("%s()", encodeInit((Filter)it)).EOL()
            }
            return this;
        }

        StreamItCoder codeFilterClose() {
            SFG sfg = mConfig.hasInit() ? mConfig.initSFG() : mConfig.steadySFG();
            sfg.components().findAll { ((Stream)it).isFilter()  && ((Filter)it).spec().getFuncNamed("close") }.each {
                Filter s = (Filter)it;
                code("%s()", encodeFunction(s, s.spec().getFuncNamed("close"))).EOL()
            }
            return this;
        }

        StreamItCoder codeInitSchedule() {
            mConfig.schedule().each(Schedule.INIT) { Stream s, int invocations ->
                if(invocations == 0) return
                code("${encodeBarrier(s)}($invocations)").EOL()
                code("${encodeWork(s)}($invocations)").EOL()
            }
            return this;
        }

        StreamItCoder codeInitSchedule(List<Component> partition) {
            mConfig.schedule().each(Schedule.INIT) { Stream s, int invocations ->
                if(invocations > 0 && partition.contains(s)) {
                    codeFor("_i", 0, invocations, 1).blockStart()
                    code("${encodeBarrier(s)}(1)").EOL()
                    code("${encodeWork(s)}(1)").EOL()
                    blockEnd()
                }
            }
            return this;
        }

        StreamItCoder codeSteadyState() {
            codeFor("_p", 0, "_periods", 1).blockStart()
            mConfig.schedule().each(Schedule.STEADY) { Stream s, int invocations ->
                code("${encodeBarrier(s)}($invocations)").EOL()
                code("${encodeWork(s)}($invocations)").EOL()
            }
            blockEnd()
            return this;
        }

        StreamItCoder codeSteadyState(List<Component> partition) {
            codeFor("_p", 0, "_periods", 1).blockStart()
            mConfig.schedule().each(Schedule.STEADY) { Stream s, int invocations ->
                if(partition.contains(s)) {
                    codeFor("_i", 0, invocations, 1).blockStart()
                    code("${encodeBarrier(s)}(1)").EOL()
                    code("${encodeWork(s)}(1)").EOL()
                    blockEnd()
                }
            }
            blockEnd()
            return this;
        }

        StreamItCoder codeRemainingSteadyState() {
            mConfig.schedule().each(Schedule.STEADY) { Stream s, int invocations ->
                code("${encodeBarrier(s)}(${invocations / profile.caling} * _iterations)").EOL()
                code("${encodeWork(s)}(${invocations / profile.scaling} * _iterations)").EOL()
            }
            return this;
        }

        StreamItCoder codeRemainingSteadyState(List<Component> partition) {
            mConfig.schedule().each(Schedule.STEADY) { Stream s, int invocations ->
                if(partition.contains(s)) {
                    codeFor("_i", 0, "${invocations / profile.scaling} * _iterations", 1).blockStart()
                    code("${encodeBarrier(s)}(1)").EOL()
                    code("${encodeWork(s)}(1)").EOL()
                    blockEnd()
                }
            }
            return this;
        }

        StreamItCoder codePartitionedSchedule() {
            mConfig.steadySFG().getPartitions().eachWithIndex { Map.Entry<Integer, List<Component>> entry, int i ->
                code("pthread_t worker$i").EOL()
                code("if(pthread_create(&worker$i, NULL, ${encodePartitionRunner(i)}, NULL)) ").blockStart()
                code("perror(\"Failed to create worker$i thread\")").EOL()
                code("exit(EXIT_FAILURE)").EOL()
                blockEnd()
            }
            mConfig.steadySFG().getPartitions().eachWithIndex { Map.Entry<Integer, List<Component>> entry, int i ->
                code("if(pthread_join(worker$i, NULL)) ").blockStart()
                code("perror(\"Failed to join worker$i thread\")").EOL()
                blockEnd()
            }
            return this
        }

        boolean mTimed;
        StreamItCoder codeTime() {
            if(!mTimed) {
                // time start
                coder.with {
                    code("#ifdef __MACH__").newline();
                    code("struct rusage rr").EOL();
                    code("getrusage(RUSAGE_SELF, &rr)").EOL();
                    code("long ut = rr.ru_utime.tv_sec * 1000000 + rr.ru_utime.tv_usec").EOL();
                    code("long st = rr.ru_stime.tv_sec * 1000000 + rr.ru_stime.tv_usec").EOL();
                    code("#else").newline();
                    code("long nanos").EOL();
                    code("struct timespec tt").EOL();
                    code("clock_gettime(CLOCK_THREAD_CPUTIME_ID, &tt)").EOL();
                    code("nanos = tt.tv_sec * 1000000000 + tt.tv_nsec").EOL();
                    code("#endif").newline();
                }
            } else {
                // time end
                coder.with {
                    code("#ifdef __MACH__").newline();
                    code("getrusage(RUSAGE_SELF, &rr)").EOL();
                    code("long rss = rr.ru_maxrss").EOL();
                    code("ut = rr.ru_utime.tv_sec * 1000000 + rr.ru_utime.tv_usec - ut").EOL();
                    code("st = rr.ru_stime.tv_sec * 1000000 + rr.ru_stime.tv_usec - st").EOL();
                    //code("printf(\"utime: %.3fs, stime: %.3fs, max rss: %ldB\\n\", ut / 1000000.0, st / 1000000.0, rss)").EOL();
                    code("#else").newline();
                    code("clock_gettime(CLOCK_THREAD_CPUTIME_ID, &tt)").EOL();
                    code("nanos = tt.tv_sec * 1000000000 + tt.tv_nsec - nanos").EOL();
                    //code("printf(\"Thread time elapsed: %fs\\n\", nanos / 1000000000.0)").EOL();
                    code("#endif").newline();
                }
            }
            mTimed = !mTimed;
            return this;
        }

        StreamItCoder codeSetup() {
            coder.with {
                code("int main(int argc, const char* argv[]) ").blockStart()
                codeFor("_i", 1, "argc", 1).blockStart()
                code("if(_i + 1 < argc && !strcmp(argv[_i], \"-i\"))").blockStart()
                code("_periods = atoi(argv[_i+1]) / ${profile.scaling}").EOL()
                code("_iterations = atoi(argv[_i+1]) % ${profile.scaling}").EOL()
                blockEnd()
                blockEnd()
            }
            return this;
        }
    }

    StreamItAdapter mStreamIt;
    Configuration mConfig;
    Stream mStream;
    Encoder encoder = new StreamItEncoder()
    StreamItCoder coder;
    Profile profile;

    def methodMissing(String name, args) {
        if(name.startsWith('encode')) {
            DefaultCodeGenerator.metaClass."$name" << { -> encoder.invokeMethod(name, *args) }
            return encoder."$name"(*args)
        } else
            throw new MissingMethodException(name, this.class, args)
    }

    public DefaultCodeGenerator(StreamItAdapter streamit, Configuration cfg) {
        mStreamIt = streamit;
        mConfig = cfg;
        coder = new StreamItCoder();
    }

    StreamItEvaluator global() {
        return mStreamIt.evaluator(mStreamIt.global());
    }

    StreamItEvaluator evaluator() {
        return mStreamIt.evaluator(mStream);
    }

    DefaultCodeGenerator defTypes() {
        coder.with {
            code("typedef bool boolean").EOL();
            code("typedef FILE* FP").EOL();
            newline();
        }
        return this;
    }

    DefaultCodeGenerator defArray(TypeArray t, String name, StreamItEvaluator evaluator) {
        coder.with {
            code("%s %s", t.getComponent(), name);
            int dims = t.getDims();
            for (int i = 0; i < dims; i++) {
                code("[%d]", evaluator.toInt(t.getLength()));
                if (i + 1 < dims)
                    t = (TypeArray) t.getBase();
            }
            EOL();
        }
        return this;
    }

    DefaultCodeGenerator defField(Filter s, Type type, String name, Expression init) {
        StreamItEvaluator evaluator = mStreamIt.evaluator(s);
        if(type instanceof TypeArray) {
            defArray((TypeArray)type, encodeField(s, name, mStreamIt.global() == s), evaluator);
        } else {
            coder.with {
                if (init == null) code("%s %s", type, encodeField(s, name, mStreamIt.global() == s)).EOL();
                else code("%s %s = %s", type, encodeField(s, name, mStreamIt.global() == s), evaluator.eval(init)).EOL();
            }
        }
        return this;
    }

    DefaultCodeGenerator defPush(Type type, int ch) {
        coder.with {
            code("static inline void %s(%s value) ", encodePush(ch), type).blockStart()
                    .code("%s[%s++] = value", encodeChannel(ch), encodeHead(ch)).EOL()
                    .blockEnd();
        }
        return this
    }

    DefaultCodeGenerator defPeek(Type type, int ch) {
        coder.with {
            code("static inline %s %s(int offset) ", type, encodePeek(ch)).blockStart()
                    .code("return %s[%s + offset]", encodeChannel(ch), encodeTail(ch)).EOL()
                    .blockEnd();
        }
        return this
    }

    DefaultCodeGenerator defPop(Type type, int ch) {
        coder.with {
            code("static inline %s %s() ", type, encodePop(ch)).blockStart()
                    .code("return %s[%s++]", encodeChannel(ch), encodeTail(ch)).EOL()
                    .blockEnd();
        }
        return this
    }

    DefaultCodeGenerator defShift(int ch, int bytes) {
        coder.with {
            Channel channel = mConfig.steadySFG().getChannel(ch)
            Stream src = (Stream) channel.src()
            Stream sink = (Stream) channel.sink()
            int size = mConfig.schedule().bufferSize(channel)
            int invocations = (size - sink.getPeekRate(src)) / sink.getPopRate(src) + 1
            int head = size
            int tail = invocations * sink.getPopRate(src)
            String memcpy = tail  < head - tail ? "memmove" : "memcpy"
            code("static inline void %s() ", encodeShift(ch)).blockStart()
                    .code("$memcpy(%s, &%s[%s], $bytes * ${head - tail})", encodeChannel(ch), encodeChannel(ch), encodeTail(ch)).EOL()
                    .blockEnd();
        }
        return this
    }

    DefaultCodeGenerator defReset(int ch) {
        coder.with {
            code("static inline void %s() ", encodeReset(ch)).blockStart()
                    .code("%s -= %s", encodeHead(ch), encodeTail(ch)).EOL()
                    .code("%s = 0", encodeTail(ch)).EOL()
                    .blockEnd();
        }
        return this
    }

    DefaultCodeGenerator defBarrier(Stream s) {
        coder.with {
            SFG sfg = mConfig.steadySFG()
            code("static inline void %s(int invocations) ", encodeBarrier(s)).blockStart()
            // input barrier
            s.getInputs().each {
                Channel channel = sfg.getChannel(it.last(), s)
                if(sfg.isPartitioned(channel)) {
                    int ch = sfg.indexOf(channel)
                    code("while(${encodeHead(ch)} - ${encodeTail(ch)} < (invocations - 1) * ${s.getPopRate(it)} + ${s.getPeekRate(it)})").EOL()
                }
            }
            // output barrier
            s.getOutputs().each {
                Channel channel = sfg.getChannel(s, it.first())
                int ch = sfg.indexOf(channel)
                Channel.State sout = mConfig.state(channel);
//                Stream sink = (Stream)channel.sink();
                if(sfg.isPartitioned(channel)) {
                    int size = mConfig.schedule().bufferSize(channel)
                    int invocations = (size - sout.PEEK) / sout.POP + 1
                    code("if(${mConfig.schedule().bufferSize(channel)} - ${encodeHead(ch)} < invocations * ${sout.PUSH}) ").blockStart()
                    code("while(${encodeHead(ch)} - ${encodeTail(ch)} > ${size - invocations * sout.POP})").EOL()
                    if (sout.PEEK > sout.POP)
                        code("%s()", encodeShift(ch)).EOL()
                    code("%s()", encodeReset(ch)).EOL()
                    blockEnd()
                } else {
                    if (sout.PEEK > sout.POP)
                        code("%s()", encodeShift(ch)).EOL()
                    code("%s()", encodeReset(ch)).EOL()
                }
            }
            blockEnd()
        }
        return this
    }

    DefaultCodeGenerator defChannels() {
        coder.with {
            // Channel buffer and indexes
            SFG sfg = mConfig.steadySFG();
            sfg.channels().eachWithIndex { channel, i ->
                Type type = ((Stream)channel.src()).getOutputType()
                code("static %s %s[%d]", type, encodeChannel(i), mConfig.schedule().bufferSize(channel)).EOL();
            }
            sfg.channels().eachWithIndex { channel, i ->
                String modifier = sfg.isPartitioned(channel) ? "volatile" : ""
                code("static $modifier int %s = 0", encodeHead(i)).EOL();
                code("static $modifier int %s = 0", encodeTail(i)).EOL();
            }
            newline();

            // Stream operations on channels
            sfg.channels().eachWithIndex { ch, i ->
                Type type = ((Stream)ch.src()).getOutputType()
                defPush(type, i)
                defPeek(type, i)
                defPop(type, i)
                defReset(i);

                // define remainder load if PEEK > POP
                Stream src = (Stream)ch.src()
                Stream sink = (Stream)ch.sink()
                if(sink.getPeekRate(src) > sink.getPopRate(src)) {
                    int bytes = 4
                    switch(type) {
                        case TypePrimitive.chartype:
                            btyes = 1; break;
                        case TypePrimitive.inttype:
                        case TypePrimitive.floattype:
                            bytes = 4; break;
                        default:
                            throw new RuntimeException("Unknown type: " + type)
                    }
                    defShift(i, bytes)
                }
            }
        }
        return this;
    }

    DefaultCodeGenerator defFilter(Filter s) {
        mStream = s;
        s.spec().accept(this);
        defBarrier(s)
        return this;
    }

    DefaultCodeGenerator defSplitter(Splitter s) {
        coder.with {
            SFG sfg = mConfig.steadySFG()
            Type type = s.getInputType(s.getInput(0))
            int input = sfg.indexOf(sfg.getInputChannel(s, 0))
            code("static inline void %s(int invocations) ", encodeWork(s)).blockStart()
            codeFor("_i", 0, "invocations", 1).blockStart()
            def weights = s.weights()
            switch(s.weights().length) {
                case 0: // duplicate
                    code("%s v", type).EOL()
                    s.getPopRate(s.getInput(0)).times {
                        code("v = %s()", encodePop(input)).EOL();
                        s.getOutputs().each {
                            code("%s(v)", encodePush(sfg.indexOf(sfg.getChannel(s, it.first())))).EOL()
                        }
                    }
                    break
                case 1:
                    weights = s.weights().toList() * s.getOutputs().size()
                default: // round robin
                    weights.eachWithIndex { weight, i ->
                        int output = sfg.indexOf(sfg.getOutputChannel(s, i))
                        int count = weight / 8
                        int rem = weight % 8
                        if(count > 1) {
                            codeFor("_c", 0, count, 1).blockStart()
                            8.times {
                                code("%s(%s())", encodePush(output), encodePop(input)).EOL()
                            }
                            blockEnd()
                            weight = rem
                        }
                        weight.times {
                            code("%s(%s())", encodePush(output), encodePop(input)).EOL()
                        }
                    }
            }
            blockEnd()
            blockEnd()
        }
        defBarrier(s)
        return this;
    }

    DefaultCodeGenerator defJoiner(Joiner s) {
        coder.with {
            SFG sfg = mConfig.steadySFG();
            int output = sfg.indexOf(sfg.getOutputChannel(s, 0))
            code("static inline void %s(int invocations) ",  encodeWork(s)).blockStart()
            codeFor("_i", 0, "invocations", 1).blockStart()
            def weights = s.weights().length == 1 ? s.weights().toList() * s.getInputs().size() : s.weights()
            weights.eachWithIndex{ weight, i ->
                int input = sfg.indexOf(sfg.getInputChannel(s, i))
                int count = weight / 8
                int rem = weight % 8
                if(count > 1) {
                    codeFor("_c", 0, count, 1).blockStart()
                    8.times {
                        code("%s(%s())", encodePush(output), encodePop(input)).EOL()
                    }
                    blockEnd()
                    weight = rem
                }
                weight.times {
                    code("%s(%s())", encodePush(output), encodePop(input)).EOL()
                }
            }
            blockEnd()
            blockEnd()
        }
        defBarrier(s)
        return this;
    }

    DefaultCodeGenerator defPartitionRunners() {
        mConfig.steadySFG().getPartitions().each { i, partition ->
            coder.with {
                code("static void* ${encodePartitionRunner(i)}(void* arg) ").blockStart()
                comment("========== Initialization Period ==========")
                codeInitSchedule(partition)
                comment("========== Steady State %s Scaling ==========", profile.scaling > 1 ? "w/" : "w/o")
                codeSteadyState(partition)
                if(profile.scaling > 1) {
                    comment("========== Remaining Steady State ==========")
                    codeRemainingSteadyState(partition)
                }
                code("return NULL").EOL()
                blockEnd()
            }
        }
        return this
    }

    @Override
    DefaultCodeGenerator compile(Profile profile) throws IOException {
        Log.i("========== StreamIt to C/C++ Code Generation ==========");
        this.profile = profile
        header();
        main();
        coder.write(Shell.path(profile.build, profile.target) + '.c');
        return this;
    }

    @Override
    DefaultCodeGenerator include() {
        coder.with {
            code("#include<stdio.h>").newline();
            code("#include<stdlib.h>").newline();
            code("#include<stdbool.h>").newline();
            code("#include<string.h>").newline();
            code("#include<math.h>").newline();
            code("#include<time.h>").newline();
            code("#include<pthread.h>").newline();
            code("#ifdef __MACH__").newline();
            code("#include <sys/resource.h>").newline();
            code("#endif").newline();
            newline();
        }
        return this
    }

    @Override
    DefaultCodeGenerator header() {
        include();
        defTypes();
        defChannels();
        coder.newline();
        return this;
    }

    @Override
    DefaultCodeGenerator main() {
        mStreamIt.program().accept(this);
        return this;
    }

    @Override
    public Object visitProgram(Program prog) {
        if((mStream = mStreamIt.global()) != null)
            mStream.spec().accept(this);

        // visit non top-level filters to generate init and work functions
        mConfig.steadySFG().components().each {
            Stream s = (Stream)it
            if (s.isFilter()) {
                defFilter((Filter) s);
            } else if (s.isSplitter()) {
                defSplitter((Splitter) s);
            } else if (s.isJoiner()) {
                defJoiner((Joiner) s);
            }
        }

        coder.with {
            code("static int _periods = ${profile.iterations / profile.scaling}").EOL()
            code("static int _iterations = ${profile.iterations % profile.scaling}").EOL()
        }
        if(mConfig.steadySFG().isPartitioned())
            defPartitionRunners()

        // call init functions with arguments
        prog.getStreams().find { mStreamIt.isTopLevelSpec(it) }?.accept(this)
        return prog;
    }

    @Override
    public Object visitStreamSpec(final StreamSpec spec) {
        coder.with {
            if (mStreamIt.isTopLevelSpec(spec)) {
                codeSetup()
//                    .codeTime()
                comment("========== Filter Initialization ==========");
                codeFilterInit()
                if(mConfig.steadySFG().isPartitioned()) {
                    codePartitionedSchedule();
                } else {
                    comment("========== Initialization Period ==========")
                    codeInitSchedule()
                    comment("========== Steady State %s Scaling ==========", profile.scaling > 1 ? "w/" : "w/o")
                    codeSteadyState();
                    if (profile.scaling > 1) {
                        comment("========== Remaining Steady State ==========")
                        codeRemainingSteadyState();
                    }
//            codeTime();
                }
                comment("========== Filter Finalization ==========")
                codeFilterClose();
                blockEnd();
            } else {
                // filter definitions with func reordering
                spec.getVars().each { it.accept(this) };
                spec.getFuncs()
                        .findAll {
                    it.getCls() != Function.FUNC_WORK && it.getCls() != Function.FUNC_PREWORK
                }.each { it.accept(this) };
                spec.getFuncs()
                        .findAll {
                    it.getCls() == Function.FUNC_WORK || it.getCls() == Function.FUNC_PREWORK
                }.each {
                    if (it == spec.getWorkFunc())
                        it.accept(this);
                    else {
                        // prework
                        // it.accept(this);
                    }
                }
            }
        }
        return spec;
    }

    @Override
    public Object visitFieldDecl(FieldDecl field) {
        for(int i = 0; i < field.getNumFields(); i++)
            defField(mStream, field.getType(i), field.getName(i), field.getInit(i));
        return field;
    }

    @Override
    public Object visitFunction(Function func) {
        switch(func.getCls()) {
            case Function.FUNC_NATIVE: return func;
            case Function.FUNC_INIT:
//                Log.d("StreamItCodeGenerator.visitFunction(): %s.init()\n", mStream);
                coder.code("static inline %s %s(%s) ", func.getReturnType().toString(), encodeInit(mStream), encodeParams(evaluator(), func.getParams()));
                func.getBody().accept(this);
                break;
            default:
                for(Object p: func.getParams()) {
                    Parameter param = (Parameter)p;
                    evaluator().registerVar(param.getName(), param.getType(), evaluator().initialize(param.getType()), StreamItSymbolTable.KIND_FUNC_PARAM);
                }

                coder.code("%s %s(%s) ", func.getReturnType().toString(), encodeFunction(mStream, func), encodeParams(evaluator(), func.getParams()));
                func.getBody().accept(this);
        }
        return func;
    }

    @Override
    public Object visitFuncWork(FuncWork work) {
//        Log.i("visitFuncWork(): %s_work()\n", mStream);
        coder.with {
            code("static inline void %s(int invocations) ", encodeWork(mStream)).blockStart();
            codeFor("invocations", 0, -1)
            Function func = (Function) work.accept(new StreamItFunctionGenerator(new StreamItEncoder(), mConfig.steadySFG(), mStream));
            func.getBody().accept(this)
            blockEnd()
            return func
        }

    }

    @Override
    public Object visitExprVar(ExprVar var) {
//        Log.i("StreamItCodeGenerator.visitExprVar(): " + var);
        if(evaluator().isRegistered(var)) {
            switch (evaluator().kind(var)) {
                case SymbolTable.KIND_STREAM_PARAM:
//                    Log.i("StreamItCodeGenerator.visitExprVar(): KIND_STREAM_PARAM " + var);
                    return evaluator().getVar(var);
                case SymbolTable.KIND_FIELD:
//                    Log.i("StreamItCodeGenerator.visitExprVar(): KIND_FIELD " + var);
                    return new ExprVar(var.getContext(), encodeField(mStream, var.getName(), mStreamIt.global() == mStream));
            }
        }
        return var;
    }

    public static String typeC(Type type) {
        if(type instanceof TypePrimitive) {
            TypePrimitive t = (TypePrimitive) type;
            switch (t.getType()) {
                case TypePrimitive.TYPE_STRING:
                    return "%s";
                case TypePrimitive.TYPE_CHAR:
                    return "%c";
                case TypePrimitive.TYPE_BIT:
                case TypePrimitive.TYPE_INT:
                    return "%d";
                case TypePrimitive.TYPE_FLOAT:
                case TypePrimitive.TYPE_DOUBLE:
                    return "%.20f";
//                    return "%.16f";
            }
        } else if(type instanceof TypeArray)
            return typeC(((TypeArray)type).getComponent());
        throw new RuntimeException("Unsupported type: " + type);

    }

    @Override
    public Object visitExprTernary(ExprTernary exp) {
        exp = (ExprTernary) super.visitExprTernary(exp);
        return new ExprTernary(exp.getContext(), exp.getOp() , exp.getA(), exp.getB(), exp.getC()) {
            @Override
            public String toString() {
                return "${getA()} ? ${getB()} : ${getC()}";
            }
        };
    }

    @Override
    public Object visitExprFunCall(ExprFunCall call) {
//        Log.d("StreamItCodeGenerator.visitExprFunCall(): %s(%s)\n", call.getName(), call.getParams());
        String name = call.getName();
        if(name.equals("println")) {
            name = "printf";
            List<Expression> expressions = evaluator().decompose((Expression) call.getParams().get(0), ExprBinary.BINOP_ADD);
            List<Object> args = new ArrayList<>();
            //args.add(new ExprConstStr(null, "\"" + String.join("", (expressions.stream().map(e -> typeC(evaluator().getType(e))).collect(Collectors.toList()))) + "\\n\""));
            args.add(new ExprConstStr(null, "\"" + expressions.collect { it -> typeC(evaluator().getType(it)) }.join("") + "\\n\""));
            args.addAll(expressions);
            call = new ExprFunCall(call.getContext(), name, args);
        } else if(name.equals("sin") || name.equals("cos") || name.equals("atan") || name.equals("exp") || name.equals("log") || name.equals("sqrt") || name.equals("pow") || name.equals("abs")) {
            // FIXME StreamIt only supports float
//            Log.d("StreamItCodeGenerator.visitExprFunCall(): stream %s, %s.evaluator has 'pos': %s\n", mStream, evaluator().stream(), evaluator().isRegistered("pos"));
//            TypePrimitive type = (TypePrimitive)evaluator().getType((Expression)call.getParams().get(0));
            String fix = "f";
//            switch(type.getType()) {
//                case TypePrimitive.TYPE_FLOAT:
//                case TypePrimitive.TYPE_DOUBLE:
//                    fix = "f";
//                    break;
//            }
            name = name.equals("abs") ? fix + name : name + fix;
            call = new ExprFunCall(call.getContext(), name, call.getParams());
        } else {
            if(!call.getName().startsWith("push") && !call.getName().startsWith("peek") && !call.getName().startsWith("pop")) {
                for (Function func: mStream.spec().getFuncs()) {
                    if (func.getName() != null && func.getName().equals(call.getName())) {
                        call = new ExprFunCall(call.getContext(), encodeFunction((Filter) mStream, func), call.getParams());
                        break;
                    }
                }
            }
        }
        return super.visitExprFunCall(call);
    }

    /**
     * Allow evaluators to register variables so as to determine printf format string.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtVarDecl(StmtVarDecl stmt) {
//        Log.i("StreamItCodeGenerator.visitStmtVarDecl(): %s in stream %s\n", stmt, mStream);
        stmt.accept(evaluator());
        stmt = (StmtVarDecl) super.visitStmtVarDecl(stmt);
        coder.with {
            for (int i = 0; i < stmt.getNumVars(); i++) {
                if (i > 0) code("; ", false);
                Type type = stmt.getType(i);
                if (type instanceof TypeArray) {
                    // FEReplacer forgot to replace TypeArray!!!
                    TypeArray ta = (TypeArray) type;
                    type = new TypeArray(ta.getBase(), (Expression) ta.getLength().accept(this));
                    String base = ((TypeArray) type).getComponent().toString();
                    String offsets = type.toString().substring(base.length());
                    code(base + " " + stmt.getName(i) + offsets, false);
                } else {
                    code("$type ${stmt.getName(i)}", false);
                }

                if (stmt.getInit(i) != null)
                    code(" = " + stmt.getInit(i), false);
            }
        }
        return stmt;
    }

    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
//        Log.i("StreamItCodeGenerator.visitStmtAssign(): " +  stmt);
        stmt = (StmtAssign) super.visitStmtAssign(stmt);
        coder.code(stmt.toString(), false);
        return stmt;
    }

    @Override
    public Object visitStmtReturn(StmtReturn ret) {
        coder.code("return " + ret.getValue().accept(this), false);
        return ret;
    }

    @Override
    public Object visitStmtContinue(StmtContinue ret) {
        coder.code(ret.toString(), false);
        return ret;
    }

    @Override
    public Object visitStmtBreak(StmtBreak ret) {
        coder.code(ret.toString(), false);
        return ret;
    }

    @Override
    public Object visitStmtExpr(StmtExpr stmt) {
        coder.with {
            stmt = (StmtExpr) super.visitStmtExpr(stmt);
            code(stmt.toString(), false);
        }
        return stmt;
    }

    @Override
    public Object visitStmtBlock(StmtBlock block) {
        coder.with {
            blockStart();
            block.getStmts().each { stmt ->
                indent();
                stmt.accept(this);
                if (stmt instanceof StmtFor) {
                } else if (stmt instanceof StmtIfThen) {
                } else if (stmt instanceof StmtBlock) {
                } else {
                    EOL();
                }
            }
            blockEnd();
        }
        return block;
    }

    @Override
    public Object visitStmtIfThen(StmtIfThen stmt) {
        coder.with {
            code("if(", false);
            Expression newCond = (Expression) stmt.getCond().accept(this);
            code(newCond.toString(), false).code(") ", false);

            Statement newCons = stmt.getCons();
            if (newCons == null) EOL();
            else newCons = (Statement) stmt.getCons().accept(this);
            if (!(newCons instanceof StmtBlock)) EOL();

            Statement newAlt = stmt.getAlt();
            if (newAlt != null) {
                code("else ");
                newAlt = (Statement) newAlt.accept(this);
                if (!(newAlt instanceof StmtBlock)) EOL();
            }
        }
        return stmt;

    }

    /**
     * Code the for loop while visiting.
     * Always evaluate each statement to replace stream params if any.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtFor(StmtFor stmt) {
        coder.with {
            code("for(", false);
            Statement init = stmt.getInit();
            if (init != null)
                init.accept(this);
            end();

            ExprBinary cond = (ExprBinary) stmt.getCond();
            code(" " + cond.accept(this).toString(), false).end().code(" ", false);
            Statement incr = stmt.getIncr();
            incr.accept(this);
            code(") ", false);

            Statement body = stmt.getBody();
            body.accept(this);
            if (!(body instanceof StmtBlock)) EOL();
        }
        return stmt;
    }

    /**
     * Code the while loop while visiting.
     * Always evaluate each statement to replace stream params if any.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtWhile(StmtWhile stmt) {
        coder.with {
            code("while(" + stmt.getCond().accept(this).toString() + ") ", false);
            Statement body = stmt.getBody();
            body.accept(this);
            if (!(body instanceof StmtBlock)) EOL();
        }
        return stmt;
    }
}