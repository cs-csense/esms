package esms.backend.java
import esms.backend.Encoder
import esms.streamit.Stream
import esms.streamit.StreamItEvaluator
import streamit.frontend.nodes.Function
import streamit.frontend.nodes.Parameter
import streamit.frontend.nodes.Type
import streamit.frontend.nodes.TypeArray

public class JavaEncoder implements Encoder {
    @Override
    String encodeChannel(int i) {
        return String.format("channel%d", i);
    }

    @Override
    String encodeReset(int i) {
        return "channel$i.clear";
    }

    @Override
    String encodeShift(int i) {
        return "channel$i.flip";
    }

    @Override
    String encodeHead(int i) {
        return String.format("channel%d.head()", i);
    }

    @Override
    String encodeTail(int i) {
        return String.format("channel%d.tail()", i);
    }

    @Override
    String encodePush(int i) {
        return '_out.push'
    }

    @Override
    String encodePeek(int i) {
        return '_in.peek'
    }

    @Override
    String encodePop(int i) {
        return '_in.pop'
    }

    @Override
    String encodeParams(StreamItEvaluator evaluator, List<Parameter> params) {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < params.size(); i++) {
            Parameter param = params.get(i);
            Type type = param.getType();
            if(type instanceof TypeArray) {
                TypeArray t = (TypeArray) type;
                builder.append("${t.getComponent()}${"[]" * t.getDims()} ${param.getName()}");
            } else
                builder.append(param.getType().toString()).append(" ").append(param.getName());
            if(i + 1 < params.size())
                builder.append(", ");
        }
        return builder.toString();
    }

    @Override
    String encodeField(Stream s, String field) {
        return encodeField(s, field, false)
    }

    @Override
    String encodeField(Stream s, String field, boolean global) {
        return global ? "${JavaCodeGenerator.PROG}.${field}" : field;
    }

    @Override
    String encodeInit(Stream s) {
        return "${s.name()}.init"
    }

    @Override
    String encodeWork(Stream s) {
        return "${s.name()}.work"
    }

    @Override
    String encodeBarrier(Stream s) {
        return "${s.name()}.barrier"
    }

    @Override
    String encodeFunction(Stream s, Function func) {
        return func.name == 'close' ? "${s.name()}.${func.name}" : "${func.name}"
    }

    @Override
    String encodePartitionRunner(int i) {
        return "scheduler$i"
    }
}