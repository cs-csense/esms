package esms.backend.java

import groovy.text.GStringTemplateEngine
import esms.api.*
import esms.backend.Encoder
import esms.streamit.Stream
import esms.streamit.Filter
import esms.streamit.StreamItAdapter
import esms.streamit.StreamItEvaluator
import esms.streamit.StreamItFunctionGenerator
import esms.streamit.StreamItSymbolTable
import esms.util.Log
import esms.util.Shell
import streamit.frontend.nodes.*

public class JavaCodeGenerator extends FEReplacer implements CodeGenerator {
    static final String TAG = JavaCodeGenerator.class.simpleName
    static String PROG = 'esms'
    static final int METHOD_SIZE = 640

    class JavaCoder extends Coder {
        GStringTemplateEngine engine
        JavaCoder() {
            super(true)
            engine = new GStringTemplateEngine()
        }

        JavaCoder codeFilterClose() {
            comment("========== Filter Finalization ==========")
            SFG sfg = mConfig.hasInit() ? mConfig.initSFG() : mConfig.steadySFG();
            sfg.components().findAll { ((Stream)it).isFilter()  && ((Filter)it).spec().getFuncNamed("close") }.each {
                Filter s = (Filter)it;
                code("%s()", encodeFunction(s, s.spec().getFuncNamed("close"))).EOL()
            }
            return this;
        }

        JavaCoder codeInitSchedule() {
            comment('========== Initialization Phase ==========')
            mConfig.schedule().each(Schedule.INIT) { Stream s, int invocations ->
                if(invocations == 0) return
                code("${encodeWork(s)}($invocations)").EOL()
            }
            return this;
        }

        JavaCoder codeInitSchedule(List<Component> partition) {
            comment('========== Initialization Phase ==========')
            SFG sfg = mConfig.steadySFG();
            mConfig.schedule().each(Schedule.INIT) { Stream s, int invocations ->
                if(invocations > 0 && s in partition) {
                    if(invocations > 1) {
                        boolean boundary = !s.getInputs().every { it.last() in partition }
                        boundary = boundary || !s.getOutputs().every { it.first() in partition }
                        if(boundary) {
                            codeFor("_i", 0, invocations, 1).blockStart()
                            code("${encodeWork(s)}(1)").EOL()
                            blockEnd()
                        } else
                            code("${encodeWork(s)}($invocations)").EOL()
                    } else
                        code("${encodeWork(s)}(1)").EOL()
                }
            }
            return this;
        }

        JavaCoder codeSteadyState() {
            comment("========== Steady State ${profile.scaling > 1 ? "w/" : "w/o"} Scaling ==========")
            codeFor("_p", 0, "_periods", 1).blockStart()
            mConfig.schedule().each(Schedule.STEADY) { Stream s, int invocations ->
                if(invocations == 0) return
                code("${encodeWork(s)}($invocations)").EOL()
            }
            blockEnd()
            return this;
        }

        JavaCoder codeSteadyState(List<Component> partition) {
            comment("========== Steady State ${profile.scaling > 1 ? "w/" : "w/o"} Scaling ==========")
            SFG sfg = mConfig.steadySFG()
            mConfig.schedule().each(Schedule.STEADY) { Stream s, int invocations ->
                if(invocations > 0 && s in partition) {
                    if(invocations > 1) {
                        // FIXME Dynamic rates cause partial filled channels preventing both producers and consumers
                        boolean boundary = !s.getInputs().every { it.last() in partition }
                        boundary = boundary || !s.getOutputs().every { it.first() in partition }
                        if(boundary) {
                            codeFor("_i", 0, invocations, 1).blockStart()
                            code("${encodeWork(s)}(1)").EOL()
                            blockEnd()
                        } else
                            code("${encodeWork(s)}($invocations)").EOL()
                    } else
                        code("${encodeWork(s)}(1)").EOL()
                }
            }
            return this;
        }

        JavaCoder codeRemainingSteadyState() {
            comment("========== Remaining Steady State ==========")
            mConfig.schedule().each(Schedule.STEADY) { Stream s, int invocations ->
                if(invocations == 0) return
                code("${encodeWork(s)}(${invocations / mScaling} * _iterations)").EOL()
            }
            return this;
        }

        JavaCoder codeRemainingSteadyState(List<Component> partition) {
            comment("========== Remaining Steady State ==========")
            SFG sfg = mConfig.steadySFG()
            mConfig.schedule().each(Schedule.STEADY) { Stream s, int invocations ->
                if(invocations > 0 && s in partition) {
                    if(invocations > 1) {
                        boolean boundary = !s.getInputs().every { it.last() in partition }
                        boundary = boundary || !s.getOutputs().every { it.first() in partition }
                        if(boundary) {
                            codeFor("_i", 0, "${invocations / mScaling} * _iterations", 1).blockStart()
                            code("${encodeWork(s)}(1)").EOL()
                            blockEnd()
                        } else
                            code("${encodeWork(s)}($invocations)").EOL()
                    } else
                        code("${encodeWork(s)}(1)").EOL()
                }
            }
            return this;
        }

        JavaCoder codePartitionedSchedule() {
            mConfig.steadySFG().getPartitions().each { int i, partition ->
                code("Scheduler scheduler$i = new Scheduler(_periods) ").blockStart()
                code('protected void initialization() throws InterruptedException ').blockStart()
                codeInitSchedule(partition)
                blockEnd()
                code('protected void steadyState() throws InterruptedException ').blockStart()
                codeSteadyState(partition)
                blockEnd()
                if (profile.iterations % profile.scaling > 0) {
                    code('protected void remaining() throws InterruptedException ').blockStart()
                    codeRemainingSteadyState(partition)
                    blockEnd(1)
                }
                blockEnd(0).EOL()
                code("scheduler${i}.start()").EOL()
                code("_schedulers.add(scheduler$i)").EOL()
            }
            return this
        }

        @Override
        JavaCoder codeTemplate(binding = [:], String filename, boolean android = false) {
            URL gsp = getClass().getResource("../${android ? 'android' : 'java'}/$filename")
            if(!binding.pkg) binding.pkg = profile.pkg
            if(binding.imports)
                binding.imports = binding.imports.collect { 'import ' + (binding.pkg ? "$binding.pkg.$it" : it) }.join(';\n')
            else binding.imports = null
            code(engine.createTemplate(gsp).make(binding).toString())
            return this
        }
    }

    StreamItAdapter mStreamIt;
    Configuration mConfig;
    Stream mStream;
    Profile profile;
    Encoder encoder = new JavaEncoder()
    def coders = [:]
    def coder

    public JavaCodeGenerator(StreamItAdapter streamit, Configuration cfg) {
        mStreamIt = streamit
        mConfig = cfg
    }

    def methodMissing(String name, args) {
        if(name.startsWith('encode')) {
            JavaCodeGenerator.metaClass."$name" << { -> encoder.invokeMethod(name, *args) }
            return encoder."$name"(*args)
        } else
            throw new MissingMethodException(name, this.class, args)
    }

    String qName(String name) {
        return profile.pkg ? "${profile.pkg}.$name" : name
    }

    StreamItEvaluator global() {
        return mStreamIt.evaluator(mStreamIt.global());
    }

    StreamItEvaluator evaluator() {
        return mStreamIt.evaluator(mStream);
    }

    def defStream() {
        return new JavaCoder().codeTemplate('Stream.gsp')
    }

    def defFilter() {
        return new JavaCoder().codeTemplate('Filter.gsp')
    }

    def defFilter(Filter s) {
        mStream = s
        coder = new JavaCoder()
        s.spec().accept(this)
        return coder
    }

    def defStreams() {
        coders[qName('Stream')] = defStream()
        coders[qName('Filter')] = defFilter()
        mConfig.steadySFG().components()
                .findAll { it.isFilter() && !(it.basename() in coders) }
                .each {
                    def coder = new JavaCoder()
                    coders[qName(it.basename())] = defFilter(it);
                }
        coders[qName('Splitter')] = defSplitter()
        coders[qName('Joiner')] = defJoiner()
    }

    def defSplitter() {
        return new JavaCoder().codeTemplate('Splitter.gsp')
    }

    def defJoiner() {
        return new JavaCoder().codeTemplate('Joiner.gsp')
    }

    def defChannels() {
        coders[qName('Channel')] = new JavaCoder().codeTemplate(imports:['Log'], 'Channel.gsp')
        coders[qName('IntChannel')] = new JavaCoder().codeTemplate(type:'int', 'PrimitiveChannel.gsp')
        coders[qName('FloatChannel')] = new JavaCoder().codeTemplate(type:'float', 'PrimitiveChannel.gsp')
    }

    // FIXME max number of 2560 channels due to JVM 64K method/constructor code size limit
    // FIXME Dalvik method size limit: 16384 units
    def defProgramChannels(coder) {
        coder.with {
            SFG sfg = mConfig.steadySFG();
            sfg.channels().eachWithIndex { channel, i ->
                Channel.State s = sfg.getState(channel)
                def type = ((Stream) channel.src()).getOutputType()
                type = type == TypePrimitive.inttype ? 'IntChannel' : 'FloatChannel'
                code("private $type ${encodeChannel(i)}").EOL()
            }

            def codeInitChannels = { int batch, int start, int end ->
                def channels = sfg.channels()[start..<end]
                code("private void initChannels$batch() ").blockStart()
                channels.eachWithIndex { channel, i ->
                    Channel.State s = sfg.getState(channel)
                    def type = ((Stream) channel.src()).getOutputType()
                    type = type == TypePrimitive.inttype ? 'IntChannel' : 'FloatChannel'
                    code("${encodeChannel(start+i)} = new $type($i, ${mConfig.schedule().bufferSize(channel)}, ${s.PUSH}, ${s.PEEK}, ${s.POP}, ${sfg.isPartitioned(channel)})").EOL();
                }
                blockEnd()
            }
            int batches = sfg.channels().size() / METHOD_SIZE
            int rem = sfg.channels().size() % METHOD_SIZE
            (batches).times {
                codeInitChannels(it, it * METHOD_SIZE, (it+1) * METHOD_SIZE)
            }
            if(rem > 0)
                codeInitChannels(batches, batches * METHOD_SIZE, sfg.channels().size())

            code("protected void initChannels() ").blockStart()
            batches.times {
                code("initChannels$it()").EOL()
            }
            if(rem > 0) code("initChannels$batches()").EOL()
            blockEnd()
        }
    }

    // FIXME max number of 2560 filters due to JVM 64K method/constructor code size limit
    // FIXME Dalvik method size limit: 16384 units
    def defProgramStreams(Coder coder) {
        coder.with {
            SFG sfg = mConfig.steadySFG()
            sfg.components().eachWithIndex { Stream s, i ->
                if(s.isFilter()) {
                    code("private ${s.basename()} ${s.name()}").EOL()
                } else if(s.isSplitter()) {
                    code("private Splitter ${s.name()}").EOL()
                } else if(s.isJoiner()){
                    code("private Joiner ${s.name()}").EOL()
                }
            }

            def codeInitFilters = { int batch, int start, int end ->
                code("private void initFilters$batch() ").blockStart()
                def components = sfg.components()[start..<end]
                components.eachWithIndex { Stream s, i ->
                    if(s.isFilter()) {
                        def input = sfg.isSource(s) ? null : encodeChannel(sfg.indexOf(sfg.getInputChannel(s, 0)))
                        def output = sfg.isSink(s) ? null : encodeChannel(sfg.indexOf(sfg.getOutputChannel(s, 0)))
                        if (s.args()) {
                            def args = []
                            s.spec().params.each {
                                args << mStreamIt.evaluator(s).getVar(it.name)
                            }
                            code("${s.name()} = new ${s.basename()}($i, $input, $output, ${args.join(", ")})").EOL()
                        } else
                            code("${s.name()} = new ${s.basename()}($i, $input, $output)").EOL()
                    } else if(s.isSplitter()) {
                        def input = encodeChannel(sfg.indexOf(sfg.getInputChannel(s, 0)))
                        def outputs = []
                        sfg.getOutputChannels(s).each {
                            outputs << encodeChannel(sfg.indexOf(it))
                        }
                        def weights = s.weights().join(", ");
                        def type = s.getInputType(s.getInput(0)) == TypePrimitive.inttype ? 'IntChannel' : 'FloatChannel'
                        code("${s.name()} = new Splitter($i, $input, new $type[]{ ${outputs.join(", ")} }, new int[] { $weights })").EOL()
                    } else if(s.isJoiner()){
                        def inputs = []
                        sfg.getInputChannels(s).each {
                            inputs << encodeChannel(sfg.indexOf(it))
                        }
                        def output = encodeChannel(sfg.indexOf(sfg.getOutputChannel(s, 0)))
                        def weights = s.weights().join(", ");
                        def type = s.getInputType(s.getInput(0)) == TypePrimitive.inttype ? 'IntChannel' : 'FloatChannel'
                        code("${s.name()} = new Joiner($i, new $type[] { ${inputs.join(', ')} }, $output, new int[] { $weights })").EOL()
                    }
                }
                blockEnd()
            }
            int batches = sfg.components().size() / METHOD_SIZE
            int rem = sfg.components().size() % METHOD_SIZE
            (batches).times {
                codeInitFilters(it, it * METHOD_SIZE, (it+1) * METHOD_SIZE)
            }
            if(rem > 0)
                codeInitFilters(batches, batches * METHOD_SIZE, sfg.components().size())

            code("protected void initFilters() ").blockStart()
            batches.times {
                code("initFilters$it()").EOL()
            }
            if(rem > 0) code("initFilters$batches()").EOL()
            blockEnd()
        }
    }

    def defProgram() {
        coder = new JavaCoder()
        coder.with {
            code("""${profile.pkg ? "package ${profile.pkg};" : ''}
import java.io.*;
import static java.lang.Math.*;
import esms.runtime.*;
import esms.util.*;
public class $PROG extends Program """).blockStart()
            code("public $PROG(String[] args) ").blockStart()
            code('super(args)').EOL()
            blockEnd()

            if((mStream = mStreamIt.global()) != null) mStream.spec().accept(this);
            defProgramChannels(coder)
            defProgramStreams(coder)
            code("public void cleanup() ").blockStart()
            codeFilterClose()
            code("super.cleanup()").EOL()
            blockEnd()
//            if(!mConfig.steadySFG().isPartitioned()) {
//                code('private void initialization() throws InterruptedException ').blockStart()
//                codeInitSchedule()
//                blockEnd(1)
//                code('private void steady() throws InterruptedException ').blockStart()
//                codeSteadyState()
//                blockEnd(1)
//                if (profile.scaling > 1) {
//                    code('private void remaining() throws InterruptedException ').blockStart()
//                    codeRemainingSteadyState()
//                    blockEnd(1)
//                }
//            }
//            code("private int _periods = ${profile.iterations / profile.scaling}").EOL()
//            code("private int _iterations = ${profile.iterations % profile.scaling}").EOL()
//            code('public static String _path = "."').EOL()
            code('protected void run() ').blockStart()
            codePartitionedSchedule();
//            if(mConfig.steadySFG().isPartitioned()) {
//                codePartitionedSchedule();
//            } else {
//                code('try ').blockStart()
//                code('initialization()').EOL()
//                code('steady()').EOL()
//                if (profile.scaling > 1)
//                    code('remaining()').EOL()
//                blockEnd(0).code('catch(InterruptedException e) ', false).blockStart().blockEnd()
//            }
//            codeFilterClose();
            blockEnd()
            code('public void report() ').blockStart()
            SFG sfg = mConfig.steadySFG()
            sfg.channels().findAll { sfg.isPartitioned(it) }
                            .collect { "${encodeChannel(sfg.indexOf(it))}.report()"  }
                            .each { code(it).EOL() }
            blockEnd()
            codeTemplate(benchmark:PROG, scaling:profile.scaling, 'main.gsp')
            blockEnd()
        }
        coders[qName(PROG)] = coder
    }

    @Override
    CodeGenerator include() {
        return this
    }

    @Override
    CodeGenerator header() {
        defChannels()
        defStreams()
        return this
    }

    @Override
    CodeGenerator main() {
        defProgram()
        return this
    }

    @Override
    CodeGenerator compile(Profile profile) throws IOException {
        Log.i(TAG, "========== StreamIt to Java Code Generation ==========");
        this.profile = profile
//        this.PROG = profile.benchmark
        include()
        header()
        main()
        def pkg = new File(profile.outputDir, profile.pkg.replace('.', File.separator))
        pkg.mkdirs()
        coders.each { String qName, coder ->
            def filename = qName.split('\\.')[-1]
            coder.write("${Shell.path(pkg, filename)}.java");
        }
        return this;
    }

    JavaCodeGenerator defArray(TypeArray t, String name, StreamItEvaluator evaluator, boolean global = false) {
        coder.with {
            code(global ? 'static ' : 'private ')
            code("${t.getComponent()}${"[]" * t.getDims()} $name", false)
            if(global) {
                int dims = t.getDims()
                code(" = new ${t.getComponent()}", false)
                for (int i = 0; i < dims; i++) {
                    code("[${t.getLength().accept(this)}]", false);
                    if (i + 1 < dims)
                        t = (TypeArray)t.getBase();
                }
            }
            EOL();
        }
        return this;
    }

    JavaCodeGenerator defField(Filter s, Type type, String name, Expression init) {
        StreamItEvaluator evaluator = mStreamIt.evaluator(s);
        if(type instanceof TypeArray) {
            if(mStreamIt.global() == s) {
                defArray((TypeArray)type, name, evaluator, true);
            } else {
                defArray((TypeArray) type, name, evaluator);
            }
        } else {
            coder.with {
                if(type instanceof TypeStructRef && type.toString() == 'FP') {
                    TypeStructRef ref = (TypeStructRef)type
                    ref.name = 'RandomAccessFile'
                }

                def modifier = mStreamIt.global() == s ? 'static ' : 'private '
                if (init == null) code("%s%s %s", modifier, type, name).EOL();
                else code("%s%s %s = %s", modifier, type, name, evaluator.eval(init)).EOL();
            }
        }
        return this;
    }

    @Override
    public Object visitStreamSpec(final StreamSpec spec) {
        coder.with {
            switch(mStream.basename()) {
                case 'AudioSource':
                case 'AccSource':
                    codeTemplate(type: spec.getStreamType().getOut().toString(), "${mStream.basename()}.gsp", true); break
                case 'FileReader':
                    codeTemplate(type: spec.getStreamType().getOut().toString(), "${mStream.basename()}.gsp"); break
                case 'FileSink':
                case 'TCPSink':
                case 'BaselineTCPSink':
                    codeTemplate(type: spec.getStreamType().getIn().toString(), "${mStream.basename()}.gsp"); break
                default:
                    def typeIn = mStream.getInputType()
                    def typeOut = mStream.getOutputType()
                    if(!mStream.is(mStreamIt.global())) {
                        if(profile.pkg) code("package $profile.pkg").EOL().newline()
                        code('import static java.lang.Math.*').EOL()
                        code('import java.io.*').EOL()
                        code('import esms.runtime.*').EOL()
                        code('import esms.util.*').EOL()
                        code("public class ${mStream.basename()} extends Filter ").blockStart()
                        // I/O channels
                        typeIn = mStream.isSource() ? 'Channel' : typeIn == TypePrimitive.inttype ? 'IntChannel' : 'FloatChannel'
                        typeOut = mStream.isSink() ? 'Channel' : typeOut == TypePrimitive.inttype ? 'IntChannel' : 'FloatChannel'
                        code("private $typeIn _in").EOL()
                        code("private $typeOut _out").EOL()
                        // Stream parameters
                        spec.getParams().each { param ->
                            if(param.type.toString() == 'string')
                                param.type = new TypeStructRef('String')
                            code("private ${param.type} ${param.name}").EOL()
                        }
                    }
                    spec.getVars().each { it.accept(this) };
                    if(!mStream.is(mStreamIt.global())) {
                        // Constructor
                        if (spec.params)
                            code("public ${mStream.basename()}(int id, Channel in, Channel out, ${encodeParams(evaluator(), spec.params)}) ").blockStart()
                        else
                            code("public ${mStream.basename()}(int id, Channel in, Channel out) ").blockStart()
                        code('super(id)').EOL()
                        code("_in = ($typeIn)in").EOL()
                        code("_out = ($typeOut)out").EOL()
                        spec.params.each { param ->
                            code("this.${param.name} = ${param.name}").EOL()
                        }
                        // Define array fields
                        spec.getVars().each { fields ->
                            (0..<fields.getNumFields()).each {
                                def type = fields.getType(it)
                                if(type instanceof TypeArray) {
                                    TypeArray t = (TypeArray)type
                                    int dims = t.getDims()
                                    code("this.${fields.getName(it)} = new ${t.getComponent()}")
                                    for (int i = 0; i < dims; i++) {
                                        code("[${t.getLength().accept(this)}]", false);
                                        if (i + 1 < dims)
                                            t = (TypeArray)t.getBase();
                                    }
                                    EOL()
                                }
                            }
                        }
                        if(spec.getInitFunc()) code('init()').EOL()
                        blockEnd()
                    }

                    spec.getFuncs().findAll {
                        it.getCls() != Function.FUNC_WORK && it.getCls() != Function.FUNC_PREWORK
                    }.each { it.accept(this) };
                    spec.getFuncs().findAll {
                        it.getCls() == Function.FUNC_WORK || it.getCls() == Function.FUNC_PREWORK
                    }.each {
                        if (it == spec.getWorkFunc())
                            it.accept(this);
                        else {
                            // prework
                            // it.accept(this);
                        }
                    }

                    if(!mStream.is(mStreamIt.global())) {
                        code('@Override').newline()
                        code('protected void barrier(int invocations) throws InterruptedException ').blockStart()
                        code('if(_in != null) _in.inputBarrier(invocations)').EOL()
                        code('if(_out != null) _out.outputBarrier(invocations)').EOL()
                        blockEnd()
                        code('@Override').newline()
                        code('protected void release() throws InterruptedException ').blockStart()
                        code('if(_in != null) _in.inputRelease()').EOL()
                        code('if(_out != null) _out.outputRelease()').EOL()
                        blockEnd()
                        blockEnd()
                    }
            }
        }
        return spec;
    }

    @Override
    public Object visitFieldDecl(FieldDecl field) {
        for(int i = 0; i < field.getNumFields(); i++)
            defField(mStream, field.getType(i), field.getName(i), field.getInit(i));
        return field;
    }

    @Override
    public Object visitFunction(Function func) {
        switch(func.getCls()) {
            case Function.FUNC_NATIVE: return func;
            case Function.FUNC_INIT:
//                Log.d(TAG, "StreamItCodeGenerator.visitFunction(): %s.init()\n", mStream);
                coder.code("private %s init(%s) ", func.getReturnType().toString(), encodeParams(evaluator(), func.getParams()));
                func.getBody().accept(this);
                break;
            default:
                func.getParams().each { param ->
                    evaluator().registerVar(param.getName(), param.getType(), evaluator().initialize(param.getType()), StreamItSymbolTable.KIND_FUNC_PARAM);
                }
                coder.code("${func.getReturnType()} ${func.getName()}(${encodeParams(evaluator(), func.getParams())})");
                func.getBody().accept(this);
        }
        return func;
    }

    @Override
    public Object visitFuncWork(FuncWork work) {
//        Log.i(TAG, "visitFuncWork(): %s_work()\n", mStream);
        coder.with {
            code("protected void doWork() ");
            Function func = (Function) work.accept(new StreamItFunctionGenerator(encoder, mConfig.steadySFG(), mStream));
            func.getBody().accept(this)
            return func
        }

    }

    @Override
    public Object visitExprVar(ExprVar var) {
//        Log.i(TAG, "StreamItCodeGenerator.visitExprVar(): " + var);
        if(evaluator().isRegistered(var)) {
            switch (evaluator().kind(var)) {
//                case SymbolTable.KIND_STREAM_PARAM:
////                    Log.i(TAG, "StreamItCodeGenerator.visitExprVar(): KIND_STREAM_PARAM " + var);
//                    return evaluator().getVar(var);
                case SymbolTable.KIND_FIELD:
//                    Log.i(TAG, "StreamItCodeGenerator.visitExprVar(): KIND_FIELD " + var + ", global? ${mStreamIt.global() == mStream}");
                    return new ExprVar(var.getContext(), encodeField(mStream, var.getName(), mStreamIt.global() == mStream));
                case SymbolTable.KIND_GLOBAL:
//                    Log.i(TAG, "StreamItCodeGenerator.visitExprVar(): KIND_GLOBAL " + var");
                    if(var.name == 'NULL')
                        return new ExprVar(var.getContext(), 'null')
                    return new ExprVar(var.getContext(), encodeField(mStream, var.getName(), true));
            }
        } else {
            if(var.name == 'stderr') return new ExprVar(var.getContext(), 'System.err')
            else Log.i(TAG, "JavaCodeGenerator.visitExprVar(): unregistered " + var);
        }
        return var;
    }

    public static String typeC(Type type) {
        if(type instanceof TypePrimitive) {
            TypePrimitive t = (TypePrimitive) type;
            switch (t.getType()) {
                case TypePrimitive.TYPE_STRING:
                    return "%s";
                case TypePrimitive.TYPE_CHAR:
                    return "%c";
                case TypePrimitive.TYPE_BIT:
                case TypePrimitive.TYPE_INT:
                    return "%d";
                case TypePrimitive.TYPE_FLOAT:
                case TypePrimitive.TYPE_DOUBLE:
                    return "%.20f";
//                    return "%.16f";
            }
        } else if(type instanceof TypeArray)
            return typeC(((TypeArray)type).getComponent());
        throw new RuntimeException("Unsupported type: " + type);

    }

    @Override
    public Object visitExprTernary(ExprTernary exp) {
        exp = (ExprTernary) super.visitExprTernary(exp);
        return new ExprTernary(exp.getContext(), exp.getOp() , exp.getA(), exp.getB(), exp.getC()) {
            @Override
            public String toString() {
                return "${getA()} ? ${getB()} : ${getC()}";
            }
        };
    }

    @Override
    public Object visitExprFunCall(ExprFunCall call) {
//        Log.d(TAG, "StreamItCodeGenerator.visitExprFunCall(): %s(%s)\n", call.getName(), call.getParams());
        String name = call.getName();
        boolean cast = false;
        if(name.equals("println")) {
            def expressions = evaluator().decompose((Expression) call.getParams().get(0), ExprBinary.BINOP_ADD);
            def args = [] << new ExprConstStr(null, "\"" + expressions.collect { it -> typeC(evaluator().getType(it)) }.join("") + "\\n\"")
            args.addAll(expressions)
            name = "Log.i";
            args = [new ExprConstStr(null, "\"${mStream.name()}\""), *args]
            call = new ExprFunCall(call.getContext(), name, args)
        } else if(name.equals("sin") || name.equals("cos") || name.equals("atan") || name.equals("exp") || name.equals("log") || name.equals("sqrt") || name.equals("pow") || name.equals("abs")) {
            // FIXME StreamIt only supports float but Java only provides double versions
            cast = true;
//        } else if(name == 'fprintf' || name == 'fopen' || name == 'fclose' || name == 'fread' || name == 'rewind' || name == 'feof' || name == 'exit' || name == 'sizeof') {
//            switch(name) {
//                case 'fprintf':
//                    return new ExprFunCall(call.getContext(), 'System.err.printf', call.getParams().subList(1, call.getParams().size()))
//                case 'fopen':
//                    return new ExprFunCall(call.getContext(), 'new RandomAccessFile', call.getParams())
//                case 'fclose':
//                    return new ExprFunCall(call.getContext(), "${call.getParams().get(0)}.close", Collections.emptyList())
//                case 'fread':
//                    def file = call.getParams().get(3)
//                    def buf = call.getParams().get(0)
//                    def sz = call.getParams().get(1)
//                    def count = call.getParams().get(2)
//                    if(count instanceof ExprBinary) {
//                        ExprBinary exp = (ExprBinary)count
//                        Expression length = exp.getLeft().accept(this)
//                        return new ExprFunCall(call.getContext(), "${file}.read", [buf, new ExprConstInt(0), length])
//                    } else
//                        return call
//                case 'rewind':
//                    return new ExprFunCall(call.getContext(), "${call.getParams().get(0)}.seek", [new ExprConstInt(0)])
//                case 'feof':
//                    def file = call.getParams().get(0)
//                    return new ExprBinary(call.getContext(), ExprBinary.BINOP_EQ, new ExprFunCall(null, "${file}.getFilePointer", []), new ExprFunCall(null, "${file}.length", []))
//                case 'sizeof':
//                    return new ExprVar(null, "${call.getParams().get(0)}.length")
//                case 'exit':
//                    return new ExprFunCall(call.getContext(), 'System.exit', call.getParams())
//            }
        } else if(!call.getName().startsWith("push") && !call.getName().startsWith("peek") && !call.getName().startsWith("pop")) {
            for (Function func: mStream.spec().getFuncs()) {
                if (func.getName() != null && func.getName().equals(call.getName())) {
                    call = new ExprFunCall(call.getContext(), encodeFunction(mStream, func), call.getParams());
                    break;
                }
            }
        }

        if(cast)
            return new ExprTypeCast(call.getContext(), TypePrimitive.floattype, super.visitExprFunCall(call))
        else
            return super.visitExprFunCall(call)
    }

    /**
     * Allow evaluators to register variables so as to determine printf format string.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtVarDecl(StmtVarDecl stmt) {
//        Log.i(TAG, "StreamItCodeGenerator.visitStmtVarDecl(): %s(%d) in stream %s\n", stmt, stmt.getNumVars(), mStream);
        stmt.accept(evaluator());
        stmt = (StmtVarDecl) super.visitStmtVarDecl(stmt);
        coder.with {
            for (int i = 0; i < stmt.getNumVars(); i++) {
                if (i > 0) code("; ", false);
                Type type = stmt.getType(i);
                if (type instanceof TypeArray) {
                    // FEReplacer forgot to replace TypeArray!!!
                    TypeArray t = (TypeArray)type
                    int dims = t.getDims()
                    code("${t.getComponent()}${"[]" * t.getDims()} ${stmt.getName(i)} = new ${t.getComponent()}", false);
                    dims.times { d ->
                        code("[${t.getLength().accept(this)}]", false);
                        if (d + 1 < dims)
                            t = (TypeArray)t.getBase();
                    }
                } else {
                    code("$type ${stmt.getName(i)}", false);
                }

                if (stmt.getInit(i) != null)
                    code(" = " + stmt.getInit(i), false);
            }
        }
        return stmt;
    }

    @Override
    public Object visitStmtAssign(StmtAssign stmt) {
//        Log.i(TAG, "StreamItCodeGenerator.visitStmtAssign(): " +  stmt);
        stmt = (StmtAssign) super.visitStmtAssign(stmt);
        coder.code(stmt.toString(), false);
        return stmt;
    }

    @Override
    public Object visitStmtReturn(StmtReturn ret) {
        coder.code("return " + (ret.getValue() ? ret.getValue().accept(this) : ''), false);
        return ret;
    }

    @Override
    public Object visitStmtContinue(StmtContinue ret) {
        coder.code(ret.toString(), false);
        return ret;
    }

    @Override
    public Object visitStmtBreak(StmtBreak ret) {
        coder.code(ret.toString(), false);
        return ret;
    }

    @Override
    public Object visitStmtExpr(StmtExpr stmt) {
        coder.with {
            stmt = (StmtExpr) super.visitStmtExpr(stmt);
            code(stmt.toString(), false);
        }
        return stmt;
    }

    @Override
    public Object visitStmtBlock(StmtBlock block) {
        coder.with {
            blockStart();
            block.getStmts().each { stmt ->
                indent();
                stmt.accept(this);
                if (stmt instanceof StmtFor) {
                } else if (stmt instanceof StmtIfThen) {
                } else if (stmt instanceof StmtBlock) {
                } else {
                    EOL();
                }
            }
            blockEnd();
        }
        return block;
    }

    @Override
    public Object visitStmtIfThen(StmtIfThen stmt) {
        coder.with {
            code("if(", false);
            Expression newCond = (Expression) stmt.getCond().accept(this);
            code(newCond.toString(), false).code(") ", false);

            Statement newCons = stmt.getCons();
            if (newCons == null) EOL();
            else newCons = (Statement) stmt.getCons().accept(this);
            if (!(newCons instanceof StmtBlock)) EOL();

            Statement newAlt = stmt.getAlt();
            if (newAlt != null) {
                code("else ");
                newAlt = (Statement) newAlt.accept(this);
                if (!(newAlt instanceof StmtBlock)) EOL();
            }
        }
        return stmt;

    }

    /**
     * Code the for loop while visiting.
     * Always evaluate each statement to replace stream params if any.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtFor(StmtFor stmt) {
        coder.with {
            code("for(", false);
            Statement init = stmt.getInit();
            if (init != null)
                init.accept(this);
            end();

            ExprBinary cond = (ExprBinary) stmt.getCond();
            code(" " + cond.accept(this).toString(), false).end().code(" ", false);
            Statement incr = stmt.getIncr();
            incr.accept(this);
            code(") ", false);

            Statement body = stmt.getBody();
            body.accept(this);
            if (!(body instanceof StmtBlock)) EOL();
        }
        return stmt;
    }

    /**
     * Code the while loop while visiting.
     * Always evaluate each statement to replace stream params if any.
     *
     * @param stmt
     * @return
     */
    @Override
    public Object visitStmtWhile(StmtWhile stmt) {
        coder.with {
            code("while(" + stmt.getCond().accept(this).toString() + ") ", false);
            Statement body = stmt.getBody();
            body.accept(this);
            if (!(body instanceof StmtBlock)) EOL();
        }
        return stmt;
    }
}