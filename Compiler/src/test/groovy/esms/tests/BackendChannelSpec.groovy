package esms.tests

import spock.lang.Ignore
import spock.lang.Specification

class BackendChannelSpec extends Specification {
    def "java backend channel test suite"() {
        expect:
        assert 1 + 1 == 2
    }
//    @Ignore
//    def "java backend channel test suite"() {
//        setup:
//        def ch = new IntChannel(64)
//        expect:
//        assert ch.size() == 0
//        assert ch.capacity() == 64
//
//        when:
//        ch.push(10)
//        then:
//        assert ch.capacity() == 64
//        assert ch.size() == 1
//
//        when:
//        ch.push(5);
//        then:
//        assert ch.size() == 2
//        assert ch.peek(0) == 10
//        assert ch.peek(1) == 5
//        assert ch.pop() == 10
//        assert ch.size() == 1
//        assert ch.pop() == 5
//        assert ch.isEmpty()
//
//        when:
//        ch.clear()
//        0.upto(5) {
//            ch.push(it)
//        }
//        then:
//        0.upto(2) {
//            assert ch.pop() == it
//        }
//        assert ch.head() == 6
//        assert ch.tail() == 3
//        assert ch.size() == 3
//
//        when:
//        ch.flip()
//        then:
//        assert ch.head() == 3
//        assert ch.tail() == 0
//        assert ch.size() == 3
//    }
}