import matplotlib.pyplot as plt

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.itervalues():
        sp.set_visible(False)

fig, host = plt.subplots()
fig.subplots_adjust(right=0.75)
fig_params = {'backend': 'ps',
        'axes.labelsize': 11,
        'text.fontsize': 18,
        'legend.fontsize': 16,
        'xtick.labelsize': 18,
        'ytick.labelsize': 14,
        'text.usetex': False,
        'ps.useafm' : True,
        'pdf.use14corefonts' : True    
        }

plt.rcParams['pdf.fonttype']=42
plt.rcParams.update(fig_params)


par1 = host.twinx()

p1, = host.plot([1, 16, 32, 64], [11.89, 11.13, 10.3, 9.7], "b-", label="AoC Util", marker='o')
p2, = host.plot([1, 16, 32, 64], [22.4, 13.14, 12.16, 11.3], "g-", label="StreamIt Util", marker='o')
p5, = host.plot([1, 32, 32, 64], [22.4, 22.4, 9.7, 9.7], "k--", label="Util Reduction")
p3, = par1.plot([1, 16, 32, 64], [31.014912, 31.014912, 31.014912, 31.232000], "r-", label="AoC RSS", marker='p')
p4, = par1.plot([1, 16, 32, 64], [31.805440, 32.563200, 32.690176, 33.243136], "y-", label="StreamIt RSS", marker='p')

host.set_xlim(0, 65)
host.set_ylim(3, 23)
par1.set_ylim(28.000000, 36.250000)

host.set_xlabel("Crowd Buffer Scaling")
host.set_ylabel("CPU Util(%)")
par1.set_ylabel("RSS(MB)")

host.yaxis.label.set_color('k')
par1.yaxis.label.set_color('k')

tkw = dict(size=4, width=1.5)
host.tick_params(axis='y', colors='k', **tkw)
par1.tick_params(axis='y', colors='k', **tkw)
host.tick_params(axis='x', **tkw)

lines = [p1, p2, p3, p4, p5]

host.legend(lines, [l.get_label() for l in lines])
plt.draw()
plt.show()
