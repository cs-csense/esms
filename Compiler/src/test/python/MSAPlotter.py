#!/usr/bin/env python

from collections import defaultdict
from subprocess import call
import sys, os, getopt
from glob import glob

REMOTE_ESMS = "/sdcard/Download/esms";
REMOTE_STREAMIT = "/sdcard/Download/streamit";

TOOLKITS = ["ESMS", "StreamIt"];
SCALINGS = [1, 16, 32, 64];
MSAs = ["BeepBeepA", "SpeakerIdentifier", "CrowdA"];
MSBs = ["AutoCor", "BitonicSort", "FIRcoarse", "FIR", "FFT2", "FFT3", "FMRadio", "MatrixMult", "MatrixBlockMult", "MergeSort", "Repeater", "BeepBeep", "MFCC", "Crowd"];

tree = lambda: defaultdict(tree)
MSA = tree();
MSB = tree();

# MSAs[tk][name][cacheopt][scaling] = {}
# MSBs[tk][name][cacheopt] = {}

def local(tk):
    return "../build/" + tk;

def path(tk, name, cacheopt, scaling):
    if(cacheopt):
        name += "Cacheopt";
    if(scaling is None):
        return local(tk) + os.path.sep + name;
    else:
        return local(tk) + os.path.sep + name + os.path.sep + str(scaling)

def loadPerf(tk, name, cacheopt, scaling = None):
    log = path(tk, name, cacheopt, scaling) + os.path.sep + "performance.log";
    if(not os.path.exists(log)): return;
    for line in open(log):
        key, val = line.partition(": ")[::2];
        if(val != "" and key != "summary"):
            val = val.split()[0];
            if(val.endswith("%")): val = val[0:-1];
            val = float(val);
            if(scaling is None):
                MSB[tk][name][cacheopt][key] = val;
            else:
                MSA[tk][name][cacheopt][scaling][key] = val;
    
def loadEnergy(tk, name, cacheopt, scaling = None):
    csv = path(tk, name, cacheopt, scaling) + os.path.sep + "energy.csv";
    if(not os.path.exists(csv)): return;
    found = False;
    duration = 0;
    power = 0;
    load = 0;
    for line in open(csv):
        if(found):
            fields = line.split(",");
            if(fields[1].strip("\"").startswith("Battery Power")):
                power = float(fields[3]);
            elif(fields[1].strip("\"").startswith("CPU Load - Normalized")):
                load = float(fields[3]);
        if(line.startswith("App State")):
            [start, end] = line.split(",")[-2:];
            start = float(start.split(": ")[-1][:-2]);
            end = float(end.split(": ")[-1][:-2]);
            if(end - start >= 60):
                found = True;
                duration = end - start;
            else:
                found = False;

    if(scaling is None):
        MSB[tk][name][cacheopt]["load"] = load;                         # %
        MSB[tk][name][cacheopt]["power"] = power;                       # uW
        MSB[tk][name][cacheopt]["energy"] = power * duration / 3600000; # mWh
        MSB[tk][name][cacheopt]["duration"] = duration;                 # s
    else:
        MSA[tk][name][cacheopt][scaling]["load"] = load;                            # %
        MSA[tk][name][cacheopt][scaling]["power"] = power;                          # uW
        MSA[tk][name][cacheopt][scaling]["energy"] = power * duration / 3600000;    # mWh
        MSA[tk][name][cacheopt][scaling]["duration"] = duration;                    # s

def loadMSA(tk, name, cacheopt, scaling):
    loadPerf(tk, name, cacheopt, scaling);
    loadEnergy(tk, name, cacheopt, scaling);

def loadMSB(tk, name, cacheopt):
    loadPerf(tk, name, cacheopt);
    loadEnergy(tk, name, cacheopt);

def loadMSAs():
    for tk in TOOLKITS:
        for msa in MSAs:
            for cacheopt in [True, False]:
                for scaling in SCALINGS:
                    loadMSA(tk, msa, cacheopt, scaling);
                    #printMSA(tk, msa, cacheopt, scaling);

def loadMSBs():
    for tk in TOOLKITS:
        for msb in MSBs:
            for cacheopt in [True, False]:
                loadMSB(tk, msb, cacheopt);

def printMSA(tk, msa, cacheopt, scaling):
    data = MSA[tk][msa][cacheopt][scaling];
    if(not data): return;
    keys = ['user', 'util', 'load', 'energy', 'rss'];
    if(cacheopt): msa += "Cacheopt";
    print "===== %s/%s/%d ======" % (tk, msa, scaling);
    for key in keys:
        print "%s: %f" % (key, data[key]);

def printMSB(tk, msb, cacheopt):
    data = MSB[tk][msb][cacheopt];
    if(not data): return;
    keys = ['user', 'util', 'load', 'energy', 'rss'];
    if(cacheopt): msb += "Cacheopt";
    print "===== %s/%s ======" % (tk, msb);
    for key in keys:
        print "%s: %f" % (key, data[key]);

def MSAValues(tk, name, cacheopt, key):
    return [ MSA[tk][name][cacheopt][scaling][key] for scaling in SCALINGS ];

def mkdirs(path):
    try: 
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

def plotMSA(msa):
    mkdirs(local("fig")); 
    pdf = PdfPages('%s/%s-energy.pdf' % (local("fig"), msa));
    fig, host = plt.subplots();
    fig.subplots_adjust(right=0.75)
    fig_params = {'backend': 'ps',
        'axes.labelsize': 11,
        'text.fontsize': 18,
        'legend.fontsize': 16,
        'xtick.labelsize': 18,
        'ytick.labelsize': 14,
        'text.usetex': False,
        'ps.useafm' : True,
        'pdf.use14corefonts' : True    
    }

    plt.rcParams['pdf.fonttype'] = 42
    plt.rcParams.update(fig_params)

    # X: buffer scaling
    # Y: user | util | load | energy | RSS
    lines = [];
    color = iter(cm.rainbow(np.linspace(0,1,4)))
    p1, = host.plot(SCALINGS, MSAValues(TOOLKITS[0], msa, False, 'energy'), c=next(color), label="%s AoC" % msa, marker='o');
    p2, = host.plot(SCALINGS, MSAValues(TOOLKITS[1], msa, False, 'energy'), c=next(color), label="%s %s" % (msa, TOOLKITS[1]), marker='o');
    lines.append(p1);
    lines.append(p2);
    if(msa == "BeepBeepA"):
        p, = host.plot(SCALINGS, MSAValues(TOOLKITS[1], msa, True, 'energy'), c=next(color), label="%sCacheopt" % msa, marker='o');
        lines.append(p);

    host.set_xlim(0, 65)
    host.set_ylim(30, 80)
    host.set_xlabel("Buffer Scaling");
    host.set_ylabel("Energy(mWh)");
    host.yaxis.label.set_color('k')
    tkw = dict(size=4, width=1.5)
    host.tick_params(axis='y', colors='k', **tkw)
    host.tick_params(axis='x', **tkw)
    host.legend(lines, [l.get_label() for l in lines])
    #plt.draw()
    #plt.show()
    pdf.savefig();
    pdf.close();

def plotMSAs():
    for msa in MSAs:
        plotMSA(msa);

def plotMSBs():
    # X: benchmarks
    # Y: energy
    print;

def help(code):
    print 'MSAPlotter.py [-i]'
    sys.exit(code)

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hi", ["import"])
    except getopt.GetoptError:
        print 'MSAPlotter.py [-i]'
        sys.exit(1)
    for opt, arg in opts:
        if opt in ("-i", "--import"):
            call(["adb", "pull", REMOTE_ESMS, local("ESMS")]);
            call(["adb", "pull", REMOTE_STREAMIT, local("StreamIt")]);
        elif opt in ("-h"):
            help(0);
        else:
            help(1);

    loadMSAs();
    loadMSBs();
    plotMSAs();
    plotMSBs();

if __name__ == "__main__":
    main(sys.argv[1:])
