import matplotlib.pyplot as plt

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.itervalues():
        sp.set_visible(False)

fig, host = plt.subplots()
fig.subplots_adjust(right=0.75)
fig_params = {'backend': 'ps',
        'axes.labelsize': 11,
        'text.fontsize': 18,
        'legend.fontsize': 16,
        'xtick.labelsize': 18,
        'ytick.labelsize': 14,
        'text.usetex': False,
        'ps.useafm' : True,
        'pdf.use14corefonts' : True    
        }

plt.rcParams['pdf.fonttype']=42
plt.rcParams.update(fig_params)


par1 = host.twinx()

p1, = host.plot([1, 16, 32, 64], [42.66, 43.54, 42.33, 41.53], "b-", label="AoC Util", marker='o')
p2, = host.plot([1, 16, 32, 64], [54.44, 56.02, 54.78, 54.26], "g-", label="StreamIt Util", marker='o')

p3, = par1.plot([1, 16, 32, 64], [30.756864, 30.9824144, 31.272960, 31.825920], "r-", label="AoC RSS", marker='p')
p4, = par1.plot([1, 16, 32, 64], [31.137792, 31.850496, 32.137216, 32.210944], "y-", label="StreamIt RSS", marker='p')

p5, = host.plot([16, 16, 64, 64], [56.02, 37.75, 37.75, 41.53], "k--", label="Util Reduction")


#ax = fig.add_subplot(111)
#bbox_props = dict(boxstyle="round,pad=0.3", fc="w", ec="k", lw=2)
#t = ax.text(32, 16, "12.7% Reduction", ha="center", va="center", rotation=0, size=15, bbox=bbox_props)

host.set_xlim(0, 65)
host.set_ylim(32, 58)
par1.set_ylim(28.000000, 45.00000)

host.set_xlabel("BeepBeep Buffer Scaling")
host.set_ylabel("CPU Util(%)")
par1.set_ylabel("RSS(MB)")

host.yaxis.label.set_color('k')
par1.yaxis.label.set_color('k')

tkw = dict(size=4, width=1.5)
host.tick_params(axis='y', colors='k', **tkw)
par1.tick_params(axis='y', colors='k', **tkw)
host.tick_params(axis='x', **tkw)

lines = [p1, p2, p3, p4, p5]

host.legend(lines, [l.get_label() for l in lines])
plt.draw()
plt.show()
