#!/usr/bin/python

from os.path import expanduser
from collections import defaultdict
from subprocess import call
import sys, os, getopt
from glob import glob

ENERGY_ESMS_A15 =  expanduser("~/Dropbox/Apps/DynaGrid.NET/ESMS/MSB/energy-esms-A15.log");
ENERGY_ESMS_A7 = expanduser("~/Dropbox/Apps/DynaGrid.NET/ESMS/MSB/energy-esms-A7.log");
#ENERGY_DVFS_CPU = expanduser("~/Dropbox/Apps/DynaGrid.NET/ESMS/MSB/energy-dvfs-cpu.log");
ENERGY_DVFS_MFCC = expanduser("~/Dropbox/Apps/DynaGrid.NET/ESMS/MSB/energy-dvfs-mfcc-esms-aoc.log");
#ENERGY_DVFS_CPU = expanduser("~/Dropbox/Apps/DynaGrid.NET/ESMS/MSB/energy-dvfs-mfcc-esms-aoc-usleep-50ms.log");
ENERGY_DVFS_SPI = expanduser("~/Dropbox/Apps/DynaGrid.NET/ESMS/MSB/energy-dvfs-mfcc-esms-aoc-9000-300-100ms.log");
ENERGY_DVFS_MEM_SPI = expanduser("~/Dropbox/Apps/DynaGrid.NET/ESMS/MSB/energy-dvfs-mem.log");
LOGS = [ENERGY_ESMS_A15, ENERGY_ESMS_A7, ENERGY_DVFS_MFCC, ENERGY_DVFS_SPI];
BENCHMARKS = { LOGS[0]: "ESMS A15", LOGS[1]: "ESMS A7", LOGS[2]: "MFCC ESMS AoC", LOGS[3]: "SpeakerIdentifier ESMS AoC"}

# StreamIt AoC IP Cacheopt

def mkdirs(path):
    try: 
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise

from matplotlib.backends.backend_pdf import PdfPages
from math import ceil
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from plot import barchart

cfgs = {"ESMSAA":"AA", "ESMSAoC":"AoC", "ESMSIP":"IP", "StreamIt":"StreamIt", "StreamItCacheopt":"Cacheopt"};
cfgs = {"ESMSAoC":"AoC", "StreamIt":"StreamIt", "StreamItCacheopt":"Cacheopt"};
def plotSpeedup(core, times):
    benchmarks = sorted(times);
    benchmarks.remove("BitonicSortRecursive");
    benchmarks.remove("FIRcoarse");
    benchmarks.remove("BeepBeep");
    #benchmarks.remove("MFCC");
    benchmarks.remove("Crowd");
    benchmarks.append("BeepBeep");
    #benchmarks.append("MFCC");
    benchmarks.append("Crowd");
    AoC = [];
    IP = [];
    Cacheopt = [];
    for benchmark in benchmarks:
        baseline = times[benchmark]["StreamIt"];
        AoC.append(baseline / times[benchmark]["AoC"]);
        #IP.append(baseline / times[benchmark]["IP"]);
        if("Cacheopt" in times[benchmark]):
            Cacheopt.append(baseline / times[benchmark]["Cacheopt"]);
        else:
            Cacheopt.append(0);

    dataset = [AoC, Cacheopt];
    legends = ('AoC', 'Cacheopt');
    barchart(dataset, benchmarks, legends, 'Speedup', '../build/fig/esms-speedup-%s.pdf' % core, True);

def plotEnergy(core, energies):
    benchmarks = sorted(energies);
    benchmarks.remove("BitonicSortRecursive");
    benchmarks.remove("FIRcoarse");
    benchmarks.remove("BeepBeep");
    #benchmarks.remove("MFCC");
    benchmarks.remove("Crowd");
    benchmarks.append("BeepBeep");
    #benchmarks.append("MFCC");
    benchmarks.append("Crowd");
    AoC = [];
    #IP = [];
    StreamIt = [];
    Cacheopt = [];
    for benchmark in benchmarks:
        baseline = energies[benchmark]["StreamIt"];
        AoC.append(energies[benchmark]["AoC"]);
        #IP.append(energies[benchmark]["IP"]);
        StreamIt.append(baseline);
        if("Cacheopt" in energies[benchmark]):
            Cacheopt.append(energies[benchmark]["Cacheopt"]);
        else:
            Cacheopt.append(0);

    dataset = [AoC, StreamIt, Cacheopt];
    legends = ('AoC', 'StreamIt', 'Cacheopt');
    barchart(dataset, benchmarks, legends, 'Energy Consumption (mWh)', '../build/fig/esms-energy-%s.pdf' % core);

def plotEDP(core, times, energies):
    benchmarks = sorted(energies);
    benchmarks.remove("BitonicSortRecursive");
    benchmarks.remove("FIRcoarse");
    benchmarks.remove("BeepBeep");
    #benchmarks.remove("MFCC");
    benchmarks.remove("Crowd");
    benchmarks.append("BeepBeep");
    #benchmarks.append("MFCC");
    benchmarks.append("Crowd");
    AoC = [];
    #IP = [];
    StreamIt = [];
    Cacheopt = [];
    for benchmark in benchmarks:
        baselineTime = times[benchmark]["StreamIt"];
        baselineEnergy = energies[benchmark]["StreamIt"];
        AoC.append(energies[benchmark]["AoC"] / baselineEnergy * times[benchmark]["AoC"] / baselineTime);
        #IP.append(energies[benchmark]["IP"] / baselineEnergy * times[benchmark]["IP"] / baselineTime);
        if("Cacheopt" in energies[benchmark]):
            Cacheopt.append(energies[benchmark]["Cacheopt"] / baselineEnergy * times[benchmark]["Cacheopt"] / baselineTime);
        else:
            Cacheopt.append(0);

    dataset = [AoC, Cacheopt];
    legends = ('AoC', 'Cacheopt');
    barchart(dataset, benchmarks, legends, 'Energy Delay Product (EDP)', '../build/fig/esms-edp-%s.pdf' % core, True);

def plotMSBOdroid():
    tree = lambda: defaultdict(tree);
    times = tree();
    energies = tree();
    for log in LOGS[0:2]:
        if(not os.path.isfile(log)): continue;
        lines = open(log).readlines();
        for i in range(0, len(lines), 6):
            cmd = lines[i].strip().split("/")[1];
            benchmark = ""
            cfg = ""
            for key in cfgs.keys():
                if(cmd.endswith(key)):
                    benchmark = cmd[0:len(cmd)-len(key)];
                    cfg = cfgs[key];
                    break;
            if(benchmark == ""): continue;
            if(benchmark == "MatrixMultBlock"): continue;
            if(benchmark == "MFCC"): continue;
            time = float(lines[i+2].split()[-1][0:-1]);
            energy = float(lines[i+5].split()[1][0:-4]) + float(lines[i+5].split()[5][0:-4]);
            times[benchmark][cfg] = time
            energies[benchmark][cfg] = energy
            print "%s %s costs %.2fmWh in %.2fs" % (benchmark, cfg, energy, time);
        if(log.endswith("A15.log")):
            core = "A15";
        else:
            core = "A7";
        plotSpeedup(core, times);
        plotEnergy(core, energies);
        plotEDP(core, times, energies);

def plotDVFSETW(benchmark, times, energies):
    mkdirs("../build/fig"); 
    pdf = PdfPages('../build/fig/dvfs-etw-%s.pdf' % benchmark.lower().replace(' ', '-'));
    fig, host = plt.subplots();
    fig.subplots_adjust(right=0.75)
    fig_params = {'backend': 'ps',
        'axes.labelsize': 14,
        'text.fontsize': 18,
        'legend.fontsize': 14,
        'xtick.labelsize': 14,
        'ytick.labelsize': 14,
        'text.usetex': False,
        'ps.useafm' : True,
        'pdf.use14corefonts' : True    
    }
    plt.rcParams['pdf.fonttype'] = 42
    plt.rcParams.update(fig_params)

    CORES = ["A7", "A15"];
    A7FS = [800, 1000, 1200, 1400]; 
    A7FS = range(min(times["A7"].keys()), max(times["A7"].keys()) + 200, 200);
    A15FS = [400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000];
    A15FS = range(min(times["A15"].keys()), max(times["A15"].keys()) + 200, 200);

    # X: times
    # Y: energy
    # line: workload
    #WORKLOAD = [1,2,3,4,5,6,7];
    WORKLOAD = [1,7];

    lines = [];
    color = iter(cm.rainbow(np.linspace(0,1,len(CORES)*len(WORKLOAD))));
    for core in CORES:
        FS = A7FS;
        if(core == "A15"): FS = A15FS;
        for wl in WORKLOAD:
            E = [ energies[core][fs][wl] for fs in FS ];
            T = [ times[core][fs][wl] for fs in FS ];
            #freq = str(fs) + "MHz";
            #if(fs >= 1000):
            #    freq = str(fs / 1000.0) + "GHz";
            p, = host.plot(T, E, c=next(color), label="%s@WL%d" % (core, wl), marker='o');
            lines.append(p);

    host.set_title(benchmark);
    host.set_xlabel("Time (s)");
    host.set_ylabel("Energy (mWh)");
    host.yaxis.label.set_color('k')
    tkw = dict(size=4, width=1.5)
    host.tick_params(axis='y', colors='k', **tkw)
    host.tick_params(axis='x', **tkw)
    host.legend(lines, [l.get_label() for l in lines], loc="center left", bbox_to_anchor=(1, 0.5))
    pdf.savefig();
    pdf.close();

def plotDVFSEDP(benchmark, times, energies):
    mkdirs("../build/fig"); 
    pdf = PdfPages('../build/fig/dvfs-edp-%s.pdf' % benchmark.lower().replace(' ', '-'));
    fig, host = plt.subplots();
    fig.subplots_adjust(right=0.75)
    fig_params = {'backend': 'ps',
        'axes.labelsize': 14,
        'text.fontsize': 18,
        'legend.fontsize': 14,
        'xtick.labelsize': 14,
        'ytick.labelsize': 14,
        'text.usetex': False,
        'ps.useafm' : True,
        'pdf.use14corefonts' : True    
    }
    plt.rcParams['pdf.fonttype'] = 42
    plt.rcParams.update(fig_params)

    WORKLOAD = [1,2,3,4,5,6,7];
    CORES = ["A7", "A15"];
    #CORES = ["A15"];
    A7FS = [800, 1000, 1200, 1400]; 
    A7FS = range(min(times["A7"].keys()), max(times["A7"].keys()) + 200, 200);
    A15FS = [400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000];
    A15FS = range(min(times["A15"].keys()), max(times["A15"].keys()) + 200, 200);
    #A15FS = [800, 1000, 2000];
    lines = [];
    ylim = 0;
    color = iter(cm.rainbow(np.linspace(0,1,len(A7FS)+len(A15FS))));
    for core in CORES:
        FS = A7FS;
        if(core == "A15"): FS = A15FS;
        for fs in FS:
            if(fs not in times[core]): continue;
            E = [ energies[core][fs][wl] for wl in WORKLOAD ];
            D = [ times[core][fs][wl] for wl in WORKLOAD ];
            P = [ e * d for e, d in zip(E, D) ];
            ylim = max(ylim, max(P));
            freq = str(fs) + "MHz";
            if(fs >= 1000):
                freq = str(fs / 1000.0) + "GHz";
            p, = host.plot(WORKLOAD, P, c=next(color), label="%s@%s" % (core, freq), marker='o');
            lines.append(p);

    host.set_title(benchmark);
    #host.set_xlim(0, 8);
    #host.set_ylim(0, ylim);
    host.set_xlabel("Workload");
    host.set_ylabel("EDP");
    host.yaxis.label.set_color('k')
    tkw = dict(size=4, width=1.5)
    host.tick_params(axis='y', colors='k', **tkw)
    host.tick_params(axis='x', **tkw)
    host.legend(lines, [l.get_label() for l in lines], loc="center left", bbox_to_anchor=(1, 0.5))
    pdf.savefig();
    pdf.close();

def plotDVFS(benchmark, ylabel, dataset):
    mkdirs("../build/fig"); 
    pdf = PdfPages('../build/fig/dvfs-%s-%s.pdf' % (ylabel.lower().replace(' ', '-'), benchmark.lower().replace(' ','-')));
    fig, host = plt.subplots();
    fig.subplots_adjust(right=0.75)
    fig_params = {'backend': 'ps',
        'axes.labelsize': 14,
        'text.fontsize': 18,
        'legend.fontsize': 14,
        'xtick.labelsize': 14,
        'ytick.labelsize': 14,
        'text.usetex': False,
        'ps.useafm' : True,
        'pdf.use14corefonts' : True    
    }
    plt.rcParams['pdf.fonttype'] = 42
    plt.rcParams.update(fig_params)

    WORKLOAD = [1,2,3,4,5,6,7];
    CORES = ["A7", "A15"];
    A7FS = [800, 1000, 1200, 1400]; 
    A7FS = range(min(dataset["A7"].keys()), max(dataset["A7"].keys()) + 200, 200);
    A15FS = [400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000];
    A15FS = range(min(dataset["A15"].keys()), max(dataset["A15"].keys()) + 200, 200);
    #A15FS = [800, 1000, 2000];
    lines = [];
    color = iter(cm.rainbow(np.linspace(0,1,len(A7FS)+len(A15FS))));
    for core in CORES:
        FS = A7FS;
        if(core == "A15"): FS = A15FS;
        for fs in FS:
            values = [ dataset[core][fs][wl] for wl in WORKLOAD ];
            freq = str(fs) + "MHz";
            if(fs >= 1000):
                freq = str(fs / 1000.0) + "GHz";
            p, = host.plot(WORKLOAD, values, c=next(color), label="%s@%s" % (core, freq), marker='o');
            lines.append(p);

    host.set_title(benchmark);
    host.set_xlim(0, 8);
    host.set_xlabel("Workload");
    host.set_ylabel(ylabel);
    host.yaxis.label.set_color('k')
    tkw = dict(size=4, width=1.5)
    host.tick_params(axis='y', colors='k', **tkw)
    host.tick_params(axis='x', **tkw)
    host.legend(lines, [l.get_label() for l in lines], loc="upper left")
    pdf.savefig();
    pdf.close();

def plotDVFSOdroid():
    tree = lambda: defaultdict(tree);
    for log in LOGS[2:]:
        if(not os.path.isfile(log)): continue;
        times = tree();
        energies = tree();
        lines = open(log).readlines();
        for i in range(0, len(lines), 8):
            tokens = lines[i].strip().split();
            (core, fs) = tokens[3].split("@");
            fs = int(fs[0:-3]);
            wl = int(tokens[6]);
            time = float(lines[i+4].split()[-1][0:-1]);
            energy = float(lines[i+7].split()[1][0:-4]) + float(lines[i+7].split()[5][0:-4]);
            times[core][fs][wl] = time;
            energies[core][fs][wl] = energy;
            print "%s@%dMHz w/ WL %d costs %.2fmWh in %.2fs" % (core, fs, wl, energy, time);
        if(log in BENCHMARKS):
            plotDVFS(BENCHMARKS[log], "Time (s)", times);
            plotDVFS(BENCHMARKS[log], "Energy Consumption (mWh)", energies);
            plotDVFSEDP(BENCHMARKS[log], times, energies);
            plotDVFSETW(BENCHMARKS[log], times, energies);

def main(argv):
    plotMSBOdroid();
    #plotDVFSOdroid();

if __name__ == "__main__":
    main(sys.argv[1:])
