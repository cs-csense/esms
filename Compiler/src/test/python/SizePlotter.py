#!/usr/bin/python

#print (os.popen("ssh -p 2222 odroid@128.255.45.245 'size Documents/dev/ESMS/MSB/build/MatrixMultBlockStreamIt'").read())
#   text    data     bss     dec     hex filename
#  40233     590   44368   85191   14cc7 Documents/dev/ESMS/MSB/build/MatrixMultBlockStreamIt

from os.path import expanduser
from collections import defaultdict
from subprocess import call
import sys, os, getopt
from glob import glob

TK = ["ESMSAoC", "ESMSIP", "StreamIt", "StreamItCacheopt"];
#TK = ["ESMSAoC", "StreamIt", "StreamItCacheopt"];
CFG = ["AoC", "IP", "StreamIt", "Cacheopt"];
#CFG = ["AoC", "StreamIt", "Cacheopt"];
MSB = ["AutoCor", "BitonicSort", "FIRcoarse", "FIR", "FFT2", "FFT3", "FMRadio", "MatrixMult", "MatrixMultBlock", "MergeSort", "Repeater", "BeepBeep", "MFCC", "Crowd"];
MSB = ["AutoCor", "BitonicSort", "FFT2", "FFT3", "FIR", "FMRadio", "MatrixMult", "MergeSort", "Repeater", "BeepBeep", "Crowd"];
#MSB = ["MatrixMultBlock", "Crowd"];

SSH_SIZE = "ssh -p 2222 odroid@128.255.45.245 'size Documents/dev/ESMS/MSB/build/%s'"; 

tree = lambda: defaultdict(tree);
dataset = tree();

def mkdirs(path):
    try: 
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from plot import barchart

def plotSize(dataset, index, ylabel):
    AoC = [];
    IP = [];
    StreamIt = [];
    Cacheopt = [];
    KB = 1024.0;
    for msb in MSB:
        AoC.append(dataset[msb][CFG[0]][index] / KB);
        IP.append(dataset[msb][CFG[1]][index] / KB);
        StreamIt.append(dataset[msb][CFG[2]][index] / KB);
        if("Cacheopt" in dataset[msb]):
            Cacheopt.append(dataset[msb][CFG[3]][index] / KB);
        else:
            Cacheopt.append(0);

    dataset = [AoC, StreamIt, Cacheopt];
    legends = ('AoC', 'StreamIt', 'Cacheopt');
    if(index == 0):
        barchart(dataset, MSB, legends, ylabel, "../build/fig/esms-code-size.pdf");
    elif(index == 1):
        barchart(dataset, MSB, legends, ylabel, "../build/fig/esms-data-size.pdf");
    return;

if(os.path.isfile("dataset_size.csv")):
    with open("dataset_size.csv", "r") as csv:
        for line in csv.readlines()[1:]:
            (text, data, bss, benchmark) = line.strip().split(", ");
            for msb in MSB:
                if(not benchmark.startswith(msb)): continue;
                tk = benchmark[len(msb):];
                if(msb == MSB[-1] and tk == TK[-1]): continue;
                target = msb + tk;
                text = int(text);
                data = int(data);
                bss = int(bss);
                cfg = "StreamIt";
                if(tk.startswith("ESMS")):
                    cfg = tk[4:];
                elif(tk.endswith("Cacheopt")):
                    cfg = tk[-8:];
                dataset[msb][cfg] = [text, data + bss];
                print("read %s[%s]: %s" % (target, cfg, dataset[msb][cfg]));
else:
    with open("dataset_size.csv", "w") as csv:
        csv.write("text, data, bss, msb\n");
        for msb in MSB:
            for tk in TK:
                if(msb == MSB[-1] and tk == TK[-1]): continue;
                target = msb + tk;
                (text, data, bss) = os.popen(SSH_SIZE % target).readlines()[1].split()[0:3];
                text = int(text);
                data = int(data);
                bss = int(bss);
                cfg = "StreamIt";
                if(tk.startswith("ESMS")):
                    cfg = tk[4:];
                elif(tk.endswith("Cacheopt")):
                    cfg = tk[-8:];
                dataset[msb][cfg] = [text, data + bss];
                print("write %s[%s]: %s" % (target, cfg, dataset[msb][cfg]));
                csv.write("%d, %d, %d, %s\n" % (text, data, bss, target));

plotSize(dataset, 0, "Code size (KB)");
plotSize(dataset, 1, "Data size (KB)");
