package esms.tests;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import esms.toolkits.ESMSToolkit;
import esms.toolkits.StreamItToolkit4j;
import esms.toolkits.Toolkit;
import esms.util.Log;
import esms.data.Performance;
import esms.util.StageTimer;

/**
 * Created by farleylai on 12/13/14.
 */
public class ToolkitTest {
    public static final String FMRadio = "FMRadio";

    @Rule
    public TestName INFO = new TestName();

    @BeforeClass
    public static void before() {
        Log.level(Log.Severity.INFO);
    }

    @Test
    public void testStreamItToolkit() throws Exception {
        Toolkit strc = new StreamItToolkit4j("build/StreamIt", FMRadio, 201600, false);
        StageTimer timer = strc.build(true);
        Performance perf = strc.run();
        Log.i(INFO.getMethodName(), strc);
        Log.title("%s Toolkit Performance", strc.name());
        Log.i(INFO.getMethodName(), timer);
        Log.title("%s Performance Measures", FMRadio);
        Log.i(INFO.getMethodName(), perf);
    }

    @Test
    public void testCSenseToolkit() throws Exception {
        Toolkit csense = new ESMSToolkit("build/CSense", FMRadio, 201600, false);
        StageTimer timer = csense.build(true);
        Performance perf = csense.run();
        Log.i(INFO.getMethodName(), csense);
        Log.title("%s Toolkit Performance", csense.name());
        Log.i(INFO.getMethodName(), timer);
        Log.title("%s Performance Measures", FMRadio);
        Log.i(INFO.getMethodName(), perf);
    }
}
