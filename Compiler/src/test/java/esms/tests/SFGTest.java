package esms.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import esms.api.Channel;
import esms.api.Component;
import esms.core.Operation;
import esms.api.SFG;
import esms.core.Simulation;
import esms.api.Simulator;
import esms.util.Log;

import static esms.core.OP.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by farleylai on 2/14/14.
 */
public class SFGTest {
    @Rule
    public TestName INFO = new TestName();

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTopsort1() throws IOException {
        SFG sfg = new SFG();
        sfg.link("7", "8");
        sfg.link("7", "11");
        sfg.link("5", "11");
        sfg.link("11", "2");
        sfg.link("11", "9");
        sfg.link("11", "10");
        sfg.link("3", "8");
        sfg.link("3", "10");
        sfg.link("8", "9");
        sfg.save(INFO.getMethodName());
        Set<List<Component>> sorted = sfg.topsort();
        //assertTrue(sorted.stream().allMatch(s -> sfg.isTopsorted(s)));
    }

    @Test
    public void testTopsort2() throws IOException {
        SFG sfg = new SFG();
        sfg.link("A", "B");
        sfg.link("B", "C");
        sfg.link("C", "D");
        sfg.link("B", "E");
        sfg.save(INFO.getMethodName());
        Set<List<Component>> sorted = sfg.topsort();
        //assertTrue(sorted.stream().allMatch(s -> sfg.isTopsorted(s)));
        Log.i(INFO.getMethodName(), sorted);
    }

    @Test
    public void testTopsort3() throws IOException {
        SFG sfg = new SFG();
        sfg.link("A", "B");
        sfg.link("B", "C");
        sfg.link("C", "D");
        sfg.link("D", "E");
        sfg.link("D", "F");
        sfg.link("E", "G");
        sfg.link("E", "H");
        sfg.link("F", "I");
        sfg.link("F", "J");
        sfg.link("G", "K");
        sfg.link("H", "K");
        sfg.link("I", "L");
        sfg.link("J", "L");
        sfg.link("K", "M");
        sfg.link("M", "N");
        sfg.link("L", "O");
        sfg.link("O", "P");
        sfg.link("N", "Q");
        sfg.link("P", "Q");
        sfg.link("Q", "R");
        sfg.link("R", "X");

        sfg.save(INFO.getMethodName());
        Set<List<Component>> sorted = sfg.topsort(true);
        //assertTrue(sorted.stream().allMatch(s -> sfg.isTopsorted(s)));
        Log.i(INFO.getMethodName(), "Components: %s %s topologically sorted when added\n", sfg.components(), sfg.isTopsorted(sfg.components()) ? "is" : "is not");
        Log.i(INFO.getMethodName(), "%d distinct topsorts enumerated\n", sorted.size());
        //sorted.forEach(c -> {
        //    Log.i(INFO.getMethodName(), c);
        //});
    }

    @Test
    public void testConstruction() {
        SFG sfg = new SFG();
        Channel AB = sfg.link("A", "B", 4, 3, 3);
        Channel BC = sfg.link("B", "C", 3, 2, 2);
        Channel AC = sfg.link("A", "C", 2, 1, 1);
        assertEquals(sfg.getState(AB).PUSH, 4);
        assertEquals(sfg.getState(BC).PEEK, 2);
        assertEquals(sfg.getState(AC).POP, 1);
        assertEquals(sfg.inDegrees("A"), 0);
        assertEquals(sfg.outDegrees("A"), 2);
        assertEquals(sfg.inDegrees("B"), 1);
        assertEquals(sfg.outDegrees("B"), 1);
        assertEquals(sfg.inDegrees("C"), 2);
        assertEquals(sfg.outDegrees("C"), 0);
//        System.out.println(mSFG);
    }

//    @Test
//    public void testSWMultiCfg() throws IOException {
//        Log.i("========= %s ===========\n", INFO.getMethodName());
//        SFG sfg = new SFG();
//        Channel AB = sfg.link("A", "B", 9, 6, 3);
//        Channel BC = sfg.link("B", "C", 6, 6, 6);
//        Channel CD = sfg.link("C", "D", 6, 6, 6);
//        sfg.save(INFO.getMethodName(), true);
//
//        sfg.annotate("A", AB, CREATE, 9);
//        sfg.annotate("C", CD, UPDATE, 0, 6);
//
//        Simulator simulator = new Simulator();
//        Simulation simulation = simulator.simulate(sfg);
//        simulation.save(INFO.getMethodName(), true);
//        Log.i(sfg);
//        Log.i(simulation.info());
//    }

    @Test
    public void testSplitterDuplicate() throws IOException {
        Log.i("========= %s ===========\n", INFO.getMethodName());
        SFG sfg = new SFG();
        Channel AB = sfg.link("A", "B", 9, 6, 3);
        Channel BC = sfg.link("B", "C", 6, 6, 6);
        Channel CD = sfg.link("C", "D", 6, 6, 6);
        Channel BE = sfg.link("B", "E", 6, 6, 6);
        sfg.save(INFO.getMethodName(), true);

        sfg.annotate("A", AB, CREATE, 9);
        sfg.annotate("B", BC, PASS, sfg.getComponent("A"), 0, 6);
        sfg.annotate("B", BE, PASS, sfg.getComponent("A"), 0, 6);
        sfg.annotate("C", CD, UPDATE, 0, 6);

        Simulator simulator = new Simulator();
        Simulation simulation = simulator.simulate(sfg);
        simulation.save(INFO.getMethodName(), true);
        Log.i(INFO.getMethodName(), sfg);
        Log.i(INFO.getMethodName(), simulation.info());
    }

    @Test
    public void testJoiner() throws IOException {
        Log.i("========= %s ===========\n", INFO.getMethodName());
        SFG sfg = new SFG();
        Channel AD = sfg.link("A", "D", 1, 1, 1);
        Channel BD = sfg.link("B", "D", 2, 2, 2);
        Channel CD = sfg.link("C", "D", 3, 3, 3);
        Channel DE = sfg.link("D", "E", 6, 6, 3);
        Channel EF = sfg.link("E", "F", 6, 6, 6);
        sfg.save(INFO.getMethodName(), true);

        sfg.annotate("A", AD, CREATE, 1);
        sfg.annotate("B", BD, CREATE, 2);
        sfg.annotate("C", CD, CREATE, 3);
        sfg.annotate("D", DE, PASS, sfg.getComponent("B"), 0, 2);
        sfg.annotate("D", DE, PASS, sfg.getComponent("A"), 0, 1);
        sfg.annotate("D", DE, PASS, sfg.getComponent("C"), 0, 3);
        sfg.annotate("E", EF, UPDATE, 0, 3);

        Simulator simulator = new Simulator();
        Simulation simulation = simulator.simulate(sfg);
        simulation.save(INFO.getMethodName(), true);
        Log.i(INFO.getMethodName(), sfg);
        Log.i(INFO.getMethodName(), simulation.info());
    }

    @Test
    public void testMovingAvg() throws IOException {
        Log.i("========= %s ===========\n", INFO.getMethodName());
        SFG sfg = new SFG();
        Channel AB = sfg.link("A", "B", 1, 10, 1);
        Channel BC = sfg.link("B", "C", 1, 1, 1);
        sfg.save(INFO.getMethodName(), true);

        sfg.annotate("A", AB, CREATE, 1);
        sfg.annotate("B", BC, CREATE, 1);

        Simulator simulator = new Simulator();
        Simulation simulation = simulator.simulate(sfg);
        simulation.save(INFO.getMethodName(), true);
        Log.i(INFO.getMethodName(), sfg);
        Log.i(INFO.getMethodName(), simulation.info());
    }

//    @Test
//    public void testSplitRemainders() throws IOException {
//        Log.i("========= %s ===========\n", INFO.getMethodName());
//        SFG sfg = new SFG();
//        Channel AB = sfg.link("A", "B", 1, 1, 1);
//        Channel BC = sfg.link("B", "C", 1, 12, 4);
//        Channel BD = sfg.link("B", "D", 1, 8, 4);
//        Channel CE = sfg.link("C", "E", 6, 3, 3);
//        Channel DE = sfg.link("D", "E", 4, 2, 2);
//        Channel EF = sfg.link("E", "F", 5, 1, 1);
//        Channel FG = sfg.link("F", "G", 1, 1, 1);
//        sfg.save(INFO.getMethodName(), true);
//
//        sfg.annotate("A", AB, Operation.create(1, 4));
//        sfg.annotate("B", BC, Operation.pass(sfg.getComponent("A"), 0, 1));
//        sfg.annotate("B", BD, Operation.pass(sfg.getComponent("A"), 0, 1));
//        sfg.annotate("C", CE, Operation.create(6, 4));
//        sfg.annotate("D", DE, Operation.create(4, 4));
//        sfg.annotate("E", EF, Operation.pass(sfg.getComponent("C"), 0, 3));
//        sfg.annotate("E", EF, Operation.pass(sfg.getComponent("D"), 0, 2));
//        sfg.annotate("F", FG, Operation.create(1, 4));
//        Simulator simulator = new Simulator();
//        Simulation simulation = simulator.simulate(sfg, 1);
//        simulation.save(INFO.getMethodName(), true);
//        Log.i(sfg);
//        Log.i(simulation.info());
//    }

/*
    @Test
    public void testExample1() throws IOException {
        System.out.println("========= testExample1 ===========");
        SFG sfg = new SFG();
        int frame = 320;
        int prepend = 192;
        int append = 64;
        int insert1 = 32;
        int insert2 = 16;
        sfg.link("Source", "Prepender", frame, frame, frame);
        sfg.link("Prepender", "Inserter1", prepend+frame, prepend+frame, prepend+frame);
        sfg.link("Inserter1", "Inserter2", prepend+frame+insert1, prepend+frame+insert1, prepend+frame+insert1);
        sfg.link("Inserter2", "Appender", prepend+frame+insert1+insert2, prepend+frame+insert1+insert2, prepend+frame+insert1+insert2);
        sfg.link("Appender", "Sink", prepend+frame+insert1+insert2+append, prepend+frame+insert1+insert2+append, prepend+frame+insert1+insert2+append);
        sfg.save("testExample1.dot", true);
        Log.i(sfg.toString());

        Component src = sfg.getComponent("Source");
        Component prepender = sfg.getComponent("Prepender");
        Component inserter1 = sfg.getComponent("Inserter1");
        Component inserter2 = sfg.getComponent("Inserter2");
        Component appender = sfg.getComponent("Appender");
        Component sink = sfg.getComponent("Sink");
        sfg.annotate(src, prepender, NEW);
        sfg.annotate(src, prepender, CREATE, 320);
        sfg.annotate(prepender, inserter1, BASE, src);
        sfg.annotate(prepender, inserter1, INSERT, 0, 192);
        sfg.annotate(inserter1, inserter2, BASE, prepender);
        sfg.annotate(inserter1, inserter2, INSERT, 64, 32);
        sfg.annotate(inserter2, appender, BASE, inserter1);
        sfg.annotate(inserter2, appender, INSERT, 256, 16);
        sfg.annotate(appender, sink, BASE, inserter2);
        sfg.annotate(appender, sink, INSERT, 560, 64);
        sfg.annotate(sink, READ, appender, 0, 624);

        Simulator simulator = new Simulator();
        Simulation simulation = simulator.simulatePASS(sfg);
        sfg.dispPASS();
        for(Schedule schedule: simulation.schedules()) {
            schedule.disp();
            int i = 0;
            for(Configuration cfg: simulation.configurations(schedule)) {
                System.out.printf("===== steady layout[%d] cost: (%d,%d) =====\n",
                                    i++, cfg.totalSteadyBytesWritten(), cfg.totalBytesAllocated());
                cfg.dispSteadyChannels();
//                Superframe layout = cfg.getChannel(appender, sink).superframe().layout();
//                //System.out.printf("Superframe:\n%s", layout);
//                assertTrue(layout.hasRange(prepender,0,0,64));
//                assertTrue(layout.hasRange(inserter1,0,64,32));
//                assertTrue(layout.hasRange(prepender,64,96,128));
//                assertTrue(layout.hasRange(src,0,224,32));
//                assertTrue(layout.hasRange(inserter2,0,256,16));
//                assertTrue(layout.hasRange(src,32,272,288));
//                assertTrue(layout.hasRange(appender,0,560,64));
            }
        }
    }

    @Test
    public void testExample2Configurations() throws IOException {
        System.out.println("========= testExample2Configurations ===========");
        SFG sfg = new SFG();
        sfg.link("A", "B", 320, 320, 160);
        sfg.link("B", "C", 320, 320, 320).setPeekLinear();
        sfg.save("testExample2Configurations.dot", true);
        sfg.disp();

        Component A = sfg.getComponent("A");
        Component B = sfg.getComponent("B");
        Component C = sfg.getComponent("C");
        sfg.annotate(A, B, NEW);
        sfg.annotate(A, B, CREATE, 320);
        sfg.annotate(B, C, BASE, A);
        sfg.annotate(B, C, UPDATE, 0, 64);
        sfg.annotate(C, READ, B, 0, 320);

        Simulator simulator = new Simulator();
        Simulation simulation = simulator.simulatePASS(sfg);
        sfg.dispPASS();
        for(Schedule schedule: simulation.schedules()) {
            schedule.disp();
            int i = 0;
            List<Configuration> cfgs = simulation.configurations(schedule);
            assertEquals(2, cfgs.sizeInBytes());
            for(Configuration cfg: cfgs) {
                if(cfg.hasInit()) {
                    System.out.printf("===== Final [INIT] Superframe[%d:%s] cost: (%d,%d) =====\n",
                                        i, cfg, cfg.totalInitBytesWritten(), cfg.totalBytesAllocated());
                    cfg.dispInitChannels();
                }
                System.out.printf("===== Final [STEADY] Superframe[%d:%s] cost: (%d,%d) =====\n",
                                    i, cfg, cfg.totalSteadyBytesWritten(), cfg.totalBytesAllocated());
                cfg.dispSteadyChannels();
                i++;
            }
        }
    }
*/
/*
    @Test
    public void testExample2MFCC() throws IOException {
        System.out.println("========= Test MFCC Real ===========");
        SFG sfg = new SFG();
        Channel AB = sfg.link("Audio", "RMS", 320, 320, 160);
        Channel BC = sfg.link("RMS", "MFCC", 320, 320, 320);
        Channel DE = sfg.link("MFCC", "DiskAudio", 320, 320, 320);
        Channel CD = sfg.link("MFCC", "DiskCC", 11, 11, 11);
        sfg.save("testExample2MFCC.dot", true);

        Schedule schedule = sfg.getSSASchedules();
        sfg.disp();
        sfg.dispSchedules();

        Component audio = sfg.getComponent("Audio");
        Component rms = sfg.getComponent("RMS");
        Component mfcc = sfg.getComponent("MFCC");
        Component diskAudio = sfg.getComponent("DiskAudio");
        Component diskCC = sfg.getComponent("DiskCC");

        audio.annotate(rms, NEW);
        audio.annotate(rms, CREATE, 320);
        rms.annotate(mfcc, BASE, audio);
        rms.annotate(mfcc, SELECT, 0, 320);
        mfcc.annotate(diskAudio, BASE, rms);
        mfcc.annotate(diskCC, NEW);
        mfcc.annotate(diskCC, CREATE, 11);
        diskAudio.annotate(READ, mfcc, 0, 320);
        diskCC.annotate(READ, mfcc, 0, 11);

        Simulator simulator = new Simulator();
        simulator.simulateInit(sfg, schedule);
        Superframe layout = mfcc.getOutputChannel(diskAudio).superframe().layout();
        assertTrue(layout.hasRange(audio,0,0,320));
        layout = mfcc.getOutputChannel(diskCC).superframe().layout();
        assertTrue(layout.hasRange(mfcc,0,0,11));
        sfg.dispChannels();

        simulator.simulateSteady(sfg, schedule);
        layout = mfcc.getOutputChannel(diskAudio).superframe().layout();
        assertTrue(layout.hasRange(rms,audio,-160,0,160));
        assertTrue(layout.hasRange(audio,0,160,320));
        layout = mfcc.getOutputChannel(diskCC).superframe().layout();
        assertTrue(layout.hasRange(mfcc,0,0,22));
        sfg.dispChannels();
    }

    @Test
    public void testMFCCRealScaled() throws IOException {
        System.out.println("========= Test MFCC Real ===========");
        SFG sfg = new SFG();
        int mult = 5;
        Channel AB = sfg.link("Audio", "RMS", 16000*mult, 16000, 8000);
        Channel BC = sfg.link("RMS", "MFCC", 16000, 320, 160);
        Channel CD = sfg.link("MFCC", "DiskCC", 11, 11, 11);
        Channel DE = sfg.link("MFCC", "DiskAudio", 320, 320, 320);

        sfg.simulatePASS();
        sfg.disp();
        sfg.dispSchedules();
        sfg.save("simMFCCRealScaled.dot", true);
    }

    @Test
    public void testExample3SlidingWindowPrepend() throws IOException {
        System.out.println("========= testExample3SlidingWindowPrepend ===========");
        SFG sfg = new SFG();
        Channel AB = sfg.link("Source", "SW",  512, 512, 384);
        Channel BC = sfg.link("SW",   "Prepend",  512, 512, 512);
        Channel CD = sfg.link("Prepend",  "Sink",  576,  576,  576);
        sfg.save("testExample3SlidingWindowPrepend.dot", true);

        Schedule schedule = sfg.getSSASchedules();
        sfg.disp();
        sfg.dispSchedules();

        Component src = sfg.getComponent("Source");
        Component sw = sfg.getComponent("SW");
        Component prepender = sfg.getComponent("Prepend");
        Component sink = sfg.getComponent("Sink");

        src.annotate(sw, NEW);
        src.annotate(sw, CREATE, 512);
        sw.annotate(prepender, BASE, src);
        prepender.annotate(sink, BASE, sw);
        prepender.annotate(sink, INSERT, 0, 64);
        sink.annotate(READ, prepender, 0, 576);

        Simulator simulator = new Simulator();
        simulator.simulateInit(sfg, schedule);
        sfg.dispChannels();
        Superframe layout = prepender.getOutputChannel(sink).superframe().layout();
        assertTrue(layout.hasRange(prepender,0,0,64));
        assertTrue(layout.hasRange(src,0,64,512));

        simulator.simulateSteady(sfg, schedule);
        sfg.dispChannels();
        layout = prepender.getOutputChannel(sink).superframe().layout();
        assertTrue(layout.hasRange(prepender,0,0,64));
        assertTrue(layout.hasRange(sw,src,-384,64,128));
        assertTrue(layout.hasRange(prepender,0,0,64));
        assertTrue(layout.hasRange(src,0,192,256));
        assertTrue(layout.hasRange(prepender,64,448,64));
        assertTrue(layout.hasRange(src,256,512,384));
        assertTrue(layout.hasRange(prepender,128,896,64));
        assertTrue(layout.hasRange(src,640,960,384));
        assertTrue(layout.hasRange(prepender,192,1344,64));
        assertTrue(layout.hasRange(src,1024,1408,512));
    }

    @Test
    public void testExample4SlidingWindowInsert() throws IOException {
        System.out.println("========= testExample4SlidingWindowInsert ===========");
        SFG sfg = new SFG();
        Channel AB = sfg.link("Source", "Insert",  512, 512, 384);
        Channel BC = sfg.link("Insert",   "Sink",  576, 576, 576);
        sfg.simulatePASS();
        sfg.dispSchedules();
        sfg.save("testExample4SlidingWindowInsert.dot", true);

        Schedule schedule = sfg.getSSASchedules();
        sfg.disp();
        sfg.dispSchedules();

        Component src = sfg.getComponent("Source");
        Component insert = sfg.getComponent("Insert");
        Component sink = sfg.getComponent("Sink");

        src.annotate(insert, NEW);
        src.annotate(insert, CREATE, 512);
        insert.annotate(sink, BASE, src);
        insert.annotate(sink, INSERT, 256, 64);
        sink.annotate(READ, insert, 0, 576);

        Simulator simulator = new Simulator();
        simulator.simulateInit(sfg, schedule);
        sfg.dispChannels();
        Superframe layout = insert.getOutputChannel(sink).superframe().layout();
        assertTrue(layout.hasRange(src,0,0,256));
        assertTrue(layout.hasRange(insert,0,256,64));
        assertTrue(layout.hasRange(src,256,320,256));

        simulator.simulateSteady(sfg, schedule);
        sfg.dispChannels();
        layout = insert.getOutputChannel(sink).superframe().layout();
        assertTrue(layout.hasRange(insert,src,-384,0,128));
        assertTrue(layout.hasRange(src,0,128,128));
        assertTrue(layout.hasRange(insert,0,256,64));
        assertTrue(layout.hasRange(src,128,320,384));
        assertTrue(layout.hasRange(insert,64,704,64));
        assertTrue(layout.hasRange(src,512,768,384));
        assertTrue(layout.hasRange(insert,128,1152,64));
        assertTrue(layout.hasRange(src,896,1216,384));
        assertTrue(layout.hasRange(insert,192,1600,64));
        assertTrue(layout.hasRange(src,1280,1664,256));
    }

    @Test
    public void testExample5DoubleJoiner() throws IOException {
        System.out.println("========= testExample5DoubleJoiner ===========");
        SFG sfg = new SFG();
        Channel AC = sfg.link("A", "C", 320, 160, 160);
        Channel BC = sfg.link("B", "C", 240, 120, 120);
        Channel CE = sfg.link("C", "E", 280, 140, 140);
        Channel DE = sfg.link("D", "E",  70,  50,  30);
        Channel EF = sfg.link("E", "F", 190, 190, 190);
        sfg.save("testExample5DoubleJoiner.dot", true);
        Schedule schedule = sfg.getSSASchedules();
        sfg.disp();
        sfg.dispSchedules();

        Component A = sfg.getComponent("A");
        Component B = sfg.getComponent("B");
        Component C = sfg.getComponent("C");
        Component D = sfg.getComponent("D");
        Component E = sfg.getComponent("E");
        Component F = sfg.getComponent("F");

        A.annotate(C, NEW);
        A.annotate(C, CREATE, 320);
        B.annotate(C, NEW);
        B.annotate(C, CREATE, 240);
        C.annotate(E, NEW);
        C.annotate(E, MAP, A, 0, 160);
        C.annotate(E, MAP, B, 0, 120);
        D.annotate(E, NEW);
        D.annotate(E, CREATE, 70);
        E.annotate(F, NEW);
        E.annotate(F, MAP, C, 0, 140);
        E.annotate(F, MAP, D, 0, 50);

        Simulator simulator = new Simulator();
        simulator.simulateInit(sfg, schedule);
        sfg.dispChannels();

//        simulator.simulateSteady(sfg, schedule);
//        sfg.dispChannels();
    }

    @Test
    public void testExample6ReorderJoiner() throws IOException {
        System.out.println("========= testExample6ReorderJoiner ===========");
        SFG sfg = new SFG();
        Channel AC = sfg.link("A", "C", 320, 160, 160);
        Channel BC = sfg.link("B", "C", 240, 120, 80);
        Channel CD = sfg.link("C", "D", 280, 280, 280);
        sfg.save("testExample6ReorderJoiner.dot", true);
        Schedule schedule = sfg.getSSASchedules();
        sfg.disp();
        sfg.dispSchedules();

        Component A = sfg.getComponent("A");
        Component B = sfg.getComponent("B");
        Component C = sfg.getComponent("C");
        Component D = sfg.getComponent("D");

        A.annotate(C, NEW);
        A.annotate(C, CREATE, 320);
        B.annotate(C, NEW);
        B.annotate(C, CREATE, 240);
        C.annotate(D, NEW);
        C.annotate(D, MAP, A, 80,  80);
        C.annotate(D, MAP, B,  0, 120);
        C.annotate(D, MAP, A,  0,  80);

        Simulator simulator = new Simulator();
        simulator.simulateInit(sfg, schedule);
        sfg.dispChannels();

        simulator.simulateSteady(sfg, schedule);
        sfg.dispChannels();
    }

    @Test
    public void testExample7ReuseSplitter() throws IOException {
        System.out.println("========= testExample7ReuseSplitter ===========");
        SFG sfg = new SFG();
        Channel AB = sfg.link("A", "B", 320, 320, 320);
        Channel BC = sfg.link("B", "C", 320, 320, 320);
        Channel CD = sfg.link("C", "D", 320, 320, 320);
        Channel BE = sfg.link("B", "E", 320, 320, 320);
        Channel EF = sfg.link("E", "F", 160, 160, 160);
        sfg.save("testExample7ReuseSplitter.dot", true);
        Schedule schedule = sfg.getSSASchedules();
        sfg.disp();
        sfg.dispSchedules();

        Component A = sfg.getComponent("A");
        Component B = sfg.getComponent("B");
        Component C = sfg.getComponent("C");
        Component D = sfg.getComponent("D");
        Component E = sfg.getComponent("E");
        Component F = sfg.getComponent("F");

        A.annotate(B, NEW);
        A.annotate(B, CREATE, 320);
        B.annotate(C, BASE, A);
        B.annotate(E, BASE, A);
        C.annotate(D, BASE, B);
        C.annotate(D, READ, 0, 320);
        C.annotate(D, UPDATE, 160, 160);
        E.annotate(F, BASE, B);
        E.annotate(F, READ, 0, 160);
        E.annotate(F, DELETE, 160, 160);
        E.annotate(F, UPDATE, 80, 80);

        Simulator simulator = new Simulator();
        simulator.simulateInit(sfg, schedule);
        sfg.dispChannels();
        simulator.simulateSteady(sfg, schedule);
        sfg.dispChannels();

        Superframe layout = C.getOutputChannel(D).superframe().layout();
        assertTrue(layout.hasRange(A, 0, 0, 160));
        assertTrue(layout.hasRange(C,0,160,160));
        layout = C.getOutputChannel(D).superframe().peekSourceWindow(0).layout();
        assertTrue(layout.hasRange(A, 0, 0, 160));
        assertTrue(layout.hasRange(C,0,160,160));

        layout = E.getOutputChannel(F).superframe().layout();
        assertTrue(layout.hasRange(A,0,0,80));
        assertTrue(layout.hasRange(E,0,80,80));
        layout = E.getOutputChannel(F).superframe().peekSourceWindow(0).layout();
        assertTrue(layout.hasRange(A,0,0,80));
        assertTrue(layout.hasRange(E,0,80,80));
    }
*/

    /*
        @Test
        public void testExample8CopyOnWriteSplitter() throws IOException {
            System.out.println("========= testExample8CopyOnWriteSplitter ===========");
            SFG sfg = new SFG();
            Channel AB = sfg.link("A", "B", 320, 320, 160);
            Channel BC = sfg.link("B", "C", 320, 320, 320);
            Channel BD = sfg.link("B", "D", 320, 320, 320);
            sfg.save("testExample8CopyOnWriteSplitter.dot", true);
            sfg.simulatePASS();
            sfg.dispSchedules();

            Schedule schedule = sfg.getSSASchedules();
            sfg.disp();
            sfg.dispSchedules();

            Component A = sfg.getComponent("A");
            Component B = sfg.getComponent("B");
            Component C = sfg.getComponent("C");
            Component D = sfg.getComponent("D");

            A.annotate(B, NEW);
            A.annotate(B, CREATE, 320);
            B.annotate(C, BASE, A);
            B.annotate(C, UPDATE, 0, 160);
            B.annotate(D, BASE, A);
            B.annotate(D, UPDATE, 160, 160);

            Simulator simulator = new Simulator();
            simulator.simulateInit(sfg, schedule);
            sfg.dispChannels();
            Superframe layout = B.getOutputChannel(C).superframe().layout();
            assertTrue(layout.hasRange(B,0,0,160));
            assertTrue(layout.hasRange(A,160,160,160));
            layout = B.getOutputChannel(D).superframe().layout();
            assertTrue(layout.hasRange(A,0,0,160));
            assertTrue(layout.hasRange(B,0,160,160));

            simulator.simulateSteady(sfg, schedule);
            sfg.dispChannels();
            layout = B.getOutputChannel(C).superframe().layout();
            assertTrue(layout.hasRange(B,0,0,320));
            assertTrue(layout.hasRange(A,0,320,320));
            layout = B.getOutputChannel(D).superframe().layout();
            assertTrue(layout.hasRange(B,A,-160,0,160));
            assertTrue(layout.hasRange(B,0,160,160));
            assertTrue(layout.hasRange(A,0,320,160));
            assertTrue(layout.hasRange(B,160,480,160));
        }

        @Test
        public void testExample9MultiLevelSlidingWindows() throws IOException {
            System.out.println("========= testExample9MultiLevelSlidingWindows ===========");
            SFG sfg = new SFG();
            Channel AB = sfg.link("A", "B", 6, 4, 2);
            Channel BC = sfg.link("B", "C", 4, 3, 2);
            Channel BD = sfg.link("C", "D", 3, 3, 3);
            sfg.save("testExample9MultiLevelSlidingWindows.dot", true);
            sfg.simulatePASS();
            sfg.dispSchedules();

            Schedule schedule = sfg.getSSASchedules();
            sfg.disp();
            sfg.dispSchedules();

            Component A = sfg.getComponent("A");
            Component B = sfg.getComponent("B");
            Component C = sfg.getComponent("C");
            Component D = sfg.getComponent("D");

            A.annotate(B, NEW);
            A.annotate(B, CREATE, 6);
            B.annotate(C, BASE, A);
            C.annotate(D, BASE, B);

            Simulator simulator = new Simulator();
            simulator.simulateInit(sfg, schedule);
    //        sfg.dispChannels();
            Superframe layout = C.getOutputChannel(D).superframe().layout();
            assertTrue(layout.hasRange(A,0,0,5));

            simulator.simulateSteady(sfg, schedule);
    //        sfg.dispChannels();
            layout = C.getOutputChannel(D).superframe().layout();
            assertTrue(layout.hasRange(C,A,-4,0,2));
            assertTrue(layout.hasRange(B,A,-4,2,2));
            assertTrue(layout.hasRange(A,0,4,5));
        }
    */
    @Test
    public void testSlidingWindowPrependFusion() throws IOException {
        System.out.println("========= testSlidingWindowPrependFusion ===========");
        SFG sfg = new SFG();
        Channel AB = sfg.link("Source", "Prepend", 512, 512, 384);
        Channel BC = sfg.link("Prepend", "Sink", 576, 576, 576);
//        sfg.simulatePASS();
//        sfg.dispSchedules();
        sfg.save("testSlidingWindowPrependFusion.dot", true);
    }

    @Test
    public void testSplitJoin() throws IOException {
        System.out.println("========= Test SplitJoin ===========");
        SFG sfg = new SFG();
        Channel AB = sfg.link("A", "B", 3, 2, 2);
        Channel AC = sfg.link("A", "C", 1, 3, 3);
        Channel BD = sfg.link("B", "D", 1, 3, 3);
        Channel CD = sfg.link("C", "D", 3, 2, 2);

        System.out.println(sfg);
//        sfg.simulatePASS();
//        sfg.dispSchedules();
        sfg.save("simSplitJoin.dot", true);
    }

    @Test
    public void testJoinSplit() throws IOException {
        System.out.println("========= Test JoinSplit ===========");
        SFG sfg = new SFG();
        Channel AC = sfg.link("A", "C", 3, 2, 2);
        Channel BC = sfg.link("B", "C", 1, 3, 3);
        Channel CD = sfg.link("C", "D", 3, 2, 2);
        Channel CE = sfg.link("C", "E", 1, 3, 3);

        System.out.println(sfg);
//        sfg.simulatePASS();
//        sfg.dispSchedules();
        sfg.save("simJoinSplit.dot", true);
    }

    @Test
    public void testSuperframeAscendingReuse() throws IOException {
        Log.i("========= %s ===========\n", INFO.getMethodName());
        SFG sfg = new SFG();
        int frame = 320;
        int prepend = 192;
        int append = 64;
        int insert1 = 32;
        int insert2 = 16;
        Channel AB = sfg.link("Source", "Prepender", 8000, 320, 320);
        Channel BC = sfg.link("Prepender", "Inserter", 512, 512, 512);
        Channel CD = sfg.link("Inserter", "Appender", 574, 574, 574);
        Channel DE = sfg.link("Appender", "Sink", 590, 590, 590);

//        sfg.simulatePASS();
//        sfg.dispSchedules();
        sfg.save(INFO.getMethodName(), true);
    }
}
