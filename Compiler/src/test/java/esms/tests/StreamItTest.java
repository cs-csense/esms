package esms.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import esms.api.Configuration;
import esms.types.Strategy;
import esms.types.Benchmark;
import esms.toolkits.ESMSToolkit;
import esms.toolkits.Toolkit;
import esms.benchmark.BenchmarkCFG;
import esms.util.DesktopBenchmarkBuilder;
import esms.util.Log;
import esms.util.SSH;
import esms.util.Shell;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeTrue;

/**
 * Target platforms: OSX_x86_64 | Linux_x86_64 | Android_arm32
 * Backends:
 * - C/Java/StreamIt for OSX_x86_64
 * - C/Java/StreamIt for Linux_x86_64
 * - Java for Android
 * - C/StreamIt/JNI for Android
 */
public class StreamItTest {
    @Rule
    public TestName INFO = new TestName();
    private static boolean MSA, MSA2;
    private static boolean MSB;
    private static boolean CLEAN;
    private static boolean PRINTING;
    private static boolean PLUS;
    private static boolean DRYRUN;
    private static boolean CACHEGRIND;
    private static boolean BENCHMARKING;
    private static boolean SCALING;
    private static boolean DBG_SFG;
    private static int LINES = 1000;
    private static SSH SSH;

    private boolean clean() {
        return CLEAN;
    }

    private int[] params() {
        return BenchmarkCFG.params(benchmark());
    }

    private int scaling() {
        return SCALING ? BenchmarkCFG.scaling(benchmark()) : 1;
    }

    private int iterations() {
        if(CACHEGRIND)
            return 1000;
        if(BENCHMARKING)
            return BenchmarkCFG.iterations(benchmark(), MSB ? Shell.OS.Android : Shell.os());
        switch(benchmark()) {
            case MatrixMultBlock:
                return 100;
            case SpeakerIdentifier2:
                return 100;
            default:
                return 1000;
        }
    }

    private boolean printing() { return !BENCHMARKING || PRINTING; }

    private File jniESMS(boolean msa, Strategy strategy) {
        return jniESMS(msa, strategy, "..");
    }

    private File jniESMS(boolean msa, Strategy strategy, String dir) {
        String suffix = "";
        switch(strategy) {
            case IN_PLACE:
                suffix = "IP"; break;
            case APPEND_ALWAYS:
                suffix = "AA"; break;
            case APPEND_ON_CONFLICT:
                suffix = "AoC"; break;
        }
        String variant = benchmark() + "ESMS" + suffix;
        File jni = new File(String.join(File.separator, dir, (msa ? "MSA" : "MSB"), "src", variant, "jni"));
        return jni;
    }

    private File jniStreamIt(boolean msa, int scaling) {
        return jniStreamIt(msa,scaling, "..");
    }

    private File jniStreamIt(boolean msa, int scaling, String dir) {
        String variant = benchmark() + "StreamIt" + (scaling > 1 ? "Cacheopt" : "");
        File jni = new File(String.join(File.separator, dir, (msa ? "MSA" : "MSB"), "src", variant, "jni"));
        return jni;
    }

    private Benchmark benchmark() {
        String name = test().substring("test".length());
        return Benchmark.valueOf(name);
    }

    private String test() {
        return INFO.getMethodName();
    }

    private List<String> res(String filename) {
        Resources.add(filename);
        return Resources;
    }

    private DesktopBenchmarkBuilder Builder;
    private List<String> Resources;

    @Before
    public void setUp() throws Exception {
        Log.level(Log.Severity.INFO);
        LINES           = 1000;
        CLEAN           = !false;
        MSA             = false;
        MSA2            = !false;
        MSB             = false;
        PLUS            = false;
        PRINTING        = false;
        SSH             = new SSH("odroid", "odroid", "128.255.45.245", 2222);
        DRYRUN          = Boolean.valueOf(System.getProperty("DRYRUN", "false"));
        CACHEGRIND      = Boolean.valueOf(System.getProperty("CACHEGRIND", "false"));
        BENCHMARKING    = !Boolean.valueOf(System.getProperty("BENCHMARKING", "true"));
        SCALING         = Boolean.valueOf(System.getProperty("SCALING", "false"));

        Resources = new ArrayList<>();
        switch(benchmark()) {
            case BPFProgram:
                DBG_SFG = true;
                break;
            case IO:
                res("Blur.float.raw.portion");
//                if(BENCHMARKING && Shell.os() == Shell.OS.Linux)
//                    PRINTING = true;
                break;

            // Benchmarks
            case AutoCor:
                break;
            case BasicCC:
                res("musicbeep.raw");
                break;
            case BitonicSort:
                break;
            case BitonicSortRecursive:
                break;
            case FIRcoarse:
                break;
            case FIR:
                break;
            case FMRadio:
                break;
            case FFT2:
                break;
            case FFT3:
                break;
            case MatrixMult:
                break;
            case MatrixMultBlock:
                break;
            case MergeSort:
                break;
            case Repeater:
                break;

            // Application benchmarks
            case BeepBeep:
                res("data.raw"); break;
            case MFCC:
                break;
            case Crowd:
                res("1personTalking.raw");
                res("guyGirlTalking.raw");
                res("3peopleTalking.raw");
                break;
            case MP3Decoder:
                PLUS = true;
                res("Blur.float.raw.portion");
                break;

            // Anroid apps
            case BeepBeepA:
                MSA = true;
                break;
            case SpeakerIdentifier:
                MSA = true;
                break;
            case SpeakerIdentifier2:
//                MSA = true;
                res("audio-16K.raw");
                res("audio-44K.raw");
                break;
            case CrowdA:
                MSA = true;
                break;
        }
        if(SCALING) assumeTrue(scaling() > 1);
        Builder = new DesktopBenchmarkBuilder(benchmark(), iterations(), scaling(), printing());

        if(MSA2) {
            Builder.addESMSToolkit4MSA2("esms.msa2");
        } else if (!SCALING) {
            // ESMS does not support exec scaling well
            for (Strategy strategy : BenchmarkCFG.strategies()) {
                switch (strategy) {
                    case IN_PLACE:
                        for (int g : BenchmarkCFG.maxWindowGroups())
                            Builder.addESMSToolkit(strategy, g, PLUS, params());
                        break;
                    case APPEND_ALWAYS:
                        Builder.addESMSToolkit(strategy, 32, PLUS, params());
                        break;
                    case APPEND_ON_CONFLICT:
                        Builder.addESMSToolkit(strategy, 32, PLUS, params());
                        break;
                }
            }
        }

        if(!MSA2) Builder.addStreamItToolkit(params());
        for(String filename: Resources)
            Builder.res(filename);

        List<Toolkit> toolkits = Builder.getToolkits("ESMS");
        for(Toolkit tk: toolkits)
            Configuration.debug(DBG_SFG, tk.pwd());
    }

    @After
    public void tearDown() throws Exception {
        if(SCALING) assumeTrue(scaling() > 1);
        Builder.build(clean());
        if(BENCHMARKING) {
            if(!DRYRUN && !MSB) {
                Builder.run();
                Builder.save();
            }
            if (CACHEGRIND && !MSB)
                Builder.cachegrind();

            // export to Android JNI
            if(MSA || MSB) {
                for (Toolkit tk : Builder.getToolkits()) {
                    if (tk.name().equals("ESMS")) {
                        ESMSToolkit toolkit = (ESMSToolkit) tk;
                        toolkit.export(jniESMS(MSA, toolkit.strategy()));
                        if(SSH != null)
                            tk.export(SSH, jniESMS(MSA, toolkit.strategy(), "Documents/dev/ESMS"));
                    } else if (tk.name().equals("StreamIt")) {
                        tk.export(jniStreamIt(MSA, scaling()));
                        if(SSH != null)
                            tk.export(SSH, jniStreamIt(MSA, scaling(), "Documents/dev/ESMS"));
                    }
                }
            }
        } else if(!Builder.verify(LINES))
            fail("Incorrect output");
        Builder.report();
    }

    @Ignore
    @Test
    public void testIO() throws Exception {}

    @Ignore
    @Test
    public void testInsert() throws Exception {}

    @Ignore
    @Test
    public void testInsert2() throws Exception {}

    @Ignore
    @Test
    public void testUpdate() throws Exception {}

    @Ignore
    @Test
    public void testStatefulInsert() throws Exception {}

    @Ignore
    @Test
    public void testBPFProgram() {}

    @Ignore
    @Test
    public void testFFT4() {}

    @Ignore
    @Test
    public void testBasicCC() {}

    @Test
    public void testAutoCor() {}

    @Test
    public void testBitonicSort() throws Exception {}

    @Test
    public void testBitonicSortRecursive() throws Exception {}

    @Test
    public void testFFT2() {}

    @Test
    public void testFFT3() {}

    @Test
    public void testFIRcoarse() {}

    @Test
    public void testFIR() {}

    /**
     * TODO slow w/ scaling
     *
     */
    @Test
    public void testFMRadio() {}

    @Test
    public void testMatrixMult() {}

    @Test
    public void testMatrixMultBlock() { LINES = 140400; }

    /**
     * TODO while loop splitting
     */
    @Test
    public void testMergeSort() {}

    @Test
    public void testProducerConsumer() {
    }

    /**
     * This example demonstrates splitjoin and loop split due to access to multipe ranges in an iteration.
     * Besides, non-productive filters are removed to enhance performance.
     *
     */
    @Test
    public void testRepeater() throws Exception {}

    /**
     * FIXME Only support one channel decoding and low performance.
     */
    @Test
    public void testMP3Decoder() {
        LINES = 57696;
    }

    @Test
    public void testBeepBeep() {}

    @Test
    public void testMFCC() {}

    @Test
    public void testCrowd() {}

    /**
     * Real MSA for Android.
     */
    @Ignore
    @Test
    public void testBeepBeepA() {}

    @Ignore
    @Test
    public void testCrowdA() {}

    @Ignore
    @Test
    public void testSpeakerIdentifier() {}

    @Ignore
    @Test
    public void testSpeakerIdentifier2() {}

    /**
     * FIXME prework(), non-deterministic
     *
     */
    @Ignore
    @Test
    public void testAudioCC() {}

    /**
     * FIXME Unsupported prework() yet that causes insufficient source windows pushed.
     *
     */
    @Ignore
    @Test
    public void testFilterBankNew() throws Exception {}

    /**
     * FIXME Unsupported portals
     *
     */
    @Ignore
    @Test
    public void testFilterBankLatency() {}
}