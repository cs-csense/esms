package esms.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.IOException;

import esms.api.Component;
import esms.core.Range;
import esms.core.Window;
import esms.util.Log;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by farleylai on 5/30/14.
 */
public class WindowTest {
    @Rule
    TestName INFO = new TestName();
    Component A = new Component("A");
    Component B = new Component("B");
    Component C = new Component("C");

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testWindowTraverse() throws IOException {
        Window window = new Window();
        window.add(new Range(A, 0, 0, 6));
        window.add(new Range(A, 3, 3, 6));
        window.add(new Range(B, 0, 9, 3));
        assertEquals(0, window.offset());
        assertEquals(15, window.size());
        assertEquals(12, window.layout().size());
        for (int pos = 0; pos < 6; pos++) {
            assertEquals(A, window.writerAt(pos));
            assertEquals(pos, window.indexAt(pos));
            assertEquals(pos, window.offsetAt(pos));
        }
        for (int pos = 6; pos < 12; pos++) {
            assertEquals(A, window.writerAt(pos));
            assertEquals(pos - 3, window.indexAt(pos));
            assertEquals(pos - 3, window.offsetAt(pos));
        }
        for (int pos = 12; pos < 15; pos++) {
            assertEquals(B, window.writerAt(pos));
            assertEquals(pos - 12, window.indexAt(pos));
            assertEquals(pos - 3, window.offsetAt(pos));
        }
    }

//    @Test
//    public void testWindowRemainder() {
//        Window window = new Window();
//        window.add(new Range(A, 45, 40, 10));
//        window.add(new Range(A, 50, 45, 3));
//        window.add(new Range(A, 49, 44, 2));
//        window.add(new Range(B, 30, 60, 5));
//        Window remainder = window.slice(10);
//        Channel.State s = new Channel.State();
//        Window saved = s.save(remainder.writer(C)).load(true);
//        assertEquals(1, saved.offsetAt(0));
//        assertEquals(0, saved.offsetAt(3));
//        assertEquals(4, saved.offsetAt(5));
//        assertEquals(7, saved.offsetAt(8));
//        assertEquals(-remainder.indexAt(0), saved.indexAt(0));
//        Log.i("original remainder:\n" + remainder);
//        Log.i("saved remainder:\n" + saved);
//    }

    @Test
    public void testRemainderWindowLayout() {
        Window window = new Window();
        window.add(new Range(B, A, -4, 2, 2));
        window.add(new Range(A, 0, 4, 2));
        Window slice = window.slice(1);
        assertEquals(-5, slice.indexAt(0));
        assertEquals(0, slice.indexAt(1));
        Log.i("window slice layout:\n%s", slice.layout());
    }

    @Test
    public void testWindowSlice1() {
        Window window = new Window();
        window.add(new Range(A, 2, 2, 2));
        window.add(new Range(A, 4, 4, 2));
        assertEquals(2, window.indexAt(0));
        assertEquals(3, window.indexAt(1));
        assertEquals(4, window.indexAt(2));
        assertEquals(5, window.indexAt(3));

        Window slice = window.slice(1, 2);
        assertEquals(3, slice.indexAt(0));
        assertEquals(4, slice.indexAt(1));
        assertEquals(2, slice.size());
//        Log.i("Window:\n%s", window);
//        Log.i("Slice:\n%s", slice);
    }

    @Test
    public void testWindowSlice2() {
        Window window = new Window();
        window.add(new Range(A, 0, 0, 6));
        assertEquals(0, window.indexAt(0));
        assertEquals(2, window.indexAt(2));
        assertEquals(4, window.indexAt(4));
        assertEquals(5, window.indexAt(5));

        Window slice = window.slice(2, 2);
        assertEquals(2, slice.indexAt(0));
        assertEquals(3, slice.indexAt(1));
        assertEquals(2, slice.size());
//        Log.i("Window:\n%s", window);
//        Log.i("Slice:\n%s", slice);
    }

    @Test
    public void testWindowSlice3() {
        Window sink = new Window();
        sink.add(A, 0, 0, 6);
        Window src = new Window();
        src.addAll(sink.slice(0, 6));
        Log.i(INFO.getMethodName(), src);
        src.contains(A, 0, 0, 6);
    }

    @Test
    public void testWindowCreate() {

    }

    @Test
    public void testWindowInsert1() {
        Window src1 = new Window();
        Window src2 = new Window();
        src1.add(new Range(A, 0, 0, 6));
        src2.add(new Range(A, 3, 3, 6));

        int offset = src1.offsetAt(4);
        src1.insert(B, 0, 4, 5, 1);
        src2.reserve(offset, 5);

//        Log.i("src1: \n%s\n", src1);
        assertEquals(B, src1.writerAt(4));
        assertEquals(1, src1.indexAt(5));
        assertEquals(5, src1.rangeAt(4).size());

//        Log.i("src2: \n%s\n", src2);
        assertEquals(A, src2.writerAt(1));
        assertEquals(1, src2.rangeAt(0).size());
        assertEquals(3, src2.indexAt(0));
        assertEquals(4, src2.indexAt(1));
        assertEquals(6, src2.layout().size());

        Window output = new Window();
        output.addAll(src1);
        output.addAll(src2);
        Window layout = output.layout();
        assertEquals(2, layout.indexAt(2));
        assertEquals(1, layout.indexAt(5));
        assertEquals(6, layout.indexAt(11));
//        Log.i("output layout: \n%s\n", layout);
    }

    @Test
    public void testWindowInsert2() {
        Window src = new Window();
        src.add(new Range(B, 0, 0, 3));
        src.add(new Range(A, 3, 3, 3));
        src.add(new Range(A, 3, 3, 6));
        src.insert(B, 3, 3, 3, 1);
        assertTrue(src.contains(B, 0, 0, 3));
        assertTrue(src.contains(B, 3, 3, 3));
        assertTrue(src.contains(A, 3, 6, 3));
        assertTrue(src.contains(A, 3, 6, 6));
    }

    @Test
    public void testWindowUpdate1() {
        Window src = new Window();
        src.add(new Range(A, 0, 0, 6));
        src.add(new Range(A, 3, 3, 6));
        src.update(B, 0, 0, 3, false);
        assertTrue(src.contains(B, 0, 0, 3));
        assertTrue(src.contains(B, 3, 3, 3));
        assertTrue(src.contains(A, 3, 3, 6));
//        Log.i("src.update(B, 0, 0, 3, false):\n%s", src);

        src.update(B, 3, 3, 3, true);
        assertTrue(src.contains(B, 0, 0, 3));
        assertTrue(src.contains(B, 3, 3, 3));
        assertTrue(src.contains(A, 3, 6, 3));
        assertTrue(src.contains(A, 3, 6, 6));
//        Log.i("src.update(B, 3, 3, 3, true):\n%s", src);
    }

    @Test
    public void testWindowUpdate2() {
        Window src = new Window();
        src.add(new Range(B, A, -6, 0, 3));
        src.add(new Range(A, 0, 3, 3));
//        Log.i("to update src window\n%s", src);
        src.update(C, 0, 0, 3, true);
        assertTrue(src.contains(C, 0, 0, 3));
        assertTrue(src.contains(A, 0, 6, 3));
//        Log.i("updated src window\n%s", src);
    }

    @Test
    public void testWindowDelete1() {
        Window src = new Window();
        src.add(new Range(A, 0, 0, 6));
        src.add(new Range(A, 3, 3, 6));
        src.delete(5, 2);
        assertTrue(src.contains(A, 0, 0, 5));
        assertTrue(src.contains(A, 4, 4, 5));
    }

    @Test
    public void testWindowDelete2() {
        Window src = new Window();
        src.add(new Range(B, 0, 0, 1));
        src.add(new Range(A, 0, 1, 9));
//        Log.i(src);
        src.delete(1, 9);
        assertTrue(src.contains(B, 0, 0, 1));
        assertEquals(1, src.size());
//        Log.i(src);
    }
}
