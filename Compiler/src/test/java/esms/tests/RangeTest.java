package esms.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.util.List;

import esms.api.Component;
import esms.core.Range;
import esms.util.Log;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by farleylai on 5/30/14.
 */
public class RangeTest {
    @Rule
    public TestName INFO = new TestName();
    Component A = new Component("A");
    Component B = new Component("B");
    Component C = new Component("C");

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCompatible() {
        Range rangeA1 = new Range(A, 0, 10, 20);
        Range rangeA2 = new Range(A, 5, 15, 20);
        Range rangeA3 = new Range(A, 15, 25, 20, -1);
        Range rangeB = new Range(B, 0, 35, 20);
        assertTrue(rangeA1.contains(rangeA2.offset()));
        assertFalse(rangeA1.contains(rangeB.limit()));
        assertTrue(rangeA1.isCompatible(rangeA2));
        assertFalse(rangeA1.isCompatible(rangeA3));
        assertTrue(rangeA1.isCompatible(rangeB));
    }

    @Test
    public void testSubRange() {
        Range rangeA = new Range(A, 0, 10, 20);
        Range rangeB = new Range(B, 0, 35, 20);
        Range rangeC = new Range(C, 5, 60, 10, -1);
        Range subA = rangeA.subrange(15, 5);
        Range subB = rangeB.subrange(5, 5);
        Range subC = rangeC.subrange(5, 3);
        assertEquals(15, subA.index());
        assertEquals(25, subA.offset());
        assertEquals(30, subA.limit());
        assertEquals(5, subB.index());
        assertEquals(40, subB.offset());
        assertEquals(45, subB.limit());
        assertEquals(7, subC.index());
        assertEquals(65, subC.offset());
        assertEquals(68, subC.limit());
    }

    @Test
    public void testSlice() {
        Range rangeA = new Range(A, 0, 10, 20);
        Range rangeB = new Range(B, 0, 35, 20);
        Range rangeC = new Range(C, 5, 60, 10, -1);
        Range sliceA = rangeA.slice(15, 5);
        Range sliceB = rangeB.slice(5, 5);
        Range sliceC = rangeC.slice(5, 3);
        assertEquals(15, sliceA.index());
        assertEquals(25, sliceA.offset());
        assertEquals(30, sliceA.limit());
        assertEquals(5, sliceB.index());
        assertEquals(40, sliceB.offset());
        assertEquals(45, sliceB.limit());
        assertEquals(10, sliceC.index());
        assertEquals(62, sliceC.offset());
        assertEquals(65, sliceC.limit());
    }

    @Test
    public void testRangeAccess() {
        Range rangeA = new Range(A, 0, 10, 20);
        assertEquals(0 + 10, rangeA.indexAt(10));
        assertEquals(-1, rangeA.indexAt(20));
        assertEquals(10 + 19, rangeA.offsetAt(19));
        assertEquals(-1, rangeA.offsetAt(20));
        Range rangeB = new Range(B, 30, 35, 20, -1);
        assertEquals(30 + 10, rangeB.indexAt(10));
        assertEquals(35 + 20 - 1 - 19, rangeB.offsetAt(19));
    }

    @Test
    public void testRangeJoin() {
        Range rangeA = new Range(A, 0, 10, 20);
        Range rangeB = new Range(A, 15, 25, 20);
        assertTrue(rangeA.isJoinable(rangeB));
        Range rangeC = rangeA.join(rangeB);
        assertEquals(15, rangeC.indexAt(15));
        assertEquals(0, rangeC.index());
        assertEquals(10, rangeC.offset());
        assertEquals(35, rangeC.size());
    }

    @Test
    public void testRangeReserve() {
        Range r = new Range(A, 3, 3, 6);
        List<Range> ranges = r.reserve(4, 5);
        Log.i(INFO.getMethodName(), ranges);
    }

    @Test
    public void testRangeInsert() {
        Range r = new Range(A, 3, 3, 3);
        List<Range> ranges = r.insert(3, new Range(B, 3, 3, 3));
        Log.i(INFO.getMethodName(), ranges);
    }
}
