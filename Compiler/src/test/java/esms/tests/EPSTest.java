package esms.tests;

import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;

import org.junit.Test;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by farleylai on 8/18/14.
 */
public class EPSTest {
    @Test
    public void testSimple() throws IOException {
        FileOutputStream outputStream = new FileOutputStream("example.eps");
        EpsGraphics g = new EpsGraphics("Example", outputStream, 0, 0, 100, 100, ColorMode.COLOR_RGB);
        g.setColor(Color.black);
        g.setStroke(new BasicStroke(2.0f));
        g.drawLine(10, 10, 50, 10);
        g.setColor(Color.blue);
        g.fillRect(10, 0, 20, 20);
//        Log.i(g);
        g.flush();
        g.close();
    }
}
