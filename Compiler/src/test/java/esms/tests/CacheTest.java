package esms.tests;

import org.junit.After;
import org.junit.Before;

import esms.util.Log;

import static org.junit.Assert.fail;

/**
 * Created by farleylai on 12/22/14.
 */
public class CacheTest {
//    private final String[] TOOLKITS = { BenchmarkCFG.name(BenchmarkCFG.Toolkit.StreamIt), BenchmarkCFG.name(BenchmarkCFG.Toolkit.CSense) };
//    private final String[] BUILDS = { BenchmarkCFG.STREAMIT_BUILD, BenchmarkCFG.CSENSE_BUILD };
//    private final Benchmark[] BENCHMARKS = { //BitonicSort,
//                                             FFT2,
//                                             //MP3Decoder,
//                                             //Crowd,
//                                        };
    private final int[] ITERATIONS = { 1, 1000, 5000, 10000, 50000, 100000, 150000 };
    private final int CACHE_SIZE = 64;
    private final int CACHE_LINE_SIZE = 32;
    private final int CACHE_ASSOCIATIVITY = 1;

    @Before
    public void setUp() throws Exception {
        Log.level(Log.Severity.INFO);
    }

    @After
    public void tearDown() throws Exception {
    }

//    @Ignore
//    @Test
//    public void testCachelessPerformance() throws Exception {
//        CachelessDataset dataset = new CachelessDataset();
//        for (Benchmark benchmark: BENCHMARKS) {
//            for (int iterations : ITERATIONS) {
//                DesktopBenchmarkBuilder builder = new DesktopBenchmarkBuilder(benchmark, iterations, 1, false);
//                for(Toolkit tk: builder.getToolkits("ESMS"))
//                    csense.core.Configuration.debug(false, tk.pwd());
//                builder.build(true);
//                builder.cachegrind(CACHE_SIZE, CACHE_ASSOCIATIVITY, CACHE_LINE_SIZE);
//                for(int i = 0; i < TOOLKITS.length; i++) {
//                    String toolkit = TOOLKITS[i];
//                    String cachegrind = Shell.path(BUILDS[i], benchmark.name(), "cachegrind.log");
//                    Log.i("cachegrind path: " + cachegrind);
//                    dataset.load(new File(cachegrind), toolkit, benchmark.name(), iterations);
//                }
//            }
//        }
//        dataset.export("../build/charts/cachegrind/cacheless", BenchmarkCFG.AUTHOR);
//    }
//
//    @Test
//    public void testCacheMissPerformance() throws Exception {
//        Optimizer.opt(IN_PLACE);
//        final int ITERATIONS = 1000;
//        final int CACHE_SIZES[] = new int[] {64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536};
//        final int CACHE_LINE_SIZE = 32;
//        final int CACHE_ASSOCIATIVITY = 2;
//        CacheMissDataset dataset = new CacheMissDataset();
//        for (Benchmark benchmark: BENCHMARKS) {
//            DesktopBenchmarkBuilder builder = new DesktopBenchmarkBuilder(benchmark, ITERATIONS, 1, false);
//            for(Toolkit tk: builder.getToolkits("ESMS"))
//                csense.core.Configuration.debug(false, tk.pwd());
//            builder.build(false);
//            for(int CACHE_SIZE: CACHE_SIZES) {
//                builder.cachegrind(CACHE_SIZE, CACHE_ASSOCIATIVITY, CACHE_LINE_SIZE);
//                for (int i = 0; i < TOOLKITS.length; i++) {
//                    String toolkit = TOOLKITS[i];
//                    String cachegrind = Shell.path(BUILDS[i], benchmark.name(), "cachegrind.log");
//                    Log.i("cachegrind path: " + cachegrind);
//                    dataset.load(new File(cachegrind), toolkit, benchmark.name(), ITERATIONS, CACHE_SIZE, CACHE_LINE_SIZE, CACHE_ASSOCIATIVITY);
//                }
//            }
//        }
//        dataset.export("../build/charts/cachegrind/miss", BenchmarkCFG.AUTHOR);
//    }
}
