#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define SAMPLE_RATE (8000)
#define SAMPLE_SIZE (3*SAMPLE_RATE)
#define PI (3.14159265359)

//output switches
#define SHOW_DIFF	(0)
#define SHOW_CMND	(0)
#define SHOW_EST	(0)
#define SHOW_FREQ	(1)

int main(){
  float sample[SAMPLE_SIZE];
  float freq = 100;
  int i;
  for(i=0; i<SAMPLE_SIZE; i++){
    sample[i] = sin(((float)i*freq)/(float)SAMPLE_RATE * ((float)2*PI));
    
  }
  
  FILE *ofp = fopen("data.raw","wb");
  fwrite(sample, sizeof(float), SAMPLE_SIZE, ofp);
  fclose(ofp);
  
  FILE *ifp = fopen("data.raw","rb");
  if(ifp == NULL) {
    printf("file does not exist\n");
    exit(1);
  }

  float window[256];
  float tmp[128];
  fread(window, sizeof(float), 128, ifp);
//  while(!feof(ifp)){
  float *yinBuff = (float *)malloc(sizeof(float)*128); // (1/2) window size
  for(i=0; i<2; i++){    
    fread(tmp, sizeof(float), 128, ifp);
    int i;
    for(i=0; i<128; i++){
      window[i+128]=tmp[i];
    }

    //PERFORM YIN
    for(i=0; i<256; i++){
      yinBuff[i]=0;
    }
    //difference
    int tau;
    int j;
    float delta;
    for(tau=1; tau<128; tau++){
      for(j=0; j<128; j++){
	    delta=window[j]-window[j+tau];
	    yinBuff[tau]+=delta*delta;
      }
    }
    if(SHOW_DIFF){
      for(i=0; i<128; i++){      
	printf("%i: %f\n",i,yinBuff[i]);
      }
    }
    //cumulative mean normalize difference
    yinBuff[0]=1;
    float runningSum = 0;
    float cmn;
    for(tau=1; tau<128; tau++){
      runningSum+=yinBuff[tau];
      cmn = yinBuff[tau] / (runningSum/tau);
      yinBuff[tau]=cmn;
      if(SHOW_CMND){
	printf("%i: %f\n",tau,yinBuff[tau]);
      }
    }
    //absolute threshold
    int ans=-1;
    int found = 0;
    for(tau=1; tau<128; tau++){
      if(yinBuff[tau]<0.15 && !found){
	if(ans == -1){
	  ans=tau;
	}else if(yinBuff[tau] < yinBuff[ans]){
	  ans=tau;
	}else{
	  found=1;
	}
      }
    }
    if(SHOW_EST){
      printf("tau est: %i\n",ans);
    }
    //parabolic interpretation
    int tauEst = ans;
    if(ans != (float)-1){
      int x0 = (tauEst<1) ? tauEst : tauEst-1;
      int x2 = (tauEst+1<(SAMPLE_SIZE/2)) ? tauEst+1 : tauEst;
      if(x0==tauEst){
	ans = (yinBuff[tauEst]<=yinBuff[x2]) ? tauEst : x2;
      }else if(x2==tauEst){
	ans = (yinBuff[tauEst]<=yinBuff[x0]) ? tauEst : x0;
      }else{
	float s0 = yinBuff[x0];
	float s1 = yinBuff[tauEst];
	float s2 = yinBuff[x2];
	
	float realTau = ((float)tauEst + 0.5*(s2-s0)/(2.0*s1-s2-s0));
	printf("%f\n",realTau);
	float freq=SAMPLE_RATE/realTau;
      }
    }
    if(SHOW_FREQ){
      printf("freq: %f\n",freq);
    }
    
    for(i=0; i<128; i++){
      window[i] = window[i+128];
    }
  }
  return 0;
}