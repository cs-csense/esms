
/*
 * Copyright 2005 by the Massachusetts Institute of Technology.
 *
 * Permission to use, copy, modify, and distribute this
 * software and its documentation for any purpose and without
 * fee is hereby granted, provided that the above copyright
 * notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting
 * documentation, and that the name of M.I.T. not be used in
 * advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 * M.I.T. makes no representations about the suitability of
 * this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 */

/**
 * @description
 * This file contains functions that implement Discrete Cosine Transforms and
 * their inverses.  When reference is made to the IEEE DCT specification, it
 * is refering to the IEEE DCT specification used by both MPEG and JPEG.
 * A definition of what makes an 8x8 DCT conform to the IEEE specification, as well
 * as a pseudocode implementation, can be found in Appendix A of the MPEG-2 specification
 * (ISO/IEC 13818-2) on P. 125. 
 *
 * @author <a href="mailto:madrake@gmail.com">Matthew Drake</a>
 * @author <a href="mailto:rodric@gmail.com">Rodric Rabbah</a>
 * @file DCT.str
 * @version 1.0
 */

/**
 * Transforms a 1D signal from the signal domain to the frequency domain
 * using a Discrete Cosine Transform. 
 * @param size The number of elements in each dimension of the signal. 
 * @input size values, representing an array of values in the signal
 *        domain, ordered by row and then column.
 * @output size values representing an array of values in the 
 *         frequency domain, ordered by row and then column. Vertical frequency
 *         increases along each row and horizontal frequency along each column.
 */
float->float filter DCT_1D_reference_fine(int size) {
    float[size][size] coeff;
    init {
        for (int u = 0; u < size; u++) {
            float Cu = u == 0 ? 1/sqrt(2) : 1;
            for (int x = 0; x < size; x++)
                coeff[u][x] = sqrt(2.0/size) * Cu * cos(u * pi * (2.0 * x + 1) / (2.0 * size));
        }
    }

    work pop size push size {
        for (int u = 0; u < size; u++) {
            float tempsum = 0;
            for (int x = 0; x < size; x++) {
                tempsum += peek(x) * coeff[u][x];
            }
            push(tempsum);
        }
        for (int x = 0; x < size; x++) 
            pop();
    }
}
