//
// Created by Farley Lai on 7/30/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <gtest/gtest.h>
#include "audio.h"
#include "video.h"

using namespace testing;

TEST(OdroidMediaTest, test_audio_source) {
    void* src = audio_source_open(AUDIO_SRC_VOICE_RECOGNITION, SAMPLE_RATE_16K, CHANNEL_IN_MONO, ENCODING_PCM_S16_LE, 16000 * 2);
    ASSERT_TRUE(src != NULL);
    ASSERT_EQ(0, audio_source_start(src));
    FILE* fp = fopen("audio.raw", "w");
    for(int i = 0; i < 5; i++) {
        audio_buffer_t* buf = audio_source_read(src);
        ASSERT_TRUE(NULL != buf);
        fwrite(buf->addr, 1, buf->size, fp);
    }
    fclose(fp);
    ASSERT_EQ(0, audio_source_stop(src));
    ASSERT_EQ(0, audio_source_close(src));
}

TEST(OdroidMediaTest, test_video_source) {
    void* src = video_source_open(LENS_FACING_FRONT, VGA_480P, MJPEG, 5);
    ASSERT_TRUE(src != NULL);
    if(NULL == src) {
        fprintf(stderr, "Failed to open video device\n");
        exit(EXIT_FAILURE);
    }

    int ret = video_source_start(src);
    ASSERT_EQ(0, ret);
    if(ret) {
        fprintf(stderr, "Failed to start video capture\n");
        video_source_close(src);
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < 5; i++) {
        size_t size;
        video_buffer_t* buffer = video_source_read(src);
        EXPECT_TRUE(NULL != buffer);
        if(buffer) {
            //            char filename[32];
            //            sprintf(filename, "image%d.jpg", i);
            //            FILE* fp = fopen(filename, "w");
            //            fwrite(buffer->addr, 1, buffer->size, fp);
            //            fclose(fp);
            printf("read video frame[%d][%d] at %.3fms of size %d \n", i, buffer->index, buffer->timestamp / 1000.0, buffer->size);
            video_source_release(src, buffer);
        } else {
            fprintf(stderr, "Failed to read video\n");
            break;
        }
    }

    ASSERT_EQ(0, video_source_stop(src));
    ASSERT_EQ(0, video_source_close(src));
}