#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

#include "video.h"

static const int PIX_FORMAT[] = {
        V4L2_PIX_FMT_NV16,
        V4L2_PIX_FMT_NV21,
        V4L2_PIX_FMT_MJPEG,
        V4L2_PIX_FMT_YUV420,
        V4L2_PIX_FMT_YUYV,
};

typedef struct {
    int fd;
    size_t count;
    video_buffer_t* buffers;
} video_source_t;

static int xioctl(int fd, int request, void *arg) {
    int r;
    do r = ioctl(fd, request, arg);
    while(-1 == r && EINTR == errno);
    return r;
}
 
static int print_caps(int fd) {
    struct v4l2_capability caps = {};
    if(-1 == xioctl(fd, VIDIOC_QUERYCAP, &caps)) {
        perror("Querying Capabilities");
        return -1;
    }

    printf( "Driver Caps:\n"
            "  Driver: \"%s\"\n"
            "  Card: \"%s\"\n"
            "  Bus: \"%s\"\n"
            "  Version: %d.%d\n"
            "  Capabilities: %08x\n",
            caps.driver,
            caps.card,
            caps.bus_info,
            (caps.version>>16)&&0xff,
            (caps.version>>24)&&0xff,
            caps.capabilities);

    struct v4l2_cropcap cropcap = {0};
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(-1 == xioctl (fd, VIDIOC_CROPCAP, &cropcap)) {
        perror("Querying Cropping Capabilities");
        return -11;
    }

    printf( "Camera Cropping:\n"
            "  Bounds: %dx%d+%d+%d\n"
            "  Default: %dx%d+%d+%d\n"
            "  Aspect: %d/%d\n",
            cropcap.bounds.width, cropcap.bounds.height, cropcap.bounds.left, cropcap.bounds.top,
            cropcap.defrect.width, cropcap.defrect.height, cropcap.defrect.left, cropcap.defrect.top,
            cropcap.pixelaspect.numerator, cropcap.pixelaspect.denominator);

    int support_grbg10 = 0;
    struct v4l2_fmtdesc fmtdesc = {0};
    fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    char fourcc[5] = {0};
    char c, e;
    printf("  FMT : CE Desc\n--------------------\n");
    while (0 == xioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc)) {
        strncpy(fourcc, (char *)&fmtdesc.pixelformat, 4);
        if (fmtdesc.pixelformat == V4L2_PIX_FMT_SGRBG10)
            support_grbg10 = 1;
        c = fmtdesc.flags & 1? 'C' : ' ';
        e = fmtdesc.flags & 2? 'E' : ' ';
        printf("  %s: %c%c %s\n", fourcc, c, e, fmtdesc.description);
        fmtdesc.index++;
    }

    struct v4l2_format fmt = {0};
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width = 640;
    fmt.fmt.pix.height = 480;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
    fmt.fmt.pix.field = V4L2_FIELD_NONE;
    if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt)) {
        perror("Setting Pixel Format");
        return -11;
    }

    strncpy(fourcc, (char *)&fmt.fmt.pix.pixelformat, 4);
    printf( "Selected Camera Mode:\n"
            "  Width: %d\n"
            "  Height: %d\n"
            "  PixFmt: %s\n"
            "  Field: %d\n",
            fmt.fmt.pix.width,
            fmt.fmt.pix.height,
            fourcc,
            fmt.fmt.pix.field);
    return 0;
}

void* video_source_open(lens_facing_t lens, video_size_t resolution, video_format_t fmt, int fps) {
    int fd = open("/dev/video0", O_RDWR);
    if(fd == -1) {
        perror("Opening video device");
        return NULL;
    }

    int width = 320;
    int height = 240;
    switch(resolution) {
        case HD_720P: width = 1280, height = 720; break;
        case VGA_480P: width = 640, height = 480; break;
    }
    struct v4l2_format format = {0};
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width       = width;
    format.fmt.pix.height      = height;
    format.fmt.pix.pixelformat = PIX_FORMAT[fmt];
    format.fmt.pix.field       = V4L2_FIELD_NONE;
    if (-1 == xioctl(fd, VIDIOC_S_FMT, &format)) {
        perror("Setting video format");
        close(fd);
        return NULL;
    }

    struct v4l2_streamparm params = {0};
    params.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    params.parm.capture.timeperframe.numerator = 1;
    params.parm.capture.timeperframe.denominator = fps;
    if(-1 == xioctl(fd, VIDIOC_S_PARM, &params)) {
        perror("Setting video FPS");
        close(fd);
        return NULL;
    }

    struct v4l2_requestbuffers req = {0};
    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;
    if(-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
        perror("Requesting MMAP Buffer");
        close(fd);
        return NULL;
    }

    video_source_t* src = calloc(1, sizeof(video_source_t));
    src->fd = fd;
    src->count = req.count;
    src->buffers = calloc(req.count, sizeof(video_buffer_t));
    for(int i = 0; i < req.count; i++) {
        struct v4l2_buffer info = {0};
        info.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        info.memory = V4L2_MEMORY_MMAP;
        info.index = i;
        if(-1 == xioctl(fd, VIDIOC_QUERYBUF, &info)) {
            perror("Querying Buffer");
            close(fd);
            free(src->buffers);
            free(src);
            return NULL;
        }
        src->buffers[i].addr = mmap(NULL, info.length, PROT_READ | PROT_WRITE, MAP_SHARED, src->fd, info.m.offset);
        if(src->buffers[i].addr == MAP_FAILED) {
            perror("MAP_FAILED");
            close(fd);
            free(src->buffers);
            free(src);
            return NULL;
        }
        src->buffers[i].index = i;
        src->buffers[i].size = info.length;
    }
    return src;
}

int video_source_start(void* handle) {
    video_source_t* src = (video_source_t*)handle;
    for(int i = 0; i < src->count; i++) {
        struct v4l2_buffer buf = {0};
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;
        if (-1 == xioctl(src->fd, VIDIOC_QBUF, &buf)) {
            perror("Enqueue Buffer");
            return -1;
        }
    }

    int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(-1 == xioctl(src->fd, VIDIOC_STREAMON, &type)) {
        perror("Start Capture");
        return -1;
    }
    return 0;
}

int video_source_stop(void* handle) {
    video_source_t* src = (video_source_t*)handle;
    int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(-1 == xioctl(src->fd, VIDIOC_STREAMOFF, &type)) {
        perror("Stop Capture");
        return -1;
    }
    return 0;
}

int video_source_close(void* handle) {
    video_source_t* src = (video_source_t*)handle;
    if(src) {
        for(int i = 0; i < src->count; i++)
            munmap(src->buffers[i].addr, src->buffers[i].size);
        close(src->fd);
        free(src);
    }
    return 0;
}

video_buffer_t* video_source_read(void* handle) {
    video_source_t* src = (video_source_t*)handle;
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(src->fd, &fds);
    struct timeval tv = {0};
    tv.tv_sec = 1;
    int r = select(src->fd+1, &fds, NULL, NULL, &tv);
    if(-1 == r) {
        perror("Waiting for Frame");
        return NULL;
    }

    struct v4l2_buffer info = {0};
    info.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    info.memory = V4L2_MEMORY_MMAP;
    if(-1 == xioctl(src->fd, VIDIOC_DQBUF, &info)) {
        perror("Retrieving Frame");
        return NULL;
    }

    video_buffer_t* buffer = &src->buffers[info.index];
    buffer->size = info.bytesused;
    buffer->timestamp = info.timestamp.tv_sec * 1000000LL + info.timestamp.tv_usec;
    return buffer;
}

int video_source_release(void* handle, video_buffer_t* buffer) {
    video_source_t* src = (video_source_t*)handle;
    struct v4l2_buffer info = {0};
    info.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    info.memory = V4L2_MEMORY_MMAP;
    info.index = buffer->index;
    if(-1 == xioctl(src->fd, VIDIOC_QBUF, &info)) {
        perror("Enqueue Frame");
        return -1;
    }
    return 0;
}