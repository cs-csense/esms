#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#ifdef __ANDROID__
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE
#endif
#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif
#endif
#include <alsa/asoundlib.h>
#include "audio.h"

static const int FRAME_SIZE[] = { 1, 2 };

static const int ENCODING[] = {
        SND_PCM_FORMAT_S8,
        SND_PCM_FORMAT_S16_LE,
        SND_PCM_FORMAT_FLOAT_LE,
};

static const int SAMPLE_SIZE[] = { 1, 2, 4 };

static inline size_t audio_frames_per_buffer(audio_sample_rate_t rate) {
    return rate / 5;
}

typedef struct {
    snd_pcm_t* handle;
    bool started;
    int channels;
    int sample_size;
    audio_buffer_t buffer;
} audio_source_t;

typedef struct {
    int card_index;
    int device_index;
    char card_id[32];
    char device_id[32];
    char device_name[32];
} audio_device_t;

static int audio_device_find(char* desc, audio_device_t* dev, bool capture) {
    snd_ctl_t* handle;
    snd_ctl_card_info_t *info;
    snd_pcm_info_t *pcminfo_capture;
    snd_pcm_info_t *pcminfo_playback;
    snd_ctl_card_info_alloca(&info);
    snd_pcm_info_alloca(&pcminfo_capture);
    snd_pcm_info_alloca(&pcminfo_playback);

    int  card_no = -1;
    bool found = false;
    bool has_capture;
    bool has_playback;
    while(snd_card_next(&card_no) >= 0 && card_no >= 0 && !found) {
        char card[32];
        snprintf(card, sizeof(card), "hw:%d", card_no);
        if (snd_ctl_open(&handle, card, 0) >= 0 && snd_ctl_card_info(handle, info) >= 0) {
            snprintf(card, sizeof(card), "hw:%s", snd_ctl_card_info_get_id(info));
            int device_no = -1;
            while (snd_ctl_pcm_next_device(handle, &device_no) >= 0 && device_no != -1) {
                char device[32];
                snprintf(device, sizeof(device), "%s,%d", card, device_no);
                snd_pcm_info_set_device(pcminfo_capture, device_no);
                snd_pcm_info_set_subdevice(pcminfo_capture, 0);
                snd_pcm_info_set_stream(pcminfo_capture, SND_PCM_STREAM_CAPTURE);
                has_capture = snd_ctl_pcm_info(handle, pcminfo_capture) >= 0;
                snd_pcm_info_set_device(pcminfo_playback, device_no);
                snd_pcm_info_set_subdevice(pcminfo_playback, 0);
                snd_pcm_info_set_stream(pcminfo_playback, SND_PCM_STREAM_PLAYBACK);
                has_playback = snd_ctl_pcm_info(handle, pcminfo_playback) >= 0;
                printf("Has %s [%d:%d] id: %s, name: %s\n", device, card_no, device_no,
                           snd_pcm_info_get_id(pcminfo_capture), snd_pcm_info_get_name(pcminfo_capture));
                if(capture && has_capture && (strstr(card, desc) || strstr(device, desc))) {
//                    printf("Found %s [%d:%d] id: %s, name: %s\n", device, card_no, device_no,
//                           snd_pcm_info_get_id(pcminfo_capture), snd_pcm_info_get_name(pcminfo_capture));
                    dev->card_index = card_no;
                    dev->device_index = device_no;
                    sprintf(dev->card_id, card);
                    sprintf(dev->device_id, device);
                    sprintf(dev->device_name, snd_pcm_info_get_name(pcminfo_capture));
                    found = true;
                    break;
                }
            }

            snd_ctl_close(handle);
        }
    }
    return !found;
}

void* audio_source_open(audio_input_t input, audio_sample_rate_t rate, audio_channel_t channels, audio_format_t fmt, size_t size) {
    char* desc;
    switch(input) {
        case AUDIO_SRC_MIC:
        case AUDIO_SRC_VOICE_RECOGNITION:
//            desc = "odroidaudio"; break;
        default:
            desc = "MT202pcs";
    }

    audio_device_t dev;
    if(audio_device_find(desc, &dev, true)) {
        fprintf(stderr, "Failed to find audio source %s\n", desc);
        return NULL;
    }
    printf("Found %s [%d:%d] id: %s, name: %s\n", dev.card_id, dev.card_index, dev.device_index, dev.device_id, dev.device_name);

    int ret;
    snd_pcm_t* handle;
    if(ret = snd_pcm_open(&handle, dev.card_id, SND_PCM_STREAM_CAPTURE, 0)) {
        fprintf(stderr, "Failed to open audio source: %s\n", snd_strerror(ret));
        goto FAIL;
    }

    snd_pcm_hw_params_t *hw_params;
    snd_pcm_hw_params_alloca(&hw_params);
    if(ret = snd_pcm_hw_params_any(handle, hw_params)) {
        fprintf(stderr, "Failed to get any hw params (%s)\n", snd_strerror(ret));
    }

    if(ret = snd_pcm_hw_params_set_access(handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) {
        fprintf(stderr, "Failed to set access type (%s)\n", snd_strerror(ret));
        goto FAIL;
    }

    if(ret = snd_pcm_hw_params_set_format(handle, hw_params, ENCODING[fmt])) {
        fprintf (stderr, "Failed to set sample format (%s)\n", snd_strerror(ret));
        goto FAIL;
    }

    if(ret = snd_pcm_hw_params_set_rate(handle, hw_params, rate, 0)) {
        fprintf (stderr, "Failed to set sample rate (%s)\n", snd_strerror(ret));
        goto FAIL;
    }

    if(ret = snd_pcm_hw_params_set_channels(handle, hw_params, channels)) {
        fprintf (stderr, "Failed to set channel count (%s)\n", snd_strerror(ret));
        goto FAIL;
    }

    if(ret = snd_pcm_hw_params(handle, hw_params)) {
        fprintf (stderr, "Failed to set hw parameters (%s)\n", snd_strerror(ret));
        goto FAIL;
    }

    audio_source_t* src = calloc(1, sizeof(audio_source_t));
    src->handle = handle;
    src->channels = channels;
    src->sample_size = SAMPLE_SIZE[fmt];
    src->buffer.size = size;
    src->buffer.addr = malloc(size);
    src->buffer.timestamp = 0;
    return src;

FAIL:
    return NULL;
}

int audio_source_start(void* ctx) {
    audio_source_t* src = (audio_source_t*)ctx;
    int ret;
    if(src->started) {
        if(ret = snd_pcm_pause(src->handle, 0))
            fprintf(stderr, "Failed to resume capturing: %s\n", snd_strerror(ret));
    } else {
        if(ret = snd_pcm_start(src->handle))
            fprintf(stderr, "Failed to start capturing: %s\n", snd_strerror(ret));
        else
            src->started = true;
    }
    return ret;
}

int audio_source_stop(void* ctx) {
    audio_source_t* src = (audio_source_t*)ctx;
    int ret = snd_pcm_pause(src->handle, 1);
    if(ret)
        fprintf(stderr, "Failed to pause capturing: %s\n", snd_strerror(ret));
    return -ret;
}

int audio_source_close(void* ctx) {
    audio_source_t* src = (audio_source_t*)ctx;
    if(src) {
        snd_pcm_close (src->handle);
        free(src->buffer.addr);
        free(src);
    }
    return 0;
}

audio_buffer_t* audio_source_read(void* ctx) {
    audio_source_t* src = (audio_source_t*)ctx;
    int frames = src->buffer.size / (src->sample_size * src->channels);
    int ret;
    if((ret = snd_pcm_readi(src->handle, src->buffer.addr, frames)) != frames) {
        fprintf(stderr, "Failed to read %d frames (%s)\n", frames, snd_strerror(ret));
        return NULL;
    }
    return &src->buffer;
}