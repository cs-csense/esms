//
// Created by Farley Lai on 9/9/15.
//
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "cpuinfo.h"
#include "thread.h"
#include "odroid/odroid_profiler.h"

static void resetCPUs() {
    //fprintf(stderr, "[resetCPUs] reset %d CPUs\n", nproc());
    for(int i = 0; i < nproc(); i++) {
        if(!cpufreq_online_get(i))
            cpufreq_online_set(i, 1);
        cpufreq_t info;
        cpufreq_get(i, &info);
        if(i < 4) info.freq = info.max = 1400000;
        else if(i < 8) info.freq = info.max = 2000000;
        cpufreq_set(i, &info);
    }
}

static void setUp() {
    if(odroid_open()) {
        perror("Failed to open sensors");
        exit(EXIT_FAILURE);
    }
}

static void tearDown() {
    odroid_close();
}

static void testThreadCPUAffinity() {
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    int ret = thread_getaffinity(pthread_self(), sizeof(cpuset), &cpuset);
    if(ret) {
        fprintf(stderr, "Failed to get thread CPU affinity: (%d) %s\n", ret, strerror(ret));
    } else {
        for(int i = 0; i < nproc(); i++) {
            if(CPU_ISSET(i, &cpuset))
                fprintf(stderr, "CPU[%d] is set\n", i);
        }
    }

    for(int i = nproc() - 1; i >=0; i--) {
        CPU_ZERO(&cpuset);
        CPU_SET(i, &cpuset);
        ret = thread_setaffinity(pthread_self(), sizeof(cpuset), &cpuset);
//        int ret = sched_setaffinity(getpid(), sizeof(cpuset), &cpuset);
        if(ret) {
            fprintf(stderr, "Failed to set affinity to CPU[%d]: (%d) %s\n", i, ret, strerror(ret));
        } else {
            fprintf(stderr, "Succeeded to set affinity to CPU[%d]\n", i);
        };
    }
}

static void testBenchmark(const char* benchmark, const char* cfg, int iterations, int a7, int a7f, int a15, int a15f) {
    // Configure CPUs
    int PROFILER = a7 == 4 ? 0 : a7;
    int duration = 300;
    int interval = 200;
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    for(int i = 0; i < 4; i++) {
        if(i < a7) {
            CPU_SET(i, &cpuset);
            //fprintf(stderr, "A7 CPU_SET(%d, &cpuset)\n", i);
        } else if(i != PROFILER) {
            //cpufreq_online_set(i, 0);
        }
    }
    cpufreq_t info;
    cpufreq_get(0, &info);
    info.max = a7f == 0 ? 500000 : a7f;
    sprintf(info.governor, "performance");
    cpufreq_set(0, &info);

    for(int i = 0; i < 4; i++) {
        if(i < a15) {
            CPU_SET(4 + i, &cpuset);
            //fprintf(stderr,  "A15 CPU_SET(%d, &cpuset)\n", 4 + i);
        } else {
            //cpufreq_online_set(4 + i, 0);
        }
    }
    info.max = a15f == 0 ? 200000 : a15f;
    sprintf(info.governor, "performance");
    cpufreq_set(4, &info);

    // Run benchmark
    setUp();
    void* profiler = odroid_profiler_start(duration, interval, PROFILER);
    if(profiler) {
        int ret = thread_setaffinity(pthread_self(), sizeof(cpuset), &cpuset);
        if(ret) {
            fprintf(stderr, "Failed to set benchmark CPU(s) affinity %d/%d: (%d) %s\n", CPU_COUNT(&cpuset), nproc(), ret, strerror(ret));
        } else {
            char wd[64];
            char perf[128];
            char cmd[192];
            sprintf(wd, "/home/odroid/Downloads/build/%s/options-%d-%s", benchmark, a7 + a15, cfg);
            sprintf(perf, "perf-%dA7-%d-%dA15-%d.log", a7, a7f, a15, a15f);
            sprintf(cmd, "perf stat -o %s -e cycles,instructions,branches,branch-misses,cache-references,cache-misses,cpu-migrations ./streamit -i %d", perf, iterations);
            if(chdir(wd)) {
                fprintf(stderr, "Failed to chdir(%s): %s\n", wd, strerror(errno));
            } else {
                //fprintf(stderr, "%s\n", cmd);
                system(cmd);
            }
        }
        odroid_profiler_stop(profiler);
        char filename[128];
        sprintf(filename, "profile-%dA7-%d-%dA15-%d.csv", a7, a7f, a15, a15f);
        odroid_profiler_save(profiler, filename);
    }
    tearDown();
}

int main(int argc, const char* argv[]) {
    int a7   = 0;
    int a15  = 0;
    int a7f  = 0;
    int a15f = 0;
    int iterations = -1;
    const char* benchmark = NULL;
    const char* cfg = NULL;
    for(int i = 1; i < argc; i++) {
        if(!strcmp(argv[i], "-a7"))
            a7 = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-a15"))
            a15 = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-a7f"))
            a7f = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-a15f"))
            a15f = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-i"))
            iterations = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-b"))
            benchmark = argv[++i];
        else if(!strcmp(argv[i], "-cfg"))
            cfg = argv[++i];
        else if(!strcmp(argv[i], "i"))
            iterations = atoi(argv[++i]);

    }
    if(a7 == 0) a7f = 0;
    if(a15 == 0) a15f = 0;
    if((a7 <= 0 && a15 <= 0) || (a7f <= 0 && a15f <= 0) || cfg == NULL || iterations < 0) {
        fprintf(stderr, "Example: benchmark -i 40000000 -b Repeater -cfg greedier.O2 -a7 4 -a7f 1400000 -a15 4 -a15f 2000000\n");
        exit(EXIT_FAILURE);
    }

    //resetCPUs();
    atexit(resetCPUs);
    //testThreadCPUAffinity();
    testBenchmark(benchmark, cfg, iterations, a7, a7f, a15, a15f);
    return 0;
}