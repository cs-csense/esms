//
// Created by Farley Lai on 7/30/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <gtest/gtest.h>
#include "thread.h"

using namespace testing;

void* run(void* arg) {
    printf("child[%u] tid: %lu forked by parent[%u]\n", getpid(), pthread_self(), getppid());
    return (void*)0;
}

TEST(OdroidThreadTest, test_thread) {
    thread_t thrd;
    ASSERT_EQ(0, thread_create(&thrd, run, NULL));
    ASSERT_EQ(0, thread_join(thrd));
#ifdef __ANDROID__
    ASSERT_NE(thrd, getpid());
#else
    ASSERT_NE(thrd, pthread_self());
#endif
    printf("parent[%u] tid: %u forks child[%u]\n", getpid(), pthread_self(), thrd);
}