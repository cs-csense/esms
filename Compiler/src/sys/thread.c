#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#ifndef __USE_BSD
#define __USE_BSD
#endif
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "thread.h"
//#define __ANDROID__

thread_t thread_self() {
#ifdef __ANDROID__
    return getpid();
#else
    return pthread_self();
#endif
}

int thread_sleep(int ms) {
    return usleep(ms * 1000);
}

int thread_create(thread_t* thrd, void *(*start)(void *), void *restrict arg) {
#ifdef __ANDROID__
    pid_t pid = fork();
    switch(pid) {
        case -1:
            perror("Failed to create a process as thread");
            return -1;
        case 0:
            // child
            exit((int)start(arg));
        default:
            // parent
            *thrd = pid;
            return 0;
    }
#else
    return pthread_create(thrd, NULL, start, arg);
#endif
}

#include<errno.h>
int thread_join(thread_t thrd) {
#ifdef __ANDROID__
    int ret = waitpid(thrd, NULL, 0);
    if (ret == thrd)
        return 0;
    else {
        fprintf(stderr, "Failed to join chinld process[%u != %u]: %s", thrd, ret, strerror(errno));
        return -1;
    }
#else
    return pthread_join(thrd, NULL);
#endif
}

void thread_exit(void* ret) {
#ifdef __ANDROID__
    exit((int)ret);
#else
    pthread_exit(ret);
#endif
}

#ifndef __APPLE__
int thread_getaffinity(thread_t thrd, size_t sz, cpu_set_t* cpuset) {
#ifdef __ANDROID__
    return sched_getaffinity(thrd, sz, cpuset);
#else
    return pthread_getaffinity_np(thrd, sz, cpuset);
#endif
}

int thread_setaffinity(thread_t thrd, size_t sz, cpu_set_t* cpuset) {
#ifdef __ANDROID__
    return sched_setaffinity(thrd, sz, cpuset);
#else
    return pthread_setaffinity_np(thrd, sz, cpuset);
#endif
}
#endif
