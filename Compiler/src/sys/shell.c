#include<stdio.h>
#include<stdlib.h>

int shell(const char* cmd, char buf[], size_t size) {
    FILE* pipe = popen(cmd, "r" );
    if (pipe == NULL)
        return -1;

    size_t sz = fread(buf, 1, size, pipe);
    if(sz > 0) {
        if(buf[sz - 1] == '\n')
            buf[sz - 1] = '\0';
        else
            buf[sz] = '\0';
    }
    pclose(pipe);
    //printf("shell: %s: '%s\'\n", cmd, buf);
    return sz == 0 ? -1 : 0;
}

int shell_atoi(const char* cmd) {
    char buf[12];
    if(shell(cmd, buf, sizeof(buf))) {
        perror(cmd);
        return -1;
    }
    return atoi(buf);
}

int shell_run(const char* cmd) {
    return system(cmd);
}
