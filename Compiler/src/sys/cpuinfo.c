#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include "cpuinfo.h"
#include "shell.h"

int nproc() {
    //return shell_atoi("nproc --all");
    return sysconf(_SC_NPROCESSORS_CONF);
}

int cpufreq_online_get(int cpu) {
    char cmd[64];
    sprintf(cmd, "cat /sys/devices/system/cpu/cpu%d/online", cpu);
    return shell_atoi(cmd);
}

int cpufreq_online_set(int cpu, int online) {
    if(online != cpufreq_online_get(cpu)) {
        char cmd[96];
        sprintf(cmd, "echo %d | sudo tee /sys/devices/system/cpu/cpu%d/online > /dev/null", online, cpu);
        return shell_run(cmd);
    }
    return 0;
}

int cpufreq_get(int cpu, cpufreq_t* info) {
    char cmd[64];
    char buf[32];
    if(!cpufreq_online_get(cpu)) return -1;
    sprintf(cmd, "cpufreq-info -c %d -l", cpu);
    if(shell(cmd, buf, sizeof(buf))) {
        perror(cmd);
        return -1;
    }

    int min, max, freq;
    sscanf(buf, "%d %d", &min, &max);
    sprintf(cmd, "cpufreq-info -c %d -f", cpu);
    freq = shell_atoi(cmd);
    sprintf(cmd, "cat /sys/devices/system/cpu/cpu%d/cpufreq/scaling_governor", cpu);
    if(shell(cmd, buf, sizeof(buf))) {
        perror(cmd);
        return -1;
    }

    info->min = min;
    info->max = max;
    info->freq = freq;
    strcpy(info->governor, buf);
    return 0;
}

int cpufreq_set(int cpu, cpufreq_t* info) {
    char cmd[64];
    if(!cpufreq_online_get(cpu)) return -1;
    sprintf(cmd, "cpufreq-set -c %d -d %d", cpu, info->min);
    shell_run(cmd);
    sprintf(cmd, "cpufreq-set -c %d -u %d", cpu, info->max);
    shell_run(cmd);
    //No work on odroid
    //sprintf(cmd, "cpufreq-set -c %d -f %d", cpu, info->freq);
    //system(cmd);
    sprintf(cmd, "cpufreq-set -c %d -g %s", cpu, info->governor);
    shell_run(cmd);
    return 0;
}

void cpufreq() {
    int n = nproc();
    cpufreq_t cpu[n];
    for(int i = 0; i < n; i++) {
        if(!cpufreq_online_get(i))
            continue;
        if(cpufreq_get(i, &cpu[i])) {
            printf("CPU[%d]: %s\n", i, strerror(errno));
            continue;
        }
        printf("CPU[%d] min: %d, max: %d, freq: %d, governor: %s\n", i, cpu[i].min, cpu[i].max, cpu[i].freq, cpu[i].governor);
    }
}

int cpustat_get(cpustat_t stat[]) {
    FILE* fp = fopen("/proc/stat", "r");
    if (fp == NULL) {
        perror("Failed to open /proc/stat");
        return -1;
    }

    char buf[80] = {0,};
    char cpuid[8] = "cpu";
    void* ret = fgets(buf, 80, fp);
    while(fgets(buf, 80, fp) && !strncmp(buf, "cpu", 3) && strncmp(buf, "intr", 4)) {
        int i = buf[3] - '0';
        sscanf(buf, "%s %d %d %d %d %d %d %d", 
                cpuid, &stat[i].user, &stat[i].nice, &stat[i].sys, &stat[i].idle, &stat[i].iowait, &stat[i].irq, &stat[i].softirq);
    }

    fclose(fp);
    return 0;
}

int cpustat_user(cpustat_t* stat) {
    return stat->user + stat->nice;
}

int cpustat_sys(cpustat_t* stat) {
    return stat->sys + stat->irq + stat->softirq;
}

int cpustat_idle(cpustat_t* stat) {
    return stat->idle + stat->iowait;
}

float cpustat_util(cpustat_t* prev, cpustat_t* cur) {
    int user = cpustat_user(cur) - cpustat_user(prev);
    int sys = cpustat_sys(cur) - cpustat_sys(prev);
    int idle = cpustat_idle(cur) - cpustat_idle(prev);
    float busy = user + sys;
    float total = busy + idle;
    return busy / total;
}
