#include <stddef.h>
#include "timer.h"

long long timestamp() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return 1000000LL * tv.tv_sec + tv.tv_usec;
}
