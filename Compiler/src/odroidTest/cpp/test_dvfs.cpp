//
// Created by Farley Lai on 7/30/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <gtest/gtest.h>

#include "thread.h"
#include "odroid/odroid_profiler.h"

using namespace testing;

static inline double testMemoryBound(int workload) {
    double sum[8192];
    for(int i = 0; i < 35000 * workload; i++) {
        for(int j = 0; j < 8192-63; j++) {
            sum[j] = sum[j] + sum[j+1] * sum[j+2] / sum[j+3] / sum[j+4];
        }
    }
    return sum[1];
}

static void setUp() {
    if(odroid_open()) {
        perror("Failed to open sensors");
        exit(EXIT_FAILURE);
    }
}

static void tearDown() {
    odroid_close();
}

TEST(OdroidDVFSTest, test_dvfs) {
    int PROFILER = 0;
    int BENCHMARK = 4;
    int duration = 1;
    int interval = 100;
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(BENCHMARK, &cpuset);
    setUp();
    void* profiler = odroid_profiler_start(duration, interval, PROFILER);
    if(thread_setaffinity(pthread_self(), sizeof(cpuset), &cpuset)) {
        perror("Failed to set benchmark CPU affinity");
    } else {
        //testESMS(argv[1], workload);
        double ret;
        //ret = testCPUBound(workload);
        ret = testMemoryBound(1);
        fprintf(stdout, "ret = %.2f\n", ret);
    }
    odroid_profiler_stop(profiler);
    odroid_profiler_info(profiler, false);
    odroid_profiler_save(profiler, "test_dvfs.csv");
    tearDown();
}