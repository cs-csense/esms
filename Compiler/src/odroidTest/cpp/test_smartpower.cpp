//
// Created by Farley Lai on 7/30/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <gtest/gtest.h>

#include "thread.h"
#include "smartpower.h"
#include "odroid/odroid_profiler.h"

using namespace testing;

static void setUp() {
}

static void tearDown() {
}

TEST(OdroidSmartPowerTest, test_SmartPower) {
    hid_device *device = smartpower_open();
    if (device) {
        unsigned char buf[65];
        sample_t sample;
        for(int i = 0; i < 5; i++) {
            if(smartpower_sample(device, &sample) == -1) {
                smartpower_close(device);
                FAIL();
            }
            fprintf(stderr, "%.3fV, %.3fA, %.3fW, %.3fwh\n", sample.sysV, sample.sysA, sample.sysW, sample.sysWh);
            thread_sleep(1000);
        }
        smartpower_close(device);
    } else {
        FAIL() << "Failed to open device\n";
    }
}