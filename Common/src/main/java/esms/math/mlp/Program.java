package esms.math.mlp;

import net.sf.javailp.Linear;
import net.sf.javailp.Operator;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryGurobi;
import net.sf.javailp.VarType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by farleylai on 2/17/15.
 */
public class Program {
    private static int M = 10000;
    private static Solver mSolver;
    private static Pattern TERM = Pattern.compile("([+-]?\\d*)(\\w+)");

    public static Linear toLinear(String exp) {
//        Log.i("toLinear(): exp=" + exp);
        Matcher m = TERM.matcher(exp);
        List<Object> vars = new ArrayList<>();
        List<Number> coeffs = new ArrayList<>();
        while(m.find()) {
            String coeff = m.group(1);
            String var = m.group(2);
            int coefficient;
            if(coeff.equals("") || coeff.equals("+"))
                coefficient = 1;
            else if(coeff.equals("-"))
                coefficient = -1;
            else
                coefficient = Integer.parseInt(coeff);
            if(vars.contains(var)) {
//                Log.i("var %s is contained and combined\n", var);
                int i = vars.indexOf(var);
                coefficient += coeffs.get(i).intValue();
                coeffs.set(i, coefficient);
            } else {
                vars.add(var);
                coeffs.add(coefficient);
            }
        }

        Linear linear = new Linear();
        for(int i = 0; i < vars.size(); i++)
            linear.add(coeffs.get(i), vars.get(i));
        if(linear.size() == 0)
            throw new RuntimeException("Invalid linear expression");
//        Log.i("toLinear(): parsed linear %s\n", linear);
        return linear;
    }

    public static String summate(String... vars) {
        return String.join("+", vars);
    }

    public static String diff(String... vars) {
        return String.join("-", vars);
    }

    static {
//        SolverFactory factory = new SolverFactoryLpSolve();
        SolverFactory factory = new SolverFactoryGurobi();
//        SolverFactory factory = new SolverFactoryCPLEX();
        factory.setParameter(Solver.VERBOSE, 1);
        factory.setParameter(Solver.TIMEOUT, 600);
        mSolver = factory.get();
    }

    private Problem mProblem;
    private Result mSolution;
    private int BV = 0;

    private String nextBV() {
        String var = "BV" + BV++;
        mProblem.setVarType(var, VarType.INT);
        mProblem.setVarUpperBound(var, 1);
        return var;
    }

    public Program() {
        this.mProblem = new Problem();
    }

    public Program(Program p) {
        this();
        mProblem.setObjective(p.mProblem.getObjective());
        mProblem.getConstraints().addAll(p.mProblem.getConstraints());
        mSolution = p.mSolution;
        BV = p.BV;
    }

    public Program minimize(String objective) {
        mProblem.setObjective(toLinear(objective), OptType.MIN);
        return this;
    }

    public Program maximize(String objective) {
        mProblem.setObjective(toLinear(objective), OptType.MAX);
        return this;
    }

    /**
     * V = max(var1, var2,...)
     * => V - var1 >= 0
     * => V - var2 >= 0
     * => ...
     *
     * @param V
     * @param vars
     * @return
     */
    public Program max(String V, String... vars) {
        for(String var: vars)
            GE(String.format("%s-%s", V, var), 0);
        return this;
    }

    public Program sum(String V, String... vars) {
        return EQ(V + "-" + String.join("-", vars), 0);
    }


    /**
     * |A - B| = V
     * => -V <= A - B <= V
     * => A - B - V <= 0
     * => A - B + V >= 0
     * @param lhs
     * @param V
     * @return
     */
    public Program abs(String V, String lhs) {
        LE(String.format("%s-%s", lhs, V), 0);
        GE(String.format("%s+%s", lhs, V), 0);
        return this;
    }

    private Program add(Linear linear, Operator op, int value) {
        for(Object var: linear.getVariables()) {
            mProblem.setVarType(var, VarType.INT);
            mProblem.setVarLowerBound(var, 0);
        }
        mProblem.add("C" + mProblem.getConstraints().size(), linear, op, value);
        return this;
    }

    public Program LE(String lhs, int value) {
        add(toLinear(lhs), Operator.LE, value);
        return this;
    }

    public Program GE(String lhs, int value) {
        add(toLinear(lhs), Operator.GE, value);
        return this;
    }

    public Program EQ(String lhs, int value) {
        add(toLinear(lhs), Operator.EQ, value);
        return this;
    }

    /**
     * A - B != V
     * => A - B >= V + 1 - M * BV      => A - B + M * BV >= V + 1      for BV = 0
     * => A - B <= V - 1 + M(1-BV)     => A - B + M * BV <= V + M - 1  for BV = 1
     *
     * @param lhs
     * @return
     */
    public Program NE(String lhs, int value) {
        String BV = nextBV();
        add(toLinear(String.format("%s+%d%s", lhs, M, BV)), Operator.GE, value + 1);
        add(toLinear(String.format("%s+%d%s", lhs, M, BV)), Operator.LE, value+M-1);
        return this;
    }

    /**
     * |A - B| >= V
     * => A - B >= V - M * BV           => A - B + M * BV >= V
     * => A - B <= -V + M (1 - BV)      => A - B + M * BV <= M - V
     *
     * @param lhs
     * @param value
     * @return
     */
    public Program absGE(String lhs, int value) {
        String BV = nextBV();
        add(toLinear(String.format("%s+%d%s", lhs, M, BV)), Operator.GE, value);
        add(toLinear(String.format("%s+%d%s", lhs, M, BV)), Operator.LE, M - value);
        return this;
    }

    /**
     * |A - B| <= V
     * => A - B <= V
     * => A - B >= -V
     *
     * @param lhs
     * @param value
     * @return
     */
    public Program absLE(String lhs, int value) {
        Linear linear = toLinear(lhs);
        LE(lhs, value);
        GE(lhs, -value);
        return this;
    }

    /**
     * |A - B| != V
     * => A - B != V
     * => A - B != -V
     * => V >= 0
     *
     * @param lhs
     * @param value
     * @return
     */
    public Program absNE(String lhs, int value) {
        NE(lhs, value);
        NE(lhs, -value);
        return this;
    }

    public Program solve() {
        mSolution = mSolver.solve(mProblem);
        return this;
    }

    public boolean solved() {
        return mSolution != null;
    }

    public Number get(String var) {
        return mSolution.get(var);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Objective: ").append(mProblem.getOptType()).append(" ").append(mProblem.getObjective()).append("\n");
//        for(Constraint c: mProblem.getConstraints())
//            builder.append(c.getName()).append(": ").append(c).append("\n");
        if(mSolution != null) {
            List<String> variables = new ArrayList<>();
            for(Object var: mProblem.getVariables()) {
                String name = var.toString();
                if(name.startsWith("BV") || name.startsWith("I") || name.startsWith("W")
                                         || name.equals("S") || name.equals("L") || name.equals("CC"))
                    continue;
                variables.add(var.toString());
            }
            Collections.sort(variables);
            builder.append("Solution Objective: ").append(mSolution.getObjective()).append("\n");
            builder.append("S").append("=").append(mSolution.get("S")).append("\n");
            builder.append("L").append("=").append(mSolution.get("L")).append("\n");
            builder.append("CC").append("=").append(mSolution.get("CC")).append("\n");
            for(String var: variables)
                builder.append(var).append("=").append(mSolution.get(var)).append("\n");
        }
        return builder.toString();
    }
}
