package esms.math;

import org.ejml.UtilEjml;
import org.ejml.data.DenseMatrix64F;
import org.ejml.factory.DecompositionFactory;
import org.ejml.interfaces.decomposition.SingularValueDecomposition;
import org.ejml.ops.SingularOps;
import org.ejml.simple.SimpleMatrix;
import org.ejml.simple.SimpleSVD;

import esms.util.Log;

/**
 * Created by farleylai on 2/17/14.
 */
public class Matrix {
    private static String TAG = Matrix.class.getSimpleName();
    public static final int END = SimpleMatrix.END;
    private SimpleMatrix mMatrix;

    public Matrix(SimpleMatrix M) {
        mMatrix = M;
    }

    public Matrix(int rows, int cols) {
        mMatrix = new SimpleMatrix(rows, cols);
    }

    public Matrix(double[][] values) {
        mMatrix = new SimpleMatrix(values);
    }

    public Matrix(double[] values) {
        mMatrix = new SimpleMatrix(values.length, 1);
        for(int i = 0; i  < values.length; i++)
            mMatrix.set(i, values[i]);
    }

    private SimpleMatrix raw() {
        return mMatrix;
    }

    public int rows() {
        return mMatrix.numRows();
    }

    public int cols() {
        return mMatrix.numCols();
    }

    public Matrix row(int idx) {
        return new Matrix(raw().extractVector(true, idx));
    }

    public Matrix column(int idx) {
        return new Matrix(raw().extractVector(false, idx));
    }

    public double get(int row, int col) {
        return mMatrix.get(row, col);
    }

    public int getInt(int row, int col) {
        return (int) mMatrix.get(row, col);
    }

    public Matrix set(int row, int col, double val) {
        mMatrix.set(row, col, val);
        return this;
    }

    public Matrix set(int row, int column, Matrix M) {
        raw().insertIntoThis(row, column, M.raw());
        return this;
    }

    public Matrix gt(double val) {
        Matrix M = new Matrix(rows(), cols());
        for (int i = 0; i < rows(); i++) {
            for (int j = 0; j < cols(); j++) {
                M.set(i, j, get(i, j) > val ? 1 : 0);
            }
        }
        return M;
    }

    public Matrix extract(int rowStart, int rowEnd, int colStart, int colEnd) {

        return new Matrix(raw().extractMatrix(rowStart, rowEnd == END ? END : rowEnd + 1, colStart, colEnd == END ? END : colEnd + 1));
    }

    public Matrix combine(int row, int column, Matrix M) {
        return new Matrix(raw().combine(row, column, M.raw()));
    }

    public boolean equals(Matrix M, double tol) {
        return mMatrix.isIdentical(M.raw(), tol);
    }

    public boolean equals(Matrix M) {
        return mMatrix.isIdentical(M.raw(), 1E-6);
    }

    public double maxAbs() { return mMatrix.elementMaxAbs(); }

    public Matrix plus(Matrix addend) {
        return new Matrix(raw().plus(addend.raw()));
    }

    public Matrix minus(Matrix subtrahend) {
        return new Matrix(raw().minus(subtrahend.raw()));
    }

    public Matrix mult(Matrix multiplicant) {
        return new Matrix(raw().mult(multiplicant.raw()));
    }

    public double sum() {
        return mMatrix.elementSum();
    }

//    public Matrix sum() {
//        return sum(0, END, true);
//    }

    public Matrix sum(boolean columnwise) {
        return sum(0, END, columnwise);
    }

//    public Matrix sum(int start, int end) {
//        return sum(start, end, true);
//    }

    /**
     * Sum up by rows or columns.
     * Columnwise means summation by rows. Otherwise, summation by columns.
     *
     * @param start
     * @param end
     * @param columnwise
     * @return
     */
    public Matrix sum(int start, int end, boolean columnwise) {
        if (columnwise) {
            Matrix row = row(start);
            end = end == Matrix.END ? rows() - 1 : end;
            for (int i = start + 1; i <= end; i++)
                row = row.plus(row(i));

            return row;
        } else {
            Matrix col = column(start);
            end = end == Matrix.END ? cols() - 1 : end;
            for (int i = start + 1; i <= end; i++)
                col = col.plus(column(i));

            return col;
        }
    }

    public Matrix transpose() {
        return new Matrix(mMatrix.transpose());
    }

    public Matrix svd(boolean compact) {
        SimpleSVD SVD = mMatrix.svd(compact);
        double quality = SVD.quality();
        Log.i(TAG, "quality: " + quality);
        return new Matrix(SVD.getW().extractDiag());
    }

    public int rank() {
        SingularValueDecomposition<DenseMatrix64F> svd = DecompositionFactory.svd(rows(), cols(), false, false, false);
        if( !svd.decompose(mMatrix.getMatrix()))
            throw new RuntimeException("Failed to decompose topology matrix");
        double[] sigma = svd.getSingularValues();
        double tol = Math.max(rows(), cols()) * sigma[0] * UtilEjml.EPS;
        int rank = SingularOps.rank(svd, tol);
        return rank;
    }

    public Matrix nullspace() {
        return new Matrix(mMatrix.svd().nullSpace());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < rows(); i++) {
            for (int j = 0; j < cols(); j++)
//                builder.append(String.format("%.16f", (float)get(i, j)));
                builder.append(String.format("%8d", getInt(i, j)));

            builder.append("\n");
        }

        return builder.toString();
    }
}
