package esms.toolkits;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import esms.util.CommandLine;
import esms.util.Shell;
import esms.util.StageTimer;

/**
 * Created by farleylai on 12/13/14.
 */
public class JavaToolChain extends Toolkit {
    private boolean printf;

    protected JavaToolChain(String src, String build, String target) {
        super("javac -cp .", src, build, target);
        printf = true;
    }

    @Override
    public File executable() {
        return pwd(target());
    }

    public JavaToolChain printf(boolean enable) {
        printf = enable;
        return this;
    }

    @Override
    protected StageTimer doBuild() {
        CommandLine cmd = new CommandLine(name());
        if(!printf) {
            //if(Shell.os() == Shell.OS.MacOS)
            //    cmd.arg("-D'printf(...)='");
            //else {
                // FIXME shell extension for wildcards
                for(String src: src()) {
                    if(src.contains("*")) continue;
                    String target = src + ".new";
                    Shell.run(pwd(), new CommandLine("grep", "-v", "printf", src), new File(target));
                    Shell.run(pwd(), new CommandLine("mv", target, src));
                }
           // }
        }
        cmd.arg(src());
        StageTimer timer = new StageTimer(name() + " Compilation");
        timer.log();
        if(Shell.run(pwd(), cmd) != Shell.EXIT_SUCCESS)
            fail("Failed to run " + cmd);
        timer.log();
        return timer;
    }
}
