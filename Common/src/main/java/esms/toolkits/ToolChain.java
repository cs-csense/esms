package esms.toolkits;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import esms.util.CommandLine;
import esms.util.Shell;
import esms.util.StageTimer;

/**
 * Created by farleylai on 12/13/14.
 */
public class ToolChain extends Toolkit {
    private List<File> mIncludes;
    private List<File> mLibraries;
    private List<String> mCFlags;
    private List<String> mLDFlags;
    private boolean printf;

    protected ToolChain(String name, String src, String build, String target) {
        super(name, src, build, target);
        mIncludes = new ArrayList<>();
        mLibraries = new ArrayList<>();
        mCFlags = new ArrayList<>();
        mLDFlags = new ArrayList<>();
        printf = true;
    }

    @Override
    public File executable() {
        return pwd(target());
    }

    public ToolChain cflag(String flag) {
        mCFlags.add(flag);
        return this;
    }

    public ToolChain include(String... headers) {
        for(String header: headers)
           mIncludes.add(new File(header));
        return this;
    }

    public ToolChain link(String lib) {
        return link(lib, null);
    }

    public ToolChain link(String lib, String path) {
        mLDFlags.add("-l" + lib);
        if(path != null)
            mLibraries.add(new File(path));
        return this;
    }

    public ToolChain printf(boolean enable) {
        printf = enable;
        return this;
    }

    @Override
    protected StageTimer doBuild() {
        CommandLine cmd = new CommandLine(name());
        if(!printf) {
            //if(Shell.os() == Shell.OS.MacOS)
            //    cmd.arg("-D'printf(...)='");
            //else {
                for(String src: src()) {
                    String target = src + ".new";
                    Shell.run(pwd(), new CommandLine("grep", "-v", "printf", src), new File(target));
                    Shell.run(pwd(), new CommandLine("mv", target, src));
                }
           // }
        }
        for(String flag: mCFlags)
            cmd.arg(flag);
        for(File inc: mIncludes)
            cmd.arg("-I" + inc.getAbsolutePath());
        for(File lib: mLibraries)
            cmd.arg("-L" + lib.getAbsolutePath());
        cmd.arg(src());
        cmd.arg("-o", target());
        for(String flag: mLDFlags)
            cmd.arg(flag);
        StageTimer timer = new StageTimer(name() + " Compilation");
        timer.log();
        if(Shell.run(pwd(), cmd) != Shell.EXIT_SUCCESS)
            fail("Failed to run " + cmd);
        timer.log();
        return timer;
    }
}
