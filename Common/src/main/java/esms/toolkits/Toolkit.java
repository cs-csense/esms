package esms.toolkits;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import esms.util.CommandLine;
import esms.util.Formatter;
import esms.util.Log;
import esms.data.Performance;
import esms.util.SSH;
import esms.util.Shell;
import esms.util.StageTimer;

/**
 * Created by farleylai on 12/13/14.
 */
public abstract class Toolkit {
    private String TAG = getClass().getSimpleName();
    private String mName;
    private File mSrcDir;
    private File mResDir;
    private File mBuild;
    private String mTarget;
    private List<File> mSources;
    private List<File> mResources;

    protected Toolkit(String name, String src, String build, String target) {
        this(name, src, src, build, target);
    }

    protected Toolkit(String name, String src, String res, String build, String target) {
        mName = name;
        mSrcDir = new File(src);
        mTarget = target;
        mSources = new ArrayList<>();
        if(res != null) {
            mResDir = new File(res);
            mResources = new ArrayList<>();
        }
        mBuild = new File(build);
    }

    public String name() {
        return mName;
    }

    public File srcDir() {
        return mSrcDir;
    }

    public Toolkit src(String dir, String filename) {
        mSources.add(new File(dir, filename));
        return this;
    }

    public Toolkit src(String filename) {
        mSources.add(new File(mSrcDir, filename));
        return this;
    }

    public String[] src() {
        String[] filenames = new String[mSources.size()];
        for(int i = 0; i < mSources.size(); i++)
            filenames[i] = mSources.get(i).getAbsolutePath();
        return filenames;
    }

    public Toolkit res(String dir, String filename) {
        mResources.add(new File(dir, filename));
        return this;
    }

    public Toolkit res(String filename) {
        mResources.add(new File(mResDir, filename));
        return this;
    }

    public String[] res() {
        String[] filenames = new String[mResources.size()];
        for(int i = 0; i < mResources.size(); i++)
            filenames[i] = mResources.get(i).getAbsolutePath();
        return filenames;
    }

    public File pwd(String filename) {
        return new File(mBuild, filename);
    }

    public File pwd() {
        return mBuild;
    }

    public String target() {
        return mTarget;
    }

    public String target(String suffix) {
        return mTarget + "." + suffix;
    }

    public Toolkit export(File dir) { return this; }

    public Toolkit export(SSH ssh, File dir) { return this; }

    public Toolkit fail(String fmt, Object... args) {
        throw new RuntimeException(String.format(fmt, args));
    }

    public final StageTimer build() throws Exception {
        return build(false);
    }

    public final StageTimer build(boolean clean) throws Exception {
        if(clean) clean();
        if(!mBuild.exists() && !mBuild.mkdirs())
            throw new RuntimeException("Failed to create build directory: " + mBuild.getAbsolutePath() + " from " + System.getProperty("user.dir"));
        for(String res: res())
            Shell.ln(res, pwd().getAbsolutePath());
        return doBuild();
    }

    public File executable() {
        return null;
    }

    protected abstract StageTimer doBuild() throws Exception;
    public Performance run() throws Exception {
        return null;
    };

    public Toolkit clean() {
        Shell.rm(mBuild.getPath());
        return this;
    }

    public Toolkit cachegrind() throws Exception {
        if(executable() != null && executable().exists()) {
            String prog = executable().getName();
            String fmt = "valgrind --tool=cachegrind --branch-sim=yes --cachegrind-out-file=cachegrind.log";
            CommandLine cachegrind = new CommandLine(String.format(fmt), Shell.pwd(prog));
            if (Shell.run(pwd(), cachegrind) != Shell.EXIT_SUCCESS)
                Log.w(TAG, "cachegrind is not available for analyzing cache performance");
        } else
            Log.w(TAG, "No executable to cachegrind");
        return this;
    }


    public Toolkit cachegrind(int size, int associativity, int line) {
        if(executable() != null && executable().exists()) {
            String prog = executable().getName();
            String fmt = "valgrind --tool=cachegrind --branch-sim=yes --I1=%d,%d,%d --D1=%d,%d,%d --LL=%d,%d,%d --cachegrind-out-file=cachegrind.log";
            CommandLine cachegrind = new CommandLine(String.format(fmt, size, associativity, line, size, associativity, line, size * 128, associativity, line), Shell.pwd(prog));
            if (Shell.run(pwd(), cachegrind) != Shell.EXIT_SUCCESS)
                Log.w(TAG, "cachegrind is not available for analyzing cache performance");
        } else
            Log.w(TAG, "No executable to cachegrind");
        return this;
    }

    public Toolkit save(String filename, String lines) throws IOException {
        RandomAccessFile writer = new RandomAccessFile(pwd(filename), "rw");
        writer.write(lines.getBytes());
        writer.close();
        return this;
    }

    public String summary() { return ""; }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Formatter.title("%s Toolkit", mName)).append("\n")
                .append("Target: ").append(mTarget).append("\n")
                .append("Build dir: ").append(mBuild.getPath()).append("\n")
                .append("Source dir: ").append(mSrcDir.getPath()).append("\n");
        if(!mSources.isEmpty()) {
            builder.append("Sources: ");
            for(File src: mSources)
                builder.append(src.getName()).append(" ");
            builder.append("\n");
        }
        if(mResDir != null && !mResources.isEmpty()) {
            builder.append("Resource dir: ").append(mResDir.getPath()).append("\n");
            builder.append("Resources: ");
            for(File res: mResources)
                builder.append(res.getName()).append(" ");
            builder.append("\n");
        }
        return builder.toString();
    }
}
