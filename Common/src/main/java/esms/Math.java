package esms;

/**
 * Created by farleylai on 2/16/14.
 */
public class Math {
    public static int[][] zeros(int rows, int cols) {
        return new int[rows][cols];
    }

    public static int[][] ones(int rows, int cols) {
        int[][] ones = new int[rows][cols];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                ones[i][j] = 1;

        return ones;
    }

    public static int digits(int val) {
        return val < 10 ? 1
                : val < 100 ? 2
                : val < 1000 ? 3
                : val < 10000 ? 4
                : val < 100000 ? 5 : 6;

    }

    public static int gcd(int a, int b) {
        return b > 0 ? gcd(b, a % b) : a;
    }
}
