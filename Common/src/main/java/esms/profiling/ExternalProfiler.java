package esms.profiling;

import esms.util.Log;

/**
 * An external profiler stub that allows integer state logging.
 */
public class ExternalProfiler extends Profiler {
    protected String _timestamp;
    public ExternalProfiler startService() {
        return this;
    }
    public ExternalProfiler stopService() {
        return this;
    }

    public final ExternalProfiler startProfiling() {
        Log.d(tag(), "Starting profiling");
        _timestamp = timestamp();
        doStartProfiling();
        return this;
    }

    public final ExternalProfiler stopProfiling() {
        doStopProfiling();
        Log.d(tag(), "Stopped profiling");
        return this;
    }

    public ExternalProfiler doStartProfiling() { return this;}
    public ExternalProfiler doStopProfiling() { return this;}

    //  App-defined state logging

    public int state() { return 0; }
    public String description() { return ""; }
    public String description(int state) { return description(); }
    public ExternalProfiler update() {
        return this;
    }
    public ExternalProfiler update(int state) {
        return this;
    }
    public ExternalProfiler update(String desc) {
        return this;
    }
    public ExternalProfiler update(int state, String desc) {
        return this;
    }
}
