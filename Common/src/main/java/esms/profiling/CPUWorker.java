package esms.profiling;

import esms.profiling.Worker;

/**
 * Created by farleylai on 6/14/16.
 */
public class CPUWorker extends Worker {
    private double[][] M;
    private double _v;
    private int _m;

    public CPUWorker(int m, int wk) {
        super(wk);
        M = new double[m][m];
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < m; j++)
                M[i][j] = (i + j) / 0.5678;
        }
        _m = m;
    }

    public void work() {
        for(int i = 0; i < _m; i++) {
            for(int j = 0; j < _m; j++) {
                for(int k = 0; k < _m; k++)
                    _v += M[i][k] * M[k][j];
            }
        }
    }
}

