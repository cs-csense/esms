package esms.profiling;

/**
 * Created by farleylai on 6/14/16.
 */
public class Worker implements Runnable {
    private int _wk;
    private boolean _running;
    private Thread _thread;

    public Worker(int wk) {
        _wk = wk;
    }

    protected void work() {}

    public final void start() {
        if(_thread == null) {
            _running = true;
            _thread = new Thread(this, getClass().getSimpleName());
            _thread.start();
        }
    }

    public final void stop() {
        _running = false;
    }

    public final void join() throws InterruptedException {
        if(_thread != null) {
            _thread.join();
        }
    }

    @Override
    public final void run() {
        while(_running && _wk-- > 0)
            work();
    }
}
