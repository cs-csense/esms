package esms.profiling;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import esms.api.Loggable;

/**
 * Created by farleylai on 12/5/15.
 */
public class Profiler extends Loggable {
    public String timestamp() {
        SimpleDateFormat df = new SimpleDateFormat();
        df.applyPattern("yyyyMMdd-HHmmss");
        return df.format(new Date());
    }

    public void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the profiling stats in the file system.
     *
     * @param file the file to save in the file system
     * @return the profiler instance
     * @throws IOException
     */
    public Profiler save(File file) throws IOException { return this; }
}
