package esms.profiling;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by farleylai on 6/14/16.
 */
public class WorkerPool {
    List<Worker> _workers = new ArrayList<>();
    public void add(Worker worker) {
        if(worker != null)
            _workers.add(worker);
    }

    public void start() {
        for(Worker w: _workers)
            w.start();
    }

    public void stop() {
        for(Worker w: _workers) w.stop();
        for(Worker w: _workers) {
            while(true) {
                try {
                    w.join();
                    break;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
