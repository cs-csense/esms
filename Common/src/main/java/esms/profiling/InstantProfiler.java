package esms.profiling;

import java.io.File;
import java.io.IOException;

/** In program profiler is used during execution.
 */
public class InstantProfiler extends Profiler {
    @Override
    public InstantProfiler save(File file) throws IOException { return this; }

    /**
     * Profiles some system stats instantly.
     *
     * @param cid App-defined component id
     * @param eid App-defined event id
     * @return the profiler instance
     */
    public InstantProfiler profile(int cid, int eid) { return this; }
}
