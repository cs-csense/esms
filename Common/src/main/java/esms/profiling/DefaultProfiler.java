package esms.profiling;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;

import esms.usage.BlockStat;
import esms.usage.RUsage;
import esms.runtime.Program;

public class DefaultProfiler extends InstantProfiler {
    private RUsage mRU;
    private BlockStat mIO;
    private ByteBuffer mRecords;

    public DefaultProfiler() {
        mRU = new RUsage();
        mIO = new BlockStat();
        mRecords = ByteBuffer.allocateDirect(1024 * 1024);
    }

    @Override
    public DefaultProfiler profile(int cid, int eid) {
        if(mRecords.remaining() < 32) {
            ByteBuffer prev = mRecords;
            mRecords = ByteBuffer.allocateDirect(2 * prev.capacity());
            prev.flip();
            mRecords.put(prev);
        }
        mRU.get();
        mRecords.putInt(cid);
        mRecords.putInt(eid);
        mRecords.putLong(mRU.real());
        mRecords.putLong(mRU.cpu());
        return this;
    }

    @Override
    public DefaultProfiler save(File file) throws IOException {
        PrintWriter out = new PrintWriter(file);
        out.println("cid, eid, real(ms), cpu(us)");
        mRecords.flip();
        while(mRecords.hasRemaining())
            out.printf("%d, %d, %d, %d\n", mRecords.getInt(), mRecords.getInt(), mRecords.getLong(), mRecords.getLong());
        out.close();
        return this;
    }
}
