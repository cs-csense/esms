package esms.usage;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import esms.runtime.Resource;
import esms.util.Shell;

/**
* Created by farleylai on 11/4/14.
*/
public class RUsage extends Usage {
    public static RUsage parse(String[] info) {
//      Arrays.stream(info).forEach(Log::i);
        RUsage ru = new RUsage();
        switch (Shell.os()) {
            case MacOS: {
//        0.00 real         0.00 user         0.00 sys
//        659456  maximum resident set size
//        0  average shared memory size
//        0  average unshared data size
//        0  average unshared stack size
//        173  page reclaims
//        1  page faults
//        0  swaps
//        0  block input operations
//        0  block output operations
//        0  messages sent
//        0  messages received
//        0  signals received
//        2  voluntary context switches
//        3  involuntary context switches
                Scanner sc = new Scanner(info[0]);
                ru.timestamp((long)(sc.nextDouble() * 1000)); sc.next();
                ru.field(0, (long)(sc.nextDouble() * 1000000)); sc.next();
                ru.field(1, (long)(sc.nextDouble() * 1000000)); sc.next();
                for(int i = 1; i < info.length; i++) {
                    sc = new Scanner(info[i]);
                    ru.field(1+i, sc.nextInt());
                }
                break;
            }
            case Linux: {
//                0.04user 0.00system 0:00.04elapsed ?%CPU (0avgtext+0avgdata 416maxresident)k
//                0inputs+0outputs (0major+145minor)pagefaults 0swaps
                Pattern regex = Pattern.compile("([-+]?[0-9]*\\.?[0-9]*)user ([-+]?[0-9]*\\.?[0-9]*)system (\\d+):([-+]?[0-9]*\\.?[0-9]*)elapsed .+%CPU \\((\\d+)avgtext\\+(\\d+)avgdata (\\d+)maxresident\\)k");
                Matcher matcher = regex.matcher(info[0]);
                if(matcher.find()) {
                    int mins = Integer.parseInt(matcher.group(3));
                    ru.timestamp((long)(mins * 60 + Double.parseDouble(matcher.group(4)) * 1000000));
                    ru.field(0, (long)(Double.parseDouble(matcher.group(1)) * 1000000));
                    ru.field(1, (long)(Double.parseDouble(matcher.group(2)) * 1000000));
                    ru.field(2, (long)(Integer.parseInt(matcher.group(7)) * 1024));
                } else
                    throw new RuntimeException("found no matched rusage");
                regex = Pattern.compile("(\\d+)inputs\\+(\\d+)outputs \\((\\d+)major\\+(\\d+)minor\\)pagefaults (\\d+)swaps");
                matcher = regex.matcher(info[1]);
                if(matcher.find()) {
                    ru.field(9, Integer.parseInt(matcher.group(1)));
                    ru.field(10, Integer.parseInt(matcher.group(2)));
                    ru.field(6, Integer.parseInt(matcher.group(4)));
                    ru.field(7, Integer.parseInt(matcher.group(3)));
                } else
                    throw new RuntimeException("found no matched rusage");
                break;
            }
            default:
                throw new RuntimeException("Unsupported platform for rusage");
        }
        return ru;
    }

    public RUsage() {
        super("rusage");
        header(
                "utime", "stime",               // us
                "maxrss",                       // bytes
                "ixrss", "idrss", "isrss",      // XXX
                "minflt", "majflt",
                "nswap",                        // XXX
                "inblock", "oublock",
                "msgsnd", "msgrcv", "nsignals", // XXX
                "nvcsw", "nivcsw"
        );
    }

    public long utime() { return field(0); }
    public long stime() { return field(1); }
    public long maxrss() { return field(2); }
    public long ixrss() { return field(3); }
    public long idrss() { return field(4); }
    public long isrss() { return field(5); }
    public long minflt() { return field(6); }
    public long majflt() { return field(7); }
    public long nswap() { return field(8); }
    public long inblock() { return field(9); }
    public long oublock() { return field(10); }
    public long nvcsw() { return field(14); }
    public long nivcsw() { return field(15); }
    public long real() { return timestamp(); }
    public long cpu() { return utime() + stime(); }
    public double util() { return 1.0 * cpu() / (real() * 1000); }

    @Override
    public native RUsage get();
}
