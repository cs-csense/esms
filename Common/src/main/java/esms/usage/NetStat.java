package esms.usage;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by farleylai on 6/24/16.
 */
public class NetStat extends Usage {
    public static final String WIFI = "wlan0";
    public static final String CELL = "rmnet_data0";

    private String[] _paths;
    public NetStat(String dev) {
        super(dev);
        header("rx_packets", "rx_bytes", "tx_packets", "tx_bytes");
        _paths = new String[count()];
        for(int i = 0; i < count(); i++)
            _paths[i] = String.format("/sys/devices/virtual/net/%s/statistics/%s", dev, header(i));
    }

    public NetStat() {
        this(WIFI);
    }

    @Override
    public NetStat get() {
        try {
            for (int i = 0; i < count(); i++) {
                RandomAccessFile stat = new RandomAccessFile(_paths[i], "r");
                timestamp(System.currentTimeMillis());
                field(i, Long.parseLong(stat.readLine()));
                stat.close();
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
        return this;
    }
}
