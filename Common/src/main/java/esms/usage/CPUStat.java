package esms.usage;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import esms.runtime.Resource;
import esms.util.Log;

/** At least the folowing 7 fields are recorded.
 *
 * user: normal processes executing in user mode
 * nice: niced processes executing in user mode
 * system: processes executing in kernel mode
 * idle: twiddling thumbs
 * iowait: waiting for I/O to complete
 * irq: servicing interrupts
 * softirq: servicing softirqs
 */
public class CPUStat extends Usage {
    public static final String[] HEADER = new String[] { "user", "nice", "system", "idle", "iowait", "irq", "softirq", "steal", "guest", "guest_nice" };
    public static class CoreStat extends Usage {
        public final int[] freqs;
        public CoreStat(int i, int[] freqs) {
            super("cpu" + i);
            String[] hdr = new String[HEADER.length + freqs.length];
            System.arraycopy(HEADER, 0, hdr, 0, HEADER.length);
            for(int j = 0; j < freqs.length; j++)
                hdr[HEADER.length + j] = String.valueOf(freqs[j]);
            header(hdr);
            this.freqs = freqs;
        }
    }

    private static final String PROC_STAT = "/proc/stat";
    private static final String FMT_TIME_IN_STATE = "/sys/devices/system/cpu/%s/cpufreq/stats/time_in_state";
    private static final Pattern CPUX = Pattern.compile("^cpu(\\d*).+");
    private static List<int[]> FREQS = new ArrayList<>();
    private static int CORES = 0;
    public static int size() { return CORES; }
    public static int steps(int i) { return FREQS.get(i).length; }
    static {
        try {
            RandomAccessFile stat = new RandomAccessFile(PROC_STAT, "r");
            String line;
            while((line = stat.readLine()) != null) {
                Log.d(CPUStat.class.getSimpleName(), line);
                Matcher matcher = CPUX.matcher(line);
                if(matcher.matches()) {
                    if(matcher.group(1).isEmpty()) continue;
                    String timeInState = String.format(Locale.ENGLISH, FMT_TIME_IN_STATE, "cpu" + CORES++);
                    try {
                        RandomAccessFile freqsUsage = new RandomAccessFile(timeInState, "r");
                        List<Integer> steps = new ArrayList<>();
                        while ((line = freqsUsage.readLine()) != null)
                            steps.add(Integer.parseInt(line.split("\\s+")[0]));
                        freqsUsage.close();
                        int[] freqs = new int[steps.size()];
                        for (int i = 0; i < steps.size(); i++)
                            freqs[i] = steps.get(i);
                        FREQS.add(freqs);
                        freqsUsage.close();
                    } catch(IOException e) {
                        Log.w(CPUStat.class.getSimpleName(), e);
                    }
                } else break;
            }
            stat.close();
        } catch (IOException e) {
            Log.w(CPUStat.class.getSimpleName(), e);
            //throw new RuntimeException(e);
        }
    }

    private static native int jiffies2ms(int jiffies);
    private List<CoreStat> _cores;
    public CPUStat() {
        super("cpu");
        header(HEADER);
        _cores = new ArrayList<>(CORES);
        for(int i = 0; i < CORES; i++) {
            if(i < FREQS.size())
                _cores.add(new CoreStat(i, FREQS.get(i)));
            else
                _cores.add(new CoreStat(i, new int[]{}));
        }
    }

    public CoreStat get(int i) {
        return _cores.get(i);
    }

    @Override
    public CPUStat get() {
        try {
            RandomAccessFile stat = new RandomAccessFile(PROC_STAT, "r");
            String line;
            while((line = stat.readLine()) != null) {
                Matcher matcher = CPUX.matcher(line);
                if (matcher.matches()) {
                    if(matcher.group(1).isEmpty()) continue;
                    CoreStat core = _cores.get(Integer.parseInt(matcher.group(1)));
                    String[] fields = line.split("\\s+");
                    core.timestamp(System.currentTimeMillis());
                    for(int i = 1; i < fields.length; i++)
                        core.field(i - 1, Long.parseLong(fields[i]));
                    String timeInState = String.format(Locale.ENGLISH, FMT_TIME_IN_STATE, core.dev());
                    try {
                        RandomAccessFile freqsUsage = new RandomAccessFile(timeInState, "r");
                        int freq = 0;
                        while ((line = freqsUsage.readLine()) != null)
                            core.field(fields.length - 1 + freq++, jiffies2ms(Integer.parseInt(line.split("\\s+")[1])));
                        freqsUsage.close();
                    } catch (IOException e) {
                        Log.w(tag(), e);
                    }
                }
            }
        } catch (IOException e) {
            Log.w(tag(), e);
            //throw new RuntimeException(e);
        }
        return this;
    }

    @Override
    public CPUStat diff(Usage usage) {
        if(instanceOf(usage)){
            CPUStat stat = (CPUStat) usage;
            CPUStat diff = new CPUStat();
            for(int i = 0; i < CORES; i++) {
                CoreStat cL = _cores.get(i);
                CoreStat cR = stat.get(i);
                CoreStat tgt = diff.get(i);
                tgt.timestamp(cL.timestamp() - cR.timestamp());
                for(int j = 0; j < count(); j++)
                    tgt.field(j, cL.field(j) - cR.field(j));
            }
            return diff;
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = Resource.getStringBuilder();
        String delim = ",";
        for(int i = 0; i < CORES; i++) {
            CoreStat core = _cores.get(i);
            builder.append(core.dev()).append(delim);
            builder.append(core.timestamp()).append(delim);
            for(int j = 0; j < count(); j++)
                builder.append(core.field(j)).append(delim);
            builder.setLength(builder.length() - 1);
            if(i < CORES - 1) builder.append("\n");
        }
        return builder.toString();
    }
}
