package esms.usage;

import esms.runtime.Resource;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by farleylai on 12/5/15.
 */
public class BlockStat extends Usage {
    private String _path;
    public BlockStat(String dev) {
        super(dev);
        _path = String.format("/sys/block/%s/stat", dev);
        header("reads", "rmerges", "rsectors", "rticks", "writes", "wmerges", "wsectors", "wticks", "pending", "ioticks", "iowait");
    }

    public BlockStat() {
        this("mmcblk0");
    }

    @Override
    public BlockStat get() {
        try {
            RandomAccessFile stat = new RandomAccessFile(_path, "r");
            String[] fields = stat.readLine().trim().split("\\s+");
            timestamp(System.currentTimeMillis());
            for(int i = 0; i < fields.length; i++)
                field(i, Long.parseLong(fields[i]));
            stat.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return this;
    }
}
