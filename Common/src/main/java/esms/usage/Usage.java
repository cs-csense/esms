package esms.usage;

import esms.api.Loggable;
import esms.runtime.Resource;
import esms.util.Log;

/** Instant usage sample as a CSV line.
 *
 */
public class Usage extends Loggable {
    // dev, timestamp, ...
    private String _dev;
    private long _timestamp;
    private String[] _header;
    private long[] _fields;
    protected void header(String...fields) {
        _header = fields;
        _fields = (_fields == null || _fields.length != _header.length) ? new long[_header.length] : _fields;
    }

    public String dev() { return _dev; }
    public long timestamp() { return _timestamp; }
    public void timestamp(long ts) { _timestamp = ts; }

    public int count() { return _fields == null ? 0 : _fields.length; }
    public String header(int i) { return _header[i]; }
    public long field(int i) { return _fields == null ? 0 : _fields[i]; }
    public void field(int i, long value) { if(_fields != null) _fields[i] = value; }
    public boolean instanceOf(Usage usage) {
        return getClass().isInstance(usage) && dev().equals(usage.dev());
    }

    public Usage(String dev) {
        this(dev, 0);
    }

    public Usage(String dev, int count) {
        _dev = dev;
        if(count > 0)
            _fields = new long[count];
    }

    public Usage get() {
        return this;
    }

    public Usage diff(Usage usage) {
        if(instanceOf(usage)) {
            Usage diff = new Usage(dev(), count());
            diff.timestamp(timestamp() - usage.timestamp());
            for(int i = 0; i < count(); i++)
                diff.field(i, field(i) - usage.field(i));
            return diff;
        }
        return null;
    }

    public String header() {
        StringBuilder builder = Resource.getStringBuilder();
        builder.append("dev").append(",");
        builder.append("timestamp").append(",");
        for(int i = 0; i < count(); i++)
            builder.append(header(i)).append(",");
        builder.setLength(builder.length() - 1);
        return builder.toString();
    }

    @Override
    public String toString() {
        StringBuilder builder = Resource.getStringBuilder();
        builder.append(dev()).append(",");
        builder.append(timestamp()).append(",");
        for(int i = 0; i < count(); i++)
            builder.append(field(i)).append(",");
        builder.setLength(builder.length() - 1);
        return builder.toString();

    }
}
