package esms.data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import esms.toolkits.Toolkit;
import esms.util.StageTimer;

/**
 * Created by farleylai on 12/14/14.
 */
public class Evaluation {
    private List<Toolkit> mToolkits;
    private Map<Toolkit, StageTimer> mBuildTimers;
    private Map<Toolkit, Performance> mPerformances;

    public static String[] Metrics = new String[] {
        "CPUTime", "Speedup", "RSS", "Writes", "Allocation", "Source", "Program", "__TEXT", "__DATA", "Compilation"
    };

    public static double improveMore(Measure m1, Measure m2) {
        return (m2.value() - m1.value()) / m1.value();
    }

    public static double improveLess(Measure m1, Measure m2) {
        return (m1.value() - m2.value()) / m1.value();
    }

    public static double speedup(Measure m1, Measure m2) {
        return m1.value() / m2.value();
    }

    public Evaluation(Toolkit... toolkits) {
        mToolkits = Arrays.asList(toolkits);
        mBuildTimers = new HashMap<>();
        mPerformances = new HashMap<>();
    }

    public Evaluation add(Toolkit tk) {
        mToolkits.add(tk);
        return this;
    }

    public StageTimer buildTimer(Toolkit tk) {
        return mBuildTimers.get(tk);
    }

    public Performance performance(Toolkit tk) {
        return mPerformances.get(tk);
    }

    public Measure get(Toolkit tk, String metric) {
        return mPerformances.get(tk).get(metric);
    }

    public Evaluation put(Toolkit toolkit, StageTimer buildTimer) {
        mBuildTimers.put(toolkit, buildTimer);
        return this;
    }

    public Evaluation put(Toolkit toolkit, Performance performance) {
        mPerformances.put(toolkit, performance);
        return this;
    }

    public static Performance evaluate(Performance baseline, Performance perf) {
        Performance improvements = new Performance();
        Measure cpuTime1 = new Measure(baseline.get("user").ns() + baseline.get("sys").ns(), Measure.UNIT.ns);
        Measure cpuTime2 = new Measure(perf.get("user").ns() + perf.get("sys").ns(), Measure.UNIT.ns);
        improvements.put(Metrics[0], improveLess(cpuTime1, cpuTime2) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[1], speedup(cpuTime1, cpuTime2), Measure.UNIT.ratio);
        improvements.put(Metrics[2], improveLess(baseline.get("rss"), perf.get("rss")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[3], improveLess(baseline.get("writes"), perf.get("writes")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[4], improveLess(baseline.get("allocation"), perf.get("allocation")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[5], improveLess(baseline.get("source"), perf.get("source")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[6], improveLess(baseline.get("program"), perf.get("program")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[7], improveLess(baseline.get("__TEXT"), perf.get("__TEXT")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[8], improveLess(baseline.get("__DATA"), perf.get("__DATA")) * 100, Measure.UNIT.percentage);
        return improvements;
    }

    public Performance evaluate(Toolkit tk1, Toolkit tk2) {
        // "CPUTime", "Speedup", "RSS", "Writes", "Allocation", "Source", "Program", "__TEXT", "__DATA", "Compilation"
        Performance improvements = new Performance();
        Measure cpuTime1 = new Measure(get(tk1, "user").ns() + get(tk1, "sys").ns(), Measure.UNIT.ns);
        Measure cpuTime2 = new Measure(get(tk2, "user").ns() + get(tk2, "sys").ns(), Measure.UNIT.ns);
        improvements.put(Metrics[0], improveLess(cpuTime1, cpuTime2) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[1], speedup(cpuTime1, cpuTime2), Measure.UNIT.ratio);
        improvements.put(Metrics[2], improveLess(get(tk1, "rss"), get(tk2, "rss")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[3], improveLess(get(tk1, "writes"), get(tk2, "writes")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[4], improveLess(get(tk1, "allocation"), get(tk2, "allocation")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[5], improveLess(get(tk1, "source"), get(tk2, "source")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[6], improveLess(get(tk1, "program"), get(tk2, "program")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[7], improveLess(get(tk1, "__TEXT"), get(tk2, "__TEXT")) * 100, Measure.UNIT.percentage);
        improvements.put(Metrics[8], improveLess(get(tk1, "__DATA"), get(tk2, "__DATA")) * 100, Measure.UNIT.percentage);
        Measure buildTime1 = new Measure(mBuildTimers.get(tk1).ns(), Measure.UNIT.ns);
        Measure buildTime2 = new Measure(mBuildTimers.get(tk2).ns(), Measure.UNIT.ns);
        improvements.put(Metrics[9], improveLess(buildTime1, buildTime2) * 100, Measure.UNIT.percentage);
        return improvements;
    }
}
