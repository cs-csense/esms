package esms.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import esms.usage.RUsage;

/**
* Created by farleylai on 12/11/14.
*/
public class Performance {
    private List<String> mMetrics;
    private Map<String, Measure> mMeasures;

    public Performance() {
        mMetrics = new ArrayList<>();
        mMeasures = new HashMap<>();
    }

    public Measure get(String metric) {
        return mMeasures.get(metric);
    }

    public Performance put(String metric, double value, Measure.UNIT unit) {
        if(!mMetrics.contains(metric))
            mMetrics.add(metric);
        mMeasures.put(metric, new Measure(value, unit));
        return this;
    }

    public Performance put(RUsage rusage) {
        put("real", rusage.real(), Measure.UNIT.secs);
        put("user", rusage.utime(), Measure.UNIT.secs);
        put("sys", rusage.stime(), Measure.UNIT.secs);
        put("util", rusage.util() * 100, Measure.UNIT.percentage);
        put("rss", rusage.maxrss(), Measure.UNIT.bytes);
        put("text", rusage.ixrss(), Measure.UNIT.bytes);
        put("data", rusage.idrss(), Measure.UNIT.bytes);
        put("stack", rusage.isrss(), Measure.UNIT.bytes);
        put("major", rusage.majflt(), Measure.UNIT.scalar);
        put("minor", rusage.minflt(), Measure.UNIT.scalar);
        put("swaps", rusage.nswap(), Measure.UNIT.scalar);
        put("voluntary", rusage.nvcsw(), Measure.UNIT.scalar);
        put("involuntary", rusage.nivcsw(), Measure.UNIT.scalar);
        return this;
    }

//    public int writes() { return get("Writes").intValue(); }
//    public int allocations() { return get("Allocation").intValue(); }
//    public int source() { return get("Source").intValue(); }
//    public int program() { return get("Program").intValue(); }

    public String summary() {
        String[] metrics = new String[mMetrics.size()];
        return summary(mMetrics.toArray(metrics));
    }

    public String summary(String... metrics) {
        StringBuilder builder = new StringBuilder();
        for(String metric: metrics) {
            Measure m = get(metric);
            builder.append(" ");
            if(m.isFractional()) builder.append(m.value());
            else builder.append(m.longValue());
        }
        return builder.toString();
    }

    public String toString(String... metrics) {
        StringBuilder builder = new StringBuilder();
        for(String metric: metrics)
            builder.append(metric).append(": ").append(get(metric)).append("\n");
        builder.append("toString: ").append(summary(metrics));
        return builder.toString();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(String metric: mMetrics)
            builder.append(metric).append(": ").append(get(metric)).append("\n");
        builder.append("toString: ").append(summary());
        return builder.toString();
    }
}
