package esms.data;

import esms.util.StageTimer;
import esms.util.Timer;

/**
* Created by farleylai on 12/14/14.
*/
public class Measure {
    public enum UNIT {
        ns, us, ms, secs,
        bytes, KB, MB, GB,
        ratio, percentage,
        scalar,
    }

    private double value;
    private UNIT unit;

    public Measure(double value, UNIT unit) {
        this.value = value;
        this.unit = unit;
    }

    public Measure(Timer timer) {
        value = timer.ns();
        unit = UNIT.ns;
    }

    public Measure(StageTimer timer) {
        value = timer.ns();
        unit = UNIT.ns;
    }

    public int intValue() {
        return (int)Math.round(value);
    }

    public long longValue() {
        return Math.round(value);
    }

    public double value() {
        return value;
    }

    public boolean isFractional() {
        return value - (long)value > 0;
    }

    public UNIT unit() { return unit; }

    public long ns() {
        switch(unit) {
            case us: return (long)(value * 1000);
            case ms: return (long)(value * 1000000);
            case secs: return (long)(value * 1000000000);
        }
        throw new RuntimeException("Undefined conversion from " + unit + " to ns");
    }

    public long bytes() {
        switch(unit) {
            case bytes: return (long)value;
            case KB: return (long)(value * 1024);
            case MB: return (long)(value * 1024 * 1024);
            case GB: return (long)(value * 1024 * 1024 * 1024);
        }
        throw new RuntimeException("Undefined conversion from " + unit + " to bytes");
    }

    public Measure to(UNIT u) {
        if(unit == u) return this;
        switch(u) {
            case ns: return new Measure(ns(), UNIT.ns);
            case us: return new Measure((double)ns() / 1000, UNIT.us);
            case ms: return new Measure((double)ns() / 1000000, UNIT.ms);
            case secs: return new Measure((double)ns() / 1000000000, UNIT.secs);
            case KB: return new Measure((double)bytes() / 1024, UNIT.KB);
            case MB: return new Measure((double)bytes() / 1024 / 1024, UNIT.MB);
            case GB: return new Measure((double)bytes() / 1024 / 1024 / 1024, UNIT.GB);
            case ratio:
                switch(unit) {
                    case ratio:
                        return this;
                    case percentage:
                        new Measure(value / 100, UNIT.ratio);
                }
                break;
            case percentage:
                switch(unit) {
                    case ratio:
                        return new Measure(value * 100, UNIT.percentage);
                    case percentage:
                        return this;
                }
                break;
         }
        throw new RuntimeException("Undefined conversion from " + unit + " to " + u);
    }

    public String format() {
        switch(unit) {
            case ratio:
                return String.format("%.3f", value);
            case percentage:
                return String.format("%.2f%%", value);
            case scalar:
                return String.valueOf((long)value);
        }
        return isFractional()
                ? String.format("%.3f %s",  value, unit)
                : String.format("%d %s", longValue(), unit);
    }

    @Override
    public String toString() {
        switch(unit) {
            case ns:
            case us:
            case ms:
            case secs:
                Measure secs = to(UNIT.secs);
                Measure ms = to(UNIT.ms);
                Measure us = to(UNIT.us);
                Measure ns = to(UNIT.ns);
                return secs.value >= 1
                        ? secs.format()
                        : ms.value >= 1
                            ? ms.format()
                            : us.value >= 1
                                ? us.format()
                                : ns.format();
            case KB:
            case MB:
            case GB:
                Measure GB = to(UNIT.GB);
                Measure KB = to(UNIT.MB);
                Measure MB = to(UNIT.KB);
                Measure bytes = to(UNIT.bytes);
                return GB.value >= 1
                        ? GB.format()
                        : MB.value >= 1
                            ? MB.format()
                            : KB.value >= 1
                                ? KB.format()
                                : bytes.format();
            default:
                return format();
        }
    }
}
