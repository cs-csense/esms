package esms.runtime;

import java.io.IOException;
import java.util.Locale;

import esms.profiling.DefaultProfiler;
import esms.profiling.InstantProfiler;
import esms.profiling.Profiler;
import esms.util.Log;
import esms.util.Shell;

public class Scheduler extends Thread {
    public static int ID;
    private int mId;
    private int mPeriods;
    private final InstantProfiler mProfiler;

    public Scheduler() {
        this(Scheduler.class.getSimpleName());
    }
    public Scheduler(int periods) {
        this(Scheduler.class.getSimpleName(), periods);
    }
    public Scheduler(InstantProfiler profiler) {
        this(Scheduler.class.getSimpleName(), profiler);
    }
    public Scheduler(int periods, InstantProfiler profiler) {
        this(Scheduler.class.getSimpleName(), periods, profiler);
    }

    public Scheduler(String name) {
        this(name, null);
    }
    public Scheduler(String name, int periods) {
        this(name, periods, null);
    }
    public Scheduler(String name, InstantProfiler profiler) {
        this(name, 0, profiler);
    }
    public Scheduler(String name, int periods, InstantProfiler profiler) {
        super(name + ID);
        mId = ID++;
        mPeriods = periods;
        mProfiler = profiler == null && Shell.os() == Shell.OS.Android ? new DefaultProfiler() : profiler;
    }

    protected void initialization() throws InterruptedException {}
    protected void steadyState() throws InterruptedException {}
    private final void steady() throws InterruptedException {
        if(mPeriods == 0) {
            while(!interrupted()) steadyState();
            throw new InterruptedException();
        } else {
            while(mPeriods-- > 0 && !interrupted()) {
                profile();
                steadyState();
                profile();
            }
            if(mPeriods > 0) throw new InterruptedException();
        }
    }
    protected void remaining() throws InterruptedException {}

    public void profile(int cid, int eid) {
        if(mProfiler != null) mProfiler.profile(cid, eid);
    }

    public void profile(int eid) {
        if(mProfiler != null) mProfiler.profile(mId, eid);
    }

    public void profile() {
        if(mProfiler != null) mProfiler.profile(mId, mPeriods);
    }

    @Override
    public void run() {
        try {
            profile(-1);
            initialization();
            profile(-2);
            steady();
            profile(-3);
            remaining();
            profile(-4);
            Log.i(getName(), "done");
        } catch(InterruptedException e) {
            profile(-5);
            Log.w(getName(), "interrupted to stop at steady period %d\n", mPeriods);
        }

        try {
            if(mProfiler != null) mProfiler.save(Program.file(getName() + ".log"));
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }
}