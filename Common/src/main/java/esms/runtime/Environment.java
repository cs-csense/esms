package esms.runtime;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public abstract class Environment {
    private static Environment INSTANCE;
    static {
        try {
            final Class<?> EnvC = Class.forName("android.os.Environment");
            INSTANCE = new Environment() {
                Method getExternalStoragePublicDirectory = EnvC.getMethod("getExternalStoragePublicDirectory", String.class);
                @Override
                protected File getExternalStorageDirectory() {
                    try {
                        return (File)getExternalStoragePublicDirectory.invoke(null, EnvC.getField("DIRECTORY_DOWNLOADS").get(null));
                    } catch (IllegalAccessException | InvocationTargetException | NoSuchFieldException e) {
                        throw new RuntimeException(e);
                    }
                }
            };
        } catch (ClassNotFoundException e) {
            INSTANCE = new Environment() {
                @Override
                protected File getExternalStorageDirectory() {
                    return new File(".");
                }
            };
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public static File getStoragePath() {
        return INSTANCE.getExternalStorageDirectory();
    }
    public static File file(String filename) { return new File(INSTANCE.getExternalStorageDirectory(), filename); }
    public static <T> T get(String key) { return (T)INSTANCE._properties.get(key); }
    public static void put(String key, Object value) { INSTANCE._properties.put(key, value); }

    private Map<String, Object> _properties = new HashMap<>();
    protected abstract File getExternalStorageDirectory();
}
