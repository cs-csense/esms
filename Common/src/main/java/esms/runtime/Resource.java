package esms.runtime;

/**
 * Created by farleylai on 6/24/16.
 */
public class Resource {
    private static final ThreadLocal<StringBuilder> StringBuilder = new ThreadLocal<StringBuilder>() {
        @Override
        protected StringBuilder initialValue() {
            return new StringBuilder();
        }
    };

    public static StringBuilder getStringBuilder() {
        StringBuilder builder = StringBuilder.get();
        builder.setLength(0);
        return builder;
    }
}
