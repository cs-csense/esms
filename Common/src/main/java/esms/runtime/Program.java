package esms.runtime;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import esms.util.Log;

import static java.util.Calendar.DATE;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;

public class Program {
    public static String TAG = "ESMS";
    public static Program instance() { return _instance; }
    private static Program _instance;
    protected static File _trace;
    protected List<Scheduler> _schedulers = new ArrayList<>();
    protected String CFG = "";
    protected int _periods = 500;
    protected int _iterations = 0;
    protected int _scaling = 1;
    protected String _remote;
    //protected String _remote = "alacran.cs.uiowa.edu";
    //protected String _remote = "localhost";

    public Program(String[] args) {
        if(_instance != null) throw new RuntimeException("Concurrent MSA execution is not supported yet");
        int iterations = _periods;
        for(int _i = 0; _i < args.length; ++_i) {
            if(_i + 1 < args.length && args[_i].equals("-b"))
                CFG = "-" + args[_i+1];
            else if(_i + 1 < args.length && args[_i].equals("-i"))
                iterations = Integer.parseInt(args[_i+1]);
            else if(_i + 1 < args.length && args[_i].equals("-scaling"))
                _scaling = Integer.parseInt(args[_i+1]);
            else if(_i + 1 < args.length && args[_i].equals("-remote"))
                _remote = args[_i+1];
        }

        Calendar now = Calendar.getInstance();
        String trace = String.format("MSA2%s-%4d%02d%02d-%02d%02d%02d", CFG, now.get(YEAR), now.get(MONTH)+1, now.get(DATE), now.get(HOUR_OF_DAY), now.get(MINUTE), now.get(SECOND));
        _trace = Environment.file(trace);
        _trace.mkdirs();
        _periods = iterations / _scaling;
        _iterations = iterations % _scaling;
        _instance = this;
        Log.i(TAG, "periods: %d, iterations: %d, remote: %s, scaling: %d\n", _periods, _iterations, _remote, _scaling);
    }

    public static File file(String filename) {
        return _trace == null ? Environment.file(filename) : new File(_trace, filename);
    }

    public final Program start() {
        if(_schedulers.isEmpty()) {
            init();
            initChannels();
            initFilters();
            run();
        } else
            Log.w(TAG, "already started");
        return this;
    }

    public static File getTracePath() {
        return _trace;
    }
    public static String getRemote() { return _instance._remote; }

    protected void init() {}
    protected void initChannels() {}
    protected void initFilters() {}
    protected void run() {}
    public void cleanup() {
        Scheduler.ID = 0;
        _schedulers.clear();
        _instance = null;
    }
    public final void join() { join(0); }
    public final void join(int ms) {
        if(_schedulers.isEmpty()) return;
        for(int i = 0; i < _schedulers.size(); i++) {
            Scheduler scheduler = _schedulers.get(i);
            try {
                switch(i) {
                    case 0:
                        scheduler.join(0); break;
                    case 1:
                        scheduler.join(ms); break;
                }
            } catch (InterruptedException e) {
            }
            if (scheduler.isAlive()) {
                scheduler.interrupt();
                Log.w(TAG, "interrupting %s\n", scheduler.getName());
            }
        }
        cleanup();
    }
    public boolean isRunning() {
        if(_schedulers.isEmpty()) return false;
        for(Scheduler scheduler: _schedulers)
            if(scheduler.isAlive()) return true;
        return false;
    }
    public void report() {}
    public static void main(String[] args) {
        new Program(args).start().join();
    }
}
