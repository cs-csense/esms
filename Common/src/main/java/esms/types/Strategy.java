package esms.types;

/**
* Created by farleylai on 4/8/15.
*/
public enum Strategy {
    IN_PLACE, INSERT_HEAD, APPEND_ALWAYS, APPEND_ON_CONFLICT;
    private static String[] VALUES = new String[] {
            "ip", "ih", "aa", "aoc"
    };

    @Override
    public String toString() {
        return VALUES[ordinal()];
    }
}
