package esms.types;

/**
* Created by farleylai on 4/8/15.
*/
public enum Benchmark {
    // Unit tests
    BPFProgram, IO, Insert, Insert2, StatefulInsert, Update,
    BasicCC, FFT4,
    // Benchmarks
    AutoCor,
    BitonicSort, BitonicSortRecursive,
    FIRcoarse, FIR, FMRadio,
    FFT2, FFT3,
    MatrixMult, MatrixMultBlock,
    MergeSort,
    ProducerConsumer,
    Repeater,
    // Applications
    BeepBeep, BeepBeepA,
    MFCC, SpeakerIdentifier, SpeakerIdentifier2,
    Crowd, CrowdA,
    MP3Decoder,
    // Not supported yet
    FilterBankLatency, FilterBankNew;
}
