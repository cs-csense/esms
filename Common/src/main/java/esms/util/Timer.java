package esms.util;

/**
 * Created by farleylai on 12/2/14.
 */
public class Timer {
    private String mName;
    private long mBegeining;
    private long mEnding;

    public Timer() {
        this("");
    }

    public Timer(String name) {
        mName = name;
        mBegeining = System.nanoTime();
        mEnding = -1;
    }

    public long begin() {
        return mBegeining;
    }

    public long end() {
        return mEnding;
    }

    public long ns() {
        return mEnding - mBegeining;
    }

    public double us() {
        return Time.ns(mEnding - mBegeining).us();
    }

    public double ms() {
        return Time.ns(mEnding - mBegeining).ms();
    }

    public double secs() {
        return Time.ns(mEnding - mBegeining).secs();
    }

    public long log() {
        long now = System.nanoTime();
        if(mEnding == -1)
            mEnding = now;
        else {
            mBegeining = mEnding;
            mEnding = now;
        }
        return mEnding - mBegeining;
    }

    public String toString() {
        String prefix = mName.length() > 0 ? mName + ": " : "";
        return prefix + Time.ns(ns());
    }
}
