package esms.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by farleylai on 12/2/14.
 */
public class StageTimer {
    private boolean mEvent;
    private String mName = "";
    private List<Timer> mTimers;

    public StageTimer() {
        mTimers = new ArrayList<>();
    }

    public StageTimer(String name) {
        this();
        mName = name;
    }

    public StageTimer(String name, Timer... timers) {
        this(name);
        for(Timer timer: timers)
            mTimers.add(timer);
    }

    public long begin() {
        return mTimers.isEmpty() ? -1 : mTimers.get(0).begin();
    }

    public long end() {
        return mTimers.isEmpty() ? -1 : mTimers.get(mTimers.size() - 1).end();
    }

    public long ns() {
        long time = 0;
        for(Timer timer: mTimers)
            time += timer.ns();
        return time;
    }

    public double us() {
        return Time.ns(ns()).us();
    }

    public double ms() {
        return Time.ns(ns()).ms();
    }

    public double secs() {
        return Time.ns(ns()).secs();
    }

    public StageTimer log(String event) {
        if(!mEvent) {
            Timer timer = new Timer(event);
            mTimers.add(timer);
            mEvent = true;
        } else if(!mTimers.isEmpty()) {
            mTimers.get(mTimers.size() - 1).log();
            mEvent = false;
        }
        return this;
    }

    public StageTimer log() {
        return log("");
    }

    public String summary() {
        StringBuilder builder = new StringBuilder();
        builder.append(ns());
        for(Timer timer: mTimers)
            builder.append(String.format(" %d", timer.ns()));
        return builder.toString();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        String prefix = mName.length() > 0 ? mName + ": " : "";
        builder.append(prefix).append(Time.ns(ns())).append(" in total\n");
        int i = 0;
        for(Timer timer: mTimers)
            builder.append(String.format("[%d] %s\n", i++, timer));
        builder.append("summary: ").append(summary());
        return builder.toString();
    }
}
