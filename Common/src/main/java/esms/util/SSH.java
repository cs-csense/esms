package esms.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by farleylai on 6/24/15.
 */
public class SSH {
    private String mUser;
    private String mPasswd;
    private String mHost;
    private int mPort;

    private CommandLine cmd(String cmd, String... args) {
        CommandLine command = new CommandLine(toString());
        command.arg(String.format("'%s'", new CommandLine(cmd, args)));
        return command;
    }

    public SSH(String user, String passwd, String host) {
        this(user, passwd, host, 22);
    }

    public SSH(String user, String passwd, String host, int port) {
        mUser = user;
        mPasswd = passwd;
        mHost = host;
        mPort = port;
    }

    public String passwd() { return mPasswd; }

    public int run(CommandLine cmd) { return run(null, null, cmd); }
    public int run(File wd, CommandLine cmd) {
        return run(wd, null, cmd);
    }
    public int run(File wd, String[] envp, CommandLine cmd) {
        return Shell.run(wd, envp, cmd(cmd.toString()));
    }

    public boolean scp(String src, String target) {
        return scp(null, src, target);
    }

    public boolean scp(File wd, String src, String target) {
        CommandLine cmd = new CommandLine("scp", "-P", mPort, "-r", src, String.format("%s@%s:%s", mUser, mHost, target));
        return Shell.run(wd, cmd) == Shell.EXIT_SUCCESS;
    }

    @Override
    public String toString() {
        return String.format("ssh -t -p %d %s@%s", mPort, mUser, mHost);
    }
}
