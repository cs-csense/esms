package esms.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by farleylai on 7/16/14.
 */
public abstract class Log {
    public enum Severity {
        VERBOSE(2) {
            @Override
            public String toString() { return "V"; }
        }, DEBUG(3) {
            @Override
            public String toString() { return "D"; }
        }, INFO(4) {
            @Override
            public String toString() { return "I"; }
        }, WARN(5) {
            @Override
            public String toString() { return "W"; }
        }, ERROR(6) {
            @Override
            public String toString() { return "E"; }
        }, ASSERT(7) {
            @Override
            public String toString() { return "A"; }
        };

        private final int priority;
        Severity(int priority) {
            this.priority = priority;
        }

        public int priority() {
            return priority;
        }
    }
    private static Log INSTANCE;
    static {
        try {
            final Class<?> LogC = Class.forName("android.util.Log");
            INSTANCE = new Log() {
                Method println = LogC.getMethod("println", Integer.TYPE, String.class, String.class);
                @Override
                protected void printf(Severity severity, String tag, String fmt, Object... args) {
                    if(fmt.charAt(fmt.length() - 1) == '\n') fmt = fmt.substring(0, fmt.length() - 1);
                    try {
                        println.invoke(null, severity.priority(), tag, String.format(fmt, args));
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        throw new RuntimeException(e);
                    }
                }
                @Override
                protected void println(Severity severity, String tag, String line) {
                    try {
                        println.invoke(null, severity.priority(), tag, line);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        throw new RuntimeException(e);
                    }
                }
            };
        } catch (ClassNotFoundException e) {
            INSTANCE = new Log() {
                @Override
                protected void printf(Severity severity, String tag, String fmt, Object... args) {
                    System.out.printf(prefix(severity, tag) + fmt, args);
                }

                @Override
                protected void println(Severity severity, String tag, String line) {
                    System.out.printf("%s%s\n", prefix(severity, tag), line);
                }
            };
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private static Severity level = Severity.INFO;
    public static Severity level() { return level; }
    public static Severity level(Severity severity) { return level = severity; }
    public static boolean isLoggable(Severity severity) { return isLoggable(null, severity); }
    public static boolean isLoggable(String tag, Severity severity) { return severity.compareTo(level) >= 0; }
    protected abstract void printf(Severity severity, String tag, String fmt, Object... args);
    protected abstract void println(Severity severity, String tag, String line);
    private static String prefix(Severity severity, String tag) {
//        if(severity != Severity.INFO)
//            return tag == null ? String.format("[%s] ", severity) : String.format("%s/%s: ", severity, tag);
//        else return "";
        return tag == null ? "" : String.format("%s/%s: ", severity, tag);
    }

    public static void e(String tag, Object line) {
        INSTANCE.println(Severity.ERROR, tag, line.toString());
    }
    public static void e(String tag, String fmt, Object... args) {
        INSTANCE.printf(Severity.ERROR, tag, fmt, args);
    }

    public static void w(String tag, Object line) {
        if (isLoggable(tag, Severity.WARN))
            INSTANCE.println(Severity.WARN, tag, line.toString());
    }
    public static void w(String tag, String fmt, Object... args) {
        if (isLoggable(tag, Severity.WARN))
            INSTANCE.printf(Severity.WARN, tag, fmt, args);
    }

    public static void i(String tag, Object line) {
        if (isLoggable(tag, Severity.INFO))
            INSTANCE.println(Severity.INFO, tag, line == null ? "" : line.toString());
    }
    public static void i(String tag, String fmt, Object... args) {
        if (isLoggable(tag, Severity.INFO))
            INSTANCE.printf(Severity.INFO, tag, fmt, args);
    }

    public static void d(String tag, Object line) {
        if (isLoggable(tag, Severity.DEBUG))
            INSTANCE.println(Severity.DEBUG, tag, line.toString());
    }
    public static void d(String tag, String fmt, Object... args) {
        if (isLoggable(tag, Severity.DEBUG))
            INSTANCE.printf(Severity.DEBUG, tag, fmt, args);
    }

    public static void v(String tag, Object line) {
        if (isLoggable(tag, Severity.VERBOSE))
            INSTANCE.println(Severity.VERBOSE, tag, line.toString());
    }
    public static void v(String tag, String fmt, Object... args) {
        if (isLoggable(tag, Severity.VERBOSE))
            INSTANCE.printf(Severity.VERBOSE, tag, fmt, args);
    }

    public static void title(String fmt, Object... args) {
        i(null, Formatter.title(String.format(fmt, args)));
    }
}
