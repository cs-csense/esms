package esms.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by farleylai on 12/13/14.
 */
public class CommandLine {
    private String cmd;
    private List<Object> args;

    public CommandLine(String cmd, String... argv) {
        this(cmd, (Object[])argv);
    }

    public CommandLine(String cmd, Object... argv) {
        this.cmd = cmd;
        args = new ArrayList<>();
        for(Object arg: argv) {
            String[] parts = arg.toString().split(" ");
            if(parts.length > 0) {
                for(String part: parts)
                    args.add(part);
            } else
                args.add(arg);
        }
    }

    public int argc() {
        return args.size();
    }

    public String[] args() {
        String[] argv = new String[args.size()];
        args.toArray(argv);
        return argv;
    }

    public CommandLine arg(String... argv) {
        for(String arg: argv)
            args.add(arg);
        return this;
    }

    public String[] toShell() {
       return new String[] { "sh", "-c", toString() };
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(cmd);
        for(Object arg: args)
            builder.append(" ").append(arg);
        return builder.toString();
    }
}
