package esms.util;

/**
 * Created by farleylai on 12/2/14.
 */
public class Time {
    private long ns;

    public static Time ns(long ns) {
        return new Time(ns);
    }

    public static Time us(long us) {
        return new Time(us * 1000);
    }

    public static Time ms(long ms) {
        return new Time(ms * 1000000);
    }

    public static Time secs(long secs) {
        return new Time(secs * 1000000000);
    }

    private Time(long nanos) {
        ns = nanos;
    }

    public long ns() {
        return ns;
    }

    public double us() {
        return  ns / 1000.0;
    }

    public double ms() {
        return ns / 1000000.0;
    }

    public double secs() {
        return ns / 1000000000.0;
    }

    @Override
    public String toString() {
        return secs() >= 1
                ? String.format("%.3fs", secs())
                : ms() >= 1
                ? String.format("%.3fms", ms())
                : us() >= 1
                ? String.format("%.3fus", us())
                : String.format("%dns", ns());
    }
}
