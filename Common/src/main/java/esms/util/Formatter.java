package esms.util;

/**
 * Created by farleylai on 12/2/14.
 */
public class Formatter {
    public static String title(String fmt, Object... args) {
        return title(String.format(fmt, args));
    }

    public static String title(String line) {
        return "=============== " + line + " ===============";
    }
}
