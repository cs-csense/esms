package esms.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import esms.usage.RUsage;

/**
 * Created by farleylai on 11/4/14.
 */
public class Shell {
    public static final int EXIT_SUCCESS = 0;
    public static final int EXIT_FAILURE = -1;
    private static String TAG = Shell.class.getSimpleName();

    private static List<String> readlines(BufferedReader reader) throws IOException {
        List<String> lines = new ArrayList<>();
        for(String line; (line = reader.readLine()) != null;)
            lines.add(line);
        return lines;
    }

    public static List<String> read(File wd, CommandLine cmd) throws IOException {
        Process p = exec(wd, cmd);
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        return readlines(reader);
    }

    public static enum OS {
        Windows, MacOS, Linux, Android, Other
    };

    public static OS os() {
        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        String VENDOR = System.getProperty("java.runtime.name").toLowerCase(Locale.ENGLISH);
        if(VENDOR.indexOf("android") >= 0)
            return Shell.OS.Android;
        else if(OS.indexOf("mac") >= 0 || OS.indexOf("darwin") >= 0)
            return Shell.OS.MacOS;
        else if(OS.indexOf("win") >= 0)
            return Shell.OS.Windows;
        else if(OS.indexOf("nux") >= 0)
            return Shell.OS.Linux;
        return Shell.OS.Other;
    }

    public static String path(String... parts) {
        return new File(String.join(File.separator, parts)).getAbsolutePath();
    }

    public static String path(File dir, String... parts) {
        return new File(dir, String.join(File.separator, parts)).getAbsolutePath();
    }

    public static String pathAt(String path, int idx) {
        String[] parts = path.split(File.separator);
        return parts[idx < 0 ? parts.length + idx : idx];
    }

    public static String pwd(String cmd) {
        return "./" + cmd;
    }

    public static CommandLine cmd(String cmd, Object... args) {
        return new CommandLine(cmd, args);
    }

    public static int run(CommandLine cmd) { return run(null, null, cmd); }
    public static int run(File wd, CommandLine cmd) {
        return run(wd, null, cmd);
    }
    public static int run(File wd, String[] envp, CommandLine cmd) {
        try {
            Log.i(TAG, "$> " + cmd);
            Process p = Runtime.getRuntime().exec(cmd.toShell(), envp, wd);
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            for(String line: Shell.readlines(reader)) Log.i(TAG, line);
            reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            for(String line: Shell.readlines(reader)) Log.i(TAG, line);
            return p.waitFor();
        } catch (IOException e) {
            return Shell.EXIT_FAILURE;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static int run(File wd, CommandLine cmd, File output) {
        try {
            return exec(wd, cmd, output).waitFor();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Process exec(File wd, CommandLine cmd) {
        return exec(wd, null, cmd, null);
    }

    public static Process exec(File wd, CommandLine cmd, File output) {
        return exec(wd, null, cmd, output);
    }

    public static Process exec(File wd, String[] envp, CommandLine cmd, File output) {
        try {
            Log.i(TAG, "$> %s%s\n", cmd, output == null ? "" : " > " + output);
            ProcessBuilder builder = new ProcessBuilder();
            if(envp != null) {
                Map<String, String> env = builder.environment();
                for(String var: envp) {
                    String[] pair = var.split("=");
                    env.put(pair[0], pair[1]);
                }
            }
            return output == null
                    ? builder.directory(wd).command(cmd.toShell()).start()
                    : builder.directory(wd).command(cmd.toShell()).redirectOutput(output).start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean verify(File wd1, String cmd1, File wd2, String cmd2, int lines) {
        try {
            Log.i(TAG, "Verifying %s and %s up to %d lines of output\n", cmd1, cmd2, lines);
            Log.i(TAG, "$> %s \n", cmd1);
            Log.i(TAG, "$> %s \n", cmd2);
            Process p1 = Runtime.getRuntime().exec(cmd1, null, wd1);
            Process p2 = Runtime.getRuntime().exec(cmd2, null, wd2);
            BufferedReader reader1 = new BufferedReader(new InputStreamReader(p1.getInputStream()));
            BufferedReader reader2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
            int n = 0;
            boolean ret = true;
            String line1 = reader1.readLine();
            String line2 = reader2.readLine();
            while(n < lines && line1 != null && line2 != null) {
                n++;
                if(!line1.equals(line2)) {
                    ret = false;
                    try {
                        if(Float.parseFloat(line1) == Float.parseFloat(line2)) ret = true;
                    } catch(NumberFormatException e) {}
                    if(!ret) {
                        Log.e(TAG, "[%d] %s != %s\n", n, line1, line2);
                        break;
                    }
                }
                line1 = reader1.readLine();
                line2 = reader2.readLine();
            }

            if(line1 != line2 && (line1 == null || line2 == null)) {
                ret = false;
                Log.e(TAG, "Inconsistent output since line[%d]\n", n);
            } else if(n == 0) {
                ret = false;
                Log.e(TAG, "Empty output to verify\n");
            } else if(n == lines) {
                Log.i(TAG, "Verified the first %d lines of output\n", n);
            }
            reader1.close();
            reader2.close();
            p1.waitFor();
            p2.waitFor();
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static RUsage time(File wd, String cmd) {
        final String TIME = "/usr/bin/time";
        Process p;
        switch(os()) {
            case MacOS:
                p = exec(wd, new CommandLine(TIME, "-l", cmd), new File("/dev/null")); break;
            case Linux:
                p = exec(wd, new CommandLine(TIME, cmd), new File("/dev/null")); break;
            default:
                throw new RuntimeException("Unsupported platform to run " + TIME);
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        try {
            String line;
            List<String> lines = new ArrayList<>();
            while((line = reader.readLine()) != null) lines.add(line);
            String[] info = new String[lines.size()];
            p.waitFor();
            return RUsage.parse(lines.toArray(info));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean cp(String src, String target) {
        return cp(null, src, target);
    }

    public static boolean cp(File wd, String src, String target) {
        return run(wd, new CommandLine("cp", "-r", src, target)) == Shell.EXIT_SUCCESS;
    }

    public static boolean ln(String src, String target) {
        return ln(null, src, target);
    }

    public static boolean ln(File wd, String src, String target) {
        return run(wd, new CommandLine("ln", "-sf", src, target)) == Shell.EXIT_SUCCESS;
    }

    public static boolean rm(File path) {
        return rm(path.getAbsolutePath());
    }

    public static boolean rm(String path) {
        return run(null, new CommandLine("rm", "-fr", path)) == Shell.EXIT_SUCCESS;
    }
}
