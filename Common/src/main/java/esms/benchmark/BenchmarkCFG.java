package esms.benchmark;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import esms.types.Benchmark;
import esms.types.Strategy;
import esms.util.Shell;

import static esms.types.Benchmark.AutoCor;
import static esms.types.Benchmark.BasicCC;
import static esms.types.Benchmark.BeepBeep;
import static esms.types.Benchmark.BeepBeepA;
import static esms.types.Benchmark.BitonicSort;
import static esms.types.Benchmark.BitonicSortRecursive;
import static esms.types.Benchmark.Crowd;
import static esms.types.Benchmark.CrowdA;
import static esms.types.Benchmark.FFT2;
import static esms.types.Benchmark.FFT3;
import static esms.types.Benchmark.FIR;
import static esms.types.Benchmark.FIRcoarse;
import static esms.types.Benchmark.FMRadio;
import static esms.types.Benchmark.FilterBankNew;
import static esms.types.Benchmark.MFCC;
import static esms.types.Benchmark.MP3Decoder;
import static esms.types.Benchmark.MatrixMult;
import static esms.types.Benchmark.MatrixMultBlock;
import static esms.types.Benchmark.MergeSort;
import static esms.types.Benchmark.ProducerConsumer;
import static esms.types.Benchmark.Repeater;
import static esms.types.Benchmark.SpeakerIdentifier;
import static esms.types.Benchmark.SpeakerIdentifier2;
import static esms.types.Strategy.*;

/**
 * Created by farleylai on 12/22/14.
 */
public class BenchmarkCFG {
    public static final String AUTHOR = "Farley Lai";

//    public static final String STREAMIT_BUILD = "../Optimizer/build/StreamIt";
//    public static final String CSENSE_BUILD = "../Optimizer/build/CSense";
//
//    private static final String[] BUILDS = { STREAMIT_BUILD, CSENSE_BUILD };
//
//    public static enum Toolkit {
//        StreamIt, CSense,
//    }
//
//    public static String name(Toolkit toolkit) {
//        return toolkit.name();
//    }
//
//    public static String build(Toolkit toolkit) {
//        return BUILDS[toolkit.ordinal()];
//    }

    private static final String BUILD = "build";

    private static Benchmark[] Benchmarks = new Benchmark[] {
            AutoCor, BitonicSort,
//            BitonicSortRecursive,
            FilterBankNew,
            FIRcoarse, FIR, FMRadio, FFT2, FFT3,
            MatrixMult, MatrixMultBlock,
            MergeSort,
//            ProducerConsumer,
            Repeater,
            BeepBeep, MFCC, Crowd,
//            MP3Decoder,
    };

    private static Strategy[] Strategies = new Strategy[] {
            //IN_PLACE,
            //APPEND_ALWAYS,
            APPEND_ON_CONFLICT
    };
    private static int[] MaxWindowGroups = new int[] {
//            1,
//            8,
//            16,
            32
    };
    private static Map<Benchmark, Integer> SCALINGS = new HashMap<>();
    private static Map<Benchmark, Integer> DESKTOP = new HashMap<>();
    private static Map<Benchmark, Integer> ANDROID = new HashMap<>();
    private static Map<Benchmark, int[]> PARAMS = new HashMap<>();
    static {
        // Benchmarks
        SCALINGS.put(AutoCor, 15);                  //  15 for 128/16
        SCALINGS.put(BasicCC, 6);                   //   6 for 256
        SCALINGS.put(BitonicSort, 31);              //  31 for 32
        SCALINGS.put(BitonicSortRecursive, 63);     //  63 for 32
        SCALINGS.put(FIRcoarse, 766);               // 766 for 512
        SCALINGS.put(FIR, 511);                     // 511 for 512
        SCALINGS.put(FMRadio, 185);                 // 185 for 11 taps
        SCALINGS.put(FFT2, 3);                      //   3 for 128
        SCALINGS.put(FFT3, 7);                      //   7 for 64
        SCALINGS.put(MatrixMult, 7);                //   7 for 12
        SCALINGS.put(MatrixMultBlock, 2);           //   2 for 12/12/9/12/3
        SCALINGS.put(MergeSort, 63);                //  63 for 128/16
        SCALINGS.put(ProducerConsumer, 1);          //   1 for 3/2
        SCALINGS.put(Repeater, 7);                  //   7 for 256/16

        // Application benchmarks
        SCALINGS.put(BeepBeep, 7);                  //   7 for 128/16
        SCALINGS.put(Crowd, 1);                     // OOM w/FFT3 256 and 1 w/FFT2 256
        SCALINGS.put(MFCC, 1);                      // OOM w/FFT3 256 and 1 w/FFT2 256
        SCALINGS.put(MP3Decoder, 3);                //   3 for 1 channel

        // Real time MSAs
        SCALINGS.put(BeepBeepA, 3);                 //   3 for 128/16
    }

    // Iterations multiple on desktop
    static {
        // Benchmarks
        DESKTOP.put(AutoCor, 20000000);
        DESKTOP.put(BasicCC, 20000000);
        DESKTOP.put(BitonicSort, 20000000);
        DESKTOP.put(BitonicSortRecursive, 20000000);
        DESKTOP.put(FFT2, 8000000); // 7 for 64, 31 for 32??
        DESKTOP.put(FFT3, 20000000); //7 for 64, 15 for 32, 31 for 8
        DESKTOP.put(FIRcoarse, 20000000);
        DESKTOP.put(FIR, 20000000);
        DESKTOP.put(FMRadio, 20000000);
        DESKTOP.put(MatrixMult, 20000000);
        DESKTOP.put(MatrixMultBlock, 20000000);
        DESKTOP.put(MergeSort, 20000000);
        DESKTOP.put(ProducerConsumer, 40000000);
        DESKTOP.put(Repeater, 40000000);

        // Application benchmarks
        DESKTOP.put(BeepBeep, 4000000);
        DESKTOP.put(Crowd, 2000000);
        DESKTOP.put(MFCC, 2000000);
        DESKTOP.put(MP3Decoder, 500000);

        // Android apps
        DESKTOP.put(BeepBeepA, 2);
        DESKTOP.put(SpeakerIdentifier, 2);
        DESKTOP.put(SpeakerIdentifier2, 2);
        DESKTOP.put(CrowdA, 2);
    }

    static {
        // Benchmarks
        ANDROID.put(AutoCor, 5000000);
        ANDROID.put(BasicCC, 5000000);
        ANDROID.put(BitonicSort, 2000000);
        ANDROID.put(BitonicSortRecursive, 2000000);
        ANDROID.put(FilterBankNew, 10000000);
        ANDROID.put(FFT2, 600000); // 7 for 64, 31 for 32??
        ANDROID.put(FFT3, 1000000); //7 for 64, 15 for 32, 31 for 8
        ANDROID.put(FIRcoarse, 10000000);
        ANDROID.put(FIR, 500000);
        ANDROID.put(FMRadio, 4000000);
        ANDROID.put(MatrixMult, 800000);
        ANDROID.put(MatrixMultBlock, 1000000);
        ANDROID.put(MergeSort, 1000000);
        ANDROID.put(ProducerConsumer, 20000000);
        ANDROID.put(Repeater, 20000000);

        // Application benchmarks
        ANDROID.put(BeepBeep, 600000);
        ANDROID.put(Crowd, 100000);
        ANDROID.put(MFCC, 150000);
        ANDROID.put(MP3Decoder, 25000);

        // Android apps
        ANDROID.put(BeepBeepA, 2);
        ANDROID.put(SpeakerIdentifier, 2);
        ANDROID.put(SpeakerIdentifier2, 2);
        ANDROID.put(CrowdA, 2);
    }

    static {
        PARAMS.put(AutoCor, new int[] { 128, 16 });
        PARAMS.put(BasicCC, new int[] { 256 });
        PARAMS.put(BitonicSort, new int[] { 32 });
        PARAMS.put(BitonicSortRecursive, new int[] { 32 });
        PARAMS.put(FIRcoarse, new int[] { 512 });
        PARAMS.put(FIR, new int[] { 512 });
        PARAMS.put(FMRadio, new int[] { 11 });
        PARAMS.put(FFT2, new int[] { 128 });
        PARAMS.put(FFT3, new int[] { 64 });
        PARAMS.put(MatrixMult, new int[] { 12 });
        PARAMS.put(MatrixMultBlock, new int[] { 12, 12, 9, 12, 3 });
        PARAMS.put(MergeSort, new int[] { 128, 16 });
        PARAMS.put(ProducerConsumer, new int[] { 3, 2 });
        PARAMS.put(Repeater, new int[] { 256, 16 });

        PARAMS.put(BeepBeep, new int[] { 128, 16 });
        PARAMS.put(MFCC, new int[] { 256, 128 });
        PARAMS.put(Crowd, new int[] { 256, 128 });
        PARAMS.put(MP3Decoder, new int[] { 1 });

        PARAMS.put(BeepBeepA, new int[] { 128, 16 });
        PARAMS.put(SpeakerIdentifier, new int[] { 256, 128 });
        PARAMS.put(CrowdA, new int[] { 256, 128 });
    }

    public static List<Benchmark> MSAs() {
        List<Benchmark> MSAs = new ArrayList<Benchmark>();
        MSAs.add(BeepBeepA);
        MSAs.add(CrowdA);
        MSAs.add(SpeakerIdentifier);
        MSAs.add(SpeakerIdentifier2);
        return MSAs;
    }

    public static List<Benchmark> MSBs() {
        List<Benchmark> MSBs = benchmarks();
        MSBs.removeAll(MSAs());
        return MSBs;
    }

    public static List<Benchmark> benchmarks() { return new ArrayList<>(Arrays.asList(Benchmarks)); }
    public static Strategy[] strategies() { return Strategies; }
    public static int[] maxWindowGroups() { return MaxWindowGroups; }
    public static int[] params(Benchmark benchmark) {
        int[] ret = PARAMS.get(benchmark);
        return ret == null ? new int[0] : ret;
    }

    public static int scaling(Benchmark benchmark) {
        Integer ret = SCALINGS.get(benchmark);
        return ret == null ? 1 : ret;
    }
    public static int iterations(Benchmark benchmark) {
        return iterations(benchmark, Shell.os());
    }
    public static int iterations(Benchmark benchmark, Shell.OS os) {
        switch (os) {
            case MacOS:
            case Linux:
                return DESKTOP.containsKey(benchmark) ? DESKTOP.get(benchmark) : 20000000;
            case Android:
                return ANDROID.containsKey(benchmark) ? ANDROID.get(benchmark) : 1000000;
            default:
                throw new RuntimeException("Unknown OS to run benchmarks");
        }
    }

    public static String buildMSA2Path(Benchmark benchmark, int scaling, String pkg) {
        return String.format("../MSA2/src/%s/java/%s", benchmark, pkg == null ? "" : pkg.replace('.', File.separatorChar));
    }

    // ESMS/strategy/g/scaling/FFT3/128/
    // "--ip 32 --scaling 7 128"
    // ==> ESMS/FFT3/strategy/g/scaling/params/
    public static String buildESMSPath(Benchmark benchmark, int scaling, Strategy strategy, int groups, int... params) {
        String cfg = "";
        for(int param: params)
            cfg = Shell.path(cfg, String.valueOf(param));
//        return Shell.path(BUILD, "ESMS", strategy.name(), String.valueOf(groups), String.valueOf(scaling), benchmark.name(), cfg);
        return Shell.path(BUILD, "ESMS", benchmark.name(), strategy.name(), String.valueOf(groups), String.valueOf(scaling), cfg);
    }

    //StreamIt/scalingt/AutoCor/64/16
    // ==> StreamIt/AutoCor/scalingt/64/16
    public static String buildStreamItPath(Benchmark benchmark, int scaling, int... params) {
        String cfg = "";
        for(int param: params)
            cfg = Shell.path(cfg, String.valueOf(param));

        return Shell.path(BUILD, "StreamIt", benchmark.name(), String.valueOf(scaling), cfg);
    }
}
