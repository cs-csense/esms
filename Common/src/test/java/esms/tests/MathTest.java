package esms.tests;

import org.junit.Test;

import esms.math.Rational;

import static org.junit.Assert.assertEquals;

/**
 * Created by farleylai on 2/16/14.
 */
public class MathTest {
    @Test
    public void testRational() {
        Rational rational = new Rational(-2.0625);
        assertEquals(-33, rational.numerator());
        assertEquals(16, rational.denominator());
//        System.out.printf("P/Q=%d/%d=%f\n", rational.numerator(), rational.denominator(), rational.toDouble());
    }

    @Test
    public void testLCMs() {
        int numbers[] = new int[]{2, 3, 6, 8, 12};
        int lcms = Rational.lcms(numbers);
        assertEquals(24, lcms);
    }

    @Test
    public void testGCDs() {
        int numbers[] = new int[]{14, 98, 63, 56, 77};
        int gcds = Rational.gcds(numbers);
        assertEquals(7, gcds);
    }

    private int exp2(int k) {
        return k == 0 ? 1 : 2 * exp2(k - 1);
    }

    private int iLog(int N) {
        int L = 0 ;
        while (exp2(L + 1) <= N)
            L++;
        return L;
    }

    @Test
    public void testILog() {
        for(int N = 1; N < 101; N++) {
            int L = iLog(N);
            assert(exp2(L) <= N);
            assert(N < exp2(L + 1));
        }
    }

    private int findPair(int[] A) {
        int L = 0;
        int R = A.length - 1;
        while (L < R) {
            int K = (L + R) / 2;
            if (A[K] < A[K + 1]) {
                return K;
            }

            if (A[L] < A[K]) {
                R = K;
            } else if (A[K] < A[R]) {
                L = K;
            }
        }
        return L;
    }

    @Test
    public void testFindPair() {
        int[] A = new int[] {0, -1, -2, 3};
        assertEquals(2, findPair(A));
    }
}
