package esms.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import esms.util.CommandLine;
import esms.util.Log;
import esms.util.SSH;
import esms.util.Shell;

import static java.util.stream.Collectors.joining;
import static org.junit.Assert.assertEquals;

/**
 * Created by farleylai on 3/14/14.
 */
public class JavaTest {
    @Rule
    public TestName INFO = new TestName();
    Random random = new Random();
    int multiple = 8;
    int base[] = new int[]{1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192};
    int size[] = new int[10];
    ByteBuffer srcs[] = new ByteBuffer[10];
    ByteBuffer sinks[] = new ByteBuffer[10];
    long nanos;

    @Before
    public void setUp() {
        for (int i = 0; i < size.length; i++) {
//            System.out.printf("Set up a ByteBuffer of %d bytes\n", sizeInBytes[i] = base[i] * multiple);
            srcs[i] = ByteBuffer.allocateDirect(size[i]);
            sinks[i] = ByteBuffer.allocateDirect(size[i]);
        }

        for (ByteBuffer src : srcs) {
            while (src.hasRemaining())
                src.putInt(random.nextInt());

            src.flip();
        }
        System.gc();
        nanos = System.nanoTime();
    }

    @After
    public void tearDown() {
        nanos = System.nanoTime() - nanos;
        System.out.printf("time elapse: %.3fus\n", nanos / 1000.0);
    }

    @Test
    public void testMemcpy() throws IOException {
        for (int i = 0; i < sinks.length; i++) {
            ByteBuffer sink = sinks[i];
            ByteBuffer src = srcs[i];
            int sz = src.remaining();

            // bulk put
            long beginning = System.nanoTime();
            sink.put(src);
            long memcpyDuration = System.nanoTime() - beginning;

            // put bytes in a loop
            src.clear();
            sink.clear();
            beginning = System.nanoTime();
            for (int s = 0; s < sz; s++)
                sink.put(src.get());
            long peekDuration = System.nanoTime() - beginning;
//            System.out.printf("memcpy of %d bytes: %.3fns\n", sz, memcpyDuration / 1000.0);
//            System.out.printf("peeking of %d bytes: %.3fns\n", sz, peekDuration / 1000.0);
        }
    }

    @Test
    public void testStream() {
        List<String> l = new ArrayList(Arrays.asList("one", "two"));
        Stream<String> sl = l.stream();
        l.add("three");
        String s = sl.collect(joining(" "));
        assertEquals(s, "one two three");
    }

    private Number getNumberNull(int n) {
        return n == 777 ? new Integer(777) : null;
    }

//    private Optional<Number> getNumberOptional(int n) {
//        return n == 777 ? Optional.of(777) : Optional.empty();
//    }

    @Test
    public void testNumberNull() {
        int count = 0;
        for (int i = 0; i < 10000; i++) {
            Number number = getNumberNull(i);
            if (number != null) count += number.intValue();
        }
        assertEquals(777, count);
    }

    @Test
    public void testOperator() {
        int[] array = new int[] {0, 1} ;
        array[0]++;
        Log.i("array[0]: %d\n", array[0]);
    }

    @Test
    public void testFloat() {
        int taps = 64;
        int cutoff = 108000000;
        int rate = 250000000;
        float pi = 3.1415927f;//(float)Math.PI;
        float[] coeff = new float[taps];

        float m = 64 - 1;
        float w = 2 * pi * cutoff / rate;
        for (int i = 0; i < taps; i++) {
            if (i - m/2 == 0)
                coeff[i] = w/pi;
            else
                coeff[i] = (float)(Math.sin(w*(i-m/2)) / pi / (i-m/2) *
                        (0.54 - 0.46 * Math.cos(2*pi*i/m)));
        }

        float sum = 0;
        for(int i = 0; i < taps; i++) {
            sum += (float)i * coeff[i];
            Log.i(INFO.getMethodName(), "peek(%d): %f, coeff[%d]: %f, sum: %f\n", i, (float)i, i, coeff[i], sum);
        }
    }

    @Test
    public void testDouble() {
        int taps = 64;
        int cutoff = 108000000;
        int rate = 250000000;
        double pi = 3.1415927;//Math.PI;
        double[] coeff = new double[taps];

        double m = 64 - 1;
        double w = 2 * Math.PI * cutoff / rate;
        for (int i = 0; i < taps; i++) {
            if (i - m/2 == 0)
                coeff[i] = w/pi;
            else
                coeff[i] = (Math.sin(w*(i-m/2)) / pi / (i-m/2) *
                        (0.54 - 0.46 * Math.cos(2*pi*i/m)));
        }

        double sum = 0;
        for(int i = 0; i < taps; i++) {
            sum += i * coeff[i];
            Log.i(INFO.getMethodName(), "peek(%d): %f, coeff[%d]: %f, sum: %f\n", i, (float)i, i, coeff[i], sum);
        }
    }

    @Test
    public void tesetTypeCast() {
        float x = (float)3.141592653589793238f;
        double z = 3.141592653589793238f;
        Log.i("x=%f\n", (float)x);
        Log.i("z=%f\n", z);
        Log.i("x=%20.16f\n", (float)x);
        Log.i("z=%20.16f\n", z);
        Log.i("pi=%20.16f\n", Math.PI);

        float v1 = 0.7071067811865476f;
        float v2 = (float)0.70710677;

        Log.i(INFO.getMethodName(), "v1=%.16f\n", v1);
        Log.i(INFO.getMethodName(), "v2=%.16f\n", v2);
        Log.i(INFO.getMethodName(), "v1==v2? " + (v1==v2));

//        3.141592741012573200
//        3.141592741012573242
//
//        3.141592653589793000
//        3.141592653589793116
    }

    @Test
    public void testAtan() {
        float temp1 = 62981764.0000000000000000f;
        float temp2 = (float) (2.14859168E8f  *  Math.atan(temp1));
        Log.i("temp2: %.16f\n", temp2);
    }

    public static int bitrev(int inp, int numbits) {
        int i, rev=0;
        for (i=0; i < numbits; i++) {
            rev = (rev * 2) | (inp & 1);
            inp = inp / 2;
        }
        return rev;
    }

    @Test
    public void testBitRev() {
        int N = 4;
        int logN = 2;
        for (int i=0; i<N; i++) {
            int br = bitrev(i, logN);
            /* treat real and imag part of one point together */
            Log.i(INFO.getMethodName(), "2br: %d, 2br+1: %d\n", 2*br, 2*br+1);
        }
    }

    @Test
    public void testDoubleEqual() {
        double value = 1024;
        boolean fractional = value - (long)value > 0;
        Log.i(INFO.getMethodName(), "value is fractional: " + fractional);
        assert(!fractional);
    }

    @Test
    public void testShell() {
        if(Shell.run(new CommandLine("env")) == Shell.EXIT_SUCCESS)
            Log.i(INFO.getMethodName(), "found strc");
    }

    @Test
    @Ignore
    public void testSSH() {
        Shell.run(Shell.cmd("ls"));
        SSH ssh = new SSH("odroid", "odroid", "128.255.45.245", 2222);
        ssh.run(Shell.cmd("ls"));
        ssh.scp("Common.iml", "Documents");
    }
}
