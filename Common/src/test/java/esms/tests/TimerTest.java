package esms.tests;

import org.junit.Test;

import esms.util.Log;
import esms.util.Timer;

/**
 * Created by farleylai on 12/2/14.
 */
public class TimerTest {
    @Test
    public void testTimers() throws InterruptedException {
        Timer e1 = new Timer("Sleeping for 500ms");
        Thread.sleep(500);
        e1.log();
        Timer e2 = new Timer("Sleeping for 5000ns");
        Thread.sleep(0, 5000);
        e2.log();
        Timer e3 = new Timer("Sleeping for 800us");
        Thread.sleep(0, 800000);
        e3.log();
        Log.d(e1.getClass().getSimpleName(), e1);
        Log.d(e2.getClass().getSimpleName(), e2);
        Log.d(e3.getClass().getSimpleName(), e3);
    }
}
