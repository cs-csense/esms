package esms.tests;

import org.junit.Test;

import esms.math.Matrix;
import esms.math.Rational;

import static org.junit.Assert.assertEquals;

/**
 * Created by farleylai on 2/16/14.
 */
public class MatrixTest {
    @Test
    public void testMatrixInsert() {
        Matrix M = new Matrix(new double[][]{{2, 3, 5}, {-4, 2, 3}});
        M.set(0, 1, M.column(2));
        assertEquals(5, M.getInt(0, 1));
        assertEquals(3, M.getInt(1, 1));
    }

    @Test
    public void testMatrixSum() {
        Matrix M = new Matrix(new double[][]{{2, 3, 5}, {-4, 2, 3}});
        Matrix sum = M.sum(false);
        assertEquals(10, sum.getInt(0, 0));
        assertEquals(1, sum.getInt(1, 0));

        sum = M.sum(true);
        assertEquals(-2, sum.getInt(0, 0));
        assertEquals(5, sum.getInt(0, 1));
        assertEquals(8, sum.getInt(0, 2));
    }

    @Test
    public void testMatrixExtraction() {
        Matrix M = new Matrix(new double[][]{{2, 3, 5}, {-4, 2, 3}});
        Matrix N = new Matrix(new double[][]{{1, 1, 1}, {-1, -1, -1}});
        Matrix M_N = M.minus(N);
        Matrix E = M.extract(0, M.END, 1, M.END);
//        System.out.println(M);
//        System.out.println(N);
//        System.out.println(M_N);
//        System.out.println(E);
    }

    @Test
    public void testNullSpace() {
        Matrix tm = new Matrix(new double[][]{{2, 3, 5}, {-4, 2, 3}});
//        System.out.println(tm);
        Matrix nullspace = tm.nullspace();
//        System.out.println(nullspace);

        Matrix PQ = new Matrix(nullspace.rows(), 2);
        for (int i = 0; i < nullspace.rows(); i++) {
            Rational rational = new Rational(nullspace.get(i, 0));
            PQ.set(i, 0, rational.numerator());
            PQ.set(i, 1, rational.denominator());
        }

//        System.out.println(PQ);
    }
}
