package esms.profiling

import javax.xml.ws.Holder;

import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfunsignedShort;
import org.datacontract.schemas._2004._07.powertool.*;
import org.tempuri.*;


import esms.util.Log;
import static java.math.BigInteger.*;

/**
 * Monsoon power monitor webservice client.
 */
public class RemoteMonsoonProfiler extends ExternalProfiler {
    public static void main(String[] args) throws Exception {
        String action = "";
        String csv = "";
        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-a")) action = args[++i];
            else if(args[i].equals("-f")) csv = args[++i];
        }

        RemoteMonsoonProfiler profiler = new RemoteMonsoonProfiler();
        if(action.equals("start")) {
            profiler.startService();
            profiler.startProfiling();
        } else if(action.equals("stop")) {
            profiler.stopProfiling();
            Log.i(profiler.TAG, "main channel profiled: samples=%d for %.3fs, avg. %.3fV, %.3fmA, %.3fmW, total %.3fuAh\n",
                    profiler.getSampleCount(), profiler.getSampleTime(),
                    profiler.getAvgMainVoltage(),
                    profiler.getAvgMainCurrent(),
                    profiler.getAvgMainPower(),
                    profiler.getTotalEnergy());
            if(!csv.isEmpty()) {
                profiler.save(csv);
                Log.i(profiler.TAG, "%s exported\n", csv);
            }
            profiler.stopService();
        }
    }

    private IPowerToolService _pt;
    private SelectionData _sd;

    public RemoteMonsoonProfiler(IPowerToolService pt) {
        _pt = pt;
    }

    public RemoteMonsoonProfiler() {
        _pt = new PowerToolService().getBasicHttpBindingIPowerToolService();
    }

    public long getSampleCount() {
        return _sd == null ? 0 : _sd.getSampleCount().longValue();
    }

    public double getSampleTime() {
        return _sd == null ? 0 : _sd.getSampleTime();
    }

    public double getAvgMainVoltage() {
        return _sd == null ? 0 : _sd.getSumMainVoltage() / _sd.getSampleCount().longValue();
    }

    public double getAvgMainCurrent() {
        return _sd == null ? 0 : _sd.getSumMainCurrent() / _sd.getSampleCount().longValue();
    }

    public double getAvgMainPower() {
        return _sd == null ? 0 : _sd.getSumMainPower() / _sd.getSampleCount().longValue();
    }

    /**
     * Charge of current over time uAh
     * @return
     */
    public double getTotalEnergy() {
        return _sd == null ? 0 : getAvgMainCurrent() * _sd.getSampleTime() / 3600 * 1000;
    }

    @Override
    public RemoteMonsoonProfiler startService() {
        // Single use only, clear any previous unfinished processing
        if(_pt.getSampleIsRunning()) _pt.stopSamplingF(true);
        if(_pt.getDeviceIsConnected()) _pt.disconnectDevice();
        if(_pt.getApplicationIsOpen()) _pt.closeApplicationF(false, true);
        if(_pt.getDeviceCount() == 0) {
            // FIXME if resetPowerMonitor returns false, reboot the power monitor
            throw new RuntimeException("No Monsoon power meter is plugged in");
            //assertTrue(pt.resetPowerMonitor());
        }

        // Enumerate devices, assume only one attached
        Holder<Long> count = new Holder<>();
        Holder<ArrayOfunsignedShort> serials = new Holder<>();
        _pt.enumerateDevices(count, serials);
        while(count.value == 0) {
            wait(1000);
            Log.w(TAG, "No Monsoon power monitor is enumerated, retry...");
            _pt.enumerateDevices(count, serials);
        }
        Log.i(TAG, "Found connected Monsoon monitor of serial %d\n", serials.value.getUnsignedShort().get(0));

        // Open application, connect device and set parameters for sampling the main channel
        // No need to set trigger setting and it works better when visible
        _pt.openApplicationFG(false, true, true);
        _pt.connectDevice(serials.value.getUnsignedShort().get(0));
        _pt.setUsbPassthroughMode(UsbPassthroughMode.AUTO);
        _pt.setEnableMainOutputVoltage(true);
        _pt.setMainOutputVoltageSetting(3.7f);
        _pt.setBatterySize(3220L);
        return this;
    }

    @Override
    public RemoteMonsoonProfiler stopService() {
        _pt.disconnectDevice();
        _pt.closeApplicationF(false, true);
        if(_pt.getExitCode() != ExitCode.SUCCESS)
            throw new RuntimeException("Failed to close power monitor: " + _pt.getExitCode());
        return this;
    }

    @Override
    public RemoteMonsoonProfiler doStartProfiling() {
        _pt.startSamplingF(true);
        return this;
    }

    @Override
    public RemoteMonsoonProfiler doStopProfiling() {
        _pt.stopSamplingF(true);
        _sd = _pt.getSelectionData();
        return this;
    }

    @Override
    public RemoteMonsoonProfiler save(String filename) throws IOException {
        //File file = new File(Program.getTracePath(), filename);
        // save samples in pt5 and csv on the remote Windows machine
        //filename = "\\\\vmware-host\\Shared Folders\\Downloads\\pt";
        //filename = "C:\\Users\\LaiFarley\\OneDrive\Batching\\pt-xxxx-xxxx.csv";
        _pt.exportCSV(ZERO, _pt.getTotalSampleCount().subtract(ONE), 1L, filename, true, true);
        //assertTrue(pt.saveFile(filename + ".pt5", true, true));
        return this;
    }
}
