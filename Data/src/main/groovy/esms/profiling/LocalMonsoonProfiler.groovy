package esms.profiling

import esms.util.Log
import esms.util.Shell
import gnu.io.CommPortIdentifier
import gnu.io.SerialPort
import gnu.io.SerialPortEvent
import gnu.io.SerialPortEventListener
import org.omg.IOP.TAG_INTERNET_IOP

import java.nio.ByteBuffer
import java.nio.FloatBuffer
import java.nio.channels.Channel
import java.nio.channels.Channels
import java.nio.channels.WritableByteChannel

/**
 * Groovy/Java Interface for a USB-connected Monsoon power meter
 * (http://msoon.com/LabEquipment/PowerMonitor/).
 *
 * Example usages:
 * Set the voltage of the device 6325 to 3.7V
 * groovyclient esms.profiling.LocalMonsoonProfiler -=3.7 --serial 6325
 *
 * Get 5000hz data from device number 6325, with unlimited number of samples
 * groovyclient esms.profiling.LocalMonsoonProfiler -samples -1 -r 5000 -serial 7536
 *
 * Get 200Hz data for 5 seconds (1000 events) from default device
 * groovyclient esms.profiling.LocalMonsoonProfiler -samples 100 -r 200
 *
 * Get unlimited 200Hz data from device attached at /dev/ttyACM0
 * groovyclient esms.profiling.LocalMonsoonProfiler -samples -1 -r 200 -dev [/dev/cu.usbmodem1471 | /dev/ttyACM0]
 */
public class LocalMonsoonProfiler extends ExternalProfiler implements MonsoonEventListener {
    public static class MonsoonEvent extends EventObject {
        MonsoonEvent(MonsoonDevice src, int seq, ByteBuffer samples) {
            super(src)
            this.seq = seq
            this.samples = samples
        }
        int seq
        ByteBuffer samples
    }

    public static interface MonsoonEventListener extends EventListener {
        def deliver(MonsoonEvent e);
    }

    public static class MonsoonDevice implements SerialPortEventListener {
        final String TAG = getClass().simpleName
        static class Status {
            static FIELDS = [
                    "packetType", "firmwareVersion", "protocolVersion",
                    "mainFineCurrent", "usbFineCurrent", "auxFineCurrent", "voltage1",
                    "mainCoarseCurrent", "usbCoarseCurrent", "auxCoarseCurrent", "voltage2",
                    "outputVoltageSetting", "temperature", "status", "leds",
                    "mainFineResistor", "serialNumber", "sampleRate",
                    "dacCalLow", "dacCalHigh",
                    "powerUpCurrentLimit", "runTimeCurrentLimit", "powerUpTime",
                    "usbFineResistor", "auxFineResistor",
                    "initialUsbVoltage", "initialAuxVoltage",
                    "hardwareRevision", "temperatureLimit", "usbPassthroughMode",
                    "mainCoarseResistor", "usbCoarseResistor", "auxCoarseResistor",
                    "defMainFineResistor", "defUsbFineResistor", "defAuxFineResistor",
                    "defMainCoarseResistor", "defUsbCoarseResistor", "defAuxCoarseResistor",
                    "eventCode", "eventData"]

            static size() {
                return 58
            }

            static unpack(ByteBuffer pkt) {
                //'>BBBhhhHhhhHBBBxBbHBHHHHBbbHHBBBbbbbbbbbbBH' = 58 bytyes
                //'>3B3hH3hH3BxBbHB4HB2b2H3B9bBH'
                def values = []
                3.times { values << (pkt.get() & 0xFF) }
                3.times { values << pkt.getShort() }
                values << (pkt.getShort() & 0xFFFF)
                3.times { values << pkt.getShort() }
                values << (pkt.getShort() & 0xFFFF)
                3.times { values << (pkt.get() & 0xFF) }

                pkt.get()

                values << (pkt.get() & 0xFF)
                values << pkt.get()
                values << (pkt.getShort() & 0xFFFF)
                values << (pkt.get() & 0xFF)
                4.times { values << (pkt.getShort() & 0xFFFF) }
                values << (pkt.get() & 0xFF)
                2.times { values << pkt.get() }
                2.times { values << (pkt.getShort() & 0xFFFF) }
                3.times { values << (pkt.get() & 0xFF) }
                9.times { values << pkt.get() }
                values << (pkt.get() & 0xFF)
                values << (pkt.getShort() & 0xFFFF)
                pkt.clear()
                def status = [FIELDS, values].transpose().collectEntries {
                    [(it[0]): it[1]]
                }
                assert status.packetType == 0x10
                for(String k in status.keySet()) {
                    if(k.endsWith("VoltageSetting")) // Convert to voltage
                        status[k] = 2.0 + status[k] * 0.01
                    else if(k.endsWith("FineCurrent"))
                        continue // needs calibration data
                    else if(k.endsWith("CoarseCurrent"))
                        continue // needs calibration data
                    else if(k.startsWith("voltage") || k.endsWith("Voltage")) // A: 62.5 and B,C for 125uV = 0.000125V / tick
                        status[k] *= 0.000125
                    else if(k.endsWith("Resistor")) { // ohms
                        status[k] = 0.05 + status[k] * 0.0001
                        if(k.startsWith("aux") || k.startsWith("defAux")) status[k] += 0.05
                    } else if(k.endsWith("CurrentLimit")) // amps
                        status[k] = 8 * (1023 - status[k]) / 1023.0
                }
                return status
            }

            static String toString(status = [:].withDefault { 0 }) {
                StringBuilder builder = new StringBuilder()
                FIELDS.each {
                    builder << "$it=${status[it]}\n"
                }
                return builder.toString()
            }
        }

        public static final String PREFIX_OSX = 'cu.usbmodem'
        public static final String PREFIX_LINUX = 'ttyACM'
        public static enum UsbPassthrough { OFF, ON, AUTO, }
        public static List<MonsoonDevice> enumerate(serial = null) {
            def devices = []
            def prefix = Shell.os() == Shell.OS.MacOS ? PREFIX_OSX : PREFIX_LINUX
            new File('/dev').listFiles(new FilenameFilter() {
                @Override
                boolean accept(File dir, String name) {
                    return name.startsWith(prefix)
                }
            }).each {
                def device = open(it, serial)
                Log.d('enumerate()', "open($it, $serial) returns $device")
                if(device) devices << device
            }
            return devices
        }

        public static MonsoonDevice open(File path, serial = null) {
            return MonsoonDevice.open(path.absolutePath, serial)
        }

        public static MonsoonDevice open(String path, serial = null) {
            MonsoonDevice device = new MonsoonDevice(path)
            return device.open(serial) ? device : null
        }

        String _path       // cu.usbmodem1471:6325
        SerialPort _port
        InputStream _in
        OutputStream _out
        ByteBuffer _pkt
        MonsoonEventListener _listener

        def _status
        boolean _error
        boolean _dataAvailableNotified

        short _coarse_ref, _fine_ref
        short _coarse_zero, _fine_zero
        float _coarse_scale, _fine_scale
        public MonsoonDevice(String path) {
            _path = path
            _pkt = ByteBuffer.allocate(Math.max(128, 8 * 128))
        }

        public String getName() {
            return _port?.getName();
        }

        public float current(short value) {
            return (value & 1) ? ((value & ~1) - _coarse_zero) * _coarse_scale : (value - _fine_zero) * _fine_scale
        }

        public boolean open(serial = null) throws IOException { // 9600, 8N1
            def port = CommPortIdentifier.getPortIdentifier(_path)
            if(port.isCurrentlyOwned()) return false
            port = port.open(getClass().simpleName, 2000)
            if(port instanceof SerialPort) {
                _port = (SerialPort)port
                _port.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE)

                //Log.d(TAG, "receiveTimeout(): ${_port.isReceiveTimeoutEnabled()}, ${_port.getReceiveTimeout()}")
                //Log.d(TAG, "receiveThreshold(): ${_port.isReceiveThresholdEnabled()}, ${_port.getReceiveThreshold()}")
                //_port.disableReceiveTimeout();
                //_port.enableReceiveThreshold(256);
                //_port.setInputBufferSize(256)
                Log.d(TAG, "getInputBufferSize()=${_port.getInputBufferSize()}, getOutputBufferSize()=${_port.getOutputBufferSize()}")
                Log.d(TAG, "receiveTimeout(): ${_port.isReceiveTimeoutEnabled()}, ${_port.getReceiveTimeout()}")
                Log.d(TAG, "receiveThreshold(): ${_port.isReceiveThresholdEnabled()}, ${_port.getReceiveThreshold()}")
                _in = _port.getInputStream()
                _out = _port.getOutputStream()
                if(!_in || !_out) {
                    _port.close()
                    return false
                }

                send(0x03, 0x00)
                Log.d(TAG, 'stopped')
                drain()
                status()
                Log.d(TAG, 'status read')
                if(_status && (serial ? _status.serialNumber == serial : true)) {
                    return true
                }
            }
            _port.close()
            return false
        }

        def close() {
            drain()
            _in?.close()
            _out?.close()
            _port?.close()
            _in = null
            _out = null
            _port = null
        }

        def start() {
            _pkt.clear()
            _port.addEventListener(this)
            _port.notifyOnParityError(true)
            _port.notifyOnFramingError(true)
            _port.notifyOnOverrunError(true)
            _port.notifyOnDataAvailable(_dataAvailableNotified = true)
            send(0x01, 0x1b, 0x01)
            send(0x02, 0xff, 0xff, 0xff, 0xff, 0x03, 0xe8)
        }

        def stop() {
            Log.d(TAG, "stop(): _in.available()=${_in.available()}, error=${_error}");
            _port.notifyOnDataAvailable(_dataAvailableNotified = false)
            _port.notifyOnParityError(false)
            _port.notifyOnFramingError(false)
            _port.notifyOnOverrunError(false)
            //_port.removeEventListener()
            send(0x03, 0x00)

            5.times {
                Log.d(TAG, "stopping for ... $it")
                sleep(1)
            }
        }

        def drain() {
            // Drain input in case of previous unfinished sampling
            //_port.notifyOnDataAvailable(false)
            _out.flush()
            int flushed = 0
            int counter =  2
            while(!_error && _in.available() > 0 && counter-- > 0) {
                flushed += _in.skip(_in.available())
                sleep(250)
            }
            if(flushed > 0)
                Log.w(TAG, "$flushed bytes drained\n")
            //_port.notifyOnDataAvailable(_dataAvailableNotified)
            return flushed
        }

        def send(int... bytes) {
            // packet = [len, bytes, checksum = (sum of bytes + len) % 256]
            int len = bytes.length + 1
            int checksum = (bytes.sum() + len) % 256
            byte[] pkt = new byte[2 + bytes.length]
            if(true) {
                pkt[0] = (byte) len
                bytes.eachWithIndex { b, i ->
                    pkt[1 + i] = b as byte
                }
                pkt[-1] = (byte) checksum
                _out.write(pkt, 0, pkt.length)
            } else {
                _out.write(len)
                bytes.each {
                    _out.write(it)
                }
                _out.write(checksum)
            }
            _out.flush()
            Log.d(TAG, "sent pkt [${pkt.collect{ it & 0xFF }.join(', ')}] of $pkt.length bytes")
            return 1 + len
        }

        // Read one full or partial packet at a time
        def recv(int available) throws IOException {
            int leftover
            if(_pkt.position() > 0)
                leftover = (_pkt.get(0) & 0xFF) - (_pkt.position() - 1)
            else {
                leftover = _in.read()
                _pkt.put((byte)leftover)
            }
            int bytes = _in.read(_pkt.array(), _pkt.position(), Math.min(leftover, available))
            if(bytes >= 0) {
                //Log.d(TAG, "recv($available), leftover=$leftover, position=${_pkt.position()}, bytes=$bytes")
                _pkt.position(_pkt.position() + bytes)
            }
            return bytes
        }

        def deliver() {
            // a packet of samples w/o header or checksum
            int size = _pkt.remaining()
            int B0 = _pkt.get() & 0xFF
            int seq = B0 & 0xF
            int type = _pkt.get() & 0xFF
            if(_pkt.remaining() < 4 + 8 + 1 || B0 < 0x20 || B0 > 0x2F) {
                //Log.w(TAG, "deliver(): Wanted data, dropped pkt of type=0x%X, size=${_pkt.remaining()}\n", B0)
                Log.w(TAG, "deliver(): Wanted data, dropped pkt: [0x%02X(0x%02X), 0x%02X, ...] of size=$size\n", B0, seq, type)
                return
            } else {
                //Log.d(TAG, "deliver(): pkt: [0x%02X(0x%02X), 0x%02X, ...] of size=$size\n", B0, seq, type)
            }

            _pkt.position(_pkt.position() + 2)
            switch(type) {
                case 0: // data packet
                    //Log.d(TAG, "coarse_scale=$_coarse_scale, fine_scale=$_fine_scale")
                    if(!_coarse_scale || !_fine_scale) {
                        Log.w(TAG, 'Waiting for calibration, dropped data packet')
                        return
                    }
                    _listener.deliver(new MonsoonEvent(this, seq, _pkt))
                    break
                case 1:
                    _fine_zero = _pkt.getShort(_pkt.position() + 0)
                    _coarse_zero = _pkt.getShort(_pkt.position() + 8)
                    //Log.d(TAG, "Zero calibration: fine 0x%04x, coarse 0x%04x\n", _fine_zero, _coarse_zero)
                    break
                case 2:
                    _fine_ref = _pkt.getShort(_pkt.position() + 0)
                    _coarse_ref = _pkt.getShort(_pkt.position() + 8)
                    //Log.d(TAG, "Ref calibration: fine 0x%04x, coarse 0x%04x\n", _fine_ref, _coarse_ref)
                    break
                default:
                    Log.w(TAG, "Discarding data packet of seq=0x%02X and type=0x%02X\n", seq, type)
            }
            if(_coarse_ref != _coarse_zero) _coarse_scale = 2.88f / (_coarse_ref - _coarse_zero)
            if(_fine_ref != _fine_zero) _fine_scale = 0.0332f / (_fine_ref - _fine_zero)
        }

        def rampVoltage(float start, float end) {
            double v = start
            if(v < 3.0) v = 3.0 // protocol doesn't support lower than this
            while (v < end) {
                setVoltage(v)
                v += 0.1
                sleep(1000)
            }
            setVoltage(end)
        }

        def setVoltage(float v) {
            // Set the output voltage, 0 to disable and convert to outputVoltageSetting otherwise
            if(v == 0) send(0x01, 0x01, 0x00)
            else send(0x01, 0x01, (v - 2.0) * 100 as int)
            Log.i(TAG, "set voltage to ${v}V")
        }

        def setMaxCurrent(double amps) {
            assert amps >= 0 && amps <= 8
            def val = 1023 - (amps / 8 * 1023 as int)
            send(0x01, 0x0a, val & 0xff)
            send(0x01, 0x0b, val >> 8)
            Log.i(TAG, "set max current to ${amps}A")
        }

        def setUsbPassthrough(UsbPassthrough passthrough) {
            send(0x01, 0x10, passthrough.ordinal())
            Log.i(TAG, "set usb passthrough to $passthrough")
        }

        // Synchounus reading to get status
        def status(String field = null) {
            if(field) return _status[field]
            else {
                assert _pkt.remaining() == _pkt.capacity()
                //_port.notifyOnDataAvailable(false)
                send(0x01, 0x00, 0x00)
                while(true) {
                    int size = _in.read()
                    while (_pkt.position() < size) {
                        int bytes = _in.read(_pkt.array(), _pkt.position(), size - _pkt.position())
                        //Log.d(TAG, "status(): _in.read(_pkt.array(), ${_pkt.position()}, ${size - _pkt.position()})=$bytes")
                        _pkt.position(_pkt.position() + bytes)
                    }
                    if (size - 1 != Status.size()) {
                        Log.w(TAG, "Dropped non-status packet of $size bytes")
                        _pkt.clear()
                    } else {
                        _pkt.flip()
                        _pkt.limit(_pkt.limit() - 1)
                        break
                    }
                }
                _status = Status.unpack(_pkt)
                _pkt.clear()
                //_port.notifyOnDataAvailable(_dataAvailableNotified)
                return _status
            }
        }

        @Override
        public void serialEvent(SerialPortEvent evt) {
            switch(evt.getEventType()) {
                case SerialPortEvent.DATA_AVAILABLE:
                    int available = _in.available()
                    if(!_dataAvailableNotified) {
                        Log.d(TAG, "serialEvent(): skipped $available bytes after stop")
                        _in.skip(available)
                        available = 0
                    }
                    while(available > 0) {
                        int bytes = recv(available)
                        if(bytes == -1) {
                            Log.w(TAG, "MonsoonDevice closed")
                            break
                        }
                        int size = _pkt.get(0) & 0xFF
                        //Log.d(TAG, "serialEvent(): $bytes/$size bytes received")
                        if (size + 1 == _pkt.position()) {
                            // full packet w/ header and checksum
                            _pkt.flip()
                            //Log.d(TAG, "serialEvent(): recv full packet of ${_pkt.remaining()} bytes")
                            int sum = 0
                            (size - 1).times {
                                sum += _pkt.get(it + 1) & 0xFF
                            }
                            int checksum = (size + sum) % 256
                            if(checksum == (_pkt.get(_pkt.remaining() - 1) & 0xFF)) {
                                _pkt.position(_pkt.position() +  1)
                                _pkt.limit(_pkt.limit() - 1)
                                //Log.d(TAG, "serialEvent(): delivering a packet of pos=${_pkt.position()}, limit=${_pkt.limit()}, sz=${_pkt.remaining()} bytes")
                                deliver()
                            } else {
                                Log.e(TAG, "serialEvent(): invalid checksum $checksum != ${_pkt.get(_pkt.remaining() - 1) & 0xFF}")
                            }
                            available -= size + 1
                            _pkt.clear()
                        } else {
                            // partial packet
                            //Log.d(TAG, "serialEvent(): recv partial packet of ${_pkt.position()}/${size+1} bytes so far")
                            available -= bytes
                        }
                    }
                    break

            // Error detections
                case SerialPortEvent.FE:
                case SerialPortEvent.OE:
                case SerialPortEvent.PE:
                    _error = true
                    break

            // Don't care
                case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                case SerialPortEvent.CTS:
                case SerialPortEvent.DSR:
                case SerialPortEvent.CD:
                case SerialPortEvent.BI:
                case SerialPortEvent.RI:
                default: break
            }
        }

        def addEventListener(MonsoonEventListener listener) {
            _listener = listener
        }

        String info() {
            StringBuffer builder = new StringBuffer()
            builder.append("Monsoon Power Meter of serial $_status.serialNumber at $_path\n")
            builder.append(Status.toString(_status));
            return builder.toString()
        }

        @Override
        String toString() {
            return "Monsoon Power Meter of serial $_status.serialNumber at $_path"
        }
    } // End of MonsoonDevice

    def _options
    int _duration
    int _samples
    int _sampleRate
    int _rate
    int _sampleCount
    int _seq
    float _timestamp
    String _trace
    PrintStream _output
    MonsoonDevice _device
    public LocalMonsoonProfiler(options) {
        _options = options
        _duration = options.duration ?: -1
        _samples = options.samples ?: -1
        _rate = options.r
    }

    String trace() {
        return _trace
    }

    @Override
    def deliver(MonsoonEvent e) {
        //Log.d(TAG, "duration=$_duration, timestamp=$_timestamp, samples=$_samples, sampleCount=$_sampleCount")
        if((_duration > 0 && _timestamp > _duration) || (_samples > 0 && _sampleCount > _samples))
            return

        if(_seq && e.seq != (_seq + 1) & 0xF) {
            Log.w(TAG, "packet loss: ${_seq & 0xF} -> ${e.seq & 0xF}, timestamp may skew")
            //_timestamp += ((e.seq - _seq) % 0xF) * (1/_sampleRate)
        }

        _seq = e.seq
        int interval = _sampleRate > _rate ? (_sampleRate + _rate - 1) / _rate : 1
        int count = e.samples.remaining() / 8
        count = _sampleCount + count > _samples ? count - (_sampleCount + count - _samples) : count
        ByteBuffer samples = e.samples
        //Log.d(TAG, "deliver(): samples=[pos=${samples.position()}, bytes=${samples.remaining()}, count=$count]")
        _seq = e.seq
        count.times {
            // One sample consists of up to four 2-bypte integers
            // [ushort main current | usb current | aux current] [ushort main | aux voltage + marker]
            int pos = samples.position() + it * 2 * 4
            if(_sampleCount == 0 || _sampleCount % interval == 0) {
                def (main, voltage) = [_device.current(samples.getShort(pos)), (samples.getShort(pos + 6) & ~0x0003) * 0.000125]
                _output.println("$_timestamp,$main,$voltage")
                _output.flush()
                Log.d(TAG, "timestamp=$_timestamp,main=$main,voltage=$voltage")
            }
            _sampleCount++
            _timestamp += 1f/ _sampleRate
        }
        if(count > 0) Log.i(TAG, "$count samples collected")
    }

    @Override
    public LocalMonsoonProfiler startService() {
        // Open device
        if(_options.dev) {
            _device = new MonsoonDevice(_options.dev as String)
            if(!_device.open(_options.serial)) {
                _device = null
                throw new RuntimeException("Failed to open device at $options.dev")
            }
        } else {
            // enumerate the first monsoon device in the system
            def devices = MonsoonDevice.enumerate(_options.serial)
            devices.each {
                Log.i(TAG, "Monsoon $it.name of serial ${it.status('serialNumber')}\n")
                if (it != devices[0]) it.close()
            }
            _device = devices[0]
        }
        if(!_device) throw new RuntimeException('No Monsoon devices found')

        // Configure device
        _sampleRate = _device.status('sampleRate') * 1000
        _rate = (_rate <= 0 || _rate > _sampleRate) ? _sampleRate : _rate
        switch(_options.usb) {
            case 'off': _device.setUsbPassthrough(MonsoonDevice.UsbPassthrough.OFF); break
            case 'on': _device.setUsbPassthrough(MonsoonDevice.UsbPassthrough.ON); break
            case 'auto': _device.setUsbPassthrough(MonsoonDevice.UsbPassthrough.AUTO); break
            default: throw new RuntimeException("Unknown USB passthrough mode $_options.usb")
        }

        if(_options.v) {
            if(_options.ramp) _device.rampVoltage(_device.status('voltage1') as float, _options.v as float)
            else _device.setVoltage(_options.v as float)
        } else _device.setVoltage(3.7f)

        if(_options.i) _device.setMaxCurrent(_options.i as float)
        if(_options.status) Log.i(TAG, _device.info())
        return this
    }

    @Override
    public LocalMonsoonProfiler stopService() {
        _device.close()
        return this;
    }

    @Override
    public LocalMonsoonProfiler doStartProfiling() {
        _seq = _timestamp = _sampleCount = 0
        _output = new PrintStream(_trace = "/tmp/ptrace-${timestamp()}.csv")
        _output.println('Time (s),Main Avg Current (mA),Main Avg Voltage (V)')
        _device.addEventListener(this)
        _device.start()
        Log.i(TAG, "trace file path: $_trace")
        return this
    }

    @Override
    public LocalMonsoonProfiler doStopProfiling() {
        _device.stop()
        _output.close()
        return this;
    }

    @Override
    public LocalMonsoonProfiler save(String filename) throws IOException {
        int ret = "mv $_trace '$filename'".execute().waitFor()
        if(ret) Log.w(TAG, "Failed to run mv $_trace '$filename'")
        return this;
    }

    public static void main(String[] args) throws Exception {
        // Define flags here to avoid conflicts with people who use us as a library
        def cli = new CliBuilder(usage:'RemoteMonsoonProfiler [options] <url>')
        cli.status(argName:'status', 'Print power meter status')
        cli.samples(argName:'samples', args:1, 'Number of samples to collect')
        cli.duration(argName:'duration', args:1, 'Duration in seconds to collect')
        cli.serial(argName:'serial', args:1, 'Device serial number to connect to')
        cli.dev(argName:'device', args:1, 'path to the deice entry [ /dev/cu.usbmodem1471 | /dev/ttyACM0 ]')
        cli.usb(argName:'usbpassthrough', args:1, 'USB passthrough mode [on | off | auto]')
        cli.v(argName:'voltage', args:1, 'Set output voltage (0 to off)')
        cli.r(argName:'rate', args:1, 'samples/s')
        cli.i(argName:'current', args:1, 'Set max output current')
        //cli.t(argName:'timestamp', 'Print timestamp integer in seconds per line')
        //cli.avg(argName:'avg.', args:1, 'Report average over last n samples')
        cli.ramp(argName:'ramp', 'Gradually increase voltage')
        cli.f(argName:'path', 'trace file path to save')
        def options = cli.parse(args)

        LocalMonsoonProfiler profiler = new LocalMonsoonProfiler(options.properties);
        profiler.startService()
        profiler.startProfiling()
        sleep(3000)
        profiler.stopProfiling()
        profiler.stopService()
        profiler.save("${System.getenv('user.home')}/Dropbox/Batching/")


        /*
        flags.DEFINE_boolean("status", None, "Print power meter status")
        flags.DEFINE_float("voltage", None, "Set output voltage (0 for off)")
        flags.DEFINE_string("usbpassthrough", None, "USB control (on, off, auto)")
        flags.DEFINE_integer("samples", None, "Collect and print this many samples")
        flags.DEFINE_integer("hz", 5000, "Print this many samples/sec")
        flags.DEFINE_string("device", None, "Path to the device in /dev/... (ex:/dev/ttyACM1)")
        flags.DEFINE_integer("serialno", None, "Look for a device with this serial number")
        flags.DEFINE_boolean("timestamp", None, "Also print integer (seconds) timestamp on each line")
        flags.DEFINE_boolean("ramp", True, "Gradually increase voltage")

        flags.DEFINE_float("current", None, "Set max output current")
        flags.DEFINE_integer("avg", None, "Also report average over last n data points")
        */

        /*
        String action = "";
        String csv = "";
        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-a")) action = args[++i];
            else if(args[i].equals("-f")) csv = args[++i];
        }

        RemoteMonsoonProfiler profiler = new RemoteMonsoonProfiler();
        if(action.equals("start")) {
            profiler.startService();
            profiler.startProfiling();
        } else if(action.equals("stop")) {
            profiler.stopProfiling();
            Log.i(profiler.TAG, "main channel profiled: samples=%d for %.3fs, avg. %.3fV, %.3fmA, %.3fmW, total %.3fuAh\n",
                    profiler.getSampleCount(), profiler.getSampleTime(),
                    profiler.getAvgMainVoltage(),
                    profiler.getAvgMainCurrent(),
                    profiler.getAvgMainPower(),
                    profiler.getTotalEnergy());
            if(!csv.isEmpty()) {
                profiler.save(csv);
                Log.i(profiler.TAG, "%s exported\n", csv);
            }
            profiler.stopService();
        }
        */
    }
}
