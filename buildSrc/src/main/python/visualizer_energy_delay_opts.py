#!/usr/bin/python

from os.path import expanduser
from collections import defaultdict
from subprocess import call
from glob import glob
from plot import params, barchart, scatter
import sys, os, getopt
import re

tree = lambda: defaultdict(tree);
dataset = tree();

# ODROID
# perf, csv

def loadDatasetPerf(path):
# perf-1A7-1000000-0A15-0.csv
# started on Mon Sep 14 15:17:13 2015
#
#
# Performance counter stats for './streamit -i 2066666':
#
#     5240415339 cycles                    #    0.000 GHz                    
#     8201472399 instructions              #    1.57  insns per cycle        
#       31476449 branches
#         260844 branch-misses             #    0.83% of all branches        
#     3457124165 cache-references
#         174095 cache-misses              #    0.005 % of all cache refs    
#              0 cpu-migrations
#
#    4.369417357 seconds time elapsed

    logs = glob(path)
    dataset = tree()
    for log in logs:
        benchmark = os.path.basename(os.path.dirname(os.path.dirname(log)))
        options = os.path.basename(os.path.dirname(log))
        options = options.split('-')
        perf = os.path.basename(log).split('.')[0].split('-');
        #print benchmark, options, perf
        c = int(options[1])
        opt = options[2]
        a7 = int(perf[1][0])
        a7f = int(perf[2]) / 1000
        a15 = int(perf[3][0])
        a15f = int(perf[4]) / 1000

        record = tree()
        with open(log) as log:
            lines = log.readlines()
            for i in range(5, 12):
                fields = lines[5].split()[0]
                record[fields[1]] = int(fields[0])
            dataset[benchmark][c][a7][a7f][a15][a15f] = record
    return dataset

def loadDatasetProfile(path):
# profile-1A7-1000000-0A15-0.csv
# 0            , 1    , 2   , 3    ,    , 19   , 20  , 21   , 22   , 23   , 24   , 25   , 26   , 27   , 28   , 29   , 30   , 31   , 32   , 33   , 34  , 35  , 36  , 37
# timestamp(us), user0, sys0, idle0, ..., user7, sys7, idle7, armuV, armuA, armuW, kfcuV, kfcuA, kfcuW, memuV, memuV, memuW, g3duV, g3duA, g3duW, sysV, sysA, sysW, sysWh

    print path
    csvs = glob(path);
    dataset = tree();
    for csv in csvs:
        benchmark = os.path.basename(os.path.dirname(os.path.dirname(csv)))
        options = os.path.basename(os.path.dirname(csv))
        options = options.split('-')
        profile = os.path.basename(csv).split('.')[0].split('-');
        c = int(options[1])
        opt = options[2]
        a7 = int(profile[1][0])
        a7f = int(profile[2]) / 1000
        a15 = int(profile[3][0])
        a15f = int(profile[4]) / 1000
        #print benchmark, c, a7, a7f, a15, a15f
        with open(csv) as csv:
            lines = csv.readlines()
            header = lines[0].strip().split(', ')
            armuWh = 0
            kfcuWh = 0
            memuWh = 0
            sysWh  = float(lines[-2].strip().split(', ')[-1])
            secs = 0
            #print header
            for i in range(1, len(lines) - 1):
                #print lines[i]
                #print lines[i+1]
                fieldsFrom = lines[i].strip().split(', ')
                fieldsTo = lines[i+1].strip().split(', ')
                us = int(fieldsTo[0]) - int(fieldsFrom[0])
                armuW = (float(fieldsFrom[24]) + float(fieldsTo[24])) / 2
                armuWh = armuWh + armuW * us / 1000000 / 3600
                kfcuW = (float(fieldsFrom[27]) + float(fieldsTo[27])) / 2
                kfcuWh = kfcuWh + kfcuW * us / 1000000 / 3600
                memuW = (float(fieldsFrom[30]) + float(fieldsTo[30])) / 2
                memuWh = memuWh + memuW * us / 1000000 / 3600
                secs = secs + us
            secs = secs / 1000000.0
            if(a7 == 0):
                dataset[benchmark][c][opt][a7][a7f][a15][a15f] = [secs, armuWh, kfcuWh, memuWh, sysWh, ("%d-%s-%dA15(%.1fGHz)" % (c, opt, a15, a15f/1000.0))]
            elif(a15 == 0):
                dataset[benchmark][c][opt][a7][a7f][a15][a15f] = [secs, armuWh, kfcuWh, memuWh, sysWh, ("%d-%s-%dA7(%.1fGHz)" % (c, opt, a7, a7f/1000.0))]
            else:
                dataset[benchmark][c][opt][a7][a7f][a15][a15f] = [secs, armuWh, kfcuWh, memuWh, sysWh, ("%d-%s-%dA7(%.1fGHz)-%dA15(%.1fGHz)" % (c, opt, a7, a7f/1000.0, a15, a15f/1000.0))]
    return dataset;


def plotEnergyDelay(profiles, prefix='', legendsize=16):
    MSB = ["AutoCor", "BitonicSort", 'BitonicRecursive', 'FilterBankNew',  "FIRcoarse", "FIR", "FFT2", "FMRadio", "MatrixMult", "MatrixMultBlock", "MergeSort", "Repeater", "BeepBeep"]
    CLUSTERS = [1,2,4,8]
    A7F = [800000, 1000000, 1200000, 1400000]
    A15F = [1200000, 1400000, 1600000, 1800000, 2000000]    
    
    #print profiles
    for msb in profiles:
        samples1 = []
        samples2 = []
        samples4 = []
        samples8 = []
        samples = [samples1, samples2, samples4, samples8]
        for c in CLUSTERS:
            for opt in profiles[msb][c]:
                for a7 in profiles[msb][c][opt]:
                    for a7f in profiles[msb][c][opt][a7]: 
                        for a15 in profiles[msb][c][opt][a7][a7f]: 
                            for a15f in profiles[msb][c][opt][a7][a7f][a15]:
                                if(c == 1): samples1.append(profiles[msb][c][opt][a7][a7f][a15][a15f])
                                if(c == 2): samples2.append(profiles[msb][c][opt][a7][a7f][a15][a15f])
                                if(c == 4): samples4.append(profiles[msb][c][opt][a7][a7f][a15][a15f])
                                if(c == 8): samples8.append(profiles[msb][c][opt][a7][a7f][a15][a15f])

        x = [[s[0] for s in samples1]] + [[s[0] for s in samples2]] + [[s[0] for s in samples4]] + [[s[0] for s in samples8]]
        y1 = [[(s[1]+s[2]+s[3])/1000.0 for s in samples1]] + [[(s[1]+s[2]+s[3])/1000.0 for s in samples2]]
        y1 += [[(s[1]+s[2]+s[3])/1000.0 for s in samples4]] + [[(s[1]+s[2]+s[3])/1000.0 for s in samples8]]
        y2 = [[s[4]*1000 for s in samples1]] + [[s[4]*1000 for s in samples2]]
        y2 += [[s[4]*1000 for s in samples4]] + [[s[4]*1000 for s in samples8]]
        scatter(x, y1, 'Latency(s)', 'Energy(mWh)', ['c=1', 'c=2', 'c=4', 'c=8'], "figures/odroid/%s-edc-sensor-all.pdf" % msb)
        scatter(x, y2, 'Latency(s)', 'Energy(mWh)', ['c=1', 'c=2', 'c=4', 'c=8'], "figures/odroid/%s-edc-smartpower-all.pdf" % msb)

        latencies = [[min(g)] for g in x]
        pos = [x[i].index(latencies[i][0]) for i in range(len(x))]
        energies1 = [[y1[i][pos[i]]] for i in range(len(y1))]
        energies2 = [[y2[i][pos[i]]] for i in range(len(y2))]
        legends = [samples[i][pos[i]][-1] for i in range(len(pos))]
        print 'Energy by Min Latencies'
        print latencies
        print energies1
        print energies2
        scatter(latencies, energies1, 'Latency(s)', 'Energy(mWh)', legends, "figures/odroid/%s-edc-sensor-min-latency.pdf" % msb)
        scatter(latencies, energies2, 'Latency(s)', 'Energy(mWh)', legends, "figures/odroid/%s-edc-smartpower-min-latency.pdf" % msb)

        energies1 = [[min(g)] for g in y1]
        pos = [y1[i].index(energies1[i][0]) for i in range(len(y1))]
        latencies = [[x[i][pos[i]]] for i in range(len(x))]
        energies2 = [[y2[i][pos[i]]] for i in range(len(y2))]
        legends = [samples[i][pos[i]][-1] for i in range(len(pos))]
        print 'Latency by Min Core Energy'
        print latencies
        print energies1
        print energies2
        scatter(latencies, energies1, 'Latency(s)', 'Energy(mWh)', legends, "figures/odroid/%s-edc-sensor-min-cores.pdf" % msb)
        scatter(latencies, energies2, 'Latency(s)', 'Energy(mWh)', legends, "figures/odroid/%s-edc-smartpower-min-cores.pdf" % msb)

        energies2 = [[min(g)] for g in y2]
        pos = [y2[i].index(energies2[i][0]) for i in range(len(y2))]
        latencies = [[x[i][pos[i]]] for i in range(len(x))]
        energies1 = [[y1[i][pos[i]]] for i in range(len(y1))]
        legends = [samples[i][pos[i]][-1] for i in range(len(pos))]
        print 'Latency by Min System Energy'
        print latencies
        print energies1
        print energies2
        scatter(latencies, energies1, 'Latency(s)', 'Energy(mWh)', legends, "figures/odroid/%s-edc-sensor-min-sys.pdf" % msb)
        scatter(latencies, energies2, 'Latency(s)', 'Energy(mWh)', legends, "figures/odroid/%s-edc-smartpower-min-sys.pdf" % msb)

if __name__ == '__main__':
    logs = sys.argv[1] + "/AutoCor/*/*.log"
    csvs = sys.argv[1] + "/AutoCor/*/*.csv"
    logs = sys.argv[1] + "/*/*/*.log"
    csvs = sys.argv[1] + "/*/*/*.csv"
    perfs = loadDatasetPerf(logs)            # StreamIt/build/MSB/ubuntu/*/*/*.log
    profiles = loadDatasetProfile(csvs)      # StreamIt/build/MSB/ubuntu/*/*/*.csv
    plotEnergyDelay(profiles)
