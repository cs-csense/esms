#!/usr/bin/python

from collections import defaultdict
import sys, os, getopt
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import math

tree = lambda: defaultdict(tree);

_PARAMS = {'backend': 'ps',
        'font.size': 18,
        'axes.labelsize': 18,
        'text.fontsize': 18,
        'legend.fontsize': 16,
        'xtick.labelsize': 18,
        'ytick.labelsize': 14,
        'pdf.fonttype':42,
        'text.usetex': False,
        'ps.useafm' : True,
#       remove to enforce embedding
#        'pdf.use14corefonts' : True    
        };

def mkdirs(path):
    try: 
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise

def params(args):
    if(args is not None):
        for key in args:
            if(key in _PARAMS): 
                _PARAMS[key] = args[key];
    return params;

def scatter(x, y, xlabel, ylabel, legends, filename, annotations=[], loc='upper center'):
    mkdirs(os.path.dirname(filename));
    CS = np.random.rand(len(legends))
    CS = ['r',  'g', 'b', 'c', 'm']
    MARKER = ['o', 'x', '*', '+', 'd', 'v']
    plt.rcParams.update(_PARAMS);
    scatters = []
    fig, ax = plt.subplots()
    for g in range(len(x)):
        A = np.pi * 4**2
        scatters.append(ax.scatter(x[g], y[g], s=A, c=CS[g], marker=MARKER[g], alpha=0.5))
    #for g in range(len(annotations)):
    #    for i in range(len(annotations[g])):
    #        ax.annotate(annotations[g][i], xy=(x[g][i], y[g][i]), xytext=(x[g][i], y[g][i]))
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend(scatters, legends, loc=loc, fontsize=10, frameon=False);
    plt.tight_layout()
    plt.savefig(filename);
    plt.close()

def barchart(dataset, xticks, legends, ylabel, filename, baseline=False, gap=50, loc='upper right'):
    total = sorted(sum(dataset, []));
    ymin = total[0];
    ymax = total[-1];
    broken = ymax - ymin > 500;
    if(broken):
        idx = len(total) / 2 - 1;
        ymedian = total[idx];
        yoffset = total[idx + 1] - total[idx];
        while(ymedian <= 0 or yoffset < gap):
            idx = idx + 1;
            if(idx >= len(total)): 
                broken = False;
                break;
            ymedian = total[idx];
            yoffset = total[idx + 1] - total[idx];

    mkdirs(os.path.dirname(filename));
    plt.rcParams.update(_PARAMS);
    axis = [];
    if(broken):
        fig, (top, bot) = plt.subplots(2, 1, sharex=True);
        axis = [bot, top];
    else:
        fig, ax = plt.subplots();
        axis = [ax];

    width = 1.0 / (len(dataset) + 1);
    indexes = np.arange(len(xticks));
    bars = [];
    for ax in axis:
        colors = ['r', 'g', 'b'];
        if(len(dataset) > len(colors)):
            colors = iter(cm.rainbow(np.linspace(0, 1, len(dataset))));
        else:
            colors = iter(colors);
        for i,data in enumerate(dataset):
            bar = ax.bar(indexes + i * width, data, width, color=next(colors));
            bars.append(bar);

    if(broken):
        if(ymin > 0): ymin = 0;
        bot.set_ylim(ymin, ymedian);
        bot.xaxis.tick_bottom();
        bot.spines['top'].set_visible(False);
        top.set_ylim(ymedian + yoffset * 7 / 10, ymax + 10);
        top.xaxis.tick_top();
        top.tick_params(labeltop='off');
        top.spines['bottom'].set_visible(False);
        # Diagonal lines in axes coordinates
        d = .005 
        kwargs = dict(transform=bot.transAxes, color='k', clip_on=False)
        bot.plot((-d,+d),(1-d,1+d), **kwargs)   # bottom-left diagonal
        bot.plot((1-d,1+d),(1-d,1+d), **kwargs) # bottom-right diagonal
        kwargs.update(transform=top.transAxes)  # switch to the bottom axes
        top.plot((-d,+d),(-d,+d), **kwargs)      # top-left diagonal
        top.plot((1-d,1+d),(-d,+d), **kwargs)    # top-right diagonal
        top.legend(tuple(bar[0] for bar in bars), legends, loc=loc);
        fig.text(0.018, 0.65, ylabel, ha='center', va='center', rotation='vertical')
        bot.yaxis.set_major_formatter(plt.FormatStrFormatter('%d'));
    else:
        plt.ylabel(ylabel);
        plt.ylim(0, math.ceil(ymax));
        plt.legend(tuple(bar[0] for bar in bars), legends, loc=loc);

    if(baseline):
        axis[0].plot((0, len(xticks)), (1, 1), 'k--');
        axis[0].legend(tuple(bar[0] for bar in bars), legends, loc=loc);
    else:
        axis[0].plot((0, len(xticks)), (0, 0), 'k');

    offset = 0.5;
    for data in dataset[1:]:
        if(data[-1] > 0): offset = offset + 1;

    plt.xticks(indexes + offset * width, xticks, rotation='vertical');
    plt.xlim(0, len(xticks)-2*width);
    plt.tight_layout();
    plt.savefig(filename);
