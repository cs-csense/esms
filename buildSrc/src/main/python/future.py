#!/usr/bin/python

from plot import *

x = [ [30, 56, 70, 100 ], [10, 15, 23, 50], [5, 7, 12, 14] ]
y = [ [20, 28, 31, 47], [17, 26, 32, 42], [30, 56, 70, 25 ] ]
scatter(x, y, "Latency (ms)", "Power (mW)", ["OPT1", "OPT2", "OPT4"], "fig/future.pdf", loc="upper right")

