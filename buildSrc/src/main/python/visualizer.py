#!/usr/bin/python

from os.path import expanduser
from collections import defaultdict
from subprocess import call
from glob import glob
from plot import params, barchart
import sys, os, getopt

tree = lambda: defaultdict(tree);
dataset = tree();

# Toro 
# Code, Data, Speedup



# Nexus 10
# RSS, Speedup

def loadDataset(path):
#real, user, sys, util, rss, text, data, stack, major, minor, swaps, valuntary, involuntary, writes, allocation, source, program, __TEXT, __DATA
#35, 34.99, 0, 99.97142857142858, 561152, 0, 0, 0, 0, 155, 0, 1, 177, 576, 572, 10403, 8552, 4096, 4096
    csvs = glob(path);
    dataset = tree();
    for csv in csvs:
        parts = os.path.basename(csv).split('.')[0].split('_');
        benchmark = parts[0];
        tk = parts[1];
        if(tk == 'StreamIt'):
            cfg = tk;
            if(len(parts) == 3):
                cfg = 'CacheOpt'
        else:
            cfg = parts[2];
        with open(csv) as csv:
            fields = csv.readlines()[1].strip().split(', ');
            dataset[benchmark][tk][cfg]['user'] = float(fields[1]);
            dataset[benchmark][tk][cfg]['sys'] = float(fields[2]);
            dataset[benchmark][tk][cfg]['rss'] = int(fields[4]);
            dataset[benchmark][tk][cfg]['__TEXT'] = int(fields[-2]);
            dataset[benchmark][tk][cfg]['__DATA'] = int(fields[-1]);
            #if(cfg == 'CacheOpt'):
            #    print(benchmark, tk, cfg, dataset[benchmark][tk][cfg]);
    return dataset;

MSB = ["AutoCor", "BitonicSort", "FIRcoarse", "FIR", "FFT2", "FFT3", "FMRadio", "MatrixMult", "MatrixMultBlock", "MergeSort", "Repeater", "BeepBeep", "MFCC", "Crowd"];
TK = ['ESMS', 'StreamIt'];
CFG = ['AA', 'AoC', 'IP', 'StreamIt', 'CacheOpt'];

def plotSpeedup(dataset, filename, legendsize=16):
    AA = [dataset[msb]['ESMS']['AA']['user'] + dataset[msb]['ESMS']['AA']['sys'] for msb in MSB];
    AoC = [dataset[msb]['ESMS']['AoC']['user'] + dataset[msb]['ESMS']['AoC']['sys'] for msb in MSB];
    IP = [dataset[msb]['ESMS']['IP']['user'] + dataset[msb]['ESMS']['IP']['sys'] for msb in MSB];
    StreamIt = [dataset[msb]['StreamIt']['StreamIt']['user'] + dataset[msb]['StreamIt']['StreamIt']['sys'] for msb in MSB];
    CacheOpt = [];
    for msb in MSB:
        if 'CacheOpt' in dataset[msb]['StreamIt']: 
            user = dataset[msb]['StreamIt']['CacheOpt']['user'];
            CacheOpt.append(user + dataset[msb]['StreamIt']['CacheOpt']['sys']);
        else: CacheOpt.append(0);
    for i in range(len(StreamIt)):
        AA[i] = StreamIt[i] / AA[i];
        AoC[i] = StreamIt[i] / AoC[i];
        IP[i] = StreamIt[i] / IP[i];
        if(CacheOpt[i] > 0): CacheOpt[i] = StreamIt[i] / CacheOpt[i];
    params({ 'legend.fontsize':legendsize });
    barchart([AA, AoC, IP, CacheOpt], MSB, ['AA', 'AoC', 'IP', 'CacheOpt'], 'Speedup', filename, True);

def plotSize(dataset, field, ylabel, filename, unit=1024.0, gap=0, loc='upper right', legendsize=16):
    AA = [dataset[msb]['ESMS']['AA'][field]/unit for msb in MSB];
    AoC = [dataset[msb]['ESMS']['AoC'][field]/unit for msb in MSB];
    IP = [dataset[msb]['ESMS']['IP'][field]/unit for msb in MSB];
    StreamIt = [dataset[msb]['StreamIt']['StreamIt'][field]/unit for msb in MSB];
    CacheOpt = [dataset[msb]['StreamIt']['CacheOpt'][field]/unit if 'CacheOpt' in dataset[msb]['StreamIt'] else 0 for msb in MSB];
    params({ 'legend.fontsize':legendsize });
    barchart([AA, AoC, IP, StreamIt, CacheOpt], MSB, ['AA', 'AoC', 'IP', 'StreamIt', 'CacheOpt'], ylabel, filename, False, gap, loc);

def plotSizeReduction(dataset, field, ylabel, filename, unit=1024.0, gap=0, loc='upper right', legendsize=16):
    AA = [dataset[msb]['ESMS']['AA'][field] for msb in MSB];
    AoC = [dataset[msb]['ESMS']['AoC'][field] for msb in MSB];
    IP = [dataset[msb]['ESMS']['IP'][field] for msb in MSB];
    StreamIt = [dataset[msb]['StreamIt']['StreamIt'][field] for msb in MSB];
    CacheOpt = [dataset[msb]['StreamIt']['CacheOpt'][field] if 'CacheOpt' in dataset[msb]['StreamIt'] else 0 for msb in MSB];
    for i in range(len(StreamIt)):
        AA[i] = (AA[i] - StreamIt[i]) / unit;
        AoC[i] = (AoC[i] - StreamIt[i]) / unit;
        IP[i] = (IP[i] - StreamIt[i]) / unit;
        if(CacheOpt[i] > 0): CacheOpt[i] = (CacheOpt[i] - StreamIt[i]) / unit;
    params({ 'legend.fontsize':legendsize });
    barchart([AA, AoC, IP, CacheOpt], MSB, ['AA', 'AoC', 'IP', 'CacheOpt'], ylabel, filename, False, gap, loc);

def plotToro(dataset):
    plotSpeedup(dataset, 'figures/esms-speedup-toro.pdf', 12);
    plotSize(dataset, '__TEXT', 'Code Size (KB)', 'figures/esms-code-toro.pdf', 1024, 30, 'upper left', 10);
    plotSize(dataset, '__DATA', 'Data Size (KB)', 'figures/esms-data-toro.pdf', 1024, 40, 'upper right', 12);

def plotNexus10(dataset):
    plotSpeedup(dataset, 'figures/esms-speedup-nexus10.pdf', 12);
    plotSizeReduction(dataset, 'rss', 'RSS (KB)', 'figures/esms-rss-nexus10.pdf', 1024, 300, 'upper right', 14);

if __name__ == '__main__':
    toroCSV = 'data/ToroCSV/performanceCSV/*.csv';
    nexus10CSV = 'data/android/performanceCSV/*.csv';
    dataset = loadDataset(toroCSV);
    plotToro(dataset);
    dataset = loadDataset(nexus10CSV);
    plotNexus10(dataset);
