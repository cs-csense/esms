package esms.toolkits

interface StreamItCfg {
    def boolean Y = true
    def boolean N = false
    def CLUSTERS = [1,2,4,8]

    def OPTIONS = [
            ['O0', '--unroll 0'],
            ['dp', '--partition_dp'],
            ['greedy', '--partition_greedy'],
            ['greedier', '--partition_greedier'],
            ['cacheopt', '--cacheopt'],

            ['O2', '--partition_dp --unroll 256 --destroyfieldarray'],
            ['greedy.O2', '--partition_greedy --unroll 256 --destroyfieldarray'],
            ['greedier.O2', '--partition_greedier --unroll 256 --destroyfieldarray'],
            ['cacheopt.O2', '--cacheopt --unroll 256'],

            ['ss', '--statespace'],
            ['lr', '--linearreplacement'],
            ['lr.ss', '--linearreplacement --statespace'],
            ['lp', '--linearpartition'],

            ['ss.O2', '--statespace --partition_dp --unroll 256 --destroyfieldarray'],
            ['lr.O2', '--linearreplacement --partition_dp --unroll 256 --destroyfieldarray'],
            ['lr.ss.O2', '--linearreplacement --statespace --partition_dp --unroll 256 --destroyfieldarray'],
            ['lp.O2', '--linearparti1tion --partition_dp --unroll 256 --destroyfieldarray'],
    ]

    // -O2 for 10s on ODROID
    def ITERATIONS = [
            AutoCor: 6200000,
            BeepBeep: 1200000,
            BitonicSort: 3250000,
            BitonicSortRecursive: 72000000,
            FilterBankNew: 4500000,
            FFT2: 8000000,
            FIR: 8200000,
            FIRcoarse: 12000000,
            FMRadio: 6500000,
            MatrixMult: 3000000,
            MatrixMultBlock: 6000000,
            MergeSort: 150000000,
            Repeater: 4500000,
    ]

    def MSB = [
            AutoCor:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, N,],
                    2:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, N,],
                    4:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, N,],
                    8:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, N,],
            ],
            BeepBeep:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, N,     Y, Y, Y, N,],
                    2:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, N,     Y, Y, Y, N,],
                    4:[Y, N, Y, Y, Y,     N, Y, Y, N,       Y, Y, Y, N,     N, N, Y, N,],
                    8:[Y, N, Y, Y, Y,     N, Y, Y, N,       Y, Y, Y, N,     N, N, N, N,],
            ],
            BitonicSort:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, N,],
                    2:[Y, N, Y, Y, Y,     N, Y, Y, N,       Y, Y, Y, Y,     N, N, N, N,],
                    4:[Y, N, Y, Y, Y,     N, Y, Y, N,       Y, Y, Y, Y,     N, N, N, N,],
                    8:[Y, N, Y, Y, Y,     N, Y, Y, N,       Y, Y, Y, Y,     N, N, N, N,],
            ],
            BitonicSortRecursive:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, N,     Y, Y, Y, N,],
                    2:[Y, N, Y, Y, Y,     N, Y, Y, N,       Y, Y, Y, N,     N, N, N, N,],
                    4:[Y, N, Y, Y, Y,     N, Y, Y, N,       Y, Y, Y, N,     N, N, N, N,],
                    8:[Y, N, Y, Y, Y,     N, Y, Y, N,       Y, Y, Y, N,     N, N, N, N,],
            ],
            FFT2:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    2:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    4:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    8:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
            ],
            FilterBankNew:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    2:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    4:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    8:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
            ],
            FIR:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    2:[Y, N, Y, Y, Y,     N, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    4:[Y, N, Y, Y, Y,     N, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    8:[Y, N, Y, Y, Y,     N, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
            ],
            FIRcoarse:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    2:[Y, N, Y, Y, Y,     N, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    4:[Y, N, Y, Y, Y,     N, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    8:[Y, N, Y, Y, Y,     N, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
            ],
            FMRadio:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    2:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    4:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    8:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
            ],
            MatrixMult:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    2:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    4:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    8:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
            ],
            MatrixMultBlock:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    2:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    4:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    8:[Y, Y, Y, Y, Y,     Y, Y, Y, Y,       Y, Y, Y, Y,     Y, Y, Y, Y,],
            ],
            MergeSort:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    2:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    4:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    8:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
            ],
            Repeater:[
                    1:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    2:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    4:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
                    8:[Y, Y, Y, Y, Y,     Y, Y, Y, N,       Y, Y, Y, Y,     Y, Y, Y, Y,],
            ],
    ]
}