package esms.toolkits

import groovy.transform.Canonical
import esms.util.Shell

import static esms.toolkits.OdroidCfg.*
import static esms.toolkits.StreamItCfg.*

@Canonical
class StreamItToolkit {
    def benchmarks
    def clusters
    def options

    def srcDir        // :StreamIt/src
    def buildDir      // :StreamIt/build
    def platform
    def include
    def lib

    StreamItToolkit(benchmarks=MSB.keySet(), clusters=CLUSTERS, options=[0..<OPTIONS.size()]) {
        this.benchmarks = benchmarks
        this.clusters = clusters
        this.options = options
    }

    def File wd(msb, cfg) {
        return new File("$buildDir/$msb/$cfg")
    }

    def sources(msb) {
        return "$srcDir/$msb/*.str"
    }

    def target(msb, cfg, suffix) {
        return "${wd(msb, cfg)}/$msb.$suffix"
    }

    def tojava(msb, cfg) {
        // java -ea -Xmx1700M  streamit.frontend.ToJava --output FMRadio.java   FMRadio.str
        def cmd = "java -ea -Xmx1700M streamit.frontend.ToJava"
        cmd += " --output ${target(msb, cfg, 'java')} ${sources(msb)}"
        if(Shell.exec(cmd, wd(msb, cfg)))
            throw new RuntimeException("[tojava] Failed to run $cmd");
    }

    def kjc(msb, options, cfg) {
        // java -ea -Xmx1700M  at.dms.kjc.Main --streamit --standalone --cluster 1 --havefftw FMRadio.java
        def cmd = "java -ea -Xmx1700M at.dms.kjc.Main"
        cmd += " $options ${target(msb, cfg, 'java')}"
        if(Shell.exec(cmd, wd(msb, cfg)))
            throw new RuntimeException("[kjc] Failed to run $cmd");
    }

    def make(msb, c, cfg) {
        def wd = wd(msb, cfg);
        def cmd = c==1 \
                    ? "$srcDir/../../main/misc/concat_cluster_threads_cpp.pl fusion.cpp thread*.cpp"
                    : "$srcDir/../../main/misc/concat_cluster_threads_cpp.pl master.cpp global.cpp thread*.cpp"
        if(Shell.exec(cmd, wd))
            throw new RuntimeException("[make] Failed to run $cmd");
        if(Shell.exec(cmd = 'grep -v -e printf combined_threads.cpp > tmp', wd))
            throw new RuntimeException("[make] Failed to run $cmd");
        if(Shell.exec(cmd = 'mv tmp combined_threads.cpp', wd))
            throw new RuntimeException("[make] Failed to run $cmd");

        def CXX = 'c++';
        def CXXFLAGS = "-O3 -I$include"
        def LDFLAGS = '-pthread -lm -lstdc++'
        switch(platform) {
            case 'ubuntu':
                CXX = 'arm-linux-gnueabihf-g++'
                CXXFLAGS += ' -DPTHREAD_STACK_MIN=16384'
                break;
        }
        if(Shell.exec(cmd = "$CXX $CXXFLAGS -o streamit combined_threads.cpp $lib $LDFLAGS", wd))
            throw new RuntimeException("[make] Failed to run $cmd");
    }

    def doBuild(msb, c, options, cfg) {
        wd(msb, cfg).mkdirs();
        try {
            tojava(msb, cfg);
            kjc(msb, options, cfg);
            make(msb, c, cfg);
            println "[PASS][$msb][$cfg] $options"
        } catch(RuntimeException e) {
            println "[FAIL][$msb][$cfg]${e.getMessage()}"
        }
    }

    def build(spec, srcDir, buildDir) {
        this.srcDir = "$srcDir/test/streamit"
        this.buildDir = "$buildDir"
        this.include = "$srcDir/cluster/headers"
        this.lib = spec.staticLibraryFile
        this.platform = spec.targetPlatform.name
        benchmarks.each {
            def msb = it;
            clusters.each {
                def c = it;
                options.each {
                    if(MSB[msb][c][it]) {
                        def opts = c == 1 \
                                    ? "${OPTIONS[it][1]} --streamit --standalone --cluster 1 -i ${ITERATIONS[msb]}"
                                    : "${OPTIONS[it][1]} --streamit --cluster $c -i ${ITERATIONS[msb]}"
                        def cfg = OPTIONS[it][0];
                        doBuild(msb, c, opts, "options-$c-$cfg");
                    }
                }
            }
        }
    }

    def benchmark(Closure run) {
        execByFreq(run)
    }

    def upload(Closure run) {
        execByOptions(run)
    }

    def download(Closure run) {
        execByOptions(run)
    }

    /**
     *
     * @param run { msb, c, option -> ... }
     * @return
     */
    def execByOptions(Closure run) {
        benchmarks.each {
            def msb = it;
            clusters.each {
                def c = it;
                options.each {
                    if(MSB[msb][c][it])
                        run(msb, c, OPTIONS[it]);
                }
            }
        }
    }

    /**
     *
     * @param run { b, option, a7, a7f, a15, a15f -> ... }
     * @return
     */
    def execByFreq(Closure run) {
        benchmarks[0..-1].each {
            def msb = it;
            clusters.each {
                def c = it;
                options.each {
                    def option = OPTIONS[it]
                    if(!MSB[msb][c][it]) return
                    switch(c) {
                        case 1:
                            A7F.each {
                                run(msb, option, 1, it, 0, 0);
                            }
                            A15F.each {
                                run(msb, option, 0, 0, 1, it);
                            }
                            break;
                        case 2:
                            A7F.each {
                                run(msb, option, 2, it, 0, 0);
                            }
                            A15F.each {
                                run(msb, option, 0, 0, 2, it);
                            }
//                            A7F.each {
//                                def a7f = it;
//                                A15F.each {
//                                    run(msb, option, 1, a7f, 1, it);
//                                }
//                            }
                            break;
                        case 4:
                            A7F.each {
                                run(msb, option, 4, it, 0, 0);
                            }
                            A15F.each {
                                run(msb, option, 0, 0, 4, it);
                            }
//                            A7F.each {
//                                def a7f = it;
//                                A15F.each {
//                                    run(msb, option, 2, a7f, 2, it);
//                                }
//                            }
                            break;
                        case 8:
                            A7F.each {
                                def a7f = it;
                                A15F.each {
                                    run(msb, option, 4, a7f, 4, it);
                                }
                            }
                            break;
                    }
                }
            }
        }
    }
}
