package esms.toolkits

interface OdroidCfg {
    def A7F = [800000, 1000000, 1200000, 1400000]
    def A15F = [1200000, 1400000, 1600000, 1800000, 2000000]
    def A7 = [0, 1, 2, 3]
    def A15 = [4, 5, 6, 7]
    def CORES = [0, 1, 2, 3, 4, 5, 6, 7]
}