package esms.util;

class Shell {
    private static String[] addShellPrefix(String command) {
        def commandArray = new String[3]
        commandArray[0] = "sh"
        commandArray[1] = "-c"
        commandArray[2] = command
        return commandArray
    }

    static int exec(String command, boolean verbose=false) {
        return exec(command, new File(System.properties.'user.dir', verbose));
    }

    static int exec(String command, File wd, boolean verbose=false) {
        if(verbose) println command;
        def process = new ProcessBuilder(addShellPrefix('source ~/.bash_profile; ' + command))
                .directory(wd)
                .redirectErrorStream(true)
                .start()
        if(verbose)
            process.inputStream.eachLine {println it}
        else
            process.consumeProcessOutput();
        return process.waitFor();
    }

    static int exec(String command, int timeout) {
        return executeOnShell(command, new File(System.properties.'user.dir'), timeout);
    }

    static int exec(String command, File wd, int timeout) {
        def process = new ProcessBuilder(addShellPrefix('source ~/.bash_profile; ' + command))
                .directory(wd)
                .redirectErrorStream(true)
                .redirectOutput(file('/dev/null'))
                .start()
        process.waitForOrKill(timeout);
        return process.waitFor();
    }
}