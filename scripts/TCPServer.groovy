import java.nio.ByteBuffer

def port = args ? args[0].toInteger() : 8888
def server = new ServerSocket(port)

while(true) {
    def socket = server.accept()
	new Thread() {
	    @Override
	    public void run() {
		println "========== Connection from $socket =========="
		socket.withStreams { input, output ->
		    int total = 0
		    int count
		    if(true) {
			byte[] bytes = new byte[4096]
			while((count = input.read(bytes)) > 0) {
			    total += count
			    println("read: $count bytes, ${count / 4} numbers")
			}
		    } else {
			int offset = 0
			byte[] bytes = new byte[4 * 1]
			ByteBuffer buffer = ByteBuffer.wrap(bytes)
			while((count = input.read(bytes, offset, bytes.length - offset)) > 0) {
			    offset += count
			    if(offset == bytes.length) {
				printf("%d\n", buffer.getInt());
	//                    printf("%f\t%f\t%f\t%f\t%f\n", buffer.getFloat(), buffer.getFloat(), buffer.getFloat(), buffer.getFloat(), buffer.getFloat())
	//                    printf("%f\t%f\t%f\t%f\t%f\n", buffer.getFloat(), buffer.getFloat(), buffer.getFloat(), buffer.getFloat(), buffer.getFloat())
	//                    printf("%f\t%f\t%f\t%f\n", buffer.getFloat(), buffer.getFloat(), buffer.getFloat(), buffer.getFloat())
	//                    println('')
				buffer.clear()
				offset = 0
				total += bytes.length
			    }
			}
		    }
		    println("total: $total bytes, ${total / 4} numbers")
		}
		println "========== Connection Closed =========="
	    }
	}.start();
}

