
def host = 'localhost'
def port = 8888
def client = new Socket(host, port);
client.withStreams { input, output ->
    def msg
    while(msg = System.console().readLine())
    output << msg << '\n'
    output.close()
}