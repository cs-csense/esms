#!/bin/sh

## Setup Check List
# 1. 2 CPU cores online w/ performance governor at the highest freq
# 2. WiFi/LTE
# 3. Time sync
# Useful apps: CPU Adjuster, Snoozy Charger, SuperSU

BAR="==============="
if [ -z $1 ]
then
    echo "Must specify cfg [_1KB | _256KB | _1M | 1KB | 256KB | 1M]"
    exit 1
fi
CFG=Batching$1
TS=`date +%G%m%d-%H%M%S`
echo "$BAR Instrumenting $CFG $BAR"

set -e
set -o xtrace

# Installation
if [ "$1" == "-i" ]; then
    ./gradlew -q InstallBatching_1KBDebug InstallBatching_1KBDebugAndroidTest
    ./gradlew -q InstallBatching_256KBDebug InstallBatching_256KBDebugAndroidTest
    ./gradlew -q InstallBatching_1MDebug InstallBatching_1MDebugAndroidTest
    ./gradlew -q InstallBatching1KBDebug InstallBatching1KBDebugAndroidTest
    ./gradlew -q InstallBatching256KBDebug InstallBatching256KBDebugAndroidTest
    ./gradlew -q InstallBatching1MDebug InstallBatching1MDebugAndroidTest
fi

# Start Instrumentation
BATCHING=$HOME/Dropbox/Batching
CLASS=esms.msa2.ApplicationTest
METHOD=testBatching
PKG=esms.msa2.$CFG.test
RUNNER=android.support.test.runner.AndroidJUnitRunner
mkdir -p $BATCHING/$CFG-$TS
rm -fr $BATCHING/ptrace*
adb shell rm -fr /sdcard/Download/MSA2*
adb shell rm -fr /sdcard/Download/ftrace*
adb shell su -C date -s `date +%G%m%d.%H%M%S`
adb shell "am instrument -r -e class $CLASS#$METHOD $PKG/$RUNNER"

# Start Profiling
./gradlew -q startProfiling

case "$1" in
    "_1KB")
        sleep $(( 10 + 5 * (4-1) + ( 15 + 35 + 45 + 60 ) + 60 )) # baseline {0}, {8}, {16}, {24}} workers
;;
    "_256KB" | "_1M")
        sleep $(( 10 + 5 * (4-1) + ( 15 + 35 + 45 + 60 ) + 35 )) # baseline {0}, {8}, {16}, {24}} workers
;;
    "1KB")
        #sleep $(( 10 + 5 * (4-1) + ( 15 + 35 + 45 + 60 ) + 100 )) # {0}, {8}, {16}, {24}} workers WiFi@1728MHz
        sleep $(( 10 + 5 * (4-1) + ( 15 + 35 + 45 + 60 ) + 70 )) # {0}, {8}, {16}, {24}} workers
;;
    "256KB")
        #sleep $(( 10 + 5 * (4-1) + ( 15 + 35 + 45 + 60 ) + 5 )) # {0}, {8}, {16}, {24}} workers
        sleep $(( 10 + 5 * (4-1) + ( 15 + 35 + 45 + 60 ) + 45 )) # {0}, {8}, {16}, {24}} workers
;;
    "1M")
        #sleep $(( 10 + 5 * (4-1) + ( 15 + 35 + 45 + 60 ) + 5 )) # {0}, {8}, {16}, {24}} workers
        sleep $(( 10 + 5 * (4-1) + ( 15 + 35 + 45 + 60 ) + 45 )) # {0}, {8}, {16}, {24}} workers WiFi@1728MHz
;;
esac
./gradlew -q stopProfiling

# Collect Traces from Device and Monsoon PM
adb wait-for-device shell ls /sdcard/Download | grep MSA2 | tr -d '\r' | xargs -n1 -I % adb pull /sdcard/Download/% $BATCHING/$CFG-$TS/%
adb wait-for-device shell ls /sdcard/Download | grep ftrace | tr -d '\r' | xargs -n1 -I % adb pull /sdcard/Download/% $BATCHING/$CFG-$TS/%
CSV=$BATCHING/ptrace*

set +e
while [ ! -f $CSV ]; do
  echo waiting for $CSV...
  sleep 3
done

set -e
mv $CSV $BATCHING/$CFG-$TS

# Anaylze the Traces
#./gradlew report
