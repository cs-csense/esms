package esms.data.batching

import de.erichseifert.gral.data.DataSeries
import de.erichseifert.gral.data.DataSource
import de.erichseifert.gral.data.DataTable
import de.erichseifert.gral.data.statistics.Statistics
import de.erichseifert.gral.graphics.Insets2D
import de.erichseifert.gral.graphics.Label
import de.erichseifert.gral.graphics.Orientation
import de.erichseifert.gral.io.plots.DrawableWriter
import de.erichseifert.gral.io.plots.DrawableWriterFactory
import de.erichseifert.gral.plots.BoxPlot
import de.erichseifert.gral.plots.Plot
import de.erichseifert.gral.plots.XYPlot
import de.erichseifert.gral.plots.colors.LinearGradient
import de.erichseifert.gral.plots.colors.ScaledContinuousColorMapper
import de.erichseifert.gral.plots.lines.DefaultLineRenderer2D
import de.erichseifert.gral.util.DataUtils
import de.erichseifert.gral.util.GraphicsUtils
import esms.api.Loggable
import esms.util.Log
import groovy.io.FileType

import java.awt.*

/**
 * Throughput vs #Threads: pkt/s
 * Energy vs #Threads: uAh
 * Inter-packet delay vs #Threads: ms
 * Workload: worker0, worker4, worker8, worker12, worker16
 * Buffer size: _1KB, _256KB, _1MB, 1KB, [8K, 128K], 256K, [512KB,] 1MB
 */
public class EnergyAnalyzer extends Loggable {
    //private static final int LEADING = 7
    //private static final double INTERVAL = 1.0 / 5000 / 3600
    public static void main(String[] args) {
        File path
        for(int i = 0; i  < args.length; i++) {
            if(args[i] == "-p") path = new File(args[++i])
        }
        if(path == null) return
        def traces = []
        path.listFiles(new FileFilter() {
            @Override
            boolean accept(File pathname) {
                return pathname.isDirectory() && pathname.getName().startsWith('MSA2-')
            }
        }).each {
            EnergyAnalyzer analyzer = new EnergyAnalyzer(it).analyze()
            analyzer.export()
            //analyzer.show()
            traces << "$it/trace.eps"

        }
        path.eachFile(FileType.DIRECTORIES) {
        }
        "open ${traces.join(' ')}".execute().waitFor()
    }

    private static void formatLine(XYPlot plot, DataSeries series, Color color) {
        //plot.setPointRenderers(series, null);
        DefaultLineRenderer2D line = new DefaultLineRenderer2D();
        line.setColor(color);
        line.setGap(1.0)
        plot.setLineRenderers(series, line);
    }

    private File _path
/*
    private DataTable _data
    private DataSeries _seriesV
    private DataSeries _seriesI
    private DataSeries _seriesP
    private Plot _plot
    private def _uAh
    private def _mWh
*/

    static float INTERVAL_H = 1f / 5000 / 3600
    static def DFMT = 'yyyyMMdd-hhmmss'
    static def BUF_SIZES = [] // ['_1KB', '_256KB', '_1M', '1KB', '256KB', '1M']
    static def NS = [] // [0, 8, 16, 24]
    def _records = []

    // exp = [ bufSize, N, timestamp, [start, stop], [pkt intervals], [mAs], [Vs], mAh ]
    // record = [ bufSize, N, timestamp, throughput, pkt_mean, mA_mean, V_mean, mAh ]
    // _records = [record, record, ...]
    // _records.groupBy { record -> record[bufSize] }
    /**
     * Throughput(pkt/s) vs. #Threads
     * Energy(mAh) vs. #Threads
     * Inter-Packet Delay vs. #Threads
     *
     * @param path
     */
    public EnergyAnalyzer(File path) {
        _path = path
    }

    public EnergyAnalyzer(String path) {
        this(new File(path));
    }

    static export(Plot plot, String filename) {
        try {
            DrawableWriter writer = DrawableWriterFactory.getInstance().get("application/postscript");
            writer.write(plot, new FileOutputStream(filename), 1024, 768);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static Plot plotXY(String title, String xLabel, String yLabel, DataTable dataset, cols = [], legends = [], spacing = [:]) {
        if(cols.isEmpty()) return
        def xCol = cols[0]
        def yCols = cols[1..-1]
        double max = Double.MIN_VALUE, min = Double.MAX_VALUE
        def series = []
        yCols.eachWithIndex { yCol, int i ->
            series << new DataSeries(legends[i] ?: yLabel, dataset, xCol, yCol)
            def col = series[-1].getColumn(series[-1].getColumnCount() - 1)
            double mx = col.getStatistics(Statistics.MAX)
            double mn = col.getStatistics(Statistics.MIN)
            max = mx > max ? mx : max;
            min = mn < min ? mn : min;
        }
        XYPlot plot = new XYPlot(*series)
        XYPlot.XYPlotArea2D plotArea = plot.getPlotArea()
        plotArea.setBorderColor(null)
        plotArea.setMajorGridX(false)
        plotArea.setMajorGridY(false)
        plotArea.setClippingArea(null)
        def colors = [Color.BLUE, Color.ORANGE, Color.lightGray, Color.RED, Color.GREEN, Color.CYAN, Color.MAGENTA]
        series.eachWithIndex { s, i ->
            formatLine(plot, s, colors[i])
        }

        plot.getTitle().setText(title);
        plot.setLegendVisible(!legends.isEmpty())
        if(!legends.isEmpty()) {
            def top = 0, middle = 0
            double mean = (max + min) / 2
            double q1 = (max + mean) / 2
            double q3 = (min + mean) / 2
            series.each { DataSeries it ->
                double v = it.get(it.getColumnCount() - 1, it.getRowCount() - 1)
                v > mean ? top-- : top++
                v > q1 || v < q3 ? middle++ : middle--
            }
            Log.d('plotXY', "max=$max, q1=$q1, mean=$mean, q3=$q3, min=$min, top=$top, middle=$middle")
            plot.getLegend().setOrientation(Orientation.VERTICAL)
            plot.getLegend().setAlignmentX(1.0)
            plot.getLegend().setAlignmentY(top.abs() >= middle ? (top > 0 ? 0 : 1) : 0.5)
            plot.setLegendDistance(0.5)
        }
        plot.setInsets(new Insets2D.Double(20.0, 60.0, 60.0, 60.0)) // top, left, bottom, right
        //plot.setAxis(XYPlot.AXIS_Y2, new Axis(0, 10))
        //plot.setAxisRenderer(XYPlot.AXIS_Y2, new LinearRenderer2D())
        //plot.setMapping(_seriesV, XYPlot.AXIS_X, XYPlot.AXIS_Y2)
        Label labelX = new Label(xLabel)
        Label labelY = new Label(yLabel)
        labelY.setRotation(90)
        plot.getAxisRenderer(XYPlot.AXIS_X).setLabel(labelX)
        plot.getAxisRenderer(XYPlot.AXIS_Y).setLabel(labelY)
        def axis  = [XYPlot.AXIS_X, XYPlot.AXIS_Y]
        def stats = cols.collect { dataset.getColumn(it) }
        axis.eachWithIndex { it, i ->
            int s = spacing[it]
            if(!s) return
            int N = stats[i].getStatistics(Statistics.N)
            max = i == 0 ? stats[i].getStatistics(Statistics.MAX) : stats[1..-1].collect { it.getStatistics(Statistics.MAX) }.max()
            plot.getAxis(it).setRange(0, max + 1)
            plot.getAxisRenderer(it).setTickSpacing(s)
        }
        //plot.getAxisRenderer(XYPlot.AXIS_X).setIntersection(0)//-Double.MAX_VALUE)
        //plot.getAxisRenderer(XYPlot.AXIS_Y).setIntersection(0)//-Double.MAX_VALUE)
        return plot
    }

    static Plot plotBox(String title, String xLabel, String yLabel, DataTable dataset, Map ticks = [:], Number ySpacing = 0) {
//        dataset.getColumnCount().times {
//            Column col = dataset.getColumn(it)
//            String name = col.getSource().getName()
//            int N = col.getStatistics(Statistics.N)
//            float min = col.getStatistics(Statistics.MIN)
//            float max = col.getStatistics(Statistics.MAX)
//            float mean = col.getStatistics(Statistics.MEAN)
//            float medium = col.getStatistics(Statistics.MEDIAN)
//            float q1 = col.getStatistics(Statistics.QUARTILE_1)
//            float q2 = col.getStatistics(Statistics.QUARTILE_2)
//            float q3 = col.getStatistics(Statistics.QUARTILE_3)
//            Log.d('plotBox', "Inter-pkt delay ${NS[it]}(${N})[$min, $medium, $q1, $q2, $q3, $mean, $max]")
//        }

        DataSource boxData = BoxPlot.createBoxData(dataset);
        Label labelX = new Label(xLabel)
        Label labelY = new Label(yLabel)
        labelY.setRotation(90)

        BoxPlot plot = new BoxPlot(boxData);
        plot.getTitle().setText(title);
        plot.setInsets(new Insets2D.Double(20.0, 60.0, 60.0, 60.0));
        plot.getPlotArea().setBorderColor(null)
        plot.getPlotArea().setClippingArea(null)
        ((XYPlot.XYPlotArea2D)plot.getPlotArea()).setMajorGridX(false)
        ((XYPlot.XYPlotArea2D)plot.getPlotArea()).setMajorGridY(false)
        //plotArea.setMajorGridX(false)
        //plotArea.setMajorGridY(false)
        plot.getAxisRenderer(BoxPlot.AXIS_X).setCustomTicks(ticks)
        plot.getAxisRenderer(BoxPlot.AXIS_X).setLabel(labelX)
        plot.getAxisRenderer(BoxPlot.AXIS_Y).setLabel(labelY)
        //plot.getAxisRenderer(BoxPlot.AXIS_Y).setLabelDistance(-5)
        //plot.getAxisRenderer(BoxPlot.AXIS_X).setTickSpacing(2.0)
        if(ySpacing) plot.getAxisRenderer(BoxPlot.AXIS_Y).setTickSpacing(ySpacing)
        //plot.getAxisRenderer(BoxPlot.AXIS_X).setIntersection(-Double.MAX_VALUE)
        //plot.getAxisRenderer(BoxPlot.AXIS_Y).setIntersection(-Double.MAX_VALUE)

        // Format boxes
        Color COLOR = new Color( 55, 170, 200);
        ScaledContinuousColorMapper colors = new LinearGradient(GraphicsUtils.deriveBrighter(COLOR), Color.WHITE);
        colors.setRange(1.0, 4.0);
        Stroke stroke = new BasicStroke(2f);
        BoxPlot.BoxWhiskerRenderer pointRenderer = (BoxPlot.BoxWhiskerRenderer) plot.getPointRenderers(boxData).get(0);
        pointRenderer.setWhiskerStroke(stroke);
        pointRenderer.setBoxBorderStroke(stroke);
        pointRenderer.setBoxBackground(colors);
        pointRenderer.setBoxBorderColor(COLOR);
        pointRenderer.setWhiskerColor(COLOR);
        pointRenderer.setCenterBarColor(COLOR);
        return plot
    }

    // XYPlot: Throughput by N
    // XYPlot: mAh by N
    // Boxplot: Inter-packet delay by N
    def save(String path = _path) {
        def recsByBufSize = _records.groupBy { it[1] }
        def throughputs = [:].withDefault { [] }
        def mAs = [:].withDefault { [] }
        def mAhs = [:].withDefault { [] }
        BUF_SIZES.each {
            def bufSize = it
            def intervals = [:].withDefault { [] }
            def exps = recsByBufSize[it].groupBy { it[2] }
            NS.each { N ->
                def throughput = 0, mA = 0, mAh = 0
                exps[N].each {
                    throughput += it[3]
                    intervals[N].addAll(it[4])
                    mA += it[5].sum() / it[5].size()
                    mAh += it[6]
                }
                int count = exps[N].size()
                if(count > 0) {
                    throughputs[bufSize] << throughput / count
                    mAs[bufSize] << mA / count
                    mAhs[bufSize] << mAh / count
                }
            }

            if(bufSize.startsWith('_')) return
            DataTable delays = new DataTable(Double.class, Double.class, Double.class, Double.class)
            //intervals[NS[0]].size().times() { int c ->
            int pkts = 0
            intervals.values().collect() { it.size() }.min().times() { int c ->
                delays.add(intervals[NS[0]][c] as double, intervals[NS[1]][c] as double, intervals[NS[2]][c] as double, intervals[NS[3]][c] as double)
            }

            // Plotter inter-packet delays
            def ticks = DataUtils.map(
                    [1.0, 2.0, 3.0, 4.0] as Double[],
                    ['0', '8', '16', '24'] as String[]
            )
            Plot delaysPlot = plotBox('Inter-Packet Delay by System Load', 'Concurrent CPU Workers', 'Delay(ms)', delays, ticks, 1000)
            export(delaysPlot, "$path/delay-${bufSize}.eps")
        }

        DataTable dataset = new DataTable(Integer.class, Float.class, Float.class, Float.class, Float.class, Float.class, Float.class, Float.class, Float.class, Float.class,
                                                         Float.class, Float.class, Float.class, Float.class, Float.class, Float.class, Float.class, Float.class, Float.class)
        NS.eachWithIndex { N, i ->
            def row = []
            row.addAll(BUF_SIZES.collect { throughputs[it][i] as float })
            row.addAll(BUF_SIZES.collect { mAs[it][i] as float })
            row.addAll(BUF_SIZES.collect { mAhs[it][i] as float })
            dataset.add(N, *row)
        }

        // Throughput by NS
        Plot throughputPlot = plotXY('Throughput by System Load', 'Concurrent CPU Workers', "packets/s", dataset, [0, *(1..6)], BUF_SIZES, [(XYPlot.AXIS_X):8, (XYPlot.AXIS_Y):200])
        export(throughputPlot, "$path/throughput.eps")

        // mA by N
        Plot mAPlot = plotXY('Current Drain by System Load', 'Concurrent CPU Workers', "mA", dataset, [0, *(7..12)], BUF_SIZES, [(XYPlot.AXIS_X):8, (XYPlot.AXIS_Y):150])
        export(mAPlot, "$path/energy-mA.eps")

        // mAh by N
        Plot mAhPlot = plotXY('Energy Consumption by System Load', 'Concurrent CPU Workers', "mAh", dataset, [0, *(13..18)], BUF_SIZES, [(XYPlot.AXIS_X):8, (XYPlot.AXIS_Y):2])
        export(mAhPlot, "$path/energy-mAh.eps")

    }

    def analyze() {
        _path.listFiles(new FileFilter() {
            @Override
            boolean accept(File pathname) {
                return pathname.getName().startsWith('Batching')
            }
        }).each {
            analyzeOne(it)
        }
    }

    // exp = [ timestamp, bufSize, N, [start, stop], [pkt intervals], [mAs], mAh ]
    // record = [ timestamp, bufSize, N, throughput, [pkt intervals], [mAs], mAh ]
    def analyzeOne(File path) {
        // path: $HOME/Dropbox/Batching/BatchingCFG-yyyymmdd-hhmmss
        def parts = path.name.split('-')
        def bufferSize = parts[0]['Batching'.length()..-1]
        def timestamp = Date.parse(DFMT, parts[1..-1].join('-'))
        if(! (bufferSize in BUF_SIZES)) BUF_SIZES << bufferSize
        File ftrace, ptrace
        path.eachFile {
            if(it.name.startsWith('ptrace')) ptrace = it
            if(it.name.startsWith('ftrace')) ftrace = it
        }
        if(!ftrace || !ptrace) {
            Log.w(tag(), "Skipped $path.name for lack of ftrace or ptrace")
            return this
        }
        Log.d(tag(), "Analyzing $path...")

        def exp = [ timestamp, bufferSize ]
        def expWK = [:]
        def N, start, stop, intervals = [], xmitT = 0
        ftrace.eachLine {
            if(it.startsWith('#')) return
            def CPU = /\[(\d\d\d)\]/
            int cpu = it.find(CPU) { _, n -> n as int }
            def (task, leftover) = it.split(CPU)
            parts = task.split('-')
            task = parts[0..-2].join('-')
            int pid = parts[-1] as int
            def (ts, event, info) = leftover.trim().split('\\s+')[1,2,3..-1]
            ts = ts[0..-2] as double
            event = event[0..-2]
            if(info.startsWith('Batching')) {
                def action, date, time
                (N, action, date, time) = info.split('-')[1..-1]
                N = N as int
                if(!NS.contains(N)) NS << N
                switch(action) {
                    case 'start':
                        exp << (N as int)
                        start = Date.parse(DFMT, "$date-$time"); break
                    case 'stop':
                        stop = Date.parse(DFMT, "$date-$time")
                        exp << [start, stop]
                        exp << intervals
                        expWK[N as int] = exp
//                        int duration = (stop.getTime() - start.getTime()) / 1000f
//                        Log.d(tag(), "exp: [$bufferSize, $N, ${duration}s, " +
//                                        "${intervals.size()+1}pkts, ${(intervals.size()+1)/duration}pkts/s, ${intervals.sum()/intervals.size()}ms]")
                        // reset exp
                        exp = [timestamp, bufferSize]
                        start = stop = 0
                        intervals = []
                        xmitT = 0
                }
            } else if(start && event == 'net_dev_xmit') {
                if(xmitT) intervals << (ts - xmitT) * 1000
                xmitT = ts
            }
        }

        // ptrace
        // exp = [ timestamp, bufSize, N, [start, stop], [pkt intervals], [mAs], mAh ]
        // record = [ timestamp, bufSize, N, throughput, [pkt intervals], [mAs], mAh ]
        def Ni = 0
        def mAs = [], mAh = 0
        def ptime = Date.parse(DFMT, ptrace.name['ptrace-'.length()..-1])
        ptrace.eachLine { line, num ->
            N = NS[Ni]
            exp = expWK[N]
            if(N == null ||  num == 1) return
            start = -2 + exp[3][0].getTime() as long
            stop = 2 + exp[3][1].getTime() as long
            def (ts, mA) = line.split(',')[0..1].collect { it.toFloat() }
            ts = ptime.getTime() + (ts as float) * 1000
            if(ts >= start && ts <= stop) {
                if(!mAs.isEmpty())
                    mAh += (mA + mAs[-1]) / 2 * INTERVAL_H
                mAs << mA
            } else if(ts > stop) {
                start += 2
                stop -= 2
                def duration = (stop - start) / 1000f
                exp[3] = (exp[4].size() + 1) / duration
                exp << mAs << mAh
                _records << exp
                Log.d(tag(), "rec: [$bufferSize, $N, ${duration}s, " +
                        "${exp[3]}pkts/s, ${exp[4].size()+1}pkts, ${exp[4].isEmpty() ? 0 : exp[4].sum()/exp[4].size()}ms, " +
                        "${exp[5].sum()/exp[5].size()}mA, ${exp[6]}mAh]")
                // reset
                mAs = []
                mAh = 0
                Ni++
            }
        }
/*
        // MSA2-yyyymmdd-hhmmss
        def time = _path.name.split('-')[-1]
        def hh = time[0..1].toInteger()
        def mm = time[2..3].toInteger()
        def ss = time[4..5].toInteger()
        double beginningF = hh * 3600 + mm * 60 + ss + LEADING
        double beginningP = beginningF
        File ftrace = new File(_path, "ftrace.log")
        File ptrace = null
        _path.eachFile(FileType.FILES) {
            if(!it.name.startsWith("ptrace-")) return
            // ptrace-yyyymmdd-hhmmss.csv
            ptrace = it
            time = it.name.split('-')[-1][0..-5]
            hh = time[0..1].toInteger()
            mm = time[2..3].toInteger()
            ss = time[4..5].toInteger()
            beginningP = hh * 3600 + mm * 60 + ss;
        }
        if(!ftrace.exists()) throw new RuntimeException("ftrace required")
        if(ptrace == null) throw new RuntimeException("ptrace required")

        // Locate the duration of ftrace
        def start = 0
        def end = ""
        ftrace.eachLine {
            if(it.startsWith('#')) return
            if(start == 0) {
                start = it.trim().split('\\s+')[3][0..-2].toDouble()
            }
            end = it
            return
        }
        end = end.trim().split('\\s+')[3][0..-2].toDouble()
        def duration = end - start - 2 * LEADING

        // Collect ptrace samples within ftrace coverage
        def V_ = 0, mA_ = 0, mW_ = 0, samples = 0
        _uAh = _mWh = 0
        _data = new DataTable(Double.class, Double.class, Double.class, Double.class)
        ptrace.eachLine { String line, num ->
            samples = num
            if(num == 1) return
            def fields = line.split(',')
            def V = fields[2].toDouble()
            def mA = fields[1].toDouble()
            def mW = V * mA
            time = beginningP + fields[0].toDouble()
            if (time >= beginningF && time <= beginningF + duration) {
                if(num == 1 || num % 250 == 0)
                    _data.add(time - beginningF, V, mA, mW)
                if(num > 1) {
                    _uAh += (mA_ + mA) / 2 * INTERVAL * 1000
                    _mWh += (mW_ + mW) / 2 * INTERVAL
                    //_mWh += (V_ + V) / 2 * (mA_ + mA) / 2 * INTERVAL
                }
            }
            V_  = V
            mA_ = mA
            mW_ = mW
        }
        _seriesV = new DataSeries("Voltage", _data, 0, 1)
        _seriesI = new DataSeries("Current", _data, 0, 2)
        _seriesP = new DataSeries("Power", _data, 0, 3)
        printf("ptrace: %.3f(%.3fs), ftrace: %.3f(%.3fs)\n", beginningP, time - beginningP, beginningF, duration)

        def intervals = samples - 1
        def title = String.format("%.2fuAh(%.2fmWh) in %.2fs, %.2fmA/%.2fmW",
                                    _uAh, _mWh, _data.get(0, _data.getRowCount() - 1) - _data.get(0, 0),
                                    _uAh / 1000 / intervals / INTERVAL, _mWh / intervals / INTERVAL)
        println title

        //_plot = new XYPlot(_seriesV, _seriesI, _seriesP)
        _plot = new XYPlot(_seriesV, _seriesI)
        XYPlot.XYPlotArea2D plotArea = _plot.getPlotArea()
        plotArea.setBorderColor(null)
        plotArea.setMajorGridX(false)
        plotArea.setMajorGridY(false)
        plotArea.setClippingArea(null)
        formatLine(_plot, _seriesV, Color.BLUE)
        formatLine(_plot, _seriesI, Color.ORANGE)
        formatLine(_plot, _seriesP, Color.LIGHT_GRAY)

        _plot.getTitle().setText(title);
        _plot.setLegendVisible(true)
        _plot.getLegend().setOrientation(Orientation.VERTICAL)
        _plot.getLegend().setAlignmentX(1.0)
        _plot.getLegend().setAlignmentY(0.0)
        _plot.setLegendDistance(0.5)
        _plot.setInsets(new Insets2D.Double(20.0, 60.0, 60.0, 60.0)) // top, left, bottom, right
        _plot.setAxis(XYPlot.AXIS_Y2, new Axis(0, 10))
        _plot.setAxisRenderer(XYPlot.AXIS_Y2, new LinearRenderer2D())
        _plot.setMapping(_seriesV, XYPlot.AXIS_X, XYPlot.AXIS_Y2)
        Label labelX = new Label("Time(s)")
        Label labelY = new Label("Current(mA)")
        Label labelY2 = new Label("Voltage(V)")
        labelY.setRotation(90)
        labelY2.setRotation(90)
        _plot.getAxisRenderer(XYPlot.AXIS_X).setLabel(labelX)
        _plot.getAxisRenderer(XYPlot.AXIS_Y).setLabel(labelY)
        _plot.getAxisRenderer(XYPlot.AXIS_Y).setLabelDistance(-5)
        _plot.getAxisRenderer(XYPlot.AXIS_Y2).setLabel(labelY2)
        _plot.getAxisRenderer(XYPlot.AXIS_X).setTickSpacing(1.0)
        _plot.getAxisRenderer(XYPlot.AXIS_Y).setTickSpacing(200)
        _plot.getAxisRenderer(XYPlot.AXIS_Y2).setTickSpacing(1.0)
        _plot.getAxisRenderer(XYPlot.AXIS_X).setIntersection(-Double.MAX_VALUE)
        _plot.getAxisRenderer(XYPlot.AXIS_Y).setIntersection(-Double.MAX_VALUE)
        _plot.getAxisRenderer(XYPlot.AXIS_Y2).setIntersection(Double.MAX_VALUE)
        return this
*/
    }
/*
    public EnergyAnalyzer export() {
        return export(_path.getPath())
    }

    public EnergyAnalyzer export(String path) {
        try {
            DrawableWriter writer = DrawableWriterFactory.getInstance().get("application/postscript");
            writer.write(_plot, new FileOutputStream(new File(path, "trace.eps")), 1024, 768);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this
    }

    public EnergyAnalyzer show() {
        JFrame f = new JFrame()
        f.setSize(640, 480)
        f.getContentPane().add(new InteractivePanel(_plot))
        f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        f.setVisible(true)
    }
*/
}