package esms.tests

import esms.util.Log
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier
import gnu.io.NoSuchPortException;
import gnu.io.SerialPort
import spock.lang.Ignore
import spock.lang.IgnoreIf;
import spock.lang.Specification

import esms.profiling.LocalMonsoonProfiler
import spock.lang.Stepwise

@Stepwise
class MonsoonSpec extends Specification {
    static final String TAG = MonsoonSpec.class.getSimpleName()
    static File dev = new File('/dev/cu.usbmodem1421')

    def setupSpec() {
        Log.level(Log.Severity.DEBUG)
    }

    @Ignore
    //@IgnoreIf({!MonsoonSpec.dev.exists()})
    def "ConnectionTest"() {
        when:
        SerialPort comPort  = CommPortIdentifier.getPortIdentifier(dev.path).open(getClass().simpleName, 2000)

        then:
        notThrown NoSuchPortException
        comPort instanceof SerialPort

        when:
        SerialPort serialPort = comPort
        serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE)
        InputStream input = serialPort.getInputStream()
        OutputStream output = serialPort.getOutputStream()

        then:
        input
        output

        cleanup:
        input && input.close()
        output && output.close()
        serialPort.close()
    }

    @IgnoreIf({!MonsoonSpec.dev.exists()})
    def "DataCollectionTest"() {
        given:
        def options = [v:3.7f, samples:10, r:5000, status:true, usb:'auto']
        def BATCHING = "${System.getProperty('user.home')}/Dropbox/Batching"
        LocalMonsoonProfiler profiler = new LocalMonsoonProfiler(options);

        when:
        profiler.startService()
        profiler.startProfiling()
        profiler.wait(4000)
        Log.d(TAG, "break 1")
        profiler.stopProfiling()
        Log.d(TAG, "break 2")
        profiler.stopService()
        Log.d(TAG, "break 3")
        profiler.save(BATCHING)
        Log.d(TAG, "break 4")

        then:
        new File(BATCHING).listFiles(new FilenameFilter() {
            @Override
            boolean accept(File dir, String name) {
                return name.equals(profiler.trace())
            }
        }).length >= 0

        cleanup:
        if(profiler && profiler.trace())
            new File(profiler.trace()).delete()
    }

}
