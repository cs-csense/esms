package esms.tests

import groovy.io.FileType
import spock.lang.Ignore
import spock.lang.Specification

import esms.util.Log
import esms.data.batching.EnergyAnalyzer
import static esms.util.Log.Severity.*

class BatchingSpec extends Specification {
    def setupSpec() {
        Log.level(DEBUG)
    }
    def BATCHING = "${System.getProperty("user.home")}/Dropbox/Batching"

    @Ignore
    def "EnergyAnalayzer Test Suite"() {
        def done = false
        new File(BATCHING).eachFile(FileType.DIRECTORIES) {
            if(!done && it.name.startsWith('MSA2-')) {
                println("Analyzing traces in $it.absolutePath")
                def analyzer = new EnergyAnalyzer(it)
                analyzer.analyze()
                analyzer.export()
                //analyzer.show()
                done = true
            }
        }

        expect:
        assert 1 + 1 == 2
    }

    def "EnergyAnalyzer for Batching"() {
        def analyzer = new EnergyAnalyzer(BATCHING)
        analyzer.analyze()
        analyzer.save()

        expect:
        assert 1 + 1 == 2
    }
}
